const dev = process.env.NODE_ENV !== 'production';
export const host = dev ? 'http://localhost:5000' : 'http://alpha.yeastman.com:100';
export const retryCount = 5;
