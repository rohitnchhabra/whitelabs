import React from 'react';
import App from 'next/app';

// redux
import configureStore from 'appRedux/configureStore'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'

//progress
import NProgress from "next-nprogress/component";
import RequiredChangePasswordModal from "components/MyAccount/RequiredChangedModal";

// material-ui
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import JssProvider from 'react-jss/lib/JssProvider';
import getPageContext from 'src/getPageContext';
import 'styles/main.scss'
import Head from 'next/head';

class MyApp extends App {
	constructor(props) {
		super(props);
		this.pageContext = getPageContext();
		const {
			router: { pathname }
		} = this.props;
		this.state = {
			previousPath: pathname !== "/login"
		};
	}

	pageContext = null;

	static async getInitialProps({ Component, router, ctx }) {
	    let pageProps = {}
		
	    if (Component.getInitialProps) {
	      	pageProps = await Component.getInitialProps(ctx)
	    }

	    return { pageProps }
	  }

	componentDidMount() {
		// Remove the server-side injected CSS.
		const jssStyles = document.querySelector('#jss-server-side');
		if (jssStyles && jssStyles.parentNode) {
			jssStyles.parentNode.removeChild(jssStyles);
		}
	}

	componentDidUpdate() {	
			
		const { previousPath } = this.state;
		const {
			router: { pathname }
		} = this.props;
		if (!previousPath && pathname !== "/login") {
			this.setState({ previousPath: true })
		}
	}

	render() {		
		const { Component, pageProps, reduxStore, persistor } = this.props;
		
		return (
			<>
				<Head>
					{/* Use minimum-scale=1 to enable GPU rasterization */}
					<meta
						name='viewport'
						content={
							'user-scalable=0, initial-scale=1, ' +
							'minimum-scale=1, width=device-width, height=device-height'
						}
					/>
				</Head>
				<Provider store={reduxStore}>
					<PersistGate loading={null} persistor={persistor}>
						{/* Wrap every page in Jss and Theme providers */}
						<JssProvider
							registry={this.pageContext.sheetsRegistry}
							generateClassName={this.pageContext.generateClassName}
						>
							{/* MuiThemeProvider makes the theme available down the React
									tree thanks to React context. */}
							<MuiThemeProvider
								theme={this.pageContext.theme}
							>
								{/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
								<CssBaseline />
								<NProgress
									color="#f28411"
									options={{ trickleSpeed: 50 }}
									showAfterMs={250}
									spinner
									/>
								{/* Pass pageContext to the _document though the renderPage enhancer
										to render collected styles on server side. */}
									{	<RequiredChangePasswordModal/>}
									
								<Component pageContext={this.pageContext} {...pageProps} previousPath={this.state.previousPath} />
							</MuiThemeProvider>
						</JssProvider>
					</PersistGate>
				</Provider>
			</>
		);
	}
}

export default configureStore(MyApp)
