import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import NavBarUserSearchDrawerLayout from 'components/NavBar/NavBarUserSearchDrawerLayout';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import withInventory from 'hocs/inventory';
import PageContainer from 'components/UI/PageContainer';
import OrderDetails from 'components/MyOrders/OrderDetails';
import { userActions } from 'appRedux/actions/userActions';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import ReactGA from 'react-ga';
import { cartActions } from 'appRedux/actions/cartActions';
import SalesDriver from 'components/MyAccount/SalesDriver';

class MyOrders extends Component {

    componentDidMount() {
        this.props.getOrderHistory();
    }

    constructor(props) {
        super(props);
        this.state = {
            openDialog: false,
            openSalesDriver: false,
            order: null
        }

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/myorders');
    }

    handleOrderDetails = (order) => {
        this.setState({ openDialog: true, order: order });
    }

    handleLeaveOrderDetails = () => {
        this.setState({ openDialog: false });
    }

    handleSalesDriver = () => {
        this.setState({ openSalesDriver: true });
    }

    handleLeaveSalesDriver = () => {
        this.setState({ openSalesDriver: false });
    }

    render() {
        const { classes, theme, user } = this.props;

        return (
            <NavBarUserSearchDrawerLayout>
                <LoadingIndicator visible={user.isLoading} label={'Getting Orders'} />
                <PageContainer heading='MY ORDERS' id='cart-box'>
                    {user.salesDriverLinks && user.salesDriverLinks.length > 0 &&
                        <Grid item className="flex-center" style={{ marginBottom: '10px', marginLeft: 'auto', marginRight: 'auto', textTransform: 'none' }}>
                            <Button
                            variant='outlined'
                            color='primary'
                            onClick={e => { this.handleSalesDriver() }}
                            style={{textTransform: 'none', fontVariant: 'small-caps'}}
                            >
                                PartnersFirst
                        </Button>
                        </Grid>
                    }
                    <Grid container spacing={6}>
                        {this.props.user.orderHistory && this.props.user.orderHistory.map((order, i) => (
                            <Grid item xs={12}>
                                <div className={classes.card}>
                                    <div style={{ position: 'absolute', top: -15, left: 20, backgroundColor: '#fafafa', paddingLeft: 10, paddingRight: 10 }}>
                                        <Typography
                                            variant='h6'
                                            color='textPrimary'
                                        >
                                            Order # {order.orderNum}
                                        </Typography>
                                    </div>
                                    <Grid item container justify="center">
                                        <Grid item justify="center">
                                            <img
                                                className={classes.image}
                                                src='/static/images/yeast.png'
                                            />
                                        </Grid>
                                        <Grid
                                        
                                            item
                                            xs={12}
                                            sm
                                            container
                                            direction='column'
                                            style={{ marginTop: 20 }}
                                        >
                                            <Grid item  >
                                                <Typography
                                                    variant='overline'
                                                    color='textPrimary'
                                                    className="flex-center"
                                                >
                                                    Ship date is {order.shipdate}
                                                </Typography>
                                            </Grid>
                                            <Grid
                                                container
                                                item
                                                spacing={6}
                                                direction='row'
                                                className="flex-center"
                                            >
                                                <Grid item className="flex-center">
                                                    <Button
                                                        variant='outlined'
                                                        color='primary'
                                                        onClick={e => { this.handleOrderDetails(order) }}
                                                    >
                                                        Order Details
                                                </Button>
                                                </Grid>
                                                <Grid
                                                    container
                                                    item
                                                    spacing={6}
                                                    justify='flex-end'
                                                >
                                                    <Grid item xs={12} md={3}>
                                                        <div
                                                            style={{
                                                                backgroundColor:
                                                                    '#f28411',
                                                                padding: 2,
                                                                textAlign: 'center',
                                                            }}
                                                        >
                                                            <Typography
                                                                variant='h6'
                                                                color='secondary'
                                                            >
                                                                {order.status.replace('Approval', 'Fulfillment')}
                                                            </Typography>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Grid>
                        ))}
                    </Grid>

                    <Dialog
                        open={this.state.openDialog}
                        onClose={this.handleLeaveOrderDetails}
                        maxWidth={'lg'}
                    >
                        <OrderDetails order={this.state.order} store={this.props.store} closeDialog={this.handleLeaveOrderDetails} user={this.props.user}
                        addItem={this.props.addItem}
                        cart={this.props.cart}
                        clearCart={this.props.clearCart}
                        />
                    </Dialog>

                    <Dialog
                        open={this.state.openSalesDriver}
                        onClose={this.handleLeaveSalesDriver}
                        maxWidth={'lg'}
                    >
                        <SalesDriver user={this.props.user} closeDialog={this.handleLeaveSalesDriver} />
                    </Dialog>
                </PageContainer>
            </NavBarUserSearchDrawerLayout>
        );
    }
}

const styles = theme => ({
    container: {
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(4),
        [theme.breakpoints.up('md')]: {
            marginLeft: 50,
            marginRight: 50
        },
        [theme.breakpoints.up('lg')]: {
            marginLeft: 100,
            marginRight: 100
        },
        [theme.breakpoints.up('xl')]: {
            marginLeft: 150,
            marginRight: 150
        }
    },
    title: {
        backgroundColor: '#FF9933',
        padding: 5,
        marginBottom: theme.spacing(4),
        textAlign: 'center',
        marginLeft: theme.spacing(-4),
        marginRight: theme.spacing(-4)
    },
    hide: {
        display: 'none'
    },
    card: {
        position: 'relative',
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2)
    },
    image: {
        width: 170,
        marginRight: 10,
        textAlign: 'center'
    },
    quantity: {
        width: 50,
        marginRight: 20
    }
});

MyOrders.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user,
        cart: state.cart,

    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({...userActions,...cartActions}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(withInventory(MyOrders)));
