
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import withInventory from 'hocs/inventory';
import Router from 'next/router';
import { compose } from 'redux';
import { withRouter } from 'next/router';
import Link from 'next/link';
import axios from 'axios';

import PropTypes from 'prop-types';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';

import NavBarUserSearchDrawerLayout from 'components/NavBar/NavBarUserSearchDrawerLayout';
import FormButton from 'components/Form/FormButton';
import LoadingIndicator from '../components/UI/LoadingIndicator';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import { parseAvailabilityResults } from '../lib/InventoryUtils';
import { IN_STOCK, OUT_OF_STOCK } from '../lib/Constants';
import { DISALLOW_BACK_ORDERS } from "components/Store/constants";
import WLHelper from 'lib/WLHelper';
import ReactHtmlParser from 'react-html-parser'; 

const YeastElements = {
    '2': {
        img: 'static/images/categories/Category-core.jpg',
        color: '#FFF'
    },
    '3': {  // Ale
        img: 'static/images/categories/Category-ale.jpg',
        icon: 'static/images/icons/Ale-icon.svg',
        color: '#FF9933'
    },
    '4': {  // Wild Yeast
        img: 'static/images/categories/Category-wild.jpg',
        icon: 'static/images/icons/wildyeast-icon.svg',
        color: '#CC9966'
    },
    '5': {  // Lager
        img: 'static/images/categories/Category-lager.jpg',
        icon: 'static/images/icons/Lager-icon.svg',
        color: '#FFCC33'
    },
    '6': {  // Wine
        img: 'static/images/categories/Category-wine.jpg',
        icon: 'static/images/icons/wine-icon.svg',
        color: '#9966CC'
    },
    '7': {  // Distilling
        img: 'static/images/categories/Category-Distilling.jpg',
        icon: 'static/images/icons/Distilling-icon.svg',
        color: '#6666CC'
    },
    '8': {  // Belgian
        img: 'static/images/categories/Category-belgian.jpg',
        icon: 'static/images/icons/Belgian-icon.svg',
        color: '#66CCCC'
    },
    '32': { // Vault
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/vault-icon.svg',
        color: '#B3B3B3'
    },
    '997': { // Vault without Pure Pitch stock but with work orders
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/Yellow_Lock_v01.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    },
    '998': { // Organic
        img: 'static/images/categories/Category-organic.jpg',
        icon: 'static/images/icons/organic-icon.svg',
        color: '#B3B3B3',
        hbTitle: 'Organic'
    },
    '999': { // Vault with Pure Pitch stock
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/Green_Lock_v01.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    }
}

function getIcon(item, salesCategory) {
    try {
        var icons = [];
        if (item.isOrganic && salesCategory !== 998) {
            icons.push(YeastElements[998].icon);
        }
        icons.push(YeastElements[parseInt(salesCategory)].icon);
        return icons;
    }
    catch (err) {
        console.log('error', salesCategory, err);
    }
}

function getColor(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].color;
    }
    catch (error) {
        throw error;
    }
}

const FormikErrorMessage = ({ error }) => {
    return error ? <div className='error'>{error}</div> : null;
};

const customFormValidation = Yup.object().shape({
    packaging: Yup.string().required('Required'),
    pack: Yup.string().nullable(),
    quantity: Yup.string().required('Required'),
});


class Yeastparam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: '1',
            packOptions: [
                { label: 'Nano', value: '0' },
                { label: '1.5L', value: '1' },
                { label: '1.5L', value: '-1' },
                { label: '2L', value: '2' }
            ],
            pack: '0',
            packagingOptions: [],
            packaging: 'pp',
            availability: null,
            availabilitySD: null,
            availabilityCPH: null,
            availabilityHK: null,
            isLoading: false,
            notifyEmail: null,
            errorNotifyEmailMsg: null,
            confirmNotifyEmailMsg: null,
            errors: {}
        };
        const {
            router: { query }
        } = this.props;
        let { item } = query;
        this.filteredItem = this.props.inventory.items.find(v => v.partNum === item)
    }

    componentDidMount() {
        if (this.filteredItem && this.filteredItem.volID[6]) {
            this.setState({ packaging: '6', pack: null });
        } else if (this.filteredItem && this.filteredItem.volID[0] && this.filteredItem && this.filteredItem.volID[2] && !this.noPurePitchForVaultItem()) {
            if (this.filteredItem && this.filteredItem.purePitch) {
                this.setState({ packaging: 'pp', pack: '0' });
            } else {
                this.setState({ packaging: 'nl', pack: '0' });
            }
        } else if (this.filteredItem && this.filteredItem.volID[0] && !this.noPurePitchForVaultItem()) {
            this.setState({ packaging: '0', pack: null });
        } else {
            this.setState({ packaging: '3', pack: null });
        }

        this.filterPackageTypes();
    }

    noPurePitchForVaultItem() {
        var noPurePitch = true;

        if (this.filteredItem) {
            if (this.filteredItem.isVaultItem && this.filteredItem.availQty) {
                // Make sure there's a PurePitch size in stock
                for (var i = 0; i < 3; i++) {
                    if (this.filteredItem.availQty[i] > 0) {
                        noPurePitch = false;
                        break;
                    }
                }
            } else {
                noPurePitch = false;
            }

            // Check for PP work orders
            if (this.filteredItem.vaultWorkOrders) {
                for (var i = 0; i < this.filteredItem.vaultWorkOrders.length; i++) {
                    if (
                        this.filteredItem.vaultWorkOrders[i].ItemId == this.filteredItem.volID[0]
                        || this.filteredItem.vaultWorkOrders[i].ItemId == this.filteredItem.volID[1]
                        || this.filteredItem.vaultWorkOrders[i].ItemId == this.filteredItem.volID[2]
                    ) {
                        noPurePitch = false;

                        // See if we need to set the available quantity to make the PP size orderable
                        if (this.filteredItem.availQty) {
                            if (this.filteredItem.vaultWorkOrders[i].ItemId == this.filteredItem.volID[0]) {
                                if (!this.filteredItem.availQty[0]) this.filteredItem.availQty[0] = parseInt(this.filteredItem.vaultWorkOrders[i].Qty);
                            } else if (this.filteredItem.vaultWorkOrders[i].ItemId == this.filteredItem.volID[1]) {
                                if (!this.filteredItem.availQty[1]) this.filteredItem.availQty[1] = parseInt(this.filteredItem.vaultWorkOrders[i].Qty);
                            } else if (this.filteredItem.vaultWorkOrders[i].ItemId == this.filteredItem.volID[2]) {
                                if (!this.filteredItem.availQty[2]) this.filteredItem.availQty[2] = parseInt(this.filteredItem.vaultWorkOrders[i].Qty);
                            }
                        }
                    }
                }
            }

            // No Pure Pitch available
        }

        return noPurePitch;
    }

    filterPackageTypes() {
        try {
            var PackageTypes = [
                { label: '1L Nalgene Bottle', value: '6' },
                { label: 'PurePitch', value: 'pp' },
                { label: 'Nalgene Bottle', value: 'nl' },
                { label: 'Custom Pour', value: '3' },
                { label: 'Homebrew', value: '4' },
                { label: 'Slant', value: '5' },
                { label: 'Yeast', value: '0' }
            ];

            var subsidiary = (this.props.user.subsidiary ? this.props.user.subsidiary : 2),
                FilteredPackageTypes = [];

            if (this.filteredItem) {
                if (this.filteredItem.volID[6]) {
                    FilteredPackageTypes.push(PackageTypes[0]);
                } else if (this.filteredItem.purePitch || (this.filteredItem.volID[0] && this.filteredItem.volID[2])) {
                    if (this.filteredItem.purePitch) {
                        if (!this.noPurePitchForVaultItem()) {
                            FilteredPackageTypes.push(PackageTypes[1]);
                        }
                    } else {
                        FilteredPackageTypes.push(PackageTypes[2]);
                    }
                } else if (this.filteredItem.volID[0]) {
                    FilteredPackageTypes.push(PackageTypes[6]);
                }

                if ((this.filteredItem.volID[3]) || this.filteredItem.purePitch) {
                    FilteredPackageTypes.push(PackageTypes[3]);
                }

                if (this.filteredItem.volID[4] && subsidiary == 2 && this.filteredItem.hbAvailQty > 0) {
                    FilteredPackageTypes.push(PackageTypes[4]);
                }

                if (this.filteredItem.volID[5]) {
                    FilteredPackageTypes.push(PackageTypes[5]);
                }
            }

            this.setState({ packagingOptions: FilteredPackageTypes });
        } catch (error) {
            throw error;
        }
    }

    handleErrors = (field, err) => {
        let { errors } = this.state;
        errors[field] = err
        this.setState({ errors })
    }

    checkVaultPurepitch = () => {
        if (this.filteredItem && !this.filteredItem.salesCategoryIconOverrideChecked && this.filteredItem.availQty && this.filteredItem.availQty.length > 0) {
            if (this.filteredItem.vaultWorkOrders && this.filteredItem.vaultWorkOrders.length > 0) {
                this.filteredItem.salesCategoryIconOverride = 997;
            }

            for (var i = 0; i < 3; i++) {
                if (this.filteredItem.availQty[i] > 0) {
                    this.filteredItem.salesCategoryIconOverride = 999;
                    this.setState({ salesCategoryIconOverride: 999 });
                    break;
                }
            }

            // Also check for standard pour quantity available
            if (!this.filteredItem.salesCategoryIconOverride && this.filteredItem.availQty.length >= 7) {
                if (this.filteredItem.availQty[6] > 0) {
                    this.filteredItem.salesCategoryIconOverride = 999;
                    this.setState({ salesCategoryIconOverride: 999 });
                }
            }

            this.filteredItem.salesCategoryIconOverrideChecked = true;
        }
    }

    checkQuantity = item => {
        try {
            var quantity = parseFloat(item.OrderDetailQty);

            if (isNaN(quantity) || quantity <= 0) {
                // TO-DO: Display message to user
                this.handleErrors('quantity', 'Please enter a valid value for the quantity')
                return false;
            }

            // Wild Yeast must have mimimum 1L
            if (item.salesCategory == 4 && quantity < 1.0) {
                this.handleErrors('quantity', 'Notice: The minimum quantity sold for Wild Yeast strains is 1L. Please adjust your quantity.')
                return false;
            }

            // Custom Pour Strains
            if (item.type == 5) {
                // Vault strains must have minimum 1.5L Custom Pour
                if (item.salesCategory == 32) {
                    if (quantity < 1.5) {
                        // TO-DO: Display message to user
                        this.handleErrors('quantity', 'Notice: The minimum quantity sold for Custom Pour Vault strains is 1.5L. Please adjust your quantity.')

                        return false;
                    } else if ((parseFloat(quantity) / parseInt(quantity) != 1.0) && (parseFloat(quantity + 0.5) / parseInt(quantity + 0.5) != 1.0)) {
                        this.handleErrors('quantity', 'Notice: Custom Pour Vault strains are sold in 0.5L increments. Please adjust your quantity.')

                        return false;
                    }
                }

                // Bacteria sold in 1L increments
                if (item.salesCategory == 4) {
                    if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
                        quantity = Math.round(quantity);

                        // TO-DO: Display message to user
                        this.handleErrors('quantity', 'Notice: Quantities for this strain must be in 1L increments, your value has been rounded accordingly. Please review your cart.')

                    }
                }

                // All other custom pour strains sold in 0.5L increments
                else {
                    if ((parseFloat(quantity) / parseInt(quantity) != 1.0) && (parseFloat(quantity + 0.5) / parseInt(quantity + 0.5) != 1.0)) {
                        this.handleErrors('quantity', 'Notice: Custom Pours are sold in 0.5L increments. Please adjust your quantity.')

                        return false;
                    }

                }

                item.size = quantity;
                item.details = quantity + 'L Custom Pour';
                item.OrderDetailQty = parseFloat(quantity);
            } else if (item.minQty && item.minQty > quantity) {
                this.handleErrors('quantity', 'Notice: Minimum quantity of this strain/size is ' + item.minQty + '. Please adjust your quantity.')
                return false;
            }
            // Non-custom pour strains must be in increments of 1
            else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
                this.handleErrors('quantity', 'Quantity Error !!')
                return false;
            }

            return true;
        } catch (error) {
            this.handleErrors('quantity', `Could not check quantity ${error}`)
            throw error;
        }
    };

    addToCart = (values) => {
        const packaging = this.state.packaging;
        const pack = this.state.pack;
        const quantity = this.state.quantity;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(this.filteredItem.Name);
        cartItem.salesCategory = parseInt(this.filteredItem.salesCategory);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.mfgEnvironment = this.filteredItem.mfgEnvironment;
        cartItem.isVaultItem = this.filteredItem.isVaultItem;
        cartItem.imageUrl = this.filteredItem.imageUrl;
        cartItem.isOrganic = this.filteredItem.isOrganic;

        // PurePitch / 1L Nalgene bottle
        if (packaging == 'pp' || packaging == 'nl') {
            switch (pack) {
                // Nano
                case '0':
                    cartItem.MerchandiseID = this.filteredItem.volID[0];
                    cartItem.details = 'Nano';
                    if (cartItem.isVaultItem) cartItem.minQty = 3;
                    break;

                // 1.5L
                case '1':
                    cartItem.MerchandiseID = this.filteredItem.volID[1];
                    cartItem.details = '1.5L';
                    break;

                // 2L
                case '2':
                    cartItem.MerchandiseID = this.filteredItem.volID[2];
                    cartItem.details = '2L';
                    break;
                default:
                    this.handleErrors('pack', `cannot add to cart, ${item}, ${packaging}, ${pack}, ${quantity}`)
                    return;
            }

            if (this.filteredItem.purePitch) {
                cartItem.details = 'PurePitch® ' + cartItem.details;
            }

            cartItem.type = 1;
        } else {
            switch (packaging) {
                // Yeast
                case '0':
                    cartItem.MerchandiseID = this.filteredItem.volID[0];
                    // Per Mike, treat as PurePitch reather than as Non-Yeast
                    cartItem.type = 1; //Was 3;
                    cartItem.details = 'Yeast';
                    break;

                // Custom Pour
                case '3':
                    cartItem.MerchandiseID = this.filteredItem.volID[3];
                    cartItem.type = 5;
                    cartItem.dispQuantity = 1;
                    cartItem.size = parseFloat(quantity);
                    cartItem.details = quantity + 'L Custom Pour';
                    cartItem.relatives = [];
                    var multipliers = [0.5, 1.5, 2];

                    for (var i = 0; i < 3; i++) {
                        if (this.filteredItem.volID[i]) {
                            var relative = {};
                            relative.id = parseInt(this.filteredItem.volID[i]);
                            if (isNaN(relative.id)) {
                                throw { message: 'Invalid VolID Index! in Relatives', code: 0 };
                            }
                            relative.mult = multipliers[i];
                            cartItem.relatives.push(relative);
                        }
                    }
                    break;

                // Homebrew
                case '4':
                    cartItem.MerchandiseID = this.filteredItem.volID[4];
                    cartItem.type = 2;
                    cartItem.details = 'Homebrew packs';
                    break;

                // Slant
                case '5':
                    cartItem.MerchandiseID = this.filteredItem.volID[5];
                    // Per Mike, treat as PurePitch reather than as Non-Yeast
                    cartItem.type = 1; //Was 3;
                    cartItem.details = 'Slants';
                    break;

                // 1L Nalgene Bottle
                case '6':
                    cartItem.MerchandiseID = this.filteredItem.volID[6];
                    cartItem.type = 1;
                    cartItem.details = '1L Nalgene Bottle';
                    break;
            }
        }

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    relItemArray[k].cartItemType = WLHelper.getYMO2TypeForItem(relatedItems[i].related[j], relItemArray[k]);
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                    })
                    .catch(error => {
                        console.log('error', error);

                        // Still add the item to the cart
                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                    });
            }
        }
    };

    checkAvailability = () => {
        var { subsidiary } = this.props.user;
        const packaging = this.state.packaging;
        const pack = this.state.pack;
        let itemID;

        if (packaging == 'pp' || packaging == 'nl') {
            switch (pack) {
                // Nano
                case '0':
                    itemID = this.filteredItem.volID[0]
                    break;

                // 1.5L
                case '1':
                    itemID = this.filteredItem.volID[1];
                    break;

                // 2L
                case '2':
                    itemID = this.filteredItem.volID[2];
                    break;

                default:
                    return;
            }
        } else {
            switch (packaging) {
                // Homebrew
                case '4':
                    itemID = this.filteredItem.volID[4];
                    break;

                // Slant
                case '5':
                    itemID = this.filteredItem.volID[5];
                    break;

                // 1L Nalgene Bottle
                case '6':
                    itemID = this.filteredItem.volID[6];
                    break;

                // Distilling
                case '0':
                    itemID = this.filteredItem.volID[0]
                    break;

                // Vault
                case '3':
                    itemID = this.filteredItem.volID[3]
                    break;

                default:
                    return;

            }
        }

        const quantity = this.state.quantity;

        this.setState({ isLoading: true });
        if (!subsidiary || subsidiary === '') {
            var subs = [2, 5, 7];
            this.setState({ availability: null });

            // Hacky "switch" statement to handle indeterminate async call duration
            // cross-contaminating result variables. Also, separate availabilities to make
            // sure they are always displayed in the same order in the results.
            for (var i = 0; i < subs.length; i++) {
                this.setState({ isLoading: true });
                switch (i) {
                    case 0:
                        var s = subs[i];
                        subsidiary = s;
                        axios.post('/item-availability', { itemID, subsidiary, quantity })
                            .then(({ data: { availability, error, availabilityMessage } }) => {
                                if (error) throw error;
                                this.setState({ availabilitySD: parseAvailabilityResults(availability, s, quantity, availabilityMessage) });
                            })
                            .catch(error => {
                                console.log('error', error);
                            })
                            .finally(() => this.setState({ isLoading: false }))
                        break;
                    case 1:
                        var s1 = subs[i];
                        subsidiary = s1;
                        axios.post('/item-availability', { itemID, subsidiary, quantity })
                            .then(({ data: { availability, error, availabilityMessage } }) => {
                                if (error) throw error;
                                this.setState({ availabilityHK: parseAvailabilityResults(availability, s1, quantity, availabilityMessage) });
                            })
                            .catch(error => {
                                console.log('error', error);
                            })
                            .finally(() => this.setState({ isLoading: false }))
                        break;
                    case 2:
                        var s2 = subs[i];
                        subsidiary = s2;
                        axios.post('/item-availability', { itemID, subsidiary, quantity })
                            .then(({ data: { availability, error, availabilityMessage } }) => {
                                if (error) throw error;
                                this.setState({ availabilityCPH: parseAvailabilityResults(availability, s2, quantity, availabilityMessage) });
                            })
                            .catch(error => {
                                console.log('error', error);
                            })
                            .finally(() => this.setState({ isLoading: false }))
                        break;
                }
            }
        } else {
            this.setState({ availabilitySD: null });
            this.setState({ availabilityCPH: null });
            this.setState({ availabilityHK: null });

            const useSubsidiary = (this.filteredItem.isOrganic ? 7 : subsidiary);

            axios.post('/item-availability', { itemID, subsidiary: useSubsidiary, quantity })
                .then(({ data: { availability, error, availabilityMessage } }) => {
                    if (error) throw error;
                    var availabilityText = parseAvailabilityResults(availability, useSubsidiary, quantity, availabilityMessage);
                    if (this.filteredItem.isOrganic && subsidiary == 2 && availabilityText.indexOf(IN_STOCK) >= 0)
                        availabilityText = availabilityText.replace('Copenhagen', 'Copenhagen and available for shipping to USA');

                    this.setState({ availability: availabilityText });
                })
                .catch(error => {
                    console.log('error', error);
                })
                .finally(() => this.setState({ isLoading: false }));
        }
    }

    setPack = event => {
        this.setState({
            pack: event.target.value,
            availabilityCPH: null, availabilityHK: null, availabilitySD: null, availability: null
        });
    };

    setPackaging = event => {
        var packaging = event.target.value;
        var pack;

        if (packaging == 'pp' || packaging == 'nl') {
            pack = '0';
        }

        this.setState({
            packaging: event.target.value,
            pack: pack,
            availabilityCPH: null, availabilityHK: null, availabilitySD: null, availability: null
        });
    };

    changeQuantity = event => {
        this.setState({ quantity: event.target.value, availabilityCPH: null, availabilityHK: null, availabilitySD: null, availability: null });
    };

    moveToCalculator = () => {
        Router.push(`/calculator?id=${this.filteredItem.volID[0]}`);
    }

    handleBack = () => {
        //set data in localstorage 
        //it indicated store page to loads previous data of page 
        localStorage.setItem("load", true)
        Router.push(`/`);
    }

    notifyEmailChange = event => {
        const email = event.target.value
        this.setState({ notifyEmail: email });
    }

    requestVaultWorkOrderNotification = () => {
        if (this.filteredItem) {
            const email = (this.state.notifyEmail ? this.state.notifyEmail : this.user.email);
            var errorNotifyEmailMsg = null;
            var errorNotifyEmail = false;

            if (email) {
                let arr = email.split(',');
                var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                for (let i = 0; i < arr.length; i++) {
                    if (!re.test(arr[i])) {
                        errorNotifyEmail = true;
                        errorNotifyEmailMsg = "* Please Enter Valid Email(s)";
                        break;
                    }
                }

                if (!errorNotifyEmail && email.length > 300) {
                    errorNotifyEmail = true;
                    errorNotifyEmailMsg = "* Maximum length of field is 300 characters";
                }
            }

            if (errorNotifyEmail) {
                this.setState({ errorNotifyEmailMsg, confirmNotifyEmailMsg: null });
            } else {
                let request = {};
                request.isQuestionnaireResponse = true;
                request.source = 'Vault Work Order Notification';
                if (this.props.user) request.id = this.props.user.id;

                request.questions = [];
                request.questions.push({ question: 'Strain No', answer: this.filteredItem.partNum });
                request.questions.push({ question: 'Email Address', answer: email });

                this.setState({ isLoading: true });

                axios.post('/record-survey-responses', { request })
                    .then(({ data: { error } }) => {
                        if (error) throw error;
                        this.setState({ confirmNotifyEmailMsg: 'Thank you! Your request has been recorded!', errorNotifyEmailMsg: null });
                    })
                    .catch(error => {
                        this.setState({ errorNotifyEmailMsg: error, confirmNotifyEmailMsg: null });
                    })
                    .finally(() => this.setState({ isLoading: false }));
            }
        }
    }

    render() {
        this.checkVaultPurepitch();

        const { classes } = this.props;
        const { errors, availability, availabilityCPH, availabilityHK, availabilitySD } = this.state;
        const spaceIndex = this.filteredItem && this.filteredItem.Name.indexOf(' ');
        const volID = this.filteredItem && this.filteredItem.volID;
        const error = errors.packaging || errors.pack || errors.quantity;
        const availQty = this.filteredItem && this.filteredItem.availQty;
        const isVaultItem = this.filteredItem && this.filteredItem.isVaultItem;

        const nextShipDateMsg = (
            availability && availability.indexOf(IN_STOCK) >= 0 ? '' :
                (availabilitySD && !availabilitySD.indexOf(IN_STOCK) >= 0)
                    || (availabilityCPH && !availabilityCPH.indexOf(IN_STOCK) >= 0)
                    || (availabilityHK && !availabilityHK.indexOf(IN_STOCK) >= 0) ? 'For out-of-stock strains, ship dates are generally less than 10 days; dates are shown in checkout.' : ''
        );

        const icons = (this.filteredItem ? getIcon(this.filteredItem, this.filteredItem.salesCategoryIconOverride ? this.filteredItem.salesCategoryIconOverride : this.filteredItem.salesCategory) : null);
        const showOrganicAlternative = (!this.props.user.subsidiary && this.filteredItem && this.filteredItem.hasOrganicVersion && !this.filteredItem.isOrganic);
        const showNonOrganicAlternative = ((!this.props.user.subsidiary || this.props.user.subsidiary != 7) && this.filteredItem && this.filteredItem.isOrganic);

        var itemDescription = this.filteredItem ? '<div style="max-height:480px; overflow-y: auto; overflow-x: hidden;">' + this.filteredItem.Description + '</div>' : '';
        itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

        const { id, email } = this.props.user;

        return (
            <NavBarUserSearchDrawerLayout>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <Grid item xs={1} dir='ltr'>
                    <FormButton className='back-param' text='Back' onClick={this.handleBack} />
                </Grid>
                <div className={classes.container}>
                    <Grid
                        item
                        container
                        xs
                        className={classes.displayMargin}
                        direction={'row'}
                    // spacing={4}
                    >
                        <Grid item style={{ display: 'flex' }}>
                            <Typography variant='h5'
                                className={classes.titleMargin}
                            >
                                {this.filteredItem && this.filteredItem.Name}
                                <Divider variant='middle' />
                            </Typography>

                            <div
                                className={classes.circleLg}
                                style={{ backgroundColor: this.filteredItem ? getColor(this.filteredItem.salesCategory) : '' }}
                            >
                                {!icons || icons.length == 0 ? null :
                                    <img
                                        src={icons[0]}
                                        className={classes.imgHieght}
                                    />
                                }
                                {
                                    icons && icons.length > 1 && <img
                                        src={icons[1]}
                                        className={classes.imgHieght}
                                    />
                                }
                            </div>
                        </Grid>
                        <div
                            className={classes.circleSm}
                            style={{ backgroundColor: this.filteredItem ? getColor(this.filteredItem.salesCategory) : '' }}
                        >
                            {!icons || icons.length == 0 ? null :
                                <img
                                    src={icons[0]}
                                    className={classes.imgHieght}
                                />
                            }
                            {
                                icons && icons.length > 1 && <img
                                    src={icons[1]}
                                    className={classes.imgHieght}
                                />
                            }
                        </div>

                        <Grid container spacing={6}>

                        </Grid>

                        <Grid
                            item
                            container
                            justify='center'
                            style={{ marginTop: '15px' }}
                            direction={'row'}
                            spacing={2}
                        >
                            <Grid item xs={12} md={6} justify='center'>
                                <div className='flex-center'>
                                    <Typography className='dialogVariant' >Attenuation:</Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: this.filteredItem ? getColor(this.filteredItem.salesCategory) : '' }}
                                    >
                                        {this.filteredItem && this.filteredItem.attenuation}
                                    </Typography>
                                </div>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <div className='flex-center' >
                                    <Typography className='dialogVariant'>Flocculation: </Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: this.filteredItem ? getColor(this.filteredItem.salesCategory) : '' }}
                                    >
                                        {this.filteredItem && this.filteredItem.flocculation}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <div className='flex-center'>
                                    <Typography className='dialogVariant'>Alcohol Tol.: </Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: this.filteredItem ? getColor(this.filteredItem.salesCategory) : '' }}
                                    >
                                        {this.filteredItem && this.filteredItem.alcoholTol}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <div className='flex-center'>
                                    <Typography className='dialogVariant'>STA1: </Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: this.filteredItem ? getColor(this.filteredItem.salesCategory) : '' }}
                                    >
                                        {this.filteredItem && this.filteredItem.sta1}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <div className='flex-center'>
                                    <Typography className='dialogVariant'>Fermentation Temp: </Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: this.filteredItem ? getColor(this.filteredItem.salesCategory) : '' }}
                                    >
                                        {this.filteredItem && this.filteredItem.optFermentTempF}
                                        {this.filteredItem && this.filteredItem.optFermentTempF && this.filteredItem.optFermentTempC ? ' (' : ''}
                                        {this.filteredItem && this.filteredItem.optFermentTempC}
                                        {this.filteredItem && this.filteredItem.optFermentTempF && this.filteredItem.optFermentTempC ? ')' : ''}
                                    </Typography>
                                </div>
                            </Grid>
                        </Grid>

                        <Grid
                            item
                            container
                            direction={'column'}
                            spacing={6}
                            className={classes.description}
                        >
                            <Grid
                                item
                                container
                                direction={'column'}
                                spacing={6}
                                className={classes.description}
                            >
                                {this.filteredItem && this.filteredItem.Description ? <Grid
                                    item
                                    container
                                    direction={'column'}
                                    spacing={6}
                                    className={classes.description}
                                >
                                    <Grid item>
                                        <div>{ReactHtmlParser(itemDescription)}</div>
                                    </Grid>
                                </Grid> : null
                                }
                                {
                                    this.filteredItem && this.filteredItem.outofstockmessage &&
                                    <Grid item>
                                        <Typography style={{ paddingTop: '10px', fontWeight: 'bold' }}>
                                            {this.filteredItem.outofstockmessage}
                                        </Typography>
                                    </Grid>
                                }
                            </Grid>
                        </Grid>

                        {
                            id && id != 0 && isVaultItem && this.filteredItem && this.filteredItem.salesCategoryIconOverride != 997 && this.filteredItem.salesCategoryIconOverride != 999 &&
                            <Grid
                                item
                                container
                                direction={'column'}
                                spacing={2}
                                style={{ color: '#f68f32', border: 'solid 1px', padding: '10px', marginTop: '10px' }}
                            >
                                {!this.state.confirmNotifyEmailMsg &&
                                    <>
                                        <Typography style={{ fontWeight: 'bolder' }}>We can notify you when this strain goes into production!</Typography>
                                        <TextField
                                            id="outlined-helperText"
                                            variant="outlined"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            name="Email"
                                            fullWidth
                                            type="email"
                                            placeholder={email}
                                            value={this.state.notifyEmail}
                                            onChange={this.notifyEmailChange}
                                        />
                                        {
                                            this.state.errorNotifyEmailMsg &&
                                            <Typography style={{ fontSize: 'small', color: 'red' }}>
                                                {this.state.errorNotifyEmailMsg}
                                            </Typography>
                                        }
                                        <Typography style={{ fontSize: 'x-small' }}>
                                            We will notify you at your main email address. To be notified at other email addresses, enter them above, separated with commas.
                                    </Typography>
                                        <div className={classes.buttons}>
                                            <Button
                                                variant='contained'
                                                color='primary'
                                                onClick={this.requestVaultWorkOrderNotification}
                                                className={classes.button}
                                            >
                                                Notify me!
                                        </Button>
                                        </div>
                                    </>
                                }
                                {
                                    this.state.confirmNotifyEmailMsg &&
                                    <Typography style={{ fontSize: 'small', color: 'green', textAlign: 'center' }}>
                                        {this.state.confirmNotifyEmailMsg}
                                    </Typography>
                                }
                            </Grid>
                        }

                        <Grid
                            item
                            container
                            direction={'column'}
                            spacing={2}
                            style={{ marginTop: 10, color: '#f68f32' }}
                        >
                            <Button onClick={this.moveToCalculator}>
                                <Grid item>
                                    <Typography style={{ color: this.filteredItem ? getColor(this.filteredItem.salesCategory) : '' }}>How much do I need?</Typography>
                                </Grid>
                            </Button>
                        </Grid>

                        <Grid
                            item
                            container
                            direction={'row'}
                            justify='center'
                        >
                            <Grid
                                item
                                xs
                                container
                                spacing={6}
                                direction={'row'}
                                justify='center'
                            >
                                {availability ?
                                    <div>
                                        <Typography className='flex-center' style={{ color: availability.indexOf(IN_STOCK) >= 0 ? 'green' : 'red' }}>
                                            <p style={{ textAlign: 'center' }}>{availability}</p>
                                        </Typography>
                                        <Typography className='flex-center'>
                                            <p style={{ textAlign: 'center' }}>{nextShipDateMsg}</p>
                                        </Typography>
                                    </div>
                                    :
                                    (availabilitySD || availabilityCPH || availabilityHK) ?
                                        <div>
                                            <Typography className='flex-center'>
                                                <p style={{ textAlign: 'center', color: availabilitySD && availabilitySD.indexOf(IN_STOCK) >= 0 ? 'green' : 'red' }}>{availabilitySD}</p>
                                            </Typography>
                                            {showOrganicAlternative ? null :
                                                <Typography className='flex-center'>
                                                    <p style={{ textAlign: 'center', color: availabilityCPH && availabilityCPH.indexOf(IN_STOCK) >= 0 ? 'green' : 'red' }}>
                                                        {availabilityCPH}
                                                    </p>
                                                </Typography>
                                            }
                                            <Typography className='flex-center'>
                                                <p style={{ textAlign: 'center', color: availabilityHK && availabilityHK.indexOf(IN_STOCK) >= 0 ? 'green' : 'red' }}>{availabilityHK}</p>
                                            </Typography>
                                            <Typography className='flex-center'>
                                                <p style={{ textAlign: 'center' }}>{nextShipDateMsg}</p>
                                            </Typography>
                                        </div>
                                        :
                                        (!this.state.pack || this.state.pack >= 0) &&
                                        <Grid
                                            item
                                            xs
                                            container
                                            spacing={6}
                                            direction={'row'}
                                            justify='center'
                                        >
                                            <div className={classes.buttons}>
                                                <Button
                                                    variant='contained'
                                                    color='primary'
                                                    onClick={this.checkAvailability}
                                                    className={classes.button}
                                                >
                                                    Get Availability
                                            </Button>
                                            </div>
                                        </Grid>
                                }
                            </Grid>
                        </Grid>

                        {this.filteredItem && this.filteredItem.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                            <Grid
                                item
                                xs
                                container
                                spacing={6}
                                className={classes.formWrapMargin}
                                direction={'row'}
                            >
                                <Formik
                                    initialValues={this.state}
                                    validationSchema={customFormValidation}
                                    onSubmit={values => this.addToCart(values)}
                                >
                                    {({ values, handleChange }) => {
                                        return (
                                            <Form className={classes.form} >
                                                <FormikErrorMessage error={error} />
                                                <Grid
                                                    item
                                                    xs
                                                    container
                                                    spacing={6}
                                                    direction={'row'}
                                                    justify='center'
                                                    className={classes.paddingFix}
                                                >
                                                    <Grid item xs={12} sm={4} md={4} className={classes.formFields} >
                                                        <FormControl>
                                                            <InputLabel >Packaging</InputLabel>
                                                            <Select
                                                                value={this.state.packaging}
                                                                onChange={this.setPackaging}
                                                            >
                                                                {this.state.packagingOptions.map(
                                                                    (option, i) => (
                                                                        <MenuItem
                                                                            key={i}
                                                                            value={option.value}
                                                                        >
                                                                            {option.label}
                                                                        </MenuItem>
                                                                    )
                                                                )}
                                                            </Select>
                                                        </FormControl>
                                                    </Grid>
                                                    {this.state.pack && (
                                                        <Grid item xs={12} sm={4} md={4} className={classes.formFields}>
                                                            <FormControl>
                                                                <InputLabel>Pack</InputLabel>
                                                                <Select
                                                                    value={this.state.pack}
                                                                    onChange={this.setPack}
                                                                >
                                                                    {this.state.packOptions.map(
                                                                        (option, i) => (
                                                                            ((option.value >= 0 && volID[parseInt(option.value)]) || (!volID[parseInt(Math.abs(option.value))] && option.value < 0 && !this.filteredItem.isOrganic)) // Pack size exists for item
                                                                                && (!isVaultItem || !availQty || option.value >= 0 && availQty[parseInt(Math.abs(option.value))] > 0)
                                                                                ?
                                                                                <MenuItem
                                                                                    key={i}
                                                                                    value={option.value}
                                                                                >
                                                                                    {option.label}
                                                                                </MenuItem>
                                                                                : null
                                                                        )
                                                                    )}
                                                                </Select>
                                                            </FormControl>
                                                        </Grid>
                                                    )}
                                                    {(!this.state.pack || this.state.pack >= 0) &&
                                                        <Grid item xs={12} sm={4} md={4} className={classes.formFields}>
                                                            <TextField
                                                                id='quantity'
                                                                label='Quantity'
                                                                className={classes.quantity}
                                                                value={this.state.quantity}
                                                                onChange={this.changeQuantity}
                                                                type='number'
                                                            />
                                                        </Grid>
                                                    }
                                                </Grid>
                                                {this.state.pack == '1' &&
                                                    <div style={{ textAlign: 'center', marginTop: '10px', marginBottom: '10px' }}>
                                                        <Typography style={{ color: 'red', fontWeight: 'bolder', fontSize: 'smaller' }}>
                                                            PLEASE NOTE: White Labs is phasing out new lots of 1.5L PurePitch packages (still available in Custom Pour).
                                                            To determine if your strain is still available in PurePitch, set the quantity you are looking for and hit the
                                                            "Get Availability" button. If not in stock, please use a combination of Nanos (0.5Ls) and 2Ls. For instance,
                                                            if you normally order two 1.5L packages, please order one 2L and two Nanos. We have adjusted prices to keep
                                                            prices the same.
                                                    </Typography>
                                                    </div>
                                                }
                                                {this.state.pack == '-1' &&
                                                    <div style={{ textAlign: 'center', marginTop: '10px', marginBottom: '10px' }}>
                                                        <Typography style={{ color: 'red', fontWeight: 'bolder', fontSize: 'smaller' }}>
                                                            PLEASE NOTE: White Labs is phasing out new lots of 1.5L PurePitch packages (still available in Custom Pour) and
                                                            this strain is no longer in stock in the 1.5L size. Please use a combination of Nanos (0.5Ls) and 2Ls. For instance,
                                                            if you normally order two 1.5L packages, please order one 2L and two Nanos. We have adjusted prices to keep
                                                            prices the same.
                                                    </Typography>
                                                    </div>
                                                }
                                                {(!this.filteredItem || this.filteredItem.outofstockbehavior !== DISALLOW_BACK_ORDERS) && (!this.state.pack || this.state.pack >= 0) &&
                                                    <Grid
                                                        item
                                                        xs
                                                        container
                                                        spacing={6}
                                                        direction={'row'}
                                                        justify='center'
                                                    >
                                                        <div className={classes.addButton}>
                                                            <Button
                                                                type='submit'
                                                                variant='contained'
                                                                color='primary'
                                                            >
                                                                Add to Cart
                                                        </Button>
                                                        </div>
                                                    </Grid>
                                                }
                                            </Form>
                                        )
                                    }
                                    }
                                </Formik>
                            </Grid>
                        }
                        {showOrganicAlternative ?
                            <Typography className='flex-center'>
                                <div style={{ color: 'darkGreen', fontWeight: 'bolder', fontSize: 'small', textAlign: 'center', marginTop: '20px' }}>
                                    Attention White Labs Copenhagen customers: This production facility makes its own organic version of this strain. Please log in now to view and order this strain from White Labs Copenhagen.
                            </div>
                            </Typography>
                            : null
                        }
                        {showNonOrganicAlternative ?
                            <Typography className='flex-center'>
                                <p style={{ color: 'white', backgroundColor: 'darkGreen', fontWeight: 'bolder', fontSize: 'small', textAlign: 'center', marginTop: '20px' }}>
                                    This organic strain is produced by White Labs Copenhagen. If you are not a customer of White Labs Copenhagen, consider the non-organic version for faster shipping!
                            </p>
                            </Typography>
                            : null
                        }
                    </Grid>
                </div>
            </NavBarUserSearchDrawerLayout>
        );
    }
}


const styles = theme => ({
    info: {
        textAlign: 'center'
    },
    container: {
        marginTop: 40,
        width: '60%',
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        textAlign: 'justify',
        display: 'flex',
        justifyContent: 'center',
        margin: '0 auto',


        padding: theme.spacing(4),
        [theme.breakpoints.down('md')]: {
            width: '100%',
        },
        [theme.breakpoints.up('md')]: {
            marginLeft: 'auto',
            marginRight: 'auto'
        },
        [theme.breakpoints.up('lg')]: {
            marginLeft: 'auto',
            marginRight: 'auto'
        },
        [theme.breakpoints.up('xl')]: {
            marginLeft: 'auto',
            marginRight: 'auto'
        }
    },
    titleMargin: {
        marginTop: '30px',
        marginBottom: '5px',
        textAlign: 'center',
        [theme.breakpoints.down('sm')]: {
            fontSize: 'smaller'
        }
    },
    quantity: {
        width: 50
    },
    hide: {
        display: 'none'
    },
    backbtn: {
        minHeight: '20px !important',
        padding: '4px !important',
        fontSize: '12px !important',
        backgroundColor: '#f28411 !important',
        fontWeight: 'bold !important',
        marginLeft: '37px !important',
        marginTop: '65px !important'

    },
    circleLg: {
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '50%',
        padding: 5,
        width: 47,
        height: 47,
        marginTop: '20px',
        marginLeft: '5px',
        [theme.breakpoints.down('xs')]: {
            width: 40,
            height: 30,
            marginTop: '32px',
            display: 'none',
        }
    },


    circleSm: {
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '50%',
        padding: 5,
        width: 47,
        height: 47,
        marginTop: '12px',
        marginLeft: '5px',
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        }
    },
    imgHieght: {
        height: 20,
        [theme.breakpoints.down('sm')]: {
            height: 20,
        }
    },
    formFields: {
        // display: 'block',
        display: 'flex',
        justifyContent: 'center',
        marginTop: '10px',
        [theme.breakpoints.down('xs')]: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '10px',
            marginLeft: '42px',
        }
    },
    formWrapMargin: {
        marginTop: 5,
        marginBottom: 10
    },
    buttons: {
        display: 'flex',
        justifyContent: 'center'
    },
    addButton: {
        display: 'flex',
        justifyContent: 'center',
        marginLeft: '21px',
        marginTop: '20px',
        marginBottom: '20px'
    },
    button: {
        marginTop: theme.spacing(3),
    },
    description: {
        textAlign: 'justify',
        marginTop: 10

    },
    displayMargin: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 20,
        maxWidth: '70%',
        [theme.breakpoints.down('sm')]: {
            maxWidth: '100%',
        },
    },
    paddingFix: {
        paddingLeft: 'unset',
        marginTop: '5px',
        [theme.breakpoints.between('sm', 'xl')]: {
            paddingLeft: '50px',
        },
        [theme.breakpoints.down('xs')]: {
            paddingLeft: '0px',
            paddingRight: '20px'
        },

    },
    close: { position: 'absolute', right: 0, top: 0 },
    form: {
        width: '100%',
    }
});

const mapStateToProps = state => {
    return {
        inventory: state.inventory,
        user: state.user
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({ ...inventoryActions, ...cartActions }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(compose(withStyles(styles, { withTheme: true })(withInventory(Yeastparam))))
);





