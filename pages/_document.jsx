import React from 'react';
import Document, { Head, Main, NextScript, Html } from 'next/document';
import JssProvider from 'react-jss/lib/JssProvider';
import flush from 'styled-jsx/server';
import getPageContext from 'src/getPageContext';
import { ServerStyleSheet } from 'styled-components';

class MyDocument extends Document {
	render() {
		const { pageContext, styleTags } = this.props;

		return (
			<Html lang='en' dir='ltr'>
				<Head>
					{/* <!-- Google Tag Manager --> */}
					<script dangerouslySetInnerHTML={{
                      __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-W7LD2QF');`,
                    }}>
                    </script>

                    {/* <!-- Contivio Chat --> */}
                    <script src='https://uschat3.contivio.com/uschat2/ContivioChatPlugin.js'></script>

					<meta charSet='utf-8' />
					{styleTags}
					{/* PWA primary color */}
					<meta name='theme-color' content={pageContext.theme.palette.primary.main} />
					<link
						rel='stylesheet'
						href='https://fonts.googleapis.com/css?family=Roboto:300,400,500'
					/>
				</Head>
				<body>
				{/* <!-- Google Tag Manager (noscript) --> */}
                 <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W7LD2QF"
                 height="0" width="0" style={{display:"none",visibility:"hidden"}}></iframe></noscript>
                 {/* <!-- End Google Tag Manager (noscript) --> */}
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

MyDocument.getInitialProps = ctx => {
	// Resolution order
	//
	// On the server:
	// 1. page.getInitialProps
	// 2. document.getInitialProps
	// 3. page.render
	// 4. document.render
	//
	// On the server with error:
	// 2. document.getInitialProps
	// 3. page.render
	// 4. document.render
	//
	// On the client
	// 1. page.getInitialProps
	// 3. page.render

	// Get the context of the page to collected side effects.
	const pageContext = getPageContext();
	const sheet = new ServerStyleSheet()
	const page = ctx.renderPage(Component => props => sheet.collectStyles(
		<JssProvider
			registry={pageContext.sheetsRegistry}
			generateClassName={pageContext.generateClassName}
		>
			<Component pageContext={pageContext} {...props} />
		</JssProvider>
	));
	const styleTags = sheet.getStyleElement();
	return {
		...page,
		pageContext,
		styles: (
			<React.Fragment>
				<style
					id='jss-server-side'
					// eslint-disable-next-line react/no-danger
					dangerouslySetInnerHTML={{ __html: pageContext.sheetsRegistry.toString() }}
				/>
				{flush() || null}
			</React.Fragment>
		),
		styleTags,
	};
};

export default MyDocument;
