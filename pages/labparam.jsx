import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import Router from 'next/router';
import { withRouter } from "next/router";
import withInventory from "hocs/inventory";
import { compose } from "redux";

import NavBarUserSearchDrawerLayout from 'components/NavBar/NavBarUserSearchDrawerLayout';
import FormButton from "components/Form/FormButton";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Formik, Form, Field } from "formik";
import * as Yup from "yup";

import { cartActions } from "appRedux/actions/cartActions";
import { inventoryActions } from "appRedux/actions/inventoryActions";
import ReactHtmlParser from 'react-html-parser'; 
import { DISALLOW_BACK_ORDERS } from "components/Store/constants";
import LoadingIndicator from '../components/UI/LoadingIndicator';
import WLHelper from 'lib/WLHelper';

const customFormValidation = Yup.object().shape({
    quantity: Yup.string().required('Required')
});

class labparam extends Component {

    constructor(props) {
        super(props);
        this.state = {
          quantity: '1',
          errors:{},
        };
        const {
            router: { query }
        } = this.props;
        let { item } = query;
        this.filteredItem = this.props.inventory.items.find(v => v.partNum === item)
    }
    handleErrors = (field, err) => {
        let { errors } = this.state;
        errors[field] = err;
        this.setState({ errors });
    };
    checkQuantity = item => {
        var quantity = parseFloat(item.OrderDetailQty);

        if (isNaN(quantity) || quantity <= 0) {
            this.handleErrors('quantity', 'Please enter a valid value for the quantity');
            return false;
        }

        //  Must be in increments of 1
        else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
            return false;
        }

        return true;
    };


    addToCart = (values) => {

        var quantity = this.state.quantity;
        var item = this.filteredItem;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(item.Name);
        cartItem.MerchandiseID = item.volID[0];
        cartItem.salesCategory = parseInt(item.salesCategory);
        cartItem.type = 3;
        cartItem.details = '';
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.imageUrl = (item.imageUrl ? item.imageUrl : 'static/images/categories/Category-wild.jpg');

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    relItemArray[k].cartItemType = WLHelper.getYMO2TypeForItem(relatedItems[i].related[j], relItemArray[k]);
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                    })
                    .catch(error => {
                        console.log('error', error);

                        // Still add the item to the cart
                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                    });
            }
        }
    }

    changeQuantity = event => {
        this.setState({ quantity: event.target.value });
    };

    handleBack =()=>{
        //set data in localstorage 
        //it indicated store page to loads previous data of page 
        localStorage.setItem("load",true)
        Router.push(`/`);
    }

  

    render() {
        const {classes}=this.props;
        const { errors } = this.state;

        var itemDescription = this.filteredItem ? '<div style="max-height:480px; overflow-y: auto; overflow-x: hidden;">' + this.filteredItem.Description + '</div>' : '';
        itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

        return (
            <NavBarUserSearchDrawerLayout>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <Grid item xs={1} dir='ltr'>
                    <FormButton className='back-param' text='Back' onClick={this.handleBack} />
                </Grid>
                 <div className={classes.container}>
                <div className={classes.dispInline}>
                     <Grid
                        item
                        container
                        xs
                        className={classes.displayMargin}
                        direction={'row'}
                    >
                        <Grid item style={{display:'flex'}}>
                        <Typography variant='h5' className={classes.titleMargin}>
                        {this.filteredItem && this.filteredItem.Name}
                            <Divider variant='middle' />
                            </Typography>
                        </Grid>
                        </Grid>
                        <Grid
                            item
                            container
                            direction={'column'}
                            spacing={6}
                            className={classes.description}
                        >
                            {this.filteredItem && this.filteredItem.Description ? <Grid
                                item
                                container
                                direction={'column'}
                                spacing={6}
                                className={classes.description}
                            >
                                <Grid item>
                                    <div>{ReactHtmlParser(itemDescription)}</div>
                                </Grid>
                            </Grid> : null
                            }
                            {
                                this.filteredItem && this.filteredItem.outofstockmessage &&
                                <Grid item>
                                    <Typography style={{ paddingTop: '10px', fontWeight: 'bold' }}>
                                        {this.filteredItem && this.filteredItem.outofstockmessage}
                                    </Typography>
                                </Grid>
                            }
                        </Grid>
                        {this.filteredItem && this.filteredItem.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                            <Grid item container spacing={6} style={{ marginTop: 5 }} direction={'row'}>
                                <Formik
                                    initialValues={this.state}
                                    validationSchema={customFormValidation}
                                    onSubmit={values => this.addToCart(values)}
                                >
                                    {({ values, handleChange }) => {
                                        return (
                                            <Form className={classes.form}>
                                                {errors.quantity && <div className='error'>{errors.quantity}</div>}
                                                <Grid item xs container spacing={6} direction={'column'} justify='center'>
                                                    <Grid item className='flex-center'>
                                                        <TextField id='quantity' label='Quantity' className='flex-center' className={classes.quantity} value={this.state.quantity} onChange={this.changeQuantity} type='number' />
                                                    </Grid>
                                                    <Grid item container spacing={6} className='flex-center'>
                                                        <Grid item>
                                                            <div className={classes.addButton}>
                                                                <Button
                                                                    type='submit'
                                                                    variant='contained'
                                                                    color='primary'
                                                                    className={classes.button}
                                                                >
                                                                    Add to Cart
                                                        </Button>
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </Form>
                                        );
                                    }}
                                </Formik>
                            </Grid>
                        }
                    </div>
                    </div>
            </NavBarUserSearchDrawerLayout>
        );
    }
}

const styles = theme => ({
    displayMargin:{
        display: 'flex',
        justifyContent:'center',
        marginTop: 10,
        marginBottom: 20,
        maxWidth:'100%',
        [theme.breakpoints.down('sm')]: {
            maxWidth:'100%',
        },
    },
    backbtn:{
        minHeight: '20px !important',
        padding: '4px !important',
        fontSize: '12px !important',
        backgroundColor:'#f28411 !important',
        fontWeight:'bold !important',
        marginLeft: '37px !important',
        marginTop:'65px !important'
    
},
dispInline:{
    display:'inline'
},
container: {
    marginTop: 40,
    width:'60%',
    border: 'solid 1px',
    borderColor: '#CCCCCC',
    textAlign:'center',
    display:'flex',
    justifyContent:'center',
    margin:'0 auto',


    padding: theme.spacing(4),
    [theme.breakpoints.down('sm')]: {
        width:'100%',
    },
    [theme.breakpoints.up('md')]: {
        marginLeft:'auto',
        marginRight: 'auto'
    },
    [theme.breakpoints.up('lg')]: {
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    [theme.breakpoints.up('xl')]: {
        marginLeft: 'auto',
        marginRight: 'auto'
    }
},

     quantity: {
         width: 50,
     },
   
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop:'15px',
        marginBottom:'15px'
    },
    button: {
        // marginTop: theme.spacing(1),
      
    },
    addButton: {
        display: 'flex',
        justifyContent: 'center',
        marginBottom:'20px'
    },
    form: {
        width: '100%',
        display:'flex',
        justifyContent:'center'
    },
    titleMargin: {
        marginTop: '30px',
        marginBottom: '5px',
        textAlign: 'center',
        [theme.breakpoints.down('sm')]: {
            fontSize: 'smaller'
        }
    }
});



const mapStateToProps = state => {
    return {
        user: state.user,
        inventory: state.inventory
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({ ...inventoryActions, ...cartActions }, dispatch);


export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(compose(withStyles(styles, { withTheme: true })(withInventory(labparam))))
);

