import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { userActions } from "appRedux/actions/userActions";
import { messageActions } from "appRedux/actions/messageActions";
import Router from "next/router";
import NavBarLayout from "components/NavBar/NavBarLayout";
import NavBarUserSearchDrawerLayout from "components/NavBar/NavBarUserSearchDrawerLayout";
import PageContainer from "components/UI/PageContainer";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import StarRatings from "react-star-ratings";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { Formik, Form, Field } from "formik";
import withStyles from "@material-ui/core/styles/withStyles";
import * as Yup from "yup";
import ReactGA from "react-ga";

class Feedback extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false
    };
    ReactGA.initialize("UA-40641680-2");
    ReactGA.pageview("/feedback");
  }

    submitScreen = (values, { resetForm }) => {
        let comment = values.comment;
        let appRating = values.appRating;
        let orderProcessRating = values.orderProcessRating;
        let contactMe = values.userDetail;
        let userContact = values.userContact;

        let request = {};
        request.isQuestionnaireResponse = true;
        request.source = 'YMO2 Feedback';
        if (this.props.user) request.id = this.props.user.id;

        request.questions = [];
        request.questions.push({ question: 'How was your experience with Yeastman 2.0?', answer: appRating + ' out of 5' });
        request.questions.push({ question: 'How was the overall ordering process?', answer: orderProcessRating + ' out of 5' });
        request.questions.push({ question: 'Do you have any comments?', answer: comment });
        request.questions.push({ question: 'Would you like to receive a response from White Labs?', answer: (contactMe ? 'Yes' : 'No') });
        request.questions.push({ question: 'Respondent email address', answer: userContact });

        axios.post('/record-survey-responses', { request })
            .then(({ data: { message, error } }) => {
                if (error) throw error;
                this.props.showBanner({
                    content: { message: (message ? message : "Feedback was submitted successfully") },
                    variant: "success"
                });
                resetForm();
                this.setState({ show: false });
                //Router.push("/");
            })
            .catch(error => {
                this.props.showBanner({
                    content: { message: error },
                    variant: "error"
                });
            });
        /*
        // Previous method of feedback: Google sheets
        axios
          .get(
            `https://script.google.com/macros/s/AKfycbxRw09XuYwQjpAGJTopERbXcBvmFNCNJlZJr35HYWwRr6WGHsc/exec?comment=${comment}&appRating=${appRating}&orderProcessRating=${orderProcessRating}&userDetail=${userDetail}&userContact=${userContact}`
          )
          .then(response => {
            this.props.showBanner({
              content: { message: "Feedback was submitted successfully" },
              variant: "success"
            });
            resetForm();
            this.setState({ show: false });
            Router.push("/");        
          })
          .catch(error => {
            this.props.showBanner({
              content: { message: error },
              variant: "error"
            });
          });
          */
    };

  handleUserContact = () => {
    this.setState(prevState => ({
      show: !prevState.show
    }));
  };

  render() {
    const customFormValidation = Yup.object().shape({
      comment: Yup.string().required("This field is mandatory.")
    });
    const { classes } = this.props;

    return (
      <NavBarUserSearchDrawerLayout>
        <div className={classes.container}>
          <PageContainer heading="FEEDBACK">
            <Grid container className="flex-center">
              <Formik
                initialValues={{
                  comment: "",
                  appRating: 0,
                  orderProcessRating: 0,
                  userDetail: false,
                  userContact: ""
                }}
                validationSchema={customFormValidation}
                enableReinitialize
                onSubmit={(values, { resetForm }) => {
                  this.submitScreen(values, { resetForm });
                }}
              >
                {({
                  errors,
                  touched,
                  isValidating,
                  handleChange,
                  values,
                  setFieldValue
                }) => {
                  return (
                    <Form>
                      <Grid item xs={12}>
                        <Typography
                          className={classes.typoText}
                          component="h2"
                          variant="h6"
                          gutterBottom
                        >
                          How was your experience with Yeastman 2.0?
                        </Typography>
                        <div className="flex-center">
                          <StarRatings
                            rating={values.appRating}
                            starRatedColor="#FF9933"
                            starHoverColor="#f28531"
                            changeRating={value =>
                              setFieldValue("appRating", value)
                            }
                            numberOfStars={5}
                            name="rating"
                          />
                        </div>
                        <Typography
                          className={classes.typoText}
                          component="h2"
                          variant="h6"
                          gutterBottom
                        >
                          How was the overall ordering process?
                        </Typography>
                        <div className="flex-center">
                          <StarRatings
                            rating={values.orderProcessRating}
                            starRatedColor="#FF9933"
                            starHoverColor="#f28531"
                            changeRating={value =>
                              setFieldValue("orderProcessRating", value)
                            }
                            numberOfStars={5}
                            name="rating"
                          />
                        </div>
                        <FormControl margin="normal" required fullWidth>
                          <textarea
                            className={classes.textarea}
                            placeholder="Your feedback is valuable to us :)"
                            name="comment"
                            label="Your Feedback is Valuable to us"
                            variant="outlined"
                            margin="normal"
                            // fullWidth
                            onChange={handleChange}
                            value={values.comment}
                            maxLength="300"
                          />
                        </FormControl>
                        {errors.comment && touched.comment && (
                          <div style={{ color: "red" }}>{errors.comment}</div>
                        )}
                        <FormControlLabel
                          control={
                            <Checkbox
                              name="userDetail"
                              checked={values.userDetail}
                              color="primary"
                              onChange={handleChange}
                              onClick={this.handleUserContact}
                            />
                          }
                          label="I would like to receive a response from White Labs."
                        />
                        <TextField
                          className={
                            this.state.show ? "show-item" : "hide-item"
                          }
                          name="userContact"
                          type="email"
                          label="Your Email Address"
                          variant="outlined"
                          margin="normal"
                          fullWidth
                          onChange={handleChange}
                          value={values.userContact}
                          maxLength="300"
                        />
                      </Grid>

                      <Grid
                        style={{ marginTop: 10 }}
                        container
                        justify="center"
                      >
                        <Grid item>
                          <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                          >
                            Send Feedback
                          </Button>
                        </Grid>
                      </Grid>
                    </Form>
                  );
                }}
              </Formik>
            </Grid>
          </PageContainer>
        </div>
      </NavBarUserSearchDrawerLayout>
    );
  }
}

const styles = theme => ({
  starDimension: {
    fontSize: "20px"
  },
  container: {
    width: "80%",
    marginLeft: "7%",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "11%"
    }
  },
  typoText: {
    display: "flex",
    justifyContent: "center",
    textAlign: "center",
    fontSize: "1.5em",
    marginTop: "20px",
    marginBottom: "20px",
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px"
    }
  },
  textarea: {
    width: "100%",
    height: "150px",
    padding: "12px 20px",
    boxSizing: "border-box",
    border: "2px solid #ccc",
    borderRadius: "4px",
    backgroundColor: "#f8f8f8",
    resize: "none"
  },
  userContDisp: {
    display: "flex"
  },
  userContNone: {
    display: "none"
  }
});

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...userActions, ...messageActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles, { withTheme: true })(Feedback));
