import 'babel-polyfill';
import React, { Component } from 'react';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import _get from 'lodash/get';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import NavBarUserSearchDrawerLayout from 'components/NavBar/NavBarUserSearchDrawerLayout';
import SearchBar from 'components/NavBar/SearchBar';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import { YeastElements } from 'components/Store/Yeast/YeastCard';
import MainMenu from 'components/Store/Menu/MainMenu';
import SubCat from 'components/Store/Menu/SubCat';
import SalesLib from 'lib/SalesLib';

import YeastDialog from 'components/Store/Yeast/YeastDialog';
import { EnzymesNutrientsCard } from 'components/Store/EnzymesNutrients';
import EnzymesNutrientsDialog from 'components/Store/EnzymesNutrients/EnzymesNutrientsDialog';
import { ServicesCard } from 'components/Store/Services';
import ServicesDialog from 'components/Store/Services/ServicesDialog';
import { LabSuppliesCard } from 'components/Store/LabSupplies';
import LabSuppliesDialog from 'components/Store/LabSupplies/LabSuppliesDialog';
import { EducationCard } from 'components/Store/Education';
import EducationDialog from 'components/Store/Education/EducationDialog';
import { GiftShopCard } from 'components/Store/GiftShop';
import GiftShopDialog from 'components/Store/GiftShop/GiftShopDialog';
import { AlcoholCard } from 'components/Store/Alcohol';
import AlcoholDialog from 'components/Store/Alcohol/AlcoholDialog';
import { HomebrewCard } from 'components/Store/Homebrew';
import { userActions } from 'appRedux/actions/userActions';
import { messageActions } from 'appRedux/actions/messageActions';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import withInventory from 'hocs/inventory';
import { filterItems } from 'lib/InventoryUtils';
import isEqual from 'lodash/isEqual';
import ReactGA from 'react-ga';
import RetailVolumePricingDialog from 'components/Store/Menu/RetailVolumePricingDialog';
import { YeastCard } from '../components/Store/Yeast';
import { REMOVE_ITEM } from '../components/Store/constants';
import { cartActions } from 'appRedux/actions/cartActions';
import {
    cartSelectors,
    inventorySelectors,
    loadingSelectors,
    messagesSelectors,
    userSelectors,
} from 'appRedux/selectors';
import ScrollableAnchor from 'react-scrollable-anchor';
import FormCheckbox from 'components/Form/FormCheckbox';
import Button from '@material-ui/core/Button';

class Vault extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openDialog: false,
            searchText: '',
            selectedMainCategory: null,
            selectedSubCategory: null,
            itemsToShow: [],
            isHomebrew: false,
            isOrganicOnly: false,
            tab: 0,
            isVault: false,
            openHBVolumePricing: false,
            showMoreVault: false,
            userStatesIsLegalDrinkingAge: false,
            showVaultFAQs: false
        };

        ReactGA.initialize('UA-40641680-2');
    }

    UNSAFE_componentWillMount() {
        const isIE = /*@cc_on!@*/false || !!document.documentMode;
        if (isIE) {
            setTimeout("location.href='https://classic.yeastman.com'", 10000);
        } else {
            const orderComplete = sessionStorage.getItem('orderComplete');
            if (orderComplete == 'yes') {
                // Hacky fix to force navigation to work after placing an order
                // Without this, you can't navigate after placing an order, for reasons unknown.
                sessionStorage.setItem('orderComplete', 'no');
                window.location.reload(true);
            } else {
                const {
                    inventory: { items, pageData }
                } = this.props;

                try {
                    let userInfo = JSON.parse(localStorage.getItem('userInfo'));
                    if (_get(userInfo, 'id')) {
                        this.props.setUserInfo({ userInfo });
                    }
                } catch (error) {
                    console.log(error);
                    //this.props.setUserInfo({ userInfo: null });
                }
                // check previous showing data is required or not
                //when we click back button on page it will load previous date
                if (localStorage.getItem("load")) {
                    const data = _.get(pageData, 'data');
                    this.setState({ ...data }, () => {
                        localStorage.setItem("load", "")

                    });
                }
            }

            this.toggleVault(true);
        }
    }

    // To force the store to reload when user clicks link in header while in the store
    // See also NavBarUserSearchDrawerLayout file
    // TODO: Find a better way
    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.router && nextProps.router.route === '/') {
            if (!isEqual(this.props, nextProps)) {
                //this.toggleHomebrew(false);
                //this.toggleVault(false);
            }
        }
    }

    toggleIsLegalDrinkingAge = () => {
        var isLegalDrinkingAge = this.state.userStatesIsLegalDrinkingAge;
        this.setState({ userStatesIsLegalDrinkingAge: !isLegalDrinkingAge });
    }

    handleAgeConfirmation = () => {
        try {
            var isLegalDrinkingAge = this.state.userStatesIsLegalDrinkingAge;
            let userInfo = JSON.parse(localStorage.getItem('userInfo'));
            if (userInfo) {
                userInfo.isLegalDrinkingAge = isLegalDrinkingAge;
                this.props.setUserInfo({ userInfo });
            } else if (isLegalDrinkingAge) {
                userInfo = {};
                userInfo.shipping = {};
                userInfo.isLegalDrinkingAge = isLegalDrinkingAge;
                this.props.setUserInfo({ userInfo });
            }

            if (isLegalDrinkingAge) {
                this.setState({ userStatesIsLegalDrinkingAge: true });
            } else {
                this.categoryBack();
            }
        } catch (error) {
            console.log(error);
            //this.props.setUserInfo({ userInfo: null });
        }
    }

    handleTab = (event, value) => {
        this.setState({ tab: value });
        if (value == 0) {
            this.toggleHomebrew(false);
            this.toggleVault(true);
        } else if (value == 1) {
            this.toggleVault(false);
            this.toggleHomebrew(true);
        } else {
            this.toggleHomebrew(false);
            this.toggleVault(false);
        }
    };

    handleStoresFromVault = () => {
        this.handleTab(null, 1);
    }

    handleProFromVault = () => {
        this.handleTab(null, 2);
    }

    handleClickItem = item => {
        this.props.dialogClose(true);
        this.setState({ item: item }, () => {
        });
    };

    handleClickHBVolumePricing = () => {
        this.props.dialogClose(false);
        this.setState({ openHBVolumePricing: this.props.isDialogClose });
    };

    handleLeaveHBVolumePricing = () => {
        this.setState({ openHBVolumePricing: false });
    };

    getRetailPricingDialogContent = () => {
        return <RetailVolumePricingDialog closeDialog={this.handleLeaveHBVolumePricing} />;
    };

    handleLeaveItem = () => {
        this.props.dialogClose(false);
        this.setState({ openDialog: false, item: null, openHBVolumePricing: false });
        this.props.setPageData();
    };

    handleLeaveHBItem = () => {
        this.props.dialogClose(false);
        this.setState({ item: null, openHBVolumePricing: false });
    };

    handleClickVaultShowMore = () => {
        this.setState({ showMoreVault: !this.state.showMoreVault });
    };

    handleClickVaultShowFAQs = () => {
        this.setState({ showVaultFAQs: !this.state.showVaultFAQs });
    };

    toggleVault = isVault => {
        if (isVault != this.state.isVault) {
            const {
                inventory: { items },
                user
            } = this.props;

            const categoryFilter = (isVault ? null : _get(this.state.selectedSubCategory, 'value')); // Not all Vault preorder items are actually in the Vault category
            this.changeSubCategory(categoryFilter);
            var itemsToShow = filterItems(items, categoryFilter, null, user, false, false, isVault);
            if (itemsToShow) {
                itemsToShow.sort((a, b) => (a.preorderedTotal < b.preorderedTotal) ? 1 : (a.preorderedTotal === b.preorderedTotal) ? ((a.Name > b.Name) ? 1 : -1) : -1)
            }
            this.setState({ isVault: isVault, isHomebrew: false, isOrganicOnly: false, itemsToShow });
        }
    };

    toggleHomebrew = isHomebrew => {
        if (isHomebrew != this.state.isHomebrew) {
            const {
                inventory: { items },
                user
            } = this.props;
            const categoryFilter = isHomebrew ? null : _get(this.state.selectedSubCategory, 'value');

            var itemsToShow = filterItems(items, categoryFilter, null, user, isHomebrew);
            this.setState({ isHomebrew, isOrganicOnly: false, itemsToShow });
        }
    };

    searchItem = searchText => {
        // debugger;
        const {
            inventory: { items },
            user,
        } = this.props;
        const { isHomebrew, isVault } = this.state;

        const itemsToShow = filterItems(items, null, searchText, user, isHomebrew, false, isVault);
        this.setState({ searchText, itemsToShow, isOrganicOnly: false });
    };


    changeMainCategory = selectedMainCategory => {
        const {
            inventory: { items },
            user,
        } = this.props;
        const { isHomebrew } = this.state;

        const value = _.get(selectedMainCategory, 'value');
        var subCategories = _.get(selectedMainCategory, 'subCategories');

        var selectedSubCategory = null;

        if (!subCategories) {
            selectedSubCategory = selectedMainCategory;
        } else if (user && user.subsidiary && user.subsidiary != 2) {
            subCategories = subCategories.filter(subCategory => subCategory.value != 998);
        }


        const sortByDate = (value == 14); // Educational classes
        var itemsToShow = filterItems(items, value, null, user, isHomebrew, sortByDate);
        this.setState({ selectedMainCategory, selectedSubCategory, itemsToShow, isOrganicOnly: false });
    };

    changeSubCategory = selectedSubCategory => {
        const {
            inventory: { items },
            user
        } = this.props;
        const { isHomebrew } = this.state;
        const value = _.get(selectedSubCategory, 'value');

        if (value == 998) {
            this.setState({ isOrganicOnly: true });
        } else {
            this.setState({ isOrganicOnly: false });
        }

        var itemsToShow = filterItems(items, value, null, user, isHomebrew);

        this.setState({ selectedSubCategory, itemsToShow });
    };

    getCard = (item, i) => {
        //Only show Vault preorder items in the vault view or if the user is searching and entered matching text
        if (item && item.isVaultPreorderItem && !this.state.isVault && !this.state.searchText) {
            return null;
        } else if (this.state.isHomebrew && item.outofstockbehavior !== REMOVE_ITEM) {
            // Yeast (core + vault)
            if (SalesLib.SALESCATEGORY[0].includes(parseInt(item.salesCategory)) || item.salesCategory == 32) {
                return <HomebrewCard key={i} item={item} />;
            }
            // Enzymes & Nutrients
            else if (SalesLib.SALESCATEGORY[8].includes(parseInt(item.salesCategory))) {
                return <EnzymesNutrientsCard key={i} item={item} onClick={this.handleClickItem} />;
            }
        } else if (this.state.isVault) {
            //return <VaultCard key={i} item={item} onClick={this.handleClickItem} />;
            return <YeastCard key={i} item={item} onClick={this.handleClickItem} />;
            // return <VaultHomebrewCard key={i} item={item} />;
        } else if (item && item.outofstockbehavior !== REMOVE_ITEM) {
            // Yeast (core + vault)
            if (SalesLib.SALESCATEGORY[0].includes(parseInt(item.salesCategory)) || item.salesCategory == 32) {
                return <YeastCard key={i} item={item} onClick={this.handleClickItem} />;
            }

            // Enzymes & Nutrients

            else if (SalesLib.SALESCATEGORY[8].includes(parseInt(item.salesCategory))) {
                return <EnzymesNutrientsCard key={i} item={item} onClick={this.handleClickItem} />;
            }

            // Services

            else if (SalesLib.SALESCATEGORY[11].includes(parseInt(item.salesCategory))) {
                return <ServicesCard key={i} item={item} onClick={this.handleClickItem} />;
            }

            // Lab Supplies
            else if (SalesLib.SALESCATEGORY[13].includes(parseInt(item.salesCategory))) {
                return <LabSuppliesCard key={i} item={item} onClick={this.handleClickItem} />;
            }

            // Education
            else if (SalesLib.SALESCATEGORY[14].includes(parseInt(item.salesCategory))) {
                return <EducationCard key={i} item={item} onClick={this.handleClickItem} />;
            }

            // Gift Shop
            else if (SalesLib.SALESCATEGORY[15].includes(parseInt(item.salesCategory))) {
                return <GiftShopCard key={i} item={item} onClick={this.handleClickItem} />;
            }

            // Alcohol
            else if (SalesLib.SALESCATEGORY[17].includes(parseInt(item.salesCategory))) {
                if (this.props.user.isLegalDrinkingAge) {
                    return <AlcoholCard key={i} item={item} onClick={this.handleClickItem} />;
                } else {
                    return null;
                }
            }
        }
        return null;
    };

    getDialogContent = item => {
        if (item) {
            // Yeast
            if (SalesLib.SALESCATEGORY[0].includes(parseInt(item.salesCategory))) {
                if (item.isVaultPreorderItem) {
                    //return <VaultDialog stateData={this.state} item={item}
                    //    closeDialog={this.state.isHomebrew ? this.handleLeaveHBItem : this.handleLeaveItem} />;
                    //return <VaultDialog stateData={this.state} item={item} closeDialog={this.handleLeaveItem} />;
                    return <YeastDialog stateData={this.state} item={item} closeDialog={this.handleLeaveItem} />;
                } else {
                    return <YeastDialog stateData={this.state} item={item} closeDialog={this.handleLeaveItem} />;
                }
            }

            // Enzymes & Nutrients
            else if (SalesLib.SALESCATEGORY[8].includes(parseInt(item.salesCategory))) {
                return <EnzymesNutrientsDialog stateData={this.state} item={item}
                    closeDialog={this.state.isHomebrew ? this.handleLeaveHBItem : this.handleLeaveItem} />;
            }

            // Services
            else if (SalesLib.SALESCATEGORY[11].includes(parseInt(item.salesCategory))) {
                return <ServicesDialog stateData={this.state} item={item} closeDialog={this.handleLeaveItem} />;
            }

            // Lab Supplies
            else if (SalesLib.SALESCATEGORY[13].includes(parseInt(item.salesCategory))) {
                return <LabSuppliesDialog stateData={this.state} item={item} closeDialog={this.handleLeaveItem} />;
            }

            // Education
            else if (SalesLib.SALESCATEGORY[14].includes(parseInt(item.salesCategory))) {
                return <EducationDialog stateData={this.state} item={item} closeDialog={this.handleLeaveItem} />;
            }

            // Gift Shop
            else if (SalesLib.SALESCATEGORY[15].includes(parseInt(item.salesCategory))) {
                return <GiftShopDialog stateData={this.state} item={item} closeDialog={this.handleLeaveItem} />;
            }

            // Alcohol
            else if (SalesLib.SALESCATEGORY[17].includes(parseInt(item.salesCategory))) {
                return <AlcoholDialog stateData={this.state} item={item} closeDialog={this.handleLeaveItem} />;
            }
        }

        return <React.Fragment />;
    };

    categoryBack() {
        this.setState({ searchText: null, isHomebrew: false, isVault: false, isOrganicOnly: false });
        this.handleTab(null, 2);

        if (!this.state.selectedSubCategory || this.state.selectedSubCategory == this.state.selectedMainCategory) {
            this.changeMainCategory(null);
        } else {
            this.changeSubCategory(null);
        }
    }

    checkForData = selectedMainCategory => {
        const {
            inventory: { items },
            user
        } = this.props;
        const { isHomebrew } = this.state;

        const value = _.get(selectedMainCategory, 'value');
        var subCategories = _.get(selectedMainCategory, 'subCategories');
        if (subCategories && user && user.subsidiary && user.subsidiary != 2) {
            subCategories = subCategories.filter(subCategory => subCategory.value != 998);
        }

        const sortByDate = (value == 14); // Educational classes
        var itemsToShow = filterItems(items, value, null, user, isHomebrew, sortByDate);

        if ((itemsToShow && itemsToShow.length) || (subCategories && subCategories.length)) {
            return true;
        }

        return false;
    }

    render() {
        const dev = process.env.NODE_ENV !== "production";
        let { classes, inventory: { welcomeMessage }, user: { isOnCreditHold } } = this.props;
        if (this.props.inventory.items && this.props.inventory.items.length) {
            var searchItem = this.props.inventory.items.filter((v) => {
                return v.pack;
            })
        }

        let { searchTextmobile } = this.state;
        const { selectedMainCategory, selectedSubCategory, searchText, isHomebrew, itemsToShow, isVault, isOrganicOnly } = this.state;

        var sectionTitle, sectionColor, pageContent, sectionSubTitle;
        var isGrouped = (itemsToShow.length > 0 && (itemsToShow[0].group || isHomebrew || isOrganicOnly));

        var showAgeVerificationPrompt = false;

        var jumpLinks = [];
        if (isGrouped) {
            var k = 0;
            for (var i = 0; i < itemsToShow.length; i++) {
                var item = itemsToShow[i];
                var groupCard = this.getCard(item, i);

                // Don't include groups/jump links with nothing in them
                if (groupCard) {
                    if (item.group) {
                        if (jumpLinks[item.group] === undefined) {
                            jumpLinks[item.group] = 'ig' + k;
                            k++;
                        }
                    } else if (isHomebrew || isOrganicOnly) {
                        var group = (YeastElements[parseInt(item.salesCategory)]
                            ? YeastElements[parseInt(item.salesCategory)].hbTitle
                            : (item.salesCategory == 29 ? 'Enzymes' : 'Nutrients')
                        );

                        if (jumpLinks[group] === undefined) {
                            jumpLinks[group] = 'ig' + k;
                            k++;
                        }
                    }
                }
            }
        }

        if (selectedSubCategory || searchText || isHomebrew || isVault || isGrouped || isOrganicOnly) {
            if (!isHomebrew && !isVault && selectedSubCategory) {
                sectionTitle = selectedSubCategory.label;
                sectionColor = selectedSubCategory.color;
            }

            // This is used as a throttle on how fast we check for green locks in the Yeast
            // cards when viewing vault items.
            localStorage.setItem('VaultAvailRequests', '0');

            let cardsNode = [];
            var lastGroup = '';

            itemsToShow.map((item, i) => {
                var itemCard = this.getCard(item, i);
                if (itemCard) {
                    var group = item.group;
                    if (!group && (isHomebrew || isOrganicOnly)) {
                        group = (YeastElements[parseInt(item.salesCategory)]
                            ? YeastElements[parseInt(item.salesCategory)].hbTitle
                            : (item.salesCategory == 29 ? 'Enzymes' : 'Nutrients')
                        );
                    }

                    if (isGrouped && lastGroup != group) {
                        // Create jump links
                        var jumpLinkBar = '';
                        var myLink = '';
                        var keys = Object.keys(jumpLinks);

                        for (var i = 0; i < keys.length; i++) {
                            var key = keys[i];
                            if (key == group) {
                                myLink = jumpLinks[key];
                            } else {
                                jumpLinkBar += '<div class="' + (i < keys.length - 1 ? 'jumpLink' : 'jumpLinkLast') + '">'
                                    + '<a href="#' + jumpLinks[key]
                                    + '" onClick="{setTimeout(function () { if (document.documentElement.scrollTop + window.innerHeight < document.documentElement.offsetHeight) window.scrollBy(0, -125); }, 500);}">'
                                    + key
                                    + '</a></div>';
                            }
                        }

                        if (group) {
                            cardsNode.push(
                                <ScrollableAnchor id={myLink}>
                                    <div key={'g' + myLink + i} className={classes.groupTitleDiv}>
                                        <span className={classes.sectionTitleSpan} style={{ color: sectionColor }} />
                                        <span className={classes.titText}>{group.toUpperCase()}</span>
                                        <span className={classes.sectionTitleSpan} style={{ color: sectionColor }} />
                                    </div>
                                </ScrollableAnchor>
                            );

                            if (jumpLinkBar) {
                                cardsNode.push(
                                    <div key={'gg' + myLink + i} className={classes.jumpLinksDiv}>
                                        <span style={{ margin: 'auto' }} dangerouslySetInnerHTML={{ __html: jumpLinkBar }} />
                                    </div>
                                );
                            }
                        }
                    }

                    if (group) lastGroup = group;
                    cardsNode.push(itemCard);
                }
            });

            pageContent = (
                <Grid className={classes.store} container spacing={6}>
                    {cardsNode}
                </Grid>
            );
        } else if (searchTextmobile) {
            let cardsNode = [];
            itemsToShow.map((item, i) => {
                cardsNode.push(this.getCard(item, i));
            });

            pageContent = (
                <Grid className={classes.store} container>
                    {cardsNode}
                </Grid>
            );

            if (selectedSubCategory) {
                sectionTitle = selectedSubCategory.label;
                sectionColor = selectedSubCategory.color;
            }
        } else if (selectedMainCategory) {
            pageContent = <SubCat mainCategory={selectedMainCategory} changeSubCategory={this.changeSubCategory} user={this.props.user} />;
            sectionTitle = selectedMainCategory.label;
            sectionColor = selectedMainCategory.color;
        } else {
            //sectionTitle = 'Yeastman 2.0 Store';
            pageContent = <MainMenu changeMainCategory={this.changeMainCategory} checkForData={this.checkForData} />;
        }

        if (sectionTitle) {
            ReactGA.pageview('/store-' + sectionTitle.replace(/\s/g, '-'));
        } else {
            ReactGA.pageview('/store');
        }

        if (itemsToShow.length > 0) {
            if (SalesLib.SALESCATEGORY[11].includes(parseInt(itemsToShow[0].salesCategory))) {
                sectionSubTitle =
                    '<div style="max-width:70%;margin:auto;">White Labs offers third party analytical testing that is independent of the White Labs internal yeast production laboratory. '
                    + 'Analytical lab services can be ordered below. After placing your order, please print the Analytical Testing Form and include '
                    + 'it in the shipment with your sample(s). Our shipping address is:'
                    + '<br /><br />'
                    + 'White Labs<br />'
                    + 'Attn: Analytical Lab<br />'
                    + '9557 Candida Street<br />'
                    + 'San Diego, CA 92126<br /><br />'
                    + 'Questions? Contact us at <a href="mailto:analyticallab@whitelabs.com">analyticallab@whitelabs.com</a> or (858) 693-3441 ext. 7387</div>';
            } else if (SalesLib.SALESCATEGORY[17].includes(parseInt(itemsToShow[0].salesCategory))) {
                if (this.props.user.isLegalDrinkingAge) {
                    sectionSubTitle = '<div style="max-width:70%;margin:auto;font-size: larger;">'
                        + 'Shipping direct to your doorstep!<br /><br />'
                        + 'CA and NC shipping <strong>ONLY</strong> because of legal restrictions. '
                        + 'Must be 21+ to purchase; you may be asked to present a valid ID to the delivery driver and sign for alcohol once it arrives at your house. '
                        + 'If you are purchasing beer only, please select FedEx Ground (Cans Only) shipping, regardless of how many cans, 4-packs, or cases you select. '
                        + 'For those adding cans to their regular orders, please use your normal shipping method.<br /><br />'
                        + 'Orders placed Sunday-Tuesday will ship the next day and will arrive in 1-2 days if shipping via FedEx Ground (Cans Only). '
                        + 'Orders placed Wednesday-Saturday will ship on Monday and will be delivered in 1-2 days.<br /><br />'
                        + 'All cans are packed cold and shipped with our advanced thermal packaging and ice to get it to your home in the best condition.<br /><br />'
                        + 'Please email <a href="mailto:info@whitelabs.com">info@whitelabs.com</a> or call 858-527-7391 if you have any questions.'
                        + '</div>';
                } else {
                    showAgeVerificationPrompt = true;
                }
            } else if (selectedSubCategory) {
                if (selectedSubCategory.value == 998) {
                    sectionSubTitle = '<div style="max-width:70%;margin:auto;font-size: larger;">'
                        + 'For European customers ordering direct from Copenhagen, please sign in to see organic strains under our normal yeast listings. '
                        + 'Please be aware there is an up to 24-hour gap in syncing organic orders from the USA. If a change in the ship date is required, '
                        + 'which would only happen on rare occasions, you will be notified.'
                }
            }
        }

        // Internet Explorer 6-11
        const isIE = /*@cc_on!@*/false || !!document.documentMode;

        return (
            isIE ?
                (
                    <Typography justify='center'>
                        <br />
                        YMO 2.0 is not compatible with Internet Explorer. Compatible browsers include:
                        <br />
                        <ul>
                            <li>Google Chrome</li>
                            <li>Microsoft Edge (Windows 10 Only)</li>
                            <li>Mozilla Firefox</li>
                            <li>Safari</li>
                            <li>Vivaldi</li>
                        </ul>
                                    To place an order using Internet Explorer, please visit <a href='https://classic.yeastman.com'>Yeastman
                        Classic</a>, or wait to be automatically redirected.
                    </Typography>
                ) : (
                    <NavBarUserSearchDrawerLayout inputVal={this.state.searchText}
                        handleSearch={searchData => this.searchItem(searchData)}>
                        <SearchBar
                            class={'searchmobile'}
                            searchText={this.state.searchText}
                            handleSearch={searchData => this.searchItem(searchData)}
                        />
                        <React.Fragment>
                            <div className={classes.sectionTitleDiv}>
                                <span
                                    className={classes.sectionTitleSpan}
                                    style={{ width: '36%', color: '#a5a5a5' }}
                                />
                                <span className={classes.titText} style={{ width: '24%' }}>
                                    <img
                                        className={classes.image}
                                        src='/static/images/the_vault.png'
                                    />
                                </span>
                                <span
                                    className={classes.sectionTitleSpan}
                                    style={{ width: '36%', color: '#a5a5a5' }}
                                />
                            </div>
                            <div style={{ margin: 'auto', textAlign: 'center' }}>
                                <Typography onClick={this.handleStoresFromVault} style={{ cursor: 'pointer', color: 'blue' }}>
                                    Looking for homebrew strains that are released <img style={{ maxHeight: '15px' }} src='static/images/icons/Green_Lock_v01.svg' /> from the Vault&reg;? Click here.
                                        </Typography>
                                <Typography onClick={this.handleProFromVault} style={{ cursor: 'pointer', color: 'blue' }}>
                                    Looking for pro sizes including those that have been released <img style={{ maxHeight: '15px' }} src='static/images/icons/Green_Lock_v01.svg' /> from the Vault&reg;? Click here.
                                        </Typography>
                            </div>
                            <div className={classes.vaultInfo}>
                                <Typography variant="h5">ABOUT THE VAULT&reg;</Typography>
                                <Typography className={classes.vaultInfoParagraph}>
                                    The Vault&reg; is White Labs collection of unique and specialty yeast strains. They are available on this page through
                                    preorders, as seasonal strains (under the Stores tab) and direct professional brewery orders (see Yeast/Vault&reg;).
                                            Homebrewers are charged <strong>$6.43</strong> per item plus a flat shipping fee of <strong>$8.20</strong> per strain
                                            (2 different strains, for instance, would have two separate ship dates and shipping charges). Stores can combine Core
                                            and Vault&reg; preorder strains in one order for volume discounts; normal rates and shipping options apply.
                                        </Typography>
                                <div>
                                    <span style={{ cursor: 'pointer', color: 'blue', marginTop: '10px' }} onClick={this.handleClickVaultShowMore}>
                                        <br />
                                        {this.state.showMoreVault ? 'Show less ...' : 'Show more ...'}
                                    </span>
                                    {this.state.showMoreVault &&
                                        <div>
                                            <Typography variant="h6" style={{ marginTop: '10px' }}>
                                                We have revamped our Vault&reg; preorder program to make it easier for you to access these specialty strains!
                                                    </Typography>
                                            <Typography className={classes.vaultInfoParagraph}>
                                                Preorder the strains you're interested in today - you won't be charged until your order ships!
                                                Once our preorder reaches 150 units, we'll notify you that the strain is going into production and when
                                                you can expect it in your mailbox. We've improved our shipping and handling and are also offering additional
                                                services to ensure your order ships when and how you want it.
                                                        <br /><br />
                                                        Want it faster? Help us spread the word on social media!
                                                    </Typography>
                                            <Typography variant="h6" style={{ marginTop: '10px' }}>Support your local homebrew retailer</Typography>
                                            <Typography className={classes.vaultInfoParagraph}>
                                                In the past, the Vault&reg; was open to preorders and shipped direct to homebrewers. We have opened that up to
                                                allow our valued retail partners access to preorder strains in addition to the Vault&reg; strains in inventory
                                                as well as our quarterly seasonal Vault&reg; releases. Retailers can now add vault strains to their regular
                                                homebrew orders. Core strains will ship as normal; vault strains will go into production once the preorder
                                                minimum is reached. Retailers will be notified when the strains will be expected in their stores to help
                                                spread the excitement to their customers!
                                                    </Typography>
                                            <Typography className={classes.vaultInfoParagraph}>
                                                The Vault&reg; is a registered trademark of White Labs.
                                                    </Typography>
                                        </div>
                                    }
                                    {this.state.showMoreVault &&
                                        <span style={{ cursor: 'pointer', color: 'blue', marginTop: '10px' }} onClick={this.handleClickVaultShowMore}><br />Show less ...</span>
                                    }
                                </div>
                                <div>
                                    <span style={{ cursor: 'pointer', color: 'blue', marginTop: '10px' }} onClick={this.handleClickVaultShowFAQs}>
                                        <br />
                                        {this.state.showVaultFAQs ? 'Hide FAQs ...' : 'Show FAQs ...'}
                                    </span>
                                    {this.state.showVaultFAQs &&
                                        <div>
                                            <Typography variant="h6" style={{ marginTop: '10px' }}>FAQs:</Typography>
                                            <Typography className={classes.vaultInfoParagraph}>
                                                &bull; <strong>How do I access my account or create a new one?</strong>
                                                <br />
                                                    If you have created an account in Yeastman or the app before, simply login or request a new password.
                                                    If you ordered Vault previously on whitelabs.com, you will need to create a new account in Yeastman.
                                                    For those who have called in orders, you already have an account on Yeastman. You just need
                                                        to <a href='https://classic.yeastman.com/Login/NewUserSetup.aspx?Mode=Activate'>confirm it</a>.
                                                        <br /><br />
                                                    &bull; <strong>How long will it take to get my order?</strong>
                                                <br />
                                                    While our preorder minimum is 150 units to trigger a production run, we're often able to package strains
                                                    before we reach that minimum. If that's the case, we'll notify you that your order will be shipping. You
                                                    can help us reach those preorders faster by spreading the word! We will notify as soon as we have a
                                                    production run scheduled in order to update any information before it ships.
                                                        <br /><br />
                                                    &bull; <strong>What if I preorder all 150 at once?</strong>
                                                <br />
                                                    The minimum is 150 units to trigger a production run. If you're a homebrew club, a retailer, or just a really,
                                                    really big fan of a strain, you can easily push that strain into production by meeting that minimum.
                                                        <br /><br />
                                                    &bull; <strong>What are my shipping options?</strong>
                                                <br />
                                                    All Vault preorders shipped direct to residences will ship on ice via FedEx One rate for a flat rate per strain
                                                    of $8.20. Vault orders via FedEx One should arrive within 2-3 days. Stores use their normal shipping method and
                                                    charges.
                                                        <br /><br />
                                                    &bull; <strong>When do you charge for the order?</strong>
                                                <br />
                                                    At shipping. However, your card will be validated at the time of the order.
                                                        <br /><br />
                                                    &bull; <strong>I am an international customer; can I preorder?</strong>
                                                <br />
                                                    Yes, the updated Vault preorder system allows international orders for individuals and stores.
                                                    You will use your normal international ship methods and prices.
                                                        <br /><br />
                                                    &bull; <strong>Can I cancel my order?</strong>
                                                <br />
                                                    Yes. Please do so by calling us at 888.593.2785 or emailing us at <a href='mailto:info@whitelabs.com'>info@whitelabs.com</a>.
                                                    </Typography>
                                        </div>
                                    }
                                    {this.state.showVaultFAQs &&
                                        <span style={{ cursor: 'pointer', color: 'blue', marginTop: '10px' }} onClick={this.handleClickVaultShowFAQs}><br />Hide FAQs ...</span>
                                    }
                                </div>
                                <img className={classes.image} style={{ maxWidth: '65%', margin: 'auto' }}
                                    src='/static/images/yeast-vault-how-it-works-graphic.png' />
                            </div>
                            <div className={classes.sectionTitleDiv}>
                                <span className={classes.sectionTitleSpan} style={{ color: '#a5a5a5' }} />
                                <span className={classes.titText}>
                                    <img
                                        className={classes.titTextImage}
                                        src='/static/images/vault_micro.png'
                                    />
                                </span>
                                <span className={classes.sectionTitleSpan} style={{ color: '#a5a5a5' }} />
                            </div>
                        </React.Fragment>

                        {
                            welcomeMessage && !sectionTitle && !isOnCreditHold && !isHomebrew && !isVault &&
                            (
                                <div className={classes.sectionTitleDiv} style={{ color: 'green', fontSize: 'x-large' }}>
                                    <span className={classes.sectionTitleSpan} />
                                    <span className={`${classes.titText} ${classes.addBorder}`}>{welcomeMessage}</span>
                                    <span className={classes.sectionTitleSpan} />
                                </div>
                            )
                        }

                        {
                            sectionTitle && !isOnCreditHold &&
                            (
                                <div className={classes.sectionTitleDiv}>
                                    <span className={classes.sectionTitleSpan} style={{ color: sectionColor }} />
                                    <span className={classes.titText}>{sectionTitle}</span>
                                    <span className={classes.sectionTitleSpan} style={{ color: sectionColor }} />
                                </div>
                            )
                        }

                        {sectionSubTitle && !isOnCreditHold && (
                            <div style={{ fontSize: 'smaller', textAlign: 'center', margin: 'auto', position: 'relative', top: '-20px' }}
                                dangerouslySetInnerHTML={{ __html: sectionSubTitle }} />
                        )}

                        {sectionTitle == 'Vault Strains' && !isOnCreditHold && (
                            <div>
                                <div className={classes.sectionTitleDiv} style={{ margin: 'auto', textAlign: 'center', color: 'white' }}>
                                    <span className={classes.sectionTitleSpan} style={{ color: 'transparent' }} />
                                    <span style={{
                                        textAlign: 'center', float: 'left', marginRight: '20px', minWidth: '150px', marginLeft: '10px', border: 'solid 1px black',
                                        backgroundImage: `url('static/images/categories/Category-vault.jpg')`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover'
                                    }}>
                                        <img
                                            src={'static/images/icons/vault-icon.svg'}
                                            height='40'
                                        />
                                        <br />Made&nbsp;To&nbsp;Order
                                    </span>
                                    <span style={{
                                        textAlign: 'center', float: 'left', marginLeft: '20px', minWidth: '150px', marginRight: '10px', border: 'solid 1px black',
                                        backgroundImage: `url('static/images/categories/Category-vault.jpg')`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover'
                                    }}>
                                        <img
                                            src={'static/images/icons/Yellow_Lock_v01.svg'}
                                            height='40'
                                        />
                                        <br />In Production
                                    </span>
                                    <span style={{
                                        textAlign: 'center', float: 'left', marginLeft: '20px', minWidth: '150px', marginRight: '10px', border: 'solid 1px black',
                                        backgroundImage: `url('static/images/categories/Category-vault.jpg')`, backgroundRepeat: 'no-repeat', backgroundSize: 'cover'
                                    }}>
                                        <img
                                            src={'static/images/icons/Green_Lock_v01.svg'}
                                            height='40'
                                        />
                                        <br />In Stock
                                    </span>
                                    <span className={classes.sectionTitleSpan} style={{ color: 'transparent' }} />
                                </div>
                                <div style={{ color: 'black', textAlign: 'center', marginTop: '20px', marginLeft: 'auto', marginRight: 'auto', maxWidth: '70%' }}>
                                    "In Stock" means some or all PurePitch or Standard Pour parts are in stock. To confirm availability, select the strain, then select the size and quantity you are looking for
                                    and hit the "Get Availability" button. The green unlocked symbol designates pro sizes only that are available. For homebrew packaging, please consult the Vault section under
                                    the Stores tab, or place a preorder under the Vault Preorder tab.
                                </div>
                            </div>
                        )}

                        {isHomebrew && !isOnCreditHold && (
                            <div style={{ maxWidth: '70%', margin: 'auto', textAlign: 'center' }}>
                                <div style={{ textAlign: 'center', paddingTop: '20px' }}>
                                    <Typography>
                                        Stores please use this page to enter your orders; the intention is for you to be able to enter all items from this page
                                        before proceeding to the cart.
                                    </Typography>
                                </div>
                                <div style={{ textAlign: 'center', paddingTop: '20px' }}>
                                    If you need to redeem or use Victory coupons, please visit <a href='https://classic.yeastman.com' target='_blank'>Yeastman Classic</a>,
                                    or call 888.593.2785 or email <a href='mailto:orders@whitelabs.com?subject=Victory%20Coupons'>orders@whitelabs.com</a>.
                                </div>
                                <div style={{ textAlign: 'center', paddingTop: '20px', cursor: 'pointer' }} onClick={this.handleClickHBVolumePricing}>
                                    <Typography>
                                        Prices drop for homebrew as volume levels are reached, with the first break happening at 25 packages. Click here for details.
                                    </Typography>
                                </div>
                                <div>
                                    <Typography style={{ textAlign: 'center' /*, visible: 'false', display: 'none'*/ }}>
                                        <br /><br />
                                        <h3>
                                            While we are all impacted by COVID-19, White Labs continues to produce yeast and
                                            support you during this time.  To protect our employees and comply with social
                                            distancing recommendations, we have scaled back our operation, and as a result,
                                            you may see limited availability with some yeast strains each week.  We continue
                                            to work diligently to produce the strains you need, and we thank you for your
                                            patience and understanding.  We value your continued business and look forward to
                                            the safe and healthy return of all in our industry!
                                    </h3>
                                    </Typography>
                                </div>
                            </div>
                        )}

                        {showAgeVerificationPrompt &&
                            <div style={{ margin: 'auto', textAlign: 'center' }} >
                                <Grid item xs={12}>
                                    <Typography style={{ margin: 'auto', fontSize: 'large' }}>
                                        Are you of legal drinking age?
                                    </Typography>
                                    <div>
                                        <FormCheckbox checked={this.state.userStatesIsLegalDrinkingAge} onChange={() => this.toggleIsLegalDrinkingAge()} />
                                        <span>I am at least 21 years old</span>
                                    </div>
                                    <Button className='form-button' variant='contained' onClick={this.handleAgeConfirmation}>
                                        {this.state.userStatesIsLegalDrinkingAge ? 'CONTINUE' : 'GO BACK'}
                                    </Button>
                                </Grid>
                            </div>
                        }

                        {!isOnCreditHold && !showAgeVerificationPrompt && pageContent}

                        <LoadingIndicator visible={this.props.loading.isLoading && this.props.loading.type == 'loadingInventory'}
                            label={'Loading Inventory'} />
                        <Dialog open={this.props.isDialogClose} onClose={this.handleLeaveItem}
                            aria-labelledby='form-dialog-title'>
                            {this.getDialogContent(this.state.item)}
                        </Dialog>
                        <Dialog open={this.state.openHBVolumePricing} onClose={this.handleLeaveHBVolumePricing}
                            aria-labelledby='form-dialog-title'>
                            {this.getRetailPricingDialogContent()}
                        </Dialog>
                    </NavBarUserSearchDrawerLayout>
                )
        );
    }
}

const styles = theme => ({
    store: {
        [theme.breakpoints.up('md')]: {
            paddingLeft: 50,
            paddingRight: 50
        },
        [theme.breakpoints.up('lg')]: {
            paddingLeft: 100,
            paddingRight: 100
        },
        [theme.breakpoints.up('xl')]: {
            paddingLeft: 150,
            paddingRight: 150
        },
        marginTop: 20,
        padding: 19,
        paddingTop: 'unset'
    },
    sectionTitleDiv: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        width: '88%',
        margin: 'auto',
        marginBottom: '54px',
        marginTop: '54px',
        [theme.breakpoints.down('sm')]: {
            width: '100%'
        }
    }, groupTitleDiv: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        width: '88%',
        margin: 'auto',
        marginBottom: '20px',
        marginTop: '20px',
        [theme.breakpoints.down('sm')]: {
            width: '100%'
        }
    }, jumpLinksDiv: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        width: '88%',
        margin: 'auto',
        fontSize: 'small',
        position: 'relative',
        top: '-16px',
        wordWrap: 'break-word',
        [theme.breakpoints.down('sm')]: {
            width: '100%'
        }
    }, vaultInfo: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        width: '88%',
        margin: 'auto',
        marginBottom: '54px',
        marginTop: '54px',
        [theme.breakpoints.down('sm')]: {
            width: '90%'
        }
    },
    sectionTitleSpan: {
        width: '42%',
        height: '2px',
        display: 'block',
        borderWidth: '1px',
        borderStyle: 'solid'
    },
    titText: {
        width: '16%',
        textAlign: 'center',
        [theme.breakpoints.down('sm')]: {
            fontSize: '20px',
            width: "auto"
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: '12px',
            width: '38%'
        }
    },
    searchInput: {
        marginLeft: 10
    },
    search: {
        padding: '5px',
        marginTop: '1vw'
    },
    divider: {
        margin: '10px auto'
    },
    tabLabel2: {
        width: '33%',
        maxWidth: 'unset',
        [theme.breakpoints.down('sm')]: {
            width: 'unset',
            maxWidth: '264px',
        },
    },
    tabLabel1: {
        padding: '0',
        width: '33%',
        maxWidth: 'unset',
        [theme.breakpoints.down('sm')]: {
            width: 'unset',
            maxWidth: '264px',
            padding: '6px 40px',
        },

    },
    headerBlock: {
        display: 'flex',
        height: 250,
        justifyContent: 'center',
        marginBottom: 40,
    },
    imageHeaderBlock: {
        width: '100%',
        height: 250,
    },
    store: {
        [theme.breakpoints.up('md')]: {
            paddingLeft: 50,
            paddingRight: 50,
        },
        [theme.breakpoints.up('lg')]: {
            paddingLeft: 100,
            paddingRight: 100,
        },
        [theme.breakpoints.up('xl')]: {
            paddingLeft: 150,
            paddingRight: 150,
        },
        marginTop: 20,
        padding: 31,
        paddingTop: 'unset',
    },
    sectionTitleDiv: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        width: '88%',
        margin: 'auto',
        marginBottom: '54px',
        marginTop: '54px',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
    },
    vaultInfo: {
        display: 'flex',
        alignItems: 'left',
        flexDirection: 'column',
        width: '88%',
        margin: 'auto',
        marginBottom: '54px',
        marginTop: '54px',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
    },
    vaultInfoParagraph: {
        marginTop: 20,
        textAlign: 'justify',
        width: '88%',
    },
    sectionImageSpan: {
        textAlign: 'center',
        float: 'left',
        marginRight: 20,
        minWidth: 150,
        marginLeft: 10,
        border: 'solid 1px black',
        backgroundImage: `url('static/images/categories/Category-vault.jpg')`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    },
    blockInfo: {
        color: 'black',
        textAlign: 'center',
        marginTop: 20,
    },
    clickableBlockInfo: {
        textAlign: 'center',
        paddingTop: 20,
        cursor: 'pointer',
    },
    addBorder: {
        border: 'solid 1px green',
        minWidth: '75%',
        padding: 10,
    },
    warningInfo: {
        textAlign: 'center',
        visible: 'false',
        display: 'none',
    },
    sectionTitleSpan: {
        width: '42%',
        height: '2px',
        display: 'block',
        borderWidth: '1px',
        borderStyle: 'solid',
    },
    titText: {
        width: '16%',
        textAlign: 'center',
        [theme.breakpoints.down('sm')]: {
            fontSize: '20px',
            width: 'auto',
        },
        [theme.breakpoints.down('xs')]: {
            fontSize: '12px',
            width: '38%',
        },
    },
    titTextWarning: {
        border: 'solid 1px red',
        minWidth: '75%',
        padding: '10px',
    },
    searchInput: {
        marginLeft: 10,
    },
    search: {
        padding: '5px',
        marginTop: '1vw',
    },
    divider: {
        margin: '10px auto',
    },
    tabLabel2: {
        width: '33%',
        maxWidth: 'unset',
    },
    tabLabel1: {
        padding: '0',
        width: '33%',
        maxWidth: 'unset',
    },

    searchIconmobile: {
        width: '34px',
        color: 'white',
        position: 'relative',
        background: '#f28531',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: '3px',
        borderRadius: '5px'
    },
    inputRootmobile: {
        color: 'inherit',
        width: '100%'
    },
    backIconWrapper: {
        position: 'fixed',
        top: '80%',
        left: '94%',
        cursor: 'pointer',
        [theme.breakpoints.down('xs')]: {
            left: '2px',
        },
    },
    backIcon: {
        display: 'block',
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        },
    },
    backIconTextButton: {
        width: '54px',
        border: '5px solid #f28531',
        borderRadius: '50%',
        fontSize: '11px',
        fontWeight: '900',
        textAlign: 'center',
        color: '#f28531',
        padding: '7px',
        [theme.breakpoints.up('md')]: {
            display: 'none'
        },
    },
    backIconText: {
        width: '54px',
        fontSize: '8px',
        fontWeight: '900',
        textAlign: 'right',
        color: '#f28531',
        padding: '7px',
        whiteSpace: 'nowrap',
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        },
    },
    inputInputmobile: {
        paddingTop: theme.spacing(1),
        paddingRight: theme.spacing(1),
        paddingBottom: theme.spacing(1),
        paddingLeft: theme.spacing(10),
        transition: theme.transitions.create('width'),
        width: '100%'
    },
    tooltip: {
        color: '#ffffff',
        padding: '2px',
        background: '#f28531',
        borderRadius: '2px solid #f28531',
        fontSize: '14px',
        fontWeight: 'bolder'
    },
    image: {
        width: '100%',
        marginRight: 10
    },
    searchIconmobile: {
        width: '34px',
        color: 'white',
        position: 'relative',
        background: '#f28531',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        margin: '3px',
        borderRadius: '5px',
    },
    inputRootmobile: {
        color: 'inherit',
        width: '100%',
    },
    backIcon: {
        display: 'block',
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
    },
    inputInputmobile: {
        paddingTop: theme.spacing(1),
        paddingRight: theme.spacing(1),
        paddingBottom: theme.spacing(1),
        paddingLeft: theme.spacing(10),
        transition: theme.transitions.create('width'),
        width: '100%',
    },
    tooltip: {
        color: '#ffffff',
        padding: '2px',
        background: '#f28531',
        borderRadius: '2px solid #f28531',
        fontSize: '14px',
        fontWeight: 'bolder',
    },
    image: {
        width: '100%',
        marginRight: 10,
    },
    titTextImage: {
        marginRight: 10,
        width: '60%',
    },
    sectionSubTitle: {
        fontSize: 'smaller',
        textAlign: 'center',
        whiteSpace: 'pre-wrap',
    },
});

Vault.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
    user: userSelectors.userSelector(state),
    messages: messagesSelectors.messagesSelector(state),
    loading: loadingSelectors.loadingSelector(state),
    inventory: inventorySelectors.inventorySelector(state),
    isDialogClose: cartSelectors.isDialogCloseSelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({ ...userActions, ...messageActions, ...inventoryActions, ...cartActions }, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )(compose(withStyles(styles, { withTheme: true })(withInventory(Vault)))),
);
