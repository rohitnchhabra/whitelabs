import React, { Component, Fragment } from "react";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { userActions } from "appRedux/actions/userActions";
import { messageActions } from "appRedux/actions/messageActions";
import Router from "next/router";
import NavBarLayout from "components/NavBar/NavBarLayout";
import NavBarUserSearchDrawerLayout from "components/NavBar/NavBarUserSearchDrawerLayout";
import PageContainer from "components/UI/PageContainer";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { Formik, Form, Field } from "formik";
import withStyles from "@material-ui/core/styles/withStyles";
import * as Yup from "yup";
import ReactGA from "react-ga";
import InputRange from "react-input-range";
import debounce from "lodash/debounce";
import axios from "axios";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormLabel from "@material-ui/core/FormLabel";

const arr = [
  {
    ques: "Are you a:",
    options: [
      { name: "Pro brewer" },
      { name: "Wine Maker" },
      { name: "HB" },
      { name: "Retailer/Wholesaler" },
      { name: "Other (specify)", text1: "true" }
    ]
  },
  {
    ques: "Where are you located?",
    options: [{ name: "USA" }, { name: "International" }]
  },
    {
        ques: "What browser are you using?",
        options: [
            { name: "Brave" },
            { name: "Chrome" },
            { name: "Edge" },
            { name: "Firefox" },
            { name: "Internet Explorer" },
            { name: "Opera" },
            { name: "Safari" },
            { name: "Vivaldi" },
            { name: "Other (specify)", text2: true }
        ]
    },
  {
    ques: "Did Yeastman meet your expectations?",
    options: [
      { name: "Yes" },
      { name: "No", input1: true },
      { name: "Unsure", input1: true }
    ]
  },
  {
    ques: "Did Yeastman allow you to find the information you needed?",
    options: [
      { name: "Yes" },
      { name: "No", input2: true },
      { name: "Unsure", input2: true }
    ]
  },
  {
    ques: "Did you find the navigation process:",
    options: [
      { name: "Difficult", input3: true },
      { name: "Average", input3: true },
      { name: "Easy" }
    ]
  }
];

class survey extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value: [],
            range1: 8,
            range2: 8,
            range3: 8,
            showRange1box: false,
            showRange2box: false,
            showRange3box: false
        };
        ReactGA.initialize("UA-40641680-2");
        ReactGA.pageview("/survey");
    }

    submitScreen = (values, { resetForm }) => {
        let rangeObj = {
            range1: this.state.range1,
            range2: this.state.range2,
            range3: this.state.range3
        };
        let obj = {};
        this.state.value.map((v, i) => {
            return (obj["check" + (1 + i)] = v);
        });
        let surveyValues = { ...values, ...obj, ...rangeObj };
        let check1 = surveyValues.check1;
        let check2 = surveyValues.check2;
        let check3 = surveyValues.check3;
        let check4 = surveyValues.check4;
        let check5 = surveyValues.check5;
        let check6 = surveyValues.check6;
        let check1Comment = surveyValues.check1Comment;
        let check3Comment = surveyValues.check3Comment;
        let check4Comment = surveyValues.check4Comment;
        let check5Comment = surveyValues.check5Comment;
        let check6Comment = surveyValues.check6Comment;
        let range1 = surveyValues.range1;
        let range2 = surveyValues.range2;
        let range3 = surveyValues.range3;
        let range1Comment = surveyValues.range1Comment;
        let range2Comment = surveyValues.range2Comment;
        let range3Comment = surveyValues.range3Comment;

        if (check1Comment) check1Comment = ': ' + check1Comment;
        if (check3Comment) check3Comment = ': ' + check3Comment;
        if (check4Comment) check4Comment = ': ' + check4Comment;
        if (check5Comment) check5Comment = ': ' + check5Comment;
        if (check6Comment) check6Comment = ': ' + check6Comment;
        if (range1Comment) range1Comment = ': ' + range1Comment;
        if (range2Comment) range2Comment = ': ' + range2Comment;
        if (range3Comment) range3Comment = ': ' + range3Comment;

        let request = {};
        request.isQuestionnaireResponse = true;
        request.source = 'Order Survey';
        if (this.props.user) request.id = this.props.user.id;

        request.questions = [];
        request.questions.push({ question: 'Customer type?', answer: (check1 + ' ' + check1Comment).trim() });
        request.questions.push({ question: 'Where are you located?', answer: check2 });
        request.questions.push({ question: 'What browser are you using?', answer: (check3 + ' ' + check3Comment).trim() });
        request.questions.push({ question: 'Did Yeastman meet your expectations?', answer: (check4 + ' ' + check4Comment).trim() });
        request.questions.push({ question: 'Did Yeastman allow you to find the information you needed?', answer: (check5 + ' ' + check5Comment).trim() });
        request.questions.push({ question: 'How did you find the navigation process?', answer: (check6 + ' ' + check6Comment).trim() });
        request.questions.push({ question: 'Please rate how easy it was to find the product/service you were searching for.', answer: (range1 + ' out of 10 ' + range1Comment).trim() });
        request.questions.push({ question: 'Please rate the speed of placing your order.', answer: (range2 + ' out of 10 ' + range2Comment).trim() });
        request.questions.push({ question: 'Please evaluate the design of yeastman.com.', answer: (range3 + ' out of 10 ' + range3Comment).trim() });

        axios.post('/record-survey-responses', { request })
            .then(({ data: { message, error } }) => {
                if (error) throw error;
                this.props.showBanner({
                    content: { message: (message ? message : "Feedback was submitted successfully") },
                    variant: "success"
                });
                resetForm();
                this.setState({ show: false });
                Router.push("/");
            })
            .catch(error => {
                this.props.showBanner({
                    content: { message: error },
                    variant: "error"
                });
            });

        /*
        // Old method of receiving feedback: Google documents
        axios
          .get(
            `https://script.google.com/macros/s/AKfycbxYbB5OwYjQ0S5C6KG8AL-SYLwqR4H8e9QXE_1VfgF3NBXZaGGo/exec?check1=${check1}&check2=${check2}&check3=${check3}&check4=${check4}&check4=${check4}&check5=${check5}&check6=${check6}&check1Comment=${check1Comment}&check3Comment=${check3Comment}&check4Comment=${check4Comment}&check5Comment=${check5Comment}&check6Comment=${check6Comment}&range1=${range1}&range2=${range2}&range3=${range3}&range1Comment=${range1Comment}&range2Comment=${range2Comment}&range3Comment=${range3Comment}`
          )
          .then(response => {
            this.props.showBanner({
              content: { message: "Survey was submitted successfully" },
              variant: "success"
            });
            resetForm();
            Router.push("/");
          })
          .catch((error) => {
            console.log(error, "err");
            this.props.showBanner({
              content: { message: error },
              variant: "error"
            });
          });
          */
    };

    handleChange = (event, i) => {
        const data = this.state.value;
        data[i] = event.target.value;
        this.setState({
            value: data
        });
    };

    componentDidUpdate(prevProps, prevState) {
        if (prevState.range1 !== this.state.range1) {
            _.delay(() => {
                if (this.state.range1 >= 0 && this.state.range1 < 8) {
                    this.setState({ showRange1box: true });
                } else {
                    this.setState({ showRange1box: false });
                }
            }, 1500);
        }
        if (prevState.range2 !== this.state.range2) {
            _.delay(() => {
                if (this.state.range2 >= 0 && this.state.range2 < 8) {
                    this.setState({ showRange2box: true });
                } else {
                    this.setState({ showRange2box: false });
                }
            }, 1500);
        }
        if (prevState.range3 !== this.state.range3) {
            _.delay(() => {
                if (this.state.range3 >= 0 && this.state.range3 < 8) {
                    this.setState({ showRange3box: true });
                } else {
                    this.setState({ showRange3box: false });
                }
            }, 1500);
        }
    }

    render() {
        const { classes } = this.props;
        console.log(this.props);

        return (
            <NavBarUserSearchDrawerLayout>
                <div className={classes.container}>
                    <PageContainer heading="Survey">
                        <Grid container className="flex-center">
                            <Formik
                                initialValues={{
                                    check1Comment: "",
                                    check3Comment: "",
                                    check4Comment: "",
                                    check5Comment: "",
                                    check6Comment: "",
                                    range1Comment: "",
                                    range2Comment: "",
                                    range3Comment: ""
                                }}
                                enableReinitialize
                                onSubmit={(values, { resetForm }) => {
                                    this.submitScreen(values, { resetForm });
                                }}
                            >
                                {({
                                    errors,
                                    touched,
                                    isValidating,
                                    handleChange,
                                    values,
                                    setFieldValue
                                }) => {
                                    return (
                                        <Form>
                                            <Grid item xs={12}>
                                                {arr.map((v, i) => {
                                                    return (
                                                        <Fragment key={i}>
                                                            <Typography
                                                                className={classes.typoText}
                                                                component="h2"
                                                                variant="Title"
                                                                gutterBottom
                                                            >
                                                                <img
                                                                    style={{ marginRight: "5px" }}
                                                                    src={"static/images/icons/arrow.svg"}
                                                                />
                                                                {v.ques}
                                                            </Typography>
                                                            <div>
                                                                <FormControl
                                                                    component="fieldset"
                                                                    className={classes.formControl}
                                                                >
                                                                    <RadioGroup
                                                                        aria-label="Gender"
                                                                        name="gender1"
                                                                        className={classes.group}
                                                                        value={this.state.value[i]}
                                                                        onChange={e => this.handleChange(e, i)}
                                                                    >
                                                                        {v.options.map((v, i) => {
                                                                            return (
                                                                                <FormControlLabel
                                                                                    value={v.name}
                                                                                    key={i}
                                                                                    control={
                                                                                        <Radio
                                                                                            style={{ marginLeft: "10px" }}
                                                                                            color="primary"
                                                                                        />
                                                                                    }
                                                                                    label={v.name}
                                                                                />
                                                                            );
                                                                        })}
                                                                    </RadioGroup>
                                                                    {v.options.find(
                                                                        item => this.state.value[i] === item.name
                                                                    ) &&
                                                                        v.options.find(
                                                                            item => this.state.value[i] === item.name
                                                                        ).input1 ? (
                                                                            <FormControl required>
                                                                                <textarea
                                                                                    className={classes.textarea}
                                                                                    placeholder="Please explain. What could be improved?"
                                                                                    name="check4Comment"
                                                                                    label="Please Explain"
                                                                                    variant="outlined"
                                                                                    margin="normal"
                                                                                    fullWidth
                                                                                    onChange={handleChange}
                                                                                    value={values.check4Comment}
                                                                                    maxLength="256"
                                                                                />
                                                                            </FormControl>
                                                                        ) : null}
                                                                    {v.options.find(
                                                                        item => this.state.value[i] === item.name
                                                                    ) &&
                                                                        v.options.find(
                                                                            item => this.state.value[i] === item.name
                                                                        ).input2 ? (
                                                                            <FormControl required>
                                                                                <textarea
                                                                                    className={classes.textarea}
                                                                                    placeholder="Please explain. What could be improved?"
                                                                                    name="check5Comment"
                                                                                    label="Please Explain"
                                                                                    variant="outlined"
                                                                                    margin="normal"
                                                                                    fullWidth
                                                                                    onChange={handleChange}
                                                                                    value={values.check5Comment}
                                                                                    maxLength="256"
                                                                                />
                                                                            </FormControl>
                                                                        ) : null}
                                                                    {v.options.find(
                                                                        item => this.state.value[i] === item.name
                                                                    ) &&
                                                                        v.options.find(
                                                                            item => this.state.value[i] === item.name
                                                                        ).input3 ? (
                                                                            <FormControl required>
                                                                                <textarea
                                                                                    className={classes.textarea}
                                                                                    placeholder="Please explain. What could be improved?"
                                                                                    name="check6Comment"
                                                                                    label="Please Explain"
                                                                                    variant="outlined"
                                                                                    margin="normal"
                                                                                    fullWidth
                                                                                    onChange={handleChange}
                                                                                    value={values.check6Comment}
                                                                                    maxLength="256"
                                                                                />
                                                                            </FormControl>
                                                                        ) : null}
                                                                    {v.options.find(
                                                                        item => this.state.value[i] === item.name
                                                                    ) &&
                                                                        v.options.find(
                                                                            item => this.state.value[i] === item.name
                                                                        ).text1 ? (
                                                                            <FormControl required>
                                                                                <textarea
                                                                                    className={classes.textarea}
                                                                                    placeholder="Please specify :)"
                                                                                    name="check1Comment"
                                                                                    label="Please Explain"
                                                                                    variant="outlined"
                                                                                    margin="normal"
                                                                                    fullWidth
                                                                                    onChange={handleChange}
                                                                                    value={values.check1Comment}
                                                                                    maxLength="256"
                                                                                />
                                                                            </FormControl>
                                                                        ) : null}
                                                                    {v.options.find(
                                                                        item => this.state.value[i] === item.name
                                                                    ) &&
                                                                        v.options.find(
                                                                            item => this.state.value[i] === item.name
                                                                        ).text2 ? (
                                                                            <FormControl required>
                                                                                <textarea
                                                                                    className={classes.textarea}
                                                                                    placeholder="Please specify :)"
                                                                                    name="check3Comment"
                                                                                    label="Please Explain"
                                                                                    variant="outlined"
                                                                                    margin="normal"
                                                                                    fullWidth
                                                                                    onChange={handleChange}
                                                                                    value={values.check3Comment}
                                                                                    maxLength="256"
                                                                                />
                                                                            </FormControl>
                                                                        ) : null}
                                                                </FormControl>
                                                            </div>
                                                        </Fragment>
                                                    );
                                                })}

                                                <Typography
                                                    className={classes.typoText}
                                                    component="h2"
                                                    variant="Title"
                                                    gutterBottom
                                                >
                                                    <img
                                                        style={{ marginRight: "5px" }}
                                                        src={"static/images/icons/arrow.svg"}
                                                    />
                                                    Please rate how easy it was to find the
                                                    product/service you were searching for.
                        </Typography>
                                                <div className={classes.InputRangeWrap}>
                                                    <InputRange
                                                        maxValue={10}
                                                        minValue={0}
                                                        value={this.state.range1}
                                                        classNames={{
                                                            activeTrack:
                                                                "input-range__track input-range__track--active my-slider-track",
                                                            disabledInputRange:
                                                                "input-range input-range--disabled",
                                                            inputRange: "input-range",
                                                            labelContainer: "input-range__label-container",
                                                            maxLabel:
                                                                "input-range__label input-range__label--max",
                                                            minLabel:
                                                                "input-range__label input-range__label--min",
                                                            slider: "input-range__slider my-slider",
                                                            sliderContainer: "input-range__slider-container",
                                                            track:
                                                                "input-range__track input-range__track--background",
                                                            valueLabel:
                                                                "input-range__label input-range__label--value"
                                                        }}
                                                        onChange={range1 => this.setState({ range1 })}
                                                    />
                                                </div>

                                                {this.state.showRange1box ? (
                                                    <Fragment>
                                                        <p
                                                            style={{
                                                                margin: "10px 0px 20px 0px",
                                                                color: "#f28411",
                                                                textAlign: "center"
                                                            }}
                                                        >
                                                            Please describe your challenges with your search?
                            </p>
                                                        <FormControl margin="normal" required fullWidth>
                                                            <textarea
                                                                className={classes.textarea}
                                                                placeholder="Your feedback is valuable to us :)"
                                                                name="range1Comment"
                                                                label="Please Explain"
                                                                variant="outlined"
                                                                margin="normal"
                                                                fullWidth
                                                                onChange={handleChange}
                                                                value={values.range1Comment}
                                                                maxLength="256"
                                                            />
                                                        </FormControl>
                                                    </Fragment>
                                                ) : null}

                                                <Typography
                                                    className={classes.typoText}
                                                    component="h2"
                                                    variant="Title"
                                                    gutterBottom
                                                >
                                                    <img
                                                        style={{ marginRight: "5px" }}
                                                        src={"static/images/icons/arrow.svg"}
                                                    />
                                                    Please rate the speed of placing your order.
                        </Typography>
                                                <div className={classes.InputRangeWrap}>
                                                    <InputRange
                                                        maxValue={10}
                                                        minValue={0}
                                                        value={this.state.range2}
                                                        classNames={{
                                                            activeTrack:
                                                                "input-range__track input-range__track--active my-slider-track",
                                                            disabledInputRange:
                                                                "input-range input-range--disabled",
                                                            inputRange: "input-range",
                                                            labelContainer: "input-range__label-container",
                                                            maxLabel:
                                                                "input-range__label input-range__label--max",
                                                            minLabel:
                                                                "input-range__label input-range__label--min",
                                                            slider: "input-range__slider my-slider",
                                                            sliderContainer: "input-range__slider-container",
                                                            track:
                                                                "input-range__track input-range__track--background",
                                                            valueLabel:
                                                                "input-range__label input-range__label--value"
                                                        }}
                                                        onChange={range2 => this.setState({ range2 })}
                                                    />
                                                </div>
                                                {this.state.showRange2box ? (
                                                    <Fragment>
                                                        <p className={classes.para}>
                                                            How could the purchasing process be improved?
                            </p>
                                                        <FormControl margin="normal" required fullWidth>
                                                            <textarea
                                                                className={classes.textarea}
                                                                placeholder="Your feedback is valuable to us :)"
                                                                name="range2Comment"
                                                                label="Please Explain"
                                                                variant="outlined"
                                                                margin="normal"
                                                                fullWidth
                                                                onChange={handleChange}
                                                                value={values.range2Comment}
                                                                maxLength="256"
                                                            />
                                                        </FormControl>
                                                    </Fragment>
                                                ) : null}

                                                <Typography
                                                    className={classes.typoText}
                                                    component="h2"
                                                    variant="Title"
                                                    gutterBottom
                                                >
                                                    <img
                                                        style={{ marginRight: "5px" }}
                                                        src={"static/images/icons/arrow.svg"}
                                                    />
                                                    Please evaluate the design of yeastman.com.
                        </Typography>
                                                <div className={classes.InputRangeWrap}>
                                                    <InputRange
                                                        maxValue={10}
                                                        minValue={0}
                                                        value={this.state.range3}
                                                        classNames={{
                                                            activeTrack:
                                                                "input-range__track input-range__track--active my-slider-track",
                                                            disabledInputRange:
                                                                "input-range input-range--disabled",
                                                            inputRange: "input-range",
                                                            labelContainer: "input-range__label-container",
                                                            maxLabel:
                                                                "input-range__label input-range__label--max",
                                                            minLabel:
                                                                "input-range__label input-range__label--min",
                                                            slider: "input-range__slider my-slider",
                                                            sliderContainer: "input-range__slider-container",
                                                            track:
                                                                "input-range__track input-range__track--background",
                                                            valueLabel:
                                                                "input-range__label input-range__label--value"
                                                        }}
                                                        onChange={range3 => this.setState({ range3 })}
                                                    />
                                                </div>
                                                {this.state.showRange3box ? (
                                                    <Fragment>
                                                        <p className={classes.para}>
                                                            Please describe how it could be improved?
                            </p>
                                                        <FormControl margin="normal" required fullWidth>
                                                            <textarea
                                                                className={classes.textarea}
                                                                placeholder="Your feedback is valuable to us :)"
                                                                name="range3Comment"
                                                                label="Please Explain"
                                                                variant="outlined"
                                                                margin="normal"
                                                                fullWidth
                                                                onChange={handleChange}
                                                                value={values.range3Comment}
                                                                maxLength="256"
                                                            />
                                                        </FormControl>
                                                    </Fragment>
                                                ) : null}
                                            </Grid>
                                            <Grid
                                                style={{ marginTop: 10 }}
                                                container
                                                justify="center"
                                            >
                                                <Grid item>
                                                    <Button
                                                        variant="contained"
                                                        color="primary"
                                                        type="submit"
                                                    >
                                                        Submit Survey
                          </Button>
                                                </Grid>
                                            </Grid>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Grid>
                    </PageContainer>
                </div>
            </NavBarUserSearchDrawerLayout>
        );
    }
}

const styles = theme => ({
  starDimension: {
    fontSize: "20px"
  },
  container: {
    width: "89%",
    marginLeft: "6%"
  },
  typoText: {
    display: "flex",
    //justifyContent: "center",
    //textAlign: "center",
    color: "#f28411",
    fontSize: "1.5em",
    //marginTop: "20px",
    //marginBottom: "20px",
    [theme.breakpoints.down("xs")]: {
      fontSize: "14px",
      marginLeft: "10px"
    }
  },
  InputRangeWrap: {
    width: "70%",
    margin: "0 auto",
    marginTop: "30px",
    marginBottom: "30px",
    display: "flex",
    justifyContent: "center"
  },
  para: {
    margin: "10px 0px 20px 0px",
    color: "#f28411",
    textAlign: "center"
  },
  textarea: {
    width: "100%",
    height: "60px",
    padding: "12px 20px",
    boxSizing: "border-box",
    border: "2px solid #ccc",
    borderRadius: "4px",
    backgroundColor: "#f8f8f8",
    resize: "none",
    marginBottom: "15px",
    margin: "0 auto",
    [theme.breakpoints.down("xs")]: {
      width: "245px"
    }
  },
  userContDisp: {
    display: "flex"
  },
  userContNone: {
    display: "none"
  },
  root: {
    display: "flex"
  },
  formControl: {
    //margin: theme.spacing(3),
  },
  group: {
    //margin: `${theme.spacing(1)}px 0`,
  },

  InputRangeClassNames: {
    backgroundColor: "red"
  }
});

const mapStateToProps = state => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...userActions, ...messageActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles, { withTheme: true })(survey));
