import React, { Component } from 'react';
import { connect } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import NavBarUserSearchDrawerLayout from 'components/NavBar/NavBarUserSearchDrawerLayout';
import PageContainer from 'components/UI/PageContainer';
import Button from '@material-ui/core/Button';


class PhoneList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        
        const { classes, theme} = this.props;

        return (
            <NavBarUserSearchDrawerLayout>
                <PageContainer heading='PHONE LIST' id='phone-list'>
                    <Grid container spacing={6}>
                            <Grid item xs={12}>
                                <div className={classes.card}>
                                    <div style={{ position: 'absolute', top: -15, left: 20, backgroundColor: '#fafafa', paddingLeft: 10, paddingRight: 10 }}>
                                        <Typography
                                            variant='h6'
                                            color='textPrimary'
                                        >
                                            Aaron Mongelli
                                        </Typography>
                                    </div>
                                    <Grid item container>
                                        <Grid
                                            item
                                            xs={12}
                                            sm
                                            container
                                            direction='column'
                                            style={{ marginTop: 20, paddingLeft:10 }}
                                        >
                                            
                                            <Grid
                                                container
                                                item
                                                spacing={6}
                                                direction='row'
                                            >
                                                

                                                <Grid
                                                    container
                                                    item
                                                    spacing={6}
                                                    justify='center'
                                                >
                                                    <Grid item xs={12} md={10}>
                                                        <Typography
                                                            variant='overline'
                                                        >
                                                            Customer Service Representative
                                                        </Typography>
                                                    </Grid>
                                                    <Grid item xs={12} md={2}>
                                                            <Typography
                                                                variant='h6'
                                                            >
                                                                EXT: 7695
                                                            </Typography>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Grid>
                    </Grid>

                </PageContainer>
            </NavBarUserSearchDrawerLayout>
        );
    }
}

const styles = theme => ({

    card: {
        position: 'relative',
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2)
    },

});
PhoneList.propTypes = {};

const mapStateToProps = state => {
    return {
        messages: state.messages
    };
};

export default connect(
    mapStateToProps,
    null
)(withStyles(styles, { withTheme: true })(PhoneList));
