import React, { Component } from 'react';
import { connect } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import NavBarUserSearchDrawerLayout from 'components/NavBar/NavBarUserSearchDrawerLayout';
import PageContainer from 'components/UI/PageContainer';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';


class SalesOrderLookup extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        
        const { classes, theme} = this.props;

        return (
            <NavBarUserSearchDrawerLayout>
                <PageContainer heading='SALES ORDER LOOKUP' id='sales-order-lookup'>
                <Paper className={classes.root}>
                    <InputBase
                        className={classes.input}
                        placeholder="Search Orders"
                        inputProps={{ 'aria-label': 'Search Orders' }}
                    />
                    <IconButton className={classes.iconButton} aria-label="Search">
                        <SearchIcon />
                    </IconButton>
                </Paper>

                <Grid container spacing={6} style={{marginTop:20}}>

                            <Grid item xs={12}>
                                <div className={classes.card}>
                                    <div style={{ position: 'absolute', top: -15, left: 20, backgroundColor: '#fafafa', paddingLeft: 10, paddingRight: 10 }}>
                                        <Typography
                                            variant='h6'
                                            color='textPrimary'
                                        >
                                            Order # 
                                        </Typography>
                                    </div>
                                    <Grid item container>
                                        <Grid item>
                                            <img
                                                className={classes.image}
                                                src='/static/images/yeast.png'
                                            />
                                        </Grid>
                                        <Grid
                                            item
                                            xs={12}
                                            sm
                                            container
                                            direction='column'
                                            style={{ marginTop: 20 }}
                                        >
                                            <Grid item xs>
                                                <Typography
                                                    variant='overline'
                                                    color='textPrimary'
                                                >
                                                    Ship date is 
                                                </Typography>
                                            </Grid>
                                            <Grid
                                                container
                                                item
                                                spacing={6}
                                                direction='row'
                                            >
                                                <Grid item>
                                                    <Button
                                                        variant='outlined'
                                                        color='primary'
                                                    >
                                                        Order Details
                                                </Button>
                                                </Grid>
                                                <Grid
                                                    container
                                                    item
                                                    spacing={6}
                                                    justify='flex-end'
                                                >
                                                    <Grid item xs={12} md={2}>
                                                        <div
                                                            style={{
                                                                backgroundColor:
                                                                    '#f28411',
                                                                padding: 2,
                                                                textAlign: 'center'
                                                            }}
                                                        >
                                                            <Typography
                                                                variant='h6'
                                                                color='secondary'
                                                            >
                                                                Status
                                                            </Typography>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </div>
                            </Grid>
                    </Grid>

                </PageContainer>
            </NavBarUserSearchDrawerLayout>
        );
    }
}

const styles = theme => ({

    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
      },
    input: {
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
        padding: 10,
    }, 
    card: {
        position: 'relative',
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2)
    },
    image: {
        width: 170,
        marginRight: 10,
        textAlign: 'center'
    },


});
SalesOrderLookup.propTypes = {};

const mapStateToProps = state => {
    return {
        messages: state.messages
    };
};

export default connect(
    mapStateToProps,
    null
)(withStyles(styles, { withTheme: true })(SalesOrderLookup));
