import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Link from "next/link";
import Router from "next/router";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import NavBarUserSearchDrawerLayout from "components/NavBar/NavBarUserSearchDrawerLayout";
import NavBarLayout from 'components/NavBar/NavBarLayout';
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import CartItem from "components/Cart/CartItem";
import SuggestedItem from "components/Cart/SuggestedItem";
import UnreorderableItem from "components/Cart/UnreorderableItem";

import FormButton from "components/Form/FormButton";
import PageContainer from "components/UI/PageContainer";
import isLoggedUser from "hocs/isLoggedUser";
import { cartActions } from "appRedux/actions/cartActions";
import { userActions } from "appRedux/actions/userActions";
import ReactGA from "react-ga";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import RetailVolumePricingDialog from "components/Store/Menu/RetailVolumePricingDialog";
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

class Cart extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openHBVolumePricing: false,
            openClearCartDialog: false,
            showUnreorderableItemsDialog: false,
            unreorderableItems: null,
            hasUnavailableItems: false
        };

        ReactGA.initialize("UA-40641680-2");
        ReactGA.pageview("/cart");
    }

    clearCartDialog = () => {
        this.setState({
            openClearCartDialog: true
        });
    };

    handleCloseDialog = () => {
        this.setState({
            openClearCartDialog: false
        });
    };

    handleClearCart = () => {
        this.props.clearCart();
        this.setState({
            openClearCartDialog: false
        });
    };

    handleProceed = () => {
        var hasUnavailableItems = false;
        if (this.props.cart.items) {
            for (var i = 0; i < this.props.cart.items.length; i++) {
                var cartItem = this.props.cart.items[i];
                for (var j = 0; j < this.props.inventory.items.length; j++) {
                    var invItem = this.props.inventory.items[j];
                    if ((cartItem.isVaultPreorderItem && !invItem.isVaultPreorderItem) || (!cartItem.isVaultPreorderItem && invItem.isVaultPreorderItem))
                        continue;

                    if (cartItem.Name == invItem.Name) {
                        if (!invItem.volID.includes(cartItem.MerchandiseID)) {
                            cartItem.isReorderItem = true;
                            cartItem.isAvailableForReorder = false;
                            hasUnavailableItems = true;
                        }
                        break;
                    }
                }
            }
        }

        if (!hasUnavailableItems) {
            const { user } = this.props;
            if (!user.id) {
                Router.push("/login");
            } else {
                this.props.checkCustomerHoldStatus();
                sessionStorage.setItem("orderComplete", false);
                Router.push("/checkout");
            }
        } else {
            this.setState({ hasUnavailableItems: true });
        }
    };

    handleClickHBVolumePricing = () => {
        this.setState({ openHBVolumePricing: true });
    };

    handleLeaveHBVolumePricing = () => {
        this.setState({ openHBVolumePricing: false });
    };

    openUnreoderableItemsDialog = () => {
        this.setState({ showUnreorderableItemsDialog: true });
    };

    handleLeaveUnreoderableItemsDialog = () => {
        this.setState({ showUnreorderableItemsDialog: false });
    };

    getRetailPricingDialogContent = () => {
        return <RetailVolumePricingDialog closeDialog={this.handleLeaveHBVolumePricing} />;
    };

    getSuggestedItems = (user, cartItems) => {
        const dev = process.env.NODE_ENV !== "production";
        var suggestedItems = [];

        if (cartItems && cartItems.length > 0) {
            var itemIds = [];

            //8/20/20: Per Mike, disable this indefinitely.
            /*
            if (user.subsidiary == 2 && user.shipping.countryid == 'US') {
                // Only suggest gel packs for US subsidiary
                var gelPackItem = {};
                gelPackItem.Name = '8oz Gel Pack (ice packs)';
                gelPackItem.salesCategory = 27; // Gift shop
                gelPackItem.dispQuantity = 1;
                gelPackItem.OrderDetailQty = 1;
                gelPackItem.MerchandiseID = 4328;
                gelPackItem.details = 'Customers are welcome to order additional ice packs if desired beyond those included in your package. Each unit costs $1.'
                    + ' If your order includes yeast sourced from more than one warehouse, the specified number of ice packs will be included in each warehouse.';
                gelPackItem.isGelPackItem = true;
                gelPackItem.includeSuggestion = true;
                suggestedItems.push(gelPackItem);
                itemIds.push(gelPackItem.MerchandiseID);
            }
            */

            // Per Mike, for now, only show item-based related items in the US subsidiary
            if (user.subsidiary == 2) {
                for (var i = 0; i < cartItems.length; i++) {
                    if (cartItems[i].relatedItems && cartItems[i].relatedItems.length > 0) {
                        for (var k = 0; k < cartItems[i].relatedItems.length; k++) {
                            if (!itemIds.includes(cartItems[i].relatedItems[k].relatedItemId)) {
                                var suggestedItem = {};
                                suggestedItem.MerchandiseID = cartItems[i].relatedItems[k].relatedItemId;
                                suggestedItem.partNum = cartItems[i].relatedItems[k].related.partNum;
                                suggestedItem.Name = cartItems[i].relatedItems[k].related.Name;
                                suggestedItem.salesCategory = cartItems[i].relatedItems[k].related.salesCategory;
                                suggestedItem.type = parseInt(cartItems[i].relatedItems[k].related.cartItemType ? cartItems[i].relatedItems[k].related.cartItemType : cartItems[i].relatedItems[k].related.designation);
                                suggestedItem.dispQuantity = 1;
                                suggestedItem.OrderDetailQty = 1;
                                suggestedItem.isGelPackItem = (suggestedItem.MerchandiseID == 4328);
                                suggestedItem.includeSuggestion = true;
                                suggestedItem.details = 'Because you ordered ' + cartItems[i].Name;
                                suggestedItem.Description = cartItems[i].relatedItems[k].related.Description;
                                suggestedItem.imageUrl = (cartItems[i].relatedItems[k].related.imageUrl ? cartItems[i].relatedItems[k].related.imageUrl : 'static/images/categories/Category-belgian.jpg');
                                suggestedItems.push(suggestedItem);
                                itemIds.push(suggestedItem.MerchandiseID);
                            }
                        }
                    }
                }
            }
        }

        return suggestedItems;
    }

    renderSuggestedItems(suggestedItems) {
        if (suggestedItems && suggestedItems.length > 0) {
            return (
                <Fragment>
                    <Grid container spacing={6} style={{ marginTop: "10px" }}>
                        {
                            suggestedItems.map((item, i) => {
                                if (item.includeSuggestion) {
                                    return <SuggestedItem key={i + 99} item={item} index={i + 99} />;
                                } else {
                                    return <SuggestedItem key={i + 99} item={item} index={i + 99} style={{ visible: 'false', display: none }} />;
                                }
                            })
                        }
                    </Grid>
                </Fragment>
            );
        }
    }

    renderUnorderableItems(unorderableItems) {
        if (unorderableItems && unorderableItems.length > 0) {
            return (
                <Fragment>
                    <Grid container spacing={6} style={{ marginTop: "10px" }}>
                        {
                            unorderableItems.map((item, i) => {
                                return <UnreorderableItem key={i + 999} item={item} index={i + 999} />;
                            })
                        }
                    </Grid>
                </Fragment>
            );
        }
    }

    renderUnoderableItemsDialog(unorderableItems) {
        return (
            <React.Fragment>
                <DialogContent>
                    <div style={{ width: '100%', textAlign: 'right', position: 'absolute', left: '0px', top: '9px' }}>
                        <IconButton
                            color='inherit'
                            size='small'
                            aria-label='Menu'
                            onClick={() => this.handleLeaveUnreoderableItemsDialog()}
                        >
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Grid
                        item
                        container
                        xs
                        style={{
                            display: 'flex',
                            marginTop: -10,
                            marginBottom: 15
                        }}
                        direction={'row'}
                        spacing={2}
                    >
                        <Grid item>
                            <Typography variant='h5' style={{ color: '#f28531' }} onClick={(partNum) => this.handleClick(partNum)}>
                                NOT ALL OF THE ITEMS IN YOUR CART ARE CURRENTLY AVAILABLE
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={2}
                        style={{ marginTop: 5 }}
                    >
                        <Grid item>
                            <Typography>
                                <p style={{ fontWeight: 'bold' }}>
                                    Some of the items in your cart cannot be ordered at this time. Please make note of these items.
                                </p>
                            </Typography>
                            <Grid container spacing={6} style={{ marginTop: "10px" }}>
                                <ul>
                                    {
                                        unorderableItems.map((item, i) => {
                                            return <li>{item.Name}</li>;
                                        })
                                    }
                                </ul>
                            </Grid>
                        </Grid>
                    </Grid>
                </DialogContent>
            </React.Fragment>
        );
    }

    checkChekoutOption() {
        const { cart } = this.props;
        if (cart.items) {
            let checkQuantity = cart.items.filter(element => {
                if (element.dispQuantity != "" && element.dispQuantity > 0) {
                    return element
                }
            });

            if (checkQuantity.length == cart.items.length) {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }

    render() {
        const { classes, cart, user } = this.props;
        var retailItemCount = cart.items ? cart.items.reduce((a, b) => a + (b.type == 2 /*&& !b.isVaultPreorderItem*/ ? b["dispQuantity"] || 0 : 0), 0) : 0;
        var yeastItemCount = cart.items ? cart.items.reduce((a, b) => a + (b.salesCategory == 2 || b.salesCategory == 3 || b.salesCategory == 4 || b.salesCategory == 5 || b.salesCategory == 6 || b.salesCategory == 7 || b.salesCategory == 32 ? b["dispQuantity"] || 0 : 0), 0) : 0;
        var preorderItemCount = cart.items ? cart.items.reduce((a, b) => a + (b.isVaultPreorderItem ? b["dispQuantity"] || 0 : 0), 0) : 0;

        var unreorderableItems = [];
        for (var i = 0; i < this.props.cart.items.length; i++) {
            if (this.props.cart.items[i].isReorderItem && !this.props.cart.items[i].isAvailableForReorder) {
                unreorderableItems.push(this.props.cart.items[i]);
                this.props.cart.items.splice(i, 1);
                i--;
            }
        }
        if (unreorderableItems.length > 0 && this.props.cart.items.length == 1 && this.props.cart.items[0].MerchandiseID == 4328) {
            //Remove standalone ice pack
            this.props.cart.items.splice(0, 1);
        }

        var suggestedItems = this.getSuggestedItems(user, cart.items);

        return (
            <NavBarUserSearchDrawerLayout>
                <PageContainer heading="SHOPPING CART" id="cart-box">
                    <Grid 
                        item
                        container 
                        // direction={'row'}
                        justify='center'
                        spacing={4} 
                        className={classes.contentBox}
                    >
                        {this.props.cart.items &&
                            this.props.cart.items.map((item, i) => {
                                if (suggestedItems.length > 0) {
                                    for (var j = suggestedItems.length - 1; j >= 0; j--) {
                                        if (item.MerchandiseID == suggestedItems[j].MerchandiseID) {
                                            suggestedItems[j].includeSuggestion = false;
                                        } else if (suggestedItems[j].isGelPackItem) {
                                            if (item.isVaultPreorderItem) {
                                                suggestedItems[j].includeSuggestion = false;
                                            } else if (retailItemCount == 0 && yeastItemCount == 0) {
                                                suggestedItems[j].includeSuggestion = false;
                                            }
                                        }

                                        if (!suggestedItems[j].includeSuggestion) suggestedItems.splice(j, 1);
                                    }
                                }
                                if (item.isReorderItem && !item.isAvailableForReorder) {
                                    return null;
                                } else {
                                    return <CartItem key={i} item={item} index={i} />;
                                }
                            })
                        }
                    </Grid>
                    {yeastItemCount > 0 &&
                        <Typography style={{ fontWeight: "bolder" }}>
                            ** Temperature sensitive products are shipped with ice packs and insulation to
                            ensure ideal temperatures are maintained from transit to delivery **
                        </Typography>
                    }
                    {cart.items.length > 0
                        &&
                        <Typography style={{ fontWeight: "bolder" }}>
                            Prices will be shown in checkout.
                        {preorderItemCount > 0 &&
                                <Typography>
                                    <br />Customers with pre-order items will receive an email when these item(s) are assigned a ship date.
                                    Customers will be asked to update their address and credit card information if applicable at this time.
                                    Cards will be charged close to the ship date.
                            </Typography>
                            }
                            {suggestedItems && suggestedItems.length > 0 &&
                                <Typography><br />Based on the contents of your cart, you may be interested in the following item(s).</Typography>
                            }
                        </Typography>
                    }
                    {this.renderSuggestedItems(suggestedItems)}
                    {this.state.hasUnavailableItems &&
                        <Grid container spacing={6} style={{ marginTop: "10px" }}>
                            <Typography>
                                <br />
                                Some of the items in your cart are no longer available. They may have been replaced with new versions. Please check the store listings for details.
                            </Typography>
                            <ul>
                                {
                                    unreorderableItems.map((item, i) => {
                                        return <li>{item.Name}</li>;
                                    })
                                }
                            </ul>
                        </Grid>
                    }
                    {this.renderUnorderableItems(this.state.unreorderableItems)}

                    <Grid container spacing={6} dir="rtl" className="block-checkout-button">
                        <Grid item xs={12}>
                            {retailItemCount > 0 && (
                                <Grid item xs={12}>
                                    <br />
                                    Your order includes {retailItemCount} yeast HB package
                                    {retailItemCount > 1 ? "s" : ""}
                                    <br />
                                    <Button variant="contained" color="primary" className={classes.button} onClick={this.handleClickHBVolumePricing}>
                                        Review Price Break Levels
                                    </Button>
                                </Grid>
                            )}
                            {cart.items.length > 0 ? (
                                <Fragment>
                                    <Grid>
                                        <Button variant="contained" color="primary" disabled={!this.checkChekoutOption()} className={classes.button} onClick={this.handleProceed}>
                                            Proceed to Checkout
                                        </Button>
                                    </Grid>
                                    <Grid>
                                        <Button variant="contained" color="primary" className={classes.button} onClick={this.clearCartDialog}>
                                            Clear Cart
                                        </Button>
                                    </Grid>
                                </Fragment>
                            ) : (
                                    <Typography variant="h5" color="primary" align="center">
                                        Cart is Empty
                                </Typography>
                                )}
                        </Grid>
                    </Grid>
                </PageContainer>
                <Dialog open={this.state.openHBVolumePricing} onClose={this.handleLeaveHBVolumePricing} aria-labelledby="form-dialog-title">
                    {this.getRetailPricingDialogContent()}
                </Dialog>
                <Dialog open={unreorderableItems.length > 0} onClose={this.handleLeaveUnreoderableItemsDialog} aria-labelledby="form-dialog-title">
                    {this.renderUnoderableItemsDialog(unreorderableItems)}
                </Dialog>
                <Dialog open={this.state.openClearCartDialog} onClose={this.handleCloseDialog} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                    <DialogTitle id="alert-dialog-title">{"Clear Cart?"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">Do You Want To Clear All The Items From Your Cart</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseDialog} color="primary">
                            No
                        </Button>
                        <Button onClick={this.handleClearCart} color="primary" autoFocus>
                            Yes
                        </Button>
                    </DialogActions>
                </Dialog>
            </NavBarUserSearchDrawerLayout>
        );
    }
}

const styles = theme => ({
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1)
    },
    hide: {
        display: "none"
    },
    card: {
        display: "flex"
    },
    image: {
        width: 150
    },
    quantity: {
        width: 50,
        marginRight: 20
    },
    details: {
        display: "flex",
        flexDirection: "column"
    },

});

Cart.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        cart: state.cart,
        user: state.user,
        inventory: state.inventory
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions, ...userActions }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(compose(withStyles(styles, { withTheme: true })(isLoggedUser(Cart))));
