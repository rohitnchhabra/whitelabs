import React from 'react';
import NavBarUserSearchDrawerLayout from '../components/NavBar/NavBarUserSearchDrawerLayout';
import { connect } from 'react-redux';
import withStyles from '@material-ui/core/styles/withStyles';
import Report from '../components/Report/Report';

const Info = () => {
    return (
        <NavBarUserSearchDrawerLayout>
            <div>
                <Report/>
            </div>
        </NavBarUserSearchDrawerLayout>
    );
};

const styles = theme => ({});

const mapStateToProps = state => {
    return {
        messages: state.messages
    };
};
export default connect(
    mapStateToProps,
    null
)(withStyles(styles, { withTheme: true })(Info));
