import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _get from 'lodash/get';
import _set from 'lodash/set';
import _isEmpty from 'lodash/isEmpty';

import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Card from 'components/UI/Card/Card.jsx';
import CardBody from 'components/UI/Card/CardBody.jsx';
import CardHeader from 'components/UI/Card/CardHeader.jsx';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

import Banner from 'components/UI/Banner';
import SimpleSnackbar from 'components/Form/SimpleSnackbar';
import { Formik, Form, Field } from 'formik';

import { messageActions } from 'appRedux/actions/messageActions';
import { userActions } from 'appRedux/actions/userActions';
import ReactGA from 'react-ga';

const FormikErrorMessage = ({ error }) => {
    return error ? <div className='error'>{error}</div> : null;
};

function ForgotPassword(props) {
    ReactGA.initialize('UA-40641680-2');
    ReactGA.pageview('/forgotpassword');

    const { classes, messages } = props;

    const validate = (values) => {
        let errors = {};

        if (!_get(values, 'email')) {
            _set(errors, 'email', 'Email address or user name is required');
        }

        return errors;
    }

    const forgotPassword = (values, { setErrors }) => {
        let errors = validate(values);
        if (_isEmpty(errors)) {
            props.forgotPassword({ email: values.email });
        } else {
            setErrors(errors);
        }
    }

    return (
        <React.Fragment>
            <main className={classes.layout}>
                <Banner />
                <SimpleSnackbar
                    messageList={messages.snackbar || []}
                    handleClose={() => props.hideSnackbar()}
                />
                <Card>
                    <CardHeader color='primary'>
                        <div className={classes.logo}>
                            <a href='/'>
                                <img
                                    src='static/images/logoHeader.png'
                                    width='100%'
                                />
                            </a>
                        </div>
                    </CardHeader>

                    <CardBody>
                        <Typography variant='headline' align='center'>
                            Forgot Password
                        </Typography>
                        <Formik
                            onSubmit={(values, actions) => forgotPassword(values, actions)}
                        >
                            {({ errors }) => {
                                return (
                                    <Form>
                                        <Field
                                            render={({ field: { value, onChange } }) => {
                                                return (
                                                    <FormControl margin='normal' required fullWidth>
                                                        <TextField
                                                            label='Email Address or User Name'
                                                            margin='normal'
                                                            fullWidth
                                                            name='email'
                                                            autoFocus
                                                            value={_get(value, 'email') || ''}
                                                            onChange={onChange}
                                                        />
                                                    </FormControl>
                                                );
                                            }}
                                        />
                                        <FormikErrorMessage error={_get(errors, 'email')} />
                                        <Button type='submit' fullWidth variant='contained' color='primary' className={classes.submit}>
                                            Reset Password
                                        </Button>
                                        <Typography style={{ marginTop: '20px' }}>
                                            * The email must match the email on record that we have for you in your Yeastman Classic account.
                                            If you do not know the email associated with your account, please contact us at&nbsp;
                                            <a href='mailto:info@whitelabs.com?subject=Forgot%20Password'>info@whitelabs.com</a> or 858-693-3441;
                                            we will ask for corroborating information for your account.
                                        </Typography>
                                    </Form>
                                );
                            }}
                        </Formik>
                    </CardBody>
                </Card>
            </main>
        </React.Fragment>
    );
}

const styles = theme => ({
    layout: {
        width: 'auto',
        display: 'block',
        marginTop: theme.spacing(7),
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        [theme.breakpoints.up(400 + (theme.spacing(3) * 2))]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto'
        },
        justifyContent: 'center'
    },
    logo: {
        alignContent: 'center',
        padding: '10'
    },
    form: {
        width: '100%', // Fix IE11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        marginTop: theme.spacing(3)
    },
    button: {
        margin: theme.spacing(1)
    }
});

ForgotPassword.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        messages: state.messages,
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({ ...userActions, ...messageActions }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(ForgotPassword));
