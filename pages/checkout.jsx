import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
import { orderActions } from 'appRedux/actions/orderActions';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Link from 'next/link';
import NavBarLayout from 'components/NavBar/NavBarLayout';
import Card from 'components/UI/Card/Card.jsx';
import CardBody from 'components/UI/Card/CardBody.jsx';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import CardHeader from 'components/UI/Card/CardHeader.jsx';
import CircularProgress from '@material-ui/core/CircularProgress';
import MobileStepper from '@material-ui/core/MobileStepper';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Banner from 'components/UI/Banner';

import Shipping from "components/Checkout/Shipping/Shipping";
import Billing from "components/Checkout/Billing/Billing";
import Items from "components/Checkout/Items/Items";
import Review from "components/Checkout/Review/Review";
import PONumber from "components/Checkout/Required/PONumber";
import RemovedItems from "components/Checkout/RemovedItems/RemovedItems";
import OrderPlaced from "components/Checkout/OrderPlaced/OrderPlaced";

import isLoggedUser from 'hocs/isLoggedUser';
import cartHasItems from 'hocs/cartHasItems';
import prepareOrder from 'hocs/prepareOrder';
import orderFinished from 'hocs/orderFinished';
import { cartActions } from 'appRedux/actions/cartActions';
import { messageActions } from "../redux/actions/messageActions";
import cloneDeep from "lodash/cloneDeep"
import Router from "next/router";
import moment from"moment"
// custom
import Alert from 'components/UI/Alert';
import ReactGA from 'react-ga';
import includes from "lodash/includes"
const steps = ["Shipping", "Billing", "Items/Ship Dates", "PO/Ordered By", "Review your order"];
let count=0;
var cloneOrders=[]

function getStepContent(step, errorOptEmail, errorOptEmailMsg, changeFunction) {
    switch (step) {
        case 0:
            return <Shipping somethingChanged={changeFunction} />;
        case 1:
            return <Billing somethingChanged={changeFunction} />;
        case 2:
            return <Items />;
        case 3:
            return <PONumber />;
        case 4:
            return <Review errorOptEmail={errorOptEmail} errorOptEmailMsg={errorOptEmailMsg} />;
        default:
            throw new Error('Unknown step');
    }
}
class Checkout extends Component {
    constructor(props) {
        super(props);

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/checkout');
        this.checkPaymentMethod=this.checkPaymentMethod.bind(this)
    }

    state = {
        activeStep: 0,
        isLoading: false,
        terms: false,
        confirmation: false,
        completed: {},
        couponCode: this.props.order.couponCode,
        isSessionExpired:false,
        errorOptEmail: false,
        progressError: null
    };

    handleOrder = () => {
        let email = this.props.order.optEmail
        let errorOptEmail = false
        let errorOptEmailMsg = "";

        if (email || this.props.order.hasOnDemandCourses) {
            // Only validate email if the order has on demand courses (to make sure an email has been entered)
            // or if an email has actually been specified
            let arr = email.split(';')
            let numEmails = email.split('@').length;

            if (this.props.order.hasOnDemandCourses && (arr.length > 1 || numEmails > 2)) {
                // Per Mike, on-demand courses can only be registered to a single email address
                document.getElementById("review-summary").scrollIntoView()
                errorOptEmail = true;
                errorOptEmailMsg = "* Orders for on-demand courses can only be registered to a single email address.";
            } else {
                var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                for (let i = 0; i < arr.length; i++) {
                    if (!re.test(arr[i])) {
                        document.getElementById("review-summary").scrollIntoView()
                        errorOptEmail = true;
                        errorOptEmailMsg = "* Please Enter Valid Email(s)";
                        break;
                    }
                }
            }
        }

        this.setState({ errorOptEmail, errorOptEmailMsg }, () => {
            if (!this.state.errorOptEmail) {
                this.setState({ terms: true });
            }
        })
    };

    handleTerms = () => {
        this.setState({ terms: false });
    };
   clearSession=()=>{
    delete localStorage.session
   }
    placeOrder = () => {
        this.setState({ terms: false });

        let a = moment(new Date());
        let b = moment(localStorage.getItem('session'));
        let diff = a.diff(b,'minute')
        if (diff >= 60) {
            this.clearSession()
            this.setState({
                isSessionExpired: true
            })
        } else {
            this.props.placeOrder();
            this.clearSession()
            this.setState({
                isSessionExpired: false
            })
        }
    };

    handleCloseTerms = () => {
        this.setState({ terms: false });
    };

    handleComplete = () => {
        const { completed } = this.state;
        completed[this.state.activeStep] = true;
        this.setState({ completed });
        this.handleNext();
    };

    handleNext = () => {
        const { activeStep } = this.state;
        if (this.okayToGoToStep(activeStep + 1)) {
            if (activeStep == steps.length - 1) {
                this.props.placeOrder();
            } else {
                if (activeStep == 1) this.applyCouponIfChanged();
                this.setState({ activeStep: activeStep + 1 });
            }
        }
    };

      checkDefaultCard = () => {
          const {user :{ card, cardsToRemove }} = this.props;          
          return cardsToRemove.filter(c => c.id === card.id).length === 1
      }

    handleBack = () => {
        if (this.props.user.isOnCreditHold) { // Return to store if customer is on credit hold
            Router.push('/');
        } else {
            const { activeStep } = this.state;
            this.setState({ activeStep: activeStep - 1 });
        }
    };

    handleReset = () => {
        this.setState({ activeStep: 0 });
    };

    handleStep = step => () => {
        if (this.props.user.isOnCreditHold) { // Return to store if customer is on credit hold
            Router.push('/');
        } else {
            if (this.okayToGoToStep(step)) {
                const { completed } = this.state;
                completed[this.state.activeStep] = true;
                if (!this.props.loading.isLoading) {
                    this.setState({ completed, activeStep: step });
                }//disabled
            } else {
                step--;
                while (step > 0) {
                    if (!this.okayToGoToStep(step, true)) {
                        step--;
                    } else {
                        const { completed } = this.state;
                        completed[this.state.activeStep] = true;
                        if (!this.props.loading.isLoading) {
                            this.setState({ completed, activeStep: step });
                        }//disabled
                        break;
                    }
                }
            }
        }
    };

    clearProgressError = () => {
        this.setState({ progressError: null });
    }

    okayToGoToStep = (stepTo, noMessage) => {
        // Keep US subsidiary end users on the Shipping step until items are loaded
        // This is to make sure the ship method doesn't change to the One Rate default after they leave this step
        // Updated 7/20/2020: Keep everyone in the US subsidiary on this page until items are loaded, due to restrictions on ship methods for beer & yeast
        var progressError = null;
        var okayToProceed = false;

        var shipMethodOkay = true;
        if (!this.props.user.shipmethod) {
            shipMethodOkay = false;
        } else if (this.props.user.shipMethods) {
            if (!this.props.user.shipMethods.find(x => x.NSID == this.props.user.shipmethod)) shipMethodOkay = false;
        }

        if (this.props.loading.isLoading && this.props.loading.type == 'prepareOrder'
        /*&& this.props.user.category == 3*/ && this.props.user.subsidiary == 2 && this.props.user.shipping.countryid == 'US') {
            progressError = null;
            okayToProceed = false;
        } else if (stepTo === steps.length - 3 && this.checkDefaultCard() && this.props.user.terms === "10") {
            if (!noMessage) {
                progressError = "Your default credit card is expired. Please enter or choose a new one before checking out.";
            }
            okayToProceed = false;
        } else if (stepTo > 0
        && (this.props.user.shipping.address1 == '' || this.props.user.shipping.city == '' || this.props.user.shipping.countryid == '' || (this.props.user.shipping.countryid == 'US' && this.props.user.shipping.zip == ''))) {
            if (!noMessage) {
                progressError = "Your shipping address is missing or incomplete. Please review and update your shipping information.";
            }
            okayToProceed = false;
        } else if (stepTo > 0 && (!this.props.user.shipmethod || !shipMethodOkay)) {
            if (!noMessage) {
                progressError = "Please select a ship method before proceeding.";
            }
            okayToProceed = false;
        } else if (stepTo > 1
        && (this.props.user.billing.address1 == '' || this.props.user.billing.city == '' || this.props.user.billing.countryid == '' || (this.props.user.billing.countryid == 'US' && this.props.user.billing.zip == ''))) {
            if (!noMessage) {
                progressError = "Your billing address is missing or incomplete. Please review and update your billing information.";
            }
            okayToProceed = false;
        } else {
            progressError = null;
            okayToProceed = true;
        }

        if (progressError) {
            if (!noMessage && (!this.state.progressError || this.state.progressError != progressError)) {
                this.setState({ progressError: progressError });
            }
            return false;
        } else {
            if (!noMessage && this.state.progressError) {
                this.setState({ progressError: null });
            }
            return okayToProceed;
        }
    }

    handleChangeCouponCode = event => {
        this.setState({ couponCode: event.target.value });
    };

    applyCouponIfChanged = () => {
        if (this.state.couponCode != this.props.cart.couponCode) {
            this.applyCoupon();
        }
    };

    applyCoupon = () => {
        this.props.setCouponCode({ couponCode: this.state.couponCode });
        this.props.prepareOrder();
        localStorage.setItem('session', new Date());
    }

    UNSAFE_componentWillMount() {
        if (this.props.user.isOnCreditHold) { // Return to store if customer is on credit hold
            Router.push('/');
        }
        if(!localStorage.getItem('session')){
            localStorage.setItem('session',new Date())
        }
    }
    handleCheckoutAgain = () => {
        this.setState({
            isSessionExpired: false
        })
        this.clearSession()
        Router.push("/");

    }
    componentWillUnmount() {
        const { order: { orderPlaced }, resetPlacedOrder } = this.props;
        if(orderPlaced) {
            resetPlacedOrder();
        }
    }
//check shipmethod exist is ShipMethod dropdown
    checkPaymentMethod() {
        const { user } = this.props
        //if shipping method is not selected return false else true
        return user.shipMethods.find(function (data) {
            return user.shipmethod == data.NSID
        }.bind(this))
    }

    render() {
        const { classes, order: { orderPlaced}, loading } = this.props;
        let { activeStep } = this.state;        
        if (this.props.order.items.length && count === 0) {
            cloneOrders = cloneDeep(this.props.order)
            count++
        }

        if (loading.type === 'orderComplete') {
            activeStep = steps.length;
        }                

        if (this.props.order && this.props.order.couponError) {
            var msg = this.props.order.couponError;
            this.props.order.couponError = null;
            this.props.showBanner({ content: { message: msg }, variant: "error" });
        }

        if (activeStep === 3 && this.props.order.isOnlyOnDemandCourses) {
            activeStep++;
        }

        const mustWait = !this.okayToGoToStep(activeStep + 1);

        if (this.props.user.isOnCreditHold) { // Return to store if customer is on credit hold
            Router.push('/');
            return null;
        } else {
            return (
                <NavBarLayout>
                    <LoadingIndicator visible={loading.isLoading && loading.type == 'placeOrder'} label={'Placing Order'} />
                    <div className={classes.container}>
                        <div className={classes.title}>
                            <Typography variant='h4' color='secondary'>
                                CHECKOUT
                        </Typography>
                            <Typography variant='h5' color='secondary'>
                                {loading.isLoading && loading.type == 'prepareOrder' && 'Yeastman is checking live inventory and preparing your shipping information.'}
                            </Typography>
                            <Typography variant='h6' color='secondary'>
                                {loading.isLoading && loading.type == 'prepareOrder' && 'For large orders, this may take a minute or two. Please be patient.'}
                            </Typography>
                            <Typography color='secondary'>
                                {
                                    (this.props.loading.isLoading && this.props.loading.type == 'prepareOrder' && this.props.user.category == 3 && this.props.user.subsidiary == 2 && this.props.user.shipping.countryid == 'US')
                                    && 'Depending on what you have ordered, available ship methods may change. Please take note of the ship method before proceeding.'
                                }
                            </Typography>
                            {loading.isLoading && loading.type == 'prepareOrder' && <CircularProgress size={30} color='secondary' />}
                        </div>

                        <Banner />
                        <RemovedItems />

                        <MobileStepper variant="dots" steps={5} position="static" activeStep={this.state.activeStep} className={classes.root} />
                        <Stepper nonLinear activeStep={activeStep} className={classes.stepper}>
                            {steps.map((label, index) => {
                                return (
                                    <Step key={label} className={label === 'Items' ? classes.step : ''}>
                                        <StepButton
                                            onClick={this.handleStep(index)} completed={this.state.completed[index]}
                                            disabled={(index > 1 && !this.props.user.billing.id) || (index > 3 && !this.props.order.ponumber && !this.props.order.isOnlyOnDemandCourses)}
                                        >
                                            <StepLabel>
                                                {label}
                                                {label === 'Items/Ship Dates' && loading.isLoading && loading.type == 'prepareOrder' && <CircularProgress size={10} />}
                                            </StepLabel>
                                        </StepButton>
                                    </Step>
                                );
                            })}
                        </Stepper>
                        <React.Fragment>
                            {this.state.progressError &&
                                <Typography style={{ color: 'red', fontWeight: 'bolder', margin: 'auto' }}>
                                    {this.state.progressError}
                                </Typography>
                            }
                            {orderPlaced ?
                                <OrderPlaced {...this.props} cloneOrders={cloneOrders} />
                                :
                                <React.Fragment>
                                    {getStepContent(activeStep, this.state.errorOptEmail, this.state.errorOptEmailMsg, this.clearProgressError)}
                                    <div style={activeStep == 1 ? { visible: 'true', display: 'block', textAlign: 'right', width: '100%' } : { visible: 'false', display: 'none' }}>
                                        Coupon Code:&nbsp;
                                    <input type='text' name='couponCode' value={this.state.couponCode} onChange={this.handleChangeCouponCode.bind(this)} />
                                        <Button onClick={this.applyCoupon} className={classes.button} style={{ position: 'relative', top: '-12px' }} >
                                            Apply
                                    </Button>
                                        <p style={{ fontSize: 'small' }}>
                                            Coupons will be reflected in 'My Orders' and<br />email confirmation following your order.
                                    </p>
                                    </div>
                                    <div className={classes.buttons}>
                                        {activeStep !== 0 && (
                                            <Button onClick={this.handleBack} className={classes.button}>
                                                Back
                                        </Button>
                                        )}
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            disabled={!this.checkPaymentMethod() || activeStep == 1 ? !this.props.user.billing.id : activeStep == 2 ? loading.isLoading && loading.type == "prepareOrder" : activeStep == 3 ? (this.props.order.ponumber == false && this.props.order.isOnlyOnDemandCourses) : false}
                                            onClick={activeStep === steps.length - 1 ? this.handleOrder : this.handleComplete}
                                            className={classes.button}
                                        >
                                            {mustWait && <CircularProgress size={20} />}
                                            {activeStep === steps.length - 1 ? 'Place order' : mustWait ? 'Loading' : 'Next'}
                                        </Button>
                                    </div>
                                </React.Fragment>
                            }

                        </React.Fragment>
                    </div>
                    <Dialog open={this.state.terms}>
                        <DialogTitle id='alert-dialog-title'>
                            Do you accept the{' '}
                            <a href='https://whitelabs.com/policy' target='_blank'>
                                White Labs Terms &amp; Conditions
                        </a>
                            ?
                    </DialogTitle>
                        <DialogContent />
                        <DialogActions>
                            <Button onClick={this.handleCloseTerms} color='primary'>
                                Decline
                        </Button>
                            <Button onClick={this.placeOrder} color='primary' autoFocus>
                                Accept
                        </Button>
                        </DialogActions>
                    </Dialog>
                    <Dialog open={this.state.confirmation}>
                        <DialogTitle id='alert-dialog-title'>Your order has been confirmed</DialogTitle>
                        <DialogContent />
                        <DialogActions>
                            <Link href='/'>
                                <Button color='primary' autoFocus>
                                    Continue
                            </Button>
                            </Link>
                        </DialogActions>
                    </Dialog>
                    <Dialog open={this.state.isSessionExpired}>
                        <DialogTitle id='alert-dialog-title'>Your Session has expired</DialogTitle>
                        <DialogTitle id='alert-dialog-title'>Checkout Again</DialogTitle>

                        <DialogContent />
                        <DialogActions>
                            <Link href='/'>
                                <Button color='primary' autoFocus onClick={this.handleCheckoutAgain}>
                                    OK
                            </Button>
                            </Link>
                        </DialogActions>
                    </Dialog>
                </NavBarLayout>
            );
        }
    }
}

const styles = theme => ({
    container: {
        marginTop: 50,
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(4),
        [theme.breakpoints.down('sm')]: {
            marginTop: 68,
        },
        [theme.breakpoints.up('md')]: {
            marginRight: 64
        },
        [theme.breakpoints.between('sm', 'md')]: {
            marginTop: 68,
            marginLeft: -14,
            marginRight: 50
          },
        [theme.breakpoints.up('lg')]: {
            marginLeft: 78,
            marginRight: 150
        },
        [theme.breakpoints.up('xl')]: {
            marginLeft: 178,
            marginRight: 250
        }
    },
    root: {
        justifyContent: 'center',
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none'
        }
    },
    title: {
        backgroundColor: '#FF9933',
        padding: 5,
        marginBottom: theme.spacing(4),
        textAlign: 'center',
        marginLeft: theme.spacing(-4),
        marginRight: theme.spacing(-4)
    },
    step: {
        width: '98px'
    },
    paper: {
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + (theme.spacing(3) * 2))]: {
            padding: theme.spacing(3)
        }
    },
    stepper: {
        padding: `${theme.spacing(3)}px 0 ${theme.spacing(5)}px`,
        backgroundColor: '#fafafa',
        display: 'flex',
        [theme.breakpoints.down('sm')]: {
            display: 'none'
        }
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1)
    }
});

Checkout.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        cart: state.cart,
        user: state.user,
        order: state.order
    };
};

const mapDispatchToProps = dispatch => bindActionCreators( { ...cartActions, ...orderActions, showSnackbar : messageActions.showSnackbar, showBanner: messageActions.showBanner }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(compose(withStyles(styles, { withTheme: true })(isLoggedUser(cartHasItems(prepareOrder(orderFinished(Checkout)))))));
