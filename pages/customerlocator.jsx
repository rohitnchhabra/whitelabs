import React, { Component } from 'react';
import { connect } from 'react-redux';

import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import NavBarUserSearchDrawerLayout from 'components/NavBar/NavBarUserSearchDrawerLayout';
import PageContainer from 'components/UI/PageContainer';
import Button from '@material-ui/core/Button';


class CustomerLocator extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        
        const { classes, theme} = this.props;

        return (
            <NavBarUserSearchDrawerLayout>
                <PageContainer heading='CUSTOMER LOCATOR' id='customer-locator'>


                </PageContainer>
            </NavBarUserSearchDrawerLayout>
        );
    }
}

const styles = theme => ({

    card: {
        position: 'relative',
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2)
    },

});
CustomerLocator.propTypes = {};

const mapStateToProps = state => {
    return {
        messages: state.messages
    };
};

export default connect(
    mapStateToProps,
    null
)(withStyles(styles, { withTheme: true })(CustomerLocator));
