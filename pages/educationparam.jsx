import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import FormButton from 'components/Form/FormButton';
import Router from 'next/router';
import { withRouter } from 'next/router';
import withInventory from 'hocs/inventory';
import { compose } from 'redux';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import Divider from '@material-ui/core/Divider';
import NavBarUserSearchDrawerLayout from 'components/NavBar/NavBarUserSearchDrawerLayout';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import LoadingIndicator from 'components/UI/LoadingIndicator';
import SalesLib from 'lib/SalesLib';
import { cartActions } from 'appRedux/actions/cartActions';
import ReactHtmlParser from 'react-html-parser'; 
import { DISALLOW_BACK_ORDERS } from "components/Store/constants";
import axios from 'axios';
import WLHelper from 'lib/WLHelper';

const customFormValidation = Yup.object().shape({
    quantity: Yup.string()
      .required('Required'),
  });

class Educationparam extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: '1',
            ques1: '',
            ques2: '',
            types: [],
            selectedType: '',
            errors: {},
            selectedOption: { option: '' }
        };
        const {
            router: { query }
        } = this.props;
        let { item } = query;
        this.filteredItem = this.props.inventory.items.find(v => v.partNum === item)
    }

    UNSAFE_componentWillMount() {
        if (this.filteredItem && this.filteredItem.volID.length > 1) {
            var types = [],
                selectedType,
                possibleTypes = [
                    { label: 'In person', value: 0 },
                    { label: 'Webinar', value: 1 }
                ];

            for (var i in this.filteredItem.volID) {
                if (this.filteredItem.volID[i] != null) {
                    types.push(possibleTypes[i]);
                }
            }

            selectedType = types[0].value;
            this.setState({ types, selectedType });
        }
    }

    componentDidMount() {
        if (this.filteredItem) {
            const { option } = this.filteredItem;
            if (option) {
                this.setState({
                    selectedOption: { option: option[0], index: 0 }
                })
            }
        }
    }

    handleErrors = (field, err) => {
        let {errors} = this.state;
        errors[field] = err
        this.setState({errors})
    }

    checkQuantity = item => {
        var quantity = parseFloat(item.OrderDetailQty);

        if (isNaN(quantity) || quantity <= 0) {
            this.handleErrors('quantity', 'Please enter a valid value for the quantity')
            return false;
        }

        //  Must be in increments of 1
        else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
            return false;
        }

        return true;
    };

    setOption = (e) => {
        let option = e.target.value
        let index = this.filteredItem && this.filteredItem.option.indexOf(option)
        this.setState({
            selectedOption: { option, index }
        })
    }

    addToCart = (values) => {
        var quantity = this.state.quantity;
        var ques1 = this.state.ques1;
        var ques2 = this.state.ques2;
        var item = this.filteredItem;
        const { selectedOption } = this.state;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(item.Name ? item.Name : item.partNum);
        cartItem.partNum = item.partNum;
        cartItem.MerchandiseID = item.volID[selectedOption.index];
        cartItem.salesCategory = parseInt(item.salesCategory);
        cartItem.type = 4;
        cartItem.details = '';
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.imageUrl = (item.imageUrl ? item.imageUrl : '/static/images/categories/Category-wine.jpg');

        cartItem.extraParams = {
            ques1: ques1,
            ques2: ques2
        }

        if (item.volID.length > 1) {
            switch (selectedOption.option) {
                case 'in-person':
                    cartItem.details = 'In person | ';
                    break;
                case 'webinar':
                    cartItem.details = 'Webinar | ';
                    break;
                default:
                    break;
            }
        }

        if (item.TagLocation || item.TagDate) {
            if (item.TagDate) {
                if (cartItem.details) cartItem.details += '\n';
                cartItem.details +=
                    'Class Date(s): ' +
                    item.TagDate;
            }
            if (item.TagLocation) {
                if (cartItem.details) cartItem.details += '\n';
                cartItem.details +=
                    'Class Location: ' +
                    item.TagLocation;
            }
        } 

        if (item.partNum.indexOf("WLEDU-WEB") >= 0 && !item.TagDate) {
            // On-demand class
            cartItem.isOnDemandCourse = true;
            if (cartItem.details) cartItem.details += '\n';
            cartItem.details += "On-Demand Course";
        }

        if (SalesLib.YeastEssentials.includes(cartItem.MerchandiseID)) {
            this.handleErrors('yeastEssentials','Attending Yeast Essentials? If you are considering or already attending Yeast Essentials 2.0, consider attending the 1 day Lab Practicum course that follows each Yeast Essentials course and allows you to apply the skills that you learn.')
        }

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    relItemArray[k].cartItemType = WLHelper.getYMO2TypeForItem(relatedItems[i].related[j], relItemArray[k]);
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                    })
                    .catch(error => {
                        console.log('error', error);

                        // Still add the item to the cart
                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                    });
            }
        }
    };

    setType = event => {
        this.setState({ selectedType: event.target.value });
    };

    change = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    changeQuantity = event => {
        this.setState({ quantity: event.target.value });
    };

    handleBack = () => {
        //set data in localstorage 
        //it indicated store page to loads previous data of page 
        localStorage.setItem("load", true)
        Router.back();
    }

    render() {
        const { classes, theme } = this.props;
        const item = this.filteredItem;
        const { errors } = this.state;

        var displayPrice = '';
        if (item && item.displayPrice) {
            if (item.displayPrice.length == 1) {
                displayPrice = '$' + item.displayPrice[0];
            } else {
                for (var i = 0; i < item.displayPrice.length; i++) {
                    if (displayPrice.length > 0) displayPrice += ', ';
                    if (item.option && i < item.option.length) {
                        displayPrice += item.option[i];
                        displayPrice += ': ';
                    }
                    displayPrice += '$';
                    displayPrice += item.displayPrice[i];
                }
            }
        }

        var itemDescription = item ? '<div style="max-height:480px; overflow-y: auto; overflow-x: hidden;">' + item.Description + '</div>' : '';
        itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

        return (
            <NavBarUserSearchDrawerLayout>
                 <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
            <Grid item xs={1} dir='ltr'>
                <FormButton className='back-param' text='Back' onClick={this.handleBack} />
            </Grid>
             <div className={classes.container}>
            <div className={classes.dispInline}>
                 <Grid
                    item
                    container
                    xs
                    className={classes.displayMargin}
                    direction={'row'}
                >
                    <Grid item style={{display:'flex'}}>
                    <Typography variant='h5' className={classes.titleMargin}>
                    {this.filteredItem && this.filteredItem.Name}
                        <Divider variant='middle' />
                        </Typography>
                    </Grid>
                </Grid>
                <Grid item container direction={'row'} spacing={2} justify='center' style={{marginBottom:'15px'}}>
                {this.filteredItem && this.filteredItem.TagLocation ?
                        <Grid item xs={12} md={6} >
                            <div style={{ display: 'flex', justifyContent:'center' }}>
                                <Typography>Class Location: </Typography>
                                &nbsp;
                                <Typography
                                    style={{
                                        color: '#66CCCC'
                                    }}
                                >
                                    { this.filteredItem.TagLocation}
                                </Typography>
                            </div>
                        </Grid> :null}
                        {this.filteredItem && this.filteredItem.TagDate? <Grid item xs={12} md={6}>
                            <div style={{ display: 'flex', justifyContent:'center' }}>
                                <Typography>Date: </Typography>
                                &nbsp;
                                <Typography
                                    style={{
                                        color: '#66CCCC'
                                    }}
                                >
                                    {this.filteredItem && this.filteredItem.TagDate}
                                </Typography>
                            </div>
                        </Grid>:null}
                    </Grid>

                <Grid
                    item
                    container
                    direction={'column'}
                    spacing={6}
                    className={classes.description}
                >
                    {this.filteredItem && this.filteredItem.Description ?
                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={6}
                        className={classes.description}
                    >
                        <Grid item>
                            <div>{ReactHtmlParser(itemDescription)}</div>
                        </Grid>
                    </Grid> : null
                    }
                    <Typography style={{ fontSize: 'smaller', fontWeight: 'bolder', marginBottom: '25px', marginLeft: 'auto', marginRight: 'auto' }} dangerouslySetInnerHTML={{ __html: displayPrice }} />
                    {
                        this.filteredItem && this.filteredItem.outofstockmessage &&
                        <Grid item>
                            <Typography style={{ paddingTop: '10px', fontWeight: 'bold' }}>
                                {this.filteredItem.outofstockmessage}
                            </Typography>
                        </Grid>
                    }
                </Grid>
                        {this.filteredItem && this.filteredItem.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                            <Grid item container spacing={6} style={{ marginTop: 5 }} direction={'row'}>
                                <Formik
                                    initialValues={this.state}
                                    validationSchema={customFormValidation}
                                    onSubmit={values => this.addToCart(values)}
                                >
                                    {({ values, handleChange }) => {
                                        return (
                                            <Form className={classes.form}>
                                                {errors.quantity && <div className='error'>{errors.quantity}</div>}
                                                <Grid item xs container spacing={6} direction={'row'} justify='center'>
                                                    <Grid item className='flex-center'>
                                                        <TextField id='quantity' label='Quantity' className='flex-center' className={classes.quantity} value={this.state.quantity} onChange={this.changeQuantity} type='number' />
                                                    </Grid>
                                                    {this.filteredItem && this.filteredItem.option &&
                                                        <Grid item xs={12} sm={4} md={4} className={classes.formFields}>
                                                            <FormControl>
                                                                <InputLabel>Class&nbsp;Type</InputLabel>
                                                                <Select
                                                                    value={this.state.selectedOption.option}
                                                                    onChange={this.setOption}
                                                                >
                                                                {this.filteredItem.option.map(
                                                                        (curr, i) => (

                                                                            <MenuItem
                                                                                key={i}
                                                                                value={curr}
                                                                            >
                                                                                {curr == 'in-person' ? 'In Person' : 'Webinar'}
                                                                            </MenuItem>
                                                                        )
                                                                    )}
                                                                </Select>
                                                            </FormControl>
                                                        </Grid>
                                                    }
                                                    {this.filteredItem.partNum.indexOf("WLEDU-WEB") >= 0 ? null :
                                                        <Grid>
                                                            <Grid item xs={12}>
                                                                <Typography variant="subheading" style={{ display: 'flex', justifyContent: 'center' }}>
                                                                    How Did You hear about this class ?
                                                        </Typography>
                                                            </Grid>

                                                            <Grid item>
                                                                <TextField
                                                                    InputProps={{
                                                                        classes: {
                                                                            input: classes.textField,
                                                                        }
                                                                    }}
                                                                    multiline
                                                                    rowsMax="4"
                                                                    id="ques1"
                                                                    name="ques1"
                                                                    label=""
                                                                    variant="outlined"
                                                                    value={this.state.ques1}
                                                                    onChange={e => this.change(e)}
                                                                />
                                                            </Grid>

                                                            <Grid item xs={12}>
                                                                <Typography variant="subheading" style={{ display: 'flex', justifyContent: 'center' }}>
                                                                    Please Mention the attendee Name.
                                                        </Typography>
                                                            </Grid>

                                                            <Grid item >
                                                                <TextField
                                                                    InputProps={{
                                                                        classes: {
                                                                            input: classes.textField,
                                                                        }
                                                                    }}
                                                                    id="ques2"
                                                                    name="ques2"
                                                                    label=""
                                                                    variant="outlined"
                                                                    value={this.state.ques2}
                                                                    onChange={e => this.change(e)}
                                                                />
                                                            </Grid>
                                                        </Grid>
                                                    }
                                                    <Grid item container spacing={6} className='flex-center'>
                                                        <Grid item>
                                                            <div className={classes.addButton}>
                                                                <Button
                                                                    type='submit'
                                                                    variant='contained'
                                                                    color='primary'
                                                                    className={classes.button}
                                                                >
                                                                    Add to Cart
                                                    </Button>
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </Form>
                                        );
                                    }}
                                </Formik>
                            </Grid>
                        }
                    </div>
                </div>
            </NavBarUserSearchDrawerLayout>
        );
    }
}

const styles = theme => ({
    displayMargin:{
        display: 'flex',
        justifyContent:'center',
        marginTop: 10,
        marginBottom: 20,
        maxWidth:'100%',
        [theme.breakpoints.down('sm')]: {
            maxWidth:'100%',
        },
    },
    backbtn:{
        minHeight: '20px !important',
        padding: '4px !important',
        fontSize: '12px !important',
        backgroundColor:'#f28411 !important',
        fontWeight:'bold !important',
        marginLeft: '37px !important',
        marginTop:'65px !important'

},
dispInline:{
    display:'inline'
},
container: {
    marginTop: 40,
    width:'60%',
    border: 'solid 1px',
    borderColor: '#CCCCCC',
    textAlign:'center',
    display:'flex',
    justifyContent:'center',
    margin:'0 auto',


    padding: theme.spacing(4),
    [theme.breakpoints.down('sm')]: {
        width:'100%',
    },
    [theme.breakpoints.up('md')]: {
        marginLeft:'auto',
        marginRight: 'auto'
    },
    [theme.breakpoints.up('lg')]: {
        marginLeft: 'auto',
        marginRight: 'auto'
    },
    [theme.breakpoints.up('xl')]: {
        marginLeft: 'auto',
        marginRight: 'auto'
    }
},
description: {
    textAlign: 'justify',
    marginTop:10

},
     quantity: {
         width: 50
     },

    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop:'15px',
        marginBottom:'15px'
    },
    button: {
        // marginTop: theme.spacing(1),

    },
    addButton: {
        display: 'flex',
        justifyContent: 'center',
        // marginLeft: '40px',
        marginBottom:'20px'
    },
    form: {
        width: '100%',
        display:'flex',
        justifyContent:'center'
    },
    titleMargin: {
        marginTop: '30px',
        marginBottom: '5px',
        textAlign: 'center',
        [theme.breakpoints.down('sm')]: {
            fontSize: 'smaller'
        }
    }
});



const mapStateToProps = state => {
    return {
        user: state.user,
        inventory: state.inventory
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(cartActions, dispatch);

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(compose(withStyles(styles, { withTheme: true })(withInventory(Educationparam))))
);
