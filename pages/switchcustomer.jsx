import React, { Component } from 'react';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import NavBarUserSearchDrawerLayout from 'components/NavBar/NavBarUserSearchDrawerLayout';
import PageContainer from 'components/UI/PageContainer';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import SwitchCustomerOption from 'components/Admin/SwitchCustomerOption';
import { adminActions } from 'appRedux/actions/adminActions';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import axios from 'axios';
import Router from 'next/router';

class SwitchCustomer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            customerName: '',
            customerOptions: [],
            isLoading: false,
            searchError: '',
            userInfo: null
        }
        this.searchForCustomer = this.searchForCustomer.bind(this);

        let userInfo = JSON.parse(localStorage.getItem('userInfo'));
        if (!userInfo || !userInfo.isStaff) {
            // Return to store if not WL staff
            Router.push('/');
        }
    }

    searchForCustomer = () => {
        var customerName = this.state.customerName;

        if (customerName == '') {
            this.setState({ searchError: 'Please enter a customer name or account number.' });
        } else {
            this.setState({ isLoading: true });
            axios.post('/request-customer-lookup', { custName: customerName })
                .then(({ data: { customers, error } }) => {
                    if (error) throw error;
                    //console.log(customers);
                    if (customers) {
                        this.setState({ customerOptions: customers, searchError: '' });
                    } else {
                        this.setState({ customerOptions: [], searchError: '' });
                    }
                    this.setState({ isLoading: false });
                })
                .catch(error => {
                    //console.log('error', error);
                    this.setState({ customerOptions: [], searchError: error.message });
                })
                .finally(() => {
                    this.setState({ isLoading: false });
                });
        }
    }

    changeCustomerName = event => {
        this.setState({ customerName: event.target.value });
    };

    render() {
        const { classes, theme } = this.props;
        const { customerOptions } = this.state;
        const userInfo = JSON.parse(localStorage.getItem('userInfo'));

        return (
            <>
                {!userInfo || !userInfo.isStaff ? null :
                    <NavBarUserSearchDrawerLayout>
                        <PageContainer heading='SWITCH CUSTOMER' id='switch-customer'>
                            <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                            <Paper className={classes.root}>
                                <InputBase
                                    className={classes.input}
                                    placeholder="Search Customers"
                                    inputProps={{ 'aria-label': 'Search Customers' }}
                                    value={this.state.customerName}
                                    onChange={this.changeCustomerName}
                                />
                                <IconButton className={classes.iconButton} aria-label="Search" onClick={this.searchForCustomer}>
                                    <SearchIcon />
                                </IconButton>
                            </Paper>

                            <Grid container spacing={6} style={{ marginTop: 20 }}>
                                {!this.state.searchError ? null :
                                    <Typography style={{ fontWeight: 'bold', color: 'red' }}>
                                        {this.state.searchError}
                                    </Typography>
                                }
                                {customerOptions && customerOptions.map((customerOption) => (
                                    <SwitchCustomerOption customerName={customerOption.name} customerId={customerOption.id} />
                                ))}
                            </Grid>
                        </PageContainer>
                    </NavBarUserSearchDrawerLayout>
                }
            </>
        );
    }
}

const styles = theme => ({

    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
    },
    input: {
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    card: {
        position: 'relative',
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2)
    },
    image: {
        width: 170,
        marginRight: 10,
        textAlign: 'center'
    },


});
SwitchCustomer.propTypes = {};

const mapStateToProps = state => {
    return {
        messages: state.messages
    };
};

export default connect(
    mapStateToProps,
    null
)(withStyles(styles, { withTheme: true })(SwitchCustomer));
