import { formValueSelector } from 'redux-form';


export const REPORT_FORM = 'reportDataForm';
const selector = formValueSelector(REPORT_FORM);

export const lotNumberSelector = (state) => selector(state, 'lotNumber');
export const packageTypeSelector = (state) => selector(state, 'packageType');

export const isReportDataSelector = (state) => state.report.isReportData;
export const isNoResultsFoundSelector = (state) => state.report.isNoResultsFound;