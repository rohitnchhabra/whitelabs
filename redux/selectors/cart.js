export const cart = state => state.cart;

export const isLoadingSelector = state => cart(state).isLoading;
export const availabilityCPHSelector = state => cart(state).availabilityCPH;
export const availabilityHKSelector = state => cart(state).availabilityHK;
export const availabilitySDSelector = state => cart(state).availabilitySD;
export const isDialogCloseSelector = state => cart(state).isDialogClose;
