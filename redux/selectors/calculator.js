import { formValueSelector } from 'redux-form';

export const CALCULATOR = 'calculator';

const selector = formValueSelector(CALCULATOR);
export const enzymeSelector = (state) => selector(state, 'enzymeType');
export const wortVolumeSelector = (state) => selector(state, 'wortVolume');
export const beerTypeSelector = (state) => selector(state, 'beerType');
export const homebrewTypeSelector = (state) => selector(state, 'homebrewType');
export const wortGravitySelector = (state) => selector(state, 'wortGravity');
export const maltValueSelector = (state) => selector(state, 'maltValue');

export const calcResultSelector = (state) => state.calculator.calcResult;
