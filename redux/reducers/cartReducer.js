import { createReducer } from 'helpers/reduxHelpers';
import { cartTypes } from 'appRedux/actions/cartActions';

export const initialState = {
    items: [],
    rehydrated: false,
    isLoading: false,
    isDialogClose: false,
    availabilitySD: null,
    availabilityHK: null,
    availabilityCPH: null,
    availability: null,
    couponCode: ''
};

export default createReducer(initialState, {
    [cartTypes.ADD_ITEM_SUCCESS]: (state, { data: { items } }) => ({
      ...state,
        items
    }),
    [cartTypes.UPDATE_ITEM_SUCCESS]: (state, { data: { items } }) => ({
      ...state,
        items
    }),
    [cartTypes.REMOVE_ITEM_SUCCESS]: (state, { data: { items } }) => ({
      ...state,
        items
    }),
    [cartTypes.REPLACE_ITEM_SUCCESS]: (state, { data: { items } }) => ({
      ...state,
        items
    }),
    [cartTypes.CLEAR_CART_SUCCESS]: (state, { data }) => ({
      ...state,
        items: []
    }),
  [cartTypes.ADD_TO_CART_SUCCESS]: (state) => ({
    ...state,
    isLoading: false,
  }),
  [cartTypes.ADD_TO_CART_ATTEMPT]: (state) => ({
    ...state,
    isLoading: true,
  }),
  [cartTypes.ADD_TO_CART_FAILURE]: (state) => ({
    ...state,
    isLoading: false,
  }),
  [cartTypes.DIALOG_CLOSE_ATTEMPT]: (state, data) => ({
    ...state,
    isDialogClose: data.data,
  }),
  [cartTypes.CHECK_AVAILABILITY_ATTEMPT]: (state) => ({
    ...state,
    isLoading: true,
  }),
  [cartTypes.CHECK_AVAILABILITY_SUCCESS]: (state, data) => ({
    ...state,
    ...data,
    isLoading: false,
  }),
  [cartTypes.CHECK_AVAILABILITY_FAILURE]: (state) => ({
    ...state,
    isLoading: false,
  }),
  [cartTypes.SET_COUPON_CODE_SUCCESS]: (state, { data: { couponCode } }) => ({
    couponCode
  })
})
