import { createReducer } from "helpers/reduxHelpers";
import { safeExecute } from "helpers/utils";
import { userTypes } from "appRedux/actions/userActions";

/* ------------- Initial State ------------- */

const initialState = {
  id: null,
  subsidiary: "",
  shipmethod: "",
  shipMethods: [],
  subsidiaryOptions: [],
  username: "",
  password: "",
  cardsToRemove: [],
  category: "",
  companyName: "",
  connectedaccounts: [],
  email: "",
  phone: "",
  terms: "",
  vat: "",
  currency: "",
  currencyOptions: [],
  accountNumber: "",
  shipping: {
    address1: "",
    address2: "",
    address3: "",
    addressee: "",
    attn: "",
    city: "",
    countryid: "",
    zip: ""
  },
  billing: {
    address1: "",
    address2: "",
    address3: "",
    addressee: "",
    attn: "",
    city: "",
    countryid: "",
    zip: ""
  },
  otherAddresses: [],
  card: {
    id: "",
    ccnumber: "",
    ccname: "",
    ccexpire: "",
    type: "",
    default: false
  },
  otherCards: [],
  isLoading: false,
  isSuccess: false,
  orderHistory: [],
  isUnsaved: false
};

/* ------------- Hookup Reducers To Types ------------- */

export default createReducer(initialState, {
  [userTypes.USER_LOGIN_FAILURE]: (state, { data }) => {
    if (!data.loading) {      
      return { changeError: data.error, isLoading: false };
    } else {
      return null;
    }
  },
  [userTypes.USER_LOGIN_ATTEMPT]: (state, { data }) => ({
    isLoading: true
  }),
  [userTypes.SET_USER_INFO_SUCCESS]: (state, { data: userInfo }) => ({
    ...userInfo, isLoading: false
    // isPasswordChangeRequired: true
  }),
  [userTypes.UPDATE_USER_INFO_ATTEMPT]: (state, { data }) => ({
    isLoading: true
  }),
  [userTypes.UPDATE_USER_INFO_FAILURE]: (state, { data }) => ({
    isLoading: false
  }),
  [userTypes.UPDATE_USER_INFO_SUCCESS]: (state, { data }) => ({
    isLoading: false
  }),
  [userTypes.CREATE_USER_ATTEMPT]: state => ({
    isLoading: true
  }),
  [userTypes.ADD_SUBSIDIARY_ATTEMPT]: state => ({
    isLoading: true
  }),
  [userTypes.ADD_SUBSIDIARY_SUCCESS]: state => ({
    isLoading: false
  }),
  [userTypes.ADD_SUBSIDIARY_FAILURE]: state => ({
    isLoading: false
  }),
  [userTypes.GET_USER_INFO_ATTEMPT]: (state, { data }) => ({
    isLoading: true
  }),
  [userTypes.GET_USER_INFO_FAILURE]: (state, { data }) => ({
    isLoading: false
  }),
  [userTypes.GET_USER_INFO_SUCCESS]: (state, { data }) => ({
    isLoading: false
  }),
  [userTypes.UNSAVED_USER_INFO_ATTEMPT]: (state, { data }) => ({
    isUnsaved: true
  }),
  [userTypes.UNSAVED_USER_CLOSE_ATTEMPT]: (state, { data }) => ({
    isUnsaved: false
  }),
  [userTypes.SET_CREDIT_CARD_SUCCESS]: (state, { data: { creditCard } }) => ({
    card: creditCard
  }),
  [userTypes.SET_SHIP_ADDRESS_SUCCESS]: (state, { data: { address } }) => ({
    address
  }),
  [userTypes.SET_BILL_ADDRESS_SUCCESS]: (state, { data: { address } }) => ({
    address
  }),
  [userTypes.SET_SHIP_METHOD_SUCCESS]: (state, { data: { shipmethod } }) => ({
    shipmethod
  }),
  [userTypes.GET_ORDER_HISTORY_ATTEMPT]: (state, { data }) => ({
    isLoading: true
  }),
  [userTypes.GET_ORDER_HISTORY_FAILURE]: (state, { data }) => ({
    isLoading: false
  }),
  [userTypes.GET_ORDER_HISTORY_SUCCESS]: (
    state,
    { data: { orderHistory } }
  ) => ({
    isLoading: false,
    orderHistory
  }),
  [userTypes.FORGOT_PASSWORD_ATTEMPT]: (state, { data }) => ({
    isLoading: true
  }),
  [userTypes.FORGOT_PASSWORD_FAILURE]: (state, { data }) => ({
    isLoading: false
  }),
  [userTypes.FORGOT_PASSWORD_SUCCESS]: state => ({
    isLoading: false
  }),
  [userTypes.CHANGE_PASSWORD_ATTEMPT]: (state, { data }) => ({
    isLoading: true
  }),
  [userTypes.CHANGE_PASSWORD_FAILURE]: (state, { data }) => ({
    isLoading: false,
    isPasswordChangeRequired: true

  }),
  [userTypes.CHANGE_PASSWORD_SUCCESS]: state => ({
    isLoading: false,
    isPasswordChangeRequired: false

  }),
  [userTypes.CREATE_USER_ATTEMPT]: (state, { data }) => ({
    registrationAttempt: true
  }),
  [userTypes.CREATE_USER_SUCCESS]: (state, { data }) => ({
    registrationStatus: "success",
    isSuccess: true,
    isLoading: false
  }),
  [userTypes.CREATE_USER_FAILURE]: (state, { data }) => ({
    registrationStatus: "failed",
    isLoading: false
  }),
  [userTypes.USER_LOGOUT_ATTEMPT]: () => {
    if (window.location.pathname === "/myaccount") {
      // window.location.pathname="/login"
    }
    localStorage.clear();
    return null;
  }, // this will return initial state
  [userTypes.ADD_FEEDBACK_ATTEMPT]: state => ({
    isSuccess: false
  }),
  [userTypes.ADD_FEEDBACK_SUCCESS]: state => ({
    isSuccess: true
  }),
  [userTypes.ADD_FEEDBACK_FAILURE]: state => ({
    isSuccess: false
  }),
  [userTypes.CHANGE_PAY_TERMS_ATTEMPT]: (state, { data }) => ({
    terms: data
    }), 
    [userTypes.RECORD_SURVEY_RESPONSES_ATTEMPT]: state => ({
        isSuccess: false
    }),
    [userTypes.RECORD_SURVEY_RESPONSES_SUCCESS]: state => ({
        isSuccess: true
    }),
    [userTypes.RECORD_SURVEY_RESPONSES_FAILURE]: state => ({
        isSuccess: false
    }),
    [userTypes.CHECK_CUSTOMER_HOLD_STATUS_ATTEMPT]: state => ({
        isCheckingCustomer: true
    }),
    [userTypes.CHECK_CUSTOMER_HOLD_STATUS_SUCCESS]: state => ({
        isCheckingCustomer: false
    }),
    [userTypes.CHECK_CUSTOMER_HOLD_STATUS_FAILURE]: state => ({
        isCheckingCustomer: false
    })
});
