import { createReducer } from 'helpers/reduxHelpers';
import { adminTypes } from 'appRedux/actions/adminActions';

/* ------------- Initial State ------------- */

const initialState = {
    customers: []
};

/* ------------- Hookup Reducers To Types ------------- */

export default createReducer(initialState, {
    [adminTypes.GET_CUSTOMER_ATTEMPT]: (state, { data }) => ({
        
    }),
    [adminTypes.GET_CUSTOMER_FAILURE]: (state, { data }) => ({
        
    }),
    [adminTypes.GET_CUSTOMER_SUCCESS]: (state, { data: { customerResults } }) => ({
        customerResults
    }),
});