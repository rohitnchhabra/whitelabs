import { createReducer } from 'helpers/reduxHelpers';
import { orderTypes } from 'appRedux/actions/orderActions';

const initialState = {
    items: [],
    transitTimes: [],
    shipMethod: '',
    ponumber:"",
    itemSub: 0,
    shippingSub: 0,
    orderSub: 0,
    shippingOptions: ['Ship All Together', 'Earliest For Each', 'Custom'],
    selectedShippingOption: 'Earliest For Each',
    removedItems: [],
    orderPlaced: false,
    extraParams: '',
    orderPlacedData: [],
    getItSooner:false,
    optEmail:""
};

/* ------------- Hookup Reducers To Types ------------- */

export default createReducer(initialState, {
    [orderTypes.PREPARE_ORDER_SUCCESS]: (state, { data }) => ({
        ...data,
        orderPlaced: false
    }),
    [orderTypes.PLACE_ORDER_SUCCESS]: (state, { data }) => ({
        ...initialState,
        orderPlaced: true,
        orderPlacedData: data
    }),
    [orderTypes.SET_ITEMS_ATTEMPT]: (state, { data: { items } }) => ({
        ...state,
        items
    }),
    [orderTypes.SET_SHIPPING_OPTION_SUCCESS]: (state, { data: {option,getItSooner} }) => ({
        ...state,
        selectedShippingOption: option,
        getItSooner
    }),
    [orderTypes.SET_PONUMBER_ATTEMPT]: (state, { data: ponumber } ) => ({
        ponumber
    }),
    [orderTypes.RESET_PLACED_ORDER_ATTEMPT]: (state, { data: { items } }) => ({
        orderPlaced: false        
    }),
    [orderTypes.RESET_REMOVED_ITEM_SUCCESS]:(state,{data})=>({
        ...data,
        removedItems:[]
    }),
    [orderTypes.SET_OPTIONAL_EMAIL_SUCCESS]:(state,{data})=>({
        optEmail:data
    }),
    [orderTypes.RESEND_INVOICE_ATTEMPT]: (state, { data }) => ({
        
    })
});
