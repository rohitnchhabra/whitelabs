import { createReducer } from 'helpers/reduxHelpers';
import { messageTypes } from 'appRedux/actions/messageActions';
import { finished } from 'stream';
import { find } from 'lodash';
import { userTypes } from "appRedux/actions/userActions";

const initialState = {
    banner: [],
    snackbar: [],
    showChangePassword:false
};

export default createReducer(initialState, {
    [messageTypes.SHOW_BANNER_SUCCESS]: (state, { data: { banner } }) => {
        return (
            {   snackbar: state.snackbar,
                banner
             }
        );
    },
    [messageTypes.SHOW_BANNER_FAILURE]: (state, data) => {
        return state;
    },
   
    [messageTypes.SHOW_SNACKBAR_SUCCESS]: (state, { data: { snackbar } }) => {
        return (
            {    banner: state.snackbar,
                 snackbar
             }
        );
    },
    [messageTypes.SHOW_SNACKBAR_FAILURE]: (state, data) => {
        return state;
    },
    [messageTypes.HIDE_SNACKBAR_ATTEMPT]: ({ snackbar }, { data: idx }) => ({
        snackbar: snackbar.filter((content, index) => content.index !== idx.index)
    }),
    [messageTypes.HIDE_BANNER_ATTEMPT]: ({ banner }, { data: idx }) => ({
        banner: banner.filter((content, index) => content.index !== idx.index)
    }),
    [messageTypes.SHOW_CHANGE_PASSWORD_MODAL_SUCCESS]: (state,{data}) => ({
        showChangePassword:data.showChangePassword
    }),
    [userTypes.CHANGE_PASSWORD_SUCCESS]: state => ({
        showChangePassword:false
    
      }),
})

