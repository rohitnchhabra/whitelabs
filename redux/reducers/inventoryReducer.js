import { createReducer } from 'helpers/reduxHelpers';
import { inventoryTypes } from 'appRedux/actions/inventoryActions';

const initialState = {
    items: [],
    itemsToShow: [],
    category: 1,
    error: null,
    isHomebrew: false,
    availability: {},
    pageData: null,
    isSimilarLoading: null,
    isAlternateLoading:true,
    alternateSizes: [],
    similarStrains: [],
    cartIndex: null
};

export default createReducer(initialState, {
    [inventoryTypes.GET_INVENTORY_SUCCESS]: (state, { data: { items, welcomeMessage }}) => ({
        items, welcomeMessage
    }),
    [inventoryTypes.TOGGLE_HOMEBREW_SUCCESS]: (state, { data: { isHomebrew } }) => ({
        isHomebrew,
    }),
    [inventoryTypes.SET_PAGE_DATA_ATTEMPT]: (state, pageData) => ({
        pageData
    }),
    [inventoryTypes.GET_ALTERNATE_SIZES_ATTEMPT]: (state, data) => ({
        isAlternateLoading: true,
        alternateSizes:[]
    }),
    [inventoryTypes.GET_ALTERNATE_SIZES_SUCCESS]: (state, {data: { alternateSizes, cartIndex }}) => ({
        isAlternateLoading: false,
        alternateSizes,
        cartIndex
    }),
    [inventoryTypes.GET_ALTERNATE_SIZES_FAILURE]: (state, data) => ({
        isAlternateLoading: false,
        alternateSizes:[]
    }),
    [inventoryTypes.GET_SIMILAR_STRAINS_ATTEMPT]: (state, data ) => ({
        isSimilarLoading: true,
        similarStrains:[]
    }),
    [inventoryTypes.GET_SIMILAR_STRAINS_SUCCESS]: (state, { data: { alternateStrains, cartIndex }}) => ({
        isSimilarLoading: false,
        similarStrains:alternateStrains,
        cartIndex
    }),
    [inventoryTypes.GET_SIMILAR_STRAINS_FAILURE]: (state, data) => ({
        isSimilarLoading: false,
        similarStrains:[]
    }),
    [inventoryTypes.CLEAR_ALTERNATE_SIZES_ATTEMPT]: (state, data) => ({
        alternateSizes:[]
    }),
    [inventoryTypes.CLEAR_SIMILAR_STRAINS_ATTEMPT]: (state, data) => ({
        similarStrains:[]
    }),
    [inventoryTypes.GET_RELATED_ITEMS_ATTEMP]: (state, data) => ({
        isSimilarLoading: true,
        similarStrains: []
    }),
    [inventoryTypes.GET_RELATED_ITEMS_SUCCESS]: (state, { data: { cartItem } }) => ({
        cartItem
    }),
    [inventoryTypes.GET_RELATED_ITEMS_FAILURE]: (state, data) => ({
        
    }),
});
