import { createReducer } from 'helpers/reduxHelpers';
import { reportTypes } from '../actions/reportActions';

export const initialState = {
    reportData: null,
    isReportData: false,
};

export default createReducer(initialState, {
    [reportTypes.GET_REPORT_SUCCESS]: (state, { data: { reportData } }) => ({
        reportData,
        isReportData: false,
        isNoResultsFound: !reportData
    }),
    [reportTypes.GET_REPORT_ATTEMPT]: () => ({
        isReportData: true
    }),
    [reportTypes.GET_REPORT_FAILURE]: () => ({
        isReportData: false
    })
})
