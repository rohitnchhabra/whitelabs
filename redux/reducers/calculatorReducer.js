import { createReducer } from 'helpers/reduxHelpers';
import { calculatorTypes } from 'appRedux/actions/calculatorActions';

export const initialState = {
    calcResult: null
};

export default createReducer(initialState, {
    [calculatorTypes.CALCULATE_SUCCESS]: (state, { data : { calcResult } }) => ({
        calcResult
    })
})
