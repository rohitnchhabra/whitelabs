import { put, call, select } from 'redux-saga/effects';
import _isNumber from 'lodash/isNumber';
import _isEmpty from 'lodash/isEmpty';
import _find from 'lodash/find';
import _get from 'lodash/get';

import { userActions } from 'appRedux/actions/userActions';
import { messageActions } from 'appRedux/actions/messageActions';
import { orderActions } from 'appRedux/actions/orderActions';

import * as api from 'services/user.js';
import { prepareUserInfo } from 'lib/UserUtils.js';
import Utils from 'lib/Utils.js';

import WLHelper from 'lib/WLHelper';

export function* loginUser(action) {    
    const {
        responseSuccess,
        responseFailure,
        data: { username, password, changePassword, newPassword }
    } = action;
    try {
        if (changePassword) {
            yield put(userActions.changePassword({ currentPassword: password, newPassword }));
            yield put(messageActions.showBanner({ content: { title: 'Success', message: 'Your password has been changed.' }, variant: 'success' }));
        } else {
            let { res: { userID, changePasswordRequired, error, isStaff } } = yield call(api.login, username, password);

            if (error) throw error;
            if (error && error.code === 0) {
                yield put(
                    messageActions.showBanner({
                        content: {
                            title: 'Yeastman',
                            message: error.message
                        },
                        variant: 'error'
                    })
                );
                yield put(messageActions.showBanner({ content: error, variant: 'error' }))
            }

            if (!_isEmpty(userID)) {
                userID = Number(userID);
                if (userID == 0 && isStaff) userID = -999;
                yield put(userActions.getUserInfo({ userID, isLogin: true, username, changePasswordRequired }));
            } else {
                yield put(messageActions.showBanner({ content: { message: 'Something went wrong', variant: 'error' } }))
            }
        }
    } catch (error) {                
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        }
        else if (error) {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        } else {
            yield put(messageActions.showBanner({ content: 'An error occurred. Please check your user name and password and try again.', variant: 'error' }))
        }
        if(changePassword) {            
            yield put(responseFailure({error,loading:false}));

        }else{
            yield put(responseFailure(error));

        }

    }
}

export function* getUserInfo(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { userID, isLogin, username }
    } = action;
    try {
        var { res } = (userID == -999 ? { res: { isStaff: true } } : yield call(api.getUserInfo, { userID }));

        if (res.error) {
            throw res.error;
        } else if (res.isStaff) {
            res.companyName = "White Labs Staff";
            res.currency = "1";
            res.subsidiary = "2";
            res.id = userID;
            res.shipping = {};
            res.shipping.id = 0;
        }

        var forcePasswordChange = (action.data.changePasswordRequired && action.data.changePasswordRequired == 'Yes');
        res.isLogin = isLogin;
        if (username) res.username = username;

        res.isPasswordChangeRequired = forcePasswordChange;

        yield put(userActions.setUserInfo({ userInfo: res }));

        var extraTimeout = 0;
        var loginMessage = '';
        if (isLogin) {
            loginMessage = 'You have successfully logged in!';
            if (res.subsidiaryOptions && res.subsidiaryOptions.length > 1) {
                const account = _find(res.connectedaccounts, { subsidiaryid: res.subsidiary });
                const internalid = _get(account, 'internalid');

                if (internalid) {
                    res = yield call(api.getUserInfo, { userID: internalid });
                    res.isPasswordChangeRequired = forcePasswordChange;
                    yield put(userActions.getUserInfo({ userID: internalid, changePasswordRequired: action.data.changePasswordRequired }));
                }
            }
        }

        if (res.isStaff) {
            if (loginMessage.length > 0) loginMessage += '\n';
            loginMessage += 'You are now logged in as White Labs staff. Please select a customer.';
            extraTimeout = 10000;
        } else {
            switch (res.subsidiary) {
                case 5:
                case 7:
                    extraTimeout = 60000;

                    if (loginMessage.length > 0) loginMessage += '\n';
                    loginMessage += '\nYou are now logged in to our ';
                    if (res.subsidiary == 5) {
                        // HK
                        loginMessage += 'Hong Kong';
                    } else if (res.subsidiary == 7) {
                        // CPH
                        loginMessage += 'Copenhagen';
                    }
                    loginMessage += ' manufacturing/shipping warehouse.\nSome items are only available from our US manufacturing/shipping warehouse. '
                        + 'To switch warehouses, visit My Account and select an option from the "Order From" dropdown.';
                    break;
                case 2:
                default:
                    if (loginMessage.length > 0) loginMessage += '\n';
                    loginMessage += '\nYou are now logged in to our US subsidiary.';
                    break;
            }
        }

        if (res.isRetailer) {
            loginMessage += '\n\nRetailers, please visit the new PartnersFirst section under the user menu. You\'ll find educational materials, recipes, and helpful documents for you and your customers.';
            extraTimeout = 30000;
        }

        localStorage.setItem('userInfo', JSON.stringify(res));
        yield put(responseSuccess());
        yield put(messageActions.showBanner({ content: { message: loginMessage }, variant: 'success', duration: extraTimeout }))
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        }
        else{
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));

    }
}

export function* setUserInfo(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { userInfo }
    } = action;
    try {
        prepareUserInfo(userInfo);
        yield put(responseSuccess(userInfo));
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error,variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function* updateUserInfo(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { request }
    } = action;
    try {
        const user = yield select(state => state.user);

        request.id = user.id;
        const { res: { error } } = yield call(api.updateUserInfo, { request });

        yield put(responseSuccess(request));

        if (error) {
            if (error.message) {
                yield put(messageActions.showBanner({ content: { message: error.message }, variant: 'error' }))
            } else {
                yield put(messageActions.showBanner({ content: { message: 'An error occurred updating your account. Please try again. If the problem persists, contact us for assistance.' }, variant: 'error' }))
            }
        } else {
            yield put(messageActions.showBanner({ content: { message: 'Your account information has been successfully updated' }, variant: 'success' }))

            yield put(
                userActions.getUserInfo({
                    userID: user.id
                })
            );
        }
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error'}))
        }
        yield put(responseFailure(error));
    }
}

export function* createUser(action) {    
    const { responseSuccess, responseFailure, data } = action;

    try {        
        var request = Object.assign({}, data);
        var nonce = Utils.uuid();
        request.creditToken = WLHelper.generateCreditToken(data.card);
        request.nonce = nonce;
        const { res } = yield call(api.createNetSuiteAccount, { request });        
        if (res.error) throw error;
        // request = {};
        request.id = res.id;
        if (request.id) data.id = request.id[0];
        request.password = data.password;
        request.nonce = nonce;
        request.email = data.email;
        const { res: { error } } = yield call(api.createYeastmanAccount, { request });
        if (error) {            
            yield put(messageActions.showBanner({ content: { message: 'Error creating account' }, variant: 'error'}))
            yield put(responseFailure(error));
            throw error;
        } else {            
            yield put(responseSuccess());
            var userInfo = data;
            yield put(userActions.getUserInfo({ userID: data.id }));
            yield put(userActions.setUserInfo({ userInfo }));
            yield put(messageActions.showBanner({ content: { message: 'Your account has been successfully created' }, variant: 'success'}))
        }
    } catch (error) {                
        yield put(messageActions.showBanner({ content: { message:'Error creating account' }, variant: 'error'}))
        yield put(responseFailure(error));

        if (error && error.status) {            
            // show network error is any regaring with api status
            yield put(messageActions.showBanner({ content: { message: 'Network Failure' }, variant: 'error'}))
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }        
        yield put(responseFailure(error));
    }
}

export function* changeSubsidiary(action) {
    const { responseSuccess, responseFailure, data: { subsidiary }} = action;
    try {
        const user = yield select(state => state.user);

        const account = _find(user.connectedaccounts, { subsidiaryid: subsidiary });
        const internalid = _get(account, 'internalid');

        yield put(userActions.getUserInfo({ userID: internalid }));
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function* checkCustomerHoldStatus(action) {
    const { responseSuccess, responseFailure } = action;
    try {
        const user = yield select(state => state.user);
        const internalid = _get(user, 'id');
        const isStaff = user.isStaff;

        var { res } = yield call(api.getUserInfo, { userID: internalid });
        user.isOnCreditHold = (res.creditHold && res.creditHold === 'ON');

        user.wasDefaultYeastShipMethodSet = false;
        res.wasDefaultYeastShipMethodSet = false;
        user.wasDefaultAlcoholShipMethodSet = false;
        res.wasDefaultAlcoholShipMethodSet = false;
        user.isStaff = isStaff;
        res.isStaff = isStaff;

        //yield put(userActions.setUserInfo({ userInfo: res }));
        localStorage.setItem('userInfo', JSON.stringify(res));
        yield put(responseSuccess());
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function* addSubsidiary(action) {
    const { responseSuccess, responseFailure, data: { subsidiary }} = action;
    try {
        const user = yield select(state => state.user);
        const account = _find(user.connectedaccounts, {subsidiaryid: 2});
        const internalid = _get(account, 'internalid');
        if (!internalid) throw { message: 'User does not have a White Labs Inc account', code: 0}

        let request = {};

        request.addSubsidiary = true;
        request.subsidiary = subsidiary;
        request.id = internalid;
        request.vat = user.vat;
        request.category = user.category;

        var { res: { id }, error } = yield call(api.addSubsidiary, { request });

        if (error) throw error;

        yield put(userActions.getUserInfo({userID: id}));

    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content:error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function* recordSurveyResponses(action) {
    const { responseSuccess, responseFailure, data: { questions, source } } = action;
    try {
        const user = yield select(state => state.user);

        let request = {};
        request.isQuestionnaireResponse = true;
        request.questions = questions;
        request.source = source;
        if (user) request.id = user.id;

        var { res: { message }, error } = yield call(api.recordSurveyResponses, { request });

        if (error) {
            throw error;
        } else {
            yield put(messageActions.showSnackbar({ content: (message ? message : 'Thank you for your responses!'), variant: 'success' }));
        }
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function* getOrderHistory(action) {
    const { responseSuccess, responseFailure } = action;
    try {
        const user = yield select(state => state.user);

        if (!user.id) {
            throw { message: 'No user id. Cannot get order history', code: 0 };
        }

        var request = {};
        request.id = user.id;

        var {
            res: { orderHistory },
            error
        } = yield call(api.getOrderHistory, { request });
        if (error) throw error;

        yield put(responseSuccess({ orderHistory }));
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content:error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function* forgotPassword(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { email, userName }
    } = action;
    try {
        // We'll need user name here if/when we return forgot password functionality back to YMO2
        let request = {};
        request.email = email;
        request.username = userName;

        const {
            res,
            error
        } = yield call(api.forgotPassword, { request });

        if (error) {
            throw error;
        } else if (res.error) {
            throw res.error;
        } else {
            yield put(messageActions.showBanner({ content: { message: 'Your password has been reset. Please watch your email for your new password' }, variant: 'success' }))
        }
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}


export function* changePassword(action) {    
    const {
        responseSuccess,
        responseFailure,
        data: { newPassword, currentPassword }
    } = action;
    try {
        const user = yield select(state => state.user);

        let request = {};
        request.newPassword = newPassword;
        request.currentPassword = currentPassword;
        request.user = user;

        var {
            res,
            error
        } = yield call(api.changePassword, { request });
        if (error) {
            throw error;
        } else if (res.error) {
            throw res.error;
        } else {
            yield put(messageActions.showSnackbar({ content: { title: 'Success', message: 'Your password has been changed.' }, variant: 'success' }));
            yield put(responseSuccess());
        }
    } catch (error) {        
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function* setShipMethod(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { shipmethod }
    } = action;
    try {
        yield put(responseSuccess({ shipmethod }));
        yield put(orderActions.prepareOrder());
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

/*************************/
/* Credit Card Functions */
/*************************/

export function* addCreditCard(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { creditCard }
    } = action;
    try {
        const user = yield select(state => state.user);

        if (!user.id) {
            yield put(messageActions({ content: { message: 'No user id. Cannot add credit card' }, variant: 'error' }))
            throw { message: 'No user id. Cannot add credit card', code: 0 };
        }

        let request = {};
        request.addCreditCard = true;
        request.token = WLHelper.generateCreditToken(creditCard);
        
        yield put(userActions.updateUserInfo({ request }));
        yield put(messageActions.showBanner({ content: error, variant: 'error' }))
    } catch (error) {
        yield put(responseFailure(error));
    }
}

export function* deleteCreditCard(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { creditCard }
    } = action;
    try {
        const user = yield select(state => state.user);

        if (!user.id) {
            yield put(messageActions({ content: { message: 'No user id. Cannot delete credit card' }, variant: 'error' }))
            throw { message: 'No user id. Cannot delete credit card', code: 0 };
        }

        let request = {};
        request.deleteCreditCard = true;
        request.card = creditCard;

        yield put(userActions.updateUserInfo({ request }));
    } catch (error) {
        yield put(responseFailure(error));
    }
}

export function* setCreditCard(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { creditCard }
    } = action;
    try {
        yield put(responseSuccess({ creditCard }));
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function* setDefaultCreditCard(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { creditCard }
    } = action;
    try {
        const user = yield select(state => state.user);

        if (!user.id) {
            throw { message: 'No user id. Cannot set default card', code: 0 };
        }

        // Update default card in NetSuite
        let request = {};
        request.defaultCreditCard = true;
        request.card = creditCard;
        yield put(userActions.updateUserInfo({ request }));
    } catch (error) {
        yield put(responseFailure(error));
    }
}

/*****************************/
/* General Address Functions */
/*****************************/

export function* addAddress(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { address }
    } = action;
    try {
        const user = yield select(state => state.user);

        if (!user.id) {
            yield put(messageActions.showBanner({ content: { message: 'No user id. Cannot add Address' }, variant: 'error'}))
            throw { message: 'No user id. Cannot add Address', code: 0 };
        }

        let request = {};
        request.addAddress = true;
        request.address = address;
        yield put(userActions.updateUserInfo({ request }));
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function* editAddress(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { address }
    } = action;
    try {
        const user = yield select(state => state.user);

        if (!user.id) {
            yield put(messageActions.showBanner({ content: { message: 'No user id. Cannot edit Address' }, variant: 'error' }))
            throw { message: 'No user id. Cannot edit address', code: 0 };
        }

        let request = {};
        request.editAddress = true;
        request.address = address;

        yield put(userActions.updateUserInfo({ request }));
    } catch (error) {
        yield put(responseFailure(error));
    }
}

export function* deleteAddress(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { address }
    } = action;
    try {
        const user = yield select(state => state.user);

        if (!user.id) {

            yield put(messageActions.showBanner({ content: { message: 'No user id. Cannot delete Address' }, variant: 'error' }))
            throw { message: 'No user id. Cannot delete Address', code: 0 };
        }

        let request = {};
        request.deleteAddress = true;
        request.address = address;

        yield put(userActions.updateUserInfo({ request }));
    } catch (error) {
        yield put(responseFailure(error));
    }
}

/******************************/
/* Shipping Address Functions */
/******************************/

export function* setShipAddress(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { address }
    } = action;
    try {
        yield put(responseSuccess({ address }));

        const user = yield select(state => state.user);
        user.shipping = address;
        user.shipMethods = WLHelper.shipMethodGroup(user.subsidiary, user.defaultShipMethodId, user.shipping.countryid);
        yield put(orderActions.prepareOrder());
    } catch (error) {
        yield put(responseFailure(error));
    }
}

export function* setDefaultShipAddress(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { address }
    } = action;
    try {
        const user = yield select(state => state.user);

        if (!user.id) {
            yield put(messageActions.showBanner({ content: { message: 'No user id. Cannot set default ship address' }, variant: 'error'}))
            throw { message: 'No user id. Cannot set default ship address', code: 0 };
        }

        let request = {};
        request.defaultShipAddress = true;
        request.address = address;
        request.shipmethod = user.defaultShipMethodId;
        user.shipMethods = WLHelper.shipMethodGroup(user.subsidiary, user.defaultShipMethodId, request.address.countryid);

        yield put(userActions.updateUserInfo({ request }));
    } catch (error) {
        yield put(responseFailure(error));
    }
}

/*****************************/
/* Billing Address Functions */
/*****************************/

export function* setBillAddress(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { address }
    } = action;
    try {
        yield put(responseSuccess({ address }));

        const user = yield select(state => state.user);
        user.billing = address;
        yield put(orderActions.prepareOrder());
    } catch (error) {
        yield put(responseFailure(error));
    }
}

export function* setDefaultBillAddress(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { address }
    } = action;
    try {
        const user = yield select(state => state.user);

        if (!user.id) {
            yield put(messageActions.showBanner({ content: { message: 'No user id. Cannot set default bill address' }, variant: 'error' }))

            throw { message: 'No user id. Cannot set default bill address', code: 0 };
        }

        let request = {};
        request.id = user.id;
        request.defaultBillAddress = true;
        request.address = address;

        yield put(userActions.updateUserInfo({ request }));
    } catch (error) {
        yield put(responseFailure(error));
    }
}

//Feedback saga function
export function* addFeedback(action) {
    const {
        responseSuccess,
        responseFailure,
        data
    } = action;
    try {
        const { res: { error } } = yield call(api.feedback, data); //api not available
        yield put(responseSuccess());

        if (error) {
            throw error;
        } else {
            yield put(messageActions.showBanner({ content: { message: 'Your Feedback has been successfully submitted' }, variant: 'success' }))
        }
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}