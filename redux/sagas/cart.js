import { call, put, select } from 'redux-saga/effects';
import { messageActions } from 'appRedux/actions/messageActions';
import _cloneDeep from 'lodash/cloneDeep';

import { addToCart, changeItemQuantity } from 'lib/CartUtils';
import { itemAvailabilityPost, relatedItemsPost } from '../../services/itemApi';
import { cartActions } from 'appRedux/actions/cartActions';
import { parseAvailabilityResults } from '../../lib/InventoryUtils';

export function* setCouponCode(action) {
    const {
        responseSuccess,
        responseFailure,
        data: { couponCode }
    } = action;
    try {
        console.log(action);
        yield put(responseSuccess({ couponCode }));
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}

export function * addCartItem(action) {
    const { responseSuccess, responseFailure, data: { cartItem } } = action;
    try {
        const items = yield select(state => state.cart.items);
        addToCart(items, cartItem);
        yield put(responseSuccess({ items }));
    } catch(error) {
        yield put(responseFailure(error));
    }
}

export function * removeCartItem(action) {
    const { responseSuccess, responseFailure, data: index } = action;
    try {
        const oldItems = yield select(state => state.cart.items);
        var items = oldItems;
        items.splice(index, 1);
        yield put(responseSuccess({ items }));
    } catch(error) {
        yield put(messageActions.showSnackbar({content:error,variant:'error'}))
        yield put(responseFailure(error));
    }
}

export function * replaceCartItem(action) {
    const { responseSuccess, responseFailure, data: { cartItems, index:cartIndex }} = action;
    try {
        const oldItems = yield select(state => state.cart.items);
        var items = _cloneDeep(oldItems);
        items.splice(cartIndex, 1, ...cartItems);
        yield put(responseSuccess({ items }));
    } catch(error) {
        yield put(messageActions.showSnackbar({content:error,variant:'error'}))
        yield put(responseFailure(error));
    }
}

export function * updateCartItem(action) {
    const { responseSuccess, responseFailure, data: { index, quantity } } = action;
    var items = yield select(state => state.cart.items);
    try {
        changeItemQuantity(items, index, quantity);
        yield put(responseSuccess({ items }));
    } catch (error) {
        yield put(messageActions.showSnackbar({content:error,variant:'error'}))
        yield put(responseFailure(error));
    }
}

export function * clearCart(action) {
    const { responseSuccess, responseFailure } = action;
    try {
        yield put(responseSuccess());
    } catch (error) {
        yield put(messageActions.showSnackbar({content:error,variant:'error'}))
        yield put(responseFailure(error));
    }
}


const checkQuantity = item => {
  const quantity = parseFloat(item.OrderDetailQty);
  if (isNaN(quantity) || quantity <= 0) {
    // this.handleErrors('quantity', 'Please enter a valid value for the quantity')
    return false;
  }
  //  Must be in increments of 1
  else if (parseFloat(quantity) / parseInt(quantity) !== 1.0) {
    return false;
  }
  return true;
};

export function *addToCartSaga(action) {
  const { responseSuccess, responseFailure, data: { cartItem } } = action;
  try {
    const inventory = yield select(state => state.inventory);
    const { data: { relatedItems } } = yield call(relatedItemsPost, { itemId: cartItem.MerchandiseID });
    if (relatedItems) {
      for (let i = 0; i < relatedItems.length; i++) {
        cartItem.relatedItems = [];
        for (let j = 0; j < relatedItems[i].related.length; j++) {
          let relItemArray = inventory.items.filter(function (el) {
            return (el.volID.includes(relatedItems[i].related[j]));
          });
          for (let k = 0; k < relItemArray.length; k++) {
            cartItem.relatedItems.push({
              relatedItemId: relatedItems[i].related[j],
              related: relItemArray[k],
            });
          }
        }
      }
      if (yield call(checkQuantity, cartItem)) {
        yield put(cartActions.dialogClose(false));
        yield put(cartActions.addItem({ cartItem }));
        yield put(responseSuccess());
      }
    }
  } catch {
    if (yield call(checkQuantity, cartItem)) {
      yield put(cartActions.addItem({ cartItem }));
      yield put(cartActions.dialogClose(false));
    }
    yield put(responseFailure());
  }
}

export function *checkAvailability(action) {
  const { responseSuccess, responseFailure, data: { data, item } } = action;
  try {
    const { data: { availability } } = yield call(itemAvailabilityPost, data);
    if (availability) {
      yield put(responseSuccess({ [item]: parseAvailabilityResults(availability, data.subsidiary) }));
    } else {
      yield put(responseFailure());
    }
  } catch (e) {
    yield put(responseFailure());
  }

}
