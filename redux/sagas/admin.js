import { put, call, select } from 'redux-saga/effects';
import _isNumber from 'lodash/isNumber';
import _isEmpty from 'lodash/isEmpty';
import _find from 'lodash/find';
import _get from 'lodash/get';

import { messageActions } from 'appRedux/actions/messageActions';

import * as api from 'services/admin.js';

export function* getCustomer(action) {
    const { responseSuccess, responseFailure, data } = action;
    try {
        if (!data) {
            throw { message: 'No customer name specified', code: 0 };
        }

        var res = yield call(api.getCustomer, { custName: data, version: '2.5.0' });
        if (res.error) throw res.error;

        yield put(responseSuccess({ customers: res }));
    } catch (error) {
        if (error.status) {
            // show network error is any regaring with api status
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        } else {
            yield put(messageActions.showBanner({ content: error, variant: 'error' }))
        }
        yield put(responseFailure(error));
    }
}