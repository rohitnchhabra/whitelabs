import { put, select } from 'redux-saga/effects'
import { find } from 'lodash';

export function* showBanner(action) {
    const { responseSuccess, responseFailure, data: { content, variant, duration } } = action;
    let message = content.message;
    let msgText = message.message || message;
    const banner = yield select(state => state.messages.banner);

    let mes = find(banner, (err) => { return err.variant === variant && err.message === msgText })
    if (!mes) {
        const ban = [...banner,
        {
            index: banner.length,
            // title,
            message: msgText,
            variant,
            timeOut: (duration ? duration : 3000),
        }
        ]
        yield put(responseSuccess({ banner: ban }));
    } else {
        yield put(responseFailure());
    }
}

export function* showSnackbar(action) {

    const { responseSuccess, responseFailure, data: { content, variant } } = action;
    let message = content.message;
    let msgText = message.message || message;
    const snackbar = yield select(state => state.messages.snackbar);
    let mes = find(snackbar, (err) => { return err.variant === variant && err.message === msgText })
    if (!mes) {
        const snack = [...snackbar,
        {
            index: snackbar.length,
            // title,
            message: msgText,
            variant,
            anchorOrigin:undefined
        }
        ]
        yield put(responseSuccess({ snackbar: snack }));
    } else {
        yield put(responseFailure());
    }
}

export function* showChangePasswordModal(action){
    const { responseSuccess } = action;
yield put(responseSuccess({showChangePassword:true}))
}