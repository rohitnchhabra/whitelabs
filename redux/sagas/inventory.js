import { take, call, put, cancelled, takeEvery, all, fork, select  } from 'redux-saga/effects';
import { messageActions } from 'appRedux/actions/messageActions';
import { inventoryActions } from 'appRedux/actions/inventoryActions';

import * as api from 'services/';

export function * getInventory (action) {
    const { responseSuccess, responseFailure, data: { search } } = action;
    try {
        yield put(messageActions.hideSnackbar({}))
        const { res: { items, error, welcomeMessage }} = yield call(api.getInventory);    
        if( error ) {
            throw error
        } else if (items) {
            for (var i = 0; i < items.length; i++) {
                if (items[i].isYeast) {
                    if (items[i].partNum.indexOf('-O') < 0) {
                        if (items.find(function (item) { return (item.partNum.replace(items[i].partNum, '') == '-O'); })) {
                            items[i].hasOrganicVersion = true;
                        }
                    } else {
                        items[i].isOrganic = true;
                    }
                }

                if (items[i].salesCategory == 11) {
                    items[i].isAlcoholItem = true;
                }
            }

            yield put(responseSuccess({ items, welcomeMessage })); 
            yield put(messageActions.hideSnackbar())   
        }
    } catch (error) { 
        
        yield put(responseFailure(error));
        yield put(messageActions.showSnackbar({  content: error, variant:'error' }));
    }     
};

export function * toggleHomebrew(action) {
    const { responseSuccess, responseFailure, data: { isHomebrew }} = action;
    try {
        yield put(responseSuccess({ isHomebrew }));
    } catch(error) {
        yield put(responseFailure(error));
    }
}

export function * getAlternateSizes(action) {
    const { responseSuccess, responseFailure, data: { item, cartIndex }} = action;
    
    try {
        const user = yield select(state => state.user);
        const inventory = yield select(state => state.inventory);
        const itemRef = inventory.items.find(x => x.volID.includes(item.MerchandiseID));

        if (!itemRef) {
            throw { message: 'itemRef is not defined', code: 0};
        }
        else if (!user) {
            throw { message: 'user is not defined', code: 0};
        }
        else {
            const request = {};
            request.id = user.id;
            request.subsidiary = user.subsidiary;
            request.alternateSizes = true;
            request.SaleItem = item;
            request.ItemGroup = itemRef.volID.slice(0,3);
    
            const { res: { alternateSizes, error }} = yield call(api.getAlternateSizes, {
                request
            });   

            if (alternateSizes.length >= 0) {
                alternateSizes.forEach(function(x) {
                    x.forEach(function(y) {
                        if (y) {
                            y.Name = String(itemRef.Name);
                            y.salesCategory = parseInt(itemRef.salesCategory);
                            y.alternateItem = true;
                            y.purepitch = true;
                        }
                    });
                });

                yield put(responseSuccess({ alternateSizes, cartIndex }));

            } 
        }
    } catch(error) {
        yield put(messageActions.showBanner({content:{message:'No alternative size combinations were found'}, variant:'error'}))
        yield put(responseFailure(error));
    }
}

export function * getSimilarStrains(action) {
    const { responseSuccess, responseFailure, data: { item, selectedStyles }} = action;
    try {
        const user = yield select(state => state.user);
        const inventory = yield select(state => state.inventory);
        const itemRef = inventory.items.find(x => x.volID.includes(item.MerchandiseID));
        
        const request = {};
        request.id = user.id;
        request.SaleItem = item;
        request.ItemGroup = itemRef.volID.slice(0,3);
        request.selectedStyles = selectedStyles;

        const { res: { alternateStrains, error }} = yield call(api.getSimilarStrains, {
            request
        });   
        if (alternateStrains.length > 0) {
            alternateStrains.forEach(function(y) {
                y.alternateItem = true;
                y.purepitch = true;
            });
            yield put(responseSuccess({ alternateStrains }));

        }
        
    } catch(error) {
        yield put(responseFailure(error));
        yield put(messageActions.showBanner({content:{message:'No similar strains were found'}, variant:'error'}))
    }
}

export function* getRelatedItems(action) {
    const { responseSuccess, responseFailure, data: { cartItem} } = action;
    try {
        const user = yield select(state => state.user);
        const inventory = yield select(state => state.inventory);
        const itemRef = inventory.items.find(x => x.volID.includes(cartItem.MerchandiseID));

        const request = {};
        request.relatedItems = true;
        request.SaleItem = cartItem;

        const { res: { relatedItems, error } } = yield call(api.getRelatedItems, {
            request
        });
        if (relatedItems.length > 0) {
            yield put(responseSuccess({ relatedItems }));
        }
    } catch (error) {
        yield put(responseFailure(error));
    }
}