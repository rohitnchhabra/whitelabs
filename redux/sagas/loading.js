import { take, call, put, cancelled, takeEvery, all, fork, select  } from 'redux-saga/effects';

export function * startLoading(action) {
    const { responseSuccess, responseFailure } = action;
    try {
        yield put(responseSuccess());
    } catch (error) {
        yield put(messageActions.showSnackbar({content:error,variant:'error'}))
        yield put(responseFailure(error));
    }
}


export function * stopLoading(action) {
    const { responseSuccess, responseFailure } = action;
    try {
        yield put(responseSuccess());
    } catch (error) {
        yield put(messageActions.showSnackbar({content:error,variant:'error'}))
        yield put(responseFailure(error));
    }
}


