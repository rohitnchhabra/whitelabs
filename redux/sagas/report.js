import { call, put, select } from 'redux-saga/effects';
import { lotNumberSelector, packageTypeSelector } from '../selectors/report';
import { getQcResults } from '../../services';


export function* getReport(action) {
    const { responseSuccess, responseFailure } = action;
    const state = yield select();

    var packagingId;
    if (!packageTypeSelector(state)) {
        packagingId = new String('1');
    } else {
        packagingId = new String(packageTypeSelector(state));
    }

    const lotNumber = new String(lotNumberSelector(state));
    try {
        const { res: { qcResults } } = yield call(getQcResults, { lotNumber, packagingId});
        yield put(responseSuccess({ reportData: qcResults[0] }));
    } catch(error) {
        yield put(responseFailure(error));
    }
}
