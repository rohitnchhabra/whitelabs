import { take, call, put, cancelled, takeEvery, all, fork, select } from 'redux-saga/effects';

import * as api from 'services/';
import { orderActions } from 'appRedux/actions/orderActions';
import { cartActions } from 'appRedux/actions/cartActions';
import { messageActions } from 'appRedux/actions/messageActions';

import {
    changeShippingOption,
    incrementItemDate,
    decrementItemDate,
    initOrder,
    validateOrder,
    setDistributedItems,
    justDateString,
    checkForUnsplittableItem
} from 'lib/OrderUtils';

const replace = (arr, newItem) => arr.map(
    (item) => {
        let items=item.Name+" "+item.details
        let currentItem=newItem.Name+" "+newItem.details        
        return items === currentItem ? newItem : item}
);

/****** Sagas logic ********/
export function* prepareOrder(action) {
    const { responseSuccess, responseFailure } = action;
    const cart = yield select(state => state.cart);
    const user = yield select(state => state.user);
    const couponCode = cart.couponCode;

    try {
        var { res: order, error } = yield call(api.prepareOrder, {
            user: {
                id: user.id,
                shipmethod: user.shipmethod,
                shipToState: user.shipping.state,
                shipToCountry: user.shipping.countryid,
                category: user.category
            },
            items: cart.items,
            couponCode: couponCode,
            cart: cart
        });

        if (error) {
            throw error;
        } else if (order.error) {
            throw order.error;
        } else {
            var initedOrder = initOrder(order, user, cart);
            if (!order.items) {
                order.items = [];
            } else if (order.items.length != initedOrder.items.length) {
                setDistributedItems(order.items, initedOrder.items);
            }
            yield put(responseSuccess(initedOrder));
        }
    } catch (error) {
        if (!action.isRetry) {
            // Retry once
            try {
                action.isRetry = true;

                var { res: order, error } = yield call(api.prepareOrder, {
                    user: { id: user.id, shipmethod: user.shipmethod },
                    items: cart.items,
                    couponCode
                });

                if (error) {
                    throw error;
                } else if (order.error) {
                    throw order.error;
                } else {
                    initedOrder = initOrder(order, user, cart);

                    if (!order.items) {
                        order.items = [];
                    } else if (order.items.length != initedOrder.items.length) {
                        setDistributedItems(order.items, initedOrder.items);
                    }
                    yield put(responseSuccess(initedOrder));
                }
            } catch (error) {
                yield put(responseFailure(error));
                yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
            }
        } else {
            yield put(responseFailure(error));
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        }
    }
};

export function* placeOrder(action) {
    const { responseSuccess, responseFailure } = action;
    try {
        var order = yield select(state => state.order);
        const user = yield select(state => state.user);
        
        var request;
        try {
            request = validateOrder(order, user);
        } catch (error) {
            // An error here is a validation failure that shouldn't be retried and the user should actually see
            yield put(responseFailure(error));
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        }

        if (request) {
            var { res: order, error } = yield call(api.placeOrder, {
                request
            });

            if (error) {
                throw error;
            } else if (order.error) {
                throw order.error;
            } else {
                yield put(responseSuccess(order.NSOrder));
                yield put(cartActions.clearCart());
            }
        }
    } catch (error) {
        if (!action.isRetry) {
            // Retry once
            try {
                action.isRetry = true;

                var { res: order, error } = yield call(api.placeOrder, {
                    request
                });

                if (error) {
                    throw error;
                } else if (order.error) {
                    throw order.error;
                } else {
                    yield put(responseSuccess(order.NSOrder));
                    yield put(cartActions.clearCart());
                }
            } catch (error) {
                yield put(responseFailure(error));
                yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
            }
        } else {
            yield put(responseFailure(error));
            yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        }
    }
}

export function* resetOrder(action) {
    const { responseSuccess, responseFailure } = action;
    try {
        yield put(responseSuccess());
    } catch (error) {
        yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        yield put(responseFailure(error));
    }
}

export function* setShippingOption(action) {
    const { responseSuccess, responseFailure, data: {option,getItSooner} } = action;
    try {
        var order = yield select(state => state.order);
        const user = yield select(state => state.user);

        const items = changeShippingOption(order, user, option);
        yield put(responseSuccess({option,getItSooner}));
        yield put(orderActions.setItems({ items }));
    } catch (error) {
        yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        yield put(responseFailure(error));
    }
}

function isMatchingGelPackItem(item, thisItem) {
    if (thisItem.Warehouse == item.Warehouse && thisItem.MerchandiseID == 4328 && justDateString(thisItem.shipDate) == justDateString(item.shipDate)) {
        return (
               item.type == 2
            || item.salesCategory == 2
            || item.salesCategory == 3
            || item.salesCategory == 4
            || item.salesCategory == 5
            || item.salesCategory == 6
            || item.salesCategory == 7
            || item.salesCategory == 32
        );
    } else {
        return false;
    }
}

export function* incrementShipDate(action) {
    const { responseSuccess, responseFailure, data: item } = action;
    try {
        const order = yield select(state => state.order);
        const user = yield select(state => state.user);

        // const uniqueDates = new Set(order.items.map((thisItem) => thisItem.deliveryDate.getDay()))

        let updateItem = order.items.map(
            (thisItem) => (
                // Keep gel pack together with yeast item when dates are changed
                thisItem.MerchandiseID == item.MerchandiseID || isMatchingGelPackItem(item, thisItem)
                    ? incrementItemDate(order, user, thisItem)
                    : thisItem
                )
            )

        checkForUnsplittableItem(item, updateItem, order, user);

        yield put(orderActions.setItems({
            items: updateItem
        }));
    } catch (error) {
        yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        yield put(responseFailure(error));
    }
}

export function* decrementShipDate(action) {
    const { responseSuccess, responseFailure, data: item } = action;
    try {
        const order = yield select(state => state.order);
        const user = yield select(state => state.user);  

        let updateItem = order.items.map(
            (thisItem) => (
                // Keep gel pack together with yeast item when dates are changed
                thisItem.MerchandiseID == item.MerchandiseID || isMatchingGelPackItem(item, thisItem)
                    ? decrementItemDate(order, user, thisItem)
                    : thisItem
            )
        )

        checkForUnsplittableItem(item, updateItem, order, user);

        yield put(orderActions.setItems({
            items: updateItem
        }));
    } catch (error) {
        yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        yield put(responseFailure(error));
    }
}
export function* incrementAllShipDate(action) {
    const { responseSuccess, responseFailure, data: item } = action;
    try {
        const order = yield select(state => state.order);
        const user = yield select(state => state.user);
        let updateItem=order.items.map((item)=>incrementItemDate(order, user, item))
        
        yield put(orderActions.setItems({
            items: updateItem
        }));
    } catch (error) {
        yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        yield put(responseFailure(error));
    }
}
export function * decrementAllShipDate(action){
    const { responseSuccess, responseFailure} = action;
    try {
        const order = yield select(state => state.order);
        const user = yield select(state => state.user);   
        let updateItem= order.items.map((item)=>decrementItemDate(order, user, item))       
        yield put(orderActions.setItems({
            items:updateItem
        }));
    } catch (error) {
        yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        yield put(responseFailure(error));
    }
}

export function * resetRemovedItems(action){
    const { responseSuccess} = action;
    yield put(responseSuccess());
}

export function* setOptionalEmail(action) {
    const {responseSuccess, data} = action;
    yield put(responseSuccess(data));
}

export function* resendInvoice(action) {
    const { responseSuccess, responseFailure } = action;
    try {
        yield put(responseSuccess());
    } catch (error) {
        yield put(messageActions.showSnackbar({ content: error, variant: 'error' }));
        yield put(responseFailure(error));
    }
}

export function* logCheckoutPresentation(action) {
    const { responseSuccess, responseFailure, data: { shipMethod, presentedItems } } = action;
    try {
        const customer = yield select(state => state.user);

        const request = {};
        request.logCheckoutView = true;
        request.customer = customer;
        request.shipMethod = shipMethod;
        request.presentedItems = presentedItems;
        request.logDate = new Date();
        // Got UUID generator from https://stackoverflow.com/questions/105034/how-to-create-guid-uuid
        // We don't really care that much about overall uniqueness, just uniqueness right now
        request.presentationId = new Date().getTime();

        yield call(api.logCheckoutPresentation, { request });
    } catch (error) {
        yield put(responseFailure(error));
    }
}