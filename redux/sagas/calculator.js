import { put, select  } from 'redux-saga/effects';
import {
    beerTypeSelector,
    enzymeSelector,
    homebrewTypeSelector,
    maltValueSelector,
    wortGravitySelector,
    wortVolumeSelector
} from '../selectors/calculator';


export function * calcResult(action) {
    const { responseSuccess, responseFailure } = action;
    try {
        const enzymeType = yield select(enzymeSelector);
        const wortVolume = yield select(wortVolumeSelector);
        const beerType = yield select(beerTypeSelector);
        const homebrewType = yield select(homebrewTypeSelector);
        const wortGravity = yield select(wortGravitySelector);
        const maltValue = yield select(maltValueSelector);

        if (enzymeType === '5' && wortVolume && wortGravity && maltValue && beerType && enzymeType) {
            const maltValueToUse = (+maltValue == 100) ? 1 : maltValue / 100;
            const quantCalc = wortVolume * 1.173477658 * ((wortGravity * maltValueToUse * beerType / 12) / 1000) * enzymeType;
            const calcResult = Math.round(quantCalc / 1.1 * 1000 * 100) / 100;
            yield put(responseSuccess({ calcResult }));
        } else if (enzymeType === '1') {
            const beerValue = +beerType === 6 ? 4 : 2;
            const calcResult = Math.round(homebrewType * beerValue * 100) / 100;
            yield put(responseSuccess({ calcResult }));
        } else {
            yield put(responseSuccess({ calcResult: null }));
        }
    } catch(error) {
        yield put(responseFailure(error));
    }
}
