/* global fetch */
import { all, takeEvery, takeLatest } from 'redux-saga/effects';
// User actions
import { userTypes } from 'appRedux/actions/userActions';
// Cart actions
import { cartTypes } from 'appRedux/actions/cartActions';
// Order actions
import { orderTypes } from 'appRedux/actions/orderActions';
// Calculator Actions
import { calculatorTypes } from 'appRedux/actions/calculatorActions';
// Inventory actions
import { inventoryTypes } from 'appRedux/actions/inventoryActions';
// Admin actions
import { adminTypes } from 'appRedux/actions/adminActions';

import { loadingTypes } from 'appRedux/actions/loadingActions';
import { messageTypes } from 'appRedux/actions/messageActions';

// Report actions
import { reportTypes } from '../actions/reportActions';
// Loading sagas
import {
    startLoading,
    stopLoading
} from './loading';
// User sagas
import {
    loginUser,
    getUserInfo,
    setUserInfo,
    updateUserInfo,
    createUser,
    changeSubsidiary,
    addSubsidiary,
    getOrderHistory,
    forgotPassword,
    changePassword,
    setShipMethod,
    addCreditCard,
    deleteCreditCard,
    setCreditCard,
    setDefaultCreditCard,
    addAddress,
    editAddress,
    deleteAddress,
    setShipAddress,
    setDefaultShipAddress,
    setBillAddress,
    setDefaultBillAddress,
    addFeedback,
    checkCustomerHoldStatus
} from './user';
// Cart sagas
import {
    addToCartSaga,
    checkAvailability,
    updateCartItem,
    addCartItem,
    removeCartItem,
    replaceCartItem,
    clearCart,
    setCouponCode
} from './cart';
// Order sagas
import {
    prepareOrder,
    placeOrder,
    setShippingOption,
    incrementShipDate,
    decrementShipDate,
    incrementAllShipDate,
    decrementAllShipDate,
    resetRemovedItems,
    setOptionalEmail,
    resendInvoice,
    logCheckoutPresentation
} from './order';
// Inventory sagas
import {
    getInventory,
    toggleHomebrew,
    getSimilarStrains,
    getAlternateSizes,
    getRelatedItems
 } from './inventory';

 //message sagas
import{
     showBanner,
     showSnackbar,
     showChangePasswordModal
 } from './message'

//Report sagas
import { getReport } from './report';

// Calculator saga
import { calcResult } from './calculator';

function * rootSaga () {
    yield all([
        // LOADING
        takeEvery(loadingTypes.START_LOADING_ATTEMPT, startLoading),
        takeEvery(loadingTypes.STOP_LOADING_ATTEMPT, stopLoading),
        // USERS
        takeLatest(userTypes.USER_LOGIN_ATTEMPT, loginUser),
        takeLatest(userTypes.GET_USER_INFO_ATTEMPT, getUserInfo),
        takeLatest(userTypes.SET_USER_INFO_ATTEMPT, setUserInfo),
        takeLatest(userTypes.UPDATE_USER_INFO_ATTEMPT, updateUserInfo),
        takeLatest(userTypes.CREATE_USER_ATTEMPT, createUser),
        takeLatest(userTypes.CHANGE_SUBSIDIARY_ATTEMPT, changeSubsidiary),
        takeLatest(userTypes.ADD_SUBSIDIARY_ATTEMPT, addSubsidiary),
        takeLatest(userTypes.GET_ORDER_HISTORY_ATTEMPT, getOrderHistory),
        takeLatest(userTypes.FORGOT_PASSWORD_ATTEMPT, forgotPassword),
        takeLatest(userTypes.CHANGE_PASSWORD_ATTEMPT, changePassword),
        takeLatest(userTypes.SET_SHIP_METHOD_ATTEMPT, setShipMethod),
        takeLatest(userTypes.ADD_CREDIT_CARD_ATTEMPT, addCreditCard),
        takeLatest(userTypes.DELETE_CREDIT_CARD_ATTEMPT, deleteCreditCard),
        takeLatest(userTypes.SET_CREDIT_CARD_ATTEMPT, setCreditCard),
        takeLatest(userTypes.SET_DEFAULT_CREDIT_CARD_ATTEMPT, setDefaultCreditCard),
        takeLatest(userTypes.ADD_ADDRESS_ATTEMPT, addAddress),
        takeLatest(userTypes.EDIT_ADDRESS_ATTEMPT, editAddress),
        takeLatest(userTypes.DELETE_ADDRESS_ATTEMPT, deleteAddress),
        takeLatest(userTypes.SET_SHIP_ADDRESS_ATTEMPT, setShipAddress),
        takeLatest(userTypes.SET_DEFAULT_SHIP_ADDRESS_ATTEMPT, setDefaultShipAddress),
        takeLatest(userTypes.SET_BILL_ADDRESS_ATTEMPT, setBillAddress),
        takeLatest(userTypes.SET_DEFAULT_BILL_ADDRESS_ATTEMPT, setDefaultBillAddress),
        takeLatest(userTypes.ADD_FEEDBACK_ATTEMPT, addFeedback),
        takeLatest(userTypes.CHECK_CUSTOMER_HOLD_STATUS_ATTEMPT, checkCustomerHoldStatus),
        // INVENTORY
        takeEvery(inventoryTypes.GET_INVENTORY_ATTEMPT, getInventory),
        takeEvery(inventoryTypes.TOGGLE_HOMEBREW_ATTEMPT, toggleHomebrew ),
        takeEvery(inventoryTypes.GET_SIMILAR_STRAINS_ATTEMPT, getSimilarStrains ),
        takeEvery(inventoryTypes.GET_ALTERNATE_SIZES_ATTEMPT, getAlternateSizes),
        takeEvery(inventoryTypes.GET_RELATED_ITEMS_ATTEMPT, getRelatedItems),
        // CART
        takeEvery(cartTypes.ADD_ITEM_ATTEMPT, addCartItem),
        takeEvery(cartTypes.ADD_TO_CART_ATTEMPT, addToCartSaga),
        takeEvery(cartTypes.REMOVE_ITEM_ATTEMPT, removeCartItem),
        takeEvery(cartTypes.UPDATE_ITEM_ATTEMPT, updateCartItem),
        takeEvery(cartTypes.CLEAR_CART_ATTEMPT, clearCart),
        takeEvery(cartTypes.REPLACE_ITEM_ATTEMPT, replaceCartItem),
        takeEvery(cartTypes.SET_COUPON_CODE_ATTEMPT, setCouponCode),
        takeEvery(cartTypes.CHECK_AVAILABILITY_ATTEMPT, checkAvailability),
        // ORDER
        takeLatest(orderTypes.PREPARE_ORDER_ATTEMPT, prepareOrder),
        takeLatest(orderTypes.PLACE_ORDER_ATTEMPT, placeOrder),
        takeLatest(orderTypes.SET_SHIPPING_OPTION_ATTEMPT, setShippingOption),
        takeLatest(orderTypes.INCREMENT_SHIP_DATE_ATTEMPT, incrementShipDate),
        takeLatest(orderTypes.DECREMENT_SHIP_DATE_ATTEMPT, decrementShipDate),
        takeLatest(orderTypes.INCREMENT_ALL_SHIP_DATE_ATTEMPT, incrementAllShipDate),
        takeLatest(orderTypes.DECREMENT_ALL_SHIP_DATE_ATTEMPT, decrementAllShipDate),
        takeLatest(orderTypes.RESET_REMOVED_ITEM_ATTEMPT, resetRemovedItems),
        takeLatest(orderTypes.SET_OPTIONAL_EMAIL_ATTEMPT, setOptionalEmail),
        takeLatest(orderTypes.RESEND_INVOICE_ATTEMPT, resendInvoice),
        takeEvery(orderTypes.LOG_CHECKOUT_PRESENTATION_ATTEMPT, logCheckoutPresentation),
        // MESSAGE
        takeLatest(messageTypes.SHOW_BANNER_ATTEMPT, showBanner),
        takeLatest(messageTypes.SHOW_SNACKBAR_ATTEMPT, showSnackbar),
        takeLatest(messageTypes.SHOW_CHANGE_PASSWORD_MODAL_ATTEMPT,showChangePasswordModal),
        //REPORT
        takeLatest(reportTypes.GET_REPORT_ATTEMPT, getReport),
        //CALCULATOR
        takeLatest(calculatorTypes.CALCULATE_ATTEMPT, calcResult)
    ]);
};

export default rootSaga;
