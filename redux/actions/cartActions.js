import { createActionsStructure } from 'helpers/reduxHelpers';

/* ------------- Action Creators ------------- */

export const { cartTypes, cartActions } = createActionsStructure('cart', [
  { name: 'ADD_ITEM', async: true },
  { name: 'UPDATE_ITEM', async: true },
  { name: 'REMOVE_ITEM', async: true },
  { name: 'REPLACE_ITEM', async: true },
  { name: 'CLEAR_CART', async: true },
  { name: 'SHOW_WANT_SOONER', async: true },
  { name: 'HIDE_WANT_SOONER', async: true },
  { name: 'ADD_TO_CART', async: true },
  { name: 'DIALOG_CLOSE', async: true },
  { name: 'CHECK_AVAILABILITY', async: true },
  { name: 'SET_COUPON_CODE', async: true }
]);
