import { createActionsStructure } from 'helpers/reduxHelpers';

/* ------------- Action Creators ------------- */

export const { messageTypes, messageActions } = createActionsStructure('message', [
  { name: 'SHOW_BANNER', async:true },
  { name: 'HIDE_BANNER', async:false },
  { name: 'SHOW_SNACKBAR',async:true },
  { name: 'HIDE_SNACKBAR', async:false },
  {name:"SHOW_CHANGE_PASSWORD_MODAL",async:true}

]);
