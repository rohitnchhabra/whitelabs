import { createActionsStructure } from 'helpers/reduxHelpers';

/* ------------- Action Creators ------------- */

export const { adminTypes, adminActions } = createActionsStructure('admin', [
    { name: 'GET_CUSTOMER', async: true },
]);


// -- uncomment to display
//console.log(adminTypes, adminActions)
