import { createActionsStructure } from 'helpers/reduxHelpers';

/* ------------- Action Creators ------------- */

export const { reportTypes, reportActions } = createActionsStructure('report', [
    { name: 'GET_REPORT', async: true },
]);
