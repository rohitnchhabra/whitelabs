import { createActionsStructure } from 'helpers/reduxHelpers';

/* ------------- Action Creators ------------- */
export const { orderTypes, orderActions } = createActionsStructure('order', [
    { name: 'PREPARE_ORDER', async: true },
    { name: 'PLACE_ORDER', async: true },
    { name: 'RESET_PLACED_ORDER', async: false },
    { name: 'SET_SHIPPING_OPTION', async: true },
    { name: 'INCREMENT_SHIP_DATE', async: true },
    { name: 'DECREMENT_SHIP_DATE', async: true },
    { name: 'INCREMENT_ALL_SHIP_DATE', async: true },
    { name: 'DECREMENT_ALL_SHIP_DATE', async: true },
    { name: 'SET_ITEMS' },
    { name: 'SET_PONUMBER', async: false },
    { name: 'RESET_REMOVED_ITEM', async: true },
    { name: 'SET_OPTIONAL_EMAIL', async: true },
    { name: 'RESEND_INVOICE', async: true },
    { name: 'LOG_CHECKOUT_PRESENTATION', async: true }
]);
