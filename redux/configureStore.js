import React from 'react'
import {
    createStore,
    applyMiddleware,
    combineReducers,
    compose
} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga';
import { all } from 'redux-saga/effects';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'
import { persistStore, persistReducer } from 'redux-persist';

//reducers
import { rootReducer } from './reducers';
import rootSaga from './sagas';

const middleware = [];
const sagaMiddleware = createSagaMiddleware();

middleware.push(sagaMiddleware);

if (process.env.NODE_ENV !== 'production') {
    middleware.push(
      createLogger({
        collapsed: true
      })
    );
   }

// const initializeStore = (initialState = initialState, isServer) => {
const initializeStore = (initialState = {}, isServer) => {
    if(isServer) {
        return {
            store: createStore(
                rootReducer, 
                initialState
            )
        }
    }
    
    const storage = require('redux-persist/lib/storage');

    const persistConfig = {
        key: 'root',
        storage: storage.default,
        whitelist: ['user', 'cart']
    };
    const persistedReducer = persistReducer(persistConfig, rootReducer);
    let store = createStore(
        persistedReducer,
        initialState,
        composeWithDevTools(
            applyMiddleware(thunk, ...middleware)
        )
    );
    
    sagaMiddleware.run(rootSaga);
    return {
        store,
        persistor: persistStore(store)
    }
}

// const initializeNewStore = (initialState = initialState, isServer) => {
const initializeNewStore = (initialState = {}, isServer) => {

    if(isServer) {
        return {
            store: createStore(
                rootReducer, 
                initialState
            ),
            persistor: null
        }
    }

    const storage = require('redux-persist/lib/storage');

    const persistConfig = {
        key: 'root',
        storage: storage.default,
        whitelist: ['cart', 'inventory', 'user']
    };
    const persistedReducer = persistReducer(persistConfig, rootReducer);
    let store = createStore(
        persistedReducer,
        initialState,
        composeWithDevTools(
            applyMiddleware(thunk, ...middleware)
        )
    );

    sagaMiddleware.run(rootSaga);
    return {
        store,
        persistor: persistStore(store)
    }
}

const isServer = typeof window === 'undefined';
const __NEXT_REDUX_STORE__ = '__NEXT_REDUX_STORE__';

function getOrCreateStore(initialState) {
    // Always make a new store if server, otherwise state is shared between requests
    const { store, persistor } = initializeStore(initialState, isServer);
    if (isServer) {
    	return {
    		store, persistor
    	};
    } else if (!window[__NEXT_REDUX_STORE__]) {
        window[__NEXT_REDUX_STORE__] = store;
    }
    return {
    	store,
    	persistor
    };
}

function createNewStore(initialState) {
    // Always make a new store if server, otherwise state is shared between requests
    const { store, persistor } = initializeNewStore(initialState, isServer);
    if (isServer) {
        return {
            store, persistor
        };
    } else if (!window[__NEXT_REDUX_STORE__]) {
        window[__NEXT_REDUX_STORE__] = store;
    }
    return {
        store,
        persistor
    };
}

const ConfigureStore = App => {
    return class AppWithRedux extends React.Component {
        static async getInitialProps (appContext) {
            // Get or Create the store with `undefined` as initialState
            // This allows you to set a custom default initialState
            const { store, persistor } = (appContext && appContext.Component.initialLoadDone ? getOrCreateStore() : createNewStore());

            // Provide the store to getInitialProps of pages
            appContext.ctx.reduxStore = store

            let appProps = {}
            if (typeof App.getInitialProps === 'function') {
                appProps = await App.getInitialProps(appContext)
            }

            appContext.Component.initialLoadDone = true;

            return {
                ...appProps,
                persistor,
                initialReduxState: store.getState()
            }
        }

        constructor(props) {
            super(props)
            const { store, persistor } = createNewStore(props.initialReduxState);
            this.reduxStore = store;
            this.persistor = persistor;
        }

        render () {
        

            return <App {...this.props} reduxStore={this.reduxStore} persistor={this.persistor} />
        }
    }
};

export default ConfigureStore;
