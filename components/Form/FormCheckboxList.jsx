import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import MenuItem from "@material-ui/core/MenuItem";
import FormCheckbox from 'components/Form/FormCheckbox';
import List from '@material-ui/core/List';
import ListItemText from "@material-ui/core/ListItemText";
import Grid from '@material-ui/core/Grid';

const MenuProps = {
    PaperProps: {
        style: {
            border: '1px solid #FF9933'
        }
    }
};

class FormCheckboxList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            options: this.props.options || []
        };
    }

    toggleSelected(optValue) {
        var optIndex = this.props.value.indexOf(optValue);
        if (optIndex == -1) {
            this.props.value.push(optValue);
        } else {
            this.props.value.splice(optIndex);
        }
        this.forceUpdate();
    }

    render() {
        let options = this.props.options || [];
        return (
            (!options.length || options.length == 0) ? null :
            <Grid item xs={12}>
                <List>
                    {
                        options.map(option => (
                            <MenuItem key={option.label} value={option.value}>
                                <FormCheckbox checked={this.props.value.indexOf(option.value) > -1} onChange={() => this.toggleSelected(option.value)} />
                                <ListItemText primary={option.label} onClick={() => this.toggleSelected(option.value)} />
                            </MenuItem>
                        ))
                    }
                </List>
            </Grid>
        );
    }
}

const styles = theme => ({
    dropdownStyle: {
        border: '1px solid black',
        marginTop: '20px',
        width: '100%'
    }
});

FormCheckboxList.propTypes = {
    value: PropTypes.array.isRequired,
    options: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
};

export default withStyles(styles)(FormCheckboxList);
