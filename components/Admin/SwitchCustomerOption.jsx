import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { adminActions } from 'appRedux/actions/adminActions';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import Button from '@material-ui/core/Button';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import LoadingIndicator from 'components/UI/LoadingIndicator';

import axios from 'axios';
import Router from 'next/router';
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";

class SwitchCustomerOption extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            selectError: '',
            selectionChanged: false
        }
    }

    handleSelectCustomer = (customerId) => {
        this.setState({ isLoading: true, selectionChanged: false });

        axios.post('/get-user-info', { userID: customerId })
            .then(({ data: { error }, data: customerInfo }) => {
                if (error) throw error;
                if (customerInfo) {
                    customerInfo.isStaff = true;
                    localStorage.setItem('userInfo', JSON.stringify(customerInfo));
                    setTimeout(() => Router.push('/'), 3000);
                }
                this.setState({ isLoading: false, selectionChanged: true });
            })
            .catch(error => {
                //console.log('error', error);
                this.setState({ selectError: error.message });
            })
            .finally(() => {
                this.setState({ isLoading: false });
            });
    };

    handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setState({ selectionChanged: false });
    };

    render() {
        const { customerId, customerName, classes, theme } = this.props;

        return (
            <Grid item xs={12}>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    open={this.state.selectionChanged}
                    autoHideDuration={6000}
                    onClose={this.handleCloseSnackBar}
                    message={'You are now operating as ' + customerName + '. Please wait to be redirected to the store.'}
                    action={
                        <React.Fragment>
                            <IconButton size="small" aria-label="close" color="inherit" onClick={this.handleCloseSnackBar}>
                                <CloseIcon fontSize="small" />
                            </IconButton>
                        </React.Fragment>
                    }
                />
                <div className={classes.card}>
                    <Grid
                        container
                        item
                        spacing={6}
                        direction='row'
                    >
                        <Grid item>
                            <PersonPinIcon />
                        </Grid>
                        <Grid item xs>
                            <Typography
                                variant='overline'
                                color='textPrimary'
                            >
                                { customerName }
                            </Typography>
                        </Grid>
                        <Grid item xs={12} md={2}>
                            <Button
                                variant='outlined'
                                color='primary'
                                onClick={e => { this.handleSelectCustomer(customerId) }}
                            >
                                Select
                            </Button>
                        </Grid>
                    </Grid>
                </div>
            </Grid>
        );
    }
}

const styles = theme => ({
    hide: {
        display: 'none'
    },
    card: {
        display: 'flex'
    },
    image: {
        width: 150
    },
    quantity: {
        width: 50,
        marginRight: 20
    },
    icon: {
        marginTop: 10,
        height: 20,
        [theme.breakpoints.down('xs')]: {
            marginTop: 52,
        }
    },
    details: {
        display: 'flex',
        flexDirection: 'column'
    }
});

SwitchCustomerOption.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user,
        inventory: state.inventory,
        cart: state.cart
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(adminActions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(SwitchCustomerOption));
