import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import { cartActions } from 'appRedux/actions/cartActions';

class UnreorderableItem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { item } = this.props;

        var nonOrderableItem = {};
        nonOrderableItem.Name = item.Name;
        nonOrderableItem.MerchandiseID = item.MerchandiseID;
        nonOrderableItem.details = 'This item is not currently available and has not been reordered.';

        return (
            <Grid item className='cart-item' style={{ backgroundColor: 'tomato' }} >
                <Grid container spacing={6}>
                    <Grid className='detail' item container xs={7} sm={10}>
                        <Grid className='item-name' item xs={12} sm={8} lg={10}>
                            {item.Name}
                            <br />
                            {nonOrderableItem.details}
                        </Grid>
                        <Grid item xs={12} sm={2} lg={1}>
                        </Grid>
                        <Grid item xs={12} sm={2} lg={1}>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

const styles = theme => ({
    hide: {
        display: 'none'
    },
    card: {
        display: 'flex'
    },
    image: {
        width: 150
    },
    quantity: {
        width: 50,
        marginRight: 20
    },
    icon: {
        marginTop: 10,
        height: 20,
        [theme.breakpoints.down('xs')]: {
            marginTop: 52,
        }
    },
    details: {
        display: 'flex',
        flexDirection: 'column'
    }
});

UnreorderableItem.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user,
        inventory: state.inventory,
        cart: state.cart
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(cartActions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(UnreorderableItem));
