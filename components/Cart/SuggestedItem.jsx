import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { cartActions } from 'appRedux/actions/cartActions';
import FormSelectbox from 'components/Form/FormSelectbox';
import FormTextbox from 'components/Form/FormTextbox';
import FormButton from 'components/Form/FormButton';
import SalesLib from 'lib/SalesLib';
import { YeastElements } from "components/Store/Yeast/YeastCard";

class SuggestedItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: this.props.item.dispQuantity,
            options_packing: [{ label: 'Packing', value: 'packing' }],
            options_pack: [{ label: 'Pack', value: 'pack' }],
            defaulPacking: 'packing',
            defaulPack: 'pack'
        };
    }

    checkQuantity = item => {
        var quantity = parseFloat(item.OrderDetailQty);

        if (isNaN(quantity) || quantity <= 0) {
            return false;
        }

        //  Must be in increments of 1
        else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
            return false;
        }

        return true;
    };

    changeQuantity = event => {
        this.props.updateItem({ index: this.props.index, quantity: event.target.value });
        this.setState({ quantity: event.target.value })  
    };

    getCardBG = (item, i) => {
        var src = item.imageUrl;

        // Yeast (core + vault)
        if (SalesLib.SALESCATEGORY[0].includes(parseInt(item.salesCategory)) || item.salesCategory == 32) {
            if (item.isOrganic) {
                this.icon.push(YeastElements[998].icon);
                this.icon.push(YeastElements[item.salesCategory].icon);
                return (src ? src : YeastElements[998].img);
            } else {
                this.icon.push(YeastElements[item.salesCategory].icon);
                //return "/static/images/categories/Category-core.jpg";
                return (src ? src : YeastElements[item.salesCategory].img);
            }
        }

        // Enzymes & Nutrients
        else if (SalesLib.SALESCATEGORY[8].includes(parseInt(item.salesCategory))) {
            return (src ? src : "/static/images/categories/Category-ale.jpg");
        }

        // Services
        else if (SalesLib.SALESCATEGORY[11].includes(parseInt(item.salesCategory))) {
            return (src ? src : "/static/images/categories/Category-belgian.jpg");
        }

        // Lab Supplies
        else if (SalesLib.SALESCATEGORY[13].includes(parseInt(item.salesCategory))) {
            return (src ? src : "/static/images/categories/Category-wild.jpg");
        }

        // Education
        else if (SalesLib.SALESCATEGORY[14].includes(parseInt(item.salesCategory))) {
            return (src ? src : "/static/images/categories/Category-wine.jpg");
        }

        // Gift Shop
        else if (SalesLib.SALESCATEGORY[15].includes(parseInt(item.salesCategory))) {
            return (src ? src : "/static/images/categories/Category-vault.jpg");
        }

        // Beer & Alcohol
        else if (SalesLib.SALESCATEGORY[17].includes(parseInt(item.salesCategory))) {
            return (src ? src : "/static/images/categories/Category-beer.jpg");
        }
    };

    render() {
        const { classes, theme, item } = this.props;
        const qty = this.state.quantity;

        var addOnItem = {};
        addOnItem.Name = item.Name;
        addOnItem.salesCategory = item.salesCategory;
        addOnItem.dispQuantity = qty;
        addOnItem.OrderDetailQty = qty;
        addOnItem.MerchandiseID = item.MerchandiseID;
        addOnItem.details = item.Description;
        addOnItem.type = item.type;
        addOnItem.imageUrl = item.imageUrl;

        return (
            <Grid item className='cart-item' style={{ backgroundColor: 'lightGreen' }} >
                <Grid container spacing={6}>
                    <Grid
                        item
                        xs={5}
                        sm={2}
                        className='first-block'
                        style={{
                            backgroundImage: `url(${this.getCardBG(item)})`,
                            backgroundRepeat: 'no-repeat',
                            backgroundSize: 'cover',
                            backgroundPosition: 'center'
                        }}
                    >
                        <div className='code' />
                        <div className='name'>{this.props.item.Name}</div>
                    </Grid>
                    <Grid className='detail' item container xs={7} sm={10}>
                        <Grid className='item-name' item xs={12} sm={8} lg={10}>
                            {item.Name}
                            <br />
                            {item.details} {item.details_link}
                        </Grid>
                        <Grid item xs={12} sm={2} lg={1}>
                                                <TextField
                                                    id='quantity'
                                                    label='Quantity'
                                                    className={classes.quantity}
                                                    value={this.state.quantity}
                                                    onChange={this.changeQuantity}
                                                    type='number'
                                                    InputProps={{ inputProps: { min: 1} }}
                                                />
                        </Grid>
                        <Grid className='delete-button-items' item xs={12} sm={2} lg={1}>
                            <FormButton className='delete-button' text='ADD TO CART' onClick={() => this.props.addItem({ cartItem: addOnItem })} angleBlockRight={true} />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

const styles = theme => ({
    hide: {
        display: 'none'
    },
    card: {
        display: 'flex'
    },
    image: {
        width: 150
    },
    quantity: {
        width: 50,
        marginRight: 20
    },
    icon: {
        marginTop: 10,
        height: 20,
        [theme.breakpoints.down('xs')]: {
            marginTop: 52,
        }
    },
    details: {
        display: 'flex',
        flexDirection: 'column'
    }
});

SuggestedItem.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user,
        inventory: state.inventory,
        cart: state.cart
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(cartActions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(SuggestedItem));
