import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { messageActions } from 'appRedux/actions/messageActions';
import Alert from './Alert';

class Banner extends React.Component {
    state = {

    };

    displayAlert = (messageList = []) => {
        let alert = []
        messageList.map((message, i) => {
            alert.push(
                <Alert key={i} message={message} />
            )
        })
        return alert;
    }
    render() {
        const { messages } = this.props;

        return (
            <React.Fragment>
                {this.displayAlert(messages.banner)}
            </React.Fragment>
        );
    }
}


const mapStateToProps = state => {

    return {
        user: state.user,
        messages: state.messages,
        loading: state.loading
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(messageActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Banner);
