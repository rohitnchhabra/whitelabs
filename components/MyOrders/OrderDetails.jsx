import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Router from 'next/router'
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Snackbar from '@material-ui/core/Snackbar';

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import SalesLib from "lib/SalesLib";

import FormButton from "components/Form/FormButton";
import LabTestingForm from "../Checkout/OrderPlaced/LabTestingForm/LabTestingForm";

import axios from 'axios';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import Utils from 'lib/Utils';

class OrderDetails extends Component {
    state = {
        showWarning: false,
        showAlcoholPrompt: false,
        showLabTestingReprintForm: false
    };

    handleDialogClose() {
        this.props.closeDialog();
    }

    handleCloseTerms = () => {
        this.setState({
            showWarning: false, showAlcoholPrompt: false
        });
        this.handleDialogClose()
    };
    checkQuantity = cartItem => {
        let quantity = parseFloat(cartItem.OrderDetailQty);
        if (isNaN(quantity) || quantity <= 0) {
            return false;
        }
        //  Must be in increments of 1
        else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
            return false;
        }
        return true;
    };

    reOrder = (order) => {
        this.props.clearCart();
        this.setState({
            showWarning: false, showAlcoholPrompt: false
        });
        this.addCart(order)
    }

    addCart = (order) => {
        order.items.forEach(val => {
            try {
                let quantity = val.OrderDetailQty;

                let item = val;

                if (item.MerchandiseID != SalesLib.CRV_FEE_NSID) {
                    // Create cart item
                    let cartItem = {};
                    cartItem.isReorderItem = true;
                    cartItem.Name = String(item.Name);
                    cartItem.MerchandiseID = parseInt(item.MerchandiseID);
                    cartItem.salesCategory = parseInt(item.salesCategory);
                    cartItem.type = item.type;
                    cartItem.details = item.details;
                    cartItem.OrderDetailQty = parseFloat(quantity);
                    cartItem.dispQuantity = parseInt(quantity);
                    cartItem.isVaultPreorderItem = item.isVaultPreorderItem;

                    if (val.isAvailableForReorder && this.checkQuantity(cartItem)) {
                        cartItem.isAvailableForReorder = true;
                        this.props.addItem({ cartItem });
                    } else {
                        cartItem.isAvailableForReorder = false;
                        cartItem.OrderDetailQty = 0;
                        cartItem.Name = item.name;
                        cartItem.MerchandiseID = item.id;
                        this.props.addItem({ cartItem });
                    }
                }
            } catch (error) { }
        });
        Router.push('/cart')
        this.handleDialogClose()
    }

    addToCart(order) {
        if (this.props.cart.items.length > 0 || order.includesAlcohol) {
            this.setState({
                showWarning: (this.props.cart.items.length > 0),
                showAlcoholPrompt: order.includesAlcohol
            });
        } else {
            this.addCart(order)
        }
    }

    resendInvoice(order) {
        this.setState({ isLoading: true, emailSent: false });
        axios.post('/resend-invoice', { orderId: order.orderNum, email: this.props.user.email })
            .then(({ data: { error } }) => {
                if (error) throw error;
                this.setState({ isLoading: false, emailSent: true });
            })
            .catch(error => {
                this.setState({ isLoading: false });
                console.log(JSON.stringify(error));
            })
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    handleCloseLabTestingForm=()=>{
        this.setState({ showLabTestingReprintForm: false });

    }

    showLabTestForm = (order) => {
        this.setState({ showLabTestingReprintForm: true });
        this.setState({ showWarning: false, showAlcoholPrompt: false });

        order.orderPlacedData = [];
        order.orderPlacedData[0] = {};
        order.orderPlacedData[0].fields = {};
        order.orderPlacedData[0].fields.entityname = this.props.user.accountNumber;
        order.orderPlacedData[0].fields.tranid = order.orderNum;
        order.orderPlacedData[0].fields.email = this.props.user.email;
        order.orderPlacedData[0].fields.createddate = order.orderDate;
        order.orderPlacedData[0].fields.terms = '';

        order.orderPlacedData[0].sublists = {};
        order.orderPlacedData[0].sublists.item = {};
        order.orderPlacedData[0].sublists.item.lines = order.items;
    }

    handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        this.setState({ emailSent: false });
      };

    _renderSummary() {
        const { classes, order } = this.props;

        return (
            <Grid container>
                <Grid container spacing={6}>
                    <Grid item sm={3} container>
                        <img className="image" src="/static/images/yeast.png" />
                    </Grid>
                    <Grid item sm={9} container>
                        <Grid item xs={12}>
                            <Typography variant="subtitle1" color="textPrimary">
                                SUMMARY
              </Typography>
                            <div
                                style={{ borderTop: "solid 1.5px", borderColor: "#CCCCCC" }}
                            />
                        </Grid>
                        <Grid container spacing={6}>
                            <Grid item sm={6}>
                                <div className="block">
                                    {" "}
                                    <span className="label">Company: </span>{" "}
                                    {order && order.companyName}
                                </div>
                                <div className="block">
                                    {" "}
                                    <span className="label">Ordered From: </span>White Labs.
                </div>
                                <div className="block">
                                    {" "}
                                    <span className="label">Order Date: </span>
                                    {order.orderDate}
                                </div>
                                <div className="block">
                                    {" "}
                                    <span className="label">PO Number: </span>
                                    {order.ponumber}
                                </div>
                            </Grid>
                            <Grid item sm={6}>
                                <div className="block">
                                    {" "}
                                    <span className="label">Ship Total: </span>
                                    {SalesLib.CURRENCY_MAP[order.currency - 1].symbol}
                                    {Utils.priceFormat(order.shipTotal)}
                                </div>
                                {order.taxTotal &&
                                    <div className="block">
                                        {" "}
                                        <span className="label">Tax Total: </span>
                                        {SalesLib.CURRENCY_MAP[order.currency - 1].symbol}
                                        {Utils.priceFormat(order.taxTotal)}
                                    </div>
                                }
                                <div className="block">
                                    {" "}
                                    <span className="label">Order Total: </span>{" "}
                                    {SalesLib.CURRENCY_MAP[order.currency - 1].symbol}
                                    {order.totalPrice}
                                </div>
                                <div className="block">
                                    {" "}
                                    <span className="label">Order Status: </span> {order.status.replace('Approval', 'Fulfillment')}
                                </div>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <div
                                style={{ borderTop: "solid 1.5px", borderColor: "#CCCCCC" }}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }

    _renderItems() {
        const { classes, order } = this.props;

        if (!order.detailsLoaded) {
            order.canReorder = false;
            order.includesAlcohol = false;
            order.labTests = [];

            for (var i = 0; i < order.items.length; i++) {
                // Not a YMO2 item if no ID or ID is negative
                if (!order.items[i].id || order.items[i].id < 0) {
                    order.items[i].exclude = true;
                    continue;
                }

                var invItem = this.props.store.items.find(function (element) {
                    if (element.volID) {
                        return element.volID.includes(parseInt(order.items[i].id));
                    } else {
                        return false;
                    }
                });

                if (!invItem && order.items[i].id == SalesLib.CRV_FEE_NSID) {
                    invItem = {salesCategory: -1, Description: order.items[i].name};
                }

                if (invItem) {
                    order.items[i].isAvailableForReorder = (order.items[i].id != SalesLib.CRV_FEE_NSID);

                    if (order.items[i].id != SalesLib.CRV_FEE_NSID && order.items[i].id != SalesLib.GEL_PACK_NSID && order.items[i].quantity > 0) {
                        order.canReorder = true;
                    }

                    if (invItem.isAlcoholItem || SalesLib.SALESCATEGORY[17].includes(parseInt(invItem.salesCategory))) {
                        order.includesAlcohol = true;
                    }

                    var newObj = {};
                    order.items[i] = Object.assign(newObj, invItem, order.items[i]);

                    // Additional values required for cart items
                    order.items[i].dispQuantity = order.items[i].quantity;
                    order.items[i].OrderDetailQty = order.items[i].quantity;
                    order.items[i].MerchandiseID = order.items[i].id;
                    order.items[i].pricePerUnit = order.items[i].price;

                    // Yeast
                    if (
                        SalesLib.SALESCATEGORY[0].includes(parseInt(invItem.salesCategory))
                    ) {
                        if (
                            order.items[i].name.includes("Nano") ||
                            order.items[i].name.includes("2L") ||
                            order.items[i].name.includes("1.5L")
                        ) {
                            order.items[i].type = 1;
                            /*
                            order.items[i].details = 'PurePitch '
                                + (order.items[i].name.includes('Nano') ? 'Nano'
                                : (order.items[i].name.includes('2L') ? '2L'
                                : "1.5L"
                            ));
                            */
                            order.items[i].details = order.items[i].name;
                        } else if (order.items[i].name.includes("-HB")) {
                            order.items[i].type = 2;
                            order.items[i].details = "Homebrew Packs";
                        } else if (order.items[i].name.includes("Custom")) {
                            order.items[i].type = 5;
                            order.items[i].details =
                                order.items[i].quantity +
                                (invItem.strainCategory == 32 || invItem.strainCategory == 33
                                    ? " 1L Nalgene Bottle(s)"
                                    : "L Custom Pour");
                        } else if (
                            invItem.volID.indexOf(order.items[i].MerchandiseID) == 6
                        ) {
                            order.items[i].type = 1;
                            order.items[i].details = "1L Nalgene Bottle";
                        } else {
                            order.items[i].type = 3;
                            order.items[i].details = order.items[i].name;
                        }
                    }
                    // Enzymes & Nutrients
                    else if (
                        SalesLib.SALESCATEGORY[8].includes(parseInt(invItem.salesCategory))
                    ) {
                        order.items[i].type = 3;
                        order.items[i].details = "";
                    }
                    // Services
                    else if (
                        SalesLib.SALESCATEGORY[11].includes(parseInt(invItem.salesCategory))
                    ) {
                        order.items[i].type = 4;
                        order.items[i].details = invItem.Description;
                        order.labTests.push(order.items[i]);
                    }
                    // Lab Supplies
                    else if (
                        SalesLib.SALESCATEGORY[13].includes(parseInt(invItem.salesCategory))
                    ) {
                        order.items[i].type = 3;
                        order.items[i].details = "";
                    }
                    // Education
                    else if (
                        SalesLib.SALESCATEGORY[14].includes(parseInt(invItem.salesCategory))
                    ) {
                        order.items[i].type = 4;
                        order.items[i].details = "";
                    }
                    // Gift Shop
                    else if (
                        SalesLib.SALESCATEGORY[15].includes(parseInt(invItem.salesCategory))
                    ) {
                        order.items[i].type = 3;
                        order.items[i].details = invItem.Description;
                    } else {
                        order.items[i].type = 5;
                        order.items[i].details = invItem.Description;
                    }
                } else {
                    // Item is no longer eligible for ordering online apparently
                    order.items[i].isAvailableForReorder = false;
                    order.items[i].name += ' (not available for repeat order)';
                }
            }

            order.detailsLoaded = true;
        }

        return (
            <Grid item container spacing={2}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography variant="subtitle1" color="textPrimary">
                            ITEMS
                        </Typography>
                        <div style={{ borderTop: "solid 1.5px", borderColor: "#CCCCCC" }} />
                    </Grid>
                    <Grid item xs={12} style={{ marginTop: "10px" }}>
                        <Grid container>
                            <Grid item xs={6} container>
                                <div className="label">Item</div>
                            </Grid>
                            <Grid item xs={3} container>
                                <div className="label" style={{ margin: "0 auto" }}>
                                    Qty.
                </div>
                            </Grid>
                            <Grid item xs={3} container>
                                <div className="label" style={{ margin: "0 auto" }}>
                                    Price
                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                    {order.items.map((item, i) => {
                        return (
                            item.exclude ? null :
                            <Grid key={i} item xs={12} className="item-block">
                                <Grid container>
                                    <Grid item xs={6}>
                                        {item.name}
                                    </Grid>
                                    <Grid item xs={3}>
                                        <div style={{ textAlign: "center" }}> {item.quantity}</div>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <div style={{ textAlign: "center" }}>
                                            {" "}
                                            {SalesLib.CURRENCY_MAP[order.currency - 1].symbol}
                                            {item.price}
                                        </div>
                                    </Grid>
                                </Grid>
                            </Grid>
                        );
                    })}
                </Grid>
            </Grid>
        );
    }

    _renderPaymentShipping() {
        const { classes, order } = this.props;
        return (
            <Grid item container spacing={2}>
                <Grid container>
                    <Grid item xs={12}>
                        <Typography variant="subtitle1" color="textPrimary">
                            PAYMENT / SHIPPING
            </Typography>
                        <div style={{ borderTop: "solid 1.5px", borderColor: "#CCCCCC" }} />
                    </Grid>
                    <Grid container spacing={6}>
                        <Grid item sm={4}>
                            <div className="label block">Billing Address:</div>
                            {order.billaddress}
                            <br />
                        </Grid>
                        <Grid item sm={4}>
                            <div className="block">
                                {" "}
                                <span className="label">Ship date: </span>
                                {order.shipdate}
                            </div>
                            <div className="block">
                                {" "}
                                <span className="label">Delivery date: </span>
                                {order.deliverydate}
                            </div>
                            <div className="block">
                                {" "}
                                <span className="label">Ship Method: </span>
                                {order.shipmethod}
                            </div>
                            <div className="block">
                                {" "}
                                <span className="label">Tracking: </span>{" "}
                                {order.trackingNumber}
                            </div>
                        </Grid>
                        <Grid item sm={4}>
                            <div className="block label"> Ship Address:</div>
                            {order.shipaddress}
                            <br />
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }

    _renderShowWarning(order) {
        return (
            <Dialog open={this.state.showWarning || this.state.showAlcoholPrompt}>
                <DialogTitle id="alert-dialog-title">
                    {this.state.showWarning &&
                        <Typography>
                            <div>Your cart currently contains other item(s). Reordering will replace the item(s) in your cart with the item(s) from this order.</div>
                        </Typography>
                    }
                    {this.state.showAlcoholPrompt && this.state.showWarning && <br />}
                    {this.state.showAlcoholPrompt &&
                        <Typography>
                            <div>This order includes beer items. By reordering, you affirm that you are at least 21 years of age.</div>
                        </Typography>                    
                    }
                    <Typography>
                        <div><br />Do you want to proceed with reordering?</div>
                    </Typography>
                </DialogTitle>

                <DialogContent />
                <DialogActions>
                    <Button onClick={this.handleCloseTerms} color="primary">
                        Decline
          </Button>
                    <Button onClick={() => { this.reOrder(order) }} color="primary" autoFocus>
                        { order.includesAlcohol ? 'Affirm & Reorder' : 'Reorder' }
          </Button>

                </DialogActions>
            </Dialog>
        );
    }


    render() {
        const { classes, order } = this.props;
        return (
            <React.Fragment>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <Snackbar
                    anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                    }}
                    open={this.state.emailSent}
                    autoHideDuration={6000}
                    onClose={this.handleCloseSnackBar}
                    message="Invoice Sent"
                    action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={this.handleCloseSnackBar}>
                        <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                    }
                />
                <DialogContent id="my-order-details">
                    <div className={classes.close}>
                        <IconButton
                            color="inherit"
                            size="small"
                            aria-label="Menu"
                            onClick={() => this.handleDialogClose()}
                        >
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <div className="main-block">
                        <div className="order-number">Order # {order.orderNum}</div>
                        <Grid container spacing={6}>
                            <Grid item container spacing={6}>
                                {this._renderSummary()}
                                {this._renderItems()}
                                {this._renderPaymentShipping()}
                            </Grid>

                            <Grid container item className={classes.btnFlex}>
                                <Grid item style={{ marginBottom: "20px" }}>
                                    <div>
                                        <FormButton
                                            className="button-resend-invoice"
                                            text="Resend Invoice"
                                            onClick={() => {
                                                this.resendInvoice(order);
                                            }}
                                        />
                                    </div>
                                </Grid>
                                {!order.canReorder ? null :
                                    <Grid item>
                                        <div>
                                            <FormButton
                                                className="button-repeat-order"
                                                text="Repeat Order"
                                                onClick={() => {
                                                    this.addToCart(order);
                                                }}
                                            />
                                        </div>
                                    </Grid>
                                }
                                {order.labTests && order.labTests.length > 0 &&
                                    <Grid item>
                                        <div>
                                            <FormButton
                                                className="button-resend-invoice"
                                                text="Lab Test Form"
                                                onClick={() => {
                                                    this.showLabTestForm(order);
                                                }}
                                            />
                                        </div>
                                    </Grid>
                                }
                            </Grid>
                        </Grid>
                    </div>
                    {(this.state.showWarning || this.state.showAlcoholPrompt) && this._renderShowWarning(order)}                    
                    {this.state.showLabTestingReprintForm && <LabTestingForm user={this.props.user} order={this.props.order}
                    showForm={this.state.showLabTestingReprintForm}
                    handleCloseLabTestingForm={this.handleCloseLabTestingForm}
                    />}
                </DialogContent>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
  close: { position: "absolute", right: 0, top: 0 },
  btnFlex: {
    display: "flex",
    justifyContent: "flex-end",
    [theme.breakpoints.down("xs")]: {
      display: "flex",
      justifyContent: "center"
    }
  }
});

OrderDetails.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(OrderDetails);