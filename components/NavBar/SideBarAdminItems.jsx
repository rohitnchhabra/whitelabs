import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Link from 'next/link';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PhoneIcon from '@material-ui/icons/Phone';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import PeopleIcon from '@material-ui/icons/People';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import ReorderIcon from '@material-ui/icons/Reorder';
import GradientIcon from '@material-ui/icons/Gradient';

import { withStyles } from '@material-ui/core/styles';

import { userActions } from 'appRedux/actions/userActions';

class SideBarAdminItems extends Component {

    render() {
        const { classes, theme } = this.props;
        return (
            <div>
                <Link /* prefetch */ href='/phonelist' style={{ visibility: 'hidden', display: 'none' }}>
                    <ListItem button style={{ visibility: 'hidden', display: 'none' }}>
                        <ListItemIcon>
                            <PhoneIcon />
                        </ListItemIcon>
                        <ListItemText primary='Phone List' />
                    </ListItem>
                </Link>

                <Link /* prefetch */ href='/switchcustomer'>
                    <ListItem button >
                        <ListItemIcon>
                            <SupervisedUserCircleIcon />
                        </ListItemIcon>
                        <ListItemText primary='Switch Customer' />
                    </ListItem>
                </Link>

                <Link /* prefetch */ href='/customerlocator' style={{ visibility: 'hidden', display: 'none' }}>
                    <ListItem button style={{ visibility: 'hidden', display: 'none' }}>
                        <ListItemIcon>
                            <PeopleIcon />
                        </ListItemIcon>
                        <ListItemText primary='Customer Locator' />
                    </ListItem>
                </Link>

                <Link /* prefetch */ href='/salesorderlookup' style={{ visibility: 'hidden', display: 'none' }}>
                    <ListItem button style={{ visibility: 'hidden', display: 'none' }}>
                        <ListItemIcon>
                            <BookmarkBorderIcon />
                        </ListItemIcon>
                        <ListItemText primary='Sales Order Lookup' />
                    </ListItem>
                </Link>

                <Link /* prefetch */ href='/csrorderhistory' style={{ visibility: 'hidden', display: 'none' }}>
                    <ListItem button style={{ visibility: 'hidden', display: 'none' }}>
                        <ListItemIcon>
                            <ReorderIcon />
                        </ListItemIcon>
                        <ListItemText primary='CSR Order History' />
                    </ListItem>
                </Link>

                <Link /* prefetch */ href='/analytics' style={{ visibility: 'hidden', display: 'none' }}>
                    <ListItem button style={{ visibility: 'hidden', display: 'none' }}>
                        <ListItemIcon>
                            <GradientIcon />
                        </ListItemIcon>
                        <ListItemText primary='Analytics' />
                    </ListItem>
                </Link>

            </div>
        );
    }
}

const styles = theme => ({
    applinks: {
        display: 'none',
        [theme.breakpoints.down('sm')]: {
            display: 'flex'
        }
    }
});

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(userActions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(SideBarAdminItems));
