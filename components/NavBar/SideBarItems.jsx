import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Link from 'next/link';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import ReceiptIcon from '@material-ui/icons/Receipt';
import FeedbackIcon from '@material-ui/icons/Feedback';
import LabIcon from '@material-ui/icons/Assessment';
import GroupWorkIcon from '@material-ui/icons/GroupWork';
import ChatIcon from '@material-ui/icons/Chat';
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew';
import Dialpad from '@material-ui/icons/Dialpad';
import Group from '@material-ui/icons/Group';
import Home from '@material-ui/icons/Home';
import { withStyles } from '@material-ui/core/styles';

import { userActions } from 'appRedux/actions/userActions';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import SupportDialog from 'components/Store/Menu/SupportDialog';
import LabTestingDialog from 'components/Store/Menu/LabTestingDialog';
import ChatWindow from 'components/Chat/ChatWindow';
import SalesDriver from 'components/MyAccount/SalesDriver';
import Router from 'next/router';

class SideBarItems extends Component {
    state = {
        openSupportDialog: false,
        userSidebarMenu: false,
        openLabDialog: false,
        openChatWindow: false,
        openSalesDriver: false
    };

    handleClickSupport = () => {
        this.setState({ openSupportDialog: true });
    };

    handleClickLaboratory = () => {
        this.setState({ openLabDialog: true });
    };

    handleClickChat = () => {
        this.setState({ openChatWindow: true });
    };

    handleLeaveItem = () => {
        this.setState({ openSupportDialog: false, openLabDialog: false, openChatWindow: false, openSalesDriver: false });
    };

    handleSalesDriver = () => {
        this.setState({ openSalesDriver: true });
    }

    getSupportDialogContent = () => {
        return <SupportDialog closeDialog={this.handleLeaveItem} />;
    }

    getLaboratoryDialogContent = () => {
        return <LabTestingDialog closeDialog={this.handleLeaveItem} />;
    }

    getChatWindowContent = () => {
        return <ChatWindow closeDialog={this.handleLeaveItem} chatMode='popup' />;
    };

    componentDidMount() {        
        if (this.props.user.id) {
            this.setState({ userSidebarMenu: true });
        }
    }

    componentDidUpdate(prevProps) {   
        if ((!this.props.user.id && prevProps.user.id) && (prevProps.user.id !== this.props.user.id)) {
            this.setState({
                userSidebarMenu: false
            });
        }
    }
    handleLogoutClick=()=>{    
        this.props.userLogout();
        if(window.location.pathname==="/myaccount"){
            Router.push("/login")
        }
    }
    render() {
        const { classes, theme, user: { salesDriverLinks } } = this.props;
        const dev = process.env.NODE_ENV !== "production";
        return (
            <div>
                <Link /* prefetch */ href='/login'>
                    <ListItem button className={this.state.userSidebarMenu ? 'hide-usermenu' : null}>
                        <ListItemIcon>
                            <PowerSettingsNew />
                        </ListItemIcon>
                        <ListItemText primary='Sign In' />
                    </ListItem>
                </Link>
                <Link /* prefetch */ href='/myaccount'>
                    <ListItem button className={!this.state.userSidebarMenu ? 'hide-usermenu' : null}>
                        <ListItemIcon>
                            <AccountBoxIcon />
                        </ListItemIcon>

                        <ListItemText primary='My Account' />
                    </ListItem>
                </Link>
                <Link /* prefetch */ href='/myorders'>
                    <ListItem button className={!this.state.userSidebarMenu ? 'hide-usermenu' : null}>
                        <ListItemIcon>
                            <ReceiptIcon />
                        </ListItemIcon>

                        <ListItemText primary='My Orders' />
                    </ListItem>
                </Link>
                <Link /* prefetch */ href='/feedback'>
                    <ListItem button >
                        <ListItemIcon>
                            <FeedbackIcon />
                        </ListItemIcon>
                        <ListItemText primary='Give Feedback' />
                    </ListItem>
                </Link>

                {salesDriverLinks && salesDriverLinks.length > 0 &&
                    <ListItem onClick={this.handleSalesDriver} style={{ cursor: 'pointer' }}>
                        <ListItemIcon>
                            <GroupWorkIcon />
                        </ListItemIcon>
                        <ListItemText primary='PartnersFirst' />
                    </ListItem>
                }

                <Link /* prefetch */ href='/'>
                    <ListItem className={classes.applinks}>
                        <ListItemIcon>
                            <Home />
                        </ListItemIcon>
                        <ListItemText primary='Store' />
                    </ListItem>
                </Link>

                <Link /* prefetch */ href='/calculator'>
                    <ListItem className={classes.applinks}>
                        <ListItemIcon>
                            <Dialpad />
                        </ListItemIcon>
                        <ListItemText primary='Calculator' />
                    </ListItem>
                </Link>

                <ListItem className={classes.applinks} onClick={this.handleClickSupport} style={{ cursor: 'pointer' }}>
                    <ListItemIcon>
                        <Group />
                    </ListItemIcon>
                    <ListItemText primary='Support' />
                </ListItem>

                <ListItem className={classes.applinks} onClick={this.handleClickChat} style={{ cursor: 'pointer' }}>
                    <ListItemIcon>
                        <ChatIcon />
                    </ListItemIcon>
                    <ListItemText primary='Chat' />
                </ListItem>

                {
                    dev ?
                        <Link /* prefetch */ href='/info' className={!this.state.userSidebarMenu ? 'landscape applinks' : 'applinks'}>
                            <ListItem className={classes.applinks}>
                                <ListItemIcon>
                                    <LabIcon />
                                </ListItemIcon>
                                <ListItemText primary='QC' />
                            </ListItem>
                        </Link>
                        :
                        <ListItem className={!this.state.userSidebarMenu ? 'landscape applinks' : 'applinks'} onClick={this.handleClickLaboratory} style={{ cursor: 'pointer' }}>
                            <ListItemIcon>
                                <LabIcon />
                            </ListItemIcon>
                            <ListItemText primary='QC' />
                        </ListItem>
                }

                <ListItem
                    button
                    onClick={
                        this.handleLogoutClick
                    }
                    className={!this.state.userSidebarMenu ? 'hide-usermenu landscape ' : 'landscape'}
                >
                    <ListItemIcon>
                        <PowerSettingsNew />
                    </ListItemIcon>
                    <ListItemText primary='Log Out' />
                </ListItem>

                <Dialog open={this.state.openSupportDialog} onClose={this.handleLeaveItem} aria-labelledby='form-dialog-title'>
                    {this.getSupportDialogContent(this.state.item)}
                </Dialog>

                <Dialog open={this.state.openLabDialog} onClose={this.handleLeaveItem} aria-labelledby='form-dialog-title'>
                    {this.getLaboratoryDialogContent()}
                </Dialog>

                <Dialog open={this.state.openChatWindow} onClose={this.handleLeaveItem} aria-labelledby='form-dialog-title'>
                    {this.getChatWindowContent()}
                </Dialog>

                <Dialog
                    open={this.state.openSalesDriver}
                    onClose={this.handleLeaveItem}
                    maxWidth={'lg'}
                >
                    <SalesDriver user={this.props.user} closeDialog={this.handleLeaveItem} />
                </Dialog>
            </div>
        );
    }
}

const styles = theme => ({
    applinks: {
        display: 'none',
        [theme.breakpoints.down('sm')]: {
            display: 'flex'
        }
    }
});

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(userActions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(SideBarItems));
