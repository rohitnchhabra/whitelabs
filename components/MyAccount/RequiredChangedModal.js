import React, { useState, useEffect } from "react";
import Dialog from "@material-ui/core/Dialog";
import { bindActionCreators } from "redux";
import DialogContent from "@material-ui/core/DialogContent";
import { withStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import Typography from "@material-ui/core/Typography";
import { messageActions } from "appRedux/actions/messageActions";
import { userActions } from 'appRedux/actions/userActions';

import "./myaccount.scss"
import Router from "next/router";

// import MuiAlert from '@material-ui/lab/Alert';
let route = ["/login"];

function RequiredChangedModal(props) {
  const [showDialog, setShowDialog] = useState(false);

  useEffect(() => {
    if (route.find(val => val === window.location.pathname)) {
      setShowDialog(false);
    }
  }, []);


  useEffect(() => {
    // Disable this function in release for now since we seem to be having a problem with password resets from YMO2
    if(false && props.user.isPasswordChangeRequired && !props.loading.isLoading){
      setShowDialog(true);
    }else{
      setShowDialog(false);
    }
   
  }, [props])

    const handleClick = () => {
        props.showChangePasswordModal();
        Router.push("/myaccount");
    };

    const handleLogOut = () => {
        this.props.userLogout();
        Router.push("/login");
    };
  
  return (
    <div>
      <Dialog open={showDialog && (!props.loading.isLoading && props.loading.type !== 'loadingInventory')}>
        <DialogContent id="change-passwords">
          <div className={props.classes.alert}>
            For your security, you must change your password after requesting a reset. Please click the link below to proceed.
          </div>
          <div className="main-block">
            <div className="order-number">
              {/* <Alert severity="error">This is an error message!</Alert> */}
            </div>
            <div></div>
          </div>
          <div className={props.classes.change} onClick={handleClick}>
            Change Password
          </div>
          <div className={props.classes.change} onClick={handleLogOut}>
            Log Out
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const styles = theme => ({
  alert: {
    color: "rgb(97, 26, 21)",
    backgroundColor: "rgb(253, 236, 234)",
    padding: "16px 26px",
    borderRadius: "4px"
  },
  change: {
    textDecoration: "underline",
    textAlign: "center",
    marginTop: "40px",
    cursor: "pointer",
    "&:hover": {
      color: "blue"
    }
  }
});

const mapStateToProps = state => {
  return {
    user: state.user,
    messages: state.messages,
    loading:state.loading
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...messageActions, ...userActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles, { withTheme: true })(RequiredChangedModal));
