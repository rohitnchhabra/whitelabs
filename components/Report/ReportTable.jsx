import React from 'react';
import { Grid } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import Typography from '@material-ui/core/Typography';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import CustomTableCellInfo from './CustomTableCellInfo';
import CustomTableCell from './CustomTableCell';
import TableBody from '@material-ui/core/TableBody';
import { checkData } from './ReportTablePDF';

const createData = (Analysis, Results, Minimum, Maximum, UOM) => (
    {
        Analysis,
        Results,
        Minimum,
        Maximum,
        UOM
    }
);

export const createRows = (data) => (
    data ? [
        !data.WildGrowthResults ? (data.WildGrowthAnalysis ? createData(data.WildGrowthAnalysis) : null) : createData(data.WildGrowthAnalysis, data.WildGrowthResults, data.WildGrowthMinimum, data.WildGrowthMaximum, data.WildGrowthUOM),
        !data.AerobicGrowthResults ? null : createData(data.AerobicGrowthAnalysis, data.AerobicGrowthResults, data.AerobicGrowthMinimum, data.AerobicGrowthMaximum, data.AerobicGrowthUOM),
        !data.AnaerobicGrowthResults ? null : createData(data.AnaerobicGrowthAnalysis, data.AnaerobicGrowthResults, data.AnaerobicGrowthMinimum, data.AnaerobicGrowthMaximum, data.AnaerobicGrowthUOM),
        !data.SacchGrowthResults ? null : createData(data.SacchGrowthAnalysis, data.SacchGrowthResults, data.SacchGrowthMinimum, data.SacchGrowthMaximum, data.SacchGrowthUOM),
        !data.pH ? null : createData(data.pHAnalysis, data.pH, (data.pHLowerLimit ? data.pHLowerLimit : data.pHLowerlimit), data.pHUpperLimit, data.pHUnits),
        !data.CellCount ? null : createData(data.CellCountAnalysis, data.CellCount, data.CellCountLowerLimit, data.CellCountUpperLimit, data.CellCountUnits),
        !data.Gravity || !data.GravityAnalysis ? null : createData(data.GravityAnalysis, data.Gravity, data.GravityLowerLimit, data.GravityUpperLimit, data.GravityUnits),
    ] : []
);

// Hack: Save header logo info here in case multiple lots are run with the same headers
var headerLogoInfo = {
    HeaderLogoLeftHeight: 100,
    HeaderLogoLeftWidth: 150,
    HeaderLogoCenterHeight: 100,
    HeaderLogoCenterWidth: 140,
    HeaderLogoRightHeight: 100,
    HeaderLogoRightWidth: 100
};

const handleHeaderLogoLeftLoad = (event, reportData) => {
    reportData.HeaderLogoLeftHeight = event.target.height;
    reportData.HeaderLogoLeftWidth = event.target.width;

    headerLogoInfo.HeaderLogoLeftHeight = event.target.height;
    headerLogoInfo.HeaderLogoLeftWidth = event.target.width;
}

const handleHeaderLogoCenterLoad = (event, reportData) => {
    reportData.HeaderLogoCenterHeight = event.target.height;
    reportData.HeaderLogoCenterWidth = event.target.width;

    headerLogoInfo.HeaderLogoCenterHeight = event.target.height;
    headerLogoInfo.HeaderLogoCenterWidth = event.target.width;
}

const handleHeaderLogoRightLoad = (event, reportData) => {
    reportData.HeaderLogoRightHeight = event.target.height;
    reportData.HeaderLogoRightWidth = event.target.width;

    headerLogoInfo.HeaderLogoRightHeight = event.target.height;
    headerLogoInfo.HeaderLogoRightWidth = event.target.width;
}

const ReportTable = ({classes, reportData}) => {
    const rows = createRows(reportData).filter(function (row) {
        return row != null;
    });

    const hasHeaderImages = (
        reportData.HeaderLogoCenterURL || reportData.HeaderLogoLeftURL || reportData.HeaderLogoRightURL ||
        reportData.LeftImageFileContents || reportData.CenterImageFileContents || reportData.RightImageFileContents
    );

    var leftImgSrc = null;
    var centerImgSrc = null;
    var rightImgSrc = null;

    if (hasHeaderImages) {
        // Retrieve logo header info from cache if we have it, in case the header logos are the same and the Load event doesn't fire this time
        reportData.HeaderLogoLeftHeight = headerLogoInfo.HeaderLogoLeftHeight;
        reportData.HeaderLogoLeftWidth = headerLogoInfo.HeaderLogoLeftWidth;

        reportData.HeaderLogoCenterHeight = headerLogoInfo.HeaderLogoCenterHeight;
        reportData.HeaderLogoCenterWidth = headerLogoInfo.HeaderLogoCenterWidth;

        reportData.HeaderLogoRightHeight = headerLogoInfo.HeaderLogoRightHeight;
        reportData.HeaderLogoRightWidth = headerLogoInfo.HeaderLogoRightWidth;

        if (reportData.LeftImageFileContents) {
            leftImgSrc = 'data:image;base64,' + reportData.LeftImageFileContents;
        } else if (reportData.HeaderLogoLeftURL) {
            leftImgSrc = reportData.HeaderLogoLeftURL;
        }

        if (reportData.CenterImageFileContents) {
            centerImgSrc = 'data:image;base64,' + reportData.CenterImageFileContents;
        } else if (reportData.HeaderLogoCenterURL) {
            centerImgSrc = reportData.HeaderLogoCenterURL;
        }

        if (reportData.RightImageFileContents) {
            rightImgSrc = 'data:image;base64,' + reportData.RightImageFileContents;
        } else if (reportData.HeaderLogoRightURL) {
            rightImgSrc = reportData.HeaderLogoRightURL;
        }
    }

    return (
        <React.Fragment>
            <Grid container justify='center' item xs={12}>
                <Typography align='center' variant='h5' style={{ color: '#FE7424', fontWeight: 700 }}>{`${reportData.QCSheetForm}`} </Typography>
            </Grid>
            <Grid container justify='center' item xs={12}>
                <Typography align='center' variant='h5' style={{ color: '#FE7424', fontWeight: 700 }}>{`${reportData.ManufLocation}`}</Typography>
                <Typography align='center' variant='h5' style={{ color: '#FE7424', fontWeight: 700 }}>:&nbsp;</Typography>
                <Typography align='center' variant='h5' style={{ color: '#FE7424', fontWeight: 700 }}>{`${reportData.Location}`}</Typography>
            </Grid>
            {
                hasHeaderImages &&
                <div style={{ margin: 'auto', textAlign: 'center' }}>
                    <Grid container spacing={6}>
                        <Grid item xs={12}>
                            <Grid container justify='center' spacing={4} >
                                <Grid item xs={12} sm={6} md={4} lg={3} >
                                    <img src={leftImgSrc} style={{ maxHeight: '100px' }}
                                        onLoad={(event) => handleHeaderLogoLeftLoad(event, reportData)}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6} md={4} lg={3} >
                                    <img src={centerImgSrc} style={{ maxHeight: '100px' }}
                                        onLoad={(event) => handleHeaderLogoCenterLoad(event, reportData)}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6} md={4} lg={3} >
                                    <img src={rightImgSrc} style={{ maxHeight: '100px' }}
                                        onLoad={(event) => handleHeaderLogoRightLoad(event, reportData)}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </div>
            }
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <CustomTableCellInfo colSpan={2}>
                                {`${reportData.StrainQCSheetForm} ${reportData.Strain} ${reportData.StrainDescription}`}
                            </CustomTableCellInfo>
                            <CustomTableCellInfo
                                colSpan={3}
                                align="left"
                            >
                                {`${reportData.LotNumberQCSheet} ${reportData.LotNumber}`}
                            </CustomTableCellInfo>
                        </TableRow>
                        <TableRow>
                            <CustomTableCellInfo
                                colSpan={2}>{`${reportData.QCReleaseDateText} ${reportData.QCReleaseDate}`}
                            </CustomTableCellInfo>
                            <CustomTableCellInfo
                                colSpan={3}
                                align="left"
                            >
                                {`${reportData.BestBeforeDate} ${reportData.ExpirationDate}`}
                            </CustomTableCellInfo>
                        </TableRow>
                        <TableRow>
                            <CustomTableCell rowSpan="2" align="center">{`${reportData.AnalysisPerformed}`}</CustomTableCell>
                            <CustomTableCell rowSpan="2" align="center" style={{ whiteSpace: 'nowrap' }}>{`${reportData.LotResults}`}</CustomTableCell>
                            <CustomTableCell colSpan='2' align="center" style={{ textAlign: 'center' }}>{`${reportData.AcceptableRange}`}</CustomTableCell>
                            <CustomTableCell rowSpan="2" align="center">{`${reportData.UOM}`}</CustomTableCell>
                        </TableRow>
                        <TableRow>
                            <CustomTableCell align="center">{`${reportData.Minimum}`}</CustomTableCell>
                            <CustomTableCell align="center">{`${reportData.Maximum}`}</CustomTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {
                            rows.map(row => (
                                !row ? null :
                                    row.Analysis && !row.Results && !row.Minimum && !row.Maximum && !row.UOM ?  
                                    <TableRow className={classes.row} key={row.Analysis}>
                                            <CustomTableCell align="center" component="th" scope="row" colSpan="5" style={{ textAlign: "center" }}>{checkData(row.Analysis)}</CustomTableCell>
                                    </TableRow>
                                        : row.Minimum && !row.Maximum ?
                                            <TableRow className={classes.row} key={row.Analysis}>
                                                <CustomTableCell align="center" component="th" scope="row">{checkData(row.Analysis)}</CustomTableCell>
                                                <CustomTableCell align="center"><div dangerouslySetInnerHTML={{ __html: checkData(row.Results) }} /></CustomTableCell>
                                                <CustomTableCell align="center" colSpan="2" style={{ textAlign: "center" }}><div dangerouslySetInnerHTML={{ __html: checkData(row.Minimum) }} /></CustomTableCell>
                                                <CustomTableCell align="center"><div dangerouslySetInnerHTML={{ __html: checkData(row.UOM) }} /></CustomTableCell>
                                            </TableRow>
                                            : !row.Minimum && row.Maximum ? 
                                                <TableRow className={classes.row} key={row.Analysis}>
                                                    <CustomTableCell align="center" component="th" scope="row">{checkData(row.Analysis)}</CustomTableCell>
                                                    <CustomTableCell align="center"><div dangerouslySetInnerHTML={{ __html: checkData(row.Results) }} /></CustomTableCell>
                                                    <CustomTableCell align="center" colSpan="2" style={{ textAlign: "center" }}><div dangerouslySetInnerHTML={{ __html: checkData(row.Maximum) }} /></CustomTableCell>
                                                    <CustomTableCell align="center"><div dangerouslySetInnerHTML={{ __html: checkData(row.UOM) }} /></CustomTableCell>
                                                </TableRow>
                                                :
                                                    <TableRow className={classes.row} key={row.Analysis}>
                                                        <CustomTableCell align="center" component="th" scope="row">{checkData(row.Analysis)}</CustomTableCell>
                                                        <CustomTableCell align="center"><div dangerouslySetInnerHTML={{__html: checkData(row.Results)}}/></CustomTableCell>
                                                        <CustomTableCell align="center"><div dangerouslySetInnerHTML={{__html: checkData(row.Minimum)}}/></CustomTableCell>
                                                        <CustomTableCell align="center"><div dangerouslySetInnerHTML={{__html: checkData(row.Maximum)}}/></CustomTableCell>
                                                        <CustomTableCell align="center"><div dangerouslySetInnerHTML={{__html: checkData(row.UOM)}}/></CustomTableCell>
                                                    </TableRow>
                                )
                            )}
                        {reportData.STA1 && reportData.STA1Analysis &&
                            <TableRow className={classes.row}>
                                <CustomTableCell align="center" component="th" scope="row">
                                    {checkData(reportData.STA1Analysis)}
                                </CustomTableCell>
                                <CustomTableCell
                                    colSpan={3}
                                    align="center"
                                    style={{ textAlign: "center" }}
                                >
                                    {checkData(reportData.STA1)}
                                </CustomTableCell>
                                <CustomTableCell align="center">{checkData(reportData.STA1Units)}</CustomTableCell>
                            </TableRow>
                        }
                    </TableBody>
                </Table>
            </Paper>
            <Grid container direction='column' className={classes.tabFooter}>
                <p>
                    <Typography variant='h4' style={{color:'#f28411', fontFamily: 'Roboto'}}>{`${reportData.ManufLocation}`}</Typography>
                    <Typography style={{ fontFamily: 'Roboto' }}>{`${reportData.ManufLocationText}`}</Typography>
                </p>
                <p>
                    <Typography variant='h4' style={{color:'#f28411',fontFamily: 'Roboto'}}>{`${reportData.AboutWLProcess}`}</Typography>
                    <Typography style={{fontFamily: 'Roboto'}}>{`${reportData.AboutWLProcessText}`}</Typography>
                </p>
            </Grid>
        </React.Fragment>
    )
};

export default ReportTable;
