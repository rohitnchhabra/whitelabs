import { checkData } from './ReportTablePDF';


export const color = {
    gray: '#96A3B0',
    orange: '#FE7424',
    grayText: '#ccc',
    white: '#fff',
    black: '#000',
    lineColor: '#B5C5C9'
};

const styles = {
    titlePDF: {
        halign: 'center',
        fillColor: '#fff',
        textColor: color.orange,
        minCellHeight: 24,
        fontSize: 20,
        font: 'RobotoBold',
        fontStyle: 'normal',
    },
    headersTopPDF: {
        fillColor: color.gray,
        font: 'Roboto',
        fontStyle: 'normal',
        cellPadding: { top: 2, right: 0, bottom: 2, left: 1 },
        lineWidth: 0.1,
        lineColor: color.gray,
    },
    headersBottomPDF: {
        fillColor: color.gray,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        cellPadding: { top: 2, right: 0, bottom: 2, left: 1 },
        valign: 'top',
        lineWidth: 0.1,
        lineColor: color.gray,
    },
    itemsHeadersPDF: {
        halign: 'center',
        fillColor: color.orange,
        minCellHeight: 8,
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        lineWidth: 0.1,
        lineColor: color.orange,
    },
    footerTopPDF: {
        fillColor: '#fff',
        font: 'RobotoBold',
        fontStyle: 'normal',
        cellPadding: 1
    },
    footerBotomPDF: {
        fillColor: '#fff',
        fontFamily: 'Roboto',
        fontStyle: 'normal',
        cellPadding: 1
    }
};

export const titlePDF = (reportData) => ({
    content: `${(reportData.QCSheetForm).toUpperCase()}\n${(reportData.ManufLocation).toUpperCase()}: ${(reportData.Location).toUpperCase()}`,
    colSpan: 5,
    rowSpan: 1,
    styles: styles.titlePDF
});

export const headerImagesPDF = (reportData) => [
    {
        content: `${'HI'}`,
        colSpan: 1,
        rowSpan: 1,
        styles: styles.titlePDF
    },
    {
        content: `${'HI'}`,
        colSpan: 3,
        rowSpan: 1,
        styles: styles.titlePDF
    },
    {
        content: `${'HI'}`,
        colSpan: 1,
        rowSpan: 1,
        styles: styles.titlePDF
    }
];

export const headersTopPDF = (reportData) => [
    {
        content: `${reportData.StrainQCSheetForm} ${reportData.Strain} ${reportData.StrainDescription}`,
        colSpan: 2,
        rowSpan: 1,
        styles: styles.headersTopPDF
    },
    {
        content: `${reportData.LotNumberQCSheet} ${reportData.LotNumber}`,
        colSpan: 3,
        rowSpan: 1,
        styles: styles.headersTopPDF
    }
];

export const headersBottomPDF = (reportData) => [
    {
        content: `${reportData.QCReleaseDateText} ${reportData.QCReleaseDate}`,
        colSpan: 2,
        rowSpan: 1,
        styles: styles.headersBottomPDF
    },
    {
        content: `${reportData.BestBeforeDate} ${reportData.ExpirationDate}`,
        colSpan: 3,
        rowSpan: 1,
        styles: styles.headersBottomPDF,
    }
];

export const itemsHeadersPDF = (reportData) => [
    [
        {
            content: `${reportData.AcceptableRange}`,
            colSpan: 1,
            rowSpan: 2,
            styles: { ...styles.itemsHeadersPDF, cellWidth: 46 }
        },
        {
            content: `${reportData.LotResults}`,
            colSpan: 1,
            rowSpan: 2,
            styles: { ...styles.itemsHeadersPDF, minCellWidth: 30 }
        },
        {
            content: `${reportData.AcceptableRange}`,
            colSpan: 2,
            rowSpan: 1,
            styles: { ...styles.itemsHeadersPDF }
        },
        {
            content: `${reportData.UOM}`,
            colSpan: 1,
            rowSpan: 2,
            styles: { ...styles.itemsHeadersPDF, cellWidth: 46 }
        }
    ],
    [
        {
            content: `${reportData.Minimum}`,
            colSpan: 1,
            rowSpan: 1,
            styles: styles.itemsHeadersPDF
        },
        {
            content: `${reportData.Maximum}`,
            colSpan: 1,
            rowSpan: 1,
            styles: styles.itemsHeadersPDF
        }
    ]
];

export const footerTopPDF = (reportData) => [
    [
        {
            content: `${reportData.ManufLocation}`,
            colSpan: 5,
            rowSpan: 1,
            styles: { ...styles.footerTopPDF, textColor: color.orange }
        }
    ],
    [
        {
            content: `${reportData.ManufLocationText}`,
            colSpan: 5,
            styles: { ...styles.footerBotomPDF, fillColor: color.white }
        }
    ],
];

export const footerBotomPDF = (reportData) => [
    [
        {
            content: `${checkData(reportData.AboutWLProcess)}`,
            colSpan: 5,
            rowSpan: 1,
            styles: { ...styles.footerTopPDF, textColor: color.orange }
        }
    ],
    [
        {
            content: `${reportData.AboutWLProcessText}`,
            colSpan: 5,
            styles: { ...styles.footerBotomPDF, fillColor: color.white }
        }
    ]
];
