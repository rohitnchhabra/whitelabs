import withStyles from '@material-ui/core/styles/withStyles';
import TableCell from '@material-ui/core/TableCell';


const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: '#f28411',
        color: theme.palette.common.white,
        fontSize: 18,
        font: 'Roboto'
    },
    body: {
        fontSize: 14,
        border: '1px solid #c1c9d0',
        font: 'Roboto'
    },
}))(TableCell);

export default CustomTableCell;
