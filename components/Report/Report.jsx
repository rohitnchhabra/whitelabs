import React from 'react';
import { Grid } from '@material-ui/core';
import { connect } from 'react-redux';
import withStyles from '@material-ui/core/styles/withStyles';
import ReportTablePDF from './ReportTablePDF';
import ReportForm from './ReportForm';
import { isReportDataSelector } from '../../redux/selectors/report';
import { isNoResultsFoundSelector } from '../../redux/selectors/report';
import LoadingIndicator from '../UI/LoadingIndicator';
import Typography from "@material-ui/core/Typography";

const styles = theme => ({
    QCHeader: {
        marginTop: 20
    },
    QCForm: {
        marginTop: 40
    },
    QCContainer: {
        marginTop: 20
    },
    print: {
        '@media print': {
            display: 'none'
        },
    }
});

class Report extends React.Component {
    state = { package: 4 };

    render() {
        const { classes } = this.props;
        console.log(this.props.reportData);
        return (
            <Grid container item lg={12} justify='center' className={classes.QCContainer}>
                <Grid item lg={10}>
                    <ReportForm/>
                    <LoadingIndicator visible={this.props.isReportData} label={'Please Wait'} />
                    {
                        this.props.reportData
                            ? <ReportTablePDF reportData={this.props.reportData} />
                            : this.props.isNoResultsFound
                                ? <Typography style={{ fontSize: 'larger', fontWeight: 'bolder', marginTop: '20px' }}>
                                    No results were found for this lot and packaging. Please check the lot number and try again.
                                 </Typography>
                                : null
                    }
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = (state) => ({
    reportData: state.report.reportData,
    isReportData: isReportDataSelector(state),
    isNoResultsFound: isNoResultsFoundSelector(state)
});

export default connect(mapStateToProps)(
    withStyles(styles, { withTheme: true })(Report)
);
