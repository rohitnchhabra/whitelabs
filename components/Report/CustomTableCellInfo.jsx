import withStyles from '@material-ui/core/styles/withStyles';
import TableCell from '@material-ui/core/TableCell';


const CustomTableCellInfo = withStyles(theme => ({
    head: {
        backgroundColor: '#a9a9a9',
        color: theme.palette.common.white,
        fontSize: 24,
        font: 'Roboto'
    },
}))(TableCell);

export default CustomTableCellInfo;
