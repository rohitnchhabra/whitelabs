import { FormControl, Grid, MenuItem, Select, TextField, FormHelperText } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import React from 'react';
import { bindActionCreators } from 'redux';
import { reportActions } from '../../redux/actions/reportActions';
import { connect } from 'react-redux';
import withStyles from '@material-ui/core/styles/withStyles';
import { Field, reduxForm } from 'redux-form';
import { lotNumberSelector, packageTypeSelector, REPORT_FORM } from '../../redux/selectors/report';
import Typography from '@material-ui/core/Typography';


const styles = theme => ({
    QCHeader: {
        marginTop: 20
    },
    QCForm: {
        marginTop: 40
    },
    QCContainer: {
        marginTop: 20
    },
    print: {
        '@media print': {
            display:'none'
        },
    }
});

const FieldInput = (props) => (
    <TextField {...props.input} variant="outlined" />
);

/*
const FieldOptions = (props) => (
    <Select
        value={props.input.value}
        onChange={props.input.onChange}
    >
        <MenuItem value="4">Homebrew Pack</MenuItem>
        <MenuItem value="0">PurePitch Nano</MenuItem>
        <MenuItem value="1">PurePitch 1.5L</MenuItem>
        <MenuItem value="2">PurePitch 2L</MenuItem>
        <MenuItem value="5">Slant</MenuItem>
        <MenuItem value="6">1L Nalgene Bottle</MenuItem>
        <MenuItem value="3">Custom Pour</MenuItem>
    </Select>
);
*/

const FieldOptions = (props) => (
    <Select
        value={props.input.value}
        onChange={props.input.onChange}
    >
        <MenuItem value="0">Homebrew</MenuItem>
        <MenuItem value="1">Professional</MenuItem>
    </Select>
);

class ReportForm extends React.Component{
    state = { package: 1 };
    render() {
        const { classes, handleSubmit } = this.props;
        return (
            <form onSubmit={handleSubmit}>
                <Grid container >
                    <Grid item lg={4} container justify='center' className={classes.print} alignItems='center'>
                        <Typography style={{marginRight: 10}}>Lot Number: </Typography>
                        <Field
                            name="lotNumber"
                            component={FieldInput}
                            type="text"
                            placeholder="Lot Number"
                        />
                    </Grid>
                    <Grid item lg={4}  container justify='center' className={classes.print}  alignItems='center'>
                        <Typography style={{marginRight: 10}}>Package Type: </Typography>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <Field
                                name="packageType"
                                component={FieldOptions}
                                type="checkbox"
                                aria-describedby="packagingDefault"
                            />
                            <FormHelperText id="packagingDefault">Default is Professional.</FormHelperText>
                        </FormControl>
                    </Grid>
                    <Grid container item lg={4} justify='center' alignItems='center' className={classes.print}>
                        <Grid item lg={12}>
                            <Button
                                fullWidth
                                variant='contained'
                                color='primary'
                                className={classes.submit}
                                onClick={() => {this.props.getReport()}}
                            >
                                Prepare Report
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
            </form>
        )
    }
}


const mapDispatchToProps = (dispatch) => bindActionCreators({ ...reportActions }, dispatch);

const mapStateToProps = (state) => ({
    packageType: packageTypeSelector(state),
    lotNumber: lotNumberSelector(state)
});

export default connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles, { withTheme: true })(reduxForm({
        form: REPORT_FORM
    })(ReportForm))
);
