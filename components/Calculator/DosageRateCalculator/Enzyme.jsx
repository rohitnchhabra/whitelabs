import { FormControl, FormControlLabel, Grid, Popover, RadioGroup, Typography } from '@material-ui/core';
import React from 'react';
import StyledRadio from './StyledRadio';
import { Field } from 'redux-form';


const RadioField = (props) => (
    <RadioGroup
        {...props.input}
    >
        <FormControlLabel
            value='5'
            control={<StyledRadio />}
            label='Clarity Ferm: 1L, 10L, 20L'
        />
        <FormControlLabel
            value='1'
            control={<StyledRadio />}
            label='Clarity Ferm (Homebrew 10mL)'
        />
    </RadioGroup>
);

const Enzyme = ({ classes, ...props }) => {
    const [state, setState] = React.useState(null);
    return (
        <fieldset style={{ border: 'none', borderBottom: '1px solid #af9b53' }}>
            <Grid container direction='row' className={classes.legend}>
                <Typography className={classes.calculatorHeaders}>
                    Select the type of Enzyme
                </Typography>
                <Typography
                    className={classes.buttonInfo}
                    component='span'
                    aria-owns={open ? 'mouse-over-popover' : undefined}
                    aria-haspopup='true'
                    onClick={(event) => setState(event.currentTarget)}
                >
                    ?
                </Typography>
                <Popover
                    id='mouse-over-popover'
                    open={!!state}
                    anchorEl={state}
                    onClose={() => setState(null)}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                >
                    <Typography className={classes.typography}>
                        Please select if you are using Clarity Ferm in Commercial or
                        Homebrew quantities.
                    </Typography>
                </Popover>
            </Grid>
            <FormControl component='fieldset'>
                <Field
                    {...props}
                    name='enzymeType'
                    component={RadioField}
                    type='radio'
                    value={props.enzymeType}
                />
            </FormControl>
        </fieldset>
    );
};

export default Enzyme;
