import { Grid, Popover, TextField, Typography } from '@material-ui/core';
import React from 'react';
import { Field } from 'redux-form';

const WortVolumeField = (props) => {
    return (
        <TextField
            style={{ width: '100%' }}
            variant='outlined'
            placeholder='Wort volume'
            onChange={(e) => handleWortVolume(e, props)}
            {...props.input}
        />
    )
};

const WortGravityField = (props) => {
    return (
        <TextField
            style={{ width: '100%' }}
            variant='outlined'
            placeholder='Wort original gravity'
            onChange={(e) => handleWortGravity(e, props)}
            {...props.input}
        />
    )
};

const MaltPercentageField = (props) => {
    return (
        <TextField
            style={{ width: '100%' }}
            variant='outlined'
            placeholder='Malt Percentage'
            onChange={(e) => handleMaltValue(e, props)}
            name='maltValue'
            {...props.input}
        />
    )
};

const handleMaltValue = (value, props) => {
    props.handleMaltValue(value);
};

const handleWortGravity = (value, props) => {
    props.handleWortGravity(value);
};

const handleWortVolume = (value) => {
    props.handleWortVolume(value);
};

const OtherData = ({ classes, ...props }) => {
    const [malt, setMalt] = React.useState(null);
    const [wortG, setWortG] = React.useState(null);
    const [wortV, setWortV] = React.useState(null);
    return (
        <Grid container>
            <fieldset id='percentage' style={{ border: 'none' }}>
                <Grid container direction='row' item xs={12} lg={12}>
                    <Grid item xs={12} lg={4}>
                        <Grid container direction='row' className={classes.legend}>
                            <Typography className={classes.calculatorHeaders}>
                                Malt Percentage
                            </Typography>
                            <Typography
                                className={classes.buttonInfo}
                                component='span'
                                aria-owns={open ? 'mouse-over-popover' : undefined}
                                aria-haspopup='true'
                                onClick={(event) => setMalt(event.currentTarget)}
                            >
                                ?
                            </Typography>
                            <Popover
                                id='mouse-over-popover'
                                open={!!malt}
                                anchorEl={malt}
                                onClose={() => setMalt(null)}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                            >
                                <Typography className={classes.typography}>
                                    Percent of recipe that is malt (compared to adjuncts)
                                </Typography>
                            </Popover>
                        </Grid>
                        <Grid
                            container
                            item
                            lg={12}
                            alignItems='center'
                            justify='center'
                            spacing={2}
                        >
                            <Grid
                                container
                                item
                                lg={12}
                                alignItems='center'
                                justify='space-between'
                                direction='row'
                            >
                                <Grid item xs={10}>
                                    <Field
                                        {...props}
                                        name='maltValue'
                                        component={MaltPercentageField}
                                        type='text'
                                    />
                                </Grid>
                                <Grid item xs={2}>
                                    <Typography style={{ marginLeft: 10 }}>%</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={12} lg={4}>
                        <Grid container direction='row' className={classes.legend}>
                            <Typography className={classes.calculatorHeaders}>
                                Wort original gravity
                            </Typography>
                            <Typography
                                className={classes.buttonInfo}
                                component='span'
                                aria-owns={open ? 'mouse-over-popover' : undefined}
                                aria-haspopup='true'
                                onClick={(event) => setWortG(event.currentTarget)}
                            >
                                ?
                            </Typography>
                            <Popover
                                id='mouse-over-popover'
                                open={!!wortG}
                                anchorEl={wortG}
                                onClose={() => setWortG(null)}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                            >
                                <Typography className={classes.typography}>
                                    Enter the starting gravity in degrees Plato. Specific gravity
                                    will need to be converted to Plato.
                                </Typography>
                            </Popover>
                        </Grid>
                        <Grid container item lg={12} alignItems='center' justify='space-between' directin='row'>
                            <Grid item xs={10}>
                                <Field
                                    {...props}
                                    name='wortGravity'
                                    component={WortGravityField}
                                    type='text'
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <Typography>&deg;P</Typography>
                            </Grid>
                        </Grid>
                        <Typography className='detail'>
                            (convert specific gravity to Plato)
                        </Typography>
                    </Grid>
                    <Grid item xs={12} lg={4}>
                        <Grid container direction='row' className={classes.legend}>
                            <Typography className={classes.calculatorHeaders}>
                                Wort volume
                            </Typography>
                            <Typography
                                className={classes.buttonInfo}
                                component='span'
                                aria-owns={open ? 'mouse-over-popover' : undefined}
                                aria-haspopup='true'
                                onClick={(event) => setWortV(event.currentTarget)}
                            >
                                ?
                            </Typography>
                            <Popover
                                id='mouse-over-popover'
                                open={!!wortV}
                                anchorEl={wortV}
                                onClose={() => setWortV(null)}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                            >
                                <Typography className={classes.typography}>
                                    Total mLs of Clarity Ferm to dose at beginning of fermentation.
                                </Typography>
                            </Popover>
                        </Grid>
                        <Grid container item lg={12} alignItems='center' justify='space-between' direction='row'>
                            <Grid item xs={10}>
                                <Field
                                    {...props}
                                    name='wortVolume'
                                    component={WortVolumeField}
                                    type='text'
                                />
                            </Grid>
                            <Grid item xs={2}>
                                <Typography>BBL</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </fieldset>
        </Grid>
    );
};

export default OtherData;
