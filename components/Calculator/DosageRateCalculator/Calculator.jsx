import React from 'react';
import CardHeader from '../../UI/Card/CardHeader';
import {
    Typography,
    Grid,
    Paper,
    Popover
} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import OtherData from './OtherData';
import AmountOfBeer from './AmountOfBeer';
import KindOfBeer from './KindOfBeer';
import Enzyme from './Enzyme';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { calculatorActions } from 'appRedux/actions/calculatorActions';
import { reduxForm } from 'redux-form';
import {
    beerTypeSelector,
    calcResultSelector,
    CALCULATOR,
    enzymeSelector,
    homebrewTypeSelector,
    maltValueSelector,
    wortGravitySelector,
    wortVolumeSelector
} from '../../../redux/selectors/calculator';
import { compose } from 'redux';

class Calculator extends React.Component {
    state = {
        elem: null
    };

    handleEnzymeType = (value) => {
        this.props.setEnzymeType({ enzymeType: value.target.value });
        this.props.calculate();
    };

    handleBeerType = (value) => {
        this.props.setBeerType({ beerType: value.target.value });
        this.props.calculate();
    };

    handleHomebrewType = (value) => {
        this.props.setHomebrewType({ homebrewType: value.target.value });
        this.props.calculate();
    };

    handleMaltValue = (value) => {
        this.props.setMaltValue({ maltValue: value.target.value });
        this.props.calculate();
    };

    handleWortGravity = (value) => {
        this.props.setWortGravity({ wortGravity: value.target.value });
        this.props.calculate();
    };

    handleWortVolume = (value) => {
        this.props.setWortVolume({ wortVolume: value.target.value });
        this.props.calculate();
    };

    render() {
        const { classes, enzymeType, calculate, calcResult, handleSubmit } = this.props;
        return (
            <Grid
                container
                direction='column'
                justify='center'
                alignItems='center'
            >
                <Grid item lg={10}>
                    <Grid>
                        <CardHeader
                            color='primary'
                            className='card-header-down'
                        >
                            <Typography
                                color='secondary'
                                variant='h4'
                                className='calc-small-variant'
                                align='left'
                            >
                                CALCULATOR
                            </Typography>
                        </CardHeader>
                    </Grid>
                    <Paper>
                        <form onSubmit={handleSubmit} onChange={() => calculate()}>
                            <Enzyme
                                {...this.props}
                            />
                            <KindOfBeer
                                {...this.props}
                            />
                            {
                                enzymeType === '1' &&
                                <AmountOfBeer
                                    {...this.props}
                                />
                            }
                            {
                                enzymeType === '5' &&
                                <OtherData
                                    {...this.props}
                                    handleMaltValue={this.handleMaltValue}
                                    handleWortGravity={this.handleWortGravity}
                                    handleWortVolume={this.handleWortVolume}
                                />
                            }
                        </form>
                        <Grid id='dosage-sidebar'>
                            <Paper style={{ marginTop: 40 }}>
                                <div id='quantity-dose'>
                                    <Grid container direction='row' className={classes.legend}>
                                        <Grid
                                            container
                                            direction='row'
                                            className={classes.legend}
                                        >
                                            <Typography className={classes.calculatorHeaders}>
                                                Quantity to Dose
                                            </Typography>
                                            <Typography
                                                className={classes.buttonInfo}
                                                component='span'
                                                aria-owns={open ? 'mouse-over-popover' : undefined}
                                                aria-haspopup='true'
                                                onClick={(event) => this.setState({ elem: event.currentTarget })}
                                            >
                                                ?
                                            </Typography>
                                            <Popover
                                                id='mouse-over-popover'
                                                open={!!this.state.elem}
                                                anchorEl={this.state.elem}
                                                onClose={() => this.setState({ elem: null })}
                                                anchorOrigin={{
                                                    vertical: 'bottom',
                                                    horizontal: 'left',
                                                }}
                                                transformOrigin={{
                                                    vertical: 'top',
                                                    horizontal: 'left',
                                                }}
                                            >
                                                <Typography className={classes.typography}>
                                                    Total mls of Clarity Ferm to dose at beginning of fermentation.
                                                </Typography>
                                            </Popover>
                                        </Grid>
                                    </Grid>
                                    <Grid
                                        container
                                        justify='center'
                                        alignItems='center'
                                        item
                                        xs={12}
                                        className={classes.calcButton}
                                        onClick={() => calculate()}
                                    >
                                        <div className={classes.amount}>
                                            <span className='number'>{calcResult ? calcResult + ' ' : 'Click to Calculate '}</span>
                                            <span className='units'>mL</span>
                                        </div>
                                    </Grid>
                                </div>
                            </Paper>
                        </Grid>
                    </Paper>
                </Grid>
            </Grid>
        );
    }
}

const styles = theme => ({
    whiteSpace: {
        whiteSpace: 'normal'
    },
    instList: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    instSubheading: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center'
    },
    handling: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
    },
    hoverBold: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        color: '#f28531',
        marginTop: '20px',
        marginBottom: '100px',
        '&:hover': {
            fontWeight: 'bolder',
            color: '#ff9933',
            cursor: 'pointer',
            fontSize: '30px',
            backgroundColor: '#ffd9b3'
        }
    },
    legend: {
        fontSize: 20,
        lineHeight: 2,
        fontFamily: 'bebas',
        fontWeight: 'normal',
        margin: '0 0 10px'
    },
    formRow: {
        backgroundColor: '#ff9933',
        padding: 5,
        marginTop: 5,
    },
    formRowInput: {
        display: 'inline-block',
        position: 'absolute',
        top: 5,
        left: 8,
        content: '',
        width: 15,
        height: 15,
        backgroundImage: '/sites/all/themes/whitelabs_2015/images/formelements.png'
    },
    buttonInfo: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: 24,
        height: 24,
        borderRadius: '50%',
        backgroundColor: '#ff9933',
        fontSize: 20,
        color: '#fff',
        marginLeft: 10,
        cursor: 'pointer'
    },
    calculatorHeaders: {
        fontSize: 20,
        fontFamily: 'Bebas Neue',
        fontWeight: 'normal',
        margin: '0 0 10px'
    },
    typography: {
        maxWidth: 400
    },
    amount: {
        color: '#fff',
        fontWeight: 'normal',
        fontSize: 30
    },
    calcButton: {
        cursor: 'pointer',
        backgroundColor: '#f28533',
        height: 80
    }
});

const mapStateToProps = state => ({
    enzymeType: enzymeSelector(state),
    wortVolume: wortVolumeSelector(state),
    beerType: beerTypeSelector(state),
    homebrewType: homebrewTypeSelector(state),
    wortGravity: wortGravitySelector(state),
    maltValue: maltValueSelector(state),
    calcResult: calcResultSelector(state)
});

const mapDispatchToProps = dispatch => bindActionCreators({ ...calculatorActions }, dispatch);

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    reduxForm({
        form: CALCULATOR,
        enableReinitialize: true
    }),
    withStyles(styles, { withTheme: true })
)(Calculator);
