import React from 'react';
import CardHeader from '../../UI/Card/CardHeader';
import {
    Typography,
    Grid,
    Paper,
    Popover
} from '@material-ui/core';
import withStyles from '@material-ui/core/styles/withStyles';
import OtherData from './OtherData';
import AmountOfBeer from './AmountOfBeer';
import KindOfBeer from './KindOfBeer';
import Enzyme from './Enzyme';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { calculatorActions } from 'appRedux/actions/calculatorActions';
import { reduxForm } from 'redux-form';
import {
    beerTypeSelector,
    calcResultSelector,
    CALCULATOR,
    enzymeSelector,
    homebrewTypeSelector,
    maltValueSelector,
    wortGravitySelector,
    wortVolumeSelector
} from '../../../redux/selectors/calculator';
import { compose } from 'redux';

export class ClarityFermCalculatorForm extends React.Component {
    render() {
        const { classes, enzymeType, calculate, calcResult, handleSubmit } = this.props;

        return (
            <form onSubmit={handleSubmit}>
                <Enzyme
                    {...this.props}
                />
                <KindOfBeer
                    {...this.props}
                />
                {
                    enzymeType === '1' &&
                    <AmountOfBeer
                        {...this.props}
                    />
                }
                {
                    enzymeType === '5' &&
                    <OtherData
                        {...this.props}
                    />
                }
            </form>
        );
    }
}

ClarityFermCalculatorForm = reduxForm({
    form: 'clarityFermCalculatorForm',
    enableReinitialize: true
})(ClarityFermCalculatorForm);

// Super important that connect comes AFTER reduxForm decorator
ClarityFermCalculatorForm = connect(
    (state) => ({
        formInfo: state.formInfo,
        initialValues: { ...state.myInitialValues }
    })
)(ClarityFermCalculatorForm);

export default ClarityFermCalculatorForm;