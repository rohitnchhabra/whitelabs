import { FormControlLabel, Grid, Popover, RadioGroup, Typography } from '@material-ui/core';
import React from 'react';
import StyledRadio from './StyledRadio';
import { Field } from 'redux-form';


const RadioField = (props) => (
    <RadioGroup
        {...props.input}
    >
        <FormControlLabel
            value='4.00'
            control={<StyledRadio />}
            label='A: Dry hop beers or beers over 40 IBU'
        />
        <FormControlLabel
            value='2.50'
            control={<StyledRadio />}
            label='B: Beers using the enzyme for haze stabilization purposes'
        />
        <FormControlLabel
            value='3.00'
            control={<StyledRadio />}
            label='C: Enzyme used for gluten reduction purposes in a beer with &lt;= 20% wheat'
        />
        <FormControlLabel
            value='6.00'
            control={<StyledRadio />}
            label='D: Enzyme used for gluten reduction purposes in a beer with &gt; 20% wheat'
        />
    </RadioGroup>
);

const KindOfBeer = ({ classes, ...props }) => {
    const [state, setState] = React.useState(null);
    return (
        <fieldset style={{ border: 'none', borderBottom: '1px solid #af9b53' }} id='beer-type'>
            <Grid container direction='row' className={classes.legend}>
                <Typography
                    className={classes.calculatorHeaders}
                >
                    Select the Kind of Beer
                </Typography>
                <Typography
                    className={classes.buttonInfo}
                    component='span'
                    aria-owns={open ? 'mouse-over-popover' : undefined}
                    aria-haspopup='true'
                    onClick={(event) => setState(event.currentTarget)}
                >
                    ?
                </Typography>
                <Popover
                    id='mouse-over-popover'
                    open={!!state}
                    anchorEl={state}
                    onClose={() => setState(null)}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'left',
                    }}
                >
                    <Typography className={classes.typography}>
                        Select the type of beer that most closely resembles your objective.
                    </Typography>
                </Popover>
            </Grid>
            <Field
                {...props}
                name='beerType'
                component={RadioField}
                type='radio'
                value={props.beerType}
            />
        </fieldset>
    );
};

export default KindOfBeer;
