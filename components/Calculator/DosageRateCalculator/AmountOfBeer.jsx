import { FormControlLabel, Grid, Popover, RadioGroup, Typography } from '@material-ui/core';
import React from 'react';
import StyledRadio from './StyledRadio';
import { Field } from 'redux-form';


const RadioField = (props) => (
    <RadioGroup
        {...props.input}
    >
        <FormControlLabel
            value='5'
            control={<StyledRadio />}
            label='5 Gallons'
        />
        <FormControlLabel
            value='10'
            control={<StyledRadio />}
            label='10 Gallons'
        />
        <FormControlLabel
            value='15'
            control={<StyledRadio />}
            label='15 Gallons'
        />
        <FormControlLabel
            value='20'
            control={<StyledRadio />}
            label='20 Gallons'
        />
    </RadioGroup>
);


const AmountOfBeer = ({ classes, ...props }) => {
    const [state, setState] = React.useState(null);
    return (
        <fieldset id='homebrew-type' style={{ border: 'none' }}>
            <div style={{ border: 'none' }} className='fieldset-inner'>
                <Grid
                    container
                    direction='row'
                    className={classes.legend}
                >
                    <Typography
                        className={classes.calculatorHeaders}
                    >
                        Select the Amount of Beer
                    </Typography>
                    <Typography
                        className={classes.buttonInfo}
                        component='span'
                        aria-owns={open ? 'mouse-over-popover' : undefined}
                        aria-haspopup='true'
                        onClick={(event) => setState(event.currentTarget)}
                    >
                        ?
                    </Typography>
                    <Popover
                        id='mouse-over-popover'
                        open={!!state}
                        anchorEl={state}
                        onClose={() => setState(null)}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'left',
                        }}
                    >
                        <Typography className={classes.typography}>
                            Select the amount of beer that you are brewing.
                        </Typography>
                    </Popover>
                </Grid>
                <Field
                    {...props}
                    name='homebrewType'
                    component={RadioField}
                    type='radio'
                    value={props.homebrewType}
                />
            </div>
        </fieldset>
    );
};

export default AmountOfBeer;
