import { Radio } from '@material-ui/core';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import React from 'react';


const StyledRadio = (props) => (
    <Radio
        disableRipple
        color="default"
        checkedIcon={<CheckCircleOutlineIcon />}
        icon={<RadioButtonUncheckedIcon />}
        {...props}
    />
);

export default StyledRadio;
