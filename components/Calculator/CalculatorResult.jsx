import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// custom
import ResultSummary from './ResultSummary';
import ResultItem from './ResultItem';

class CalculatorResult extends Component {

    getResultItems = () => {
        const { id, result } = this.props;
        const yeastStrains = this.props.inventory.items.filter(item => {
            if (item.partNum == 'WLP709') {
                var x = 1;
            }

            if (id) {
                if (item.volID[0] == Number(id))
                {
                    return true;
                }
                return false;
            } else {
                //item.IsPrivate can evidently be either a string (NS ID of the owner, always private) or an array of boolean values (may be private)
                //The "for" loop handles both scenarios
                if(result.ultraFerm) {
                    if(item.partNum === 'WLE4100') {
                        return true;
                    }
                    return false;
                } else if(result.isFanmax) {
                    if(item.partNum === 'WLN2000'){
                        return true;
                    }
                    return false;
                } else if (result.isHomebrewer && [2, 3, 5, 6, 8, 32].indexOf(item.salesCategory) > -1 && item.volID[4]) {
                    if (item.IsPrivate) {
                        if (item.IsPrivate.length) {
                            for (var p = 0; p < item.IsPrivate.length; p++) {
                                if (item.IsPrivate[p]) {
                                    return false;
                                }
                            }
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return true;
                    }
                } else if (!result.isHomebrewer && [2, 3, 5, 6, 7, 8].indexOf(item.salesCategory) > -1 && !item.volID[6] && item.volID[2]) {
                    if (item.IsPrivate) {
                        if (item.IsPrivate.length) {
                            for (var p = 0; p < item.IsPrivate.length; p++) {
                                if (item.IsPrivate[p]) {
                                    return false;
                                }
                            }
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        })

        return yeastStrains.map((item, i) => (
            <ResultItem
                key={i}
                item={item}
                closeDialog={this.props.closeDialogMain}
                result={result}
            />
        ));
    };

    render() {
        const { result: { isCustom } } = this.props;
        const isPPNG = (this.props.result ? this.props.result.isPPNG : false);
        var resultItems = (!isCustom ? this.getResultItems() : null);
        var resultHeader = null;

        if (!isPPNG && resultItems && resultItems.length > 1) {
            for (var i = 0; i < resultItems.length; i++) {
                if (resultItems[i].props.item.isOrganic) {
                    resultHeader = 'Customers ordering from the USA generally select regular strains. Customers ordering from Copenhagen, '
                        + 'you must select organic if that option is available. '
                        + 'USA customers and those ordering from the USA have the option of selecting organic, but please be aware of longer shipping times.';
                    break;
                }
            }
        }

        return (
            <div id='calculator-result-box'>
                <ResultSummary {...this.props} />
                {resultHeader}
                {isPPNG ? null : resultItems}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        messages: state.messages,
        inventory: state.inventory
    };
};

export default connect(
    mapStateToProps,
)(CalculatorResult);
