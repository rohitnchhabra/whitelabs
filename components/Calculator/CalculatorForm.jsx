import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import _times from 'lodash/times';

import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import NavBarLayout from 'components/NavBar/NavBarLayout';
import Card from 'components/UI/Card/Card.jsx';
import CardBody from 'components/UI/Card/CardBody.jsx';
import CardHeader from 'components/UI/Card/CardHeader.jsx';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import { Formik, Form, Field } from 'formik';

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import StarIcon from '@material-ui/icons/Star';

// custom

import FormSelectbox from 'components/Form/FormSelectbox';
import FormButton from 'components/Form/FormButton';
import FormCheckbox from 'components/Form/FormCheckbox';

import SalesLib from 'lib/SalesLib';
import ReactGA from 'react-ga';

import Calculator from 'components/Calculator/DosageRateCalculator/Calculator';

const FormikErrorMessage = ({ error }) => {
    return error ? <div className='error'>{error}</div> : null;
};

class CalculatorForm extends Component {
    constructor(props) {
        super(props);
        ReactGA.initialize('UA-40641680-2');

        this.state = {
            custom: 'PurePitch',
            isHomebrewer: false,
            instDialog: false
        };
    }

    toggleCustom = (value) => {
        this.setState({ custom: value });
    }

    handlingContent = () => {
        this.setState({ custom: 'Handling' })
    }
    componentDidMount() {
        window.scrollTo(0, 0)
    }
    toggleIsHomebrewer = ({ setFieldValue }) => {
        const isHomebrewer = !this.state.isHomebrewer;
        let volChoices, gravChoices;

        if (isHomebrewer) {
            setFieldValue('volChoices', SalesLib.homebrewVolChoices);
            setFieldValue('gravChoices', SalesLib.homebrewGravChoices);
            setFieldValue('volUnit', 'L');
            setFieldValue('volVal', '20');

        } else {
            setFieldValue('volChoices', SalesLib.volChoices);
            setFieldValue('gravChoices', SalesLib.gravChoices);
            setFieldValue('volUnit', 'BBL');
            setFieldValue('volVal', '1');
        }

        setFieldValue('gravUnit', 'PLA');
        setFieldValue('gravVal', 'less than 13.5');

        this.setState({ isHomebrewer });
    }

    calculate = (values, { setErrors }) => {
        const errors = this.validate(values);
        if (_isEmpty(errors)) {
            this.props.onCalculate({ ...values, ...this.state });
        } else {
            setErrors(errors);
        }
    };

    changeUnit = (e, { setFieldValue, values }, type) => {
        const choices = _get(values, `${type}Choices`)
        let unit = e.target.value;
        const options = choices[unit];
        setFieldValue(`${type}Val`, options[0]);
        setFieldValue(`${type}Unit`, unit);
    }

    changePPNGUnit = (e, { setFieldValue, values }, type) => {
        console.log(e, { setFieldValue, values }, type, "changePPNGUnit");
        const choices = _get(values, `${type}Choices`)
        let unit = e.target.value;
        const options = choices[choices.findIndex((data) => data.value === unit)];
        setFieldValue(`${type}Val`, options.value);
        setFieldValue(`${type}Unit`, options.label);
    }

    openInstDialog = () => {
        this.setState({
            instDialog: true
        });
    };

    closeInstDialog = () => {
        this.setState({
            instDialog: false
        });
    };

    validate = values => {
        var errors = {};

        if (this.state.custom == 'Re-Pitching') {
            if (!values.startingGravity) {
                errors.startingGravity = 'Starting gravity is required';
            }
            else if (isNaN(values.startingGravity) && values.startingGravity <= 0) {
                errors.startingGravity = 'Invalid value';
            }

            if (!values.targetPitchRate) {
                errors.targetPitchRate = 'Target pitch rate is required';
            }
            else if (isNaN(values.targetPitchRate) && values.targetPitchRate <= 0) {
                errors.targetPitchRate = 'Invalid value';
            }

            if (!values.volume) {
                errors.volume = 'Volume is required';
            }
            else if (isNaN(values.volume) && values.volume <= 0) {
                errors.targetPitchRate = 'Invalid value';
            }

            if (!values.viability) {
                errors.viability = 'Viability is required';
            }
            else if (isNaN(values.viability) && values.viability <= 0) {
                errors.viability = 'Invalid value';
            }

            if (!values.cellCount) {
                errors.cellCount = 'Cell count is required';
            }
            else if (isNaN(values.cellCount) && values.cellCount <= 0) {
                errors.cellCount = 'Invalid value';
            }
        } else if (this.state.custom == 'PurePitch') {
            if (!values.volVal) {
                errors.volVal = 'Required';
            }

            if (!values.volUnit) {
                errors.volVal = 'Required';
            }

            if (!values.tempVal) {
                errors.tempUnit = 'Required';
            }

            if (!values.gravVal) {
                errors.gravUnit = 'Required';
            }
        } else if (this.state.custom == 'FANMax Bio®') {
            if (!values.product) {
                errors.product = 'Required';
            }

            if (!values.volume) {
                errors.volume = 'Required';
            }

            if (!values.fanMaxUnits) {
                errors.fanMaxUnits = 'Required';
            }
        } else if (this.state.custom == 'PPNG') {
            if (values.gravUnit == 'SPE' && values.ppnggravVal <= 1) {
                errors.gravVal = 'Specific Gravity must be greater than 1';
            } else if (values.ppnggravVal <= 0) {
                errors.gravVal = 'Starting Gravity must be greater than 0';
            }
        }

        return errors;
    };

    handleFocus = (event) => event.target.select();

    _renderHandlingContent = (props) => {
        const { classes } = props;
        return (
            <Fragment>
                <Typography component="h2" variant="headline" className={classes.instSubheading} gutterBottom>
                    YEAST, WILD YEAST & BACTERIA HANDLING
                </Typography>
                <Typography component="h2" variant="headline" className={classes.instSubheading} style={{ color: '#f28531' }} gutterBottom>
                    Recommendations for White Labs Pitchable Yeast Cultures
                </Typography>
                <List component="nav" className={classes.instList}>
                    <ListItem>
                        <ListItemIcon>
                            <StarIcon />
                        </ListItemIcon>
                        <ListItemText inset primary="Always store the yeast at temperatures between 36 to 40°F (2–4°C) and follow the recommended best by dates for optimal performance." />
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <StarIcon />
                        </ListItemIcon>
                        <ListItemText inset primary="For the first generation of the new yeast culture, a lighter style beer with a 10 to 12°Plato gravity is recommended for best yeast performance. WLN1000 White Labs Yeast Nutrient will help shorten the fermentation lag time and make the yeast healthier for subsequent generations." />
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <StarIcon />
                        </ListItemIcon>
                        <ListItemText inset primary="Keep yeast in the refrigerator until needed. Do not freeze the culture. Remove yeast at least two hours before pitching, so the slurry can come close to room temperature. To inoculate, sanitize scissors, cut the top left of the bag and pour in." />
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <StarIcon />
                        </ListItemIcon>
                        <ListItemText inset primary="Fermentation is best started warmer (approximately 70oF/21oC) and lowered to desired fermentation temperature after krausen formation or evidence of CO2 begins, usually less than 12 hours." />
                    </ListItem>
                    <ListItem>
                        <ListItemIcon>
                            <StarIcon />
                        </ListItemIcon>
                        <ListItemText inset primary="The initial signs of fermentation should be evident within 12 to 20 hours depending on the age of the yeast. Successive generations will have a shorter lag time and faster fermentation. The first complete fermentation usually takes one to three days longer because yeast needs to adapt from a laboratory culture to a fermentation environment." />
                    </ListItem>
                </List>
            </Fragment>
        )
    }

    _renderLabGrownForm = (formikProps) => {
        ReactGA.pageview('/calculator');

        const { isHomebrewer } = this.state;

        const { values, errors } = formikProps;

        const volUnit = _get(values, 'volUnit');
        const tempUnit = _get(values, 'tempUnit');
        const gravUnit = _get(values, 'gravUnit');
        const volChoices = _get(values, 'volChoices');
        const tempChoices = _get(values, 'tempChoices');
        const gravChoices = _get(values, 'gravChoices');

        return (
            <Form>
                <Grid container spacing={6} className='button-grid' >
                    <Grid item xs={12}>
                        <div className='homebrew-box'>
                            <FormCheckbox checked={isHomebrewer} onChange={() => this.toggleIsHomebrewer(formikProps)} />
                            <span>HOMEBREWER</span>
                        </div>
                    </Grid>
                </Grid>
                <fieldset className='fieldset'>
                    <legend>Volume</legend>
                    <Grid container spacing={6} className='field-margin'>
                        <Field
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'volVal')} />
                                        <FormSelectbox
                                            fullWidth
                                            name='volVal'
                                            value={_get(value, 'volVal')}
                                            options={volChoices[volUnit]}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                        <Field
                            render={({ field: { value, onChange }, form: { setFieldValue } }) => {
                                const volUnits = SalesLib.volUnits.filter(unit => (!isHomebrewer && !unit.forHomebrew) || (isHomebrewer && unit.forHomebrew));
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'volUnit')} />
                                        <FormSelectbox
                                            select
                                            fullWidth
                                            name='volUnit'
                                            label='Unit'
                                            value={_get(value, 'volUnit')}
                                            options={volUnits}
                                            onChange={(e) => this.changeUnit(e, formikProps, 'vol')}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>
                <fieldset className='fieldset'>
                    <legend>Temperature</legend>
                    <Grid container spacing={6} className='field-margin'>
                        <Field
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'tempVal')} />
                                        <FormSelectbox
                                            fullWidth
                                            name='tempVal'
                                            value={_get(value, 'tempVal')}
                                            options={tempChoices[tempUnit]}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />

                        <Field
                            render={({ field: { value, onChange }, form: { setFieldValue } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'tempUnit')} />
                                        <FormSelectbox
                                            fullWidth
                                            name='tempUnit'
                                            label='Unit'
                                            value={_get(value, 'tempUnit')}
                                            options={SalesLib.tempUnits}
                                            onChange={(e) => this.changeUnit(e, formikProps, 'temp')}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>
                <fieldset className='fieldset'>
                    <legend>Gravity</legend>
                    <Grid container spacing={6} className='field-margin'>
                        <Field
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'gravVal')} />
                                        <FormSelectbox
                                            fullWidth
                                            name='gravVal'
                                            value={_get(value, 'gravVal')}
                                            options={gravChoices[gravUnit]}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                        <Field
                            render={({ field: { value, onChange }, form: { setFieldValue } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'gravUnit')} />
                                        <FormSelectbox
                                            fullWidth
                                            label='Unit'
                                            name='gravUnit'
                                            value={_get(value, 'gravUnit')}
                                            options={SalesLib.gravUnits}
                                            onChange={(e) => this.changeUnit(e, formikProps, 'grav')} />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>
                <Grid container spacing={6} className='button-grid'>
                    <Grid item xs={12} md={6} />
                    <Grid item xs={12} md={6}>
                        <Button className='calculate-button form-button' variant='contained' type='submit'>
                            CALCULATE
                        </Button>
                        <Grid item xs={12} md={6}>
                            <h4>
                                * Calculator for Beer Yeast Only.
                        </h4>
                        </Grid>
                    </Grid>
                </Grid>
            </Form>
        )
    };

    _renderCustomForm = (formikProps) => {
        ReactGA.pageview('/calculator-repitch');

        const { values, errors } = formikProps;
        const { classes } = this.props;
        const quantityUnits = _get(values, 'quantityUnits');

        return (
            <Form>
                <Grid container spacing={6}>
                    <Field
                        render={({ field: { value, onChange } }) => {
                            return (
                                <Grid item xs={12} md={6}>
                                    <FormikErrorMessage error={_get(errors, 'startingGravity')} />
                                    <TextField
                                        fullWidth
                                        variant='outlined'
                                        name='startingGravity'
                                        label='Starting Gravity in Plato'
                                        value={_get(value, 'startingGravity') || ''}
                                        onChange={onChange}
                                        onFocus={this.handleFocus}
                                        InputProps={{
                                            className: classes.whiteSpace
                                        }}
                                    />
                                </Grid>
                            );
                        }}
                    />
                    <Field
                        render={({ field: { value, onChange } }) => {
                            return (
                                <Grid item xs={12} md={6}>
                                    <FormikErrorMessage error={_get(errors, 'targetPitchRate')} />
                                    <TextField
                                        fullWidth
                                        variant='outlined'
                                        name='targetPitchRate'
                                        label='Target Pitch Rate in Cells per mL'
                                        value={_get(value, 'targetPitchRate') || ''}
                                        onChange={onChange}
                                        onFocus={this.handleFocus}
                                        InputProps={{
                                            className: classes.whiteSpace
                                        }}
                                    />
                                </Grid>
                            );
                        }}
                    />
                    <Field
                        render={({ field: { value, onChange } }) => {
                            return (
                                <Grid item xs={12} md={6}>
                                    <FormikErrorMessage error={_get(errors, 'volume')} />
                                    <TextField
                                        fullWidth
                                        variant='outlined'
                                        name='volume'
                                        label='Batch Size in Unit'
                                        value={_get(value, 'volume') || ''}
                                        onChange={onChange}
                                        onFocus={this.handleFocus}
                                        InputProps={{
                                            className: classes.whiteSpace
                                        }}
                                    />
                                </Grid>
                            );
                        }}
                    />
                    <Field
                        render={({ field: { value, onChange } }) => {
                            return (
                                <Grid item xs={12} md={6}>
                                    <FormikErrorMessage error={_get(errors, 'quaUnit')} />
                                    <FormSelectbox
                                        fullWidth
                                        name='quaUnit'
                                        value={_get(value, 'quaUnit')}
                                        options={quantityUnits}
                                        onChange={onChange}
                                        margin='none'
                                        className=""
                                        InputProps={{
                                            className: classes.whiteSpace
                                        }}
                                    />
                                </Grid>
                            );
                        }}
                    />
                    <Field
                        render={({ field: { value, onChange } }) => {
                            return (
                                <Grid item xs={12} md={6}>
                                    <FormikErrorMessage error={_get(errors, 'viability')} />
                                    <TextField
                                        fullWidth
                                        variant='outlined'
                                        name='viability'
                                        label='Viability %'
                                        value={_get(value, 'viability') || ''}
                                        onChange={onChange}
                                        onFocus={this.handleFocus}
                                        InputProps={{
                                            className: classes.whiteSpace
                                        }}
                                    />
                                </Grid>
                            );
                        }}
                    />
                    <Field
                        render={({ field: { value, onChange } }) => {
                            return (
                                <Grid item xs={12} md={6}>
                                    <FormikErrorMessage error={_get(errors, 'cellCount')} />
                                    <TextField
                                        fullWidth
                                        variant='outlined'
                                        name='cellCount'
                                        label='Yeast Cell Count in Cells per mL'
                                        value={_get(value, 'cellCount') || ''}
                                        onChange={onChange}
                                        onFocus={this.handleFocus}
                                        InputProps={{
                                            className: classes.whiteSpace
                                        }}
                                    />
                                </Grid>
                            );
                        }}
                    />
                </Grid>
                <Grid container spacing={6} className='button-grid'>
                    <Grid item xs={12} md={6}>
                        <h4>
                            <sup>*</sup>Re-pitching is at your own risk. Initial values are intended as examples. Please adjust to reflect your actual parameters.
                        </h4>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Button className='calculate-button form-button' variant='contained' type='submit'>
                            CALCULATE
                        </Button>
                    </Grid>
                </Grid>
            </Form>
        )
    }

    _renderPPNGForm = (formikProps) => {
        ReactGA.pageview('/calculator');

        const { isHomebrewer } = this.state;

        const { values, errors } = formikProps;

        const ppngVolChoices = _get(values, 'ppngVolChoices');
        const tempUnit = _get(values, 'tempUnit');
        const gravUnit = _get(values, 'gravUnit');
        const volChoices = _get(values, 'volChoices');
        const tempChoices = _get(values, 'tempChoices');
        const gravChoices = _get(values, 'gravChoices');
        const ppngVolVal = _get(values, 'ppngVolVal');

        return (
            <Form>
                <fieldset className='fieldset'>
                    <legend>Volume of Wort</legend>
                    <Grid container spacing={6} className='field-margin' >
                        <Field
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'volume')} />
                                        <TextField
                                            fullWidth
                                            variant='outlined'
                                            name='volume'
                                            value={_get(value, 'volume') || ''}
                                            onChange={onChange}
                                            onFocus={this.handleFocus}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                        <Field
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'quaUnit')} />
                                        <FormSelectbox
                                            fullWidth
                                            name='ppngVolVal'
                                            value={ppngVolVal}
                                            options={ppngVolChoices}
                                            onChange={(e) => this.changePPNGUnit(e, formikProps, 'ppngVol')}
                                            margin='none'
                                            label='Unit'
                                        />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>
                <fieldset className='fieldset'>
                    <legend>Enter Pitching Temperature</legend>
                    <Grid container spacing={6} className='field-margin'>
                        <Field
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'ppngTempVal')} />
                                        <TextField
                                            fullWidth
                                            margin='normal'
                                            variant='outlined'
                                            name='ppngTempVal'
                                            type='number'
                                            value={_get(value, 'ppngTempVal') || ''}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />

                        <Field
                            render={({ field: { value, onChange }, form: { setFieldValue } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'tempUnit')} />
                                        <FormSelectbox
                                            fullWidth
                                            name='tempUnit'
                                            label='Unit'
                                            value={_get(value, 'tempUnit')}
                                            options={SalesLib.tempUnits}
                                            onChange={(e) => this.changeUnit(e, formikProps, 'temp')}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>
                <fieldset className='fieldset'>
                    <legend>Starting Gravity of Wort</legend>
                    <Grid container spacing={6} className='field-margin'>
                        <Field
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'gravVal')} />
                                        {/*                                         
                                        <FormSelectbox
                                            fullWidth
                                            name='gravVal'
                                            value={_get(value, 'gravVal')}
                                            options={gravChoices[gravUnit]}
                                            onChange={onChange}
                                        /> */}
                                        <TextField
                                            fullWidth
                                            margin='normal'
                                            variant='outlined'
                                            name='ppnggravVal'
                                            type='number'
                                            value={_get(value, 'ppnggravVal') || ''}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                        <Field
                            render={({ field: { value, onChange }, form: { setFieldValue } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'gravUnit')} />
                                        <FormSelectbox
                                            fullWidth
                                            label='Unit'
                                            name='gravUnit'
                                            value={_get(value, 'gravUnit')}
                                            options={SalesLib.gravUnits}
                                            onChange={(e) => this.changeUnit(e, formikProps, 'grav')} />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>
                <fieldset className='fieldset' style={{ visibility: 'hidden', display: 'none' }}>
                    <legend>Enter Viability % Here, if Available</legend>
                    <Grid container spacing={6} className='field-margin'>
                        <Field
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'viabilityPPng')} />
                                        <TextField
                                            fullWidth
                                            margin='normal'
                                            variant='outlined'
                                            name='viabilityPPng'
                                            type='number'
                                            value={_get(value, 'viabilityPPng') || ''}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>
                <Grid container spacing={6} className='button-grid'>
                    <Grid item xs={12} md={6} />
                    <Grid item xs={12} md={6}>
                        <Button className='calculate-button form-button' variant='contained' type='submit'>
                            CALCULATE
                        </Button>
                    </Grid>
                </Grid>
            </Form>
        )
    }

    _renderUltraFermForm = (formikProps) => {
        let poundOfGrains = _times(31, (v) => ((v + 5) * 100));
        const { values, errors } = formikProps;

        return (
            <Form>
                <fieldset className='fieldset'>
                    <legend>Pounds of Grain</legend>
                    <Grid container spacing={6} className='field-margin'>
                        <Field
                            render={({ field: { value, onChange } }) => {
                                let poundOfGrainsValue = _get(value, 'poundOfGrains')
                                return (
                                    <Grid item xs={12} md={12}>
                                        <FormikErrorMessage error={_get(errors, 'poundOfGrains')} />
                                        <FormSelectbox
                                            // fullWidth
                                            name='poundOfGrains'
                                            value={poundOfGrainsValue}
                                            options={poundOfGrains}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>
                <Grid container spacing={6} className='button-grid'>
                    <Grid item xs={12} md={6} />
                    <Grid item xs={12} md={6}>
                        <Button className='calculate-button form-button' variant='contained' type='submit'>
                            CALCULATE
                        </Button>
                    </Grid>
                </Grid>
            </Form>
        );
    }

    _renderFANMaxForm = (formikProps) => {
        ReactGA.pageview('/calculator');

        const { isHomebrewer } = this.state;

        const { values, errors } = formikProps;
        const products = SalesLib.listProducts.products;
        const fanMaxUnits = SalesLib.listProducts.units;
        const fanMaxResults = SalesLib.listProducts.results
        return (
            <Form>
                <fieldset className='fieldset'>
                    <legend>Product</legend>
                    <Grid container spacing={6} className='field-margin'>
                        <Field
                            render={({ field: { value, onChange } }) => {
                                let productValue = _get(value, 'product');
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'product')} />
                                        <FormSelectbox
                                            fullWidth
                                            name='product'
                                            value={productValue}
                                            options={products}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>

                <fieldset className='fieldset'>
                    <legend>Volume</legend>
                    <Grid container spacing={6} className='field-margin'>
                        <Field
                            render={({ field: { value, onChange } }) => {
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'volume')} />
                                        <TextField
                                            fullWidth
                                            margin='normal'
                                            variant='outlined'
                                            name='volume'
                                            label='Volume'
                                            type='number'
                                            value={_get(value, 'volume') || ''}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />

                        <Field
                            render={({ field: { value, onChange } }) => {
                                let fanMaxUnitsValue = _get(value, 'fanMaxUnits');
                                return (
                                    <Grid item xs={12} md={6}>
                                        <FormikErrorMessage error={_get(errors, 'fanMaxUnits')} />
                                        <FormSelectbox
                                            fullWidth
                                            name='fanMaxUnits'
                                            label='Units'
                                            value={fanMaxUnitsValue}
                                            options={fanMaxUnits}
                                            onChange={onChange}
                                        />
                                    </Grid>
                                );
                            }}
                        />
                    </Grid>
                </fieldset>

                <Grid container spacing={6} className='button-grid'>
                    <Grid item xs={12} md={6} />
                    <Grid item xs={12} md={6}>
                        <Button className='calculate-button form-button' variant='contained' type='submit'>
                            CALCULATE AND VIEW INSTRUCTIONS
                        </Button>
                        {/* <Grid item xs={12} md={6}>
                            <h4>
                                * Calculator for Beer Yeast Only.
                        </h4>
                        </Grid> */}
                    </Grid>
                </Grid>
            </Form>
        )
    }

    _renderClarityFerm(props) {
        return (
            <Calculator props={props} />
        );
    }

    render() {
        const { custom, isHomebrewer } = this.state;
        const { classes } = this.props;
        const dev = process.env.NODE_ENV !== 'production';

        return (
            <Fragment>
                <Card>
                    <CardHeader color='primary' className='card-header-down'>
                        <Typography color='secondary' variant='h4' className='calc-small-variant' align='center'>
                            CALCULATOR
                        </Typography>
                    </CardHeader>

                    <Grid container id='professional-homebrew-switch' className='flex-center'>
                        <Grid item >
                            <FormButton
                                className={`smallbtn form-button-small-size ${custom == 'PurePitch' ? 'form-button-active' : ''}`}
                                text='PurePitch'
                                onClick={() => this.toggleCustom('PurePitch')}
                            />
                        </Grid>
                        <Grid item >
                            <FormButton
                                className={`smallbtn form-button-small-size ${custom == 'Re-Pitching' ? 'form-button-active' : ''}`}
                                text='Re-Pitching'
                                onClick={() => this.toggleCustom('Re-Pitching')}
                            />
                        </Grid>
                        <Grid item >
                            <FormButton
                                className={`smallbtn form-button-small-size ${custom == 'FANMax Bio®' ? 'form-button-active' : ''}`}
                                text='FANMax Bio®'
                                onClick={() => this.toggleCustom('FANMax Bio®')}
                            />
                        </Grid>
                        <Grid item >
                            <FormButton
                                className={`smallbtn form-button-small-size ${custom == 'Ultra-ferm' ? 'form-button-active' : ''}`}
                                text='Ultra-ferm'
                                onClick={() => this.toggleCustom('Ultra-ferm')}
                            />
                        </Grid>
                        <Grid item>
                            <FormButton
                                className={`smallbtn form-button-small-size ${custom == 'Clarity-Ferm' ? 'form-button-active' : ''}`}
                                text='Clarity-Ferm'
                                onClick={() => this.toggleCustom('Clarity-Ferm')}
                            />
                        </Grid>
                        <Grid item >
                            <FormButton
                                className={`smallbtn form-button-small-size ${custom == 'PPNG' ? 'form-button-active' : ''}`}
                                text='PPNG (beta)'
                                onClick={() => this.toggleCustom('PPNG')}
                            />
                        </Grid>
                    </Grid>
                    <Grid container id='professional-homebrew-switch' style={{ display: 'none' }}></Grid>
                    <CardBody>
                        <Formik
                            initialValues={{
                                volVal: '1',
                                volUnit: 'BBL',
                                ppngVolUnit: 'Hectoliters',
                                ppngVolVal: 100,
                                gravVal: 'less than 13.5',
                                ppnggravVal: 12,
                                gravUnit: 'PLA',
                                tempVal: 'less than 59',
                                ppngTempVal: 70,
                                tempUnit: 'F',
                                quaUnit: 100000,
                                startingGravity: '12',
                                targetPitchRate: '1000000',
                                volume: (custom == 'PPNG' ? '5' : '10'),
                                viability: '90',
                                cellCount: '1000000000',
                                viabilityPPng: 100,
                                poundOfGrains: 500,
                                product: SalesLib.listProducts.products[0],
                                fanMaxUnits: SalesLib.listProducts.units[0],
                                volChoices: SalesLib.volChoices,
                                tempChoices: SalesLib.tempChoices,
                                gravChoices: SalesLib.gravChoices,
                                quantityUnits: SalesLib.quantityUnits,
                                fanMaxResults: SalesLib.listProducts.results,
                                ppngVolChoices: SalesLib.ppngVolChoices
                            }}
                            enableReinitialize
                            onSubmit={(values, actions) => this.calculate(values, actions)}
                        >
                            {(props) => {
                                return custom === 'Handling' ? this._renderHandlingContent(props) :
                                    custom === 'Re-Pitching' ? this._renderCustomForm(props) :
                                        custom === 'PPNG' ? this._renderPPNGForm(props) :
                                            custom === 'PurePitch' ? this._renderLabGrownForm(props) :
                                                custom === 'FANMax Bio®' ? this._renderFANMaxForm(props) :
                                                    custom === 'Clarity-Ferm' ? this._renderClarityFerm(props) :
                                                        this._renderUltraFermForm(props)
                                }
                            }
                        </Formik>
                    </CardBody>
                    <Grid item xs={12} className='flex-center' dir='ltr'>
                        <Typography variant="h5" onClick={this.openInstDialog} className={classes.hoverBold} gutterBottom>
                            Wild Yeast And Bacteria Handling Instructions
                        </Typography>
                    </Grid>
                </Card>
                <Dialog open={this.state.instDialog} onClose={this.closeInstDialog} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                    <DialogTitle id="alert-dialog-title" className={classes.instSubheading} >HANDLING INSTRUCTIONS</DialogTitle>
                    <DialogContent>
                        <Typography component="h2" variant="headline" className={classes.instSubheading} gutterBottom>
                            WILD YEAST & BACTERIA HANDLING
                        </Typography>
                        <List component="nav" className={classes.instList}>
                            <Typography component="h2" variant="headline" className={classes.instSubheading} style={{ color: '#f28531' }} gutterBottom>
                                General use for secondary fermentation:
                            </Typography>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="1L per 2-3BBLs for Lactobacillus, Pediococus and Brettanomyces strains." />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Souring will need maturation times around 3+ months." />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Organisms will have a difficult time growing in enviroments below a pH of 3.5." />
                            </ListItem>
                            <Typography component="h2" variant="headline" className={classes.instSubheading} style={{ color: '#f28531' }} gutterBottom>
                                General use of wild yeast for primary fermentation:
                            </Typography>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Typical Pitch rates are 750,000 to 1 million cells/ml." />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Our general recommendation is 1L per 1-1.5BBLs." />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Fermentation timeline will be slower, closer to 18 to 30 days depending on the strain." />
                            </ListItem>
                            <Typography component="h2" variant="headline" className={classes.instSubheading} style={{ color: '#f28531' }} gutterBottom>
                                General use of bacteria for kettle souring/quick souring methods:
                           </Typography>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Generally 1L per 5BBLs is necessary for quick souring within 48 to 72 hours." />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="The higher the pitching rate, the faster the souring." />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Anaerobic enviroment is preferred for Lactobacillus." />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Temperatures ranging from 80 to 95 &#8457; (25-35 &#8457;) are optimal for most Lactobacillus strains. See reference guide for strain specifics at whitelabs.com" />
                            </ListItem>
                            <ListItem>
                                <ListItemIcon>
                                    <StarIcon />
                                </ListItemIcon>
                                <ListItemText inset primary="Wort production needs to be very clean." />
                            </ListItem>
                        </List>
                    </DialogContent>
                </Dialog>
            </Fragment>
        );
    }
}

const styles = theme => ({
    whiteSpace: {
        whiteSpace: 'normal'
    },
    instList: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
    },
    instSubheading: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center'
    },
    handling: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
    },
    hoverBold: {
        display: 'flex',
        justifyContent: 'center',
        textAlign: 'center',
        color: '#f28531',
        marginTop: '20px',
        marginBottom: '100px',
        fontWeight: 'bold',
        '&:hover': {
            fontWeight: 'bolder',
            color: '#ff9933',
            cursor: 'pointer',
            fontSize: '30px',
            backgroundColor: '#ffd9b3'
        }
    },
});

const mapStateToProps = state => {
    return {
        messages: state.messages
    };
};


export default connect(mapStateToProps)(
    withStyles(styles, { withTheme: true })(CalculatorForm)
);
