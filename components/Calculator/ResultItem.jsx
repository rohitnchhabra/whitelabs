import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Card from '@material-ui/core/Card';
import CardBody from 'components/UI/Card/CardBody.jsx';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import ReactHtmlParser from 'react-html-parser';
import { cartActions } from 'appRedux/actions/cartActions';

// custom
import FormButton from 'components/Form/FormButton';
import axios from 'axios';

class ResultItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pack: this.props.item && this.props.item.pack ? this.props.item.pack[0] : '',
            quantity: 1
        };
        this.item = this.props.item;
    }

    setPack = event => {
        this.setState({
            pack: event.target.value
        });
    };

    changeQuantity = event => {
        this.setState({ quantity: event.target.value });
    };

    checkQuantity = item => {
        var quantity = parseFloat(item.OrderDetailQty);

        if (isNaN(quantity) || quantity <= 0) {
            return false;
        }

        //  Must be in increments of 1
        else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
            return false;
        }

        return true;
    };

    addToCart = () => {
        const { result: { packs, ultraFerm, isFanmax } } = this.props;
        if (ultraFerm || isFanmax) {
            var cartItem = {};
            cartItem.Name = String(this.item.Name + ' ' + this.state.pack);
            cartItem.salesCategory = parseInt(this.item.salesCategory);
            cartItem.dispQuantity = parseInt(this.state.quantity);
            cartItem.type = 1;
            cartItem.OrderDetailQty = parseInt(this.state.pack);
            const merchandiseIndex = this.item.pack.findIndex((v) => (v === this.state.pack));
            if (merchandiseIndex > -1) {
                cartItem.MerchandiseID = this.item.volID[merchandiseIndex];
                this.props.addItem({ cartItem });
            }

        } else {
            for (var key in packs) {
                if (!isNaN(key) && packs[key] > 0) {
                    var cartItem = {};

                    cartItem.Name = String(this.item.Name);
                    cartItem.salesCategory = parseInt(this.item.salesCategory);
                    cartItem.dispQuantity = parseInt(packs[key]);
                    cartItem.type = 1;

                    var packageSize;

                    switch (key) {
                        case '0.5':
                            packageSize = 'Nano';
                            cartItem.MerchandiseID = parseInt(this.item.volID[0]);
                            break;
                        case '1.5':
                            packageSize = '1.5L';
                            cartItem.MerchandiseID = parseInt(this.item.volID[1]);
                            break;
                        case '2.0':
                            packageSize = '2L';
                            cartItem.MerchandiseID = parseInt(this.item.volID[2]);
                            break;
                        case '0.04':
                            packageSize = 'HB';
                            cartItem.MerchandiseID = parseInt(this.item.volID[4]);
                            break;
                        default:
                            break;
                    }

                    if (this.item.purePitch) {
                        cartItem.details = 'From CC Calculator: PurePitch® ' + packageSize;
                    }
                    else {
                        cartItem.details = 'From CC Calculator: ' + packageSize;
                    }

                    cartItem.OrderDetailQty = parseInt(cartItem.dispQuantity);

                    if (this.checkQuantity(cartItem)) {
                        const ci = cartItem;
                        axios.post('/related-items', { itemId: ci.MerchandiseID })
                            .then(({ data: { relatedItems, error } }) => {
                                if (error) throw error;

                                for (var i = 0; i < relatedItems.length; i++) {
                                    ci.relatedItems = [];
                                    for (var j = 0; j < relatedItems[i].related.length; j++) {
                                        var relItemArray = this.props.inventory.items.filter(function (el) {
                                            return (el.volID.includes(relatedItems[i].related[j]));
                                        });
                                        for (var k = 0; k < relItemArray.length; k++) {
                                            ci.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                        }
                                    }
                                }

                                this.props.addItem({ cartItem: ci });
                                this.props.closeDialog();
                            })
                            .catch(error => {
                                console.log('error', error);

                                // Still add the item to the cart
                                this.props.addItem({ cartItem: cartItems[itemId] });
                                this.props.closeDialog();
                            });
                    }
                }
            }
        }

        this.props.closeDialog();
    }

    render() {
        const spaceIndex = this.item.Name.indexOf(' ');
        const itemID = this.item.Name.substr(0, spaceIndex);
        const itemName = this.item.Name.substr(spaceIndex + 1);
        const { result } = this.props;

        /*
        //This block is for preventing Vault strains from being ordered in sizes they don't come in
        //but the requirements for this functionality are unclear at the moment.
        var packSizeWarning = '';
        var showAddToCart = true;

        if (result && this.item.isVaultItem && this.item.availQty) {
            if (!this.item.availQty[0] && !this.item.availQty[1] && !this.item.availQty[2]) {
                packSizeWarning = 'This strain is '
            }

            if (result.packs && result.packs["0.5"] > 0 && !this.item.availQty[0]) {
                packSizeWarning += '* This strain not currently available in Nano';
            }
        }
        */

        return (
            <Card className='result-item-card'>
                <CardBody>
                    <Grid container spacing={6}>
                        <Grid item xs={12} className='item-heading'>
                            {itemID} | {itemName}
                        </Grid>
                    </Grid>

                    <Grid container spacing={6}>
                        <Grid item xs={12} className='item-description'>
                            {ReactHtmlParser(this.item.Description)}
                        </Grid>
                    </Grid>
                    {result.ultraFerm || result.isFanmax ?
                        <Grid container spacing={6}>
                            {/* {this.state.pack && ( */}
                            <Grid item xs={12} sm={4} md={4} /* className={classes.formFields} */>
                                <FormControl>
                                    <label>Pack</label>
                                    <Select
                                        value={this.state.pack}
                                        onChange={this.setPack}
                                    >
                                        {this.item.pack.map(
                                            (option, i) => (
                                                // volID[parseInt(option.value)] // Pack size exists for item
                                                // && (!isVaultItem || !availQty || availQty[parseInt(option.value)] > 0)
                                                //     ?
                                                <MenuItem
                                                    key={i}
                                                    value={option}
                                                >
                                                    {option}
                                                </MenuItem>
                                                // : null
                                            )
                                        )}
                                    </Select>
                                </FormControl>
                            </Grid>
                            {/* )} */}
                            <Grid item xs={12} sm={4} md={4}/*  className={classes.formFields} */>
                                {/* <form> */}
                                <FormControl>
                                    <label htmlFor="quantity">Quantity</label>
                                    <TextField
                                        id='quantity'
                                        // label='Quantity'
                                        // className={classes.quantity}
                                        value={this.state.quantity}
                                        onChange={this.changeQuantity}
                                        type='number'
                                    // step={this.item.salesCategory == 32 || this.item.type == 5 ? '0.5' : '1'}
                                    // pattern={this.item.salesCategory == 32 || this.item.type == 5 ? '-?[0-9]*(\.[5]+)?' : ''}
                                    // error={
                                    //     this.item.salesCategory == 32 ? true
                                    //         : this.item.type == 5 ? true : false
                                    // }
                                    />
                                </FormControl>
                                {/* </form> */}
                            </Grid>
                        </Grid>
                        // </Grid>
                        : null}
                    <Grid container spacing={6}>
                        <Grid item xs={12} className='item-button'>
                            <FormButton text='ADD TO CART' onClick={this.addToCart} />
                        </Grid>
                    </Grid>
                </CardBody>
            </Card>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        inventory: state.inventory
    }
}

const mapDispatchToProps = dispatch => bindActionCreators(cartActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ResultItem);
