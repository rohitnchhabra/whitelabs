import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import Card from 'components/UI/Card/Card.jsx';
import CardBody from 'components/UI/Card/CardBody.jsx';
import Grid from '@material-ui/core/Grid';
import { Slider } from 'material-ui-slider';
import Tooltip from '@material-ui/core/Tooltip';

class ResultSummary extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ppngProSliderValue: null
        };
        this.ppngProSliderChange = this.ppngProSliderChange.bind(this);
    }

    ppngProSliderChange(value) {
        this.setState({ppngProSliderValue: value});
    }

    render() {
        const {
            result: {
                isHomebrewer,
                total,
                packs,
                isCustom, // This means 'Re-Pitching'
                unit,
                isFanmax,
                calculatedMax,
                calculatedMin,
                product,
                ultraFerm,
                Low,
                Mid,
                High,
                fanMaxResults,
                poundsOfGrain,
                isPPNG,
                proPacks,
                hbPacks,
                proPitch,
                hbPitch,
                resultproPitchPlato,
                resulthbPitchPlato,
                ppngWortGravity,
                ppngWortVolumeL,
                ppngPackageMultiplier,
                proPackResult,
                resultProPitchRate,
                ppngBarMin,
                ppngBarMax,
                ppngBarUnderpitchingBelow,
                ppngBarIdealLow,
                ppngBarIdealHigh,
                ppngBarOverpitchingAbove
            }
        } = this.props;
        let filteredResult = []
        if (isFanmax) {
            filteredResult = fanMaxResults.filter(val => val.product == product)
        }

        var ppngProPitchRatePlato = resultproPitchPlato;
        var ppngProPitchRate = resultProPitchRate;
        var proResultToUse = proPackResult;

        if (this.state.ppngProSliderValue != null) {
            ppngProPitchRatePlato = parseFloat(this.state.ppngProSliderValue.toFixed(2));
            ppngProPitchRate = parseFloat((ppngProPitchRatePlato * ppngWortGravity).toFixed(2));
            proResultToUse = ((Math.ceil(((1000000000.0 * ppngWortVolumeL * ppngWortGravity * ppngProPitchRatePlato) / (2150000000.0 * 1750.0)) / 0.01)) * 0.01).toFixed(2);
        }

        var ppngBarProColor;
        if (ppngProPitchRatePlato >= ppngBarIdealLow && ppngProPitchRatePlato <= ppngBarIdealHigh) {
            ppngBarProColor = '#008000';
        } else if (ppngProPitchRatePlato < ppngBarUnderpitchingBelow) {
            ppngBarProColor = '#FF0000';
        } else if (ppngProPitchRatePlato < ppngBarIdealLow) {
            ppngBarProColor = '#FFFF00';
        } else if (ppngProPitchRatePlato > ppngBarOverpitchingAbove) {
            ppngBarProColor = '#BA55D3';
        } else if (ppngProPitchRatePlato > ppngBarIdealHigh) {
            ppngBarProColor = '#90EE90';
        } else {
            ppngBarProColor = '#008000';
        }

        return (
            <Card className='result-summary-card'>
                {ultraFerm ?
                    <CardBody>
                        <Grid container spacing={6}>
                            <Grid item xs={12} className='summary-heading'>
                                ULTRA-FERM
                            </Grid>
                        </Grid>

                        <Grid item>
                            The results will assist you in choosing dosage rates for adding Ultra-Ferm in the mash.
                            Dosage rates for Ultra-Ferm are based on mash temperature, raw materials, and time. Below
                            are general guidelines to help determine your dosing rates, though we recommend doing a
                            trial mash to verify your target glucose percentage (sugar profile/dryness of beer).
                            <Typography>
                                <ul>
                                    <li>The optimal mash temperature range for Ultra-Ferm is 131-140&deg;F (55-60&deg;C). Optimal temperature is 60&deg;C. 
                                        Amyloglucosidase activity is completely destroyed when the saccharified liquor is held at 85&deg;C for 10 minutes.
                                        Lower temperatures require higher dosing rates.
                                    </li>
                                    <li>Higher gravity and lower diastatic power mashes require higher dosage rates.</li>
                                    <li>Recommended mash times are between 1-4 hours. For shorter mash times, use higher dosing rates.</li>
                                </ul>
                            </Typography>
                        </Grid>
                        <table style={{ border: 'solid 1px black', margin: 'auto' }}>
                            <thead>
                                <th style={{ border: 'solid 1px black', color: 'white', backgroundColor: 'black' }} colSpan="3">{poundsOfGrain} Pounds of Grain</th>
                            </thead>
                            <thead>
                                <th style={{ border: 'solid 1px black' }}>High Dosing Rate</th>
                                <th style={{ border: 'solid 1px black' }}>Mid Dosing Rate</th>
                                <th style={{ border: 'solid 1px black' }}>Low Dosing Rate</th>
                            </thead>
                            <thead>
                                <th style={{ fontSize: 'x-small', border: 'solid 1px black' }}>mLs of Ultra-Ferm</th>
                                <th style={{ fontSize: 'x-small', border: 'solid 1px black' }}>mLs of Ultra-Ferm</th>
                                <th style={{ fontSize: 'x-small', border: 'solid 1px black' }}>mLs of Ultra-Ferm</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{High}</td>
                                    <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{Mid}</td>
                                    <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{Low}</td>
                                </tr>
                            </tbody>
                        </table>
                    </CardBody>
                    : isHomebrewer ?
                        <CardBody>
                            <Grid item>
                                Homebrew Packs needed: {total}
                            </Grid>
                        </CardBody>
                        : isFanmax ? <CardBody>
                            <Grid container spacing={6} className='summary-total'>
                                <table style={{ border: 'solid 1px black', margin: 'auto' }}>
                                    <thead>
                                        <th style={{ border: 'solid 1px black', color: 'white', backgroundColor: 'black' }} colSpan="2">{product}</th>
                                    </thead>
                                    <thead>
                                        <th style={{ border: 'solid 1px black', color: 'white', backgroundColor: 'black' }} colSpan="2">{total} {unit}</th>
                                    </thead>
                                    <thead>
                                        <th style={{ border: 'solid 1px black' }}>Low Dosage</th>
                                        <th style={{ border: 'solid 1px black' }}>High Dosage</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{calculatedMin.toFixed(2)}g</td>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{calculatedMax.toFixed(2)}g</td>
                                        </tr>
                                        <tr>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{(calculatedMin / 1000.0).toFixed(2)}kg</td>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{(calculatedMax / 1000.0).toFixed(2)}kg</td>
                                        </tr>
                                        <tr>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{(calculatedMin * 0.00220462).toFixed(2)}lbs</td>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{(calculatedMax * 0.00220462).toFixed(2)}lbs</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Grid>
                            <Grid container spacing={6} className='summary-total' style={{ marginTop: '20px' }}>
                                <table style={{ border: 'solid 1px black', margin: 'auto' }}>
                                    <thead>
                                        <th style={{ border: 'solid 1px black', color: 'white', backgroundColor: 'black' }} colSpan="2">Additional Information</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>Dosage</td>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{filteredResult[0].dosage}</td>
                                        </tr>
                                        <tr>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>When to Add</td>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{filteredResult[0].whenToAdd}</td>
                                        </tr>
                                        <tr>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>Notes</td>
                                            <td style={{ border: 'solid 1px black', textAlign: 'center' }}>{filteredResult[0].Notes ? filteredResult[0].Notes : 'N/A'}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </Grid>
                        </CardBody> :
                            isPPNG ?
                                <CardBody>
                                    <Typography style={{ fontSize: 'smaller', marginBottom: '10px', textAlign: 'center' }}>
                                        White Labs is beta testing PurePitch Next Generation, or PPNG, and our beta customers are using this calculator. 
                                        The beta calculator is designed for 1 BBL size batches and over. To become a beta tester, please email Katie Skow 
                                        at <a href='mailto:kskow@whitelabs.com?subject=PPNG%20Beta%20Testing'>kskow@whitelabs.com</a>.
                                        <br /><br />
                                    </Typography>
                                    <Typography style={{ fontWeight: 'bolder', marginBottom: '10px', textAlign: 'center' }}>
                                        This reflects our recommended number of pouches based on your batch size, temperature, and gravity. 
                                        Alternatively, you can customize your pitch rate with the Pitch Rate Thermometer below. 
                                        You will want to stay in the green zone so as not to under&#8209;pitch.
                                        <hr />
                                    </Typography>
                                    <Grid item>
                                        Recommended Number of PPNG Pouches:
                                    <Typography>
                                            <ul>

                                                <li>{proResultToUse}</li>
                                                <li style={{ visibility: 'hidden', display: 'none' }}>HB: {Math.ceil(hbPacks)}</li>
                                            </ul>
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        Resulting Pitching Rate in Wort (million cells/mL):
                                        <Typography>
                                            <ul>
                                                <li>{ppngProPitchRate}</li>
                                                <li style={{ visibility: 'hidden', display: 'none' }}>HB: {hbPitch}</li>
                                            </ul>
                                        </Typography>
                                    </Grid>
                                    <Grid item>
                                        Resulting Pitching Rate in Wort (million cells/mL/Plato):
                                        <Typography>
                                            <ul>
                                                <li>
                                                    {ppngProPitchRatePlato}
                                                </li>
                                                <li style={{ visibility: 'hidden', display: 'none' }}>
                                                    HB: {resulthbPitchPlato}
                                                </li>
                                            </ul>
                                        </Typography>
                                    </Grid>
                                    <hr />
                                    <table style={{ fontSize: 'xx-small', margin: 'auto', width: '100%', position: 'relative' }}>
                                        <tbody>
                                            <tr>
                                                <Tooltip title={ppngProPitchRatePlato} placement="top">
                                                    <td colspan='3'>
                                                        <Slider style={{ width: '100%' }}
                                                            defaultValue={parseFloat(ppngProPitchRatePlato)}
                                                            max={ppngBarMax}
                                                            min={ppngBarMin}
                                                            color={ppngBarProColor}
                                                            onChange={this.ppngProSliderChange}
                                                            value={this.state.ppngProSliderValue}
                                                            scaleLength={0.01}
                                                        />
                                                    </td>
                                                </Tooltip>
                                            </tr>
                                            <tr>
                                                <td style={{ width: '33%', textAlign: 'left', position: 'relative', top: '-20px' }}>{ppngBarMin}</td>
                                                <td style={{ width: '33%', textAlign: 'center', position: 'relative', top: '-20px' }}></td>
                                                <td style={{ width: '33%', textAlign: 'right', position: 'relative', top: '-20px' }}>{ppngBarMax}</td>
                                            </tr>
                                            <tr>
                                                <td style={{ width: '33%', textAlign: 'left', position: 'relative', top: '-20px', fontWeight: 'bold' }}>
                                                    {ppngProPitchRatePlato < ppngBarUnderpitchingBelow ? 'UNDERPITCHING' : null}
                                                </td>
                                                <td style={{ width: '33%', textAlign: 'center', position: 'relative', top: '-20px', fontWeight: 'bold' }}>
                                                    {ppngProPitchRatePlato >= ppngBarIdealLow && ppngProPitchRatePlato <= ppngBarIdealHigh ? 'IDEAL' : null}
                                                </td>
                                                <td style={{ width: '33%', textAlign: 'right', position: 'relative', top: '-20px', fontWeight: 'bold' }}>
                                                    {ppngProPitchRatePlato > ppngBarOverpitchingAbove ? 'OVERPITCHING' : null}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colSpan="3" style={{ textAlign: 'center' }}>
                                                    Pitching rate in million cells per mL per degree Plato
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </CardBody>
                                :
                            <CardBody>
                                <Grid container spacing={6}>
                                    <Grid item xs={12} className='summary-heading'>
                                        Your Results
                                </Grid>
                                </Grid>
                                <Grid container spacing={6} className='summary-total'>
                                    <Grid item xs={6}>
                                        Suggested Amount of Yeast: {total}{isCustom ? 'L' : 'L'}
                            </Grid>
                                    <Grid item xs={6} dir='rtl' />
                                </Grid>
                                {!isCustom ?
                                    <React.Fragment>
                                        <Grid container spacing={6} className='summary-others'>
                                            <Grid item xs={6}>
                                                0.5L (Nano):
                                </Grid>
                                            <Grid item xs={6} dir='rtl'>
                                                {packs['0.5']}
                                            </Grid>
                                        </Grid>
                                        <Grid container spacing={6} className='summary-others' style={{ visibility: 'hidden', display: 'none' }}>
                                            <Grid item xs={6}>
                                                1.5L:
                                            </Grid>
                                            <Grid item xs={6} dir='rtl'>
                                                {packs['1.5']}
                                            </Grid>
                                        </Grid>
                                        <Grid container spacing={6} className='summary-others'>
                                            <Grid item xs={6}>
                                                2L:
                                            </Grid>
                                            <Grid item xs={6} dir='rtl'>
                                                {packs['2.0']}
                                            </Grid>
                                        </Grid>
                                    </React.Fragment>
                                    :
                                    null
                                }
                                <Grid container style={{ marginTop: 10 }} spacing={6}>
                                    <Grid item xs={12}>
                                        * Optimal pitch rate recommendations are based on batch size, fermentation temperature,
                                        and starting gravity. White Labs product specifications and cell concentrations have not
                                        changed. If you are accustomed to ordering by barrel or batch size you may continue to do
                                        so. Intended for White Labs liquid brewer's yeast applications only.
                                </Grid>
                                </Grid>
                            </CardBody>
                }
            </Card>
        );
    }
}

export default ResultSummary;
