import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import moment from 'moment';
import ItemPanel from 'components/Checkout/Items/ItemPanel';
import { orderActions } from 'appRedux/actions/orderActions';
import { userActions } from 'appRedux/actions/userActions';
import SalesLib from 'lib/SalesLib';
import WLHelper from "lib/WLHelper";
import ReactGA from 'react-ga';
import Utils from "lib/Utils";

const styles = theme => ({
    listItem: {
        padding: `${theme.spacing(1)}px 0`
    },
    total: {
        fontWeight: '700'
    },
    title: {
        marginTop: theme.spacing(2)
    },
    sectionTitleDivider: {
        borderTop: 'solid 1.5px',
        borderColor: '#CCCCCC',
        marginBottom: 10
    }
});

function Review(props) {
    ReactGA.initialize('UA-40641680-2');
    ReactGA.pageview('/checkout-review');
// const [error, setError] = useState(false)
    const { classes } = props;

    var hasVaultPreorderItems = props.order.hasVaultPreorderItems;

    const change=(e)=>{
        const email=e.target.value
        props.setOptionalEmail(email)
    }

    const isInternationalShipMethod = (props.user.shipmethod && WLHelper.isShipMethodInternational(props.user.shipmethod));

    var displayedTotal = props.order.orderSubtotal;
    if (props.order.taxRate > 0) {
        // We may be off by a penny or two due to JavaScript vs. NS calculations, so display our totals
        displayedTotal = props.order.itemSubtotal + props.order.totalTax + (props.order.freightMultiplier * props.order.shippingSubtotal);
    }

    if (props.order.crvTotal) {
        displayedTotal += props.order.crvTotal;
    }
    
    return (
        <React.Fragment>
            <Typography variant='h6' color='textPrimary'>
                ORDER SUMMARY
            </Typography>
            <div className={classes.sectionTitleDivider} />
            <Grid item container direction='column' xs={12} sm={6} id="review-summary">
                <Grid item xs={12} sm={6}>
                    <Typography
                        variant='h6'
                        gutterBottom
                        className={classes.title}
                    >
                        User Detail
                    </Typography>
                    <Typography gutterBottom>{props.user.username ? props.user.username : ''}</Typography>
                   {!props.order.optEmail && <Typography gutterBottom>{props.user.email && props.user.email}</Typography>}
                    <Typography gutterBottom>{props.user.phone && props.user.phone}</Typography>
                    <Typography gutterBottom>{props.user.subsidiary && props.user.subsidiaryOptions.map(data => {
                        if (data.value == props.user.subsidiary) {
                            return data.label
                        }
                    })}</Typography>
                </Grid>


    <Grid item >
        <div style={{ fontSize: "12px", color: "#f28411", margin: "10px 0 5px" }}>
            {props.order.hasOnDemandCourses
                ? 'The email you enter will be used as the login for any on-demand courses purchased in this order. Please allow up to 15 minutes to receive login credentials.'
                : 'Replace your default email with another (this order only)'
            }
        </div>
        <TextField
        id="outlined-helperText"
        variant="outlined"
        InputLabelProps={{
          shrink: true,
        }}
            name="Optional Email"
            fullWidth
            type="email"
            placeholder={props.order.hasOnDemandCourses ? "Enter email address (required)." : "Enter optional email(s) to receive communications about this order." }
            onChange={e=>change(e)} 
            value={props.order.optEmail}
        />
        {props.errorOptEmail && <div style={{ color: "red" }} >* Enter a valid email address</div>}
        <Typography variant="subtitle1" style={{ fontSize: "smaller", fontFamily: "auto", fontStyle: "italic" }}>
            {props.order.hasOnDemandCourses 
                ? <Typography>Only one email allowed on orders for on-demand courses.<br />This will not change your primary or user email address.</Typography>
                : <Typography>Separate multiple email addresses by <b style={{ color: "red" }}>;</b> (semicolon). <br />No spaces allowed.<br />This will not change your primary or user email address.</Typography>
            }
        </Typography>
    </Grid>
            </Grid>
            <Grid item container direction='column' xs={12} sm={12}>
                <Grid
                    container
                    spacing={3}
                >
                    <Typography
                        variant='h6'
                        gutterBottom
                        className={classes.title}
                    >
                        Item List
                    </Typography>
                    {
                        !props.order.isLoading &&

                        <Grid item xs={12}>
                            <List>
                                {props.order.items.map((item, i) => (
                                    item.isDistributed && item.OrderDetailQty == 0
                                        ? null : 
                                        <ItemPanel key={i} item={item} index={i} />
                                ))}
                            </List>
                        </Grid>
                    }
                </Grid>
            </Grid>

            <Grid item container direction='column' xs={12} sm={6}>
                <Typography
                    variant='h6'
                    gutterBottom
                    className={classes.title}
                >
                    Payment details
                    </Typography>
                {
                    props.user.terms === '10' ?
                        props.user.card &&
                        <Grid container>
                            <Grid item xs={6}>
                                <Typography gutterBottom>
                                    Card Type
                                    </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography gutterBottom>
                                    Visa
                                    </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography gutterBottom>
                                    Card holder
                                    </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography gutterBottom>
                                    {props.user.card.ccname}
                                </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography gutterBottom>
                                    Card number
                                    </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography gutterBottom>
                                    {props.user.card.ccnumber}
                                </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography gutterBottom>
                                    Expiry date
                                    </Typography>
                            </Grid>
                            <Grid item xs={6}>
                                <Typography gutterBottom>
                                    {moment(props.user.card.ccexpire).format('MM/YYYY')}
                                </Typography>
                            </Grid>
                        </Grid>
                        :
                        <Typography>
                            {WLHelper.getPaymentTerm(props.user.terms)}
                        </Typography>
                }
            </Grid>

            <Grid container spacing={4}>
                <Grid item xs={12} sm={6}>
                    <Typography
                        variant='h6'
                        gutterBottom
                        className={classes.title}
                    >
                        Shipping Address
                    </Typography>
                    <Typography gutterBottom>{props.user.username ? props.user.username : ''}</Typography>

                    {props.user.shipping && (
                        <div>
                            <Typography>
                                {props.user.shipping.attn}
                            </Typography>
                            <Typography>
                                {props.user.shipping.addressee}
                            </Typography>
                            <Typography>
                                {props.user.shipping.address1}
                            </Typography>
                            <Typography>
                                {props.user.shipping.address2}
                            </Typography>
                            <Typography>
                                {props.user.shipping.address3}
                            </Typography>
                            <Typography>
                                {props.user.shipping.city}, {props.user.shipping.countryid}, {props.user.shipping.zip}
                            </Typography>
                        </div>
                    )}
                </Grid>

                <Grid item xs={12} md={6}>
                    <Typography
                        variant='h6'
                        gutterBottom
                        className={classes.title}
                    >
                        BILLING ADDRESS
                        </Typography>
                    <Typography >{props.user.username ? props.user.username : ''}</Typography>
                    {props.user.billing && (
                        <div>
                            <Typography>
                                {props.user.billing.attn}
                            </Typography>
                            <Typography>
                                {props.user.billing.addressee}
                            </Typography>
                            <Typography>
                                {props.user.billing.address1}
                            </Typography>
                            <Typography>
                                {props.user.billing.address2}
                            </Typography>
                            <Typography>
                                {props.user.billing.address3}
                            </Typography>
                            <Typography>
                                {props.user.billing.city}, {props.user.billing.countryid}, {props.user.billing.zip}
                            </Typography>
                        </div>
                    )}
                </Grid>

                <Grid item container direction='column' xs={12} sm={12}>
                    <Grid
                        container
                        spacing={3}
                    >
                        <Typography
                            variant='h6'
                            gutterBottom
                            className={classes.title}
                        >
                            Order Price
                    </Typography>
                        {
                            !props.order.isLoading &&

                            <Grid item xs={12}>
                                <List>
                                    {!props.order.itemSubtotal ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary='Item Subtotal' />
                                            <Typography
                                                variant='subtitle1'
                                                color='primary'
                                                className={classes.total}
                                            >
                                                {SalesLib.CURRENCY_MAP[props.user.currency - 1].symbol}
                                                {(props.order.itemSubtotal + props.order.couponDiscount) && Utils.priceFormat(props.order.itemSubtotal + props.order.couponDiscount)}
                                            </Typography>
                                        </ListItem>
                                    }
                                    {!props.order.couponDiscount ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary={(props.order.couponCode ? props.order.couponCode + ' ' : '') + 'Coupon Discount'} />
                                            <Typography variant='subtitle1' color='primary' className={classes.total}>
                                                ({SalesLib.CURRENCY_MAP[props.user.currency - 1].symbol}
                                                {props.order.couponDiscount && Utils.priceFormat(props.order.couponDiscount)})
                                            </Typography>
                                        </ListItem>
                                    }{!props.order.couponDiscount ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary='Item Subtotal With Discount' />
                                            <Typography variant='subtitle1' color='primary' className={classes.total}>
                                                {SalesLib.CURRENCY_MAP[props.user.currency - 1].symbol}
                                                {props.order.itemSubtotal && Utils.priceFormat(props.order.itemSubtotal)}
                                            </Typography>
                                        </ListItem>
                                    }
                                    {!props.order.totalTax ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary='Estimated Tax' />
                                            <Typography variant='subtitle1' color='primary' className={classes.total} >
                                                {SalesLib.CURRENCY_MAP[props.user.currency - 1].symbol}
                                                {Utils.priceFormat(props.order.totalTax)}
                                            </Typography>
                                        </ListItem>
                                    }
                                    {!props.order.totalTax ? null :
                                        <Typography style={{ fontSize: 'smaller', fontWeight: 'bolder', textAlign: 'right' }}>
                                            The order total during your checkout reflects estimated sales tax.<br />
                                            Final sales tax charged on your order will be displayed in your order confirmation.
                                        </Typography>
                                    }
                                    <ListItem className={classes.listItem}>
                                        <ListItemText primary='Shipping Cost' />
                                        <Typography
                                            variant='subtitle1'
                                            color='primary'
                                            className={classes.total}
                                        >
                                            {SalesLib.CURRENCY_MAP[props.user.currency - 1].symbol}
                                            {props.order.shippingSubtotal && Utils.priceFormat(props.order.freightMultiplier * props.order.shippingSubtotal)}
                                        </Typography>
                                    </ListItem>
                                    {!hasVaultPreorderItems ? null :
                                        <Typography style={{ fontSize: 'smaller', fontWeight: 'bolder', textAlign: 'right' }}>
                                            {props.order.freightMultiplier <= 1
                                                ? 'NOTE: Each pre-order strain ships individually and has a minimum shipping charge, which may result in a higher total shipping charge than shown.'
                                                : '('
                                                + (props.order.items.length > props.order.preorderItemStrains.length ? (props.order.items.length - props.order.preorderItemStrains.length)
                                                    + ' regular items @ ' + SalesLib.CURRENCY_MAP[props.user.currency - 1].symbol + Utils.priceFormat(props.order.shippingSubtotal) + ' + ' : '')
                                                + props.order.preorderItemStrains.length + ' preorder strains @ ' + SalesLib.CURRENCY_MAP[props.user.currency - 1].symbol
                                                + Utils.priceFormat(props.order.shippingSubtotal) + '/strain)'
                                            }
                                        </Typography>
                                    }
                                    {!isInternationalShipMethod || !(props.user.shipmethod == 2843 || props.user.shipmethod == 2844 || props.user.shipmethod == 3475 || props.user.shipmethod == 13320)
                                        ? null :
                                        <Typography>
                                            <br />
                                            Please note: FedEx is charging an extra $8.17 handling fee during the COVID-19 crisis for international shipments.
                                            This will be reflected in your invoice and is in addition to normal shipping charges reflected online.
                                        </Typography>
                                    }
                                    {!props.order.crvTotal ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary='CRV Total' />
                                            <Typography variant='subtitle1' color='primary' className={classes.total}>
                                                {SalesLib.CURRENCY_MAP[props.user.currency - 1].symbol}
                                                {Utils.priceFormat(props.order.crvTotal)}
                                            </Typography>
                                        </ListItem>
                                    }
                                    <ListItem className={classes.listItem}>
                                        <ListItemText primary='Order Subtotal' />
                                        <Typography
                                            variant='subtitle1'
                                            color='primary'
                                            className={classes.total}
                                        >
                                            {SalesLib.CURRENCY_MAP[props.user.currency - 1].symbol}
                                            {Utils.priceFormat(displayedTotal)}
                                        </Typography>
                                    </ListItem>
                                </List>
                            </Grid>
                        }
                    </Grid>
                </Grid>

            </Grid>
        </React.Fragment>
    );
}

Review.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user,
        order: state.order
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({ ...orderActions, ...userActions }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(Review));
