import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Router from "next/router";
import Link from "next/link";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";

import { orderActions } from "appRedux/actions/orderActions";
import { cartActions } from "appRedux/actions/cartActions";

import ReactGA from "react-ga";
import LabTestingForm from "./LabTestingForm/LabTestingForm";
import filter from "lodash/filter";
import SalesLib from "lib/SalesLib";

const styles = theme => ({
  listItem: {
    padding: `${theme.spacing(1)}px 0`
  },
  total: {
    fontWeight: "700"
  },
  title: {
    marginTop: theme.spacing(2)
  },
  hoverBold: {
    "&:hover": {
      fontWeight: "bolder",
      color: "#ff9933",
      cursor: "pointer"
    }
  },
  sectionTitleDivider: {
    borderTop: "solid 1.5px",
    borderColor: "#CCCCCC",
    marginBottom: 10
  }
});

function OrderPlaced(props) {
  ReactGA.initialize("UA-40641680-2");
  ReactGA.pageview("/orderplaced");

  const { classes } = props;

  const backToStore = () => {
    sessionStorage.setItem("orderComplete", "yes");
    props.clearCart();
    window.location.reload(true);
    Router.push("/");
    };

  let testedItem =
    props.cloneOrders &&
    filter(props.cloneOrders.items, o => {
      return SalesLib.SALESCATEGORY[11].includes(parseInt(o.salesCategory));
    });
  return (
    <React.Fragment>
      <Typography variant="h6" color="textPrimary">
        ORDER PLACED
      </Typography>
      <div className={classes.sectionTitleDivider} />
      <Grid item container direction="column" xs={12} sm={6}>
        <Grid item xs={12} sm={12}>
          <Typography variant="title" gutterBottom className={classes.title}>
            Your order has been placed. Visit <a href="/myorders">My Orders</a>{" "}
            to view this order and others.
          </Typography>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={backToStore}
          >
            Back to Store
          </Button>
        </Grid>
        <h3>Help us improve our services by taking a quick survey.</h3>
        <Button
          variant="contained"
          color="primary"
          className={classes.button}
          onClick={() => {
            Router.push("/survey");
          }}
        >
          Take Survey Here
        </Button>
        {testedItem.length > 0 ? <LabTestingForm {...props} showForm={testedItem.length?true:false}/> : null}
      </Grid>
    </React.Fragment>
  );
}

OrderPlaced.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    user: state.user,
    order: state.order
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...orderActions, ...cartActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(OrderPlaced));
