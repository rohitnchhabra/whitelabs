import React from "react";
import Barcode from "react-barcode";
import moment from "moment";
import Grid from "@material-ui/core/Grid";

export default function FirstSectionLab(props) {
  return (
    <>
      <Grid className="office_use">
        <Grid> Office use only (Date Received: ____/____/______)</Grid>
      </Grid>
      <Grid item container className="first_section" >
        <Grid className="heading">
          <Grid>ANALYTICAL TESTING REQUIRED FORM</Grid>
          <Grid>Please include this form with your shipment</Grid>
        </Grid>
        <Grid className="customer_detail" item xs={12}>
          <Grid className="address">
          <Grid>Customer:</Grid>
            <Grid style={{ paddingLeft: '5px', minWidth: '150px' }}>
                <Grid>{props.user.shipping.address1}</Grid>
                <Grid>{props.user.shipping.addressee}</Grid>
                <Grid>{props.user.shipping.attn}</Grid>
                <Grid>{props.user.shipping.city}</Grid>
                <Grid>{props.user.shipping.zip}</Grid>
                <Grid>{props.user.shipping.countryid}</Grid>
            </Grid>
          </Grid>
            <Grid className="account_detail" item xs={12} style={{ paddingLeft: '50px' }}>
                <Grid>Account: {props.order.orderPlacedData[0].fields.entityname}</Grid>
                <Grid>Order#: {props.order.orderPlacedData[0].fields.tranid}</Grid>
                <Grid>Email: {props.order.orderPlacedData[0].fields.email}</Grid>
                <Grid>Date: {moment(props.order.orderPlacedData[0].fields.createddate).format('MM/DD/YYYY')}</Grid>
                <Grid>Payment Method: {props.order.orderPlacedData[0].fields.terms}</Grid>
            </Grid>
        </Grid>
      </Grid>
    </>
  );
}
