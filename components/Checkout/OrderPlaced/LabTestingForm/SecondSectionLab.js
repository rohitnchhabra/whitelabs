import React, { Component } from "react";
import Barcode from "react-barcode";
import SalesLib from "lib/SalesLib";

export default class SecondSectionLab extends Component {
    render() {
        var x = 0,
            price = 0;
          let orderData = this.props.order.orderPlacedData
            .find(v =>
                v.sublists.item.lines.some(m =>
                    SalesLib.SALESCATEGORY[11].includes(parseInt(m.salesCategory))
                )
            );
        return (
            <div>
                <div className="second_section">
                    <table>
                        <thead>
                            <tr style={{ fontWeight: 'bold' }}>
                                <td className="labTestName">TEST</td>
                                <td className="w-120">PRICE</td>
                                <td className="w-120">QTY</td>
                                <td className="w-120">TOTAL</td>
                                <td className="w-180">SAMPLE NAME(S)</td>
                            </tr>
                        </thead>
                        {orderData && orderData.sublists && orderData.sublists.item && orderData.sublists.item.lines.map(val => {
                                if (
                                    !SalesLib.SALESCATEGORY[11].includes(
                                        parseInt(val.salesCategory)
                                    )
                                ) {
                                    return null;
                                }
                                x = x + val.dispQuantity;
                                price = price + val.dispQuantity * val.pricePerUnit;
                                return (
                                    <tbody>
                                        <tr>
                                            <td style={{ verticalAlign: 'top' }}>{val.Name}</td>
                                            <td style={{ verticalAlign: 'top' }}>${val.pricePerUnit.toFixed(2)}</td>
                                            <td style={{ verticalAlign: 'top' }}>{val.dispQuantity}</td>
                                            <td style={{ verticalAlign: 'top' }}>${(val.pricePerUnit * val.dispQuantity).toFixed(2)}</td>
                                            <td style={{ borderBottom: 'solid 1px black' }}>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                );
                            })}
                        <tr className="total">
                            <td>Total:</td>
                            <td />
                            <td>{x}</td>
                            <td>${price.toFixed(2)}</td>
                            <td />
                        </tr>
                    </table>
                </div>
                <div className="instruction">
                    Please send your sample to:<br /><br />
                    White Labs<br />
                    Attn: Analytical Laboratory Services<br />
                    9557 Candida Street, San Diego, CA 92126 USA<br />
                    Tel: 858-693-3441 ext 7387
        </div>
            </div>
        );
    }
}
