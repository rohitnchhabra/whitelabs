import React, { useState, useRef, useEffect } from "react";
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import Dialog from "@material-ui/core/Dialog";
import FirstSectionLab from "./FirstSectionLab";
// import "./labtesting.scss"
import SecondSectionLab from "./SecondSectionLab";
import ReactToPrint from "react-to-print";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
export default function LabTestingForm(props) {
  const [show, setShow] = useState(true);
  const componentRef = useRef();
  useEffect(() => {
    setShow(props.showForm);
  }, props.showForm);

  function handleClose() {
    props.handleCloseLabTestingForm && props.handleCloseLabTestingForm();
    setShow(false);
  }

  return (
    <Dialog open={show} onClose={handleClose} maxWidth={"lg"}>
    <div className="top">
      <div  className="close-icon">
        <IconButton
          color="inherit"
          size="small"
          aria-label="Menu"
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      </div>
      <ReactToPrint
        trigger={() => <button className="print">Print</button>}
        content={() => componentRef.current}
        // className=""
      />
      </div>
      <DialogContent id="testing-form" ref={componentRef}>
        <Grid>
          <FirstSectionLab {...props} />
          <SecondSectionLab {...props} />
        </Grid>
      </DialogContent>
    </Dialog>
  );
}
