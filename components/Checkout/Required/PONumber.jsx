import React, {Component} from 'react'

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { orderActions } from "appRedux/actions/orderActions";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import ReactGA from 'react-ga';

class PONumber extends Component {
    constructor(props) {
        super(props);

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/checkout-ponumber');
    }

    change = (e) => {
        this.props.setPonumber(e.target.value)
    }

    render() {

        return (
            <div className="flex-center" >
                <Grid item xs={12} sm={6}>
                    <Typography variant="subheading" style={{ marginBottom: '15px' }} align="center">
                        PO # (or name of person entering this order)
                    </Typography>
                    <TextField
                        name="ponumber"
                        label="PO# Number"
                        variant="outlined"
                        fullWidth
                        // autoComplete="ponumber"
                        onChange={e => this.change(e)}
                        value={this.props.order.ponumber}
                        inputProps={{ maxLength: 45 }}
                    />
                    <Typography variant="subheading" style={{ marginTop: '15px', fontSize: 'xx-small' }} align="center">
                        (maximum 45 characters)
                    </Typography>
                </Grid>
            </div>
        )
    }
}


const styles = theme => ({});

const mapStateToProps = state => {
    return {
        order: state.order
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({...orderActions}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(PONumber));



