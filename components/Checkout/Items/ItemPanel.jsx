import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import Collapse from '@material-ui/core/Collapse';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import WLHelper from 'lib/WLHelper';
import Utils from 'lib/Utils';
import { orderActions } from 'appRedux/actions/orderActions';
import SalesLib from 'lib/SalesLib';

class ItemPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            expanded: props.initiallyExapanded
        };
    }

    handleExpand = () => {
        this.setState({
            expanded: !this.state.expanded
        });
    };

    wantThisStrainSoonerCondition = () => {
        const { user: { subsidiary }, item } = this.props;
        var cutoffDate = new Date();

        if ((cutoffDate.getHours() >= 14 && subsidiary == 2) || (cutoffDate.getHours() >= 12 && subsidiary != 2)) {
            cutoffDate.setDate(cutoffDate.getDate() + 1);
        }

        if (cutoffDate.getDay() == 5) {
            cutoffDate.setDate(cutoffDate.getDate() + 3);
        }
        else if (cutoffDate.getDay() == 6) {
            cutoffDate.setDate(cutoffDate.getDate() + 2);
        }
        else {
            cutoffDate.setDate(cutoffDate.getDate() + 1);
        }

        var wantThisStrainSoonerCondition = (Utils.compareDates(cutoffDate, item.earliestShipDate) > 0
            && item.type == 1
            && item.salesCategory != 32);

        return wantThisStrainSoonerCondition;
    }

    render() {
        const { classes, order, item, index, user } = this.props;
        const noShipCategories = [24, 25, 26, 28];
        return (
            <ExpansionPanel expanded={this.state.expanded} onChange={this.handleExpand}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography color='primary' className={classes.heading}>
                        {item.Name} {item.details} {item.details_link}
                        <br />
                        <Typography style={{ fontSize: 'smaller' }}>
                            Quantity: {item.OrderDetailQty} Price per Quantity: {SalesLib.CURRENCY_MAP[user.currency - 1].symbol}{item.pricePerUnit.toFixed(2)}
                            {!item.couponDiscount ? null :
                                <Typography style={{ fontSize: 'smaller' }}>
                                    {order.couponCode} Coupon Discount: {SalesLib.CURRENCY_MAP[user.currency - 1].symbol}{item.couponDiscount.toFixed(2)}
                                </Typography>
                            }
                            {!item.taxAmt ? null :
                                <Typography style={{ fontSize: 'smaller' }}>
                                    Estimated Tax: {SalesLib.CURRENCY_MAP[user.currency - 1].symbol}{Utils.priceFormat(item.taxAmt)}
                                </Typography>
                            }
                            {!item.crvAmt ? null :
                                <Typography style={{ fontSize: 'smaller' }}>
                                    {item.crvType ? item.crvType : 'Redemption Fee'}: {SalesLib.CURRENCY_MAP[user.currency - 1].symbol}{Utils.priceFormat(item.crvAmt)}
                                </Typography>
                            }
                        </Typography>
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Grid container spacing={6}>
                        <Grid item xs={12} sm={3} style={{ textAlign: 'center' }}>
                            <img className={classes.image} src={item.imageUrl ? item.imageUrl : '/static/images/yeast.png'} />
                        </Grid>
                        <Grid item xs={12} sm={9} container style={{ textAlign: 'center' }} alignItems='center'>
                            <Grid item xs={12} sm={4}>
                                <Typography color='textPrimary' variant='body1' style={{ display: 'inline-block' }}>
                                    Location:
                                </Typography>
                                &nbsp;
                                <Typography color='primary' variant='body1' style={{ display: 'inline-block' }}>
                                    {WLHelper.getWarehouse(item.Warehouse)}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={4} style={{ display: noShipCategories.includes(item.salesCategory) ? 'none' : '' }}>
                                <Typography color='textPrimary' variant='body1' style={{ display: 'inline-block' }}>
                                    Ship Date:
                                </Typography>
                                &nbsp;
                                <Typography color='primary' variant='body1' style={{ display: 'inline-block' }}>
                                    {item.isVaultPreorderItem ? 'Vault Preorder' : new Date(item.shipDate).toDateString()}
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={4} style={{ display: noShipCategories.includes(item.salesCategory) || item.isVaultPreorderItem ? 'none' : '' }}>
                                <Typography color='textPrimary' variant='body1' style={{ display: 'inline-block' }}>
                                    Delivery/Pickup Date:
                                </Typography>
                                &nbsp;
                                <Typography color='primary' variant='body1' style={{ display: 'inline-block' }}>
                                    {new Date(item.deliveryDate).toDateString()}
                                </Typography>
                            </Grid>
                            {this.wantThisStrainSoonerCondition() &&
                                <Grid className='detail' item xs={12}>
                                    <Button className={classes.wantThisStrainSooner} onClick={() => this.props.getAlternateOptions(item, index)}>
                                        Want this Strain sooner?
                                    </Button>
                                </Grid>
                            }
                            <Grid item xs={12}>
                                {order.selectedShippingOption == 'Custom' && item.MerchandiseID != 4328 && (
                                    <Button variant='outlined' color='primary' onClick={() => this.props.decrementShipDate(item)} style={{ marginRight: 5, marginLeft: 5 }}>
                                        Sooner
                                    </Button>
                                )}

                                {order.selectedShippingOption == 'Custom' && item.MerchandiseID != 4328 && (
                                    <Button variant='outlined' color='primary' onClick={() => this.props.incrementShipDate(item)} style={{ marginRight: 5, marginLeft: 5 }}>
                                        Later
                                    </Button>
                                )}
                            </Grid>
                        </Grid>
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }
}

const styles = theme => ({
    wantThisStrainSooner: {

    },
    heading: {
        fontSize: theme.typography.pxToRem(17)
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary
    },
    image: {
        width: 200,
        marginRight: 10
    }
});

ItemPanel.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user,
        order: state.order
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(orderActions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(ItemPanel));
