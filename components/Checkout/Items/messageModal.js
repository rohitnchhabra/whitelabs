import React, { PureComponent } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import isEqual from "lodash/isEqual";
export default class AlertDialog extends PureComponent {
  state = {
    open: false
  };
  handleClose = () => {
    this.setState({
      open: false
    });
  };
  componentDidUpdate(prevProps) {
    if (!isEqual(this.props, prevProps)) {
      this.props.getItSooner
        ? this.setState({
            open: true
          })
        : this.setState({
            open: false
          });
    }
  }
  render() {
    return (
      <div>
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Thank you!</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              If any earlier ship date is available one of our customer service
              representatives will be in contact with you.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary" autoFocus>
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}
