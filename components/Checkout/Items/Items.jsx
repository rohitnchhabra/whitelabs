import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import CircularProgress from '@material-ui/core/CircularProgress';
import ItemPanel from './ItemPanel';
import WantSooner from 'components/WantSooner/WantSooner';
import { orderActions } from 'appRedux/actions/orderActions';
import { cartActions } from 'appRedux/actions/cartActions';
import { userActions } from 'appRedux/actions/userActions';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import Button from '@material-ui/core/Button';

import SalesLib from 'lib/SalesLib';
import WLHelper from 'lib/WLHelper';
import ReactGA from 'react-ga';
import Utils from "lib/Utils";
import MessageModal from "./messageModal"

import axios from 'axios';

class Items extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showAlternateOptionsDialog: false,
            item: null,
            replaceItemIndex: null,
            showEstimatedTaxExplanation: false
        };

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/checkout-items');
    }

    getAlternateOptions = (item,index) => {
        this.setState({
            showAlternateOptionsDialog: true,
            item,
            replaceItemIndex: index
        })
    };

    hideWantSoonerDialog = () => {
        this.setState({
            showAlternateOptionsDialog: false
        })
    }

    toggleShowEstimatedTax = () => {
        var showingTax = this.state.showEstimatedTaxExplanation;
        this.setState({ showEstimatedTaxExplanation: !showingTax });
    }

    logPresentation = () => {
        var request = {};
        request.presentedItems = [];
        for (var i = 0; i < this.props.order.items.length; i++) {
            var item = this.props.order.items[i];
            var presentedItem = {};
            presentedItem.shipDate = (item.isVaultPreorderItem ? 'Vault Preorder' : new Date(item.shipDate).toDateString());
            presentedItem.warehouseId = WLHelper.getWarehouse(item.Warehouse);
            presentedItem.Name = item.MerchandiseID + '-' + item.Name + ' ' + item.details;
            presentedItem.qty = item.OrderDetailQty;
            request.presentedItems.push(presentedItem);
        }

        request.presentationDate = new Date();
        request.customerName = this.props.user.companyName;
        request.shippingOption = this.props.order.selectedShippingOption;
        request.shipMethod = (this.props.user.shipmethod ? WLHelper.shipMethodNameFromId(this.props.user.shipmethod).substring(0, 25) : 'N/A');
        request.presentationId = this.props.user.id + '-' + new String(request.presentationDate.getTime()).slice(-5);

        axios.post('/log-checkout', { request })
            .then(({ data: { error } }) => {
                if (error) throw error;
            })
            .catch(error => {
                console.log('error', error);
            });
    }

    render() {
        this.logPresentation();

        const { classes } = this.props;
        const { showAlternateOptionsDialog } = this.state;

        const isInternationalShipMethod = (this.props.user.shipmethod && WLHelper.isShipMethodInternational(this.props.user.shipmethod));
        const isInternationalUSCustomer = (this.props.user.subsidiary === 2 && this.props.user.shipping.countryid !== 'US');

        if (isInternationalUSCustomer && this.props.order.selectedShippingOption == 'Earliest For Each') {
            this.props.setShippingOption({ option: 'Ship All Together', getItSooner: false })
        }

        if (this.props.order.isLoading) {
            // We don't need to set the timer on checkout until we have our list of items
            delete localStorage.session;
        } else {
            // Now we need to set the timer
            if (!localStorage.getItem('session')) {
                localStorage.setItem('session', new Date())
            }
        }

        var hasVaultPreorderItems = false;
        var isAllVaultPreorderItems = true;
        var preorderItemStrains = [];

        for (var i = 0; i < this.props.order.items.length; i++) {
            if (this.props.order.items[i].isVaultPreorderItem) {
                hasVaultPreorderItems = true;
                if (!preorderItemStrains.includes(this.props.order.items[i]).Name) preorderItemStrains.push(this.props.order.items[i].Name);
            } else {
                isAllVaultPreorderItems = false;
                if (hasVaultPreorderItems) break;
            }
        }
        if (this.props.order.hasVaultPreorderItems != hasVaultPreorderItems) this.props.order.hasVaultPreorderItems = hasVaultPreorderItems;
        if (!this.props.order.preorderItemStrains || !preorderItemStrains || this.props.order.preorderItemStrains.length != preorderItemStrains.length)
            this.props.order.preorderItemStrains = preorderItemStrains;

        var hasGelPacks = false;
        for (var i = 0; i < this.props.order.items.length; i++) {
            if (this.props.order.items[i].MerchandiseID == 4328) {
                hasGelPacks = true;
                break;
            }
        }

        var hasAlcoholItems = false;
        for (var i = 0; i < this.props.order.items.length; i++) {
            if (this.props.order.items[i].isAlcoholItem) {
                hasAlcoholItems = true;
                break;
            }
        }
        if (this.props.order.hasAlcoholItems != hasAlcoholItems) this.props.order.hasAlcoholItems = hasAlcoholItems;

        var freightMultiplier = (hasVaultPreorderItems
            && this.props.user.shipmethod == SalesLib.END_USER_DEFAULT_SHIP_METHOD_US_YEAST
            && this.props.user.category == 3 ? preorderItemStrains.length + (isAllVaultPreorderItems ? 0 : 1) : 1);
        if (this.props.order.freightMultiplier != freightMultiplier) this.props.order.freightMultiplier = freightMultiplier;

        var hasOnDemandCourses = false;
        var isOnlyOnDemandCourses = true;
        for (var i = 0; i < this.props.order.items.length; i++) {
            if (this.props.order.items[i].isOnDemandCourse) {
                hasOnDemandCourses = true;
            } else {
                isOnlyOnDemandCourses = false;
            }

            // This is all the information we need
            if (hasOnDemandCourses && !isOnlyOnDemandCourses) break;
        }
        if (this.props.order.hasOnDemandCourses != hasOnDemandCourses) this.props.order.hasOnDemandCourses = hasOnDemandCourses;
        if (this.props.order.isOnlyOnDemandCourses != isOnlyOnDemandCourses) this.props.order.isOnlyOnDemandCourses = isOnlyOnDemandCourses;

        var hasItemsThatCouldShipSooner = false;
        var cutoffDate = new Date();
        cutoffDate.setDate(cutoffDate.getDate() + 3);
    
        while (cutoffDate.getDay() == 6 || cutoffDate.getDay() == 0) {
            cutoffDate.setDate(cutoffDate.getDate() + 1);
        }

        if (!hasVaultPreorderItems) {
            for (var i = 0; i < this.props.order.items.length; i++) {
                if (![16, 20, 21, 23, 24, 25, 26, 28].includes(this.props.order.items[i].salesCategory)) {
                    if (this.props.order.items[i].earliestShipDate && Utils.daysBetween(cutoffDate, new Date(this.props.order.items[i].earliestShipDate)) > 3) {
                        hasItemsThatCouldShipSooner = true;
                        break;
                    }
                }
            }
        }

        var displayedTotal = this.props.order.orderSubtotal;
        if (this.props.order.taxRate > 0) {
            // We may be off by a penny or two due to JavaScript vs. NS calculations, so display our totals
            displayedTotal = this.props.order.itemSubtotal + this.props.order.totalTax + (freightMultiplier * this.props.order.shippingSubtotal);
        }

        if (this.props.order.crvTotal) {
            displayedTotal += this.props.order.crvTotal;
        }

        return (
            <React.Fragment>
                {this.props.order.isLoading ? (
                    <Grid container justify='center' alignItems='center' spacing={6}>
                        <CircularProgress size={50} />
                    </Grid>
                ) : (
                        <Grid container justify='center' alignItems='center' spacing={6}>
                            <Grid item xs={12} md={9}>
                                <Typography variant='h6' color='textPrimary'>
                                    ITEMS/SHIP DATES
                                </Typography>
                                {!hasGelPacks ? null :
                                    <Typography color='textPrimary' style={{ fontSize: 'smaller' }}>
                                        Your order includes gel packs. We will attempt to distribute these so that all yeast ships with the quantity of gel pack
                                        you have requested. If your yeast ships on multiple dates or from multiple locations, this will result in additional gel
                                        packs being included in your order. Please review the item list for details.
                                    </Typography>
                                }
                                <Typography color='textPrimary' style={{ fontSize: 'small' }}>
                                    {
                                        this.props.order.selectedShippingOption !== 'Custom' && 
                                        'To change ship dates, please select shipping option Custom'
                                    }
                                </Typography>
                                <Typography color='textPrimary' style={{ fontSize: 'small' }}>
                                    {isInternationalUSCustomer &&
                                        <div>
                                            International customers: If you are in Asia, Australia, or New Zealand, we recommend Friday ship days.
                                            For Mexico, Canada, Europe, South America, and Guam, we recommend Monday ship days.
                                        </div>
                                    }
                                    {
                                        (isInternationalShipMethod || isInternationalUSCustomer)
                                        && this.props.user.subsidiary == 2
                                        && this.props.order.selectedShippingOption !== 'Ship All Together'
                                        && 'International customers ordering from White Labs USA must pick "Ship All Together" to avoid unnecessary custom fees'
                                    }
                                </Typography>
                                <div className={classes.sectionTitleDivider} />
                            </Grid>
         
                            <Grid item xs={12} md={3} style={{ textAlign: "center" }}>
              <TextField
                InputProps={{ classes: { input: classes.whiteSpace } }}
                select
                required
                id="shippingOption"
                name="shippingOption"
                label="Shipping Option"
                variant="outlined"
                autoComplete="country"
                fullWidth
                value={this.props.order.selectedShippingOption}
                onChange={event =>
                  this.props.setShippingOption({option:event.target.value, getItSooner:this.props.order.getItSooner})
                }
              >
                    {this.props.order.shippingOptions.map((option, i) => (
                        (hasVaultPreorderItems && option != 'Earliest For Each') || (isInternationalUSCustomer && option == 'Earliest For Each') ? null :
                            <MenuItem key={i} value={option}>
                                {option}
                            </MenuItem>
                        )
                    )}
              </TextField>
            </Grid>

                            {
                                (!this.props.user.isPro || !hasItemsThatCouldShipSooner) ? null :
                                    <Grid item xs={12} md={3} style={{ textAlign: "center" }}>
                                        <FormControlLabel
                                            control={
                                                <Checkbox
                                                    checked={this.props.order.getItSooner}
                                                    onChange={event =>
                                                        this.props.setShippingOption({
                                                            option: this.props.order.selectedShippingOption,
                                                            getItSooner: !JSON.parse(event.target.value)
                                                        })
                                                    }
                                                    value={this.props.order.getItSooner}
                                                    color="primary"
                                                />
                                            }
                                            label="Get It Sooner"
                                        />
                                        <div style={{ fontSize: 'x-small', textAlign: 'center' }}>
                                            Click this box if you would like us to check if we can ship your product(s) prior to the earliest date indicated below.
                                            We will contact you if we can ship anything sooner.
                                        </div>
                                    </Grid>
                            }
                                <MessageModal getItSooner={this.props.order.getItSooner}/>
                                    <Grid item xs={12}>
                                {this.props.order.selectedShippingOption == 'Custom' && (
                                    <Button variant='outlined' color='primary' onClick={() => this.props.decrementAllShipDate()} style={{ marginRight: 5, marginLeft: 5 }}>
                                        All Earlier
                                    </Button>
                                )}

                                {this.props.order.selectedShippingOption == 'Custom' && (
                                    <Button variant='outlined' color='primary' onClick={() => this.props.incrementAllShipDate()} style={{ marginRight: 5, marginLeft: 5 }}>
                                        All Later
                                    </Button>
                                )}
                            </Grid>
      
                            <Grid item xs={12}>
                                <List>
                                    {this.props.order.items.map((item, i) => (
                                        item.isDistributed && item.OrderDetailQty == 0
                                            ? null : 
                                            <ItemPanel key={i} item={item} index={i} getAlternateOptions={this.getAlternateOptions} initiallyExapanded />
                                    ))}
                                    {!this.props.order.itemSubtotal ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary='Item Subtotal' />
                                            <Typography variant='subtitle1' color='primary' className={classes.total}>
                                                {SalesLib.CURRENCY_MAP[this.props.user.currency - 1].symbol}
                                                {(this.props.order.itemSubtotal + this.props.order.couponDiscount) && Utils.priceFormat(this.props.order.itemSubtotal + this.props.order.couponDiscount)}
                                            </Typography>
                                        </ListItem>
                                    }
                                    {!this.props.order.couponDiscount ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary={(this.props.order.couponCode ? this.props.order.couponCode + ' ' : '') + 'Coupon Discount'} />
                                            <Typography variant='subtitle1' color='primary' className={classes.total}>
                                                ({SalesLib.CURRENCY_MAP[this.props.user.currency - 1].symbol}
                                                {this.props.order.couponDiscount && Utils.priceFormat(this.props.order.couponDiscount)})
                                            </Typography>
                                        </ListItem>
                                    }{!this.props.order.couponDiscount ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary='Item Subtotal With Discount' />
                                            <Typography variant='subtitle1' color='primary' className={classes.total}>
                                                {SalesLib.CURRENCY_MAP[this.props.user.currency - 1].symbol}
                                            {this.props.order.itemSubtotal && Utils.priceFormat(this.props.order.itemSubtotal)}
                                            </Typography>
                                        </ListItem>
                                    }
                                    {!this.props.order.totalTax ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary='Estimated Tax' style={{ cursor: 'help' }} onClick={this.toggleShowEstimatedTax} />
                                            <Typography variant='subtitle1' color='primary' className={classes.total} style={{ cursor: 'help' }} onClick={this.toggleShowEstimatedTax} >
                                                {SalesLib.CURRENCY_MAP[this.props.user.currency - 1].symbol}
                                                {Utils.priceFormat(this.props.order.totalTax)}
                                            </Typography>
                                        </ListItem>
                                    }
                                    {!this.state.showEstimatedTaxExplanation ? null :
                                        <Typography style={{ fontSize: 'smaller', fontWeight: 'bolder', textAlign: 'right' }}>
                                            The order total during your checkout reflects estimated sales tax.<br />
                                            Final sales tax charged on your order will be displayed in your order confirmation.
                                        </Typography>
                                    }
                                    <ListItem className={classes.listItem}>
                                        <ListItemText primary='Shipping Cost' />
                                        <Typography variant='subtitle1' color='primary' className={classes.total}>
                                            {SalesLib.CURRENCY_MAP[this.props.user.currency - 1].symbol}
                                            {this.props.order.shippingSubtotal && Utils.priceFormat(freightMultiplier * this.props.order.shippingSubtotal)}
                                        </Typography>
                                    </ListItem>
                                    {!hasVaultPreorderItems ? null :
                                        <Typography style={{ fontSize: 'smaller', fontWeight: 'bolder', textAlign: 'right' }}>
                                            {freightMultiplier <= 1
                                                ? 'NOTE: Each pre-order strain ships individually and has a minimum shipping charge, which may result in a higher total shipping charge than shown.'
                                                : '('
                                                + (this.props.order.items.length > preorderItemStrains.length ? (this.props.order.items.length - preorderItemStrains.length)
                                                    + ' regular items @ ' + SalesLib.CURRENCY_MAP[this.props.user.currency - 1].symbol + Utils.priceFormat(this.props.order.shippingSubtotal) + ' + ' : '')
                                                + preorderItemStrains.length + ' preorder strains @ ' + SalesLib.CURRENCY_MAP[this.props.user.currency - 1].symbol
                                                + Utils.priceFormat(this.props.order.shippingSubtotal) + '/strain)'
                                            }
                                        </Typography>
                                    }
                                    {!isInternationalShipMethod || !(this.props.user.shipmethod == 2843 || this.props.user.shipmethod == 2844 || this.props.user.shipmethod == 3475 || this.props.user.shipmethod == 13320)
                                        ? null :
                                        <Typography>
                                            <br />
                                            Please note: FedEx is charging an extra $8.17 handling fee during the COVID-19 crisis for international shipments.
                                            This will be reflected in your invoice and is in addition to normal shipping charges reflected online.
                                        </Typography>
                                    }
                                    {!this.props.order.crvTotal ? null :
                                        <ListItem className={classes.listItem}>
                                            <ListItemText primary='CRV Total' />
                                            <Typography variant='subtitle1' color='primary' className={classes.total}>
                                                {SalesLib.CURRENCY_MAP[this.props.user.currency - 1].symbol}
                                                {Utils.priceFormat(this.props.order.crvTotal)}
                                            </Typography>
                                        </ListItem>
                                    }
                                    <ListItem className={classes.listItem}>
                                        <ListItemText primary='Order Subtotal' />
                                        <Typography variant='subtitle1' color='primary' className={classes.total}>
                                            {SalesLib.CURRENCY_MAP[this.props.user.currency - 1].symbol}
                                            {displayedTotal && Utils.priceFormat(displayedTotal)}
                                        </Typography>
                                    </ListItem>
                                    {!hasGelPacks ? null :
                                        <Typography color='textPrimary' style={{ fontSize: 'smaller' }}>
                                            Your order includes gel packs. We will attempt to distribute these so that all yeast ships with the quantity of gel pack
                                            you have requested. If your yeast ships on multiple dates or from multiple locations, this will result in additional gel
                                            packs being included in your order. Please review the item list for details.
                                        </Typography>
                                    }
                                </List>
                            </Grid>
                        </Grid>
                    )}
                <Dialog
                    open={this.state.showAlternateOptionsDialog}
                    onClose={this.hideWantSoonerDialog}
                    aria-labelledby='form-dialog-title'
                    classes={{ paper: classes.dialogPaper }}
                >
                    <WantSooner {...this.props} item={this.state.item} index={this.state.replaceItemIndex} />
                </Dialog>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
    sectionTitleDivider: {
        borderTop: 'solid 1.5px',
        borderColor: '#CCCCCC',
        marginBottom: 10
    },
    listItem: {
        padding: `${theme.spacing(1)}px 0`,
        paddingRight: 20,
        paddingLeft: 20
    },
    total: {
        fontWeight: '700'
    },
    title: {
        marginTop: theme.spacing(2)
    },
    whiteSpace: {
        whiteSpace: 'normal'
    },
    dialogPaper: {
        minWidth: '70%'
    }
});

Items.propTypes = {
    classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        order: state.order,
        cart: state.cart,
        user: state.user
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({ ...orderActions, ...cartActions, ...userActions, ...inventoryActions }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(Items));
