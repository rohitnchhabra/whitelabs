import React, { Component } from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Router from 'next/router';
import _size from 'lodash/size';
import { bindActionCreators } from 'redux';
import { orderActions } from 'appRedux/actions/orderActions'; 
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import withStyles from '@material-ui/core/styles/withStyles';
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Link from 'next/link';

import ReactGA from 'react-ga';

class RemovedItems extends Component {

    constructor(props) {
        super(props);
        this.state = {
            acknowledged: false,
            removedItemCount: 0,
            wereRestrictedItemsRemoved: false
        }

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/checkout-removeditems');
        this.itemsInCart = props.cart.items
    }

    closeDialog = () => {
        var { order: { removedItems, restrictedItemsRemoved } } = this.props;
        const wereRestrictedItemsRemoved = (restrictedItemsRemoved && restrictedItemsRemoved.length > 0);

        this.setState({ acknowledged: true });
        if (this.state.removedItemCount != removedItems.length) {
            var isFirstAcknowledgment = (this.state.removedItemCount == 0);
            this.setState({ removedItemCount: removedItems.length, acknowledged: isFirstAcknowledgment, wereRestrictedItemsRemoved: wereRestrictedItemsRemoved });
        } else if (restrictedItemsRemoved && restrictedItemsRemoved.length > 0 && !this.state.wereRestrictedItemsRemoved) {
            this.setState({ wereRestrictedItemsRemoved: true, acknowledged: false });
        } else if (this.state.wereRestrictedItemsRemoved && (!restrictedItemsRemoved || restrictedItemsRemoved.length == 0)) {
            this.setState({ wereRestrictedItemsRemoved: false, acknowledged: false });
        }
    }

    backToStore = () => {
        this.setState({ acknowledged: true })
        this.closeDialog();
        //window.location.reload(true);
        Router.push('/');
    }
      componentWillUnmount() {
        this.props.resetRemovedItem()
      }
    render() {        
        const { acknowledged } = this.state;
        var { order: { removedItems, restrictedItemsRemoved } } = this.props;
        const { cart: { items }, classes } = this.props;
        const wereRestrictedItemsRemoved = (restrictedItemsRemoved && restrictedItemsRemoved.length > 0);

        var organicItemsRemoved = false;
        if (!removedItems) {
            removedItems = [];
        } else {
            for (var i = 0; i < removedItems.length; i++) {
                if (removedItems[i].isOrganic) {
                    organicItemsRemoved = true;
                    break;
                }
            }
        }

        const allItemsRemoved = (_size(removedItems) == _size(this.itemsInCart));

        return (
            <Dialog
                open={Boolean(_size(removedItems)) && !acknowledged}
                maxWidth='md'
                fullWidth
            >
                <DialogContent id='removed-items'>
                    <div className={classes.close}>
                        <IconButton style={{ padding: '4.5px' }} color='inherit' size='small' aria-label='Menu' onClick={() => this.closeDialog()}>
                            <CloseIcon />
                        </IconButton>
                    </div>

                    {allItemsRemoved ?
                        <React.Fragment>
                            <div className='main-block'>
                                <div className='order-number'>
                                    <Typography variant='h6' color='textPrimary'>
                                        Removed Items
                                    </Typography>
                                </div>
                                <Grid item>
                                    <Typography>All items have been removed due to unavailability</Typography>
                                    {organicItemsRemoved &&
                                        <Typography style={{ fontSize: 'small' }}>
                                            <hr />
                                            The organic strains and sizes you selected are out of stock currently and new production is temporarily suspended. In the meantime, lots of other strains and sizes are available.
                                            Please pick a similar organic strain (or a different size of the same strain) and click the button "Get Availability" to see if the strain is in stock.
                                        </Typography>
                                    }
                                    {wereRestrictedItemsRemoved &&
                                        <Typography style={{ fontSize: 'small' }}>
                                            <hr />
                                            The beer item(s) you ordered cannot be shipped to the selected state or country. Beer can currently only be shipped to
                                            California or North Carolina. Please review the shipping address and try again.
                                        </Typography>
                                    }
                                </Grid>
                            </div>
                            <div style={{ textAlign: 'center' }}>
                                <Grid item>
                                    <Button
                                        variant='contained'
                                        color='primary'
                                        onClick={e => this.backToStore()}
                                    >
                                        Back to Store
                                    </Button>
                                </Grid>
                            </div>
                        </React.Fragment>
                        :
                        <React.Fragment>
                            <div className='main-block'>
                                <div className='order-number' style={{textAlign:"center"}}>
                                    <Typography variant='h6' color='textPrimary'>
                                        The following items are unavailable and have been removed:
                                    </Typography>
                                </div>
                                <br />
                                <Grid container spacing={2} justify='center' alignItems='center'>
                                    <React.Fragment>
                                        {removedItems.map((item) => {
                                            return (
                                                <Grid item>
                                                    <Typography>
                                                        {item.Name + (item.isOrganic ? '*' : '') }
                                                    </Typography>
                                                </Grid>
                                            )
                                        })}
                                        {organicItemsRemoved &&
                                            <Typography style={{ fontSize: 'small' }}>
                                                * This size is out of stock currently and new production is temporarily suspended. In the meantime, lots of other strains and sizes are available. 
                                                Please pick a similar organic strain (or a different size of the same strain) and click the button "Get Availability" to see if the strain is in stock. 
                                            </Typography>
                                        }
                                        {wereRestrictedItemsRemoved &&
                                            <Typography style={{ fontSize: 'small' }}>
                                                <hr />
                                                The beer item(s) you ordered cannot be shipped to the selected state or country. Beer can currently only be shipped to
                                                California or North Carolina. Please review the shipping address and try again.
                                        </Typography>
                                        }
                                    </React.Fragment>
                                </Grid>
                            </div>
                            <div style={{ textAlign: 'center' }}>
                                <br />
                                <Grid item>
                                    <Button
                                        variant='contained'
                                        color='primary'
                                        onClick={e => this.closeDialog()}
                                    >
                                        Acknowledge
                                    </Button>
                                </Grid>
                                <div style={{fontSize:"15px",marginTop:"10px"}}> <Link  
                                        //  prefetch
                                         href="/myaccount">
                                            Please confirm you are ordering from the correct
                                             subsidiary in the 'Order From' field.
                                        </Link>
                                        </div>
                            </div>
                        </React.Fragment>
                    }

                </DialogContent>
            </Dialog>
        )
    }
}

const styles = theme => ({
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1)
    }
});
const mapStateToProps = state => {
    return {
        order: state.order,
        cart: state.cart
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({ ...orderActions }, dispatch);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(compose(withStyles(styles, { withTheme: true })(RemovedItems)));
