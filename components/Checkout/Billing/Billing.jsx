import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import moment from "moment";

import PropTypes from "prop-types";
import classNames from "classnames";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import MenuItem from "@material-ui/core/MenuItem";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from "@material-ui/core/Button";

import WLHelper from "lib/WLHelper";
import Utils from "lib/Utils";
import { userActions } from "appRedux/actions/userActions";

import ManageBilling from "components/MyAccount/ManageBilling";
import AddAddress from "components/MyAccount/AddAddress";

import BillingForm from "components/MyAccount/Billing";
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import ManageCards from "components/MyAccount/ManageCards";
import AddCard from "components/MyAccount/AddCard";
import LoadingIndicator from "components/UI/LoadingIndicator";
import ReactGA from "react-ga";
import { Formik, Form } from "formik";
import { Paper } from "@material-ui/core";
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import _isEqual from 'lodash/isEqual';
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import SalesLib from 'lib/SalesLib';

const paymentOptions = [
  { label: "Credit Card", value: "10" },
  { label: "Net15", value: "1" },
  { label: "Net30", value: "2" },
  { label: "Pay on Pickup", value: "3" },
  { label: "Wire Transfer to WL", value: "11" },
  { label: "Prepay", value: "12" },
  { label: "Bank Transfer", value: "13" },
  { label: "PayPal", value: "14" },
  { label: "C.O.D", value: "7" }
];

class Billing extends Component {
    constructor(props) {
        super(props);
        this.currentMonth = new Date().getMonth().toString();
        this.currentYear = new Date().getFullYear().toString();
        this.state = {
            terms: props.user.terms,
            openDialogCard: false,
            openDialogAddress: false,
            addCard: false,
            newAddress: false,
            manageAddresses: false,
            manageCards: false,
            billing: {
                attn: "",
                addresee: "",
                address1: "",
                address2: "",
                address3: "",
                city: "",
                countryid: "US",
                zip: ""
            },
            card: {
                name: "",
                number: "",
                expireMonth: this.currentMonth,
                expireYear: this.currentYear
            },
            expirationDates: Utils.getExpirationDates(),
            payment: this.props.user.terms,
            editAddress: false,
            showCCMessage: (WLHelper.getPaymentTerm(props.user.terms).toUpperCase() == 'CREDIT CARD' && props.user.customerCategory != SalesLib.CUSTOMER_TYPE_END_USER)
        };

        ReactGA.initialize("UA-40641680-2");
        ReactGA.pageview("/checkout-billing");
    }

    manageAddresses = () => {
        this.setState({ manageAddresses: true });
        this.props.somethingChanged();
    };

    closeAddresses = () => {
        this.setState({ manageAddresses: false });
    };

    acceptCCMessage = () => {
        this.setState({ showCCMessage: false });
    }

    handleNewCard = () => {
        this.setState({ newCard: !this.state.newCard });
        this.props.somethingChanged();
    };

    handleUseCard = () => {
        this.setState({ useCard: !this.state.useCard });
        this.props.somethingChanged();
    };

    handleDialogCardOpen = () => {
        this.setState({ openDialogCard: true });
    };

    handleDialogCardClose = () => {
        this.setState({ openDialogCard: false, newCard: false });
    };

    handleNewAddress = () => {
        this.setState({ newAddress: true });
    };

    handleDialogAddressOpen = () => {
        this.setState({ openDialogAddress: true });
    };

    handleDialogAddressClose = () => {
        this.setState({ openDialogAddress: false, newAddress: false });
    };

    manageCards = () => {
        this.setState({ manageCards: true });
    };

    closeCards = () => {
        this.setState({ manageCards: false });
        if (this.props.user.card.id) {
            this.props.changePayTerms("10");
        }
    };

    addNewAddress = (value) => {
        this.props.addAddress(value);
        // this.handleDialogClose();
    }

    handlePaymentChange = e => {
        this.setState({ payment: e.target.value });
        this.props.changePayTerms(e.target.value)

        if (WLHelper.getPaymentTerm(e.target.value).toUpperCase() == 'CREDIT CARD' && this.props.user.customerCategory != SalesLib.CUSTOMER_TYPE_END_USER) {
            this.setState({ showCCMessage: true });
        }
    };

    editAddress = () => {
        this.setState({ editAddress: true, manageAddresses: false });
    };

    closeEditAddress = () => {
        this.setState({ editAddress: false, manageAddresses: false });
    }

    handleSubmit = (values, { setErrors }) => {
        const errors = this.validate(values);
        if (_isEmpty(errors)) {
            const request = this.buildRequest(this.props.user, values);
            this.setState({ editAddress: false });
            this.props.updateUserInfo({ request });
        } else {
            setErrors(errors);
        }
    };

    buildRequest = (oldState, newState) => {
        var request = {};

        if (!_isEqual(newState.billing, oldState.billing)) {
            request.billing = {};
            request.defaultBillAddress = true;
            request.billing = Object.assign({}, newState.billing);
        }

        return request;
    };


    validate = values => {
        var errors = {};
        if (!_get(values, "billing.attn")) {
            _set(errors, "billing.attn", "Attention is required");
        }

        if (!_get(values, "billing.addressee")) {
            _set(errors, "billing.addressee", "Addressee is required");
        }

        if (!_get(values, "billing.address1")) {
            _set(errors, "billing.address1", "Address1 is required");
        }

        if (!_get(values, "billing.city")) {
            _set(errors, "billing.city", "City is required");
        }

        var country = _get(values, 'billing.countryid');
        if (!country) {
            _set(errors, 'billing.countryid', 'Country is required');
        } else if (country === 'CA' || country === 'US') {
            if (!_get(values, 'billing.countryid')) {
                _set(errors, 'billing.countryid', 'Country is required');
            }
        }

        return errors;
    };


  render() {    
    const { classes, user : {billing}} = this.props; 

    if (!this.props.user.defaultTerms) this.props.user.defaultTerms = this.props.user.terms;

    return (
      <React.Fragment>
          <LoadingIndicator visible={this.props.user.isLoading} label="Updating Account Info" />
        <Grid container spacing={6}>
          <Grid item xs={12} md={6}>
            <Typography variant="h6" color="textPrimary">
              PAYMENT
            </Typography>
            <div className={classes.sectionTitleDivider} />
            <TextField
              select
              name="payment"
              fullWidth
              value={this.state.payment}
              onChange={this.handlePaymentChange}
              label="Select Payment Method"
            >
            {paymentOptions.map((option, i) => {
                return (
                    (option.value === '10' && this.props.user.subsidiary !== 5) || ((option.value === '11' || option.value === '13') && this.props.user.subsidiary == 5)
                    || option.value === this.props.user.defaultTerms ? 
                        <MenuItem key={i} value={option.value}>
                            {option.label}
                        </MenuItem>
                    : null
                );
              })}
            </TextField>
            <div>
              <Typography style={{marginTop:'10px', color:'#f28531',fontWeight:'bolder',fontSize:'18px'}} variant="body2">
              Selected Payment Mode: {WLHelper.getPaymentTerm(this.props.user.terms).toUpperCase()}
              </Typography>
                        {this.props.user.subsidiary == 5 ? null :
                            this.props.user.card.id ? (
                                <div>
                                    <Fragment>
                                        <Typography>Your Payment</Typography>
                                        <Typography>Information</Typography>
                                        <Typography>{this.props.user.card.ccname}</Typography>
                                        {WLHelper.getPaymentTerm(this.props.user.terms).toUpperCase()!=="NET30" && <>
                                        <Typography>{this.props.user.card.ccnumber}</Typography>
                                        <Typography>
                                            {moment(this.props.user.card.ccexpire).format(
                                                "MM-YYYY"
                                            )}
                                        </Typography>
                                        </>
                                        }
                                        <Button
                                            style={{ marginTop: 10 }}
                                            variant="outlined"
                                            color="primary"
                                            onClick={this.manageCards}
                                        >
                                            Change Card
                      </Button>
                                    </Fragment>
                                </div>
                            ) : (
                                <Grid item xs={12}>
                                    <AddAddress addAddress={this.addNewAddress} />
                                </Grid>
                            )}
                            </div>
                    </Grid>
                </Grid>

                <Dialog open={this.state.manageAddresses} maxWidth={"md"} fullWidth>
                    <ManageBilling
                        checkout={true}
                        closeDialog={this.closeAddresses}
                        openEditAddress={this.editAddress}
                    />
                </Dialog>

                <Dialog open={this.state.editAddress} maxWidth={"md"} fullWidth>
                    {/* <ClickAwayListener onClickAway={this.closeEditAddress}> */}
                    <div className={classes.close}>
                        <IconButton style={{ padding: '4.5px' }} color='inherit' size='small' aria-label='Menu' onClick={this.closeEditAddress}>
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Formik
                        initialValues={
                            {
                                billing: {
                                    attn: billing.attn,
                                    addressee: billing.addressee,
                                    address1: billing.address1,
                                    address2: billing.address2,
                                    address3: billing.address3,
                                    zip: billing.zip,
                                    city: billing.city,
                                    state: billing.state,
                                    countryid: billing.countryid,
                                    id: billing.id
                                },
                            }
                        }
                        enableReinitialize
                        onSubmit={(values, actions) => {
                            this.handleSubmit(values, actions);
                        }}
                    >
                        {props => {
                            const { errors, touched } = props;

                            return (
                                <Paper className={classes.paper}>
                                    <BillingForm checkout={true} closeDialog={this.closeAddresses} />
                                    <Form >
                                        <Button
                                            variant="contained"
                                            color="secondary"
                                            onClick={this.closeEditAddress}
                                        >
                                            Cancel
                                        </Button>
                                        <Button variant="contained" color="primary" type="submit">
                                            Save
                                        </Button>
                                    </Form>
                                </Paper>
                            )
                        }}
                    </Formik>
                    {/* </ClickAwayListener> */}
                </Dialog>


                <Dialog open={this.state.manageCards} maxWidth={"md"} fullWidth>
                    <ManageCards checkout={true} closeDialog={this.closeCards} />
                </Dialog>

                <Dialog open={this.state.showCCMessage} maxWidth={"md"} fullWidth>
                    <DialogTitle id='alert-dialog-title'>
                        By using a credit card, you agree that a pre-authorization will be placed on the selected card as a temporary hold.
                        Your credit card will be processed at the time of shipping. 
                    </DialogTitle>
                    <Button onClick={this.acceptCCMessage} color='primary' autoFocus>
                        Accept
                    </Button>
                </Dialog>
            </React.Fragment >
        );
    }
}

const styles = theme => ({
  paper: {
    padding: theme.spacing(2)
  },
  button: {
    marginTop: 10
  },
  close: { position: "absolute", right: 0, top: 0 },
  selected: {
    border: "solid 2px",
    borderColor: "#f28411"
  },
  sectionTitleDivider: {
    borderTop: "solid 1.5px",
    borderColor: "#CCCCCC",
    marginBottom: 10
  }
});

Billing.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  return {
    user: state.user,
    order: state.order
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(userActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Billing));
