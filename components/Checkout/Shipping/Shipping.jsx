import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";
import classNames from "classnames";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import WLHelper from "lib/WLHelper";
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import ShippingForm from "components/MyAccount/Shipping";
import LoadingIndicator from "components/UI/LoadingIndicator";

import ManageShipping from "components/MyAccount/ManageShipping";
import AddAddress from "components/MyAccount/AddAddress";
import ReactGA from "react-ga";
// custom
import { userActions } from "appRedux/actions/userActions";
import { Formik, Form } from "formik";
import { Paper } from "@material-ui/core";
import _get from "lodash/get";
import _isEmpty from "lodash/isEmpty";
import _isEqual from "lodash/isEqual";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import SalesLib from 'lib/SalesLib';

class Shipping extends Component {
    constructor(props) {
        super(props);
        this.state = {
            address: true,
            manageAddresses: false,
            editAddress: false,
        };

        ReactGA.initialize("UA-40641680-2");
        ReactGA.pageview("/checkout-shipping");
    }

    manageAddresses = () => {
        this.setState({ manageAddresses: true });
        this.props.somethingChanged();
    };

    closeAddresses = () => {
        this.setState({ manageAddresses: false });
    };

    changeShipMethod = (e) => {
        this.props.setShipMethod({ shipmethod: e.target.value });
    };

    editAddress = () => {
        this.setState({ editAddress: true, manageAddresses: false });
    };

    closeShipping = () => {
        this.setState({ editAddress: false, manageAddresses: false });
    };

    handleSubmit = (values, { setErrors }) => {
        const errors = this.validate(values);
        if (_isEmpty(errors)) {
            const request = this.buildRequest(this.props.user, values);
            this.setState({ editAddress: false });
            this.props.updateUserInfo({ request });
        } else {
            setErrors(errors);
        }
    };

    buildRequest = (oldState, newState) => {
        var request = {};

        if (!_isEqual(newState.shipping, oldState.shipping)) {
            // Force a ship method update so it can be validated with the new ship address
            request.shipmethod = newState.shipmethod;
            request.shipping = {};
            request.defaultShipAddress = true;
            request.shipping = Object.assign({}, newState.shipping);
        }

        return request;
    };

    validate = (values) => {
        var errors = {};
        if (!_get(values, "shipping.attn")) {
            _set(errors, "shipping.attn", "Attention is required");
        }

        if (!_get(values, "shipping.addressee")) {
            _set(errors, "shipping.addressee", "Addressee is required");
        }

        if (!_get(values, "shipping.address1")) {
            _set(errors, "shipping.address1", "Address1 is required");
        }

        if (!_get(values, "shipping.city")) {
            _set(errors, "shipping.city", "City is required");
        }

        var country = _get(values, "shipping.countryid");
        if (!country) {
            _set(errors, "shipping.countryid", "Country is required");
        } else if (country === "CA" || country === "US") {
            if (!_get(values, "shipping.zip")) {
                _set(errors, "shipping.zip", "Zip is required");
            }
        }

        return errors;
    };

    addNewAddress = (value) => {
        this.props.addAddress(value);
        // this.handleDialogClose();
    }

    render() {
        var { classes, user: { shipping }, order } = this.props;

        var isYeastItemOrdered = false;
        var hasAlcoholItems = false;

        // These are initialized to true and can only get changed to false in the loop
        var isAllVaultPreorderItems = true;
        var isAllYeastItems = true;
        var isAllAlcoholItems = true;
        var isAllHBPacks = true;

        for (var i = 0; i < order.items.length; i++) {
            if (order.items[i].isVaultPreorderItem) {
                // Vault items are also yeast items
                isYeastItemOrdered = true;
            } else {
                isAllVaultPreorderItems = false;
                if (order.items[i].isYeast || SalesLib.SALESCATEGORY[0].includes(order.items[i].salesCategory)) {
                    isYeastItemOrdered = true;
                }
            }

            if (!order.items[i].isAlcoholItem && !SalesLib.SALESCATEGORY[17].includes(order.items[i].salesCategory)) {
                isAllAlcoholItems = false;
            } else {
                order.items[i].isAlcoholItem = true;
                hasAlcoholItems = true;
                isAllYeastItems = false;
                isAllHBPacks = false;
            }

            if (order.items[i].MerchandiseID != 4328) {
                // Ignore gel packs for yeast checks
                if (!order.items[i].isYeast && !SalesLib.SALESCATEGORY[0].includes(order.items[i].salesCategory)) {
                    isAllYeastItems = false;
                    isAllHBPacks = false;
                } else if (order.items[i].type != 2) {
                    // Not a homebrew pack
                    isAllHBPacks = false;
                }
            }
        }

        // In case we missed something somehow, we can't have all HB packs if we don't also have all yeast items
        // (but not, of course, vice-versa)
        if (!isAllYeastItems) isAllHBPacks = false;

        if (this.props.order.hasAlcoholItems != hasAlcoholItems) this.props.order.hasAlcoholItems = hasAlcoholItems;
        if (this.props.order.isAllAlcoholItems != isAllAlcoholItems) this.props.order.isAllAlcoholItems = isAllAlcoholItems;

        var shipMethodToUse = this.props.user.shipmethod;
        var shipMethodChanged = false;
        if (this.props.user.category == 3 && this.props.user.subsidiary == 2 && this.props.user.shipping.countryid == 'US') {
            if (isAllHBPacks) {
                if (!this.props.user.wasDefaultYeastShipMethodSet) {
                    // Deliberately making ship method a string by adding it to a string
                    // Has to be a string to match the values in the dropdown, and using "new String()" doesn't match
                    shipMethodToUse = '' + SalesLib.END_USER_DEFAULT_SHIP_METHOD_US_YEAST;
                    if (this.props.user.shipmethod != shipMethodToUse) {
                        this.props.user.shipmethod = shipMethodToUse;
                        this.props.user.wasDefaultYeastShipMethodSet = true;
                        this.props.user.wasDefaultAlcoholShipMethodSet = false;
                        shipMethodChanged = true;
                    }
                }
            } else if (isAllAlcoholItems) {
                if (!this.props.user.wasDefaultAlcoholShipMethodSet) {
                    // Deliberately making ship method a string by adding it to a string
                    // Has to be a string to match the values in the dropdown, and using "new String()" doesn't match
                    shipMethodToUse = '' + SalesLib.END_USER_DEFAULT_SHIP_METHOD_US_ALCOHOL;
                    if (this.props.user.shipmethod != shipMethodToUse) {
                        this.props.user.shipmethod = shipMethodToUse;
                        this.props.user.wasDefaultYeastShipMethodSet = false;
                        this.props.user.wasDefaultAlcoholShipMethodSet = true;
                        shipMethodChanged = true;
                    }
                }
            } else if (!isAllHBPacks && !isAllAlcoholItems && (this.props.user.wasDefaultYeastShipMethodSet || this.props.user.wasDefaultAlcoholShipMethodSet)
            && this.props.user.defaultShipMethodId) {
                if (this.props.user.shipmethod = this.props.user.defaultShipMethodId) {
                    this.props.user.shipmethod = this.props.user.defaultShipMethodId;
                    shipMethodToUse = this.props.user.defaultShipMethodId;
                    shipMethodChanged = true;
                }
                this.props.user.wasDefaultYeastShipMethodSet = false;
                this.props.user.wasDefaultAlcoholShipMethodSet = false;
            } else if (!shipMethodToUse) {
                shipMethodToUse = this.props.user.defaultShipMethodId;
                shipMethodChanged = true;
                this.props.user.wasDefaultYeastShipMethodSet = false;
                this.props.user.wasDefaultAlcoholShipMethodSet = false;
            }
        } else {
            shipMethodToUse = this.props.user.shipmethod;
        }

        if (shipMethodChanged) {
            var ev = {};
            ev.target = {};
            ev.target.value = shipMethodToUse;
            this.changeShipMethod(ev);
        }

        const isInternationalShipMethod = (this.props.user.shipmethod && WLHelper.isShipMethodInternational(this.props.user.shipmethod));

        // Variables for allowed and disallowed ship methods
        // This is based on items in the cart
        // If using allowed ship methods, disallowed ship methods should be empty, and vice-versa
        // Conditionally default FedEx One Rate ship method
        // Original logic was that had to be End User and all items Vault preorder for method to be available,
        // but as of 6/24/20 is if any yeast item is ordered by End User, FedEx One Rate should be the default.
        // 7/1/2020: Using FedEx Home Delivery instead of FedEx One Rate
        // 7/20/2020: Only show One Rate if a yeast item is ordered
        // 7/20/2020: If only Beer items are ordered, only allow Ground (Cans Only) and Will Call
        // 8/14/2020a: Only allow One Rate if nothing but yeast (and gel packs) is ordered
        // 8/14/2020b: The previous logic only applies to HB packs
        var allowedShipMethods = null;
        var disallowedShipMethods = null;

        if (isAllAlcoholItems) {
            allowedShipMethods = ['' + SalesLib.END_USER_DEFAULT_SHIP_METHOD_US_ALCOHOL, '3511', '13332', '3469', '3472']; //Alcohol + Will Call
            disallowedShipMethods = null;
        } else if (!isAllHBPacks) { //was isYeastItemOrdered
            allowedShipMethods = null;
            disallowedShipMethods = ['' + SalesLib.END_USER_DEFAULT_SHIP_METHOD_US_YEAST, '' + SalesLib.END_USER_DEFAULT_SHIP_METHOD_US_ALCOHOL];
        } else if (!(this.props.user.subsidiary == 2 && this.props.user.category == 3)) {
            allowedShipMethods = null;
            disallowedShipMethods = ['' + SalesLib.END_USER_DEFAULT_SHIP_METHOD_US_YEAST, '' + SalesLib.END_USER_DEFAULT_SHIP_METHOD_US_ALCOHOL];
        } else {
            disallowedShipMethods = ['' + SalesLib.END_USER_DEFAULT_SHIP_METHOD_US_ALCOHOL];
        }

        return (
            <React.Fragment>
                <LoadingIndicator
                    visible={this.props.user.isLoading}
                    label="Updating Account Info"
                />

                <Grid container spacing={6}>
                    {this.props.user.subsidiary === 2 &&
                        this.props.user.shipping.countryid !== "US" && (
                            <div>
                                International customers: If you are in Asia, Australia, or New
                                Zealand, we recommend Friday ship days. For Mexico, Canada,
                                Europe, South America, and Guam, we recommend Monday ship days.
                            </div>
                        )}
                    <Grid item xs={12} md={6}>
                        <Typography variant="h6" color="textPrimary">
                            SHIPPING ADDRESS
                        </Typography>
                        <div className={classes.sectionTitleDivider} />

                        {shipping.id ? (
                            <div>
                                <Typography>{shipping.attn}</Typography>
                                <Typography>{shipping.addressee}</Typography>
                                <Typography>{shipping.address1}</Typography>
                                <Typography>{shipping.address2}</Typography>
                                <Typography>{shipping.address3}</Typography>
                                <Typography>{shipping.city}</Typography>
                                <Typography>{shipping.countryid}</Typography>
                                <Typography>{shipping.zip}</Typography>

                                <Button
                                    style={{ marginTop: 10 }}
                                    variant='outlined' color='primary'
                                    onClick={this.manageAddresses}
                                >
                                    Change Shipping Address
                                </Button>
                            </div>
                        ) : (
                                <AddAddress addAddress={this.addNewAddress} />
                            )}
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Typography variant='h6' color='textPrimary'>
                            SHIPPING METHOD
                        </Typography>
                        <div className={classes.sectionTitleDivider} />
                        <TextField
                            InputProps={{
                                classes: {
                                    input: classes.whiteSpace
                                }
                            }}
                            id='shipmethod'
                            select
                            label={shipMethodToUse ? '' : 'Ship Method'}
                            helperText='Please select your shipping method'
                            margin='normal'
                            variant='outlined'
                            value={shipMethodToUse}
                            onChange={e => this.changeShipMethod(e)}
                        >
                            {this.props.user.shipMethods.map(method => (
                                   (!allowedShipMethods || allowedShipMethods.includes(method.NSID))
                                && (!disallowedShipMethods || !disallowedShipMethods.includes(method.NSID))
                                && (!method.USinternational || this.props.user.shipping.countryid != 'US' || this.props.user.subsidiary != 2)
                                && (method.USinternational || this.props.user.shipping.countryid == 'US' || this.props.user.subsidiary != 2)
                                &&
                                <MenuItem key={method.NSID} value={method.NSID}>
                                    {method.Name}

                                </MenuItem>
                            ))}
                        </TextField>
                        {!isInternationalShipMethod || !(this.props.user.shipmethod == 2843 || this.props.user.shipmethod == 2844 || this.props.user.shipmethod == 3475 || this.props.user.shipmethod == 13320)
                            ? null :
                            <Typography>
                                Please note: FedEx is charging an extra $8.17 handling fee during the COVID-19 crisis for international shipments.
                                This will be reflected in your invoice and is in addition to normal shipping charges reflected online.
                            </Typography>
                        }
                    </Grid>
                </Grid>

                <Dialog
                    open={this.state.manageAddresses}
                    maxWidth={"md"}
                    fullWidth
                    closeDialog={this.closeAddresses}
                >
                    <ManageShipping
                        checkout={true}
                        openEditAddress={this.editAddress}
                        closeDialog={this.closeAddresses}
                    />
                </Dialog>
                <Dialog
                    //   className={classes.dailog}
                    open={this.state.editAddress}
                    closeDialog={this.closeShipping}
                    maxWidth={"md"}
                    fullWidth
                    st
                >
                    {/* <ClickAwayListener onClickAway={this.closeShipping}> */}
                    <div className={classes.close}>
                        <IconButton style={{ padding: '4.5px' }} color='inherit' size='small' aria-label='Menu' onClick={this.closeShipping}>
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Formik
                        initialValues={{
                            shipping: {
                                attn: shipping.attn,
                                addressee: shipping.addressee,
                                address1: shipping.address1,
                                address2: shipping.address2,
                                address3: shipping.address3,
                                city: shipping.city,
                                state: shipping.state,
                                zip: shipping.zip,
                                countryid: shipping.countryid,
                                id: shipping.id,
                            },
                        }}
                        enableReinitialize
                        onSubmit={(values, actions) => {
                            this.handleSubmit(values, actions);
                        }}
                    >
                        {(props) => {
                            return (
                                <Paper className={classes.paper}>
                                    <ShippingForm closeDialog={this.closeShipping} />
                                    <Form>
                                        <Button
                                            variant="contained"
                                            color="secondary"
                                            onClick={this.closeShipping}
                                        >
                                            Cancel
                                        </Button>
                                        <Button variant="contained" color="primary" type="submit">
                                            Save
                                        </Button>
                                    </Form>
                                </Paper>
                            );
                        }}
                    </Formik>
                    {/* </ClickAwayListener> */}
                </Dialog>
            </React.Fragment>
        );
    }
}

const styles = (theme) => ({
  paper: {
    padding: theme.spacing(2),
  },
  button: {
    marginTop: 10,
  },
  close: { position: "absolute", right: 0, top: 0 },
  selected: {
    border: "solid 2px",
    borderColor: "#f28411",
  },
  whiteSpace: {
    whiteSpace: "normal",
  },
  sectionTitleDivider: {
    borderTop: "solid 1.5px",
    borderColor: "#CCCCCC",
    marginBottom: 10,
  },
});

Shipping.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
    order: state.order,
  };
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(userActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(Shipping));
