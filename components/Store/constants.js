export const DISALLOW_BACK_ORDERS = "Disallow back orders but display out-of-stock message";
export const DISPLAY_OUT_OF_STOCK = "Allow back orders but display out-of-stock message";
export const REMOVE_ITEM = "Remove item when out-of-stock";
export const NO_OUT_OF_STOCK = "Allow back orders with no out-of-stock message";
