import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import Router from 'next/router';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import { cartActions } from 'appRedux/actions/cartActions';
import ReactGA from 'react-ga';
import { DISALLOW_BACK_ORDERS } from '../constants';
import ReactHtmlParser from 'react-html-parser';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import axios from 'axios';
import { cartSelectors, inventorySelectors, userSelectors } from 'appRedux/selectors';
import WLHelper from 'lib/WLHelper';

const customFormValidation = Yup.object().shape({
  size: Yup.string()
    .required('Required'),
  quantity: Yup.string()
    .required('Required'),
});

class GiftShopDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: '1',
      sizes: [],
      size: '',
      errors: {},
      isLoading: false,
    };

    this.item = this.props.item;

    ReactGA.initialize('UA-40641680-2');
    ReactGA.pageview('/store-giftshop-' + this.item.Name.replace(/\s/g, '-'));
  };

  UNSAFE_componentWillMount() {
    if (this.item.volID.length > 1) {
      let sizes = [],
        size,
        possibleSizes = [
          { label: 'M', value: 0 },
          { label: 'XS', value: 1 },
          { label: 'S', value: 2 },
          { label: 'L', value: 3 },
          { label: 'XL', value: 4 },
          { label: 'XXL', value: 5 },
          { label: 'XXXL', value: 6 },
        ];

      for (var i in this.item.volID) {
        if (this.item.volID[i] != null) {
          sizes.push(possibleSizes[i]);
        }
      }

      size = sizes[0].value;
      this.setState({ sizes, size });
    }
  };

  handleErrors = (field, err) => {
    let { errors } = this.state;
    errors[field] = err;
    this.setState({ errors });
  };

  setSize = event => {
    this.setState({ size: event.target.value });
  };

  checkQuantity = item => {
    const quantity = parseFloat(item.OrderDetailQty);

    if (isNaN(quantity) || quantity <= 0) {
      this.handleErrors('quantity', 'Please enter a valid value for the quantity');
      return false;
    }

    //  Must be in increments of 1
    else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
      return false;
    }

    return true;
  };

  addToCart = () => {
    const quantity = this.state.quantity;
    const item = this.item;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(item.Name);
        cartItem.MerchandiseID = item.volID[0];
        cartItem.salesCategory = parseInt(item.salesCategory);
        cartItem.type = 3;
        cartItem.details = item.Description;
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.imageUrl = (item.imageUrl ? item.imageUrl : 'static/images/categories/Category-vault.jpg');

        if (this.item.volID.length > 1) {
            switch (this.state.size) {
                case 0:
                    cartItem.MerchandiseID = item.volID[0];
                    cartItem.details += (cartItem.details ? ' ' : '') + 'Size: M';
                    break;
                case 1:
                    cartItem.MerchandiseID = item.volID[1];
                    cartItem.details += (cartItem.details ? ' ' : '') + 'Size: XS';
                    break;
                case 2:
                    cartItem.MerchandiseID = item.volID[2];
                    cartItem.details += (cartItem.details ? ' ' : '') + 'Size: S';
                    break;
                case 3:
                    cartItem.MerchandiseID = item.volID[3];
                    cartItem.details += (cartItem.details ? ' ' : '') + 'Size: L';
                    break;
                case 4:
                    cartItem.MerchandiseID = item.volID[4];
                    cartItem.details += (cartItem.details ? ' ' : '') + 'Size: XL';
                    break;
                case 5:
                    cartItem.MerchandiseID = item.volID[5];
                    cartItem.details += (cartItem.details ? ' ' : '') + 'Size: XXL';
                    break;
                case 6:
                    cartItem.MerchandiseID = item.volID[6];
                    cartItem.details += (cartItem.details ? ' ' : '') + 'Size: XXXL';
                    break;
                default:
                    break;
            }
        }

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    relItemArray[k].cartItemType = WLHelper.getYMO2TypeForItem(relatedItems[i].related[j], relItemArray[k]);
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    })
                    .catch(error => {
                        console.log('error', error);

                        this.setState({ isLoading: false });
                        // Still add the item to the cart
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    });
            }
        }
    };

  changeQuantity = event => {
    this.setState({ quantity: event.target.value });
  };

  handleDialogClose() {
    this.props.closeDialog();
  }

    handleClick = (partNum) => {
        partNum = encodeURIComponent(partNum);
        Router.push(`/giftparam?item=${partNum}`)
        this.props.setPageData(this.props.stateData);
    }

    render() {
        const { partNum } = this.props.item;
        const { classes, theme, item } = this.props;
        const { errors } = this.state;

        var displayPrice = '';
        if (item.displayPrice) {
            if (item.displayPrice.length == 1) {
                displayPrice = '$' + item.displayPrice[0];
            } else {
                var displayPriceAmount = 0;
                for (var i = 0; i < item.displayPrice.length; i++) {
                    if (displayPriceAmount == 0 || displayPriceAmount < parseFloat(item.displayPrice[i])) {
                        if (displayPriceAmount != 0) {
                            displayPrice = 'from $';
                        } else {
                            displayPrice = '$';
                        }
                        displayPrice += item.displayPrice[i];
                        displayPriceAmount = parseFloat(item.displayPrice[i]);
                    }
                }
            }
        }

        var itemDescription = '<div style="max-height:480px; overflow-y: auto; overflow-x: hidden;">' + this.item.Description + '</div>';
        itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

        return (
            <React.Fragment>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <DialogContent>
                    <div className={classes.close}>
                        <IconButton
                            color='inherit'
                            size='small'
                            aria-label='Menu'
                            onClick={() => this.handleDialogClose()}
                        >
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Grid
                        item
                        container
                        xs
                        style={{
                            display: 'flex',
                            marginTop: -10,
                            marginBottom: 20,
                            marginRight: 30
                        }}
                        direction={'row'}
                        spacing={2}
                    >
                        <Grid item>
                            <Typography variant='h5' className={classes.hoverBold} onClick={() => this.handleClick(partNum)}>
                                {this.item.Name}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <div>{ReactHtmlParser(itemDescription)}</div>
                            {this.item.outofstockmessage &&
                                <Typography style={{ paddingTop: '10px', fontWeight: 'bold' }}>
                                    {this.item.outofstockmessage}
                                </Typography>
                            }
                        </Grid>
                    </Grid>

                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={2}
                        style={{ marginTop: 5 }}
                    >
                        <Grid item>
                            <Typography style={{ fontSize: 'smaller' }}>{displayPrice}</Typography>
                        </Grid>
                    </Grid>
                    {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                        <Formik
                            initialValues={this.state}
                            validationSchema={customFormValidation}
                            onSubmit={values => this.addToCart(values)}
                        >
                            {({ values, handleChange }) => {
                                return (
                                    <Form className={classes.form}>
                                        {errors.quantity && <div className='error' >* {errors.quantity}</div>}
                                        <Grid
                                            item
                                            xs
                                            container
                                            spacing={6}
                                            style={{ marginTop: 5 }}
                                            direction={'row'}
                                        >
                                            {this.state.sizes.length > 0 && (
                                                <Grid item>
                                                    <FormControl>
                                                        <InputLabel>Sizes</InputLabel>
                                                        <Select
                                                            value={this.state.size}
                                                            onChange={this.setSize}
                                                        >
                                                            {this.state.sizes.map((option, i) => (
                                                                <MenuItem
                                                                    key={i}
                                                                    value={option.value}
                                                                >
                                                                    {option.label}
                                                                </MenuItem>
                                                            ))}
                                                        </Select>
                                                    </FormControl>
                                                </Grid>
                                            )}

                                            <Grid item>
                                                <TextField
                                                    id='quantity'
                                                    label='Quantity'
                                                    className={classes.quantity}
                                                    value={this.state.quantity}
                                                    onChange={this.changeQuantity}
                                                    type='number'
                                                />
                                            </Grid>
                                            {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                                                <Grid
                                                    item
                                                    xs
                                                    container
                                                    spacing={6}
                                                    direction={'row'}
                                                    justify='flex-end'
                                                >
                                                    <Grid item>
                                                        <div className={classes.buttons}>
                                                            <Button
                                                                // type='submit'
                                                                variant='contained'
                                                                color='primary'
                                                                onClick={this.addToCart}
                                                                className={classes.button}
                                                            >
                                                                Add to Cart
                                                    </Button>
                                                        </div>
                                                    </Grid>
                                                </Grid>
                                            }
                                        </Grid>
                                    </Form>
                                )
                            }}
                        </Formik>
                    }
                </DialogContent>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
  image: {
    width: 250,
    height: 250,
  },
  info: {
    textAlign: 'center',
  },
  quantity: {
    width: 50,
  },
  hide: {
    display: 'none',
  },
  circle: {
    textAlign: 'center',
    position: 'absolute',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
    padding: 5,
    width: 37,
    height: 37,
  },
  hoverBold: {
    '&:hover': {
      fontWeight: 'bolder',
      color: '#ff9933',
      cursor: 'pointer',
    },
  },
  buttons: {
    display: 'flex',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(-5),
    marginBottom: 10
  },
  close: { position: 'absolute', right: 0, top: 0 },
  form: {
    width: '100%',
  },
});

GiftShopDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  dialogClose: PropTypes.func,
};

const mapStateToProps = state => {
  return {
    user: userSelectors.userSelector(state),
    inventory: inventorySelectors.inventorySelector(state),
    isLoading: cartSelectors.isLoadingSelector(state),
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...cartActions, ...inventoryActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(GiftShopDialog));
