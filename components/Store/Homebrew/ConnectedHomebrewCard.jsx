import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';

import { availabilitySelectors, cartSelectors, inventorySelectors, userSelectors } from 'appRedux/selectors';
import { cartActions } from 'appRedux/actions/cartActions';

import HomebrewCard from './HomebrewCard';


const styles = theme => ({
    // quantity: {
    //     display: 'flex',
    //     justifyContent: 'center',
    // },
    // infoBlock: {
    //     display: 'flex',
    //     flexDirection: 'column',
    // },
    // cardContainer: {
    //     display: 'flex',
    //     flexDirection: 'column',
    //     padding: 10,
    // },
    // flipContainer: {
    //     width: 325,
    //     height: 250,
    //     display: 'flex',
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     alignText: 'center',
    // },
    // container: {
    //     minHeight: 500,
    //     width: 325,
    //     display: 'flex',
    //     cursor: 'pointer',
    //     alignItems: 'center',
    //     padding: 10,
    //     flexDirection: 'column',
    //     border: '1px solid rgba(0, 0, 0, 0.54)',
    // },
    // infoContainer: {
    //     justifyContent: 'center',
    // },
    // image: {
    //     maxWidth: 250,
    //     maxHeight: 250,
    //     height: 'auto',
    // },
    card: {
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2),
        height: '100%',
        minHeight: 230,
        cursor: 'pointer',
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    info: {
        textAlign: 'center',
    },
    // infoItem: {
    //     marginTop: 20,
    //     color: '#a0a0a0',
    //     display: 'flex',
    //     flexDirection: 'column',
    // },
    // nameContainer: {
    //     backgroundColor: '#ad83cd',
    //     padding: 1,
    //     textAlign: 'center',
    // },
    dataBlock: {
        color: '#ad83cd',
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: 'center',
    },
    popup: {
        margin: theme.spacing(4),
        fontSize: '22px',
        fontWeight: 'bolder',
    },
});

const mapStateToProps = state => ({
  user: userSelectors.userSelector(state),
  inventory: inventorySelectors.inventorySelector(state),
  isLoading: cartSelectors.isLoadingSelector(state),
  availability: availabilitySelectors.availabilitySelector(state),
  availabilityCPH: cartSelectors.availabilityCPHSelector(state),
  availabilityHK: cartSelectors.availabilityHKSelector(state),
  availabilitySD: cartSelectors.availabilitySDSelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(HomebrewCard));
