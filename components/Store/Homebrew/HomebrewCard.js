import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Popover from "@material-ui/core/Popover";
import Grow from "@material-ui/core/Grow";
import {REMOVE_ITEM} from "../constants";
import { cartActions } from "appRedux/actions/cartActions";
import axios from 'axios';
import LoadingIndicator from 'components/UI/LoadingIndicator';
const YeastElements = {
    "2": {
        img: 'static/images/categories/Category-core.jpg',
        color: '#FFF'
    },
    "3": {  // Ale
        img: 'static/images/categories/Category-ale.jpg',
        icon: 'static/images/icons/Ale-icon.svg',
        color: "#FF9933"
    },
    "4": {  // Wild Yeast
        img: 'static/images/categories/Category-wild.jpg',
        icon: 'static/images/icons/wildyeast-icon.svg',
        color: "#CC9966"
    },
    "5": {  // Lager
        img: 'static/images/categories/Category-lager.jpg',
        icon: 'static/images/icons/Lager-icon.svg',
        color: "#FFCC33"
    },
    "6": {  // Wine
        img: 'static/images/categories/Category-wine.jpg',
        icon: 'static/images/icons/wine-icon.svg',
        color: "#9966CC"
    },
    "7": {  // Distilling
        img: 'static/images/categories/Category-Distilling.jpg',
        icon: 'static/images/icons/Distilling-icon.svg',
        color: "#6666CC"
    },
    "8": {  // Belgian
        img: 'static/images/categories/Category-belgian.jpg',
        icon: 'static/images/icons/Belgian-icon.svg',
        color: "#66CCCC"
    },
    "32": { // Vault
        img: 'static/images/categories/Category-vault.jpg',
        //icon: 'static/images/icons/vault-icon.svg',
        icon: 'static/images/icons/Green_Lock_v01.svg',
        color: "#B3B3B3"
    }
}

function getImage(item, salesCategory) {
    try {
        if (item.ImageURL) {
            // Not currently displaying images in the HB view
            return null;
            //return item.ImageURL;
        } else {
            null;
        }
    } catch (err) {
        console.log('error', salesCategory, err);
    }
}

function getBackgroundImage(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].img;
    } catch (err) {
        console.log('error', salesCategory, err);
    }
}

function getIcon(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].icon;
    }
    catch(error) {

    }
}

function getColor(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].color;
    }
    catch(error) {

        throw error;
    }
}

class HomebrewCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: (props.currentQty ? props.currentQty : ""),
            hover: false,
            img: null,
            icon: null,
            color: null,
            anchorEl: null,
        };
        this.item = this.props.item;
    }

    handleItemHoverEnter = () => {
        this.setState({ hover: true });
    };

    handleItemHoverLeave = () => {
        this.setState({ hover: false });
    };

    checkQuantity = (cartItem) => {
        var quantity = parseFloat(cartItem.OrderDetailQty);

        if(isNaN(quantity) || quantity <= 0 ) {
            return false;
        }
        //  Must be in increments of 1
        else if ((parseFloat(quantity) / parseInt(quantity) != 1.0)) {
            return false;
        }

        return true;
    }

    changeQuantity = (event) => {
        this.setState({ quantity: event.target.value });
    }

    changeListQuantity = (event) => {
        // These things happen in this order on purpose
        var qty = event.target.value;
        this.setState({ quantity: qty });
        if (qty == '') qty = 0;
        this.replaceItemInCart(qty);
    }

    addToCart = (event) => {
        try {
            var quantity = this.state.quantity;
            this.addItemToCart(quantity);
        } catch (error) {

        }

        this.setState({
            anchorEl: event.currentTarget,
        });
        setTimeout(()=>{ this.setState({anchorEl:null}) }, 500);
    };

    createCartItem = (quantity) => {
        var item = this.item;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(item.Name);
        cartItem.salesCategory = parseInt(item.salesCategory);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.OrderDetailQty = parseFloat(quantity);

        cartItem.MerchandiseID = item.volID[4];
        cartItem.type = 2;
        cartItem.details = "Homebrew packs";
        cartItem.imageUrl = item.imageUrl;

        cartItem.allowBackorder = this.props.allowOutOfStock;

        return cartItem;        
    }

    addItemToCart = (quantity) => {
        var cartItem = this.createCartItem(quantity);
        if (this.checkQuantity(cartItem)) {
            this.props.addItem({ cartItem });
        }
    }

    replaceItemInCart = (quantity) => {
        var cartItem = this.createCartItem(quantity);
        cartItem.isReplaceItem = true;
        if (quantity == 0 || this.checkQuantity(cartItem)) {
            this.props.addItem({ cartItem });
        }
    }

    render() {
        const { classes, theme, item, PaperProps, isListMode, currentQty, allowOutOfStock} = this.props;

        const spaceIndex = item.Name.indexOf(" ");
        const itemID = item.Name.substr(0, spaceIndex);
        const itemName = item.Name.substr(spaceIndex + 1);

        const { anchorEl,
            } = this.state;
        const open = Boolean(anchorEl);

        var showHoverState = (isListMode || (this.state.hover && (item.hbAvailQty >= 1 || allowOutOfStock)));
        var isOutOfStock = !item.hbAvailQty || item.hbAvailQty < 1;
        var qtyToShow = (!this.state.quantity && currentQty ? currentQty : this.state.quantity);

        const outOfStockText = (allowOutOfStock ? "Currently available for back-order." : "Currently out of stock in Homebrew size.");

        return (
            isListMode ?
                <>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                {item.outofstockbehavior !== REMOVE_ITEM &&
                <Grid
                    item
                    xs={12}
                    sm={6}
                    md={4}
                    lg={3}
                    spacing={6}
                >
                    <div
                        className={classes.listCard}
                        style={{ backgroundColor: "#fff", cursor: 'default', fontSize: 'small' }}
                    >

                        {isOutOfStock ? (
                            <Grid item container spacing={2}>
                                <Grid item xs={12}>
                                    <div
                                        style={{
                                            backgroundColor: getColor(this.props.item.salesCategory),
                                            textAlign: "center",
                                            marginLeft: theme.spacing(-2),
                                            marginRight: theme.spacing(-2),
                                            cursor: 'default',
                                            position: 'relative',
                                            top: '-17px'
                                        }}
                                    >
                                        <Typography color="secondary" style={{ fontSize: 'small' }}>
                                            {itemID}: {itemName}
                                        </Typography>
                                        <Typography color="black" style={{ fontSize: 'small' }}>
                                            {outOfStockText}
                                        </Typography>
                                    </div>
                                </Grid>
                            </Grid>
                        ) : (
                            <Grid item container direction={"column"} spacing={2}>
                                <Grid item xs={12}>
                                    <div
                                        style={{
                                            backgroundColor: getColor(this.props.item.salesCategory),
                                            textAlign: "center",
                                            marginLeft: theme.spacing(-2),
                                            marginRight: theme.spacing(-2),
                                            cursor: 'default',
                                            position: 'relative',
                                            top: '-17px'
                                        }}
                                    >
                                        <Typography color="secondary" style={{ fontSize: 'small' }}>
                                            {itemID}: {itemName}
                                        </Typography>
                                        {isOutOfStock &&
                                            <Typography color="black" style={{ fontSize: 'small' }}>
                                                {outOfStockText}
                                            </Typography>
                                        }
                                    </div>
                                        <TextField style={{ position: 'relative', top: '-17px' }}
                                        id="quantity"
                                        className={classes.quantity}
                                        value={qtyToShow}
                                        onChange={this.changeListQuantity}
                                        type="number"
                                    />
                                </Grid>
                            </Grid>
                        )}
                    </div>
                </Grid>
            }
            </>
            //End of list mode, beginning of "regular" mode
            :
            <>
            <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
            {item.outofstockbehavior !== REMOVE_ITEM &&
            <Grid
                item
                xs={12}
                sm={6}
                md={4}
                lg={3}
                spacing={6}
                onMouseEnter={this.handleItemHoverEnter}
                onMouseLeave={this.handleItemHoverLeave}
            >
                <div
                    className={classes.card}
                   
                    style={
                        !showHoverState
                            ? {
                                backgroundImage: `url(${getBackgroundImage(this.props.item.salesCategory)})`,
                                  backgroundRepeat: "no-repeat",
                                  backgroundSize: "cover"
                              }
                            : { backgroundColor: "#fff" }
                    }
                >

                    {!showHoverState ? (
                        <Grid item container spacing={2}>
                            <Grid
                                item
                                xs={12}
                                className={classes.info}
                                style={{ marginTop: 30 }}
                            >
                                <img
                                    src={getIcon(this.props.item.salesCategory)}
                                    height="40"
                                />
                                <Typography variant="h5" color="secondary">
                                    {itemID}
                                </Typography>
                                <Typography
                                    variant="subheading"
                                    color="secondary"
                                >
                                    {itemName}
                                </Typography>
                                {item.hbAvailQty >= 1
                                    ? <img className={classes.image} src={getImage(this.props.item, this.props.item.salesCategory)} />
                                    :
                                    <Typography style={{ fontSize: 'smaller' }} color="black">
                                        {outOfStockText}
                                    </Typography>
                                }
                            </Grid>
                        </Grid>
                    ) : (
                        <Grid item container direction={"column"} spacing={2}>
                            <Grid item xsonClick={this.handleClickOrTouch} onTouch={this.handleClickOrTouch}>
                                <Typography
                                    className={classes.info}
                                    variant="subtitle1"
                                    style={{color: getColor(this.props.item.salesCategory)}}
                                >
                                    {itemID}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}onClick={this.handleClickOrTouch} onTouch={this.handleClickOrTouch}>
                                <div
                                    style={{
                                        backgroundColor: getColor(this.props.item.salesCategory),
                                        padding: 1,
                                        textAlign: "center",
                                        marginLeft: theme.spacing(-2),
                                        marginRight: theme.spacing(-2),
                                    }}
                                >
                                    <Typography
                                        variant="subtitle1"
                                        color="secondary"
                                    >
                                        {itemName}
                                    </Typography>
                                    {item.hbAvailQty < 1 &&
                                        <Typography style={{ fontSize: 'smaller' }} color="black">
                                            {outOfStockText}
                                        </Typography>
                                    }
                                </div>
                                <img className={classes.image} src={getImage(this.props.item, this.props.item.salesCategory)} />
                            </Grid>
                            <Grid
                                item
                                xs
                                container
                                direction={"row"}
                                spacing={2}
                                justify="center"
                            >
                                <Grid item >
                                    <TextField
                                        id="quantity"
                                        label="Quantity"
                                        className={classes.quantity}
                                        value={this.state.quantity}
                                        onChange={this.changeQuantity}
                                        type="number"
                                    />
                                </Grid>
                                <Grid item>
                                    <div className={classes.buttons}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            onClick={this.addToCart}
                                            onTouch={this.addToCart}
                                            className={classes.button}
                                            aria-owns={open ? "simple-popper" : undefined}
                                            aria-haspopup="true"
                                        aria-owns={open ? "simple-popper" : undefined}
                                            aria-haspopup="true"
                                        >
                                            Add to Cart
                                        </Button>
                                    </div>
                                </Grid>

                                 <Popover
                                    id="simple-popper"
                                    open={open}
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                    }}
                                    transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                    }}
                                >
                                 <Typography style={{fontSize:'22px', fontWeight:'bolder'}} className={classes.popup}>
                                     Added To Cart
                                 </Typography>
                                </Popover>

                            </Grid>
                        </Grid>
                    )}
                </div>
            </Grid>
        }
            </>
        );
    }
}

HomebrewCard.propTypes = {
    classes: PropTypes.object.isurld,
    theme: PropTypes.object.isurld
};

const styles = theme => ({
    card: {
        border: "solid 1px",
        borderColor: "#CCCCCC",
        padding: theme.spacing(2),
        height: 230,
        cursor: "pointer"
    },
    listCard: {
        border: "solid 1px",
        borderColor: "#CCCCCC",
        padding: theme.spacing(2),
        height: 65,
        cursor: "pointer"
    },
    cardHover: {
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        textAlign: "center"
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: "center"
    },
    popup:{
        margin: theme.spacing(4)
    },
    paper:{
        border: "1px solid black",
        backgroundColor:'blue'
    }
});

const mapStateToProps = state => ({
    user: state.user,
    store: state.inventory
})

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(HomebrewCard));
