import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import Router from 'next/router';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DialogContent from '@material-ui/core/DialogContent';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { DISALLOW_BACK_ORDERS } from '../constants';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import ReactGA from 'react-ga';
import SalesLib from 'lib/SalesLib';
import ReactHtmlParser from 'react-html-parser';
import { cartSelectors, inventorySelectors, userSelectors } from 'appRedux/selectors';
import WLHelper from 'lib/WLHelper';

const customFormValidation = Yup.object().shape({
  quantity: Yup.string()
    .required('Required'),
});

class ServicesDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: '1',
      errors: {},
    };

    this.item = this.props.item;

    ReactGA.initialize('UA-40641680-2');
    ReactGA.pageview('/store-services-' + this.item.Name.replace(/\s/g, '-'));
  }

  handleErrors = (field, err) => {
    let { errors } = this.state;
    errors[field] = err;
    this.setState({ errors });
  };

  addToCart = () => {
    const quantity = this.state.quantity;
    const item = this.item;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(item.Name);
        cartItem.MerchandiseID = item.volID[0];
        cartItem.salesCategory = parseInt(item.salesCategory);
        cartItem.type = 4;
        cartItem.details = item.Description;
        cartItem.imageUrl = (item.imageUrl ? item.imageUrl : 'static/images/categories/Category-belgian.jpg');

        if (SalesLib.SALESCATEGORY[12].includes(parseInt(item.salesCategory))) {
            cartItem.details +=
                '\nPlease send your samples to:\nWhite Labs\nAttn: Analytical Lab\n9557 Candida Street\nSan Diego, CA 92126\nFor information on how much to send please visit:';
            cartItem.details_link =
                'https://www.whitelabs.com/other-products/analytical-lab-services';
        }
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.dateAvailable = item.dateAvailable;
        cartItem.isBigQCDayItem = item.isBigQCDayItem;

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    relItemArray[k].cartItemType = WLHelper.getYMO2TypeForItem(relatedItems[i].related[j], relItemArray[k]);
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    })
                    .catch(error => {
                        console.log('error', error);

                        // Still add the item to the cart
                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    });
            }
        }
    };

    checkQuantity = item => {
        var quantity = parseFloat(item.OrderDetailQty);

        if (isNaN(quantity) || quantity <= 0) {
            this.handleErrors('quantity', 'Please enter a valid value for the quantity');
            return false;
        }

        //  Must be in increments of 1
        else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
            return false;
        }

        return true;
    };

  changeQuantity = event => {
    this.setState({ quantity: event.target.value });
  };

  handleDialogClose() {
    this.props.closeDialog(false);
  }

    handleClick = (partNum) => {
        partNum = encodeURIComponent(partNum);
        Router.push(`/servicesparam?item=${partNum}`);
        this.props.setPageData(this.props.stateData);
    }

    render() {

        const { partNum } = this.props.item;
        const { classes, theme, item } = this.props;
        const { errors } = this.state;
        const spaceIndex = item.Name.indexOf(' ');
        const itemID = item.Name.substr(0, spaceIndex);
        const itemName = item.Name.substr(spaceIndex + 1);

        var itemDescription = '<div style="max-height:480px; overflow-y: auto; overflow-x: hidden;">' + this.item.Description + '</div>';
        itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

        return (
            <React.Fragment>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <DialogContent>
                    <div className={classes.close}>
                        <IconButton
                            color='inherit'
                            size='small'
                            aria-label='Menu'
                            onClick={() => this.handleDialogClose()}
                        >
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Grid
                        item
                        container
                        xs
                        style={{
                            display: 'flex',
                            marginTop: -10,
                            marginBottom: 20
                        }}
                        direction={'row'}
                        spacing={2}
                    >
                        <Grid item>
                            <Typography variant='h5' className={classes.hoverBold} onClick={() => this.handleClick(partNum)}>
                                {itemID} | {itemName}
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={2}
                        style={{ marginTop: 5 }}
                    >
                        <Grid item>
                            {this.item.dateAvailable &&
                                <Typography className={this.item.isPastDeadline ? classes.pastDeadline : classes.deadline}>
                                    DEADLINE TO ORDER{this.item.isPastDeadline ? ' WAS' : ':'} {new Date(this.item.dateAvailable).toDateString().toUpperCase()}
                                </Typography>
                            }
                            {this.item.isPastDeadline && this.item.nextItem &&
                                <Typography className={classes.deadlinePointer} onClick={() => this.handleClick(this.item.nextItem.partNum)} style={{ paddingBottom: '10px' }}>
                                    The next available item is <b>{this.item.nextItem.Name}</b>. Click here for details or to order.
                                    <hr />
                                </Typography>
                            }
                            <div>{ReactHtmlParser(itemDescription)}</div>
                            {this.item.outofstockmessage &&
                                <Typography style={{ paddingTop: '10px', fontWeight: 'bold' }}>
                                    {this.item.outofstockmessage}
                                </Typography>
                            }
                        </Grid>
                    </Grid>
                    {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS && !this.item.isPastDeadline &&
                        <Grid
                            item
                            xs
                            container
                            spacing={6}
                            style={{ marginTop: 5 }}
                            direction={'row'}
                        >
                            <Formik
                                initialValues={this.state}
                                validationSchema={customFormValidation}
                                onSubmit={values => this.addToCart(values)}
                            >
                                {({ values, handleChange }) => {
                                    return (
                                        <Form className={classes.form}>
                                            {errors.quantity && <div className='error' >* {errors.quantity}</div>}
                                            <Grid
                                                item
                                                xs
                                                container
                                                spacing={6}
                                                direction={'row'}
                                                justify='flex-start'
                                            >
                                                <Grid item>
                                                    <TextField
                                                        id='quantity'
                                                        label='Quantity'
                                                        className={classes.quantity}
                                                        value={this.state.quantity}
                                                        onChange={this.changeQuantity}
                                                        type='number'
                                                    />
                                                </Grid>
                                                {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                                                    <Grid
                                                        item
                                                        xs
                                                        container
                                                        spacing={6}
                                                        direction={'row'}
                                                        justify='flex-end'
                                                    >
                                                        <Grid item>
                                                            <div className={classes.buttons}>
                                                                <Button
                                                                    type='submit'
                                                                    variant='contained'
                                                                    color='primary'
                                                                    // onClick={this.addToCart}
                                                                    className={classes.button}
                                                                >
                                                                    Add to Cart
                                                    </Button>
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                                }
                                            </Grid>
                                        </Form>
                                    )
                                }}
                            </Formik>
                        </Grid>
                    }
                </DialogContent>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  subContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    minWidth: 200,
  },
  image: {
    width: 250,
    height: 250,
  },
  card: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    height: '100%',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  cardHover: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  info: {
    alignItems: 'center',
    padding: 5,
    backgroundColor: '#e4e4e4',
    textAlign: 'center',
  },
  quantity: {
    width: 50,
  },
  hide: {
    display: 'none',
  },
  hoverBold: {
    '&:hover': {
      fontWeight: 'bolder',
      color: '#ff9933',
      cursor: 'pointer',
    },
  },
  buttons: {
    display: 'flex',
  },
  button: {
    marginTop: theme.spacing(1),
    marginRight: theme.spacing(-5),
  },
  close: { position: 'absolute', right: 0, top: 0 },
  form: {
    width: '100%',
  },
});

ServicesDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  user: userSelectors.userSelector(state),
  inventory: inventorySelectors.inventorySelector(state),
  isLoading: cartSelectors.isLoadingSelector(state),
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...cartActions, ...inventoryActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(ServicesDialog));
