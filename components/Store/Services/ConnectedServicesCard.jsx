import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';
import ServicesCard from './ServicesCard';
import { bindActionCreators } from 'redux';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventorySelectors, userSelectors } from 'appRedux/selectors';
import { changeImage } from '../utils';


const styles = theme => ({
  cardContainer: {
    display: 'flex',
    flexDirection: 'column',
    padding: 10,
  },
  flipContainer: {
    width: 275,
    height: 250,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  container: {
    minHeight: 500,
    display: 'flex',
    flexDirection: 'column',
    cursor: 'pointer',
    alignItems: 'center',
    padding: 10,
    border: '1px solid rgb(174, 248, 247)',
  },
  infoContainer: {
    justifyContent: 'center',
  },
  image: {
    width: 250,
    height: 250,
  },
  card: {
    border: 'solid 1px',
    borderColor: '#CCCCCC',
    padding: theme.spacing(2),
    height: '100%',
    minHeight: 230,
    cursor: 'pointer',
  },
  cardHover: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  info: {
    textAlign: 'center',
  },
  infoItem: {
    marginTop: 20,
    color: '#a0a0a0',
  },
  itemBlock: {
    flexDirection: 'row',
    display: 'flex',
    marginTop: 20,
    marginBottom: 40,
  },
  messageBlock: {
    marginTop: 5,
    display: 'flex',
    flexDirection: 'column',
  },
  name: {
    padding: 3,
    marginLeft: theme.spacing(-2),
    marginRight: theme.spacing(-2),
    textAlign: 'center',
  },
});


const mapStateToProps = (state, props) => ({
  item: changeImage(props, 'static/images/categories/Category-belgian.jpg'),
  user: userSelectors.userSelector(state),
  inventory: inventorySelectors.inventorySelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators(cartActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(ServicesCard));
