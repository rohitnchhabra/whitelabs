import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import Router from 'next/router';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import LoadingIndicator from 'components/UI/LoadingIndicator';
import { cartActions } from 'appRedux/actions/cartActions';

const customFormValidation = Yup.object().shape({
    quantity: Yup.string()
      .required('Required'),
  });

class RetailVolumePricingDialog extends Component {
    constructor(props) {
        super(props);
    }
    handleErrors = (field, err) => {
        let {errors} = this.state;
        errors[field] = err
        this.setState({errors})
    }

    handleDialogClose() {
        this.props.closeDialog();
    }

    render() {
        return (
            <React.Fragment>
                <DialogContent>
                    <div style={{ width: '100%', textAlign: 'right' ,position: 'absolute', left: '0%',top: '2%'}}>
                        <IconButton
                            color='inherit'
                            size='small'
                            aria-label='Menu'
                            onClick={() => this.handleDialogClose()}
                        >
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Grid
                        item
                        container
                        xs
                        style={{
                            display: 'flex',
                            marginTop: -10,
                            marginBottom: 30
                        }}
                        direction={'row'}
                        spacing={2}
                    >
                        <Grid item>
                            <Typography variant='h5'>
                                <div style={{textAlign:'center',color:'#f28531',marginRight:'10px'}}>
                                    RETAIL STORE VOLUME PRICING LEVELS
                                </div>
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={2}
                        style={{ marginTop: 5 }}
                    >
                        <Grid item>
                            <Typography>
                                <table style={{ border: 'solid 1px black', margin: 'auto' }}>
                                    <tr style={{ fontWeight: 'bold' }}>
                                        <td style={{ border: 'solid 1px black', padding:'2px' }}>Quantity</td>
                                        <td style={{ width: '20px' }} />
                                        <td style={{ border: 'solid 1px black', padding: '2px' }}>Quantity</td>
                                    </tr>
                                    <tr>
                                        <td style={{ border: 'solid 1px black', padding: '2px', textAlign: 'right'  }}>1 - 24</td>
                                        <td />
                                        <td style={{ border: 'solid 1px black', padding: '2px', textAlign: 'right' }}>145 - 432</td>
                                    </tr>
                                    <tr>
                                        <td style={{ border: 'solid 1px black', padding: '2px', textAlign: 'right'  }}>25 - 48</td>
                                        <td />
                                        <td style={{ border: 'solid 1px black', padding: '2px', textAlign: 'right' }}>433 - 864</td>
                                    </tr>
                                    <tr>
                                        <td style={{ border: 'solid 1px black', padding: '2px', textAlign: 'right'  }}>49 - 144</td>
                                        <td colSpan="2" />
                                    </tr>
                                </table>
                            </Typography>
                        </Grid>
                    </Grid>
                </DialogContent>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
    card: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        height: '100%',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        alignItems: 'center',
        padding: 5,
        backgroundColor: '#e4e4e4',
        textAlign: 'center'
    },
    quantity: {
        width: 50
    },
    hide: {
        display: 'none'
    },
    hoverBold:{
        '&:hover': {
            fontWeight:'bolder',
            color:'#ff9933',
            cursor:'pointer'
         }
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(-5)
    },
    close: { position: 'absolute', right: 0, top: 0 },
    form:{
        width:'100%',
    }
});

RetailVolumePricingDialog.propTypes = {
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators(cartActions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(RetailVolumePricingDialog));
