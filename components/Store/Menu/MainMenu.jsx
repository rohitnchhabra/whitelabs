import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Link from 'next/link';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import Router from 'next/router';

import SalesLib from 'lib/SalesLib';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    card: {
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2),
        height: 230
    },
    info: {
        textAlign: 'center'
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: 'center'
    },
    paper: {
        height: 490,
        width: 300,
    },
    control: {
        padding: theme.spacing(2),
    },
    typoTitle: {
        fontWeight: 'bolder', textShadow: '0px 1px, 1px 0px, 1px 1px', letterSpacing: '2px'
    },
    divTitle: {
        margin: 'auto', width: 'auto', justifyContent: 'center'
        , height: '100%', display: 'flex', alignItems: 'center', textAlign: 'center'
    }
});

class MainMenu extends Component {
    render() {
        const { classes, changeMainCategory, checkForData } = this.props;
        //This can be uncommented and used to suppress categories from production if desired
        //const dev = process.env.NODE_ENV !== "production";

        return (
            <div style={{ marginTop: '5%' }}>
                <Grid container spacing={6}>
                    <Grid item xs={12}>
                        <Grid container justify='center' spacing={4} >
                            {SalesLib.CATEGORIES.map(category => (
                                checkForData(category) ?  <Grid key={category.value} item xs={12} sm={6} md={4} lg={3} >
                                <div
                                    style={{
                                        textAlign: 'center',
                                        backgroundImage: `url(${category.img})`,
                                        backgroundRepeat: 'no-repeat',
                                        backgroundSize: 'cover',
                                        height: '430px',
                                        width: '100%',
                                        cursor: 'pointer',
                                        borderRadius: '5px',
                                        boxShadow: '0 2px 5px rgba(0,0,0,.2)'
                                    }}
                                    onClick={() => changeMainCategory(category)}
                                >
                                    <div className={classes.divTitle}>
                                        <Typography
                                            variant='h6'
                                            color='secondary'
                                            className={classes.info}
                                            className={classes.typoTitle}
                                        >
                                            {category.label}
                                        </Typography>
                                    </div>
                                </div>
                            </Grid> : null
                               
                            ))}
                        </Grid>
                    </Grid>
                </Grid>
            </div>
        );
    }
}


const mapStateToProps = state => {
    return {
        store: state.inventory
    };
};

const mapDispatchToProps = dispatch => bindActionCreators(inventoryActions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withWidth()(withStyles(styles, { withTheme: true })(MainMenu)));
