import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { inventoryActions } from 'appRedux/actions/inventoryActions';


const SubCat = props => {
  const { classes, mainCategory, changeSubCategory } = props;
  return (
    <div className={classes.divCat}>
      <Grid container className={classes.root} spacing={4}>
        <Grid item xs={12}>
          <Grid
            container
            className={classes.demo}
            justify='center'
            spacing={8}
          >
                {
                    mainCategory.subCategories
                        ? mainCategory.subCategories.map(category => (
                            (category.value != 998 || !props.user || !props.user.subsidiary || props.user.subsidiary == 2) ?
                                <Grid
                                    key={category.value}
                                    item
                                    xs={12}
                                    sm={6}
                                    md={4}
                                    lg={3}
                                >
                                    <div
                                        className={classes.imgBack}
                                        style={{ backgroundImage: `url(${category.img})` }}
                                        onClick={() => changeSubCategory(category)}
                                    >
                                        <div className={classes.divIcon}>
                                            <img className={classes.imgIcon} src={category.icon} />
                                            <Typography
                                                variant='subtitle1'
                                                color='secondary'
                                                className={classes.titleTypo}
                                            >
                                                {category.label}
                                            </Typography>
                                        </div>
                                    </div>
                                </Grid>
                                : null
                        ))
                        : null
                }
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

const styles = theme => ({
  imgBack: {
    textAlign: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    height: '200px',
    width: 'auto',
    cursor: 'pointer',
    borderRadius: '5px',
    boxShadow: '0 2px 5px rgba(0,0,0,.2)',
  },
  root: {
    flexGrow: 1,
  },
  card: {
    border: 'solid 1px',
    borderColor: '#CCCCCC',
    padding: theme.spacing(2),
    height: 230,
  },
  info: {
    textAlign: 'center',
  },
  name: {
    padding: 3,
    marginLeft: theme.spacing(-2),
    marginRight: theme.spacing(-2),
    textAlign: 'center',
  },
  paper: {
    height: 490,
    width: 300,
  },
  control: {
    padding: theme.spacing(2),
  },
  titleTypo: {
    textShadow: '0px 1px, 1px 0px, 1px 1px',
    letterSpacing: '2px',
    textAlign: 'left',
  },
  divIcon: {
    margin: 'auto',
    width: '80%',
    padding: '0 10% 0 10%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
  },
  imgIcon: {
    width: '30%',
    maxHeight: '55px',
    marginRight: '12px',
  },
  divCat: {
    margin: '6.35%',
    marginTop: 'unset',
    width: 'auto',
    justifyContent: 'center',
    display: 'flex',
    alignItems: 'center',
    textAlign: 'center',
    height: 'auto',
    [theme.breakpoints.down('sm')]: {
      margin: '10%',
    },
  },
});

const mapStateToProps = state => {
  return {
    store: state.inventory,
  };
};

const mapDispatchToProps = dispatch => bindActionCreators(inventoryActions, dispatch);

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(compose(withStyles(styles, { withTheme: true })(SubCat))),
);
