import * as React from 'react';
import ReactCardFlip from 'react-card-flip';
import { isMobile } from 'react-device-detect';

class CardFlip extends React.Component {
    state = {
        hover: false
    };

    handleItemHoverEnter = () => {
        this.setState({ hover: true });
    };

    handleItemHoverLeave = () => {
        this.setState({ hover: false });
    };

    render() {
        //Hacky logic below to try to get around displaying the back side text in reverse in mobile when rendering HTML in the cards
        const mobile = isMobile;

        return (
            <div
                onClick={this.handleItemHoverEnter}
                onMouseEnter={this.handleItemHoverEnter}
                onMouseLeave={this.handleItemHoverLeave}
            >
                <ReactCardFlip isFlipped={this.state.hover} flipDirection="horizontal">
                    {(!this.state.hover || !mobile) && this.props.children && this.props.children.length > 0 ? this.props.children[0] : null}
                    {(this.state.hover || !mobile) && this.props.children && this.props.children.length > 1 ? this.props.children[1] : null}
                </ReactCardFlip>
            </div>
        );
    }
}

export default CardFlip;
