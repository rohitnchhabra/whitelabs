import React from "react";


const CardFlipBackSide = ({ className, children }) => (
  <div className={className}>
    {children}
  </div>
);

export default CardFlipBackSide;
