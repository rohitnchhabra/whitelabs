import React from "react";


const CardFlipFrontSide = ({ className, children }) => (
  <div className={className}>
    {children}
  </div>
);

export default CardFlipFrontSide;
