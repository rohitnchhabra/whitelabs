import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import axios from 'axios';
import isEmpty from 'lodash/isEmpty';
import Router from 'next/router';

import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import LoadingIndicator from 'components/UI/LoadingIndicator';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import { IN_STOCK } from 'lib/Constants';

import { parseAvailabilityResults } from 'lib/InventoryUtils';
import ReactGA from 'react-ga';
import {DISALLOW_BACK_ORDERS} from "../constants";
import ReactHtmlParser from 'react-html-parser'; 
import WLHelper from 'lib/WLHelper';

const customFormValidation = Yup.object().shape({
    quantity: Yup.string().required('Required')
});

class EnzymesNutrientsDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            quantity: '1',
            availability: null,
            availabilitySD: null,
            availabilityCPH: null,
            availabilityHK: null,
            isLoading: false,
            errors: {},
            selectedPack:{pack: ""}
        };

        this.item = this.props.item;

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/store-enzymesnutrients-' + this.item.Name.replace(/\s/g, '-'));
    }
    handleErrors = (field, err) => {
        let { errors } = this.state;
        errors[field] = err;
        this.setState({ errors });
    };
    componentDidMount() {
        if (this.item.pack) {
            this.setState({
                selectedPack: { pack: this.item.pack[0], index: 0 }
            })
        }
    }
    checkQuantity = item => {
        var quantity = parseFloat(item.OrderDetailQty);

        if (isNaN(quantity) || quantity <= 0) {
            this.handleErrors('quantity', 'Please enter a valid value for the quantity');
            return false;
        }

        //  Must be in increments of 1
        else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
            return false;
        }

        return true;
    };

    setPack=(e)=>{
        let pack=e.target.value        
        let index=this.item.pack.indexOf(pack)
        this.setState({
            selectedPack:{pack,index}
        })
    }

    handleDialogClose() {
        this.props.closeDialog();
    }

    addToCart = values => {
        var quantity = this.state.quantity;
        var item = this.item;
        const { selectedPack } = this.state;
        // Create cart item
        var cartItem = {};
        cartItem.Name = String(item.Name);

        if (item.pack && selectedPack) {
            cartItem.MerchandiseID = item.volID[selectedPack.index];
            cartItem.details = (item.pack ? item.pack[selectedPack.index] : '');
        } else {
            cartItem.MerchandiseID = item.volID[0];
        }

        cartItem.salesCategory = parseInt(item.salesCategory);
        cartItem.type = 3;
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.imageUrl = (item.imageUrl ? item.imageUrl : 'static/images/categories/Category-ale.jpg');

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    relItemArray[k].cartItemType = WLHelper.getYMO2TypeForItem(relatedItems[i].related[j], relItemArray[k]);
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    })
                    .catch(error => {
                        console.log('error', error);

                        this.setState({ isLoading: false });
                        // Still add the item to the cart
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    });
            }
        }
    };

    checkAvailability = () => {
        ReactGA.pageview('/store-enzymesnutrients-availability-' + this.item.Name.replace(/\s/g, '-'));

        var item = this.item;
        const { selectedPack } = this.state;

        var itemID;

        if (item.pack && selectedPack && selectedPack.index) {
            itemID = item.volID[selectedPack.index];
        } else {
            itemID = item.volID[0];
        }

        var { subsidiary } = this.props.user;

        this.setState({ isLoading: true });
        if (!subsidiary || subsidiary === '') {
            var subs = [2, 5, 7];
            this.setState({ availability: null });

            // Hacky "switch" statement to handle indeterminate async call duration
            // cross-contaminating result variables. Also, separate availabilities to make
            // sure they are always displayed in the same order in the results.
            for (var i = 0; i < subs.length; i++) {
                this.setState({ isLoading: true });
                switch (i) {
                    case 0:
                        var s = subs[i];
                        subsidiary = s;
                        axios.post('/item-availability', { itemID, subsidiary })
                            .then(({ data: { availability, error } }) => {
                                if (error) throw error;
                                this.setState({ availabilitySD: parseAvailabilityResults(availability, s) });
                            })
                            .catch(error => {
                                console.log('error', error);
                            })
                            .finally(() => this.setState({ isLoading: false }))
                        break;
                    case 1:
                        var s1 = subs[i];
                        subsidiary = s1;
                        axios.post('/item-availability', { itemID, subsidiary })
                            .then(({ data: { availability, error } }) => {
                                if (error) throw error;
                                this.setState({ availabilityHK: parseAvailabilityResults(availability, s1) });
                            })
                            .catch(error => {
                                console.log('error', error);
                            })
                            .finally(() => this.setState({ isLoading: false }))
                        break;
                    case 2:
                        var s2 = subs[i];
                        subsidiary = s2;
                        axios.post('/item-availability', { itemID, subsidiary })
                            .then(({ data: { availability, error } }) => {
                                if (error) throw error2;
                                this.setState({ availabilityCPH: parseAvailabilityResults(availability, s2) });
                            })
                            .catch(error => {
                                console.log('error', error);
                            })
                            .finally(() => this.setState({ isLoading: false }))
                        break;
                }
            }
        } else {
            this.setState({ availabilitySD: null });
            this.setState({ availabilityCPH: null });
            this.setState({ availabilityHK: null });

            axios.post('/item-availability', { itemID, subsidiary })
                .then(({ data: { availability, error } }) => {
                    if (error) throw error;
                    this.setState({ availability: parseAvailabilityResults(availability, subsidiary) });
                })
                .catch(error => {
                    console.log('error', error);
                })
                .finally(() => this.setState({ isLoading: false }));
        }
    };

    changeQuantity = event => {
        this.setState({ quantity: event.target.value });
    };

    handleClick = (partNum) => {
        partNum = encodeURIComponent(partNum);
        Router.push(`/enzymeparam?item=${partNum}`)
        this.props.setPageData(this.props.stateData);
    }

    render() {
        const { classes, item } = this.props;
        const { partNum } = item;
        const { errors, availability, availabilityCPH, availabilityHK, availabilitySD } = this.state;
        const spaceIndex = item.Name.indexOf(' ');
        const itemID = item.Name.substr(0, spaceIndex);
        const itemName = item.Name.substr(spaceIndex + 1);

        const nextShipDateMsg = (
            availability && availability.startsWith(IN_STOCK) ? '' :
                (availabilitySD && !availabilitySD.startsWith(IN_STOCK))
                    || (availabilityCPH && !availabilityCPH.startsWith(IN_STOCK))
                    || (availabilityHK && !availabilityHK.startsWith(IN_STOCK)) ? '(next available ship date shown in checkout)' : ''
        );

        var itemDescription = '<div style="max-height:480px; overflow-y: auto; overflow-x: hidden;">' + this.item.Description + '</div>';
        itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

        return (
            <React.Fragment>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <DialogContent>
                    <div className={classes.close}>
                        <IconButton color='inherit' size='small' aria-label='Menu' onClick={() => this.handleDialogClose()}>
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Grid
                        item
                        container
                        xs
                        style={{
                            display: 'flex',
                            marginTop: -10,
                            marginBottom: 20
                        }}
                        direction={'row'}
                        spacing={2}
                    >
                        <Grid item>
                            <Typography variant='h5' className={classes.hoverBold} onClick={() => this.handleClick(partNum)}>
                                {itemID} | {itemName}
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid item container direction={'column'} spacing={2} style={{ marginTop: 5 }}>
                        <Grid item>
                            <div>{ReactHtmlParser(itemDescription)}</div>
                            {this.item.outofstockmessage &&
                                <Typography style={{ paddingTop: '10px', fontWeight: 'bold' }}>
                                    {this.item.outofstockmessage}
                                </Typography>
                            }
                        </Grid>
                    </Grid>

                    <Grid item container style={{ marginTop: 5 }} direction={'row'}>
                        <Grid item xs container spacing={6} direction={'row'} justify='flex-start'>
                            {availability ?
                                <div>
                                    <Typography className='flex-center' style={{ color: availability.startsWith(IN_STOCK) ? 'green' : 'red' }}>
                                        <p style={{ textAlign: 'center' }}>{availability}</p>
                                    </Typography>
                                    <Typography className='flex-center'>
                                        <p style={{ textAlign: 'center' }}>{nextShipDateMsg}</p>
                                    </Typography>
                                </div>
                                :
                                (availabilitySD || availabilityCPH || availabilityHK) ?
                                    <div>
                                        <Typography className='flex-center'>
                                            <p style={{ textAlign: 'center', color: availabilitySD && availabilitySD.startsWith(IN_STOCK) ? 'green' : 'red' }}>{availabilitySD}</p>
                                        </Typography>
                                        <Typography className='flex-center'>
                                            <p style={{ textAlign: 'center', color: availabilityCPH && availabilityCPH.startsWith(IN_STOCK) ? 'green' : 'red' }}>{availabilityCPH}</p>
                                        </Typography>
                                        <Typography className='flex-center'>
                                            <p style={{ textAlign: 'center', color: availabilityHK && availabilityHK.startsWith(IN_STOCK) ? 'green' : 'red' }}>{availabilityHK}</p>
                                        </Typography>
                                        <Typography className='flex-center'>
                                            <p style={{ textAlign: 'center' }}>{nextShipDateMsg}</p>
                                        </Typography>
                                    </div>
                                    :
                                    <Grid item xs container spacing={6} direction={'row'} justify='flex-end'>
                                        <Grid item>
                                            <div className={classes.buttons}>
                                                <Button variant='contained' color='primary' onClick={this.checkAvailability} className={classes.button}>
                                                    Get Availability
                                            </Button>
                                            </div>
                                        </Grid>
                                    </Grid>
                            }
                        </Grid>
                    </Grid>
                    {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                        <Grid item xs container spacing={6} style={{ marginTop: 5 }} direction={'row'}>
                            <Formik
                                initialValues={this.state}
                                validationSchema={customFormValidation}
                                onSubmit={values => this.addToCart(values)}
                            >
                                {({ values, handleChange }) => {
                                    return (
                                        <Form className={classes.form}>
                                            {errors.quantity && <div className='error'>{errors.quantity}</div>}
                                            <Grid item xs container spacing={6} direction={'row'} justify='flex-start'>
                                                <Grid item>
                                                    <TextField id='quantity' label='Quantity' className={classes.quantity} value={this.state.quantity} onChange={this.changeQuantity} type='number' />
                                                </Grid>
                                                {this.item.pack &&
                                                    <Grid item xs={12} sm={4} md={4} className={classes.formFields} style={{ minWidth: '50%' }}>
                                                        <FormControl>
                                                            <InputLabel>Pack</InputLabel>
                                                            <Select
                                                                value={this.state.selectedPack.pack}
                                                                onChange={this.setPack}
                                                            >
                                                                {item.pack.map(
                                                                    (option, i) => (

                                                                        <MenuItem
                                                                            key={i}
                                                                            value={option}
                                                                        >
                                                                            {option}
                                                                        </MenuItem>
                                                                    )
                                                                )}
                                                            </Select>
                                                        </FormControl>
                                                    </Grid>
                                                }
                                                {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                                                    <Grid item xs container spacing={6} direction={'row'} justify='flex-end'>
                                                        <Grid item>
                                                            <div className={classes.buttons} style={{ whiteSpace: 'nowrap' }}>
                                                                <Button
                                                                    type='submit'
                                                                    variant='contained'
                                                                    color='primary'
                                                                    className={classes.button}
                                                                >
                                                                    Add to Cart
                                                        </Button>
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                                }
                                            </Grid>
                                        </Form>
                                    );
                                }}
                            </Formik>
                        </Grid>
                    }
                </DialogContent>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
    card: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        height: '100%',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        alignItems: 'center',
        padding: 5,
        backgroundColor: '#e4e4e4',
        textAlign: 'center'
    },
    quantity: {
        width: 50
    },
    hide: {
        display: 'none'
    },
    hoverBold: {
        '&:hover': {
            fontWeight: 'bolder',
            color: '#ff9933',
            cursor: 'pointer'
        }
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(-5)
    },
    close: { position: 'absolute', right: 0, top: 0 },
    form: {
        width: '100%'
    }
});

EnzymesNutrientsDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user,
        inventory: state.inventory
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions, ...inventoryActions }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(EnzymesNutrientsDialog));
