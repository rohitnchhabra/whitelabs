import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';
import AlcoholCard from './AlcoholCard';
import { bindActionCreators } from 'redux';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventorySelectors, userSelectors } from 'appRedux/selectors';
import { changeImage } from '../utils';

const styles = theme => ({
  cardContainer: {
    display: 'flex',
    flexDirection: 'column',
    padding: 10,
  },
  flipContainer: {
    width: 275,
    height: 250,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  container: {
    minHeight: 500,
    display: 'flex',
    cursor: 'pointer',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 10,
    border: '1px solid rgba(0, 0, 0, 0.54)',
  },
  nameItem: {
    textAlign: 'center',
    marginTop: 20,
    color: '#a0a0a0',
  },
  priceItem: {
    textAlign: 'center',
  },
  image: {
      maxWidth: 250,
      maxHeight: 250,
      height: 'auto',
  },
  card: {
    border: 'solid 1px',
    borderColor: '#CCCCCC',
    padding: theme.spacing(2),
    width: 400,
    height: 250,
    minHeight: 250,
  },
  cardHover: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  info: {
    textAlign: 'bottom',
  },
  name: {
    padding: 3,
    marginLeft: theme.spacing(-2),
    marginRight: theme.spacing(-2),
    textAlign: 'center',
  },
});


const mapStateToProps = (state, props) => ({
  item: changeImage(props, 'static/images/categories/Category-alcohol.jpg'),
  user: userSelectors.userSelector(state),
  inventory: inventorySelectors.inventorySelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators(cartActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(AlcoholCard));
