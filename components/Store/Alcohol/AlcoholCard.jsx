import React, { Component } from 'react';

import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { CardFlip, CardFlipBackSide, CardFlipFrontSide } from '../CardFlip';
import { REMOVE_ITEM } from "../constants";
import ReactHtmlParser from 'react-html-parser';
import { isMobile, getUA } from "react-device-detect";

class AlcoholCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { classes, item } = this.props;

        var displayPrice = '';
        if (item.displayPrice) {
            if (item.displayPrice.length == 1) {
                displayPrice = '$' + item.displayPrice[0];
            } else {
                var displayPriceAmount = 0;
                for (var i = 0; i < item.displayPrice.length; i++) {
                    if (displayPriceAmount == 0 || displayPriceAmount < parseFloat(item.displayPrice[i])) {
                        if (displayPriceAmount != 0) {
                            displayPrice = 'from $';
                        } else {
                            displayPrice = '$';
                        }
                        displayPrice += item.displayPrice[i];
                        displayPriceAmount = parseFloat(item.displayPrice[i]);
                    }
                }
            }
        }

        const dev = process.env.NODE_ENV !== "production";
        var itemDescription = item.Description;
        var backItemDescription = item.Description;

        if (dev || true) {
            itemDescription = '<div style="max-height:160px; overflow-y: auto; overflow-x: hidden;">' + item.Description + '</div>';
            itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

            backItemDescription = '<div style="max-height:250px; overflow-y: auto; overflow-x: hidden;">' + item.Description + '</div>';
            backItemDescription = backItemDescription.replace(/font-family/g, 'font-family-ignore');
        } else {
            var descrMaxLength = (isMobile ? 250 : (getUA.indexOf('CrOS') >= 0 ? 85 : 360));
            if (itemDescription.length > descrMaxLength) {
                itemDescription = itemDescription.substring(0, descrMaxLength - 4);
                while (!itemDescription.endsWith(' ')) itemDescription = itemDescription.substring(0, itemDescription.length - 1);
                itemDescription += ' ...';
            }

            if (backItemDescription.length > descrMaxLength * 3) {
                backItemDescription = backItemDescription.substring(0, (descrMaxLength * 3) - 4);
                while (!backItemDescription.endsWith(' ')) backItemDescription = backItemDescription.substring(0, backItemDescription.length - 1);
                backItemDescription += ' ...';
            }
        }

        return (
            <>
                {item.outofstockbehavior !== REMOVE_ITEM &&
                    <Grid
                        item
                        container
                        xs={12}
                        sm={12}
                        md={6}
                        lg={4}
                        xl={3}
                        spacing={2}
                        onClick={this.props.onClick.bind(this, this.props.item)}
                        className={classes.cardContainer}
                    >
                        <CardFlip>
                            <CardFlipFrontSide>
                                <div className={classes.container}>
                                    <div className={classes.flipContainer}>
                                        <img className={classes.image} src={item.ImageURL} />
                                    </div>

                                    <Grid item container spacing={2}>
                                        <Grid
                                            item
                                            xs={12}
                                            className={classes.info}
                                        >
                                            <Typography
                                                variant='h6'
                                                className={classes.nameItem}
                                            >
                                                {item.Name}
                                            </Typography>
                                            <Typography
                                                variant='h6'
                                                className={classes.priceItem}>
                                                {displayPrice}
                                            </Typography>
                                        <div>{ReactHtmlParser(itemDescription)}</div>
                                        </Grid>
                                    </Grid>
                                </div>
                            </CardFlipFrontSide>
                            <CardFlipBackSide>
                                <div className={classes.container}>
                                    <div className={classes.flipContainer}>
                                        {item.Name}
                                    </div>
                                    <Grid item container spacing={2}>
                                        <Grid
                                            item
                                            xs={12}
                                            className={classes.infoContainer}
                                        >
                                            <Grid
                                                item
                                                xs={12}
                                                className={classes.info}
                                            >
                                                <Typography
                                                    variant='h6'
                                                    className={classes.priceItem}>
                                                    {displayPrice}
                                                </Typography>
                                            <div>{ReactHtmlParser(backItemDescription)}</div>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </div>
                            </CardFlipBackSide>
                        </CardFlip>
                    </Grid>
                }
            </>
        );
    }
}

const styles = theme => ({
    card: {
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2),
        height: '100%',
        minHeight:230,
        cursor: 'pointer'
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        textAlign: 'center'
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: 'center'
    }
});

AlcoholCard.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  item: PropTypes.shape({
    Name: PropTypes.string,
    ImageURL: PropTypes.string,
    outofstockbehavior: PropTypes.string,
    Price: PropTypes.string,
  }),
};

export default AlcoholCard;
