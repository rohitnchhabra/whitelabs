import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { CardFlip, CardFlipBackSide, CardFlipFrontSide } from '../CardFlip';
import { DISALLOW_BACK_ORDERS, DISPLAY_OUT_OF_STOCK, REMOVE_ITEM, NO_OUT_OF_STOCK } from "../constants";
import ReactHtmlParser from 'react-html-parser';
import { isMobile, getUA } from "react-device-detect";

class EducationCard extends Component {
    render() {
        const { classes, item } = this.props;

        var displayPrice = '';
        if (item.displayPrice) {
            if (item.displayPrice.length == 1) {
                displayPrice = '$' + item.displayPrice[0];
            } else {
                for (var i = 0; i < item.displayPrice.length; i++) {
                    if (displayPrice.length > 0) displayPrice += '<br />';
                    if (item.option && i < item.option.length) {
                        displayPrice += item.option[i];
                        displayPrice += ': ';
                    }
                    displayPrice += '$';
                    displayPrice += item.displayPrice[i];
                }
            }
        }

        const dev = process.env.NODE_ENV !== "production";
        var itemDescription = item.Description;
        var backItemDescription = item.Description;

        if (dev || true) {
            itemDescription = '<div style="max-height:160px; overflow-y: auto; overflow-x: hidden;">' + item.Description + '</div>';
            itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

            backItemDescription = '<div style="max-height:250px; overflow-y: auto; overflow-x: hidden;">' + item.Description + '</div>';
            backItemDescription = backItemDescription.replace(/font-family/g, 'font-family-ignore');
        } else {
            var descrMaxLength = (isMobile ? 250 : (getUA.indexOf('CrOS') >= 0 ? 85 : 360));
            if (itemDescription.length > descrMaxLength) {
                itemDescription = itemDescription.substring(0, descrMaxLength - 4);
                while (!itemDescription.endsWith(' ')) itemDescription = itemDescription.substring(0, itemDescription.length - 1);
                itemDescription += ' ...';
            }

            if (backItemDescription.length > descrMaxLength * 3) {
                backItemDescription = backItemDescription.substring(0, (descrMaxLength * 3) - 4);
                while (!backItemDescription.endsWith(' ')) backItemDescription = backItemDescription.substring(0, backItemDescription.length - 1);
                backItemDescription += ' ...';
            }
        }

        return (
            <>
                {item.outofstockbehavior !== REMOVE_ITEM && <Grid
                    item
                    xs={12}
                    sm={12}
                    md={6}
                    lg={4}
                    xl={3}
                    spacing={2}
                    container
                    onClick={this.props.onClick.bind(this, this.props.item)}
                    className={classes.cardContainer}
                >
                    <CardFlip>
                        <CardFlipFrontSide className={classes.container}>
                            <div className={classes.flipContainer}>
                                <img className={classes.image} src={item.ImageURL} />
                            </div>
                            <Grid
                                item
                                container
                                spacing={2}
                            >
                                <Grid
                                    item
                                    xs={12}
                                    className={classes.info}
                                >
                                    <Typography className={classes.infoItem} variant='h6'>
                                        {item.Name}
                                    </Typography>
                                    <div>{ReactHtmlParser(itemDescription)}</div>
                                </Grid>
                            </Grid>
                        </CardFlipFrontSide>
                        <CardFlipBackSide className={classes.container}>
                            <div className={classes.flipContainer}>
                                <Grid item container direction={'column'} spacing={2}>
                                    <Grid item xs={12}>
                                        <div className={classes.nameContainer}>
                                            <Typography color='secondary' variant='h6'>
                                                {(item.Name ? item.Name : item.partNum)}
                                            </Typography>
                                            <Typography style={{ fontSize: 'smaller' }} color='secondary' dangerouslySetInnerHTML={{ __html: displayPrice }} />
                                        </div>
                                    </Grid>
                                    <Grid
                                        item
                                        xs
                                        container
                                        style={{ whiteSpace: 'nowrap' }}
                                    >
                                        {!item.TagLocation ? null :
                                            <Grid item xs={6}>
                                                <div className={classes.info}>
                                                    <Typography>Class Location</Typography>
                                                    <Typography className={classes.dataBlock}>
                                                        {item.TagLocation}
                                                    </Typography>
                                                </div>
                                            </Grid>
                                        }
                                        {!item.TagDate ? null :
                                            <Grid item xs={6}>
                                                <div className={classes.info}>
                                                    <Typography>Date</Typography>
                                                    <Typography className={classes.dataBlock}>
                                                        {item.TagDate}
                                                    </Typography>
                                                </div>
                                            </Grid>
                                        }
                                    </Grid>
                                </Grid>
                            </div>
                            <Grid
                                item
                                container
                                spacing={2}
                            >
                                <Grid
                                    item
                                    xs={12}
                                    className={classes.info}
                                >
                                    <Grid item container direction={'column'} spacing={2}>
                                        <Grid
                                            item
                                            xs
                                            container
                                        >
                                            <div>{ReactHtmlParser(backItemDescription)}</div>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </CardFlipBackSide>
                    </CardFlip>
                </Grid>
                }
            </>
        );
    }
}

EducationCard.propTypes = {
  classes: PropTypes.object.isRequired,
  item: PropTypes.shape({
    Name: PropTypes.string,
    ImageURL: PropTypes.string,
    TagDate: PropTypes.string,
    TagLocation: PropTypes.string,
  }),
};

export default EducationCard;
