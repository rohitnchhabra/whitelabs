import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';
import EducationCard from './EducationCard';
import { bindActionCreators } from 'redux';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventorySelectors, userSelectors } from 'appRedux/selectors';
import { changeImage } from '../utils';


const styles = theme => ({
    cardContainer: {
        display: 'flex',
        flexDirection: 'column',
        padding: 10,
    },
    flipContainer: {
        width: 275,
        height: 250,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        alignText: 'center',
    },
    container: {
        minHeight: 500,
        display: 'flex',
        cursor: 'pointer',
        alignItems: 'center',
        padding: 10,
        flexDirection: 'column',
        border: '1px solid rgb(173, 131, 205)',
    },
    infoContainer: {
        justifyContent: 'center',
    },
    image: {
        maxWidth: 250,
        maxHeight: 250,
        height: 'auto',
    },
    card: {
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2),
        height: '100%',
        minHeight: 230,
        cursor: 'pointer',
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    info: {
        textAlign: 'center',
    },
    infoItem: {
        marginTop: 20,
        color: '#a0a0a0',
    },
    nameContainer: {
        backgroundColor: '#ad83cd',
        padding: 1,
        textAlign: 'center',
    },
    dataBlock: {
        color: '#ad83cd',
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: 'center',
    },
});

const mapStateToProps = (state, props) => ({
  item: changeImage(props, 'static/images/categories/Category-wine.jpg'),
  user: userSelectors.userSelector(state),
  inventory: inventorySelectors.inventorySelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators(cartActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(EducationCard));
