import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Router from 'next/router';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';

import SalesLib from 'lib/SalesLib';
import {formatDate} from 'lib/Utils';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import { cartActions } from 'appRedux/actions/cartActions';
import ReactGA from 'react-ga';
import { log } from "util";
import ReactHtmlParser from 'react-html-parser'; 
import {DISALLOW_BACK_ORDERS,DISPLAY_OUT_OF_STOCK,REMOVE_ITEM,NO_OUT_OF_STOCK} from "../constants";
import axios from 'axios';

import LoadingIndicator from 'components/UI/LoadingIndicator';
import WLHelper from 'lib/WLHelper';

const customFormValidation = Yup.object().shape({
    quantity: Yup.string()
        .required('Required'),
});

class EducationDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: "1",
            ques1:'',
            ques2:'',
            types: [],
            selectedType: '',
            errors: {},
            selectedOption: { option: '' },
            isLoading: false,
        };

        this.item = this.props.item;

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/store-education-' + this.item.Name.replace(/\s/g, '-'));
    }

    UNSAFE_componentWillMount() {
        if (this.item.volID.length > 1) {
            var types = [],
                selectedType,
                possibleTypes = [
                    { label: 'In person', value: 0 },
                    { label: 'Webinar', value: 1 }
                ];

            for (var i in this.item.volID) {
                if (this.item.volID[i] != null) {
                    types.push(possibleTypes[i]);
                }
            }

            selectedType = types[0].value;
            this.setState({ types, selectedType });
        }
    }

    componentDidMount() {
        const {option}=this.props.item
        if (option) {
            this.setState({
                selectedOption: { option: option[0], index: 0 }
            })
        }
    }

    handleErrors = (field, err) => {
        let { errors } = this.state;
        errors[field] = err
        this.setState({ errors })
    }

    checkQuantity = item => {
        var quantity = parseFloat(item.OrderDetailQty);

        if (isNaN(quantity) || quantity <= 0) {
            this.handleErrors('quantity', 'Please enter a valid value for the quantity')
            return false;
        }

        //  Must be in increments of 1
        else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
            return false;
        }

        return true;
    };

    setOption=(e)=>{
        let option=e.target.value        
        let index=this.item.option.indexOf(option)
        this.setState({
            selectedOption:{option,index}
        })
    }

    addToCart = (values) => {
        var quantity = this.state.quantity;
        var ques1=this.state.ques1;
        var ques2=this.state.ques2;
        var item = this.item;
        const { selectedOption } = this.state;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(item.Name ? item.Name : item.partNum);
        cartItem.partNum = item.partNum;
        cartItem.MerchandiseID = item.volID[selectedOption.index];
        cartItem.salesCategory = parseInt(item.salesCategory);
        cartItem.type = 4;
        cartItem.details = '';
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.extraParams = {
            ques1: ques1,
            ques2: ques2
        }
        cartItem.imageUrl = (item.imageUrl ? item.imageUrl : '/static/images/categories/Category-wine.jpg');

        if (item.volID.length > 1) {
            switch (selectedOption.option) {
                case 'in-person':
                    cartItem.details = 'In person | ';
                    break;
                case 'webinar':
                    cartItem.details = 'Webinar | ';
                    break;
                default:
                    break;
            }
        }

        if (item.TagLocation || item.TagDate) {
            if (item.TagDate) {
                if (cartItem.details) cartItem.details += '\n';
                cartItem.details +=
                    'Class Date(s): ' +
                    item.TagDate;
            }
            if (item.TagLocation) {
                if (cartItem.details) cartItem.details += '\n';
                cartItem.details +=
                    'Class Location: ' +
                    item.TagLocation;
            }
        }

        if (item.partNum.indexOf("WLEDU-WEB") >= 0 && !item.TagDate) {
                // On-demand class
                cartItem.isOnDemandCourse = true;
                if (cartItem.details) cartItem.details += '\n';
                cartItem.details += "On-Demand Course";
        }

        if (SalesLib.YeastEssentials.includes(cartItem.MerchandiseID)) {
            this.handleErrors('yeastEssentials', 'Attending Yeast Essentials? If you are considering or already attending Yeast Essentials 2.0, consider attending the 1 day Lab Practicum course that follows each Yeast Essentials course and allows you to apply the skills that you learn.')
        }

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    relItemArray[k].cartItemType = WLHelper.getYMO2TypeForItem(relatedItems[i].related[j], relItemArray[k]);
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    })
                    .catch(error => {
                        console.log('error', error);
                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    });
            }
        }
    };

    setType = event => {
        this.setState({ selectedType: event.target.value });
    };

    change=(e)=>{
        this.setState({
            [e.target.name]:e.target.value
        })
      }

    handleDialogClose() {
        this.props.closeDialog();
    }

    handleClick = (partNum) => {
        partNum = encodeURIComponent(partNum);
        Router.push(`/educationparam?item=${partNum}`)
        this.props.setPageData(this.props.stateData);
    }

    render() {
        const { partNum } = this.props.item;        
        const { classes, theme, item } = this.props;
        const { errors } = this.state;

        var itemDescription = '<div style="max-height:480px; overflow-y: auto; overflow-x: hidden;">' + this.item.Description + '</div>';
        itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

        var displayPrice = '';
        if (item.displayPrice) {
            if (item.displayPrice.length == 1) {
                displayPrice = '$' + item.displayPrice[0];
            } else {
                for (var i = 0; i < item.displayPrice.length; i++) {
                    if (displayPrice.length > 0) displayPrice += '<br />';
                    if (item.option && i < item.option.length) {
                        displayPrice += item.option[i];
                        displayPrice += ': ';
                    }
                    displayPrice += '$';
                    displayPrice += item.displayPrice[i];
                }
            }
        }
        
        return (
            <React.Fragment>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <DialogContent>
                    <div className={classes.close}>
                        <IconButton
                            color='inherit'
                            size='small'
                            aria-label='Menu'
                            onClick={() => this.handleDialogClose()}
                        >
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Grid
                        item
                        container
                        xs
                        style={{
                            display: 'flex',
                            marginTop: -10,
                            marginBottom: 20
                        }}
                        direction={'row'}
                        spacing={2}
                    >
                        <Grid item>
                            <Typography variant='h5' className={classes.hoverBold} onClick={() => this.handleClick(partNum)}>
                                {(item.Name ? item.Name : item.partNum)}
                            </Typography>
                        </Grid>
                    </Grid>
                        <Grid item container direction={'row'} spacing={2}>
                        {!item.TagLocation ? null :
                            <Grid item xs>
                                <div style={{ display: 'flex' }}>
                                    <Typography>Class Location: </Typography>
                                    &nbsp;
                                    <Typography
                                        style={{
                                            color: '#66CCCC'
                                        }}
                                    >
                                        {this.item.TagLocation}
                                    </Typography>
                                </div>
                            </Grid>
                        }
                        {!item.TagDate ? null :
                            <Grid item xs={12} md={6}>
                                <div style={{ display: 'flex' }}>
                                    <Typography>Date: </Typography>
                                    &nbsp;
                                    <Typography
                                        style={{
                                            color: '#66CCCC'
                                        }}
                                    >
                                        {this.item.TagDate}
                                    </Typography>
                                </div>
                            </Grid>
                        }
                        </Grid>
                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={2}
                        style={{ marginTop: 5 }}
                    >
                        <Grid item>
                            <div>{ReactHtmlParser(this.item.Description)}</div>
                            {this.item.outofstockmessage &&
                                <Typography style={{ paddingTop: '10px', fontWeight: 'bold' }}>
                                    {this.item.outofstockmessage}
                                </Typography>
                            }
                        </Grid>
                    </Grid>
                    {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&

                        <Grid
                            item
                            xs
                            container
                            spacing={6}
                            style={{ marginTop: 5 }}
                            direction={'row'}
                    >
                            <Typography style={{ fontSize: 'smaller', fontWeight: 'bolder', marginLeft: 'auto', marginRight: 'auto' }} dangerouslySetInnerHTML={{ __html: displayPrice }} />

                            <Formik
                                initialValues={this.state}
                                validationSchema={customFormValidation}
                                onSubmit={values => this.addToCart(values)}
                            >
                                {({ values, handleChange }) => {
                                    return (
                                        <Form className={classes.form}>
                                            {errors.YeastEssentials && <div className='info'  >* {errors.YeastEssentials}</div>}
                                            {errors.quantity && <div className='error' >* {errors.quantity}</div>}
                                            <Grid
                                                item
                                                xs
                                                container
                                                spacing={6}
                                                direction={"row"}
                                                justify="center"
                                                className={classes.paddingFix}
                                            >
                                                <Grid item>
                                                    <TextField
                                                        id="quantity"
                                                        name="quantity"
                                                        label="Quantity"
                                                        className={classes.quantity}
                                                        value={this.state.quantity}
                                                        onChange={e => this.change(e)}
                                                        type="number"
                                                    />
                                                </Grid>
                                                {item.option &&
                                                    <Grid item xs={12} sm={4} md={4} className={classes.formFields}>
                                                        <FormControl>
                                                            <InputLabel>Class&nbsp;Type</InputLabel>
                                                            <Select
                                                                value={this.state.selectedOption.option}
                                                                onChange={this.setOption}
                                                            >
                                                                {this.item.option.map(
                                                                    (curr, i) => (

                                                                        <MenuItem
                                                                            key={i}
                                                                            value={curr}
                                                                        >
                                                                            {curr == 'in-person' ? 'In Person' : 'Webinar'}
                                                                        </MenuItem>
                                                                    )
                                                                )}
                                                            </Select>
                                                        </FormControl>
                                                    </Grid>
                                                }
                                                {this.item.partNum.indexOf("WLEDU-WEB") >= 0 ? null :
                                                    <Grid>
                                                        <Grid item xs={12}>
                                                            <Typography variant="subheading" style={{ display: 'flex', justifyContent: 'center' }}>
                                                                How Did You hear about this class ?
                                                            </Typography>
                                                        </Grid>

                                                        <Grid item>
                                                            <TextField
                                                                InputProps={{
                                                                    classes: {
                                                                        input: classes.textField,
                                                                    }
                                                                }}
                                                                multiline
                                                                rowsMax="4"
                                                                id="ques1"
                                                                name="ques1"
                                                                label=""
                                                                variant="outlined"
                                                                value={this.state.ques1}
                                                                onChange={e => this.change(e)}
                                                            />
                                                        </Grid>

                                                        <Grid item xs={12}>
                                                            <Typography variant="subheading" style={{ display: 'flex', justifyContent: 'center' }}>
                                                                Please Mention the attendee Name.
                                                    </Typography>
                                                        </Grid>

                                                        <Grid item >
                                                            <TextField
                                                                InputProps={{
                                                                    classes: {
                                                                        input: classes.textField,
                                                                    }
                                                                }}
                                                                id="ques2"
                                                                name="ques2"
                                                                label=""
                                                                variant="outlined"
                                                                value={this.state.ques2}
                                                                onChange={e => this.change(e)}
                                                            />
                                                        </Grid>
                                                    </Grid>
                                                }
                                            </Grid>
                                            {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                                                <Grid
                                                    item
                                                    xs
                                                    container
                                                    spacing={6}
                                                    direction={"row"}
                                                    justify="center"
                                                >
                                                    <div className={classes.addButton}>
                                                        <Button
                                                            //className={classes.button}
                                                            type="submit"
                                                            variant="contained"
                                                            color="primary"
                                                        // className={classes.button}
                                                        >
                                                            Add to Cart
                                            </Button>
                                                    </div>
                                                </Grid>
                                            }
                                        </Form>
                                    )
                                }}
                            </Formik>
                        </Grid>
                    }
                </DialogContent>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
    info: {
        textAlign: 'center'
    },
    quantity: {
        width: 50
    },
    hide: {
        display: 'none'
    },
    circle: {
        textAlign: 'center',
        position: 'absolute',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '50%',
        padding: 5,
        width: 37,
        height: 37
    },
    buttons: {
        display: "flex",
        justifyContent: "center"
    },

    paddingFix:{
        paddingLeft:'unset',
        marginTop:'5px',
        [theme.breakpoints.between("sm", "xl")]: {
           paddingLeft:'45px',
       },
       [theme.breakpoints.down("xs")]: {
           paddingLeft:'44px',
       },

},
addButton:{
    display: "flex",
    justifyContent: "center",
    marginLeft:'42px',
    marginTop:'14px',
    marginBottom:'30px'
    // [theme.breakpoints.down("xs")]: {
    //     marginLeft:'16px',
    // }


},
    hoverBold:{
        '&:hover': {
            fontWeight: 'bolder',
            color: '#ff9933',
            cursor: 'pointer'
        }
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(-5)
    },
    close: { position: "absolute", right: 0, top: 0 },
    form:{
        width:'100%',
    },
    textField:{
    height: '40px !important',
    width: '40vw',
    maxWidth: '500px',
    minWidth: '203px'
    }
});

EducationDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user,
        inventory: state.inventory
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({ ...cartActions, ...inventoryActions }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(EducationDialog));
