export const changeImage = (props, image) => {
  if (!props.item.ImageURL) {
    let item = {...props.item, ImageURL: image};
    return item;
  }
  return props.item;
};
