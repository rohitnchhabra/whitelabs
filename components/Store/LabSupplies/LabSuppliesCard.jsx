import React, { Component } from 'react';

import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { CardFlip, CardFlipBackSide, CardFlipFrontSide } from '../CardFlip';
import { REMOVE_ITEM } from "../constants";
import ReactHtmlParser from 'react-html-parser';
import { isMobile, getUA } from "react-device-detect";

class LabSuppliesCard extends Component {
    render() {
        const { classes, item } = this.props;
        const spaceIndex = item.Name.indexOf(' ');

        const dev = process.env.NODE_ENV !== "production";
        var itemDescription = item.Description;
        var backItemDescription = item.Description;

        if (dev || true) {
            itemDescription = '<div style="max-height:160px; overflow-y: auto; overflow-x: hidden;">' + item.Description + '</div>';
            itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

            backItemDescription = '<div style="max-height:250px; overflow-y: auto; overflow-x: hidden;">' + item.Description + '</div>';
            backItemDescription = backItemDescription.replace(/font-family/g, 'font-family-ignore');
        } else {
            var descrMaxLength = (isMobile ? 250 : (getUA.indexOf('CrOS') >= 0 ? 85 : 360));
            if (itemDescription.length > descrMaxLength) {
                itemDescription = itemDescription.substring(0, descrMaxLength - 4);
                while (!itemDescription.endsWith(' ')) itemDescription = itemDescription.substring(0, itemDescription.length - 1);
                itemDescription += ' ...';
            }

            if (backItemDescription.length > descrMaxLength * 3) {
                backItemDescription = backItemDescription.substring(0, (descrMaxLength * 3) - 4);
                while (!backItemDescription.endsWith(' ')) backItemDescription = backItemDescription.substring(0, backItemDescription.length - 1);
                backItemDescription += ' ...';
            }
        }

        return (
            <>
                {item.outofstockbehavior !== REMOVE_ITEM &&
                    <Grid
                        item
                        xs={12}
                        sm={12}
                        md={6}
                        lg={4}
                        xl={3}
                        spacing={2}
                        container
                        onClick={this.props.onClick.bind(this, this.props.item)}
                        className={classes.cardContainer}
                    >
                        <CardFlip>
                            <CardFlipFrontSide className={classes.container}>
                                <div className={classes.flipContainer}>
                                    <img className={classes.image} src={item.ImageURL} />
                                </div>
                                <Grid
                                    item
                                    xs={12}
                                    className={classes.info}
                                >
                                    <Typography className={classes.itemID} variant="h6">
                                        {item.Name}
                                    </Typography>
                                    <div>{ReactHtmlParser(itemDescription)}</div>
                                </Grid>
                            </CardFlipFrontSide>
                            <CardFlipBackSide className={classes.container}>
                                <div className={classes.flipContainer}>
                                    {item.Name}
                                </div>
                                <Grid
                                    item
                                    xs={12}
                                    className={classes.info}
                                >
                                    <div>{ReactHtmlParser(backItemDescription)}</div>
                                </Grid>
                            </CardFlipBackSide>
                        </CardFlip>
                    </Grid>
                }
            </>
        );
    }
}
            
            
LabSuppliesCard.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  item: PropTypes.shape({
    Name: PropTypes.string,
    ImageURL: PropTypes.string,
    outofstockbehavior: PropTypes.string,
    Price: PropTypes.string,
  }),
};

export default LabSuppliesCard;
