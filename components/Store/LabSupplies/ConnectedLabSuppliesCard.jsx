import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';
import LabSuppliesCard from './LabSuppliesCard';
import { bindActionCreators } from 'redux';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventorySelectors, userSelectors } from 'appRedux/selectors';
import { changeImage } from '../utils';


const styles = theme => ({
  cardContainer: {
    display: 'flex',
    flexDirection: 'column',
    padding: 10,
  },
  flipContainer: {
    width: 275,
    height: 250,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  container: {
    minHeight: 500,
    display: 'flex',
    flexDirection: 'column',
    cursor: 'pointer',
    alignItems: 'center',
    padding: 10,
    border: '1px solid rgb(214, 167, 117)',
  },
  image: {
    width: 250,
    height: 250,
  },
  itemID: {
    color: '#a0a0a0',
    textAlign: 'center',
    marginTop: 20,
  },
  card: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    height: '100%',
    border: 'solid 1px',
    borderColor: '#CCCCCC',
    minHeight: 230,
    cursor: 'pointer',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  cardHover: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  info: {
    textAlign: 'center',
  },
  quantity: {
    width: 50,
  },
  hide: {
    display: 'none',
  },
});

const mapStateToProps = (state, props) => ({
  item: changeImage(props, 'static/images/categories/Category-wild.jpg'),
  user: userSelectors.userSelector(state),
  inventory: inventorySelectors.inventorySelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators(cartActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(LabSuppliesCard));
