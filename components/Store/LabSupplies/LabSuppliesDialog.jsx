import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import Router from 'next/router';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import {DISALLOW_BACK_ORDERS} from "../constants";
import { cartActions } from 'appRedux/actions/cartActions';
import ReactGA from 'react-ga';
import ReactHtmlParser from 'react-html-parser'; 
import axios from 'axios';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import WLHelper from 'lib/WLHelper';

const customFormValidation = Yup.object().shape({
    size: Yup.string()
        .required('Required'),
    quantity: Yup.string()
        .required('Required'),
});

class LabSuppliesDialog extends Component {

    constructor(props) {
        super(props);
        this.state = {
            quantity: '1',
            errors: {},
        };

        this.item = this.props.item;

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/store-labsupplies-' + this.item.Name.replace(/\s/g, '-'));
    }
    handleErrors = (field, err) => {
        let { errors } = this.state;
        errors[field] = err
        this.setState({ errors })
    }
    checkQuantity = (item) => {

        var quantity = parseFloat(item.OrderDetailQty);

        if (isNaN(quantity) || quantity <= 0) {
            this.handleErrors('quantity', 'Please enter a valid value for the quantity')
            return false;
        }

        //  Must be in increments of 1
        else if ((parseFloat(quantity) / parseInt(quantity) != 1.0)) {
            return false;
        }

        return true;
    }

    addToCart = (values) => {

        var quantity = this.state.quantity;
        var item = this.item;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(item.Name);
        cartItem.MerchandiseID = item.volID[0];
        cartItem.salesCategory = parseInt(item.salesCategory);
        cartItem.type = 3;
        cartItem.details = '';
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.imageUrl = (item.imageUrl ? item.imageUrl : 'static/images/categories/Category-wild.jpg');

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    relItemArray[k].cartItemType = WLHelper.getYMO2TypeForItem(relatedItems[i].related[j], relItemArray[k]);
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    })
                    .catch(error => {
                        console.log('error', error);

                        // Still add the item to the cart
                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    });
            }
        }
    }

    changeQuantity = (event) => {
        this.setState({ quantity: event.target.value })
    }

    handleClick = (partNum) => {
        partNum = encodeURIComponent(partNum);
        Router.push(`/labparam?item=${partNum}`)
        this.props.setPageData(this.props.stateData);
    }

    render() {
        const { partNum } = this.props.item;
        const { classes, theme } = this.props;
        const { errors } = this.state;

        var itemDescription = '<div style="max-height:480px; overflow-y: auto; overflow-x: hidden;">' + this.item.Description + '</div>';
        itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

        return (
            <React.Fragment>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <DialogTitle id='form-dialog-title' onClick={() => this.handleClick(partNum)} className={classes.hoverBold}>
                    {this.item.Name}
                    <div>{ReactHtmlParser(itemDescription)}</div>
                    {this.item.outofstockmessage &&
                        <Typography style={{ paddingTop: '10px', fontWeight: 'bold' }}>
                            {this.item.outofstockmessage}
                        </Typography>
                    }
                </DialogTitle>
                {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                    <Formik
                        initialValues={this.state}
                        validationSchema={customFormValidation}
                        onSubmit={values => this.addToCart(values)}
                    >
                        {({ values, handleChange }) => {
                            return (
                                <Form className={classes.form}>
                                    <DialogContent>
                                        {errors.quantity && <div className='error' >* {errors.quantity}</div>}
                                        <Grid
                                            item
                                            xs
                                            container
                                            spacing={6}
                                            style={{ marginTop: 5 }}
                                            direction={'row'}
                                        >
                                            <Grid
                                                item
                                                xs
                                                container
                                                spacing={6}
                                                direction={'row'}
                                                justify='flex-start'
                                            >
                                                <Grid item>
                                                    <TextField
                                                        id='quantity'
                                                        label='Quantity'
                                                        className={classes.quantity}
                                                        value={this.state.quantity}
                                                        onChange={this.changeQuantity}
                                                        type='number'
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                    </DialogContent>
                                    {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&

                                        <DialogActions>
                                            <Button
                                                // type='submit'
                                                onClick={this.addToCart}
                                                color='primary'
                                            // onChange={this.changeQuantity}
                                            >
                                                Add to Cart
                                        </Button>
                                        </DialogActions>
                                    }
                                </Form>
                            )
                        }}
                    </Formik>
                }
            </React.Fragment>
        );
    }
}

const styles = theme => ({
    card: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        height: '100%',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        alignItems: 'center',
        padding: 5,
        backgroundColor: '#e4e4e4',
        textAlign: 'center'
    },
    quantity: {
        width: 50
    },
    hide: {
        display: 'none'
    },
    hoverBold: {
        '&:hover': {
            fontWeight: 'bolder',
            color: '#ff9933',
            cursor: 'pointer'
        }
    },
    form: {
        width: '100%',
    }
});

LabSuppliesDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        inventory: state.inventory
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions, ...inventoryActions }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles, { withTheme: true })(LabSuppliesDialog));
