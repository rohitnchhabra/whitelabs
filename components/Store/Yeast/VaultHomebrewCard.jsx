import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import { cartActions } from 'appRedux/actions/cartActions';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import { availabilitySelectors, cartSelectors, inventorySelectors, userSelectors } from 'appRedux/selectors/cart';
import { CardFlipBackSide, CardFlipFrontSide, CartFlip } from '../CardFlip';


const YeastElements = {
  "2": {
    img: 'static/images/categories/Category-core.jpg',
    color: '#FFF',
  },
  "3": {  // Ale
    img: 'static/images/categories/Category-ale.jpg',
    icon: 'static/images/icons/Ale-icon.svg',
    color: "#FF9933",
  },
  "4": {  // Wild Yeast
    img: 'static/images/categories/Category-wild.jpg',
    icon: 'static/images/icons/wildyeast-icon.svg',
    color: "#CC9966",
  },
  "5": {  // Lager
    img: 'static/images/categories/Category-lager.jpg',
    icon: 'static/images/icons/Lager-icon.svg',
    color: "#FFCC33",
  },
  "6": {  // Wine
    img: 'static/images/categories/Category-wine.jpg',
    icon: 'static/images/icons/wine-icon.svg',
    color: "#9966CC",
  },
  "7": {  // Distilling
    img: 'static/images/categories/Category-Distilling.jpg',
    icon: 'static/images/icons/Distilling-icon.svg',
    color: "#6666CC",
  },
  "8": {  // Belgian
    img: 'static/images/categories/Category-belgian.jpg',
    icon: 'static/images/icons/Belgian-icon.svg',
    color: "#66CCCC",
  },
  "32": { // Vault
    img: 'static/images/categories/Category-vault.jpg',
    icon: 'static/images/icons/vault-icon.svg',
    color: "#B3B3B3",
  },
};

function getImage(salesCategory) {
  try {
    return YeastElements[parseInt(salesCategory)].img;
  } catch (error) {

  }
}

function getIcon(salesCategory) {
  try {
    return YeastElements[parseInt(salesCategory)].icon;
  } catch (error) {

  }
}

function getColor(salesCategory) {
  try {
    return YeastElements[parseInt(salesCategory)].color;
  } catch (error) {

    throw error;
  }
}

class VaultHomebrewCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: "1",
      img: null,
      icon: null,
      color: null,
      anchorEl: null,
    };
    this.item = this.props.item;
  }

  checkQuantity = (cartItem) => {

    var quantity = parseFloat(cartItem.OrderDetailQty);

    if (isNaN(quantity) || quantity <= 0) {

      return false;
    }

    //  Must be in increments of 1
    else if ((parseFloat(quantity) / parseInt(quantity) != 1.0)) {
      return false;
    }

    return true;
  };

  changeQuantity = (event) => {
    this.setState({ quantity: event.target.value });
  };

  addToCart = (event) => {
    try {
      var quantity = this.state.quantity;
      var item = this.item;

      // Create cart item
      var cartItem = {};
      cartItem.Name = String(item.Name);
      cartItem.salesCategory = parseInt(item.salesCategory);
      cartItem.dispQuantity = parseInt(quantity);
      cartItem.OrderDetailQty = parseFloat(quantity);

      cartItem.MerchandiseID = item.volID[4];
      cartItem.type = 2;
      cartItem.details = "Vault homebrew packs";

      cartItem.mfgEnvironment = this.item.mfgEnvironment;
      cartItem.isVaultPreorderItem = true;

      this.props.addToCart({ cartItem });
    } catch (error) {

    }
    this.setState({
      anchorEl: event.currentTarget,
    });
    setTimeout(() => {
      this.setState({ anchorEl: null });
    }, 500);
  };

  render() {
    const { classes, theme, item, PaperProps } = this.props;

    const spaceIndex = item.Name.indexOf(" ");
    const itemID = item.Name.substr(0, spaceIndex);
    const itemName = item.Name.substr(spaceIndex + 1);

    const {
      anchorEl,
    } = this.state;
    const open = Boolean(anchorEl);

    return (
      <React.Fragment>
        <LoadingIndicator visible={this.props.isLoading} label={'Please Wait'}/>
        <Grid
          item
          xs={12}
          sm={6}
          md={4}
          lg={3}
          spacing={6}
          onMouseEnter={this.handleItemHoverEnter}
          onMouseLeave={this.handleItemHoverLeave}
        >
          <CartFlip>
            <CardFlipFrontSide>
              <Grid item container spacing={2}>
                <Grid
                  item
                  xs={12}
                  className={classes.info}
                >
                  <img
                    src={getIcon(this.props.item.salesCategory)}
                    height="40"
                  />
                  <Typography variant="h5" color="secondary">
                    {itemID}
                  </Typography>
                  <Typography
                    variant="subheading"
                    color="secondary"
                  >
                    {itemName}
                  </Typography>
                </Grid>
              </Grid>
            </CardFlipFrontSide>
            <CardFlipBackSide>
              <Grid item container direction={'column'} spacing={2}>
                <Grid item xsonClick={this.handleClickOrTouch} onTouch={this.handleClickOrTouch}>
                  <Typography
                    className={classes.info}
                    variant="subtitle1"
                    style={{ color: getColor(this.props.item.salesCategory) }}
                  >
                    {itemID}
                  </Typography>
                </Grid>
                <Grid item xs={12} onClick={this.handleClickOrTouch} onTouch={this.handleClickOrTouch}>
                  <div
                    style={{
                      backgroundColor: getColor(this.props.item.salesCategory),
                      padding: 1,
                      textAlign: 'center',
                      marginLeft: theme.spacing(-2),
                      marginRight: theme.spacing(-2),
                    }}
                  >
                    <Typography
                      variant="subtitle1"
                      color="secondary"
                    >
                      {itemName}
                    </Typography>
                  </div>
                </Grid>
                <Grid
                  item
                  xs
                  container
                  spacing={2}
                  className={classes.infoItem}
                >
                  {
                    (item.optFermentTempF || item.optFermentTempC) && (item.optFermentTempF != 'F - F' || item.optFermentTempC != 'C - C') &&
                    <Grid item xs={6}>
                      <div className={classes.infoSmall}>
                        <Typography style={{ fontSize: 'x-small' }}>
                          Fermentation Temp
                        </Typography>
                        <Typography style={{ color: this.state.color, fontSize: 'x-small' }}>
                          {(item.optFermentTempF + (item.optFermentTempF && item.optFermentTempC ? '/' : '') + item.optFermentTempC).replace(/\s/g, '')}
                        </Typography>
                      </div>
                    </Grid>
                  }
                  {
                    item.flocculation &&
                    <Grid item xs={6}>
                      <div className={classes.infoSmall}>
                        <Typography style={{ fontSize: 'x-small' }}>Flocculation</Typography>
                        <Typography style={{ color: this.state.color, fontSize: 'x-small' }}>
                          {item.flocculation}
                        </Typography>
                      </div>
                    </Grid>
                  }
                  {
                    item.alcoholTol &&
                    <Grid item xs={6}>
                      <div className={classes.infoSmall}>
                        <Typography style={{ fontSize: 'x-small' }}>Alcohol Tol.</Typography>
                        <Typography style={{ color: this.state.color, fontSize: 'x-small' }}>
                          {item.alcoholTol}
                        </Typography>
                      </div>
                    </Grid>
                  }
                  {
                    item.attenuation && item.attenuation != ' - ' && item.attenuation != '- ' &&
                    <Grid item xs={6}>
                      <div className={classes.infoSmall}>
                        <Typography style={{ fontSize: 'x-small' }}>Attenuation</Typography>
                        <Typography style={{ color: this.state.color, fontSize: 'x-small' }}>
                          {item.attenuation}
                        </Typography>
                      </div>
                    </Grid>
                  }
                  <Grid item>
                    <div style={{ whiteSpace: 'nowrap' }}>
                      <TextField
                        id="quantity"
                        label="Quantity"
                        className={classes.quantity}
                        value={this.state.quantity}
                        onChange={this.changeQuantity}
                        type="number"
                        style={{ maxWidth: '25%' }}
                      />
                      <div className={classes.buttons} style={{ float: 'right' }}>
                        <Button
                          variant="contained"
                          color="primary"
                          onClick={this.addToCart}
                          onTouch={this.addToCart}
                          className={classes.button}
                          aria-owns={open ? 'simple-popper' : undefined}
                          aria-haspopup="true"
                          aria-owns={open ? 'simple-popper' : undefined}
                          aria-haspopup="true"
                        >
                          Add to Cart
                        </Button>
                      </div>
                    </div>
                  </Grid>

                  <Popover
                    id="simple-popper"
                    open={open}
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'top',
                      horizontal: 'center',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'center',
                    }}
                  >
                    <Typography className={classes.popup}>
                      Added To Cart
                    </Typography>
                  </Popover>

                </Grid>
              </Grid>
            </CardFlipBackSide>
          </CartFlip>
          <div className={classes.vaultInfoContainer}>
            <div className={classes.vaultInfo}>
              <img className={classes.image} src='/static/images/vault_graphic.png'/>
              <Typography variant="caption"
                          color="primary">{item.preorderedTotal} OF {item.preorderTriggerQty}</Typography>
              <Typography variant="caption">Preordered</Typography>
            </div>
          </div>
        </Grid>
      </React.Fragment>
    );
  }
}

VaultHomebrewCard.propTypes = {
  classes: PropTypes.object.isurld,
  theme: PropTypes.object.isurld,
};

const styles = theme => ({
  vaultInfoContainer: {
    backgroundColor: '#FFF',
    padding: 5,
  },
  card: {
    border: "solid 1px",
    borderColor: "#CCCCCC",
    padding: theme.spacing(2),
    height: 230,
    minWidth: '340px',
    cursor: "pointer",
    [theme.breakpoints.down('lg')]: {
      minWidth: '250px',
      height: '280px',
    },
    [theme.breakpoints.down('md')]: {
      minWidth: '220px',
      height: '280px',
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  infoItem: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  cardHover: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  info: {
    textAlign: "center",
  },
  infoSmall: {
    textAlign: "center",
    fontSize: "x-small",
  },
  name: {
    padding: 3,
    marginLeft: theme.spacing(-2),
    marginRight: theme.spacing(-2),
    textAlign: "center",
  },
  popup: {
    margin: theme.spacing(4),
    fontSize: 22,
    fontWeight: 'bolder',
  },
  paper: {
    border: "1px solid black",
    backgroundColor: 'blue',
  },
  vaultInfo: {
    display: 'flex',
    alignItems: "center",
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
});

const mapStateToProps = state => ({
  user: userSelectors.userSelector(state),
  inventory: inventorySelectors.inventorySelector(state),
  isLoading: cartSelectors.isLoadingSelector(state),
  availability: availabilitySelectors.availabilitySelector(state),
  availabilityCPH: cartSelectors.availabilityCPHSelector(state),
  availabilityHK: cartSelectors.availabilityHKSelector(state),
  availabilitySD: cartSelectors.availabilitySDSelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(VaultHomebrewCard));
