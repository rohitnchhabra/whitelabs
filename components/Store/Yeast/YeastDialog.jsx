import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Router from 'next/router';
import Link from 'next/link';
import axios from 'axios';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DialogContent from '@material-ui/core/DialogContent';
import { Formik, Form } from 'formik';
import * as Yup from 'yup';

import LoadingIndicator from 'components/UI/LoadingIndicator';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import { parseAvailabilityResults } from 'lib/InventoryUtils';
import { IN_STOCK, OUT_OF_STOCK } from 'lib/Constants';
import { DISALLOW_BACK_ORDERS } from "../constants";
import WLHelper from 'lib/WLHelper';
import ReactGA from 'react-ga';
import ReactHtmlParser from 'react-html-parser';

const YeastElements = {
    '2': {
        img: 'static/images/categories/Category-core.jpg',
        color: '#FFF'
    },
    '3': {  // Ale
        img: 'static/images/categories/Category-ale.jpg',
        icon: 'static/images/icons/Ale-icon.svg',
        color: '#FF9933'
    },
    '4': {  // Wild Yeast
        img: 'static/images/categories/Category-wild.jpg',
        icon: 'static/images/icons/wildyeast-icon.svg',
        color: '#CC9966'
    },
    '5': {  // Lager
        img: 'static/images/categories/Category-lager.jpg',
        icon: 'static/images/icons/Lager-icon.svg',
        color: '#FFCC33'
    },
    '6': {  // Wine
        img: 'static/images/categories/Category-wine.jpg',
        icon: 'static/images/icons/wine-icon.svg',
        color: '#9966CC'
    },
    '7': {  // Distilling
        img: 'static/images/categories/Category-Distilling.jpg',
        icon: 'static/images/icons/Distilling-icon.svg',
        color: '#6666CC'
    },
    '8': {  // Belgian
        img: 'static/images/categories/Category-belgian.jpg',
        icon: 'static/images/icons/Belgian-icon.svg',
        color: '#66CCCC'
    },
    '32': { // Vault
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/vault-icon.svg',
        color: '#B3B3B3'
    },
    '997': { // Vault without Pure Pitch stock but with work orders
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/Yellow_Lock_v01.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    },
    '998': { // Organic
        img: 'static/images/categories/Category-organic.jpg',
        icon: 'static/images/icons/organic-icon.svg',
        color: '#B3B3B3',
        hbTitle: 'Organic'
    },
    '999': { // Vault with Pure Pitch stock
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/Green_Lock_v01.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    }
}

function getIcon(item, salesCategory) {
    try {
        var icons = [];
        if (item.isOrganic && salesCategory !== 998) {
            icons.push(YeastElements[998].icon);
        }
        icons.push(YeastElements[parseInt(salesCategory)].icon);
        return icons;
    }
    catch (err) {
        console.log('error', salesCategory, err);
    }
}

function getColor(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].color;
    }
    catch (error) {
        throw error;
    }
}

const FormikErrorMessage = ({ error, classes }) => {
    return error ? <div className={classes.errorMsg + ' error'}>{error}</div> : null;
};

const customFormValidation = Yup.object().shape({
    packaging: Yup.string().required('Required'),
    pack: Yup.string().nullable(),
    quantity: Yup.string().required('Required'),
});

class YeastDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: '1',
            packOptions: [
                { label: 'Nano', value: '0' },
                { label: '1.5L', value: '1' },
                { label: '1.5L', value: '-1' },
                { label: '2L', value: '2' }
            ],
            pack: '0',
            packagingOptions: [],
            packaging: 'pp',
            availability: null,
            availabilitySD: null,
            availabilityCPH: null,
            availabilityHK: null,
            isLoading: false,
            errors: {},
            salesCategoryIconOverride: (this.props.item.salesCategoryIconOverride ? this.props.item.salesCategoryIconOverride : this.props.item.salesCategory),
            notifyEmail: null,
            errorNotifyEmailMsg: null,
            confirmNotifyEmailMsg: null
        };

        this.item = this.props.item;

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/store-yeast-' + this.item.Name.replace(/\s/g, '-'));
    }

    componentDidMount() {
        if (this.item.volID[6]) {
            this.setState({ packaging: '6', pack: null });
        } else if (this.item.volID[0] && this.item.volID[2] && !this.noPurePitchForVaultItem()) {
            if (this.item.purePitch) {
                this.setState({ packaging: 'pp', pack: '0' });
            } else {
                this.setState({ packaging: 'nl', pack: '0' });
            }
        } else if (this.item.volID[0] && !this.noPurePitchForVaultItem()) {
            this.setState({ packaging: '0', pack: null });
        } else {
            this.setState({ packaging: '3', pack: null });
        }

        this.filterPackageTypes();
    }

    filterPackageTypes() {
        try {
            var PackageTypes = [
                { label: '1L Nalgene Bottle', value: '6' },
                { label: 'PurePitch', value: 'pp' },
                { label: 'Nalgene Bottle', value: 'nl' },
                { label: ((this.item.strainCategory == 32 || this.item.strainCategory == 33) && !this.item.volID[6] ? '1L Nalgene Bottle' : 'Custom Pour'), value: '3' },
                { label: 'Homebrew', value: '4' },
                { label: 'Slant', value: '5' },
                { label: 'Yeast', value: '0' }
            ];

            var subsidiary = (this.props.user.subsidiary ? this.props.user.subsidiary : 2),
                FilteredPackageTypes = [];

            if (this.item.volID[6]) {
                FilteredPackageTypes.push(PackageTypes[0]);
            } else if (this.item.volID[0] && this.item.volID[2]) {
                if (this.item.purePitch) {
                    if (!this.noPurePitchForVaultItem()) {
                        FilteredPackageTypes.push(PackageTypes[1]);
                    }
                } else {
                    FilteredPackageTypes.push(PackageTypes[2]);
                }
            } else if (this.item.volID[0]) {
                FilteredPackageTypes.push(PackageTypes[6]);
            }

            if (this.item.volID[3]) {
                FilteredPackageTypes.push(PackageTypes[3]);
            }

            if (this.item.volID[4] && subsidiary == 2 && (this.item.hbAvailQty > 0 || this.item.isVaultPreorderItem)) {
                FilteredPackageTypes.push(PackageTypes[4]);
            }

            if (this.item.volID[5]) {
                FilteredPackageTypes.push(PackageTypes[5]);
            }

            this.setState({ packagingOptions: FilteredPackageTypes });
            if (FilteredPackageTypes.length == 1) {
                this.setState({ packaging: FilteredPackageTypes[0].value });
            }
        } catch (error) {
            return error;
        }
    }

    noPurePitchForVaultItem() {
        var noPurePitch = true;

        if (this.item.isVaultItem && this.item.availQty) {
            // Make sure there's a PurePitch size in stock
            for (var i = 0; i < 3; i++) {
                if (this.item.availQty[i] > 0) {
                    noPurePitch = false;
                    break;
                }
            }
        } else {
            noPurePitch = false;
        }

        // Check for PP work orders
        if (this.item.vaultWorkOrders) {
            for (var i = 0; i < this.item.vaultWorkOrders.length; i++) {
                if (
                    this.item.vaultWorkOrders[i].ItemId == this.item.volID[0]
                    || this.item.vaultWorkOrders[i].ItemId == this.item.volID[1]
                    || this.item.vaultWorkOrders[i].ItemId == this.item.volID[2]
                ) {
                    noPurePitch = false;

                    // See if we need to set the available quantity to make the PP size orderable
                    if (this.item.availQty) {
                        if (this.item.vaultWorkOrders[i].ItemId == this.item.volID[0]) {
                            if (!this.item.availQty[0]) this.item.availQty[0] = parseInt(this.item.vaultWorkOrders[i].Qty);
                        } else if (this.item.vaultWorkOrders[i].ItemId == this.item.volID[1]) {
                            if (!this.item.availQty[1]) this.item.availQty[1] = parseInt(this.item.vaultWorkOrders[i].Qty);
                        } else if (this.item.vaultWorkOrders[i].ItemId == this.item.volID[2]) {
                            if (!this.item.availQty[2]) this.item.availQty[2] = parseInt(this.item.vaultWorkOrders[i].Qty);
                        }
                    }
                }
            }
        }

        // No Pure Pitch available
        return noPurePitch;
    }

    handleErrors = (field, err) => {
        let { errors } = this.state;
        errors[field] = err
        this.setState({ errors })
    }

    checkQuantity = item => {
        try {
            var quantity = parseFloat(item.OrderDetailQty);

            if (isNaN(quantity) || quantity <= 0) {
                // TO-DO: Display message to user
                this.handleErrors('quantity', 'Please enter a valid value for the quantity')

                return false;
            }

            // Wild Yeast must have mimimum 1L

            if (item.salesCategory == 4 && quantity < 1.0) {
                this.handleErrors('quantity', 'Notice: The minimum quantity sold for Wild Yeast strains is 1L. Please adjust your quantity.')
                return false;
            }
            // Custom Pour Strains
            if (item.type == 5) {
                // Vault strains must have minimum 1.5L Custom Pour
                if ((item.salesCategory == 32 || item.isVaultItem) && !(this.item.strainCategory == 32 || this.item.strainCategory == 33)) {
                    if (quantity < 1.5) {
                        // TO-DO: Display message to user
                        this.handleErrors('quantity', 'Notice: The minimum quantity sold for Custom Pour Vault strains is 1.5L. Please adjust your quantity.')
                        return false;
                    } else if ((parseFloat(quantity) / parseInt(quantity) != 1.0) && (parseFloat(quantity + 0.5) / parseInt(quantity + 0.5) != 1.0)) {
                        this.handleErrors('quantity', 'Notice: Custom Pour Vault strains are sold in 0.5L increments. Please adjust your quantity.')
                        return false;
                    }
                }

                // Bacteria sold in 1L increments
                if (item.salesCategory == 4 || this.item.strainCategory == 32 || this.item.strainCategory == 33) {
                    if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
                        quantity = Math.round(quantity);

                        // TO-DO: Display message to user
                        this.handleErrors('quantity', 'Notice: Quantities for this strain must be in 1L increments, your value has been rounded accordingly. Please review your cart.')
                    }
                }

                // All other custom pour strains sold in 0.5L increments
                else {
                    if ((parseFloat(quantity) / parseInt(quantity) != 1.0) && (parseFloat(quantity + 0.5) / parseInt(quantity + 0.5) != 1.0)) {
                        this.handleErrors('quantity', 'Notice: Custom Pours are sold in 0.5L increments. Please adjust your quantity.')
                        return false;
                    }
                }

                item.size = quantity;
                item.details = quantity + ((this.item.strainCategory == 32 || this.item.strainCategory == 33) && !this.item.volID[6] ? ' 1L Nalgene Bottle(s)' : 'L Custom Pour');
                item.OrderDetailQty = parseFloat(quantity);
            } else if (item.minQty && item.minQty > quantity) {
                this.handleErrors('quantity', 'Notice: Minimum quantity of this strain/size is ' + item.minQty + '. Please adjust your quantity.')
                return false;
            }
            // Non-custom pour strains must be in increments of 1
            else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
                this.handleErrors('quantity', 'Quantity Error !!')
                return false;
            }

            return true;
        } catch (error) {
            this.handleErrors('quantity', `Could not check quantity ${error}`)
        }
    };

    addToCart = (values) => {
        const packaging = this.state.packaging;
        const pack = this.state.pack;
        const quantity = this.state.quantity;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(this.item.Name);
        cartItem.salesCategory = parseInt(this.item.salesCategory);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.mfgEnvironment = this.item.mfgEnvironment;
        cartItem.isVaultPreorderItem = this.item.isVaultPreorderItem;
        cartItem.isVaultItem = this.item.isVaultItem;
        cartItem.imageUrl = this.item.imageUrl;
        cartItem.isOrganic = this.item.isOrganic;

        // PurePitch / 1L Nalgene bottle
        if (packaging == 'pp' || packaging == 'nl') {
            switch (pack) {
                // Nano
                case '0':
                    cartItem.MerchandiseID = this.item.volID[0];
                    cartItem.details = 'Nano';
                    //Per Mike, since we are checking Vault availability now, we don't need to set 3-item the minimum
                    //if (cartItem.isVaultItem) cartItem.minQty = 3;
                    break;

                // 1.5L
                case '1':
                    cartItem.MerchandiseID = this.item.volID[1];
                    cartItem.details = '1.5L';
                    break;

                // 2L
                case '2':
                    cartItem.MerchandiseID = this.item.volID[2];
                    cartItem.details = '2L';
                    break;
                default:
                    this.handleErrors('pack', `cannot add to cart, ${item}, ${packaging}, ${pack}, ${quantity}`)
                    return;
            }

            if (this.item.purePitch) {
                cartItem.details = 'PurePitch® ' + cartItem.details;
            }

            cartItem.type = 1;
        } else {
            switch (packaging) {
                // Yeast
                case '0':
                    cartItem.MerchandiseID = this.item.volID[0];
                    // Per Mike, treat as PurePitch reather than as Non-Yeast
                    cartItem.type = 1; //Was 3;
                    cartItem.details = 'Yeast';
                    break;

                // Custom Pour
                case '3':
                    cartItem.MerchandiseID = this.item.volID[3];
                    cartItem.type = 5;
                    cartItem.dispQuantity = 1;
                    cartItem.size = parseFloat(quantity);
                    cartItem.details = quantity + ((this.item.strainCategory == 32 || this.item.strainCategory == 33) && !this.item.volID[6] ? ' 1L Nalgene Bottle(s)' : 'L Custom Pour');
                    cartItem.relatives = [];
                    var multipliers = [0.5, 1.5, 2];

                    for (var i = 0; i < 3; i++) {
                        if (this.item.volID[i]) {
                            var relative = {};
                            relative.id = parseInt(this.item.volID[i]);
                            if (isNaN(relative.id)) {
                                throw { message: 'Invalid VolID Index! in Relatives', code: 0 };
                            }
                            relative.mult = multipliers[i];
                            cartItem.relatives.push(relative);
                        }
                    }
                    break;

                // Homebrew
                case '4':
                    cartItem.MerchandiseID = this.item.volID[4];
                    cartItem.type = 2;
                    cartItem.details = 'Homebrew packs';
                    break;

                // Slant
                case '5':
                    cartItem.MerchandiseID = this.item.volID[5];
                    // Per Mike, treat as PurePitch reather than as Non-Yeast
                    cartItem.type = 1; //Was 3;
                    cartItem.details = 'Slants';
                    break;

                // 1L Nalgene Bottle
                case '6':
                    cartItem.MerchandiseID = this.item.volID[6];
                    cartItem.type = 1;
                    cartItem.details = '1L Nalgene Bottle';
                    break;
            }
        }

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    relItemArray[k].cartItemType = WLHelper.getYMO2TypeForItem(relatedItems[i].related[j], relItemArray[k]);
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    })
                    .catch(error => {
                        console.log('error', error);
                        this.setState({ isLoading: false });
                        // Still add the item to the cart
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    });
            }
        }
    };

    getQcResults = (values) => {
        const packaging = this.state.packaging;
        const pack = this.state.pack;
        const quantity = this.state.quantity;

        var merchandiseId = 0;
        var packagingId = 0;

        // PurePitch / 1L Nalgene bottle
        if (packaging == 'pp' || packaging == 'nl') {
            packagingId = pack;
            switch (pack) {
                // Nano
                case '0':
                    merchandiseId = this.item.volID[0];
                    break;

                // 1.5L
                case '1':
                    merchandiseId = this.item.volID[1];
                    break;

                // 2L
                case '2':
                    merchandiseId = this.item.volID[2];
                    break;
                default:
                    this.handleErrors('pack', `cannot add to cart, ${item}, ${packaging}, ${pack}, ${quantity}`)
                    return;
            }
        } else {
            packagingId = packaging;
            switch (packaging) {
                // Yeast
                case '0':
                    merchandiseId = this.item.volID[0];
                    break;

                // Custom Pour
                case '3':
                    merchandiseId = this.item.volID[3];
                    break;

                // Homebrew
                case '4':
                    merchandiseId = this.item.volID[4];
                    break;

                // Slant
                case '5':
                    merchandiseId = this.item.volID[5];
                    break;

                // 1L Nalgene Bottle
                case '6':
                    merchandiseId = this.item.volID[6];
                    break;
            }
        }

        this.setState({ isLoading: true });
        axios.post('/qc-results', { itemId: new String(merchandiseId), lotNumber: quantity, packagingId: packagingId })
            .then(({ data: { qcResults, error } }) => {
                if (error) throw error;
                console.log(qcResults);
                this.setState({ isLoading: false });
            })
            .catch(error => {
                console.log('error', error);
                this.setState({ isLoading: false });
            })
            .finally(() => {
                this.setState({ isLoading: false });
            });
    };

    checkAvailability = () => {
        ReactGA.pageview('/store-yeast-availability-' + this.item.Name.replace(/\s/g, '-'));
        var { subsidiary } = this.props.user;
        const packaging = this.state.packaging;
        const pack = this.state.pack;
        let itemID;

        if (packaging == 'pp' || packaging == 'nl') {
            switch (pack) {
                // Nano
                case '0':
                    itemID = this.item.volID[0]
                    break;

                // 1.5L
                case '1':
                    itemID = this.item.volID[1];
                    break;

                // 2L
                case '2':
                    itemID = this.item.volID[2];
                    break;

                default:
                    return;
            }
        } else {
            switch (packaging) {
                // Homebrew
                case '4':
                    itemID = this.item.volID[4];
                    break;

                // Slant
                case '5':
                    itemID = this.item.volID[5];
                    break;

                // 1L Nalgene Bottle
                case '6':
                    itemID = this.item.volID[6];
                    break;

                // Distilling
                case '0':
                    itemID = this.item.volID[0]
                    break;

                // Vault
                case '3':
                    itemID = this.item.volID[3]
                    break;

                default:
                    return;

            }
        }

        const quantity = this.state.quantity;

        this.setState({ isLoading: true });
        if (!subsidiary || subsidiary === '') {
            var subs = [2, 5, 7];
            this.setState({ availability: null });
            this.setState({ availabilitySD: null });
            this.setState({ availabilityCPH: null });
            this.setState({ availabilityHK: null });

            // Hacky "switch" statement to handle indeterminate async call duration
            // cross-contaminating result variables. Also, separate availabilities to make
            // sure they are always displayed in the same order in the results.
            for (var i = 0; i < subs.length; i++) {
                this.setState({ isLoading: true });
                switch (i) {
                    case 0:
                        var s = subs[i];
                        subsidiary = s;
                        axios.post('/item-availability', { itemID, subsidiary, quantity })
                            .then(({ data: { availability, error, availabilityMessage} }) => {
                                if (error) throw error;
                                if (availability) this.setState({ availabilitySD: parseAvailabilityResults(availability, s, quantity, availabilityMessage) });

                                if (this.item && this.item.purePitch && this.item.isVaultItem) {
                                    if (this.state.availabilitySD && this.state.availabilitySD.indexOf(IN_STOCK) >= 0) {
                                        this.item.salesCategoryIconOverride = 999;
                                        this.setState({ salesCategoryIconOverride: 999 });
                                    }
                                    this.item.salesCategoryIconOverrideChecked = true;
                                }
                            })
                            .catch(error => {
                                console.log('error', error);
                            })
                            .finally(() => this.setState({ isLoading: false }))
                        break;
                    case 1:
                        var s1 = subs[i];
                        subsidiary = s1;
                        axios.post('/item-availability', { itemID, subsidiary, quantity })
                            .then(({ data: { availability, error, availabilityMessage } }) => {
                                if (error) throw error;
                                if (availability) this.setState({ availabilityHK: parseAvailabilityResults(availability, s1, quantity, availabilityMessage) });
                            })
                            .catch(error => {
                                console.log('error', error);
                            })
                            .finally(() => this.setState({ isLoading: false }))
                        break;
                    case 2:
                        var s2 = subs[i];
                        subsidiary = s2;
                        axios.post('/item-availability', { itemID, subsidiary, quantity })
                            .then(({ data: { availability, error, availabilityMessage } }) => {
                                if (error) throw error;
                                if (availability) this.setState({ availabilityCPH: parseAvailabilityResults(availability, s2, quantity, availabilityMessage) });
                            })
                            .catch(error => {
                                console.log('error', error);
                            })
                            .finally(() => this.setState({ isLoading: false }))
                        break;
                }
            }
        } else {
            this.setState({ availabilitySD: null });
            this.setState({ availabilityCPH: null });
            this.setState({ availabilityHK: null });

            const useSubsidiary = (this.item.isOrganic ? 7 : subsidiary);

            axios.post('/item-availability', { itemID, subsidiary: useSubsidiary, quantity })
                .then(({ data: { availability, error, availabilityMessage } }) => {
                    if (error) throw error;
                    var availabilityText = parseAvailabilityResults(availability, useSubsidiary, quantity, availabilityMessage);
                    if (this.item.isOrganic && subsidiary == 2 && availabilityText.indexOf(IN_STOCK) >= 0)
                        availabilityText = availabilityText.replace('Copenhagen', 'Copenhagen and available for shipping to USA');

                    this.setState({ availability: availabilityText });

                    if (this.item && this.item.purePitch && this.item.isVaultItem && subsidiary == 2) {
                        if (this.state.availability && this.state.availability.indexOf(IN_STOCK) >= 0) {
                            this.item.salesCategoryIconOverride = 999;
                            this.setState({ salesCategoryIconOverride: 999 });
                        }
                        this.item.salesCategoryIconOverrideChecked = true;
                    }
                })
                .catch(error => {
                    console.log('error', error);
                })
                .finally(() => this.setState({ isLoading: false }));
        }
    }

    handleDialogClose() {
        this.props.closeDialog();
    };

    setPack = event => {
        this.setState({
            pack: event.target.value,
            availabilityCPH: null, availabilityHK: null, availabilitySD: null, availability: null
        });
    };

    setPackaging = event => {
        var packaging = event.target.value;
        var pack;

        if (packaging == 'pp' || packaging == 'nl') {
            pack = '0';
        }

        this.setState({
            packaging: event.target.value,
            pack: pack,
            availabilityCPH: null, availabilityHK: null, availabilitySD: null, availability: null
        });
    };

    changeQuantity = event => {
        this.setState({ quantity: event.target.value, availabilityCPH: null, availabilityHK: null, availabilitySD: null, availability: null });
    };

    moveToCalculator = () => {
        Router.push(`/calculator?id=${this.item.volID[0]}`);
    }

    handleClick = (partNum) => {
        partNum = encodeURIComponent(partNum);
        Router.push(`/yeastparam?item=${partNum}`)
        this.props.setPageData(this.props.stateData);
    }

    notifyEmailChange = event => {
        const email = event.target.value
        this.setState({ notifyEmail: email });
    }

    requestVaultWorkOrderNotification = () => {
        const email = (this.state.notifyEmail ? this.state.notifyEmail : this.user.email);
        var errorNotifyEmailMsg = null;
        var errorNotifyEmail = false;

        if (email) {
            let arr = email.split(',');
            var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            for (let i = 0; i < arr.length; i++) {
                if (!re.test(arr[i])) {
                    errorNotifyEmail = true;
                    errorNotifyEmailMsg = "* Please Enter Valid Email(s)";
                    break;
                }
            }

            if (!errorNotifyEmail && email.length > 300) {
                errorNotifyEmail = true;
                errorNotifyEmailMsg = "* Maximum length of field is 300 characters";
            }
        }

        if (errorNotifyEmail) {
            this.setState({ errorNotifyEmailMsg, confirmNotifyEmailMsg: null });
        } else {
            let request = {};
            request.isQuestionnaireResponse = true;
            request.source = 'Vault Work Order Notification';
            if (this.props.user) request.id = this.props.user.id;

            request.questions = [];
            request.questions.push({ question: 'Strain No', answer: this.item.partNum });
            request.questions.push({ question: 'Email Address', answer: email });

            this.setState({ isLoading: true });
                            
            axios.post('/record-survey-responses', { request })
                .then(({ data: { error } }) => {
                    if (error) throw error;
                    this.setState({ confirmNotifyEmailMsg: 'Thank you! Your request has been recorded!', errorNotifyEmailMsg: null });
                })
                .catch(error => {
                    this.setState({ errorNotifyEmailMsg: error, confirmNotifyEmailMsg: null });
                })
                .finally(() => this.setState({ isLoading: false }));
        }
    }

    render() {
        const { partNum, volID, availQty, isVaultItem } = this.props.item;
        const { classes } = this.props;
        const { errors, availability, availabilityCPH, availabilityHK, availabilitySD } = this.state;
        const spaceIndex = this.item.Name.indexOf(' ');
        const itemID = this.item.Name.substr(0, spaceIndex);
        const itemName = this.item.Name.substr(spaceIndex + 1);
        const error = errors.packaging || errors.pack || errors.quantity;
        const nextShipDateMsg = (
            availability && availability.indexOf(IN_STOCK) >= 0 ? '' :
                (availabilitySD && !availabilitySD.indexOf(IN_STOCK) >= 0)
                    || (availabilityCPH && !availabilityCPH.indexOf(IN_STOCK) >= 0)
                    || (availabilityHK && !availabilityHK.indexOf(IN_STOCK) >= 0) ? 'For out-of-stock strains, ship dates are generally less than 10 days; dates are shown in checkout.' : ''
        );

        const icons = getIcon(this.props.item, this.state.salesCategoryIconOverride ? this.state.salesCategoryIconOverride : effectiveSalesCategory);
        const showOrganicAlternative = (!this.props.user.subsidiary && this.props.item.hasOrganicVersion && !this.props.item.isOrganic);
        const showNonOrganicAlternative = ((!this.props.user.subsidiary || this.props.user.subsidiary != 7) && this.props.item.isOrganic);

        var itemDescription = '<div style="max-height:480px; overflow-y: auto; overflow-x: hidden;">' + this.item.Description + '</div>';
        itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');

        const { id, email } = this.props.user;

        return (
            <React.Fragment>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <DialogContent>
                    <div className={classes.close}>
                        <IconButton
                            color='inherit'
                            size='small'
                            aria-label='Menu'
                            onClick={() => this.handleDialogClose()}
                        >
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Grid
                        item
                        container
                        xs
                        style={{
                            display: 'flex',
                            marginTop: -10,
                            marginBottom: 20
                        }}
                        direction={'row'}
                        spacing={2}
                    >
                        <Grid item>
                            <Typography variant='h5' className={this.item.isVaultPreorderItem ? null : classes.hoverBold} onClick={this.item.isVaultPreorderItem ? null : () => this.handleClick(partNum)}>
                                {itemID} | {itemName}
                            </Typography>
                            {this.item.isVaultPreorderItem &&
                                <>
                                    <Typography variant='h6' className={this.item.isVaultPreorderItem ? null : classes.hoverBold} onClick={this.item.isVaultPreorderItem ? null : () => this.handleClick(partNum)}>
                                        VAULT PREORDER ONLY
                                    </Typography>
                                    <div style={{ backgroundColor: "#FFF", padding: 5 }}>
                                        <div className={classes.vaultInfo}>
                                            <Typography variant="caption" color="primary">{this.item.preorderedTotal} OF {this.item.preorderTriggerQty} Preordered</Typography>
                                        </div>
                                    </div>
                                </>
                            }
                        </Grid>
                    </Grid>

                    <Grid container spacing={6}>
                        <Grid item xs={2} md={1}>
                            <div
                                className={classes.circle}
                                style={{ backgroundColor: getColor(this.item.salesCategory) }}
                            >
                                {!icons || icons.length == 0 ? null :
                                    <img
                                        src={icons[0]}
                                        height='20'
                                    />
                                }
                                {
                                    icons && icons.length > 1 && <img
                                        src={icons[1]}
                                        height='20'
                                    />
                                }
                            </div>
                        </Grid>
                        <Grid
                            item
                            container
                            xs={10}
                            md={11}
                            direction={'row'}
                            spacing={2}
                        >
                            <Grid item xs={12} md={6}>
                                <div style={{ display: 'flex' }}>
                                    <Typography className='dialogVariant' >Attenuation:</Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.attenuation}
                                    </Typography>
                                </div>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <div style={{ display: 'flex' }}>
                                    <Typography className='dialogVariant'>Flocculation:</Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.flocculation}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <div style={{ display: 'flex' }}>
                                    <Typography className='dialogVariant'>Alcohol&nbsp;Tol.:</Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.alcoholTol}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <div style={{ display: 'flex' }}>
                                    <Typography className='dialogVariant'>STA1: </Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.sta1}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <div className={classes.fermTemp}>
                                    <Typography className='dialogVariant'>Fermentation Temp: </Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.optFermentTempF ? this.item.optFermentTempF : null}
                                        {this.item.optFermentTempF && this.item.optFermentTempC ? ' (' : ''}
                                        {this.item.optFermentTempC ? this.item.optFermentTempC : null}
                                        {this.item.optFermentTempF && this.item.optFermentTempC ? ')' : ''}
                                    </Typography>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={2}
                        className={classes.description}
                    >
                        <Grid item>
                            <div dangerouslySetInnerHTML={{ __html: itemDescription }} />
                        </Grid>
                    </Grid>

                    {
                        id && id != 0 && isVaultItem && this.item.salesCategoryIconOverride != 997 && this.item.salesCategoryIconOverride != 999 &&
                        <Grid
                            item
                            container
                            direction={'column'}
                            spacing={2}
                            style={{ color: '#f68f32', border: 'solid 1px', padding: '10px', marginTop: '10px' }}
                        >
                                {!this.state.confirmNotifyEmailMsg &&
                                    <>
                                        <Typography style={{ fontWeight: 'bolder' }}>We can notify you when this strain goes into production!</Typography>
                                        <TextField
                                            id="outlined-helperText"
                                            variant="outlined"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            name="Email"
                                            fullWidth
                                            type="email"
                                            placeholder={email}
                                            value={this.state.notifyEmail}
                                            onChange={this.notifyEmailChange}
                                        />
                                        {
                                            this.state.errorNotifyEmailMsg &&
                                            <Typography style={{ fontSize: 'small', color: 'red' }}>
                                                {this.state.errorNotifyEmailMsg}
                                            </Typography>
                                        }
                                        <Typography style={{ fontSize: 'x-small' }}>
                                            We will notify you at your main email address. To be notified at other email addresses, enter them above, separated with commas.
                                    </Typography>
                                        <div className={classes.buttons}>
                                            <Button
                                                variant='contained'
                                                color='primary'
                                                onClick={this.requestVaultWorkOrderNotification}
                                                className={classes.button}
                                            >
                                                Notify me!
                                        </Button>
                                        </div>
                                    </>
                                }
                                {
                                this.state.confirmNotifyEmailMsg &&
                                    <Typography style={{ fontSize: 'small', color: 'green', textAlign: 'center' }}>
                                        {this.state.confirmNotifyEmailMsg}
                                    </Typography>
                                }
                        </Grid>
                    }

                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={2}
                        style={{ marginTop: 20, color: '#f68f32' }}
                    >
                        <Button onClick={this.moveToCalculator}>
                            <Grid item>
                                <Typography style={{ color: getColor(this.item.salesCategory) }}>How much do I need?</Typography>
                            </Grid>
                        </Button>
                    </Grid>

                    <Grid
                        item
                        container
                        style={{ marginTop: 5 }}
                        direction={'row'}
                        justify='center'
                        spacing={10}
                    >
                            {availability ?
                                <div>
                                    <Typography className='flex-center' style={{ textAlign: 'center', color: availability.indexOf(IN_STOCK) >= 0 ? 'green' : 'red' }}>
                                        {availability}
                                    </Typography>
                                    <Typography className='flex-center'>
                                        <p style={{ textAlign: 'center' }}>{nextShipDateMsg}</p>
                                    </Typography>
                                </div>
                                :
                                (availabilitySD || availabilityCPH || availabilityHK) ?
                                    <div>
                                        <Typography 
                                            className='flex-center' 
                                            style={{ textAlign: 'center', color: availabilitySD && availabilitySD.indexOf(IN_STOCK) >= 0 ? 'green' : 'red' }}
                                        >
                                            {availabilitySD}
                                        </Typography>
                                        {showOrganicAlternative ? null :
                                            <Typography 
                                                className='flex-center' 
                                                style={{ textAlign: 'center', color: availabilityCPH && availabilityCPH.indexOf(IN_STOCK) >= 0 ? 'green' : 'red' }}
                                            >
                                                {availabilityCPH}
                                            </Typography>
                                        }
                                        <Typography 
                                            className='flex-center' 
                                            style={{ textAlign: 'center', color: availabilityHK && availabilityHK.indexOf(IN_STOCK) >= 0 ? 'green' : 'red' }}
                                        >
                                            {availabilityHK}
                                        </Typography>
                                        <Typography className='flex-center' style={{ textAlign: 'center' }}>{nextShipDateMsg}</Typography>
                                    </div>
                                    :
                                    !this.item.isVaultPreorderItem && (!this.state.pack || this.state.pack >= 0) &&
                                        <Grid
                                            item
                                            xs
                                            container
                                            spacing={6}
                                            direction={'row'}
                                            justify='center'
                                        >
                                            <div className={classes.buttons}>
                                                <Button
                                                    variant='contained'
                                                    color='primary'
                                                    onClick={this.checkAvailability}
                                                    className={classes.button}
                                                >
                                                    Get Availability
                                                </Button>
                                            </div>
                                        </Grid>
                            }
                    </Grid>
                    {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                        <Grid
                            item
                            container
                            spacing={6}
                            style={{ marginTop: 5, marginBottom: 10 }}
                            direction={'row'}
                        >
                            <Formik
                                initialValues={this.state}
                                validationSchema={customFormValidation}
                                onSubmit={values => this.addToCart(values)}
                            >
                                {({ values, handleChange }) => {
                                    return (
                                        <Form className={classes.form}>
                                            <FormikErrorMessage error={error} classes={classes} />
                                            <Grid
                                                item
                                                xs
                                                container
                                                spacing={6}
                                                direction={'row'}
                                                justify='center'
                                                className={classes.paddingFix}
                                            >
                                                <Grid item xs={12} sm={4} md={4} className={classes.formFields} >
                                                    <FormControl>
                                                        <InputLabel >Packaging</InputLabel>
                                                        <Select
                                                            value={this.state.packaging}
                                                            onChange={this.setPackaging}
                                                        >
                                                            {this.state.packagingOptions.map(
                                                                (option, i) => (
                                                                    <MenuItem
                                                                        key={i}
                                                                        value={option.value}
                                                                    >
                                                                        {option.label}
                                                                    </MenuItem>
                                                                )
                                                            )}
                                                        </Select>
                                                    </FormControl>
                                                </Grid>
                                                {this.state.pack && (
                                                    <Grid item xs={12} sm={4} md={4} className={classes.formFields}>
                                                        <FormControl>
                                                            <InputLabel>Pack</InputLabel>
                                                            <Select
                                                                value={this.state.pack}
                                                                onChange={this.setPack}
                                                            >
                                                                {this.state.packOptions.map(
                                                                    (option, i) => (
                                                                        ((option.value >= 0 && volID[parseInt(option.value)]) || (!volID[parseInt(Math.abs(option.value))] && option.value < 0 && !this.item.isOrganic)) // Pack size exists for item
                                                                        && (!isVaultItem || !availQty || option.value >= 0 && availQty[parseInt(Math.abs(option.value))] > 0)
                                                                            ?
                                                                            <MenuItem
                                                                                key={i}
                                                                                value={option.value}
                                                                            >
                                                                                {option.label}
                                                                            </MenuItem>
                                                                            : null
                                                                    )
                                                                )}
                                                            </Select>
                                                        </FormControl>
                                                    </Grid>
                                                )}
                                                {(!this.state.pack || this.state.pack >= 0) &&
                                                    <Grid item xs={12} sm={4} md={4} className={classes.formFields}>
                                                        {/* <form> */}
                                                            <TextField
                                                                id='quantity'
                                                                label='Quantity'
                                                                className={classes.quantity}
                                                                value={this.state.quantity}
                                                                onChange={this.changeQuantity}
                                                                type='number'
                                                                step={this.item.salesCategory == 32 || this.item.type == 5 ? '0.5' : '1'}
                                                                pattern={this.item.salesCategory == 32 || this.item.type == 5 ? '-?[0-9]*(\.[5]+)?' : ''}
                                                                error={
                                                                    this.item.salesCategory == 32 ? true
                                                                        : this.item.type == 5 ? true : false
                                                                }
                                                            />
                                                        {/* </form> */}
                                                    </Grid>
                                                }
                                            </Grid>
                                            {this.state.pack == '1' &&
                                                <div style={{textAlign: 'center', marginTop: '10px', marginBottom: '10px'}}>
                                                    <Typography style={{ color: 'red', fontWeight: 'bolder', fontSize: 'smaller' }}>
                                                        PLEASE NOTE: White Labs is phasing out new lots of 1.5L PurePitch packages (still available in Custom Pour). 
                                                        To determine if your strain is still available in PurePitch, set the quantity you are looking for and hit the 
                                                        "Get Availability" button. If not in stock, please use a combination of Nanos (0.5Ls) and 2Ls. For instance, 
                                                        if you normally order two 1.5L packages, please order one 2L and two Nanos. We have adjusted prices to keep 
                                                        prices the same.
                                                    </Typography>
                                                </div>
                                            }
                                            {this.state.pack == '-1' &&
                                                <div style={{ textAlign: 'center', marginTop: '10px', marginBottom: '10px' }}>
                                                    <Typography style={{ color: 'red', fontWeight: 'bolder', fontSize: 'smaller' }}>
                                                        PLEASE NOTE: White Labs is phasing out new lots of 1.5L PurePitch packages (still available in Custom Pour) and 
                                                        this strain is no longer in stock in the 1.5L size. Please use a combination of Nanos (0.5Ls) and 2Ls. For instance,
                                                        if you normally order two 1.5L packages, please order one 2L and two Nanos. We have adjusted prices to keep
                                                        prices the same.
                                                    </Typography>
                                                </div>
                                            }
                                            {this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS && (!this.state.pack || this.state.pack >= 0) &&
                                                <Grid
                                                    item
                                                    xs
                                                    container
                                                    spacing={6}
                                                    direction={'row'}
                                                    justify='center'
                                                >
                                                    <div className={classes.addButton}>
                                                        <Button
                                                            type='submit'
                                                            variant='contained'
                                                            color='primary'
                                                        >
                                                            Add to Cart
                                                        </Button>
                                                    </div>
                                                </Grid>
                                            }
                                        </Form>
                                    )
                                }
                                }
                            </Formik>
                        </Grid>
                    }
                    {showOrganicAlternative ?
                        <Typography style={{ color: 'darkGreen', fontWeight: 'bolder', fontSize: 'small', textAlign: 'center', marginTop: '40px' }} className='flex-center'>
                                Attention White Labs Copenhagen customers: This production facility makes its own organic version of this strain. Please log in now to view and order this strain from White Labs Copenhagen.
                        </Typography>
                        : null
                    }
                    {showNonOrganicAlternative ?
                        <Typography className='flex-center'>
                            <p style={{ color: 'white', backgroundColor: 'darkGreen', fontWeight: 'bolder', fontSize: 'small', textAlign: 'center', marginTop: '20px' }}>
                                This organic strain is produced by White Labs Copenhagen. If you are not a customer of White Labs Copenhagen, consider the non-organic version for faster shipping!
                            </p>
                        </Typography>
                        : null
                    }
                </DialogContent>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
    info: {
        textAlign: 'center'
    },
    quantity: {
        width: 50
    },
    hide: {
        display: 'none'
    },
    hoverBold: {
        '&:hover': {
            fontWeight: 'bolder',
            color: '#ff9933',
            cursor: 'pointer'
        }
    },
    circle: {
        textAlign: 'center',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: '50%',
        padding: 5,
        width: 37,
        height: 37,
        [theme.breakpoints.down('sm')]: {
            width: 30,
            height: 30,
        }
    },
    errorMsg: {
        marginTop: '20px',
        textAlign: 'center'
    },
    formFields: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '15px',
        // marginLeft:'0px',
        [theme.breakpoints.down('xs')]: {
            display: 'flex',
            justifyContent: 'center',
            marginTop: '15px',
            marginLeft: '42px',
        }
    },
    buttons: {
        display: 'flex',
        justifyContent: 'center'
    },
    addButton: {
        display: 'flex',
        justifyContent: 'center',
        marginLeft: '42px',
        marginTop: '14px',
    },
    button: {
        marginTop: theme.spacing(1),
    },
    description: {
        textAlign: 'center',
        marginTop: 20
    },
    paddingFix: {
        paddingLeft: 'unset',
        marginTop: '5px',
        [theme.breakpoints.between('sm', 'xl')]: {
            paddingLeft: '80px',
        },
        [theme.breakpoints.down('xs')]: {
            paddingLeft: '0px',
        },
    },
    close: { position: 'absolute', right: 0, top: 0 },
    form: {
        width: '100%',
    },
    fermTemp: {
        display: 'flex',
        whiteSpace: 'nowrap',
        [theme.breakpoints.down('xs')]: {
            whiteSpace: 'inherit'
        },
    }
});

YeastDialog.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        inventory: state.inventory,
        user: state.user
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({ ...inventoryActions, ...cartActions }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(YeastDialog));
