import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Popover from "@material-ui/core/Popover";
import Grow from "@material-ui/core/Grow";
import {REMOVE_ITEM} from "../constants";
import { cartActions } from "appRedux/actions/cartActions";
import axios from 'axios';
import LoadingIndicator from 'components/UI/LoadingIndicator';
const YeastElements = {
    "2": {
        img: 'static/images/categories/Category-core.jpg',
        color: '#FFF'
    },
    "3": {  // Ale
        img: 'static/images/categories/Category-ale.jpg',
        icon: 'static/images/icons/Ale-icon.svg',
        color: "#FF9933"
    },
    "4": {  // Wild Yeast
        img: 'static/images/categories/Category-wild.jpg',
        icon: 'static/images/icons/wildyeast-icon.svg',
        color: "#CC9966"
    },
    "5": {  // Lager
        img: 'static/images/categories/Category-lager.jpg',
        icon: 'static/images/icons/Lager-icon.svg',
        color: "#FFCC33"
    },
    "6": {  // Wine
        img: 'static/images/categories/Category-wine.jpg',
        icon: 'static/images/icons/wine-icon.svg',
        color: "#9966CC"
    },
    "7": {  // Distilling
        img: 'static/images/categories/Category-Distilling.jpg',
        icon: 'static/images/icons/Distilling-icon.svg',
        color: "#6666CC"
    },
    "8": {  // Belgian
        img: 'static/images/categories/Category-belgian.jpg',
        icon: 'static/images/icons/Belgian-icon.svg',
        color: "#66CCCC"
    },
    "32": { // Vault
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/vault-icon.svg',
        color: "#B3B3B3"
    }
}

function getImage(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].img;
    }
    catch(error) {

    }
}

function getIcon(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].icon;
    }
    catch(error) {

    }
}

function getColor(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].color;
    }
    catch(error) {

        throw error;
    }
}

class VaultHomebrewCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: "1",
            hover: false,
            img: null,
            icon: null,
            color: null,
            anchorEl: null,
        };
        this.item = this.props.item;
    }

    handleItemHoverEnter = () => {
        this.setState({ hover: true });
    };

    handleItemHoverLeave = () => {
        this.setState({ hover: false });
    };

    checkQuantity = (cartItem) => {

        var quantity = parseFloat(cartItem.OrderDetailQty);

        if(isNaN(quantity) || quantity <= 0 ) {

            return false;
        }

        //  Must be in increments of 1
        else if ((parseFloat(quantity) / parseInt(quantity) != 1.0)) {
            return false;
        }

        return true;
    }

    changeQuantity = (event) => {
        this.setState({ quantity: event.target.value });
    }

    addToCart = (event) => {
        try {
            var quantity = this.state.quantity;
            var item = this.item;

            // Create cart item
            var cartItem = {};
            cartItem.Name = String(item.Name);
            cartItem.salesCategory = parseInt(item.salesCategory);
            cartItem.dispQuantity = parseInt(quantity);
            cartItem.OrderDetailQty = parseFloat(quantity);

            cartItem.MerchandiseID = item.volID[4];
            cartItem.type = 2;
            cartItem.details = "Vault homebrew packs";

            cartItem.mfgEnvironment = this.item.mfgEnvironment;
            cartItem.isVaultPreorderItem = true;
            cartItem.imageUrl = item.imageUrl;

            if (this.checkQuantity(cartItem)) {
                if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                    this.props.addItem({ cartItem });
                    this.props.closeDialog();
                } else {
                    this.setState({ isLoading: true });
                    axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                        .then(({ data: { relatedItems, error } }) => {
                            if (error) throw error;

                            for (var i = 0; i < relatedItems.length; i++) {
                                cartItem.relatedItems = [];
                                for (var j = 0; j < relatedItems[i].related.length; j++) {
                                    var relItemArray = this.props.inventory.items.filter(function (el) {
                                        return (el.volID.includes(relatedItems[i].related[j]));
                                    });
                                    for (var k = 0; k < relItemArray.length; k++) {
                                        cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                    }
                                }
                            }

                            this.setState({ isLoading: false });
                            this.props.addItem({ cartItem });
                            this.props.closeDialog();
                        })
                        .catch(error => {
                            console.log('error', error);

                            // Still add the item to the cart
                            this.setState({ isLoading: false });
                            this.props.addItem({ cartItem });
                            this.props.closeDialog();
                        });
                }
            }
        } catch (error) {

        }
        this.setState({
            anchorEl: event.currentTarget,
        });
        setTimeout(() => { this.setState({ anchorEl: null }) }, 500);
    };

    render() {
        const { classes, theme, item , PaperProps} = this.props;

        const spaceIndex = item.Name.indexOf(" ");
        const itemID = item.Name.substr(0, spaceIndex);
        const itemName = item.Name.substr(spaceIndex + 1);

        const { anchorEl,
            } = this.state;
        const open = Boolean(anchorEl);

        return (
            <>
            <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
            {item.outofstockbehavior !== REMOVE_ITEM &&
            <Grid
                item
                xs={12}
                sm={6}
                md={4}
                lg={3}
                spacing={6}
                onMouseEnter={this.handleItemHoverEnter}
                onMouseLeave={this.handleItemHoverLeave}
            >
                <div
                    className={classes.card}
                   
                    style={
                        !this.state.hover
                            ? {
                                  backgroundImage: `url(${getImage(this.props.item.salesCategory)})`,
                                  backgroundRepeat: "no-repeat",
                                  backgroundSize: "cover"
                              }
                            : { backgroundColor: "#fff" }
                    }
                >
                    {!this.state.hover ? (
                        <Grid item container spacing={2}>
                            <Grid
                                item
                                xs={12}
                                className={classes.info}
                                style={{ marginTop: 30 }}
                            >
                                <img
                                    src={getIcon(this.props.item.salesCategory)}
                                    height="40"
                                />
                                <Typography variant="h5" color="secondary">
                                    {itemID}
                                </Typography>
                                <Typography
                                    variant="subheading"
                                    color="secondary"
                                >
                                    {itemName}
                                </Typography>
                            </Grid>
                        </Grid>
                    ) : (
                        <Grid item container direction={"column"} spacing={2}>
                            <Grid item xsonClick={this.handleClickOrTouch} onTouch={this.handleClickOrTouch}>
                                <Typography
                                    className={classes.info}
                                    variant="subtitle1"
                                    style={{color: getColor(this.props.item.salesCategory)}}
                                >
                                    {itemID}
                                </Typography>
                            </Grid>
                            <Grid item xs={12}onClick={this.handleClickOrTouch} onTouch={this.handleClickOrTouch}>
                                <div
                                    style={{
                                        backgroundColor: getColor(this.props.item.salesCategory),
                                        padding: 1,
                                        textAlign: "center",
                                        marginLeft: theme.spacing(-2),
                                        marginRight: theme.spacing(-2),
                                    }}
                                >
                                    <Typography
                                        variant="subtitle1"
                                        color="secondary"
                                    >
                                        {itemName}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid
                                item
                                xs
                                container
                                direction={"row"}
                                spacing={2}
                                justify="center"
                                >
                                        {(item.optFermentTempF || item.optFermentTempC) && (item.optFermentTempF != '�-�F' || item.optFermentTempC != '�-�C' ) &&
                                        <Grid item xs={6}>
                                            <div className={classes.infoSmall}>
                                                <Typography style={{ fontSize: 'x-small' }}>
                                                    Fermentation Temp
                                            </Typography>
                                                <Typography style={{ color: this.state.color, fontSize: 'x-small' }}>
                                                    {item.optFermentTempF}
                                                    {item.optFermentTempF && item.optFermentTempC ? ' (' : ''}
                                                    {item.optFermentTempC}
                                                    {item.optFermentTempF && item.optFermentTempC ? ')' : ''}
                                                </Typography>
                                            </div>
                                        </Grid>
                                    }
                                    {item.flocculation &&
                                        <Grid item xs={6}>
                                            <div className={classes.infoSmall}>
                                                <Typography style={{ fontSize: 'x-small' }}>Flocculation</Typography>
                                                <Typography style={{ color: this.state.color, fontSize: 'x-small' }}>
                                                    {item.flocculation}
                                                </Typography>
                                            </div>
                                        </Grid>
                                    }
                                    {item.alcoholTol &&
                                        <Grid item xs={6}>
                                            <div className={classes.infoSmall}>
                                                <Typography style={{ fontSize: 'x-small' }}>Alcohol Tol.</Typography>
                                                <Typography style={{ color: this.state.color, fontSize: 'x-small' }}>
                                                    {item.alcoholTol}
                                                </Typography>
                                            </div>
                                        </Grid>
                                    }
                                    {item.attenuation && item.attenuation != ' - ' && item.attenuation != '- ' &&
                                        <Grid item xs={6}>
                                            <div className={classes.infoSmall}>
                                                <Typography style={{ fontSize: 'x-small' }}>Attenuation</Typography>
                                                <Typography style={{ color: this.state.color, fontSize: 'x-small' }}>
                                                    {item.attenuation}
                                                </Typography>
                                            </div>
                                        </Grid>
                                    }
                                    <Grid item>
                                        <div style={{ whiteSpace: 'nowrap' }}>
                                    <TextField
                                            id="quantity"
                                            label="Quantity"
                                            className={classes.quantity}
                                            value={this.state.quantity}
                                            onChange={this.changeQuantity}
                                            type="number"
                                            style={{maxWidth: '25%'}}
                                    />
                                            <div className={classes.buttons} style={{ float: 'right' }}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            onClick={this.addToCart}
                                            onTouch={this.addToCart}
                                            className={classes.button}
                                            aria-owns={open ? "simple-popper" : undefined}
                                            aria-haspopup="true"
                                        aria-owns={open ? "simple-popper" : undefined}
                                            aria-haspopup="true"
                                        >
                                            Add to Cart
                                        </Button>
                                            </div>
                                        </div>
                                </Grid>

                                 <Popover
                                    id="simple-popper"
                                    open={open}
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                    }}
                                    transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                    }}
                                >
                                 <Typography style={{fontSize:'22px', fontWeight:'bolder'}} className={classes.popup}>
                                     Added To Cart
                                 </Typography>
                                </Popover>

                            </Grid>
                        </Grid>
                    )}
                </div>
                <div style={{ backgroundColor: "#FFF", padding: 5 }}>
                    <div className={classes.vaultInfo}>
                        <img className={classes.image} src='/static/images/vault_graphic.png' />
                        <Typography variant="caption" color="primary">{item.preorderedTotal} OF {item.preorderTriggerQty}</Typography>
                        <Typography variant="caption">Preordered</Typography>
                    </div>
                </div>
            </Grid>
            }
        </>
        );
    }
}

VaultHomebrewCard.propTypes = {
    classes: PropTypes.object.isurld,
    theme: PropTypes.object.isurld
};

const styles = theme => ({
    card: {
        border: "solid 1px",
        borderColor: "#CCCCCC",
        padding: theme.spacing(2),
        height: 230,
        minWidth: '340px',
        cursor: "pointer",
        [theme.breakpoints.down('lg')]: {
            minWidth: '250px',
            height: '280px',
        },
        [theme.breakpoints.down('md')]: {
            minWidth: '220px',
            height: '280px',
        },
        [theme.breakpoints.down('sm')]: {
            width: '100%'
        }
    },
    cardHover: {
        transition: theme.transitions.create("width", {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        textAlign: "center"
    },
    infoSmall: {
        textAlign: "center",
        fontSize: "x-small"
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: "center"
    },
    popup:{
        margin: theme.spacing(4)
    },
    paper:{
        border: "1px solid black",
        backgroundColor:'blue'
    },
    vaultInfo: {
        display: 'flex',
        alignItems: "center",
        justifyContent: 'space-between',
        flexDirection: 'row',
    }
});

const mapStateToProps = state => ({
    user: state.user,
    store: state.inventory
})

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(VaultHomebrewCard));
