import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Router from 'next/router';
import axios from 'axios';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import DialogContent from '@material-ui/core/DialogContent';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';

import LoadingIndicator from 'components/UI/LoadingIndicator';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventoryActions } from 'appRedux/actions/inventoryActions';
import { IN_STOCK } from 'lib/Constants';

import ReactGA from 'react-ga';
import { DISALLOW_BACK_ORDERS } from '../constants';
import ReactHtmlParser from 'react-html-parser';

const YeastElements = {
    '2': {
        img: 'static/images/categories/Category-core.jpg',
        color: '#FFF',
    },
    '3': {  // Ale
        img: 'static/images/categories/Category-ale.jpg',
        icon: 'static/images/icons/Ale-icon.svg',
        color: '#FF9933',
    },
    '4': {  // Wild Yeast
        img: 'static/images/categories/Category-wild.jpg',
        icon: 'static/images/icons/wildyeast-icon.svg',
        color: '#CC9966',
    },
    '5': {  // Lager
        img: 'static/images/categories/Category-lager.jpg',
        icon: 'static/images/icons/Lager-icon.svg',
        color: '#FFCC33',
    },
    '6': {  // Wine
        img: 'static/images/categories/Category-wine.jpg',
        icon: 'static/images/icons/wine-icon.svg',
        color: '#9966CC',
    },
    '7': {  // Distilling
        img: 'static/images/categories/Category-Distilling.jpg',
        icon: 'static/images/icons/Distilling-icon.svg',
        color: '#6666CC',
    },
    '8': {  // Belgian
        img: 'static/images/categories/Category-belgian.jpg',
        icon: 'static/images/icons/Belgian-icon.svg',
        color: '#66CCCC',
    },
    '32': { // Vault
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/vault-icon.svg',
        color: '#B3B3B3',
    },
    '998': { // Organic
        img: 'static/images/categories/Category-organic.jpg',
        icon: 'static/images/icons/organic-icon.svg',
        color: '#B3B3B3',
        hbTitle: 'Organic'
    },
    '999': { // Vault with Pure Pitch stock
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/Green_Lock_v01.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    },
};

function getIcon(item, salesCategory) {
    try {
        var icons = [];
        if (item.isOrganic && salesCategory !== 998) {
            icons.push(YeastElements[998].icon);
        }
        icons.push(YeastElements[parseInt(salesCategory)].icon);
        return icons;
    }
    catch (err) {
        console.log('error', salesCategory, err);
    }
}

function getColor(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].color;
    } catch (error) {
        throw error;
    }
}

const FormikErrorMessage = ({ error, classes }) => {
  return error ? <div className={classes.errorMsg + ' error'}>{error}</div> : null;
};

const customFormValidation = Yup.object().shape({
  packaging: Yup.string().required('Required'),
  pack: Yup.string().nullable(),
  quantity: Yup.string().required('Required'),
});

class VaultDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            quantity: '1',
            packOptions: [
                { label: 'Nano', value: '0' },
                { label: '1.5L', value: '1' },
                { label: '2L', value: '2' },
            ],
            pack: '0',
            packagingOptions: [],
            packaging: 'pp',
            availability: null,
            availabilitySD: null,
            availabilityCPH: null,
            availabilityHK: null,
            isLoading: false,
            errors: {},
        };

        this.item = this.props.item;

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/store-yeast-' + this.item.Name.replace(/\s/g, '-'));
    }

    componentDidMount() {
        if (this.item.volID[6]) {
            this.setState({ packaging: '6', pack: null });
        } else if (this.item.volID[0] && this.item.volID[2]) {
            if (this.item.purePitch) {
                this.setState({ packaging: 'pp', pack: '0' });
            } else {
                this.setState({ packaging: 'nl', pack: '0' });
            }
        } else if (this.item.volID[0]) {
            this.setState({ packaging: '0', pack: null });
        } else {
            this.setState({ packaging: '3', pack: null });
        }

        this.filterPackageTypes();
    }

    filterPackageTypes() {
        try {
            var PackageTypes = [
                { label: '1L Nalgene Bottle', value: '6' },
                { label: 'PurePitch', value: 'pp' },
                { label: 'Nalgene Bottle', value: 'nl' },
                { label: ((this.item.strainCategory == 32 || this.item.strainCategory == 33) ? '1L Nalgene Bottle' : 'Custom Pour'), value: '3' },
                { label: 'Homebrew', value: '4' },
                { label: 'Slant', value: '5' },
                { label: 'Yeast', value: '0' },
            ];

            var subsidiary = (this.props.user.subsidiary ? this.props.user.subsidiary : 2),
                FilteredPackageTypes = [];

            if (this.item.volID[6]) {
                FilteredPackageTypes.push(PackageTypes[0]);
            } else if (this.item.volID[0] && this.item.volID[2]) {
                if (this.item.purePitch) {
                    FilteredPackageTypes.push(PackageTypes[1]);
                } else {
                    FilteredPackageTypes.push(PackageTypes[2]);
                }
            } else if (this.item.volID[0]) {
                FilteredPackageTypes.push(PackageTypes[6]);
            }

            if (this.item.volID[3]) {
                FilteredPackageTypes.push(PackageTypes[3]);
            }

            if (this.item.volID[4] && subsidiary == 2) {
                FilteredPackageTypes.push(PackageTypes[4]);
            }

            if (this.item.volID[5]) {
                FilteredPackageTypes.push(PackageTypes[5]);
            }

            this.setState({ packagingOptions: FilteredPackageTypes });
            if (FilteredPackageTypes.length == 1) {
                this.setState({ packaging: FilteredPackageTypes[0].value });
            }
        } catch (error) {
            return error;
        }
    }

    handleErrors = (field, err) => {
        let { errors } = this.state;
        errors[field] = err;
        this.setState({ errors });
    };

    checkQuantity = item => {
        try {
            var quantity = parseFloat(item.OrderDetailQty);

            if (isNaN(quantity) || quantity <= 0) {
                // TO-DO: Display message to user
                this.handleErrors('quantity', 'Please enter a valid value for the quantity');

                return false;
            }

            // Wild Yeast must have mimimum 1L

            if (item.salesCategory == 4 && quantity < 1.0) {
                this.handleErrors('quantity', 'Notice: The minimum quantity sold for Wild Yeast strains is 1L. Please adjust your quantity.');
                return false;
            }
            // Custom Pour Strains
            if (item.type == 5) {
                // Vault strains must have minimum 1.5L Custom Pour
                if (item.salesCategory == 32 && !(this.item.strainCategory == 32 || this.item.strainCategory == 33)) {
                    if (quantity < 1.5) {
                        // TO-DO: Display message to user
                        this.handleErrors('quantity', 'Notice: The minimum quantity sold for Custom Pour Vault strains is 1.5L. Please adjust your quantity.');
                        return false;
                    } else if ((parseFloat(quantity) / parseInt(quantity) != 1.0) && (parseFloat(quantity + 0.5) / parseInt(quantity + 0.5) != 1.0)) {
                        this.handleErrors('quantity', 'Notice: Custom Pour Vault strains are sold in 0.5L increments. Please adjust your quantity.');
                        return false;
                    }
                }

                // Bacteria sold in 1L increments
                if (item.salesCategory == 4 || this.item.strainCategory == 32 || this.item.strainCategory == 33) {
                    if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
                        quantity = Math.round(quantity);

                        // TO-DO: Display message to user
                        this.handleErrors('quantity', 'Notice: Quantities for this strain must be in 1L increments, your value has been rounded accordingly. Please review your cart.');
                    }
                }

                // All other custom pour strains sold in 0.5L increments
                else {
                    if ((parseFloat(quantity) / parseInt(quantity) != 1.0) && (parseFloat(quantity + 0.5) / parseInt(quantity + 0.5) != 1.0)) {
                        this.handleErrors('quantity', 'Notice: Custom Pours are sold in 0.5L increments. Please adjust your quantity.');
                        return false;
                    }
                }

                item.size = quantity;
                item.details = quantity + ((this.item.strainCategory == 32 || this.item.strainCategory == 33) ? ' 1L Nalgene Bottle(s)' : 'L Custom Pour');
                item.OrderDetailQty = parseFloat(quantity);
            }

            // Non-custom pour strains must be in increments of 1
            else if (parseFloat(quantity) / parseInt(quantity) != 1.0) {
                this.handleErrors('quantity', 'Quantity Error !!');
                return false;
            }

            return true;
        } catch (error) {
            this.handleErrors('quantity', `Could not check quantity ${error}`);
        }
    };

    addToCart = (values) => {
        const packaging = this.state.packaging;
        const pack = this.state.pack;
        const quantity = this.state.quantity;

        // Create cart item
        var cartItem = {};
        cartItem.Name = String(this.item.Name);
        cartItem.salesCategory = parseInt(this.item.salesCategory);
        cartItem.dispQuantity = parseInt(quantity);
        cartItem.OrderDetailQty = parseFloat(quantity);
        cartItem.mfgEnvironment = this.item.mfgEnvironment;
        cartItem.isVaultPreorderItem = true;
        cartItem.imageUrl = this.item.imageUrl;

        // PurePitch / 1L Nalgene bottle
        if (packaging == 'pp' || packaging == 'nl') {
            switch (pack) {
                // Nano
                case '0':
                    cartItem.MerchandiseID = this.item.volID[0];
                    cartItem.details = 'Nano';
                    break;

                // 1.5L
                case '1':
                    cartItem.MerchandiseID = this.item.volID[1];
                    cartItem.details = '1.5L';
                    break;

                // 2L
                case '2':
                    cartItem.MerchandiseID = this.item.volID[2];
                    cartItem.details = '2L';
                    break;
                default:
                    this.handleErrors('pack', `cannot add to cart, ${item}, ${packaging}, ${pack}, ${quantity}`);
                    return;
            }

            if (this.item.purePitch) {
                cartItem.details = 'PurePitch® ' + cartItem.details;
            }

            cartItem.type = 1;
        } else {
            switch (packaging) {
                // Yeast
                case '0':
                    cartItem.MerchandiseID = this.item.volID[0];
                    cartItem.type = 3;
                    cartItem.details = 'Yeast';
                    break;

                // Custom Pour
                case '3':
                    cartItem.MerchandiseID = this.item.volID[3];
                    cartItem.type = 5;
                    cartItem.dispQuantity = 1;
                    cartItem.size = parseFloat(quantity);
                    cartItem.details = quantity + ((this.item.strainCategory == 32 || this.item.strainCategory == 33) ? ' 1L Nalgene Bottle(s)' : 'L Custom Pour');
                    cartItem.relatives = [];
                    var multipliers = [0.5, 1.5, 2];

                    for (var i = 0; i < 3; i++) {
                        if (this.item.volID[i]) {
                            var relative = {};
                            relative.id = parseInt(this.item.volID[i]);
                            if (isNaN(relative.id)) {
                                throw { message: 'Invalid VolID Index! in Relatives', code: 0 };
                            }
                            relative.mult = multipliers[i];
                            cartItem.relatives.push(relative);
                        }
                    }
                    break;

                // Homebrew
                case '4':
                    cartItem.MerchandiseID = this.item.volID[4];
                    cartItem.type = 2;
                    cartItem.details = 'Homebrew packs';
                    break;

                // Slant
                case '5':
                    cartItem.MerchandiseID = this.item.volID[5];
                    cartItem.type = 3;
                    cartItem.details = 'Slants';
                    break;

                // 1L Nalgene Bottle
                case '6':
                    cartItem.MerchandiseID = this.item.volID[6];
                    cartItem.type = 1;
                    cartItem.details = '1L Nalgene Bottle';
                    break;
            }
        }

        if (this.checkQuantity(cartItem)) {
            if (this.props.user.subsidiary && this.props.user.subsidiary != 2) {
                this.props.addItem({ cartItem });
                this.props.closeDialog();
            } else {
                this.setState({ isLoading: true });
                axios.post('/related-items', { itemId: cartItem.MerchandiseID })
                    .then(({ data: { relatedItems, error } }) => {
                        if (error) throw error;

                        for (var i = 0; i < relatedItems.length; i++) {
                            cartItem.relatedItems = [];
                            for (var j = 0; j < relatedItems[i].related.length; j++) {
                                var relItemArray = this.props.inventory.items.filter(function (el) {
                                    return (el.volID.includes(relatedItems[i].related[j]));
                                });
                                for (var k = 0; k < relItemArray.length; k++) {
                                    cartItem.relatedItems.push({ relatedItemId: relatedItems[i].related[j], related: relItemArray[k] });
                                }
                            }
                        }

                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    })
                    .catch(error => {
                        console.log('error', error);

                        // Still add the item to the cart
                        this.setState({ isLoading: false });
                        this.props.addItem({ cartItem });
                        this.props.closeDialog();
                    });
            }
        }
    };

    checkAvailability = () => {
        ReactGA.pageview('/store-yeast-availability-' + this.item.Name.replace(/\s/g, '-'));
        let { subsidiary } = this.props.user;
        const packaging = this.state.packaging;
        const pack = this.state.pack;
        let itemID;

        if (packaging == 'pp' || packaging == 'nl') {
            switch (pack) {
                // Nano
                case '0':
                    itemID = this.item.volID[0];
                    break;

                // 1.5L
                case '1':
                    itemID = this.item.volID[1];
                    break;

                // 2L
                case '2':
                    itemID = this.item.volID[2];
                    break;

                default:
                    return;
            }
        } else {
            switch (packaging) {
                // Homebrew
                case '4':
                    itemID = this.item.volID[4];
                    break;

                // Slant
                case '5':
                    itemID = this.item.volID[5];
                    break;

                // 1L Nalgene Bottle
                case '6':
                    itemID = this.item.volID[6];
                    break;

                // Distilling
                case '0':
                    itemID = this.item.volID[0];
                    break;

                // Vault
                case '3':
                    itemID = this.item.volID[3];
                    break;

                default:
                    return;

            }
        }

        if (!subsidiary || subsidiary === '') {
            var subs = [2, 5, 7];
            this.setState({ availability: null });

            // Hacky "switch" statement to handle indeterminate async call duration
            // cross-contaminating result variables. Also, separate availabilities to make
            // sure they are always displayed in the same order in the results.
            for (let i = 0; i < subs.length; i++) {
                this.setState({ isLoading: true });
                switch (i) {
                    case 0:
                        const s = subs[i];
                        subsidiary = s;
                        this.props.checkAvailability({ data: { itemID, subsidiary }, item: 'availabilitySD' });
                        break;
                    case 1:
                        const s1 = subs[i];
                        subsidiary = s1;
                        this.props.checkAvailability({ data: { itemID, subsidiary }, item: 'availabilityHK' });
                        break;
                    case 2:
                        const s2 = subs[i];
                        subsidiary = s2;
                        this.props.checkAvailability({ data: { itemID, subsidiary }, item: 'availabilityCPH' });
                        break;
                }
            }
        } else {
            this.props.checkAvailability({ data: { itemID, subsidiary }, item: 'subsidiary' });
        }
    };

    handleDialogClose() {
        this.props.closeDialog();
    };

    setPack = event => {
        this.setState({
            pack: event.target.value,
            availability: null,
        });
    };

    setPackaging = event => {
        var packaging = event.target.value;
        var pack;

        if (packaging == 'pp' || packaging == 'nl') {
            pack = '0';
        }

        this.setState({
            packaging: event.target.value,
            pack: pack,
            availability: null,
        });
    };

    changeQuantity = event => {
        this.setState({ quantity: event.target.value });
    };

    moveToCalculator = () => {
        Router.push(`/calculator?id=${this.item.volID[0]}`);
    };

    handleClick = (partNum) => {
        partNum = encodeURIComponent(partNum);
        Router.push(`/yeastparam?item=${partNum}`)
        this.props.setPageData(this.props.stateData);
    }

    render() {
        const {
            partNum,
            volID,
            availability,
            availabilityCPH,
            availabilityHK,
            availabilitySD,
        } = this.props.item;

        const { classes } = this.props;
        const { errors } = this.state;
        const spaceIndex = this.item.Name.indexOf(' ');
        const itemID = this.item.Name.substr(0, spaceIndex);
        const itemName = this.item.Name.substr(spaceIndex + 1);
        const error = errors.packaging || errors.pack || errors.quantity;
        const nextShipDateMsg = (
            availability && availability.startsWith(IN_STOCK) ? '' :
                (availabilitySD && !availabilitySD.startsWith(IN_STOCK))
                    || (availabilityCPH && !availabilityCPH.startsWith(IN_STOCK))
                    || (availabilityHK && !availabilityHK.startsWith(IN_STOCK)) ? '(next available ship date shown in checkout)' : ''
        );

        var effectiveSalesCategory = (this.item.isOrganic ? 998 : this.item.salesCategory);
        const icons = getIcon(this.item, this.item.salesCategoryIconOverride ? this.item.salesCategoryIconOverride : effectiveSalesCategory);

        return (
            <React.Fragment>
                <LoadingIndicator visible={this.state.isLoading} label={'Please Wait'} />
                <DialogContent>
                    <div className={classes.close}>
                        <IconButton
                            color='inherit'
                            size='small'
                            aria-label='Menu'
                            onClick={() => this.handleDialogClose()}
                        >
                            <CloseIcon />
                        </IconButton>
                    </div>
                    <Grid
                        item
                        container
                        xs
                        style={{
                            display: 'flex',
                            marginTop: -10,
                            marginBottom: 20
                        }}
                        direction={'row'}
                        spacing={2}
                    >
                        <Grid item>
                            <Typography variant='h5' className={classes.hoverBold} onClick={() => this.handleClick(partNum)}>
                                {itemID} | {itemName}
                            </Typography>
                        </Grid>
                    </Grid>

                    <Grid container spacing={6}>
                        <Grid item xs={2} md={1}>
                            <div
                                className={classes.circle}
                                style={{ backgroundColor: getColor(this.item.salesCategory) }}
                            >
                                {!icons || icons.length == 0 ? null :
                                    <img
                                        src={icons[0]}
                                        height='20'
                                    />
                                }
                                {
                                    icons && icons.length > 1 && <img
                                        src={icons[1]}
                                        height='20'
                                    />
                                }
                            </div>
                        </Grid>
                        <Grid
                            item
                            container
                            xs={10}
                            md={11}
                            direction={'row'}
                            spacing={2}
                        >
                            <Grid item xs={12} md={6}>
                                <div style={{ display: 'flex' }}>
                                    <Typography className='dialogVariant' >Attenuation:</Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.attenuation}
                                    </Typography>
                                </div>
                            </Grid>

                            <Grid item xs={12} md={6}>
                                <div style={{ display: 'flex' }}>
                                    <Typography className='dialogVariant'>Flocculation: </Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.flocculation}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <div style={{ display: 'flex' }}>
                                    <Typography className='dialogVariant'>Alcohol&nbsp;Tol.:</Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.alcoholTol}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <div style={{ display: 'flex' }}>
                                    <Typography className='dialogVariant'>STA1: </Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.sta1}
                                    </Typography>
                                </div>
                            </Grid>
                            <Grid item xs={12} md={6}>
                                <div className={classes.fermTemp}>
                                    <Typography className='dialogVariant'>Fermentation Temp: </Typography>
                                    &nbsp;
                                    <Typography
                                        className='dialogVariant'
                                        style={{ color: getColor(this.item.salesCategory) }}
                                    >
                                        {this.item.optFermentTempF ? this.item.optFermentTempF : null}
                                        {this.item.optFermentTempF && this.item.optFermentTempC ? ' (' : ''}
                                        {this.item.optFermentTempC ? this.item.optFermentTempC : null}
                                        {this.item.optFermentTempF && this.item.optFermentTempC ? ')' : ''}
                                    </Typography>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>

                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={2}
                        className={classes.description}
                    >
                        <Grid item>
                            <div>{ReactHtmlParser(this.item.Description)}</div>
                            {this.item.outofstockmessage &&
                                <Typography style={{ paddingTop: '10px', fontWeight: 'bold' }}>
                                    {this.item.outofstockmessage}
                                </Typography>
                            }
                        </Grid>
                    </Grid>

                    <Grid
                        item
                        container
                        direction={'column'}
                        spacing={2}
                        style={{ marginTop: 20, color: '#f68f32' }}
                    >
                        <Button onClick={this.moveToCalculator}>
                            <Grid item>
                                <Typography style={{ color: getColor(this.item.salesCategory) }}>How much do I need?</Typography>
                            </Grid>
                        </Button>
                    </Grid>
                    <Grid
                        item
                        container
                        style={{ marginTop: 5 }}
                        direction={'row'}
                        justify='center'
                    >
                        <Grid
                            item
                            xs
                            container
                            spacing={6}
                            direction={'row'}
                            justify='center'
                        >
                            {
                                availability ? (
                                    <div>
                                        <Typography className='flex-center'
                                            style={{ color: availability.startsWith(IN_STOCK) ? 'green' : 'red' }}>
                                            <p style={{ textAlign: 'center' }}>{availability}</p>
                                        </Typography>
                                        <Typography className='flex-center'>
                                            <p style={{ textAlign: 'center' }}>{nextShipDateMsg}</p>
                                        </Typography>
                                    </div>
                                ) : (availabilitySD || availabilityCPH || availabilityHK) ? (
                                    <div>
                                        <Typography className='flex-center'>
                                            <p
                                                style={{ textAlign: 'center', color: availabilitySD && availabilitySD.startsWith(IN_STOCK) ? 'green' : 'red' }}>{availabilitySD}</p>
                                        </Typography>
                                        <Typography className='flex-center'>
                                            <p
                                                style={{ textAlign: 'center', color: availabilityCPH && availabilityCPH.startsWith(IN_STOCK) ? 'green' : 'red' }}>{availabilityCPH}</p>
                                        </Typography>
                                        <Typography className='flex-center'>
                                            <p
                                                style={{ textAlign: 'center', color: availabilityHK && availabilityHK.startsWith(IN_STOCK) ? 'green' : 'red' }}>{availabilityHK}</p>
                                        </Typography>
                                        <Typography className='flex-center'>
                                            <p style={{ textAlign: 'center' }}>{nextShipDateMsg}</p>
                                        </Typography>
                                    </div>
                                ) : (
                                            <Grid
                                                item
                                                xs
                                                container
                                                spacing={6}
                                                direction={'row'}
                                                justify='center'
                                            >
                                                <div className={classes.buttons}>
                                                    <Button
                                                        variant='contained'
                                                        color='primary'
                                                        onClick={this.checkAvailability}
                                                        className={classes.button}
                                                        style={{ display: 'none', visibility: 'hidden' }}
                                                    >
                                                        Get Availability
                      </Button>
                                                </div>
                                            </Grid>
                                        )
                            }
                        </Grid>
                    </Grid>
                    {
                        this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                        (
                            <Grid
                                item
                                xs
                                container
                                spacing={6}
                                style={{ marginTop: 5, marginBottom: 10 }}
                                direction={'row'}
                            >
                                <Formik
                                    initialValues={this.state}
                                    validationSchema={customFormValidation}
                                    onSubmit={values => this.props.addToCart(values)}
                                >
                                    {
                                        ({ values, handleChange }) => (
                                            <Form className={classes.form}>
                                                <FormikErrorMessage error={error} classes={classes} />
                                                <Grid
                                                    item
                                                    xs
                                                    container
                                                    spacing={6}
                                                    direction={'row'}
                                                    justify='center'
                                                    className={classes.paddingFix}
                                                >
                                                    <Grid item xs={12} sm={4} md={4} className={classes.formFields}>
                                                        <FormControl>
                                                            <InputLabel>Packaging</InputLabel>
                                                            <Select
                                                                value={this.state.packaging}
                                                                onChange={this.setPackaging}
                                                            >
                                                                {
                                                                    this.state.packagingOptions.map(
                                                                        (option, i) => (
                                                                            <MenuItem
                                                                                key={i}
                                                                                value={option.value}
                                                                            >
                                                                                {option.label}
                                                                            </MenuItem>
                                                                        ),
                                                                    )
                                                                }
                                                            </Select>
                                                        </FormControl>
                                                    </Grid>
                                                    {
                                                        this.state.pack &&
                                                        (
                                                            <Grid item xs={12} sm={4} md={4}
                                                                className={classes.formFields}>
                                                                <FormControl>
                                                                    <InputLabel>Pack</InputLabel>
                                                                    <Select
                                                                        value={this.state.pack}
                                                                        onChange={this.setPack}
                                                                    >
                                                                        {
                                                                            this.state.packOptions.map(
                                                                                (option, i) => (
                                                                                    volID[parseInt(option.value)]
                                                                                        ?
                                                                                        <MenuItem
                                                                                            key={i}
                                                                                            value={option.value}
                                                                                        >
                                                                                            {option.label}
                                                                                        </MenuItem>
                                                                                        : null
                                                                                ),
                                                                            )
                                                                        }
                                                                    </Select>
                                                                </FormControl>
                                                            </Grid>
                                                        )
                                                    }
                                                    <Grid item xs={12} sm={4} md={4} className={classes.formFields}>
                                                        <form>
                                                            <TextField
                                                                id='quantity'
                                                                label='Quantity'
                                                                className={classes.quantity}
                                                                value={this.state.quantity}
                                                                onChange={this.changeQuantity}
                                                                type='number'
                                                            />
                                                        </form>
                                                    </Grid>
                                                </Grid>
                                                {
                                                    this.item.outofstockbehavior !== DISALLOW_BACK_ORDERS &&
                                                    (
                                                        <Grid
                                                            item
                                                            xs
                                                            container
                                                            spacing={6}
                                                            direction={'row'}
                                                            justify='center'
                                                        >
                                                            <div className={classes.addButton}>
                                                                <Button
                                                                    type='submit'
                                                                    variant='contained'
                                                                    color='primary'
                                                                >
                                                                    Add to Cart
                                                                </Button>
                                                            </div>
                                                        </Grid>
                                                    )
                                                }
                                            </Form>
                                        )
                                    }
                                </Formik>
                            </Grid>
                        )
                    }
                </DialogContent>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
  info: {
    textAlign: 'center',
  },
  quantity: {
    width: 50,
  },
  hide: {
    display: 'none',
  },
  hoverBold: {
    '&:hover': {
      fontWeight: 'bolder',
      color: '#ff9933',
      cursor: 'pointer',
    },
  },
  circle: {
    textAlign: 'center',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '50%',
    padding: 5,
    width: 37,
    height: 37,
    [theme.breakpoints.down('sm')]: {
      width: 30,
      height: 30,
    },
  },
  errorMsg: {
    marginTop: '20px',
    textAlign: 'center',
  },
  formFields: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '15px',
    // marginLeft:'0px',
    [theme.breakpoints.down('xs')]: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: '15px',
      marginLeft: '42px',
    },
  },
  buttons: {
    display: 'flex',
    justifyContent: 'center',
  },
  addButton: {
    display: 'flex',
    justifyContent: 'center',
    marginLeft: '42px',
    marginTop: '14px',
  },
  button: {
    marginTop: theme.spacing(1),
  },
  description: {
    textAlign: 'center',
    marginTop: 20,
  },
  paddingFix: {
    paddingLeft: 'unset',
    marginTop: '5px',
    [theme.breakpoints.between('sm', 'xl')]: {
      paddingLeft: '80px',
    },
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '0px',
    },

    },
    close: { position: 'absolute', right: 0, top: 0 },
    form: {
        width: '100%',
    },
    fermTemp: {
        display: 'flex',
        whiteSpace: 'nowrap',
        [theme.breakpoints.down('xs')]: {
            whiteSpace: 'inherit'
        },
    }
});

VaultDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
    return {
        inventory: state.inventory,
        user: state.user
    };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators({ ...inventoryActions, ...cartActions }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(VaultDialog));
