import React, { Component } from 'react';

import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { parseAvailabilityResults } from '../../../lib/InventoryUtils';
import { IN_STOCK, OUT_OF_STOCK } from '../../../lib/Constants';
import { REMOVE_ITEM } from "../constants";
import { CardFlip, CardFlipBackSide, CardFlipFrontSide } from '../CardFlip';


export const YeastElements = {
    '2': {
        img: 'static/images/categories/Category-core.jpg',
        color: '#FFF',
        hbTitle: 'Core Strains'
    },
    '3': {  // Ale
        img: 'static/images/categories/Category-ale.jpg',
        icon: 'static/images/icons/Ale-icon.svg',
        color: '#FF9933',
        hbTitle: 'Ale Strains'
    },
    '4': {  // Wild Yeast
        img: 'static/images/categories/Category-wild.jpg',
        icon: 'static/images/icons/wildyeast-icon.svg',
        color: '#CC9966',
        hbTitle: 'Wild Yeast & Bacteria'
    },
    '5': {  // Lager
        img: 'static/images/categories/Category-lager.jpg',
        icon: 'static/images/icons/Lager-icon.svg',
        color: '#FFCC33',
        hbTitle: 'Lager Strains'
    },
    '6': {  // Wine
        img: 'static/images/categories/Category-wine.jpg',
        icon: 'static/images/icons/wine-icon.svg',
        color: '#9966CC',
        hbTitle: 'Wine, Mead, & Cider Strains'
    },
    '7': {  // Distilling
        img: 'static/images/categories/Category-Distilling.jpg',
        icon: 'static/images/icons/Distilling-icon.svg',
        color: '#6666CC',
        hbTitle: 'Distilling Strains'
    },
    '8': {  // Belgian
        img: 'static/images/categories/Category-belgian.jpg',
        icon: 'static/images/icons/Belgian-icon.svg',
        color: '#66CCCC',
        hbTitle: 'Specialty & Belgian Strains'
    },
    '32': { // Vault
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/vault-icon.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    },
    '997': { // Vault with no Pure Pitch stock, but with work orders
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/Yellow_Lock_v01.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    },
    '998': { // Organic
        img: 'static/images/categories/Category-organic.jpg',
        icon: 'static/images/icons/organic-icon.svg',
        color: '#B3B3B3',
        hbTitle: 'Organic'
    },
    '999': { // Vault with Pure Pitch stock
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/Green_Lock_v01.png',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    }
}

function getImage(item, salesCategory) {
    try {
        if (item.ImageURL) {
            return item.ImageURL;
        } else {
            return YeastElements[parseInt(salesCategory)].img;
        }
    } catch (err) {
        console.log('error', salesCategory, err);
    }
}

function getIcon(item, salesCategory) {
    try {
        var icons = [];
        if (item.isOrganic && salesCategory !== 998) {
            icons.push(YeastElements[998].icon);
        }
        icons.push(YeastElements[parseInt(salesCategory)].icon);
        return icons;
    }
    catch (err) {
        console.log('error', salesCategory, err);
    }
}

function getColor(salesCategory) {
    try {
        return YeastElements[parseInt(salesCategory)].color;
    } catch (err) {
        console.log(err);
        throw err;
    }
}

class VaultCard extends Component {
    state = {
        img: null,
        icon: null,
        color: null,
        salesCategoryIconOverride: (this.props.item.salesCategoryIconOverride ? this.props.item.salesCategoryIconOverride : this.props.item.salesCategory),
    };

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    checkVaultPurepitch = () => {
        if (this.props.item && !this.props.item.salesCategoryIconOverrideChecked && this.props.item.availQty && this.props.item.availQty.length > 0) {
            for (var i = 0; i < 3; i++) {
                if (this.props.item.availQty[i] > 0) {
                    this.props.item.salesCategoryIconOverride = 999;
                    this.setState({ salesCategoryIconOverride: 999 });
                    break;
                }
            }
            this.props.item.salesCategoryIconOverrideChecked = true;
        }

        /*
        if (this.props.item.purePitch && this.props.item.isVaultItem) {
            if (!this.props.item.salesCategoryIconOverrideChecked) {
                const subsidiary = 2;

                if (parseInt(localStorage.getItem('VaultAvailRequests')) > 2) {
                    if (!this.interval) {
                        this.interval = setInterval(() => this.checkVaultPurepitch(), 1000);
                    }
                } else {
                    clearInterval(this.interval);
                    localStorage.setItem('VaultAvailRequests', (parseInt(localStorage.getItem('VaultAvailRequests')) + 1));
                    // Verify PP Vault item size availability
                    axios.post('/item-availability', { itemID: this.props.item.volID, subsidiary })
                        .then(({ data: { availability, error } }) => {
                            localStorage.setItem('VaultAvailRequests', (parseInt(localStorage.getItem('VaultAvailRequests')) - 1));
                            if (!error) {
                                const availabilitySD = parseAvailabilityResults(availability, subsidiary);
                                if (availabilitySD && availabilitySD.startsWith(IN_STOCK)) {
                                    if (this.props.item) this.props.item.salesCategoryIconOverride = 999;
                                    this.setState({ salesCategoryIconOverride: 999 });
                                }
                                if (this.props.item) this.props.item.salesCategoryIconOverrideChecked = true;
                            }
                        })
                        .catch(error => {
                            localStorage.setItem('VaultAvailRequests', (parseInt(localStorage.getItem('VaultAvailRequests')) - 1));
                            console.log('error', error);
                        });
                }
            } else {
                clearInterval(this.interval);
            }
        }
        */
    }

    render() {
        this.checkVaultPurepitch();
        const { classes, theme, item } = this.props;
        const spaceIndex = item.Name.indexOf(' ');
        const itemID = item.Name.substr(0, spaceIndex);
        const itemName = item.Name.substr(spaceIndex + 1);

        var effectiveSalesCategory = (this.props.item.isOrganic ? 998 : this.props.item.salesCategory);

        const strainInfoClassName = (item.alcoholTol && item.alcoholTol.length > 20 ? classes.strainInfoLong : classes.strainInfo);
        const strainInfoHeaderClassName = (item.alcoholTol && item.alcoholTol.length > 20 ? classes.strainInfoCaptionLong : classes.strainInfoCaptionLong);

        const icons = getIcon(item, this.state.salesCategoryIconOverride ? this.state.salesCategoryIconOverride : effectiveSalesCategory);

        return (
            <>
                {item.outofstockbehavior !== REMOVE_ITEM && <Grid
                    item
                    xs={12}
                    sm={12}
                    md={6}
                    lg={4}
                    xl={3}
                    // spacing={2}
                    onClick={this.props.onClick.bind(this, this.props.item)}
                    className={classes.cardContainer}
                >
                    <CardFlip>
                        <CardFlipFrontSide className={classes.container}>
                            <div className={classes.flipContainer}>
                                <div style={{ position: 'relative', top: '0', left: '0' }}>
                                    <img className={classes.image} src={getImage(this.props.item, this.props.item.salesCategory)} />
                                </div>
                                <div style={{ position: 'absolute', margin: 'auto', textAlign: 'center' }}>
                                    {!icons || icons.length == 0 ? null :
                                        <img
                                            src={icons[0]}
                                            height='40'
                                        />
                                    }
                                    {
                                        icons && icons.length > 1 && <img
                                            src={icons[1]}
                                            height='40'
                                        />
                                    }
                                </div>
                            </div>
                            <Grid item container spacing={2}>
                                <Grid
                                    xs={12}
                                    item
                                    className={classes.info}
                                >
                                    <Typography
                                        variant='h6'
                                        className={classes.typoTitle}
                                    >
                                        {itemID}
                                    </Typography>
                                    <Typography
                                        variant='h6'
                                        className={classes.break + (effectiveSalesCategory == 8 && ' stroke')}
                                    >
                                        {itemName}
                                        {item.Name}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </CardFlipFrontSide>
                        <CardFlipBackSide className={classes.container}>
                            <div className={classes.flipContainer}>
                                <Grid
                                    className={classes.flipContainerBlock}
                                    xs
                                    item
                                    container
                                    spacing={10}
                                    direction={'row'}
                                >
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>
                                                Fermentation Temp
                                        </Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.optFermentTempF ? item.optFermentTempF : null}
                                                {item.optFermentTempF && item.optFermentTempC ? ' (' : ''}
                                                {item.optFermentTempC ? item.optFermentTempC : null}
                                                {item.optFermentTempF && item.optFermentTempC ? ')' : ''}
                                            </Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>Flocculation</Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.flocculation}
                                            </Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>Alcohol Tol.</Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.alcoholTol}
                                            </Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>Attenuation</Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.attenuation}
                                            </Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>STA1</Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.sta1}
                                            </Typography>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                            <Grid containers="true">
                                <Grid
                                    xs={12}
                                    item
                                    className={classes.info}
                                >
                                    <Typography
                                        variant='h6'
                                        className={classes.typoTitle}
                                    >
                                        {itemID}
                                    </Typography>
                                    <Typography
                                        variant='h6'
                                        className={classes.break}
                                        style={{
                                            padding: 1,
                                            textAlign: 'center'
                                        }}
                                    >
                                        {itemName}
                                        {item.Name}
                                    </Typography>
                                </Grid>
                            </Grid>
                        </CardFlipBackSide>
                    </CardFlip>
                    <div style={{ backgroundColor: "#FFF", padding: 5 }}>
                        <div className={classes.vaultInfo}>
                            <img className={classes.image} src='/static/images/vault_graphic.png' />
                            <Typography variant="caption" color="primary">{item.preorderedTotal} OF {item.preorderTriggerQty}</Typography>
                            <Typography variant="caption">Preordered</Typography>
                        </div>
                    </div>
                </Grid>
                }
            </>
        );
    }
}

const styles = theme => ({
    cardWrapper: {
        width: '100%',
        [theme.breakpoints.up(768)]: {
            width: '50%'
        },
        [theme.breakpoints.up(1100)]: {
            width: '33%'
        },
        [theme.breakpoints.up(1440)]: {
            width: '25%'
        }
    },
    card: {
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        height: '100%',
        minHeight: 230,
        maxWidth: 355,
        minWidth: 265,
        cursor: 'pointer',
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        textAlign: 'center',
    },
    hoverInfo: {
        textAlign: 'center',
        [theme.breakpoints.down('xs')]: {
            fontSize: 'xx-small'
        },
    },
    strainInfoCaption: {
        fontSize: 'x-small',
        [theme.breakpoints.down(600)]: {
            fontSize: 'xx-small'
        },
    },
    strainInfoCaptionLong: {
        fontSize: 'x-small',
        [theme.breakpoints.down(1650)]: {
            fontSize: 'xx-small',
            paddingRight: '5px',
            paddingLeft: '5px'
        },
    },
    strainInfo: {
        fontSize: 'small',
        whiteSpace: 'nowrap',
        [theme.breakpoints.down(1650)]: {
            fontSize: 'xx-small',
            //whiteSpace: 'normal'
        },
    },
    strainInfoLong: {
        fontSize: 'x-small',
        whiteSpace: 'nowrap',
        [theme.breakpoints.down(1650)]: {
            fontSize: 'xx-small',
            //whiteSpace: 'normal',
            paddingRight: '5px',
            paddingLeft: '5px'
        },
    },
    break: {
        wordBreak: 'break-word'
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: 'center'
    },
    typoTitle: {
        fontWeight: 'bolder',
        textShadow: '0px 1px, 1px 0px, 1px 1px',
        letterSpacing: '2px'
    }
});

VaultCard.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    item: PropTypes.shape({
        Name: PropTypes.string,
        salesCategory: PropTypes.number,
        alcoholTol: PropTypes.string,
        attenuation: PropTypes.string,
        flocculation: PropTypes.string,
        optFermentTempF: PropTypes.string,
        optFermentTempC: PropTypes.string,
        salesCategoryIconOverrideChecked: PropTypes.boolean,
        salesCategoryIconOverride: PropTypes.number,
    }),
};

export default VaultCard;
