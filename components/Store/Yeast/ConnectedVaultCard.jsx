import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';
import VaultCard from './VaultCard';
import { bindActionCreators } from 'redux';
import { cartActions } from 'appRedux/actions/cartActions';
import { inventorySelectors, userSelectors } from 'appRedux/selectors';


const styles = theme => ({
    vaultContainer: {
        display: 'flex',
        flexDirection: 'row',
        borderBottom: '1px solid #a0a0a0',
        padding: 30,
        alignItems: 'center',
    },
    buttonPreOrder: {
        marginRight: 5,
        marginLeft: 5,
        backgroundColor: '#aaaaaa',
        textTransform: 'inherit',
        color: '#fff',
        marginTop: 20,
        fontWeight: 'bolder',
    },
    vaultContainerInfo: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerChild: {
        color: '#a0a0a0',
    },
    infoItemBlock: {
        color: '#979797',
        fontWeight: "bold",
    },
    descriptionContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: 20,
    },
    informationContainer: {
        borderLeft: '1px solid #a0a0a0',
        width: '70%',
        paddingLeft: 20,
        display: 'flex',
        flexDirection: 'column',
    },
    informationItem: {
        width: '100%',
        borderBottom: '1px solid #a0a0a0',
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'flex-start',
        padding: 15,
    },
    informationI: {
        width: '100%',
        flexDirection: 'row',
        display: 'flex',
        justifyContent: 'flex-start',
        padding: 15,
    },
    child: {
        width: '50%',
        display: 'flex',
        alignItems: 'flex-end',
    },
    informationItemBlock: {
        width: '50%',
        display: 'flex',
        alignItems: 'flex-end',
        justifyContent: 'space-between',
        paddingRight: 20,
        color: '#ED7932',
        fontWeight: 'bold',
    },
    card: {
        border: 'solid 1px #a0a0a0',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2),
        width: '50%',
        height: 250,
        cursor: 'pointer',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        height: 250,
    },
    break: {
        wordBreak: 'break-word',
        textAlign: 'center',
        color: '#fff',
        fontSize: 24,
        marginTop: 10,
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: 'center',
    },
    typoTitle: {
        fontWeight: 'bolder',
        textShadow: '0px 1px, 1px 0px, 1px 1px',
        letterSpacing: '2px',
        color: '#fff',
        marginTop: 10,
    },
    vaultInfo: {
        display: 'flex',
        alignItems: "center",
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    image: {
        width: 25,
    },
});

const mapStateToProps = state => ({
  user: userSelectors.userSelector(state),
  inventory: inventorySelectors.inventorySelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators(cartActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(VaultCard));
