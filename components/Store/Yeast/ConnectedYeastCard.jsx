import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core';
import YeastCard from './YeastCard';
import { bindActionCreators } from 'redux';
import { cartActions } from 'appRedux/actions/cartActions';
import React from 'react';
import { availabilitySelectors, cartSelectors, inventorySelectors, userSelectors } from 'appRedux/selectors';


const styles = theme => ({
    cardContainer: {
        display: 'flex',
        flexDirection: 'column',
        padding: 10,
    },
    flipContainer: {
        width: 275,
        height: 250,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        
    },
    container: {
        minHeight: 500,
        display: 'flex',
        flexDirection: 'column',
        cursor: 'pointer',
        alignItems: 'center',
        padding: 10,
        border: '1px solid #a0a0a0',
    },
    infoContainer: {
        justifyContent: 'center',
    },
    image: {
        width: 250,
        height: 250,
    },
    card: {
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        padding: theme.spacing(2),
        height: 250,
        minHeight: 250,
        cursor: 'pointer',
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    info: {
        textAlign: 'center',
    },
    description: {
        textAlign: 'center',
        marginTop: 20,
        fontSize: 'xx-small',
    },
    flipContainerBlock: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    break: {
        wordBreak: 'break-word',
        marginTop: 20,
        color: '#a0a0a0',
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: 'center',
    },
    typoTitle: {
        fontWeight: 'bolder',
        textShadow: '0px 1px, 1px 0px, 1px 1px',
        letterSpacing: '2px',
        marginTop: 20,
        color: '#a0a0a0',
    },
    // Styles from the card
    cardWrapper: {
        width: '100%',
        [theme.breakpoints.up(768)]: {
            width: '50%'
        },
        [theme.breakpoints.up(1100)]: {
            width: '33%'
        },
        [theme.breakpoints.up(1440)]: {
            width: '25%'
        }
    },
    card: {
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        height: '100%',
        minHeight: 230,
        maxWidth: 355,
        minWidth: 265,
        cursor: 'pointer',
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        textAlign: 'center',
    },
    hoverInfo: {
        textAlign: 'center',
        [theme.breakpoints.down('xs')]: {
            fontSize: 'xx-small'
        },
    },
    strainInfoCaption: {
        fontSize: 'x-small',
        [theme.breakpoints.down(600)]: {
            fontSize: 'xx-small'
        },
    },
    strainInfoCaptionLong: {
        fontSize: 'x-small',
        [theme.breakpoints.down(1650)]: {
            fontSize: 'xx-small',
            paddingRight: '5px',
            paddingLeft: '5px'
        },
    },
    strainInfo: {
        fontSize: 'small',
        whiteSpace: 'nowrap',
        [theme.breakpoints.down(1650)]: {
            fontSize: 'xx-small',
            //whiteSpace: 'normal'
        },
    },
    strainInfoLong: {
        fontSize: 'x-small',
        whiteSpace: 'nowrap',
        [theme.breakpoints.down(1650)]: {
            fontSize: 'xx-small',
            //whiteSpace: 'normal',
            paddingRight: '5px',
            paddingLeft: '5px'
        },
    },
    break: {
        wordBreak: 'break-word'
    },
    vaultInfo: {
        display: 'flex',
        alignItems: "center",
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    vaultLockImage: {
        width: 25,
    },
    /*
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: 'center'
    },
    typoTitle: {
        fontWeight: 'bolder',
        textShadow: '0px 1px, 1px 0px, 1px 1px',
        letterSpacing: '2px'
    }
    */
});

const mapStateToProps = state => ({
  user: userSelectors.userSelector(state),
  inventory: inventorySelectors.inventorySelector(state),
  isLoading: cartSelectors.isLoadingSelector(state),
  availability: availabilitySelectors.availabilitySelector(state),
  availabilityCPH: cartSelectors.availabilityCPHSelector(state),
  availabilityHK: cartSelectors.availabilityHKSelector(state),
  availabilitySD: cartSelectors.availabilitySDSelector(state),
});

const mapDispatchToProps = dispatch => bindActionCreators(cartActions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles, { withTheme: true })(YeastCard));
