import React, { Component } from 'react';

import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { parseAvailabilityResults } from '../../../lib/InventoryUtils';
import { IN_STOCK, OUT_OF_STOCK } from '../../../lib/Constants';
import { REMOVE_ITEM } from "../constants";
import { CardFlip, CardFlipBackSide, CardFlipFrontSide } from '../CardFlip';
import ReactHtmlParser from 'react-html-parser';
import { isMobile, getUA } from "react-device-detect";

export const YeastElements = {
    '2': {
        img: 'static/images/categories/Category-core.jpg',
        color: '#FFF',
        hbTitle: 'Core Strains'
    },
    '3': {  // Ale
        img: 'static/images/categories/Category-ale.jpg',
        icon: 'static/images/icons/Ale-icon.svg',
        color: '#FF9933',
        hbTitle: 'Ale Strains'
    },
    '4': {  // Wild Yeast
        img: 'static/images/categories/Category-wild.jpg',
        icon: 'static/images/icons/wildyeast-icon.svg',
        color: '#CC9966',
        hbTitle: 'Wild Yeast & Bacteria'
    },
    '5': {  // Lager
        img: 'static/images/categories/Category-lager.jpg',
        icon: 'static/images/icons/Lager-icon.svg',
        color: '#FFCC33',
        hbTitle: 'Lager Strains'
    },
    '6': {  // Wine
        img: 'static/images/categories/Category-wine.jpg',
        icon: 'static/images/icons/wine-icon.svg',
        color: '#9966CC',
        hbTitle: 'Wine, Mead, & Cider Strains'
    },
    '7': {  // Distilling
        img: 'static/images/categories/Category-Distilling.jpg',
        icon: 'static/images/icons/Distilling-icon.svg',
        color: '#6666CC',
        hbTitle: 'Distilling Strains'
    },
    '8': {  // Belgian
        img: 'static/images/categories/Category-belgian.jpg',
        icon: 'static/images/icons/Belgian-icon.svg',
        color: '#66CCCC',
        hbTitle: 'Specialty & Belgian Strains'
    },
    '32': { // Vault
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/vault-icon.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    },
    '997': { // Vault without Pure Pitch stock but with work orders
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/Yellow_Lock_v01.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    },
    '998': { // Organic
        img: 'static/images/categories/Category-organic.jpg',
        icon: 'static/images/icons/organic-icon.svg',
        color: '#B3B3B3',
        hbTitle: 'Organic'
    },
    '999': { // Vault with Pure Pitch stock
        img: 'static/images/categories/Category-vault.jpg',
        icon: 'static/images/icons/Green_Lock_v01.svg',
        color: '#B3B3B3',
        hbTitle: 'Vault Strains'
    }
}

function getImage(item, salesCategory) {
    try {
        if (item.ImageURL) {
            return item.ImageURL;
        } else {
            return YeastElements[parseInt(salesCategory)].img;
        }
    } catch (err) {
        console.log('error', salesCategory, err);
    }
}

function getIcon(item, salesCategory) {
    try {
        var icons = [];
        // Only display icons on the background if not displaying an item image from NetSuite
        if (!item.ImageURL) {
            if (item.isOrganic && salesCategory !== 998) {
                icons.push(YeastElements[998].icon);
            }
            icons.push(YeastElements[parseInt(salesCategory)].icon);
        }
        return icons;
    }
    catch(err) {
        console.log('error', salesCategory, err);
    }
}

function getColor(salesCategory) {
  try {
    return YeastElements[parseInt(salesCategory)].color;
  } catch (err) {
    console.log(err);
    throw err;
  }
}

class YeastCard extends Component {
    state = {
        img: null,
        icon: null,
        color: null,
        salesCategoryIconOverride: (this.props.item.salesCategoryIconOverride ? this.props.item.salesCategoryIconOverride : null),
    };

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    checkVaultPurepitch = () => {
        if (this.props.item && !this.props.item.salesCategoryIconOverrideChecked && this.props.item.availQty && this.props.item.availQty.length > 0) {
            if (this.props.item.vaultWorkOrders && this.props.item.vaultWorkOrders.length > 0) {
                this.props.item.salesCategoryIconOverride = 997;
            }

            for (var i = 0; i < 3; i++) {
                if (this.props.item.availQty[i] > 0) {
                    this.props.item.salesCategoryIconOverride = 999;
                    this.setState({ salesCategoryIconOverride: 999 });
                    break;
                }
            }

            // Also check for standard pour quantity available
            if (!this.props.item.salesCategoryIconOverride && this.props.item.availQty.length >= 7) {
                if (this.props.item.availQty[6] > 0) {
                    this.props.item.salesCategoryIconOverride = 999;
                    this.setState({ salesCategoryIconOverride: 999 });
                }
            }

            this.props.item.salesCategoryIconOverrideChecked = true;
        }
    }

    render() {
        this.checkVaultPurepitch();
        const { classes, item } = this.props;
        const spaceIndex = item.Name.indexOf(' ');
        const itemID = item.Name.substr(0, spaceIndex);
        const itemName = item.Name.substr(spaceIndex + 1);

        const showOrganicAlternative = (!this.props.user.subsidiary && item.hasOrganicVersion && !item.isOrganic);
        const showNonOrganicAlternative = ((!this.props.user.subsidiary || this.props.user.subsidiary != 7) && item.isOrganic);

        var effectiveSalesCategory = (this.props.item.isVaultPreorderItem ? 32 : (this.props.item.isOrganic ? 998 : this.props.item.salesCategory));

        const strainInfoClassName = (item.alcoholTol && item.alcoholTol.length > 20 ? classes.strainInfoLong : classes.strainInfo);
        const strainInfoHeaderClassName = (item.alcoholTol && item.alcoholTol.length > 20 ? classes.strainInfoCaptionLong : classes.strainInfoCaptionLong);

        const icons = getIcon(item, this.state.salesCategoryIconOverride ? this.state.salesCategoryIconOverride
            : (this.props.item.salesCategoryIconOverride ? this.props.item.salesCategoryIconOverride : effectiveSalesCategory)
        );
        
        const dev = process.env.NODE_ENV !== "production";
        var itemDescription = item.Description;
        var backItemDescription = item.Description;

        if (dev || true) {
            if (showOrganicAlternative || showNonOrganicAlternative || !this.props.user.subsidiary || this.props.user.subsidiary != 7) {
                if (isMobile) {
                    itemDescription = '<div style="height:150px; overflow-y: auto; overflow-x: hidden; margin-top:10px;">' + item.Description + '</div>';
                    backItemDescription = '<div style="height:148px; overflow-y: auto; overflow-x: hidden; margin-top:10px;">' + item.Description + '</div>';
                } else {
                    itemDescription = '<div style="height:155px; overflow-y: auto; overflow-x: hidden;margin-top:5px;">' + item.Description + '</div>';
                    backItemDescription = '<div style="height:153px; overflow-y: auto; overflow-x: hidden;margin-top:5px;">' + item.Description + '</div>';
                }
            } else {
                itemDescription = '<div style="height:160px; overflow-y: auto; overflow-x: hidden;">' + item.Description + '</div>';
                backItemDescription = '<div style="height:160px; overflow-y: auto; overflow-x: hidden;">' + item.Description + '</div>';
            }

            itemDescription = itemDescription.replace(/font-family/g, 'font-family-ignore');
            backItemDescription = backItemDescription.replace(/font-family/g, 'font-family-ignore');
        } else {
            var descrMaxLength = (isMobile ? 250 : (getUA.indexOf('CrOS') >= 0 ? 85 : 360));
            if (itemDescription.length > descrMaxLength) {
                itemDescription = itemDescription.substring(0, descrMaxLength - 4);
                while (!itemDescription.endsWith(' ')) itemDescription = itemDescription.substring(0, itemDescription.length - 1);
                itemDescription += ' ...';
            }

            if (backItemDescription.length > descrMaxLength * 3) {
                backItemDescription = backItemDescription.substring(0, (descrMaxLength * 3) - 4);
                while (!backItemDescription.endsWith(' ')) backItemDescription = backItemDescription.substring(0, backItemDescription.length - 1);
                backItemDescription += ' ...';
            }
        }

        return (
            <>
                {item.outofstockbehavior !== REMOVE_ITEM && <Grid
                    item
                    xs={12}
                    sm={12}
                    md={6}
                    lg={4}
                    xl={3}
                    // spacing={2}
                    onClick={this.props.onClick.bind(this, this.props.item)}
                    className={classes.cardContainer}
                >
                    <CardFlip>
                        <CardFlipFrontSide className={classes.container}>
                            <div className={classes.flipContainer}>
                                <div style={{ position: 'relative', top: '0', left: '0' }}>
                                    <img className={classes.image} src={getImage(this.props.item, this.props.item.salesCategory)} />
                                </div>
                                <div style={{position: 'absolute', margin: 'auto', textAlign: 'center'}}>
                                    {!icons || icons.length == 0 ? null :
                                        <img
                                            src={icons[0]}
                                            height='40'
                                        />
                                    }
                                    {
                                        icons && icons.length > 1 && <img
                                            src={icons[1]}
                                            height='40'
                                        />
                                    }
                                </div>
                            </div>
                            <Grid item container spacing={2}>
                                <Grid
                                    xs={12}
                                    item
                                    className={classes.info}
                                >
                                    <Typography
                                        variant='h6'
                                        className={classes.typoTitle}
                                    >
                                        {itemID}
                                    </Typography>
                                    <Typography
                                        variant='h6'
                                        className={classes.break + (effectiveSalesCategory == 8 && ' stroke')}
                                    >
                                        {itemName}
                                    </Typography>
                                    <div style={!this.props.user.subsidiary || this.props.user.subsidiary != 7 ? { fontSize: 'x-small', height: '50px' } : { fontSize: 'x-small', maxHeight: '50px' }}>
                                        {!showOrganicAlternative ? <p></p> :
                                            <p style={{ border: 'solid 1px darkGreen', color: 'darkGreen', fontWeight: 'bolder' }}>
                                                Customer of White Labs Copenhagen? This strain is produced in an organic version! Please <a href='/login'>sign in</a> to view organic purchasing options.
                                            </p>
                                        }
                                        {!showNonOrganicAlternative ? <p></p> :
                                            <p style={{ border: 'solid 1px darkGreen', color: 'white', backgroundColor: 'darkGreen', fontWeight: 'bolder' }}>
                                                This organic strain is produced by White Labs Copenhagen. If you are not a customer of White Labs Copenhagen, consider the non-organic version for faster shipping!
                                            </p>
                                        }
                                    </div>
                                    <div>{ReactHtmlParser(itemDescription)}</div>
                                </Grid>
                            </Grid>
                        </CardFlipFrontSide>
                        <CardFlipBackSide className={classes.container}>
                            <div className={classes.flipContainer}>
                                <Grid
                                    className={classes.flipContainerBlock}
                                    xs
                                    item
                                    container
                                    spacing={10}
                                    direction={'row'}
                                >
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>
                                                Fermentation Temp
                                            </Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.optFermentTempF ? item.optFermentTempF : null}
                                                {item.optFermentTempF && item.optFermentTempC ? ' (' : ''}
                                                {item.optFermentTempC ? item.optFermentTempC : null}
                                                {item.optFermentTempF && item.optFermentTempC ? ')' : ''}
                                            </Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>Flocculation</Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.flocculation}
                                            </Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>Alcohol Tol.</Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.alcoholTol}
                                            </Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>Attenuation</Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.attenuation}
                                            </Typography>
                                        </div>
                                    </Grid>
                                    <Grid item xs={6}>
                                        <div className={classes.hoverInfo}>
                                            <Typography className={strainInfoHeaderClassName}>STA1</Typography>
                                            <Typography className={strainInfoClassName} style={{ color: getColor(effectiveSalesCategory) }}>
                                                {item.sta1}
                                            </Typography>
                                        </div>
                                    </Grid>
                                </Grid>
                            </div>
                            <Grid containers="true">
                                <Grid
                                    xs={12}
                                    item
                                    className={classes.info}
                                >
                                    <Typography
                                        variant='h6'
                                        className={classes.typoTitle}
                                    >
                                        {itemID}
                                    </Typography>
                                    <Typography
                                        variant='h6'
                                        className={classes.break}
                                        style={{
                                            padding: 1,
                                            textAlign: 'center'
                                        }}
                                    >
                                        {itemName}
                                    </Typography>
                                    <div style={!this.props.user.subsidiary || this.props.user.subsidiary != 7 ? { fontSize: 'x-small', height: '50px' } : { fontSize: 'x-small', maxHeight: '50px' }}>
                                        {!showOrganicAlternative ? <p></p> :
                                            <p style={{ border: 'solid 1px darkGreen', color: 'darkGreen', fontWeight: 'bolder' }}>
                                                Customer of White Labs Copenhagen? This strain is produced in an organic version! Please <a href='/login'>sign in</a> to view organic purchasing options.
                                            </p>
                                        }
                                        {!showNonOrganicAlternative ? <p></p> :
                                            <p style={{ border: 'solid 1px darkGreen', color: 'white', backgroundColor: 'darkGreen', fontWeight: 'bolder' }}>
                                                This organic strain is produced by White Labs Copenhagen. If you are not a customer of White Labs Copenhagen, consider the non-organic version for faster shipping!
                                            </p>
                                        }
                                    </div>
                                    <div>{ReactHtmlParser(backItemDescription)}</div>
                                </Grid>
                            </Grid>
                        </CardFlipBackSide>
                    </CardFlip>
                    {!item.isVaultPreorderItem ? null :
                        <div style={{ backgroundColor: "#FFF", padding: 5 }}>
                            <div className={classes.vaultInfo}>
                                <img className={classes.vaultLockImage} src='/static/images/vault_graphic.png' />
                                <div style={{textAlign: 'right'}}>
                                    <Typography variant="caption" color="primary">{item.preorderedTotal} OF {item.preorderTriggerQty}</Typography>
                                    <Typography variant="caption">Preordered</Typography>
                                </div>
                            </div>
                        </div>                        
                    }
                </Grid>
                }
            </>
        );
    }
}

const styles = theme => ({
    cardWrapper: {
        width: '100%',
        [theme.breakpoints.up(768)]: {
            width: '50%'
        },
        [theme.breakpoints.up(1100)]: {
            width: '33%'
        },
        [theme.breakpoints.up(1440)]: {
            width: '25%'
        }
    },
    card: {
        border: 'solid 1px',
        borderColor: '#CCCCCC',
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        height: '100%',
        minHeight: 230,
        maxWidth: 355,
        minWidth: 265,
        cursor: 'pointer',
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        textAlign: 'center',
    },
    hoverInfo: {
        textAlign: 'center',
        [theme.breakpoints.down('xs')]: {
            fontSize: 'xx-small'
        },
    },
    strainInfoCaption: {
        fontSize: 'x-small',
        [theme.breakpoints.down(600)]: {
            fontSize: 'xx-small'
        },
    },
    strainInfoCaptionLong: {
        fontSize: 'x-small',
        [theme.breakpoints.down(1650)]: {
            fontSize: 'xx-small',
            paddingRight: '5px',
            paddingLeft: '5px'
        },
    },
    strainInfo: {
        fontSize: 'small',
        whiteSpace: 'nowrap',
        [theme.breakpoints.down(1650)]: {
            fontSize: 'xx-small',
            //whiteSpace: 'normal'
        },
    },
    strainInfoLong: {
        fontSize: 'x-small',
        whiteSpace: 'nowrap',
        [theme.breakpoints.down(1650)]: {
            fontSize: 'xx-small',
            //whiteSpace: 'normal',
            paddingRight: '5px',
            paddingLeft: '5px'
        },
    },
    break: {
        wordBreak: 'break-word'
    },
    name: {
        padding: 3,
        marginLeft: theme.spacing(-2),
        marginRight: theme.spacing(-2),
        textAlign: 'center'
    },
    typoTitle: {
        fontWeight: 'bolder',
        textShadow: '0px 1px, 1px 0px, 1px 1px',
        letterSpacing: '2px'
    }
});

YeastCard.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
    item: PropTypes.shape({
        Name: PropTypes.string,
        salesCategory: PropTypes.number,
        alcoholTol: PropTypes.string,
        attenuation: PropTypes.string,
        flocculation: PropTypes.string,
        optFermentTempF: PropTypes.string,
        optFermentTempC: PropTypes.string,
        salesCategoryIconOverrideChecked: PropTypes.boolean,
        salesCategoryIconOverride: PropTypes.number,
    }),
};

export default YeastCard;
