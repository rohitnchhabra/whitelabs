import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import DialogContent from '@material-ui/core/DialogContent';

import ReactGA from 'react-ga';
//const Contivio = require('https://uschat3.com/uschat2/ContivioChatPlugin.js');

class ChatWindow extends React.Component {
    constructor(props) {
        super(props);

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/chat');
        this.state = {
            isChatting: false,
            chatLoaded: false,
            firstScroll: false,
            serviceID: 329,
            queueID: '31440',
            openWindowOptions: 'menubar=0,resizable=0,width=500,height=700,scroll=0'
        };

        this.SlideUp();
    }

    handleErrors = (field, err) => {
        let { errors } = this.state;
        errors[field] = err
        this.setState({ errors })
    }

    handleDialogClose() {
        this.props.closeDialog();
    }

    InitializeChat = (ifrm) => {
        if (!this.state.chatLoaded) {
            this.setState({ chatLoaded: true });
            ContivioSetStartChatImage('https://classic.yeastman.com/images/wl_lets_chat.jpg');
            ContivioSetLogoUrl('https://classic.yeastman.com/images/wl_chat_logo.jpg', 'center');
            ContivioSetWaitImageUrl('https://classic.yeastman.com/images/wl_chat_waiting.jpg', 'center');
            ContivioSetEndChatImageUrl('https://classic.yeastman.com/images/wl_chat_complete.jpg', 'center');
            ContivioSetStartChatMessage('Hi there! To help us better serve you, please enter your name and email address below, and tell us a little bit about what you would like to discuss.');
            ContivioSetWaitForResponseMessage('Thank you! A customer service representative will be with you shortly. Please stand by ...');
            ContivioSetRequestQueuedMessage('Thank you! A customer service representative will be with you shortly. Please stand by ...');
            ContivioSetTimeDisplayOption('HH:MM');
            ContivioSetRequestRejectedMessage('Sorry we are unable to take your message right now, our chat hours are 6AM-5PM PST Mon-Fri.');

            ContivioCustomizeCustomerNameField('Name', '', true);
            ContivioCustomizeCustomerEmailField('Email', '', true);
            ContivioCustomizeCustomerPhoneField('Phone', '', true);
            ContivioCustomizeChatQuestionField('Question', '', true);

            if (ifrm != null && ifrm != '') {
                if (this.state.openWindowOptions) {
                    ContivioSetChatFrameDimension('640', '400');
                    ContivioStartChat(this.state.serviceID, this.state.queueID, ifrm, this.state.openWindowOptions);
                } else {
                    ContivioStartChat(this.state.serviceID, this.state.queueID, ifrm);
                }
            } else {
                ContivioSetChatFrameDimension('640', '400');
                ContivioStartChat(this.state.serviceID, this.state.queueID, '', this.state.openWindowOptions);
            }

            localStorage.setItem('ContivioChatLoaded', true);
        }
    }

    SlideUp = () => {
        if (!this.props.chatMode || this.props.chatMode !== 'popup') {
            this.InitializeChat('ifrmChatContainer');
        } else {
            this.InitializeChat();
        }
        // Closing dialog immediately because embedded version of chat not working
        // and don't want to leave screen dimmed after the pop-up opens.
        this.props.closeDialog();
        this.setState({ firstScroll: true });
    }

    SlideDown = () => {
        this.setState({ firstScroll: false });
    }

    checkForChatLoading = () => {
        if (localStorage.getItem('ContivioChatLoaded')) {
            ContivioStartChat(this.state.serviceID, this.state.queueID, 'ifrmChatContainer');
        }
    }

    EndChat = () => {
        ContivioEndChat('ifrmChatContainer');
        this.SlideDown();
        localStorage.removeItem('ContivioChatLoaded');
    }

    render() {
        return (
            this.state.chatLoaded ?
                <React.Fragment>
                    <iframe id='ifrmChatContainer' style='display: block;' />
                </React.Fragment>
            : null
        );
    }
}

const styles = theme => ({
    card: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
        height: '100%',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    cardHover: {
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        })
    },
    info: {
        alignItems: 'center',
        padding: 5,
        backgroundColor: '#e4e4e4',
        textAlign: 'center'
    },
    quantity: {
        width: 50
    },
    hide: {
        display: 'none'
    },
    hoverBold: {
        '&:hover': {
            fontWeight: 'bolder',
            color: '#ff9933',
            cursor: 'pointer'
        }
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end'
    },
    button: {
        marginTop: theme.spacing(1),
        marginRight: theme.spacing(-5)
    },
    close: { position: 'absolute', right: 0, top: 0 },
    form: {
        width: '100%',
    }
});

ChatWindow.propTypes = {
    theme: PropTypes.object.isRequired
};

const mapStateToProps = state => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = dispatch =>
    bindActionCreators({}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(ChatWindow));
