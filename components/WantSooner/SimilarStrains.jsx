import React, { Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

import NavBarLayout from "components/NavBar/NavBarLayout";
import LoadingIndicator from "components/UI/LoadingIndicator";
import Card from "components/UI/Card/Card.jsx";
import CardBody from "components/UI/Card/CardBody.jsx";
import CardHeader from "components/UI/Card/CardHeader.jsx";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";

import OptionItem from "./OptionItem";
import FormCheckboxList from "components/Form/FormCheckboxList";
import { cartActions } from "appRedux/actions/cartActions";
import { inventoryActions } from "appRedux/actions/inventoryActions";
import Router from "next/router";
import _cloneDeep from "lodash/cloneDeep";
import ReactGA from 'react-ga';

class SimilarStrains extends React.Component {
    constructor(props) {
        super(props);

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/checkout-similar-strains');

        this.state = {
            options: [],
            multipleOptionsSelected: [],
            isChecked: false,
            isSelected:true
        };
    }

    callSimilarStrains = () => {
        const { item } = this.props;
        
        const { multipleOptionsSelected } = this.state;
        if(multipleOptionsSelected.length){
            this.setState({
                isSelected:true
            })
            this.props.getSimilarStrains({
                item,
                selectedStyles: multipleOptionsSelected
            });
        }else{
             this.setState({
                 isSelected:false
             })
        }
        
    };

    calculateGroupCost = options => {
        var sum = 0.0;
        if (options) {
            sum += options.pricePerUnit * options.OrderDetailQty;
        }
        return sum.toFixed(2);
    };

    exchangeItem = options => {
        const { index } = this.props;
        const cartItems = [];
        const cartItem = {};
        cartItem.Name = String(options.Name);
        cartItem.details = " Similar Strain";
        // cartItem.salesCategory = parseInt(option.salesCategory);
        cartItem.dispQuantity = parseInt(options.OrderDetailQty);
        cartItem.OrderDetailQty = cartItem.dispQuantity;
        cartItem.MerchandiseID = options.MerchandiseID;
        cartItem.type = 1;
        cartItems.push(cartItem);
        this.props.replaceItem({ cartItems, index });
        Router.push("/cart");
    };

    _renderItems = () => {
        const { similarStrains, cartIndex } = this.props.inventory;
        const { classes } = this.props;
        return _.map(similarStrains, (options, i) => {
            return (
                <Fragment>
                    <Grid key={i} container spacing={6}>
                        <Grid item xs={12} className="two-options-item-block">
                            <Grid container spacing={6}>
                                <div className="total-cost">Total Group Cost: {this.calculateGroupCost(options)}</div>
                                <Grid container spacing={6}>
                                    <Grid item xs={6} className="item-heading">
                                        <Typography style={{ marginTop: "23px" }} className="option-text">
                                            OPTION {i + 1}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={6} className="item-heading select-option">
                                        <div className={classes.buttons}>
                                            <Button type="submit" variant="contained" color="primary" onClick={() => this.exchangeItem(options)} className={classes.button}>
                                                <img src={"static/images/icons/addTocart.svg"} />
                                            </Button>
                                        </div>
                                    </Grid>
                                </Grid>
                                <Grid className="option-parent-block" item xs={12} md={6} style={{ margin: "auto" }}>
                                    <OptionItem
                                        option={options}
                                        hide
                                        // size={i}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Fragment>
            );
        });
    };

    componentDidMount() {
        const { items } = this.props.inventory;
        const { Name } = this.props.item;
        const cloneItems = _cloneDeep(items);
        var beers = cloneItems.filter((v, i) => {
            return v.Name === Name;
        });
        var beerStyles = beers && beers[0].beerStyles;
        var beerOptions = (!beerStyles ? {} : beerStyles.map((v, i) => {
            v.label = v["name"];
            v.value = v["id"];
            delete v.name;
            delete v.id;
            return v;
        }));
        this.setState({ options: beerOptions });
    }

    render() {
        const { classes } = this.props;
        const { similarStrains, cartIndex, isSimilarLoading } = this.props.inventory;
        let NoSimSize = similarStrains && similarStrains.every(ele => ele == null);

        return (
            <Card id="similar-strains">
                <CardHeader color="primary" className="card-header-down">
                    <Typography color="secondary" variant="display1" align="center">
                        Similar Strains
                    </Typography>
                </CardHeader>
                <CardBody>
                  
                    {isSimilarLoading !== null ? (
                        !isSimilarLoading ? (
                            NoSimSize && isSimilarLoading === false ? (
                                <Grid item xs={6}>
                                    <Typography className="content-text">Sorry! No Similar Strains available for this item.</Typography>
                                </Grid>
                            ) : null
                        ) : (
                            "Processing..."
                        )
                    ) : (
                        <Fragment>
                            <Grid container spacing={6}>
                                <Grid item xs={12}>
                                    <Grid container spacing={6}>
                                        <Grid item xs={12} md={6}>
                                            <Typography className={classes.typoText}>
                                                Select the style of beer that you are trying to make. The more styles you select the more options you will be offered.
                                                {!this.state.isSelected &&<div style={{color:"red"}}>* Select at least one option </div>}
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12} md={6} style={{ overflowY: "scroll", maxHeight: "350px", marginBottom: "20px" }}>
                                            <FormCheckboxList
                                                value={this.state.multipleOptionsSelected}
                                                options={this.state.options}
                                                onChange={e => {
                                                    this.setState({
                                                        multipleOptionsSelected: e.target.value.join(", ")
                                                    });
                                                }}
                                                className="style-of-beer"
                                            />
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid className={classes.alignEnd} style={{ display: "flex", justifyContent: "flex-end" }}>
                                <Grid item xs={6} className={classes.searchButtons}>
                                    <Button type="submit" variant="contained" color="primary" onClick={this.callSimilarStrains} className={classes.searchButton}>
                                        Search
                                    </Button>
                                </Grid>
                            </Grid>
                        </Fragment>
                    )}
                    <Fragment>
                        <LoadingIndicator visible={this.props.inventory.isSimilarLoading === true} label={"Getting Similar Strains"} />
                        {this._renderItems()}
                    </Fragment>
                </CardBody>
            </Card>
        );
    }
}

const mapStateToProps = state => {
    return {
        inventory: state.inventory
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions, ...inventoryActions }, dispatch);

const styles = theme => ({
    buttons: {
        display: "flex",
        justifyContent: "center",
        marginTop: "15px"
    },
    button: {
        borderRadius: "20px",
        padding: "0px"
    },
    typoText: {
        fontSize: "16px",
        fontWeight: "bolder"
    },
    alignEnd: {
        display: "flex",
        justifyContent: "flex-end",
        [theme.breakpoints.down("sm")]: {
            justifyContent: "center !important"
        }
    },
    searchButtons: {
        display: "flex",
        justifyContent: "center",
        marginTop: "0px",
        marginBottom: "17px",
        [theme.breakpoints.down("sm")]: {
            marginTop: "25px"
        }
    },
    searchButton: {
        borderRadius: "20px",
        padding: "10px 30px"
    }
});

SimilarStrains.propTypes = {};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(SimilarStrains));
