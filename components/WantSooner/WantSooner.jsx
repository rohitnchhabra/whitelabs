import React from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AlternateSizes from './AlternateSizes';
import SimilarStrains from './SimilarStrains';
import LoadingIndicator from 'components/UI/LoadingIndicator';
import ReactGA from 'react-ga';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { cartActions } from 'appRedux/actions/cartActions';
import { userActions } from 'appRedux/actions/userActions';
import { inventoryActions } from 'appRedux/actions/inventoryActions';

class WantSooner extends React.Component {
    constructor(props) {
        super(props);

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/want-sooner');
        this.state = {
            activeTab: 'AlternateSizes'
        };
    }

    wantSoonerTabSwitch = () => {
        if (this.state.activeTab === 'SimilarStrains') {
            this.setState({
                activeTab: 'AlternateSizes'
            });
        } else {
            this.setState({
                activeTab: 'SimilarStrains'
            });
        }
    };

    componentWillUnmount(){
    this.props.clearAlternateSizes();
    this.props.clearSimilarStrains();
    }

    render() {
        (this.state.activeTab == 'AlternateSizes' ? ReactGA.pageview('/checkout-alternate-sizes') : ReactGA.pageview('/checkout-similar-strains'));

        const { cart, index,item } = this.props;
        return (
            <div id="want-sooner-box">
                <div>
                    <AppBar position='static'>
                        <Tabs value={this.state.activeTab}>
                            <Tab
                                value="AlternateSizes"
                                onClick={this.wantSoonerTabSwitch}
                                label='ALTERNATE SIZES'
                            />
                            <Tab
                                value="SimilarStrains"
                                onClick={this.wantSoonerTabSwitch}
                                label='SIMILAR STRAINS'
                            />
                        </Tabs>
                    </AppBar>
                    {this.state.activeTab == 'AlternateSizes' ? (
                        <AlternateSizes
                            item={item}
                            index={index}
                        />
                    ) : (
                        <SimilarStrains
                         index={index}
                         item={item} />
                    )}
                </div>
            </div>
        );
    }
}

const styles = theme => ({});

WantSooner.propTypes = {};

const mapStateToProps = state => {
    return {
        cart: state.cart,
        user: state.user,
        inventory: state.inventory
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions, ...userActions, ...inventoryActions }, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(WantSooner));
