import React, { Fragment } from 'react'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import _ from 'lodash';

import NavBarLayout from "components/NavBar/NavBarLayout";
import LoadingIndicator from "components/UI/LoadingIndicator";
import Card from "components/UI/Card/Card.jsx";
import CardBody from "components/UI/Card/CardBody.jsx";
import CardHeader from "components/UI/Card/CardHeader.jsx";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import FormCheckbox from "components/Form/FormCheckbox";
import cloneDeep from "lodash/cloneDeep"
import OptionItem from "./OptionItem";
import { cartActions } from "appRedux/actions/cartActions";
import { inventoryActions } from "appRedux/actions/inventoryActions";
import Router from "next/router";
import ReactGA from 'react-ga';

class AlternateSizes extends React.Component {
    constructor(props) {
        super(props);

        ReactGA.initialize('UA-40641680-2');
        ReactGA.pageview('/checkout-alternate-sizes');

        this.state = {
            checkedValue: {},
        };
        const { item, index } = this.props;
        const { alternateSizes } = this.props.inventory;
        if (alternateSizes.length === 0) {
            this.props.getAlternateSizes({ item, cartIndex: index });
        }
    }

    exchangeItem = (options) => {
        this.setState({ checked: true });
        const { index } = this.props;
        const cartItems = [];
        options.forEach((option, size) => {
            if (option) {
                var details = '';
                switch (size) {
                    case 0:
                        details = 'Nano';
                        break;
                    case 1:
                        details = '1.5L';
                        break;
                    case 2:
                        details = '2L';
                        break;
                    default:
                        details = '';
                }
                const cartItem = {};
                cartItem.Name = String(option.Name);
                cartItem.details = details + ' alternate';
                cartItem.salesCategory = parseInt(option.salesCategory);
                cartItem.dispQuantity = parseInt(option.OrderDetailQty);
                cartItem.OrderDetailQty = parseInt(option.OrderDetailQty);
                cartItem.MerchandiseID = option.MerchandiseID;
                cartItem.type = 1;
                cartItems.push(cartItem);
            }
        })
        this.props.replaceItem({ cartItems, index });
        Router.push('/cart');
    }

    calculateGroupCost = (itemGroup) => {
        var sum = 0.0;
        itemGroup.forEach((item) => {
            if (item) {
                sum += item.pricePerUnit * item.OrderDetailQty;
            }
        });

        return sum.toFixed(2);
    }

    _renderItems = () => {
        const { alternateSizes, cartIndex } = this.props.inventory;
        const { classes } = this.props;
        return _.map(alternateSizes, (options, i) => {
            return (
                <Grid key={i} container spacing={6}>
                    <Grid item xs={12} className='two-options-item-block'>
                        <Grid container spacing={6}>
                            <div className='total-cost'>Total Group Cost: {this.calculateGroupCost(options)}</div>
                            <Grid container spacing={6}>
                                <Grid item xs={6} className='item-heading'>
                                    <Typography style={{ marginTop: '23px' }} className='option-text'>OPTION {i + 1}</Typography>
                                </Grid>
                                <Grid item xs={6} className='item-heading select-option'>
                                    <div className={classes.buttons}>
                                        <Button
                                            type='submit'
                                            variant='contained'
                                            color='primary'
                                            onClick={() => this.exchangeItem(options)}
                                            className={classes.button}
                                        >
                                            <img src={'static/images/icons/addTocart.svg'} />
                                        </Button>
                                    </div>
                                </Grid>
                            </Grid>
                            {_.map(options, (option, size) => {
                                if (option) {
                                    return (
                                        <Grid className='option-parent-block' item xs={12} md={6} style={{ margin: 'auto' }}>
                                            <OptionItem
                                                option={option}
                                                hide
                                                size={size}
                                            />
                                        </Grid>
                                    );
                                }
                            })}
                        </Grid>
                    </Grid>
                </Grid>
            );
        });
    };

    render() {
        const { isAlternateLoading, alternateSizes, cartIndex } = this.props.inventory;        
        let NoAltSize = alternateSizes && alternateSizes.every((ele) => ele == null)
        return (
            <Card id="alternate-sizes">
                <LoadingIndicator visible={isAlternateLoading} label={"Getting Alternate Sizes"} />
                <CardHeader color="primary" className="card-header-down">
                    <Typography color="secondary" variant="display1" align="center">
                        Alternate Sizes
                    </Typography>
                </CardHeader>
                <CardBody>
                    {!isAlternateLoading ?
                        NoAltSize ? <Grid item xs={6}>
                            <Typography className="content-text">Sorry! No alternate options available for this item.</Typography>
                        </Grid>
                            :
                            <Grid container spacing={6}>
                                <Grid item xs={12}>
                                    <Grid container spacing={6}>
                                        <Grid item xs={6}>
                                            <Typography className="content-text">Pick different sizes and get your order sooner. All the options below are in stock.</Typography>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Grid>
                        : "Processing..."}
                    {!NoAltSize ? <Fragment>{this._renderItems()}</Fragment> : null}
                </CardBody>
            </Card>
        );
    }
}

const mapStateToProps = state => {
    return {
        inventory: state.inventory
    };
};

const mapDispatchToProps = dispatch => bindActionCreators({ ...cartActions, ...inventoryActions }, dispatch);

const styles = theme => ({
    buttons: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: '15px'
    },
    button: {
        borderRadius: '20px',
        padding: '0px'
    }
});

AlternateSizes.propTypes = {};
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles, { withTheme: true })(AlternateSizes));
