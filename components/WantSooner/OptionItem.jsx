import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Card from '@material-ui/core/Card';
import CardBody from 'components/UI/Card/CardBody.jsx';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FormCheckbox from 'components/Form/FormCheckbox';
import Utils from '../../lib/Utils';

class OptionItem extends React.Component {
    state = {
        checked: false
    };

    indexSize = (size) => {
        switch (parseInt(size)) {
            case 0:
                return ' - Nano';
            case 1:
                return ' - 1.5L';
            case 2:
                return ' - 2L';
            default:
                return '';
        }
    }

    render() {
        const { option, size, exchangeItem } = this.props;
        return (
            <Card className='option-item-card'>
                <CardBody>
                    {!this.props.hide && <Grid container spacing={6}>
                        <Grid item xs={6} className='item-heading'>
                            <Typography className='option-text'>OPTION 6</Typography>
                        </Grid>
                        <Grid item xs={6} className='item-heading select-option'>
                            <FormCheckbox className='check-item' checked={this.state.checked} onChange={exchangeItem} />
                        </Grid>
                    </Grid>
                    }

                    <Grid container spacing={6}>
                        <Grid item xs={12} style={{ display: 'flex', justifyContent: 'center' }} className='item-name'>
                            {option.Name + this.indexSize(size)}
                        </Grid>
                    </Grid>

                    <Grid container spacing={6} className='summary-others'>
                        <Grid item xs={6}>
                            Quantity:
                        </Grid>
                        <Grid item xs={6} dir='rtl'>
                            {option.OrderDetailQty}
                        </Grid>
                    </Grid>
                    <Grid container spacing={6} className='summary-others'>
                        <Grid item xs={6}>
                            Price Per Quantity:
                        </Grid>
                        <Grid item xs={6} dir='rtl'>
                            {option.pricePerUnit}
                        </Grid>
                    </Grid>
                    <Grid container spacing={6} className='summary-others'>
                        <Grid item xs={6}>
                            Ship Date:
                        </Grid>
                        <Grid item xs={6} dir='rtl'>
                            {Utils.formatDate(option.chosenShipDate)}
                        </Grid>
                    </Grid>
                    <Grid container spacing={6} className='summary-others'>
                        <Grid item xs={6}>
                            Addressee
                        </Grid>
                        <Grid item xs={6} dir='rtl'>
                            {option.Warehouse}
                        </Grid>
                    </Grid>
                </CardBody>
            </Card>
        );
    }
}

export default OptionItem;
