import { requestWrapper } from './base';

export const getInventory = () => requestWrapper(`/inventory`);

export const getSimilarStrains = (params) => requestWrapper(`/similar-strains`, {
    method: "POST",
    body: JSON.stringify(params)
});

export const getAlternateSizes = (params) => requestWrapper(`/alternate-sizes`, {
    method: "POST",
    body: JSON.stringify(params)
});

export const getRelatedItems = (params) => requestWrapper(`/related-items`, {
    method: "POST",
    body: JSON.stringify(params)
});

export const getQcResults = (params) => requestWrapper(`/qc-results`, {
    method: "POST",
    body: JSON.stringify(params)
});