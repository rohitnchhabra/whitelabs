import { requestWrapper } from './base';


export const relatedItemsPost = (data) => requestWrapper('/related-items', {
    method: "POST",
    body: JSON.stringify(data)
});


export const itemAvailabilityPost = (data) => requestWrapper('/item-availability', {
    method: "POST",
    body: JSON.stringify(data)
});
