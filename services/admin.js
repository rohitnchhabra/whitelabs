import { requestWrapper } from './base';

export const getCustomer = (params) => requestWrapper('/request-customer-lookup', {
    method: "POST",
    body: JSON.stringify(params)
});
