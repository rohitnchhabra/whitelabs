function versionUpToDate(clientVersion) {

    const serverVersion = "2.3.7";

    if (clientVersion) {
        return clientVersion >= serverVersion;
    }
    else {
        return serverVersion;
    }
}

function AES(encrypt, key, value) {
    if (encrypt) {
        return CryptoJS.AES.encrypt(value, key);
    }
    else {
        return CryptoJS.AES.decrypt(value, key);
    }
}

function ReceiveMessage(input) {
    return JSON.parse(CryptoJS.AES.decrypt(input.data, '5TVDpAHPqLZSNY7EuLWJWDhLVaGBi862qlNp48ULcgaR6oDDH2hCLKdY92MA0pG', { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: "ocbEMVHyax6MTNxD" }).toString(CryptoJS.enc.Utf8));
}

function SendMessage(input) {
    var message = { data: CryptoJS.AES.encrypt(JSON.stringify(input), 'DHA5ZsROSYuY1jJXxOcHS4dKNODjgzJPnsMFIEumUpOizL6dj9GnU8QC2Ctd2x8', { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: "8vr5BxmawvE5mZo7" }).toString() };
    return message;
}

function decryptCard(cardToken) {
    if (!cardToken) {
        throw { message: 'no card provided', code: -1 };
    }

    try {
        return JSON.parse(CryptoJS.AES.decrypt(cardToken, 'AWJeBmcD9uEy27L2Zla0ZKTKUbgjRJKwxr6CaKBqKDNbHSKsISC6BH2EhUvNm9s', { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: "A216T8JtJANg7F7C" }).toString(CryptoJS.enc.Utf8));
    }
    catch (error) {
        throw { message: 'couldn\'t decrypt card', code: -1 };
    }
}

function getTermsFromSub(subsidiaryID) {
    switch (subsidiaryID) {
        case 2: //US
        case 7: //CPH
            return 10; //Credit Card

        case 5: //HK
            return 13; //Bank Transfer
    }
}

function getCurrencyFromSub(subsidiaryID, cphDomestic) {
    switch (subsidiaryID) {
        case 2: //US
            return 1; //Dollar

        case 5: //HK
            return 5; //HK Dollar

        case 7: //CPH
            if (cphDomestic) {
                return 6; //DKK
            }
            else {
                return 4; //EUR
            }
    }
}

function getShipMethodFromSub(subsidiaryID, countryCode) {
    switch (subsidiaryID) {
        case 2: //US
            if (countryCode == 'US') {
                return 2787; //Fedex Priority Overnight
            } else {
                return 2844; //Fedex International Priority
            }
        case 5: //HK
            return 13301; //Will Call

        case 7: //CPH
            if (countryCode == 'DK') {
                return 13299; //CPH Domestic
            }
            else {
                return 13300; //CPH Non-Domestic
            }
    }
}

function warehouseMap(subsidiaryID) {
    switch (subsidiaryID) {
        // USA
        case 2:
            return '9'; //SD Shipping
            //  return 11; //Ash Shipping
            break;
        // HK
        case 5:
            return '31'; //HK Shipping
            break;
        // CPH
        case 7:
            return '30'; //CPH Shipping
            break;
        default:
            return '9';
    }
}

function taxCodeLookup(countryID) {
    var EUCountries = "ATBEBGHRCZEEFIFRDEGRHUIEITLVLTLUMTNLPLPTROSKSIESSEGB";
    if (countryID == "DK") {
        return 13322;
    }
    else if (EUCountries.indexOf(countryID) % 2 == 0) {
        return 13324;
    }
    else {
        return 13323;
    }
}

function priceLevelDetermination(category) {
    if (category == 2) //retailer customer
    {
        return 7; //retailer price level
    }
    else {
        return 1; //msrp
    }
}

const USHOLIDAYS = [{ day: 1, month: 1 }, //new years day
{ day: 25, month: 12 }, //christmas
{ day: 4, month: 7 }, //4th of july
{ day: -1, month: 5, week: -1, dayofweek: 1 }, //memorial day
{ day: -1, month: 11, week: 4, dayofweek: 4 }, //thanksgiving
//{ day: -1, month: 11, week: 5, dayofweek: 5 }, //day after thanksgiving (not a non-shipping day in 2020)
{ day: -1, month: 9, week: 1, dayofweek: 1 } //labor day

//custom (2019)
//{ day: 22, month: 11 },
]


const HKHOLIDAYS = [{ day: 30, month: 3 },
{ day: 31, month: 3 },
{ day: 2, month: 4 },
{ day: 5, month: 4 },

//custom
{ day: 22, month: 11 },
{ day: 25, month: 12 }];

const CPHHOLIDAYS = [
    { day: 1, month: 1 },
    { day: 29, month: 3 },
    { day: 30, month: 3 },
    { day: 2, month: 4 },
    { day: 27, month: 4 },
    { day: 10, month: 5 },
    { day: 21, month: 5 },
    { day: 24, month: 12 },
    { day: 25, month: 12 },
    { day: 26, month: 12 },

    //custom
    { day: 22, month: 11 },

    //2019 holiday blackout per Mike
    //Excludes the standard days above
    { day: 18, month: 12 },
    { day: 19, month: 12 },
    { day: 20, month: 12 },
    { day: 21, month: 12 },
    { day: 22, month: 12 },
    { day: 23, month: 12 },
    { day: 27, month: 12 },
    { day: 28, month: 12 },
    { day: 29, month: 12 },
    { day: 30, month: 12 },
    { day: 31, month: 12 }
];

/*
* checkHoliday()
*
* Summary: checks against holidays defined at the top of SalesIntegration.js
*/
function checkHoliday(date, subsidiary) {
    var HOLIDAYS;
    if (subsidiary == 7 || subsidiary == 5) {
        // Per Mike, apply US holidays to CPH and HK
        if (subsidiary == 7) {
            HOLIDAYS = CPHHOLIDAYS;
        } else if (subsidiary == 5) {
            HOLIDAYS = HKHOLIDAYS;
        }
        HOLIDAYS = HOLIDAYS.concat(USHOLIDAYS);
    } else {
        HOLIDAYS = USHOLIDAYS;
    }

    var dateToCheck = new Date(date);

    if ((dateToCheck.getDay() < 1 || dateToCheck.getDay() > 3) && subsidiary != 2) {
        return true;
    } else {
        for (var i = HOLIDAYS.length - 1; i >= 0; i--) {
            if (HOLIDAYS[i].month - 1 == dateToCheck.getMonth()) {

                if (HOLIDAYS[i].day == -1) {
                    //using day of week
                    if (HOLIDAYS[i].dayofweek == dateToCheck.getDay()) {
                        if (HOLIDAYS[i].week < 0) {
                            //counting to end
                            var nextMonth = HOLIDAYS[i].month % 12;
                            var testDate = new Date(dateToCheck);
                            testDate.setDate(testDate.getDate() - (HOLIDAYS[i].week * 7));
                            if (testDate.getMonth() == nextMonth) {
                                return true;
                            }
                        }
                        else {
                            //counting from start
                            var prevMonth = (HOLIDAYS[i].month - 2) % 12;
                            var testDate = new Date(dateToCheck);
                            var testDate2 = new Date(dateToCheck);
                            testDate.setDate(testDate.getDate() - (HOLIDAYS[i].week * 7));
                            testDate2.setDate(testDate2.getDate() - ((HOLIDAYS[i].week - 1) * 7));
                            if (testDate.getMonth() == prevMonth && testDate2.getMonth() == prevMonth + 1) {
                                return true;
                            }
                        }
                    }

                }
                else {
                    //using date
                    if (HOLIDAYS[i].day == dateToCheck.getDate()) {
                        return true;
                    }
                }
            }
        }
    }

    return false;
}

function getShipDate(date, subsidiary, addTravelTime, travelOnMonday, isAsheville, isCustomPour) {
    var finalDate = new Date(date);
    var today = getLocalTime(subsidiary, isAsheville);

    // 2pm shipping cutoff
    // 6/27/19: There is now no longer a next-day cutoff for subsidiary 2, getHours() will never be > 23
    if ((today.getHours() > 23 && subsidiary == 2) || (today.getHours() > 12 && subsidiary != 2)) {
        today.setDate(today.getDate() + 2);
    }
    else {
        today.setDate(today.getDate() + 1);
    }

    if (compareDates(today, finalDate) == -1) {
        finalDate = new Date(today);
    }

    if (addTravelTime) {
        if (travelOnMonday) {
            if (finalDate.getDay() == 1)			// Monday, ship on following Monday
            {
                finalDate.setDate(finalDate.getDate() + 7);
            }
            else if (finalDate.getDay() == 0)	// Sunday, ship on following Monday
            {
                finalDate.setDate(finalDate.getDate() + 8);
            } else if (finalDate.getDay() == 2) // Tuesday, ship on following Monday
            {
                finalDate.setDate(finalDate.getDate() + 6);
            }
            else {
                finalDate.setDate(finalDate.getDate() + (8 - finalDate.getDay()) + 7);
            }
        }
        else {
            if (finalDate.getDay() == 5)			// Friday
            {
                finalDate.setDate(finalDate.getDate() + 7);
            }
            else if (finalDate.getDay() == 6)	// Saturday
            {
                finalDate.setDate(finalDate.getDate() + 13);
            }
            else {
                finalDate.setDate(finalDate.getDate() + (5 - finalDate.getDay()) + 7);
            }
        }
    }

    // HK
    if (subsidiary == 5) {
        if (finalDate.getDay() == 5)			// Friday
        {
            finalDate.setDate(finalDate.getDate() + 3);
        }
        else {
            finalDate.setDate(finalDate.getDate() + 1);
        }
    }

    if (isCustomPour) {
        finalDate.setDate(finalDate.getDate() + 1);
    }

    return valiDate(finalDate, subsidiary);
}

function getOrganicItemShipDate(date, subsidiary, addTravelTime, travelOnMonday, isAsheville, isCustomPour) {
    if (subsidiary != 2) {
        // Just return regular value
        return getShipDate(date, subsidiary, addTravelTime, travelOnMonday, isAsheville, isCustomPour);
    } else {
        //log.audit({ title: 'Current date/time', details: new Date() });
        var baseShipDate = unGetLocalTime(new Date(), subsidiary);

        // Per Mike, if ship date is before Thursday, item ships to USA the Monday immediately following, and ships to customer the Monday after that
        // If ship date is Thursday or later, item ships to USA the Monday AFTER the Monday immediately following, and ships to the customer the Monday after that
        //log.audit({ title: 'Base organic item ship date', details: baseShipDate });
        var addWeek = (baseShipDate.getDay() >= 4);
        if (baseShipDate.getDay() != 1) {
            baseShipDate.setDate(baseShipDate.getDate() + (8 - baseShipDate.getDay()) % 7);
        } else {
            //Per Mike, something that's ready Sunday or Monday can ship that same Monday from CPH to the US
            //baseShipDate.setDate(baseShipDate.getDate() + 7);
        }
        //log.audit({ title: 'Advanced to following Monday', details: baseShipDate });
        if (addWeek) baseShipDate.setDate(baseShipDate.getDate() + 7);
        //log.audit({ title: 'Leaves CPH on', details: baseShipDate });
        baseShipDate.setDate(baseShipDate.getDate() + 7);
        //log.audit({ title: 'Ship date for customer', details: baseShipDate });
        return valiDate(baseShipDate, subsidiary);
    }
}

function valiDate(date, subsidiary) {
    var date = new Date(date);
    var sunday = date.getDay() == 0;
    var saturday = date.getDay() == 6;
    var holiday = checkHoliday(date, subsidiary);

    if (saturday || sunday || holiday) {
        return valiDate(date.setDate(date.getDate() + 1), subsidiary);
    }
    else {
        return date;
    }

}

function getLocalTime(subsidiary, isAsheville) {
    var now = new Date();
    var date = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());

    switch (subsidiary) {
        case 2: //US
            if (isAsheville) {
                date.setHours(date.getHours() - 4);
            }
            else {
                date.setHours(date.getHours() - 7);
            }
            break;

        case 5: //HK
            date.setHours(date.getHours() + 8);
            break;

        case 7: //CPH
            date.setHours(date.getHours() + 2);
            break;
    }

    return date;
}

function unGetLocalTime(now, subsidiary) {
    var date = new Date(now);
    log.audit({ title: '"Now"', details: now });
    log.audit({ title: 'Incoming Ship Date', details: date });
    //log.audit({ title: '"Now" Hour, Day, Date, & Month', details: date.getHours() + ' ' + date.getDay() + ' ' + date.getDate() + '/' + date.getMonth() });

    // The time zone offset means early morning ship dates are interpreted as afternoon hours in here
    if (date.getHours() >= 17) {
        //Hour 12 will be interpreted as 8pm by NetSuite due to TZ offset and that should prevent it from adjusting the date back to the previous day
        //due to whatever it thinks it needs to do with time zones.
        //Okay setting hours to 12 seems to screw up evening date shifts
        //var ungotDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 12, 0, 0, 0);
      	var ungotDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), 0, 0, 0);

        log.audit({ title: 'Initial "Ungot" Ship Date', details: ungotDate });
        log.audit({ title: 'Initial "Ungot" Day, Date, & Month', details: ungotDate.getDay() + ' ' + ungotDate.getDate() + '/' + ungotDate.getMonth() });

        if (ungotDate.getDay() === 0) {
            //Adjust the date to get off of Sunday
            ungotDate.setDate(ungotDate.getDate() + 1);
            log.audit({ title: 'Adjust to get off Sunday', details: new Date(ungotDate)});
        } else if (ungotDate.getDay() < 5 && ungotDate.getDay() <= date.getDay()) {
            //NS seems to like to adjust us back a day for early-morning ship dates (we don't ship in the early morning but times still get added to ship dates during processing)
            //This is a problem even when it's not a Sunday since the ship date won't match what was shown to the customer.
            //Deliberately using "<=" here because NetSuite seems to have a discrepancy between the "get" values and the "set" values
            while (ungotDate.getDay() <= date.getDay()) {
                ungotDate.setDate(ungotDate.getDate() + 1);
                log.audit({ title: 'Adjusted to get back to the correct day of the week', details: new Date(ungotDate)});
            }
        } else if (ungotDate.getMonth() <= date.getMonth()) {
            //If the ship date was early morning at the end of the month, we'll be in the previous month and need to return to the proper month.
            //This could include getting bumped back from 1/1 to 12/31 of the previous year, although theoretically we should never have 1/1 as a ship date since it's a holiday everywhere.
            //Deliberately using "<=" here because NetSuite seems to have a discrepancy between the "get" values and the "set" values
            while (ungotDate.getMonth() < date.getMonth() || ungotDate.getMonth() == date.getMonth() && ungotDate.getDate() < date.getDate() || (ungotDate.getMonth() >= 11 && date.getMonth() === 0)) {
                //Adding days to avoid jumping from, say, 11/30 to 12/30 (we will only be 1 day off anyway so this doesn't cost us anything)
                ungotDate.setDate(ungotDate.getDate() + 1);
                log.audit({ title: 'Adjusting to get to the correct target date', details: new Date(ungotDate)});
            }
        }

        log.audit({ title: 'Final "Ungot" Ship Date', details: ungotDate });
        //log.audit({ title: 'Final "Ungot" Day, Date, & Month', details: ungotDate.getDay() + ' ' + ungotDate.getDate() + '/' + ungotDate.getMonth() });
        return valiDate(new Date(ungotDate), subsidiary);
    } else {
        // Return unchanged (but validated)
        return valiDate(date, subsidiary);
    }
}

function compareDates(date1, date2) {
    date1 = new Date(date1);
    date2 = new Date(date2);

    if (date1.getFullYear() != date2.getFullYear()) {
        if (date1.getFullYear() < date2.getFullYear()) {
            return 1;
        }
        else {
            return -1;
        }
    }
    else if (date1.getMonth() != date2.getMonth()) {
        if (date1.getMonth() < date2.getMonth()) {
            return 1;
        }
        else {
            return -1;
        }
    }
    else if (date1.getDate() != date2.getDate()) {
        if (date1.getDate() < date2.getDate()) {
            return 1;
        }
        else {
            return -1;
        }
    }

    return 0;
}

function searchAvailabilityResults(Availability, location) {
    log.audit({ title: location + ' Availability', details: Availability });

    for (var i = 0, totalLength = Availability.length; i < totalLength; i++) {
        if (Availability[i].inventoryLocation == String(location) && Availability[i].type == 'Avail Qty') {
            log.audit({ title: location + ' Selected Availability', details: Availability[i] });
            return Availability[i];
        }
    }

    log.audit({ title: location + ' Availability', details: 'Not available' });
    // item not available
    return [{
        "type": "Avail Qty",
        "action": "Add",
        "qty": "0",
        "availQty": 0,
        "availQtyToShip": 0,
        "dateValue": null,
        "inventoryLocation": null,
        "volume": 1,
        "item": null
    }]
}

//From https://netsuitehub.com/forums/topic/adding-modern-functionality-to-array-object-find-findindex-in-your-server-side-suitescripts/
//fix netsuite javascript engine
//adds modern array functionality
function fixNetSuiteJavaScript() {
    //https://tc39.github.io/ecma262/#sec-array.prototype.find
    if (!Array.prototype.find) {
        Object.defineProperty(Array.prototype, 'find', {
            value: function (predicate) {
                // 1. Let O be ? ToObject(this value).
                if (this == null) {
                    throw new TypeError('"this" is null or not defined');
                }

                var o = Object(this);

                // 2. Let len be ? ToLength(? Get(O, �length")).
                var len = o.length >>> 0;

                // 3. If IsCallable(predicate) is false, throw a TypeError exception.
                if (typeof predicate !== 'function') {
                    throw new TypeError('predicate must be a function');
                }

                // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
                var thisArg = arguments[1];

                // 5. Let k be 0.
                var k = 0;

                // 6. Repeat, while k < len 
                while (k < len) {
                    // a. Let Pk be ! ToString(k). 
                    // b. Let kValue be ? Get(O, Pk). 
                    // c. Let testResult be ToBoolean(? Call(predicate, T, � kValue, k, O �)). 
                    // d. If testResult is true, return kValue. 
                    var kValue = o[k]; 
                    if (predicate.call(thisArg, kValue, k, o)) {
                        return kValue;
                    }
                    // e. Increase k by 1.
                    k++;
                }
                // 7. Return undefined. 
                return undefined;
            }, configurable: true, writable: true
        });
    }

    // https://tc39.github.io/ecma262/#sec-array.prototype.findIndex
    if (!Array.prototype.findIndex) {
        Object.defineProperty(Array.prototype, 'findIndex', {
            value: function (predicate) {
                // 1. Let O be ? ToObject(this value). 
                if (this == null) {
                    throw new TypeError('"this" is null or not defined');
                }
                var o = Object(this);
                // 2. Let len be ? ToLength(? Get(O, "length")). 
                var len = o.length >>> 0;

                // 3. If IsCallable(predicate) is false, throw a TypeError exception.
                if (typeof predicate !== 'function') {
                    throw new TypeError('predicate must be a function');
                }

                // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
                var thisArg = arguments[1];

                // 5. Let k be 0.
                var k = 0;

                // 6. Repeat, while k < len 
                while (k < len) {
                    // a. Let Pk be ! ToString(k). 
                    // b. Let kValue be ? Get(O, Pk). 
                    // c. Let testResult be ToBoolean(? Call(predicate, T, � kValue, k, O �)). 
                    // d. If testResult is true, return k.
                    var kValue = o[k];
                    if (predicate.call(thisArg, kValue, k, o)) {
                        return k;
                    }
                    // e. Increase k by 1. 
                    k++;
                }
                // 7. Return -1. 
                return -1;
            }, configurable: true, writable: true
        });
    }
}