/**
 *@NApiVersion 2.x
 *@NScriptType Restlet
 *Author: Dimitri Vasilev
 */

/* ***************
   * Description *
   ***************
   This endpoint was established to perform the following tasks for the White Labs App:
   1 - get | Get Past Order/Order History
   2 - put | Get Order Dates/Pricing
   3 - post| Place Order
 */

//"./item-availability.js"

define(["N/record", "N/log", "N/search", "N/format", "../wl_ba_cs_item_availability.js", "../Dom Dev Library/aes", "../Dom Dev Library/enc-base64-min.js", "./YMO-LIB"], function (
    record,
    log,
    search,
    format,
    itemAvailability
) {
    function get(input) {
    }

    function put(input) {
        try {
            var response = ReceiveMessage(input);

            if (!response.transitTimes) {
                response = getTransitTimes(input);
            } else if (!response.itemsAdded && (!response.lastItemLoaded || response.lastItemLoaded > 1)) {
                response = getItems(input);
            } else if (!response.pricingAdded) {
                response.itemsAdded = true;
                response = getPricing(input);
                response.loadComplete = true;
            } else if (!response.loadComplete) {
                response.loadComplete = true;
            }

            if (response.loopCounter >= 10) {
                try {
                    if (response.user && response.user.id) {
                        log.audit({ title: 'Customer #' + response.user.id + ': Too many loops in response', details: JSON.stringify(response) });
                    } else {
                        log.audit({ title: 'Too many loops in response', details: JSON.stringify(response) });
                    }
                } catch (error) {
                    logError('record response loops', error);
                }
            }

            return SendMessage(response);
        } catch (error) {
            logError("prepare order", error);
            return { error: error };
        }
    }

    //Get Transit Times
    function getTransitTimes(input) {
        try {
            var response = ReceiveMessage(input);

            //Transit Times
            response.transitTimes = addTransitTimesAndRanges();
            return response;
        } catch (error) {
            logError("getTransitTimes", error);
            return { error: error };
        }
    }

    //Get Items
    function getItems(input) {
        try {
            var response = ReceiveMessage(input);

            const userRecord = record.load({ type: record.Type.CUSTOMER, id: response.user.id });
            const userLocation = parseInt(userRecord.getValue({ fieldId: "subsidiary" }));
            var userWarehouse = parseInt(warehouseMap(userLocation));
            const territory = userRecord.getValue({ fieldId: "territory" });

            const SDLocalTime = getLocalTime(2, false);
            const shipCountry = (response.user && response.user.shipToCountry ? response.user.shipToCountry.toUpperCase() : userRecord.getValue({ fieldId: "shipcountry" }));
            const splitOrder = userLocation == 2 && shipCountry != "US" ? false : true; // no split orders for internationals
            const hasPlates = orderContainsPlates(response.items);

            //Ship Date for hb items
            const hbDate = HomebrewShipDate(userLocation);

            var allowedWarehouses = [];

            if (userLocation == 2 && (response.user.category == 2 || response.user.category == 3)) {
                // Restrict non-pro customers to sourcing from San Diego, unless they selected to pick up in AVL
                if (response.user.shipmethod == 13332 || response.user.shipmethod == 3472) {
                    // AVL pickup method, only allow AVL warehouses
                    allowedWarehouses.push(11);
                    log.audit({ title: 'Customer #' + response.user.id + ' WH Restricted to 11', details: 'AVL Pickup Ship Method Selected' });
                } else {
                    // Only allow SD warehouses
                    allowedWarehouses.push(9);
                    log.audit({ title: 'Customer #' + response.user.id + ' WH Restricted to 9', details: 'Non-Pro Customer' });
                }
            } else if (response.user.shipmethod) {
                if (response.user.shipmethod == 3511 || response.user.shipmethod == 3469) {
                    // SD pickup method, only allow SD warehouses
                    allowedWarehouses.push(9);
                    log.audit({ title: 'Customer #' + response.user.id + ' WH Restricted to 9', details: 'SD Pickup Ship Method Selected' });
                } else if (response.user.shipmethod == 13332 || response.user.shipmethod == 3472) {
                    // AVL pickup method, only allow AVL warehouses
                    allowedWarehouses.push(11);
                    log.audit({ title: 'Customer #' + response.user.id + ' WH Restricted to 11', details: 'AVL Pickup Ship Method Selected' });
                } else if (response.user.shipmethod == 4 || response.user.shipmethod == 2791 || response.user.shipmethod == 3475) {
                    // Ground method, only allow the user's default warehouse
                    // Actually this is the CLOSEST warehouse, so if they are in AVL territory, make that the default
                    if (userWarehouse == 9) {
                        if (territory >= 3) {
                            userWarehouse = 11;
                            allowedWarehouses.push(-9); // This will indicate that non-yeast items can still come from the other warehouse
                        } else {
                            allowedWarehouses.push(-11); // This will indicate that non-yeast items can still come from the other warehouse
                        }
                    }
                    allowedWarehouses.push(userWarehouse);
                    log.audit({ title: 'Customer #' + response.user.id + ' WH Restricted to ' + userWarehouse, details: 'Ground Ship Method Selected' });
                }
            }

            if (allowedWarehouses.length == 0) {
                // No allowed warehouse restrictions, unless we are not splitting orders
                allowedWarehouses.push(9);  //SD
                if (splitOrder) {
                    allowedWarehouses.push(11); //AVL
                    allowedWarehouses.push(30); //CPH
                    allowedWarehouses.push(31); //HK
                }
            }

            if (!splitOrder && allowedWarehouses.length > 0) {
                // To prevent splits, remove everything except the primary warehouse
                allowedWarehouses = allowedWarehouses.slice(0, 1);
            }

            if (allowedWarehouses.indexOf(userWarehouse) < 0) {
                userWarehouse = allowedWarehouses[0];
            }

            log.audit({
                title: 'Customer #' + response.user.id + ', subsidiary ' + userLocation + ', warehouse ' + userWarehouse + ', allow splits ' + splitOrder
                    + ', allowed warehouses', details: allowedWarehouses
            });

            var startingItem = (response.lastItemLoaded ? response.lastItemLoaded : response.items.length);

            //Populate Ship dates
            for (var i = startingItem - 1; i >= 0; i--) {
                if (i < startingItem - 35) {
                    //logError("getItems", "Recycling to load more items");
                    break;
                } else {
                    response.lastItemLoaded = i + 1;
                }

                try {
                    response.items[i].AllWarehouses = [];
                    var thisItemDetails = response.items[i].Name + ' ' + response.items[i].details + ' x' + response.items[i].OrderDetailQty;
                    var wasItemAdded = false;

                    if (response.items[i].MerchandiseID == 4328) {
                        // Special gel pack handling
                        if (response.user.shipmethod == 17237) {
                            // Per Mike, gel packs don't fit in One Rate box
                            response.items.splice(i, 1);
                            wasItemAdded = false;
                        } else {
                            for (var j = 0; j < allowedWarehouses.length; j++) {
                                response.items[i].AllWarehouses.push(Math.abs(parseInt(allowedWarehouses[j])));
                            }
                            response.items[i].Warehouse = userWarehouse;

                            var shipDate = new Date(SDLocalTime);
                            response.items[i].shipDate = getShipDate(shipDate, userLocation, userLocation != 2, userLocation == 7, false, false);

                            wasItemAdded = true;
                        }
                    } else if (response.items[i].isVaultPreorderItem) {
                        // Special vault preorder item handling
                        response.items[i].Warehouse = 9;
                        response.items[i].shipDate = new Date(8640000000000000);

                        wasItemAdded = true;
                    } else if (response.items[i].salesCategory == 11 || response.items[i].isAlcoholItem) {
                        // Alcohol item
                        wasItemAdded = processAlcoholItem(response, i, allowedWarehouses, userWarehouse, userLocation, territory, SDLocalTime, hasPlates);
                    }
                    // Service items
                    else if (response.items[i].type == 4) {
                        wasItemAdded = processServiceItem(response, i, allowedWarehouses, hbDate, userLocation);
                    }

                    // Homebrew
                    else if (response.items[i].type == 2) {
                        wasItemAdded = processHomebrewItem(response, i, allowedWarehouses, hbDate, userLocation, territory, SDLocalTime);
                        if (!response.yeastItems) response.yeastItems = [];
                        response.yeastItems.push(response.items[i]);
                    }

                    // Nonyeast
                    else if (response.items[i].type == 3) {
                        if (response.items[i].salesCategory == 29 || response.items[i].salesCategory == 30) {
                            // Nutrients & Enzymes
                            wasItemAdded = processNutrientsAndEnzymes(response, i, allowedWarehouses, splitOrder, hasPlates, userWarehouse, userLocation, territory, SDLocalTime);
                        } else {
                            // Other non-yeast
                            wasItemAdded = processNonYeastItem(response, i, allowedWarehouses, userWarehouse, userLocation, territory, SDLocalTime, hasPlates);
                        }
                    }

                    // Purepitch
                    else {
                        wasItemAdded = processPurePitchItem(response, i, allowedWarehouses, splitOrder, hasPlates, userWarehouse, userLocation, territory, SDLocalTime);
                        if (!response.yeastItems) response.yeastItems = [];
                        response.yeastItems.push(response.items[i]);
                    }
                    response.lastItemLoaded = i;

                    if (!wasItemAdded) {
                        log.audit({ title: 'Unavailable Item Removed', details: thisItemDetails });
                    }
                } catch (error) {
                    try {
                        logError("getItems error on " + response.items[i].MerchandiseID, error);
                        response.items.splice(i, 1);
                    } catch (error1) {
                        // Do nothing
                    }
                }
            }

            if (response.lastItemLoaded <= 1) response.itemsAdded = true;
            return response;
        } catch (error) {
            logError("getItems abort", error);
            // Cannot prepare order
            response.itemsAdded = true;
            response.pricingAdded = true;
            response.loadComplete = true;
            return { error: error };
        }
    }

    function processAlcoholItem(response, i, allowedWarehouses, userWarehouse, userLocation, territory, SDLocalTime, hasPlates) {
        try {
            // Apply alcohol-specific rules prior to inventory rules
            if (!response.restrictedItemsRemoved) response.restrictedItemsRemoved = [];
            if (response.user.shipToState) {
                var alcoholWarehouses = null;

                switch (response.user.shipToState.toUpperCase()) {
                    case 'CA':
                        alcoholWarehouses = [[9], [11]];
                        break;
                    case 'NC':
                        alcoholWarehouses = [[11], [9]];
                        break;
                    default:
                        response.restrictedItemsRemoved.push(response.items[i]);
                        log.audit({ title: 'Invalid alcohol item destination', details: 'Ship to state: ' + response.user.shipToState });
                        response.items.splice(i, 1);
                        return false;
                }

                if (alcoholWarehouses) {
                    //Beer items must always allow both US warehouses, preferentially the one near the customer
                    log.audit({ title: 'Alcohol item destination OK', details: 'Ship to state: ' + response.user.shipToState });
                    var itemAdded = processNonYeastItem(response, i, alcoholWarehouses[0], alcoholWarehouses[0][0], userLocation, territory, SDLocalTime, hasPlates);
                    if (!itemAdded) itemAdded = processNonYeastItem(response, i, alcoholWarehouses[1], alcoholWarehouses[1][0], userLocation, territory, SDLocalTime, hasPlates);
                    return itemAdded;
                }
            } else {
                response.restrictedItemsRemoved.push(response.items[i]);
                log.audit({ title: 'Invalid alcohol item destination', details: 'Ship to state missing' });
                response.items.splice(i, 1);
                return false;
            }
        } catch (error) {
            logError("Process Alcohol Item", error);
            response.items.splice(i, 1);
            return false;
        }
    }

    function processServiceItem(response, i, allowedWarehouses, hbDate, userLocation) {
        var ServiceRecord = loadItem(response.items[i].type, response.items[i].MerchandiseID);

        if (ServiceRecord.getValue({ fieldId: "class" }) == 28) {
            var seatsLeft = parseInt(ServiceRecord.getValue({ fieldId: "custitem_class_seats_remaining" }));
            if (!isNaN(seatsLeft)) {
                if (seatsLeft <= response.items[i].OrderDetailQty) {
                    //no seats left
                    response.items.splice(i, 1);
                    return false;
                }
            }
        }

        var warehouses = ServiceRecord.getValue({ fieldId: "custitemwarehouse" });
        for (var j = 0; j < warehouses.length; j++) {
            if (allowedWarehouses.indexOf(parseInt(warehouses[j])) >= 0 || allowedWarehouses.indexOf(-1 * parseInt(warehouses[j])) >= 0) {
                response.items[i].AllWarehouses.push(parseInt(warehouses[j]));
            }
        }

        if (userLocation === 7 && response.items[i].AllWarehouses.indexOf(30) >= 0) {
            //cph
            response.items[i].Warehouse = 30;
        } else if (userLocation === 5 && response.items[i].AllWarehouses.indexOf(31) >= 0) {
            //HK
            response.items[i].Warehouse = 31;
        } else if (response.items[i].AllWarehouses.indexOf(9) >= 0) {
            //sd
            response.items[i].Warehouse = 9;
        } else if (userLocation !== 5 && userLocation !== 7 && response.items[i].AllWarehouses.indexOf(11) >= 0) {
            //avl
            response.items[i].Warehouse = 11;
        } else {
            response.items.splice(i, 1);
            return false;
        }

        var leadTime = parseInt(ServiceRecord.getValue({ fieldId: "custitem_wl_order_lead_time" }));
        if (!leadTime) leadTime = getItemLeadTime(response.items[i].MerchandiseID, leadTime);

        if (response.items[i].dateAvailable) {
            var serviceAvailableDate = response.items[i].dateAvailable;
            response.items[i].isBigQCDayItem = response.items[i].isBigQCDayItem;

            if (serviceAvailableDate) {
                response.items[i].isFixedShipDateItem = true;
                response.items[i].shipDate = new Date(serviceAvailableDate);

                if (response.items[i].shipDate < hbDate) response.items[i].isPastDeadline = true;
            } else {
                response.items[i].shipDate = valiDate(new Date(hbDate).setDate(hbDate.getDate() + leadTime), userLocation);
            }
        } else {
            response.items[i].shipDate = valiDate(new Date(hbDate).setDate(hbDate.getDate() + leadTime), userLocation);
        }

        return true;
    }

    function processHomebrewItem(response, i, allowedWarehouses, hbDate, userLocation, territory, SDLocalTime) {
        if (userLocation != 2) {
            response.items.splice(i, 1);
            return false;
        }

        var SDAvailability = null;
        try {
            SDAvailability = itemAvailability.GetItemAvailability([String(response.items[i].MerchandiseID)], ["9"], true);
        } catch (error) {
            logError("Get SD avail", error);
            SDAvailability = [];
        }

        var SDAvail = searchAvailabilityResults(SDAvailability, "9");

        if (SDAvail.availQtyToShip >= response.items[i].OrderDetailQty) {
            response.items[i].shipDate = valiDate(new Date(hbDate), userLocation);
            if (allowedWarehouses.indexOf(9) >= 0 || allowedWarehouses.indexOf(11) < 0) {
                response.items[i].Warehouse = 9;
            } else {
                // Customer picking up from Asheville
                response.items[i].Warehouse = 11;
            }
        } else if (response.user.shipToCountry && response.user.shipToCountry.toUpperCase() == 'US' && response.items[i].allowBackorder) {
            // Check work orders for out-of-stock HB items for US customers
            var itemRecord = loadItem(response.items[i].type, response.items[i].MerchandiseID);
            var leadTime = parseInt(itemRecord.getValue({ fieldId: "custitem_wl_order_lead_time" }));
            var additionalLeadTime = getItemLeadTime(response.items[i].MerchandiseID, 0);
            var allAvailability = { "9": SDAvailability };

            checkWorkOrders(response, i, userLocation, allAvailability, territory, allowedWarehouses, true, additionalLeadTime);

            if (!response.items[i].shipDate && leadTime) {
                response.items[i].Warehouse = 9; //Will be coming from San Diego
                var shipDate = new Date(SDLocalTime);
                response.items[i].shipDate = getShipDate(
                    shipDate.setDate(SDLocalTime.getDate() + (leadTime + additionalLeadTime)),
                    userLocation,
                    userLocation != 2,
                    userLocation == 7,
                    false,
                    false
                );
            } else {
                response.items.splice(i, 1);
                return false;
            }
        } else {
            response.items.splice(i, 1);
            return false;
        }

        return true;
    }

    function processNonYeastItem(response, i, allowedWarehouses, userWarehouse, userLocation, territory, SDLocalTime, hasPlates) {
        try {
            var itemRecord = loadItem(response.items[i].type, response.items[i].MerchandiseID);
            var warehouses = itemRecord.getValue({ fieldId: "custitemwarehouse" });

            for (var j = 0; j < warehouses.length; j++) {
                if (allowedWarehouses.indexOf(parseInt(warehouses[j])) >= 0 || allowedWarehouses.indexOf(-1 * parseInt(warehouses[j])) >= 0) {
                    response.items[i].AllWarehouses.push(parseInt(warehouses[j]));
                }
            }

            if (response.items[i].AllWarehouses.indexOf(userWarehouse) >= 0) {
                var shipDate = new Date(SDLocalTime);
                var leadTime = parseInt(itemRecord.getValue({ fieldId: "custitem_wl_order_lead_time" }));
                if (!leadTime) leadTime = getItemLeadTime(response.items[i].MerchandiseID, leadTime);

                var ASHAvailability = null;
                try {
                    ASHAvailability = itemAvailability.GetItemAvailability([String(response.items[i].MerchandiseID)], ["11"], true);
                } catch (error) {
                    logError("Get AVL avail", error);
                    ASHAvailability = [];
                }

                var SDAvailability = null;
                try {
                    SDAvailability = itemAvailability.GetItemAvailability([String(response.items[i].MerchandiseID)], ["9"], true);
                } catch (error) {
                    logError("Get SD avail", error);
                    SDAvailability = [];
                }

                var ASHAvail = searchAvailabilityResults(ASHAvailability, "11");
                var SDAvail = searchAvailabilityResults(SDAvailability, "9");

                // Always check Asheville for plates, and ONLY always for plates (not for EVERYTHING when even a single plate is ordered)
                if (hasPlates && orderContainsPlates([response.items[i]])) {
                    response.items[i].shipDate = getShipDate(shipDate.setDate(SDLocalTime.getDate() + leadTime), userLocation, userLocation != 2, userLocation == 7, false, false);
                    if ((allowedWarehouses.indexOf(11) >= 0 || allowedWarehouses.indexOf(-11) >= 0) || (allowedWarehouses.indexOf(9) < 0 || allowedWarehouses.indexOf(-9) < 0)) {
                        response.items[i].Warehouse = 11;
                    } else {
                        // Customer picking up in SD
                        response.items[i].Warehouse = 9;
                    }
                } else {
                    if (territory >= 3) {
                        if (response.items[i].OrderDetailQty <= parseInt(ASHAvail.availQtyToShip) && (allowedWarehouses.indexOf(11) >= 0 || allowedWarehouses.indexOf(-11) >= 0)) {
                            response.items[i].shipDate = getShipDate(shipDate.setDate(SDLocalTime.getDate() + leadTime), userLocation, userLocation != 2, userLocation == 7, false, false);
                            response.items[i].Warehouse = 11;
                        } else if (response.items[i].OrderDetailQty <= parseInt(SDAvail.availQtyToShip) && (allowedWarehouses.indexOf(9) >= 0 || allowedWarehouses.indexOf(-9) >= 0)) {
                            response.items[i].shipDate = getShipDate(shipDate.setDate(SDLocalTime.getDate() + leadTime), userLocation, userLocation != 2, userLocation == 7, false, false);
                            response.items[i].Warehouse = 9;
                        } else if (leadTime) {
                            response.items[i].shipDate = getShipDate(
                                shipDate.setDate(SDLocalTime.getDate() + leadTime),
                                userLocation,
                                userLocation != 2,
                                userLocation == 7,
                                false,
                                false
                            );
                            response.items[i].Warehouse = userWarehouse;
                        } else {
                            response.items.splice(i, 1);
                            return false;
                        }
                    } else {
                        if (response.items[i].OrderDetailQty <= parseInt(SDAvail.availQtyToShip) && (allowedWarehouses.indexOf(9) >= 0 || allowedWarehouses.indexOf(-9) >= 0)) {
                            response.items[i].shipDate = getShipDate(shipDate.setDate(SDLocalTime.getDate() + leadTime), userLocation, userLocation != 2, userLocation == 7, false, false);
                            response.items[i].Warehouse = 9;
                        } else if (response.items[i].OrderDetailQty <= parseInt(ASHAvail.availQtyToShip) && (allowedWarehouses.indexOf(11) >= 0 || allowedWarehouses.indexOf(-11) >= 0)) {
                            response.items[i].shipDate = getShipDate(shipDate.setDate(SDLocalTime.getDate() + leadTime), userLocation, userLocation != 2, userLocation == 7, false, false);
                            response.items[i].Warehouse = 11;
                        } else if (leadTime) {
                            response.items[i].shipDate = getShipDate(
                                shipDate.setDate(SDLocalTime.getDate() + leadTime),
                                userLocation,
                                userLocation != 2,
                                userLocation == 7,
                                false,
                                false
                            );
                            response.items[i].Warehouse = userWarehouse;
                        } else {
                            response.items.splice(i, 1);
                            return false;
                        }
                    }
                }
            } else {
                response.items.splice(i, 1);
                return false;
            }

            return true;
        } catch (error) {
            logError("Process Non Yeast Item", error);
            response.items.splice(i, 1);
            return false;
        }
    }

    function processNutrientsAndEnzymes(response, i, allowedWarehouses, splitOrder, hasPlates, userWarehouse, userLocation, territory, SDLocalTime) {
        try {
            var itemRecord = loadItem(response.items[i].type, response.items[i].MerchandiseID);
            var warehouses = itemRecord.getValue({ fieldId: "custitemwarehouse" });

            for (var j = 0; j < warehouses.length; j++) {
                if (allowedWarehouses.indexOf(parseInt(warehouses[j])) >= 0) {
                    response.items[i].AllWarehouses.push(parseInt(warehouses[j]));
                }
            }

            log.audit({ title: 'Item #' + response.items[i].MerchandiseID, details: 'Allowed warehouses: ' + response.items[i].AllWarehouses + ', User warehouse: ' + userWarehouse });

            if (response.items[i].AllWarehouses.indexOf(userWarehouse) >= 0) {
                var leadTime = parseInt(itemRecord.getValue({ fieldId: "custitem_wl_order_lead_time" }));
                var checkProduction = true;

                var allAvailability = {};
                // Nutrients & Enzymes are not PurePitch but if we say false here they get treated as custom pours in checkAvailabilityForLocation
                var isPurepitch = true; 
                var additionalLeadTime = 0;

                if (userLocation == 7) {
                    checkProduction = !checkAvailabilityForLocation(response, i, "30", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                } else if (userLocation == 5) {
                    checkProduction = !checkAvailabilityForLocation(response, i, "31", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                } else if (userLocation == 2) {
                    if (territory >= 3) {
                        if (splitOrder && allowedWarehouses.indexOf(11) >= 0) {
                            checkProduction = !checkAvailabilityForLocation(response, i, "11", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                            if (checkProduction && allowedWarehouses.indexOf(9) >= 0 && (!hasPlates || (hasPlates && !splitOrder))) {
                                checkProduction = !checkAvailabilityForLocation(response, i, "9", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                            }
                        } else {
                            checkProduction = !checkAvailabilityForLocation(response, i, "9", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                        }
                    } else {
                        if (allowedWarehouses.indexOf(9) >= 0 && (!hasPlates || (hasPlates && !splitOrder))) {
                            checkProduction = !checkAvailabilityForLocation(response, i, "9", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                            if (checkProduction && splitOrder && allowedWarehouses.indexOf(11) >= 0) {
                                checkProduction = !checkAvailabilityForLocation(response, i, "11", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                            }
                        } else if (splitOrder && allowedWarehouses.indexOf(11) >= 0) {
                            checkProduction = !checkAvailabilityForLocation(response, i, "11", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                        }
                    }
                } else {
                    response.items = null;
                    throw { message: "Invalid User Location", code: -1 };
                }

                // Check Production warehouses for Packaging WOs
                var usingTransitTime = false;

                if (checkProduction) {
                    usingTransitTime = checkWorkOrders(response, i, userLocation, allAvailability, territory, allowedWarehouses, isPurepitch, additionalLeadTime);
                }

                log.audit({ title: 'NE item #' + response.items[i].MerchandiseID + ' ship date', details: new Date(response.items[i].shipDate) });

                if ((!response.items[i].shipDate || usingTransitTime) && userLocation != 2) {
                    // If in CPH or HK and no ship date or sourcing via a work order, check for availability in the US
                    checkProduction = !checkAvailabilityForLocation(response, i, "9", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                    if (checkProduction) {
                        checkProduction = !checkAvailabilityForLocation(response, i, "11", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                    }

                    if (response.items[i].shipDate) {
                        var sd = getShipDate(new Date(response.items[i].shipDate), userLocation, true, (userLocation == 7), false, !isPurepitch);
                        sd.setDate(sd.getDate() + additionalLeadTime)
                        log.audit({ title: 'Ship date ' + new Date(response.items[i].shipDate) + ' with ' + additionalLeadTime + ' lead time', details: sd });
                        response.items[i].shipDate = sd;
                        response.items[i].Warehouse = (userLocation == 5 ? 31 : 30);
                    } else {
                        // Check work orders in the US
                        usingTransitTime = checkWorkOrders(response, i, userLocation, allAvailability, territory, [9, 11], isPurepitch, additionalLeadTime);
                    }
                }

                if (!response.items[i].shipDate) {
                    if (leadTime) {
                        var shipDate = new Date(SDLocalTime);
                        response.items[i].shipDate = getShipDate(
                            shipDate.setDate(SDLocalTime.getDate() + (leadTime + additionalLeadTime)),
                            userLocation,
                            userLocation != 2,
                            userLocation == 7,
                            false,
                            !isPurepitch
                        );
                        response.items[i].Warehouse = userWarehouse;
                    } else {
                        response.items.splice(i, 1);
                        return false;
                    }
                }
            } else {
                //isn't available to ship from given location
                response.items.splice(i, 1);
                return false;
            }

            return true;
        } catch (error) {
            response.items.splice(i, 1);
            logError("Process Pure Pitch Item", error);
            return false;
        }
    }

    function processPurePitchItem(response, i, allowedWarehouses, splitOrder, hasPlates, userWarehouse, userLocation, territory, SDLocalTime) {
        try {
            var itemRecord = loadItem(response.items[i].type, response.items[i].MerchandiseID);
            var warehouses = itemRecord.getValue({ fieldId: "custitemwarehouse" });

            for (var j = 0; j < warehouses.length; j++) {
                if (allowedWarehouses.indexOf(parseInt(warehouses[j])) >= 0) {
                    response.items[i].AllWarehouses.push(parseInt(warehouses[j]));
                }
            }

            log.audit({ title: 'Item #' + response.items[i].MerchandiseID, details: 'Allowed warehouses: ' + response.items[i].AllWarehouses + ', User warehouse: ' + userWarehouse });
            if (!response.items[i].isOrganic && response.items[i].Name.indexOf('-O') >= 0) response.items[i].isOrganic = true;

            if (response.items[i].AllWarehouses.indexOf(userWarehouse) >= 0 || (response.items[i].isOrganic && userLocation == 2) /*US customers can order organic items from CPH*/) {
                var leadTime = parseInt(itemRecord.getValue({ fieldId: "custitem_wl_order_lead_time" }));
                var additionalLeadTime = getItemLeadTime(response.items[i].MerchandiseID, 0);

                if (additionalLeadTime || leadTime) {
                    log.audit({ title: 'Lead time: ' + leadTime + ', Additional lead time: ' + additionalLeadTime });
                }

                if (response.items[i].OrderDetailQty >= 10.0 && response.items[i].type == 5) {
                    //large bbl custom pour
                    var shipDate = new Date(SDLocalTime);
                    leadTime = leadTime ? leadTime : 21;
                    response.items[i].shipDate = getShipDate(shipDate.setDate(SDLocalTime.getDate() + leadTime), userLocation, userLocation != 2, userLocation == 7, false, false);
                    response.items[i].Warehouse = userWarehouse;
                    return false;
                } else {
                    //Per Mike, use the actual field value, or 17 for cores, or 21 for vaults
                    if (!leadTime) {
                        if (itemRecord.getValue({ fieldId: "class" }) == 32) {
                            leadTime = 21;
                        } else {
                            leadTime = 17;
                        }
                    }
                    //leadTime = getItemLeadTime(response.items[i].MerchandiseID, leadTime);
                }

                var checkProduction = true;
                var isPurepitch = true;
                var packMethods = itemRecord.getText({ fieldId: "custitem_wl_packaging_methods" });
                if (packMethods == "Custom Pour") {
                    isPurepitch = false;
                }

                var allAvailability = {};

                if (userLocation == 7) {
                    checkProduction = !checkAvailabilityForLocation(response, i, "30", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                } else if (userLocation == 5) {
                    checkProduction = !checkAvailabilityForLocation(response, i, "31", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                } else if (userLocation == 2) {
                    if (response.items[i].isOrganic) {
                        // Organic items are always sourced from CPH, but we will pretend like we are shipping them from the US
                        checkProduction = !checkAvailabilityForLocation(response, i, "30", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                        if (!checkProduction) {
                            // We have it in stock in CPH but need to adjust the "ship-from" warehouse to the US or we'll get a subsidiary error
                            // Also, we need to adjust it according to the ship date rules for organic items
                            if (territory > 3) {
                                response.items[i].Warehouse = 11;
                            } else {
                                response.items[i].Warehouse = 9;
                            }
                            // Since the item is stock, don't use the calculated ship date, use the order date to determine the ship date
                            response.items[i].shipDate = getOrganicItemShipDate(SDLocalTime, userLocation, userLocation != 2, userLocation == 7,
                                response.items[i].Warehouse == 11, !isPurepitch, true);
                        }
                    } else if (territory >= 3) {
                        if (splitOrder && allowedWarehouses.indexOf(11) >= 0) {
                            checkProduction = !checkAvailabilityForLocation(response, i, "11", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                            if (checkProduction && allowedWarehouses.indexOf(9) >= 0 && (!hasPlates || (hasPlates && !splitOrder))) {
                                checkProduction = !checkAvailabilityForLocation(response, i, "9", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                            }
                        } else {
                            checkProduction = !checkAvailabilityForLocation(response, i, "9", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                        }
                    } else {
                        if (allowedWarehouses.indexOf(9) >= 0 && (!hasPlates || (hasPlates && !splitOrder))) {
                            checkProduction = !checkAvailabilityForLocation(response, i, "9", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                            if (checkProduction && splitOrder && allowedWarehouses.indexOf(11) >= 0) {
                                checkProduction = !checkAvailabilityForLocation(response, i, "11", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                            }
                        } else if (splitOrder && allowedWarehouses.indexOf(11) >= 0) {
                            checkProduction = !checkAvailabilityForLocation(response, i, "11", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                        }
                    }
                } else {
                    response.items = null;
                    throw { message: "Invalid User Location", code: -1 };
                }

                // Check Production warehouses for Packaging WOs
                var usingTransitTime = false;

                if (checkProduction) {
                    usingTransitTime = checkWorkOrders(response, i, userLocation, allAvailability, territory, allowedWarehouses, isPurepitch, additionalLeadTime);
                }

                if ((!response.items[i].shipDate || usingTransitTime) && userLocation != 2) {
                    // If in CPH or HK and no ship date or sourcing via a work order, check for availability in the US
                    checkProduction = !checkAvailabilityForLocation(response, i, "9", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                    if (checkProduction) {
                        checkProduction = !checkAvailabilityForLocation(response, i, "11", userLocation, isPurepitch, additionalLeadTime, allAvailability);
                    }

                    if (response.items[i].shipDate) {
                        var sd = getShipDate(new Date(response.items[i].shipDate), userLocation, true, (userLocation == 7), false, !isPurepitch);
                        sd.setDate(sd.getDate() + additionalLeadTime)
                        log.audit({ title: 'Ship date ' + new Date(response.items[i].shipDate) + ' with ' + additionalLeadTime + ' lead time', details: sd });
                        response.items[i].shipDate = sd;
                        response.items[i].Warehouse = (userLocation == 5 ? 31 : 30);
                    } else {
                        // Check work orders in the US
                        usingTransitTime = checkWorkOrders(response, i, userLocation, allAvailability, territory, [9, 11], isPurepitch, additionalLeadTime);
                    }
                }

                if (!response.items[i].shipDate) {
                    if (leadTime) {
                        var shipDate = new Date(SDLocalTime);
                        if (userLocation == 7 && (response.items[i].isOrganic || (warehouses.length == 1 && warehouses[0] == 30))) {
                            // Organic item produced in CPH or item only produced in CPH, no transit times
                            response.items[i].shipDate = getShipDate(
                                shipDate.setDate(SDLocalTime.getDate() + (leadTime + additionalLeadTime)),
                                userLocation,
                                false,
                                false,
                                false,
                                !isPurepitch
                            );
                        } else {
                            if (response.items[i].isOrganic) {
                                response.items[i].shipDate = getOrganicItemShipDate(
                                    shipDate.setDate(SDLocalTime.getDate() + (leadTime + additionalLeadTime)),
                                    userLocation,
                                    userLocation != 2,
                                    userLocation == 7,
                                    false,
                                    !isPurepitch,
                                    false
                                );
                            } else {
                                response.items[i].shipDate = getShipDate(
                                    shipDate.setDate(SDLocalTime.getDate() + (leadTime + additionalLeadTime)),
                                    userLocation,
                                    userLocation != 2,
                                    userLocation == 7,
                                    false,
                                    !isPurepitch
                                );
                            }
                        }
                        response.items[i].Warehouse = userWarehouse;
                    } else {
                        response.items.splice(i, 1);
                        return false;
                    }
                }
            } else {
                //isn't available to ship from given location
                response.items.splice(i, 1);
                return false;
            }

            return true;
        } catch (error) {
            response.items.splice(i, 1);
            logError("Process Pure Pitch Item", error);
            return false;
        }
    }

    function checkAvailabilityForLocation(response, i, locationId, userLocation, isPurepitch, additionalLeadTime, allAvailability) {
        try {
            var availability = itemAvailability.GetItemAvailability([String(response.items[i].MerchandiseID)], [locationId], true);
            var avail = searchAvailabilityResults(availability, locationId);

            var enoughAvailable = false;

            if (parseInt(avail.availQtyToShip) >= response.items[i].OrderDetailQty) {
                enoughAvailable = true;
                log.audit({ title: 'Item #' + response.items[i].MerchandiseID + ' avail date', details: new Date(avail.dateValue) });
                response.items[i].shipDate = getShipDate(new Date(avail.dateValue), userLocation, false, true, false, !isPurepitch);
                log.audit({ title: 'Item #' + response.items[i].MerchandiseID + ' ship date', details: new Date(response.items[i].shipDate) });
                response.items[i].shipDate = response.items[i].shipDate.setDate(response.items[i].shipDate.getDate() + additionalLeadTime);
                response.items[i].Warehouse = parseInt(locationId);
            }

            if (allAvailability) {
                allAvailability[locationId] = availability;
            }

            return enoughAvailable;
        } catch (error) {
            logError('checkAvailabilityForLocation', error);
            return false;
        }
    }

    function checkWorkOrders(response, i, userLocation, allAvailability, territory, allowedWarehouses, isPurepitch, additionalLeadTime) {
        try {
            var packagingWOs = [];
            var usingTransitTime = false;

            if (userLocation != 2 || response.items[i].isOrganic) {
                // Preferentially get work orders from the subsidiary location
                // Organic items are only sourced from CPH
                if (userLocation == 7 || response.items[i].isOrganic) {
                    if (allAvailability["30"]) {
                        putShipDatesIntoOrder(allAvailability["30"], "34", packagingWOs); // CPH Production warehouse
                    }
                } else if (userLocation == 5) {
                    if (allAvailability["31"]) {
                        putShipDatesIntoOrder(allAvailability["31"], "31", packagingWOs); // Hong Kong (no production warehouse)
                    }
                }
            }

            // If US subsidiary and on East coast, check Asheville first
            if (!response.items[i].isOrganic) {
                if (territory > 3 && userLocation == 2) {
                    if (allowedWarehouses.indexOf(11) >= 0 && allAvailability["11"]) putShipDatesIntoOrder(allAvailability["11"], "10", packagingWOs); // Asheville Production warehouse
                    if (allowedWarehouses.indexOf(9) >= 0 && allAvailability["9"]) putShipDatesIntoOrder(allAvailability["9"], "8", packagingWOs); // San Diego Production warehouse
                } else {
                    if (allowedWarehouses.indexOf(9) >= 0 && allAvailability["9"]) putShipDatesIntoOrder(allAvailability["9"], "8", packagingWOs); // San Diego Production warehouse
                    if (allowedWarehouses.indexOf(11) >= 0 && allAvailability["11"]) putShipDatesIntoOrder(allAvailability["11"], "10", packagingWOs); // Asheville Production warehouse
                }
            }

            if (packagingWOs && packagingWOs.length) {
                for (var j = 0; j < packagingWOs.length; j++) {
                    if (response.items[i].OrderDetailQty <= parseInt(packagingWOs[j].availQtyToShip)) {
                        log.audit({ title: 'Selected Packaging WO', details: packagingWOs[j] });

                        usingTransitTime = (
                            (userLocation == 5 && packagingWOs[j].inventoryLocation != "31")
                            || (userLocation == 7 && packagingWOs[j].inventoryLocation != "30" && packagingWOs[j].inventoryLocation != "34")
                            || (userLocation != 5 && response.items[i].isOrganic)
                        );

                        if (response.items[i].isOrganic) {
                            response.items[i].shipDate = getOrganicItemShipDate(
                                new Date(packagingWOs[j].dateValue),
                                userLocation,
                                usingTransitTime,
                                userLocation == 7,
                                false,
                                !isPurepitch,
                                false
                            );
                        } else {
                            response.items[i].shipDate = getShipDate(
                                new Date(packagingWOs[j].dateValue),
                                userLocation,
                                usingTransitTime,
                                userLocation == 7,
                                false,
                                !isPurepitch
                            );
                        }

                        response.items[i].shipDate = response.items[i].shipDate.setDate(response.items[i].shipDate.getDate() + additionalLeadTime);

                        // Use the appropriate shipping warehouse for the customer
                        response.items[i].Warehouse = (
                            (userLocation == 7 ? 30 :
                                (userLocation == 5 ? 31 :
                                    (packagingWOs[j].inventoryLocation == 10 ? 11 :
                                        (packagingWOs[j].inventoryLocation == 8 ? 9 :
                                            (response.items[i].isOrganic ? (territory > 3 ? 11 : 9) : parseInt(packagingWOs[j].inventoryLocation))
                                        )
                                    )
                                )
                            )
                        );

                        break;
                    }
                }
            }

            return usingTransitTime;
        } catch (error) {
            logError("Check Work Orders", error);
            return false;
        }
    }

    //Get Pricing
    function getPricing(input) {
        try {
            var response = ReceiveMessage(input);

            //PRICING
            response.itemSubtotal = 0;
            response.shippingSubtotal = 0;
            response.orderSubtotal = 0;
            response.couponDiscount = 0;
            response.couponPercent = 0;

            var fakeOrder = record.create({ type: "salesorder", isDynamic: true });
            fakeOrder.setValue("entity", response.user.id);

            if (response.user.shipmethod && response.user.shipmethod != -3) {
                fakeOrder.setValue({ fieldId: "shipmethod", value: response.user.shipmethod });
            }

            if (response.couponCode) {
                log.audit({
                    title: "Looking for coupon code",
                    details: response.couponCode
                });

                try {
                    var couponId = getCouponId(response.couponCode.toString().toUpperCase());

                    if (couponId != 0) {
                        try {
                            //These lines are for SuitePromotions environment
                            fakeOrder.selectNewLine({ sublistId: "promotions" });
                            fakeOrder.setCurrentSublistValue({ sublistId: "promotions", fieldId: "promocode", value: couponId });
                            fakeOrder.commitLine({ sublistId: "promotions" });
                        } catch (error) {
                            var errorText = error.code ? JSON.stringify(error.code) : error.toString();
                            logError('apply coupon: ' + response.couponCode, errorText);
                            response.couponError = "An error occurred applying coupon " + response.couponCode + ". Please contact White Labs for assistance.";
                        }
                    } else {
                        log.audit({
                            title: "Coupon code not found",
                            details: response.couponCode
                        });

                        response.couponError = "Coupon code " + response.couponCode + " not found or cannot be applied.";
                    }
                } catch (error) {
                    var errorText = error.code ? JSON.stringify(error.code) : error.toString();
                    logError('apply coupon: ' + response.couponCode, errorText)
                    response.couponError = "An error occurred applying coupon " + response.couponCode + ". Please contact White Labs for assistance.";
                }
            } else {
                log.audit({
                    title: "No coupon code for order",
                    details: "N/A"
                });
            }

            // Add items to fake order
            for (var i = 0; i < response.items.length; i++) {
                try {
                    fakeOrder.selectNewLine({ sublistId: "item" });
                    fakeOrder.setCurrentSublistValue({ sublistId: "item", fieldId: "item", value: response.items[i].MerchandiseID });
                    fakeOrder.setCurrentSublistValue({ sublistId: "item", fieldId: "quantity", value: response.items[i].OrderDetailQty });
                    fakeOrder.setCurrentSublistValue({ sublistId: "item", fieldId: "location", value: response.items[i].Warehouse });
                    fakeOrder.commitLine({ sublistId: "item" });
                } catch (error) {
                    var errorText = error.code ? JSON.stringify(error.code) : error.toString();
                    log.error({
                        title: "getPricing for item " + response.items[i].MerchandiseID + " in warehouse " + response.items[i].Warehouse,
                        details: errorText
                    });
                    log.audit({ title: 'Unavailable Item Removed', details: response.items[i].Name + ' x' + response.items[i].OrderDetailQty });
                    // Skip this item and move on to the next one
                    response.items.splice(i, 1);
                    i -= 1;
                }
            }

            // Get item prices
            var itemTotal = 0;
            var taxTotal = 0;
            var lineCount = fakeOrder.getLineCount({ sublistId: "item" });

            // For saving the tax rate of taxable items
            // (we don't seem to be able to get this info from the taxability fields on the fake order)
            var orderTaxRate = 0;
            var crvTotal = 0;
            var crvItems = getCRVItems();
            const crvFee = 0.05;

            for (var i = 0; i < response.items.length; i++) {
                // Also check for SD Will Call methods
                if ((response.user.shipmethod && [3469, 3511].indexOf(parseInt(response.user.shipmethod)) >= 0)
                || (response.user.shipToState && response.user.shipToState.toUpperCase() == 'CA')) {
                    // Hack for CRV testing
                    response.user.shipToState = 'CA';
                    var itemCrv = crvItems[response.items[i].MerchandiseID];
                    if (itemCrv && !isNaN(itemCrv)) {
                        response.items[i].crvFee = (crvFee * itemCrv).toFixed(2);
                        response.items[i].crvAmt = (parseFloat(response.items[i].crvFee) * response.items[i].OrderDetailQty).toFixed(2);
                        response.items[i].crvType = response.user.shipToState.toUpperCase() + ' Redemption Value';
                        crvTotal += parseFloat(response.items[i].crvAmt);
                    }
                }	

                for (var j = 0; j < lineCount; j++) {
                    fakeOrder.selectLine({ sublistId: "item", line: j });

                    if (fakeOrder.getCurrentSublistValue({ sublistId: "item", fieldId: "item" }) == response.items[i].MerchandiseID) {
                        response.items[i].pricePerUnit = fakeOrder.getCurrentSublistValue({ sublistId: "item", fieldId: "rate" });
                        itemTotal += (response.items[i].pricePerUnit * response.items[i].OrderDetailQty);

                        response.items[i].couponDiscount = 0;
                        // Look for a coupon in the next line
                        if (/*response.couponCode &&*/ j < lineCount - 1) {
                            fakeOrder.selectLine({ sublistId: "item", line: j + 1 });
                            if (fakeOrder.getCurrentSublistValue({ sublistId: "item", fieldId: "rate" }) < 0) {
                                response.items[i].couponDiscount = fakeOrder.getCurrentSublistValue({ sublistId: "item", fieldId: "amount" });

                                log.audit({
                                    title: "Discount for item " + response.items[i].Name,
                                    details: response.items[i].couponDiscount
                                });
                            }
                        }
                        // Go back to the current line
                        fakeOrder.selectLine({ sublistId: "item", line: j });

                        response.items[i].taxRate = fakeOrder.getCurrentSublistValue({ sublistId: "item", fieldId: "taxrate1" });
                        if (response.items[i].taxRate && (((response.items[i].pricePerUnit * response.items[i].OrderDetailQty) + response.items[i].couponDiscount) > 0)) {
                            response.items[i].taxAmt = (((response.items[i].pricePerUnit * response.items[i].OrderDetailQty) + response.items[i].couponDiscount) * (response.items[i].taxRate / 100)).toFixed(2);
                            taxTotal += parseFloat(response.items[i].taxAmt);
                        }
                        if (orderTaxRate == 0) orderTaxRate = response.items[i].taxRate;

                        break;
                    }
                }
            }

            response.itemSubtotal += fakeOrder.getValue({ fieldId: "subtotal" });
            response.crvTotal = crvTotal;
            response.totalTax = taxTotal;

            // Get coupon discount, if applicable
            if (response.itemSubtotal > 0 && !response.couponError) {
                var couponCount = fakeOrder.getLineCount({ sublistId: "promotions" });
                if (couponCount == 0 && response.couponCode) {
                    response.couponError = "No qualifying items found for coupon " + response.couponCode + " or coupon not valid.";
                } else {
                    fakeOrder.selectLine({ sublistId: "promotions", line: 0 });

                    if (response.couponCode && fakeOrder.getCurrentSublistValue({ sublistId: "promotions", fieldId: "promotionisvalid" }) != "T") {
                        response.couponError = "Coupon code " + response.couponCode + " not found or not valid.";
                    } else if (itemTotal > response.itemSubtotal) {
                        response.couponDiscount = Math.abs(response.itemSubtotal - itemTotal).toFixed(2);
                        response.couponPercent = response.couponDiscount / response.itemSubtotal;
                    } else {
                        response.couponDiscount = Math.abs(fakeOrder.getCurrentSublistValue({ sublistId: "promotions", fieldId: "purchasediscount" }));
                        if (!response.couponDiscount) {
                            //This seems to still be okay, just indicates that the discount is at the order level
                        } else {
                            response.itemSubtotal -= response.couponDiscount;
                        }
                    }
                }
            }

            log.audit({
                title: "Coupon discount",
                details: response.couponDiscount
            });

            if (response.user.shipmethod && response.user.shipmethod != -3) {
                response.shippingSubtotal += fakeOrder.getValue({ fieldId: "shippingcost" });

                if (response.shippingSubtotal > 0 && orderTaxRate > 0) {
                    response.shippingTax = (response.shippingSubtotal * (orderTaxRate / 100)).toFixed(2);
                    response.shippingSubtotal += parseFloat(response.shippingTax);
                }
                response.orderSubtotal += fakeOrder.getValue({ fieldId: "total" });
            } else {
                response.shippingSubtotal = 0.0;
                response.orderSubtotal = response.itemSubtotal;
            }
            //It looks like the total tax is already included in the total
            //if (response.totalTax) response.orderSubtotal += response.totalTax;
            response.taxRate = orderTaxRate;

            response.pricingAdded = true;
            return response;
        } catch (error) {
            logError("getPricing", error);
            return { error: error };
        }
    }

    function getCRVItems() {
        var resultSet = search.create({
            type: "item",
            filters:
                [
                    ["isinactive", "is", "F"],
                    "AND",
                    [["custitemiqfshopfloorcategory", "anyof", "17"], "OR", ["custitem_wl_shopfloorcategorykits", "anyof", "17"]],
                    "AND",
                    ["custitem_wl_packaging_methods", "anyof", "5"]
                ],
            columns:
                [
                    search.createColumn({ name: "internalid" }),
                    search.createColumn({ name: "itemid" }),
                    search.createColumn({ name: "custitem_wl_batch_size" })
                ]
        }).run();
        var results = resultSet.getRange({ start: 0, end: 1000 });

        var crvItems = {};
        results.forEach(function (itemSearch) {
            var crvItemId = parseInt(itemSearch.getValue('internalid'));
            crvItems[crvItemId] = itemSearch.getValue('custitem_wl_batch_size');
        });

        return crvItems;
    }   

    //Place Order
    function post(input) {
        var message = ReceiveMessage(input);

        if (message.get) {
            try {
                if (message.logCheckoutView) {
                    //Logging
                    logCheckoutView(message);
                } else if (message.admin) {
                    //CSR Order History
                    getCsrOrderHistory(message);
                } else if (message.admin == false) {
                    //Regular User Order History
                    getCustomerOrderHistory(message);
                } else {
                    //Get Past Order
                    getOrderHistoryDetail(message);
                }

                return SendMessage(message);
            } catch (error) {
                logError("Order: Post", error);
                return { error: error };
            }
        } else {
            // Create new Sales Order
            return createNewSalesOrder(message);
        }
    }

    function logCheckoutView(request) {
        var customer = request.customerName;
        var shipMethod = request.shipMethod;
        var items = request.presentedItems;
        var presentationId = request.presentationId;
        var presentationDate = request.presentationDate;
        var shipOption = request.shippingOption;

        if (items && items.length > 0) {
            log.audit({
                title: "Checkout ID: " + presentationId + ", " + customer + ",  " + shipOption + " " + shipMethod,
                details: presentationDate
            });

            for (var i = 0; i < items.length; i++) {
                log.audit({
                    title: presentationId + " #" + (i + 1) + " x" + items[i].qty + " " + items[i].Name,
                    details: items[i].warehouseId + ": " + items[i].shipDate
                });
            }
        }
    }

    function createNewSalesOrder(message) {
        try {
            var response = { orderNum: [], NSOrder: [] };

            var shipaddressindex;
            var today = new Date();
            var customerRecord = record.load({ type: record.Type.CUSTOMER, id: message.user.id });
            var userLocation = parseInt(customerRecord.getValue({ fieldId: "subsidiary" }));

            var WillCall = null;
            if (
                [3470, 3472, 13332, 3469, 3511].indexOf(parseInt(message.user.shipmethod)) >= 0 ||
                (!message.user.shipmethod && [3470, 3472, 13332, 3469, 3511].indexOf(parseInt(customerRecord.getValue({ fieldId: "shippingitem" }))) >= 0)
            ) {
                if (message.user.shipmethod) {
                    WillCall = parseInt(message.user.shipmethod);
                } else {
                    WillCall = parseInt(customerRecord.getValue({ fieldId: "shippingitem" }));
                }
            }

            var shipAddressIndex = customerRecord.findSublistLineWithValue({ sublistId: "addressbook", fieldId: "id", value: String(message.user.shipping.id) });
            var billAddressIndex = customerRecord.findSublistLineWithValue({ sublistId: "addressbook", fieldId: "id", value: String(message.user.billing.id) });

            // Make sure everything has a ship date and that the ship date isn't today
            for (var i = 0; i < message.order.items.length; i++) {
                if (!message.order.items[i].shipDate) {
                    throw { message: "Invalid/missing ship date on item " + message.order.items[i].MerchandiseID, code: -1 };
                } else if (
                    new Date(message.order.items[i].shipDate).getFullYear() === today.getFullYear() &&
                    new Date(message.order.items[i].shipDate).getMonth() === today.getMonth() &&
                    new Date(message.order.items[i].shipDate).getDate() === today.getDate()
                ) {
                    // Ignore this for gel pack items
                    if (message.order.items[i].MerchandiseID != 4328) {
                        throw { message: "Ship date same as current date on item " + message.order.items[i].MerchandiseID, code: -1 };
                    } else {
                        message.order.items[i].isSameDayGelPack = true;
                        log.audit({ title: 'Customer #' + message.user.id, details: 'Got same-day Gel Pack item' });
                    }
                }
            }

            for (var i = 0; i < message.order.items.length; i++) {
                if (!message.order.items[i].done && message.order.items[i].OrderDetailQty > 0) {
                    // Initialize new sales order object
                    var salesOrderRecord = record.create({ type: record.Type.SALES_ORDER, isDynamic: true });

                    if (message.order.cardId && message.order.terms && message.order.terms == 10) {
                        salesOrderRecord.setValue({ fieldId: "customform", value: 102 });
                        salesOrderRecord.setValue({ fieldId: "entity", value: message.user.id });
                        salesOrderRecord.setValue({ fieldId: "creditcard", value: message.order.cardId });
                        salesOrderRecord.setValue({ fieldId: "terms", value: message.order.terms });

                        log.audit({ title: 'Customer #' + message.user.id + ', Use Credit Card ID', details: message.order.cardId });
                    } else {
                        salesOrderRecord.setValue({ fieldId: "customform", value: 101 });
                        salesOrderRecord.setValue({ fieldId: "entity", value: message.user.id });

                        if (message.order.terms) {
                            salesOrderRecord.setValue({ fieldId: "terms", value: message.order.terms });
                            log.audit({ title: 'Customer #' + message.user.id + ', Use Terms', details: message.order.terms });
                        } else {
                            if (parseInt(customerRecord.getValue({ fieldId: "subsidiary" })) != 2 &&
                                (!customerRecord.getValue({ fieldId: "terms" }) || customerRecord.getValue({ fieldId: "terms" }) == 10)) {
                                salesOrderRecord.setValue({ fieldId: "terms", value: 13 }); //Bank transfer required for non US
                                log.audit({ title: 'Customer #' + message.user.id + ', Use Terms', details: '13' });
                            } else {
                                log.audit({ title: 'Customer #' + message.user.id + ', Use Terms', details: 'Default' });
                            }
                        }
                    }

                    salesOrderRecord.setValue({ fieldId: "trandate", value: today });

                    if (message.order.getItSooner) {
                        salesOrderRecord.setValue({ fieldId: "custbody_wl_get_it_sooner", value: true });
                    }

                    if (shipAddressIndex >= 0) {
                        salesOrderRecord.setValue({ fieldId: "shipaddresslist", value: String(message.user.shipping.id) });
                    } else {
                        throw { message: "No shipping address provided. Cannot place order", code: 0 };
                    }

                    if (billAddressIndex >= 0) {
                        salesOrderRecord.setValue({ fieldId: "billaddresslist", value: String(message.user.billing.id) });
                    } else {
                        throw { message: "No billing address provided. Cannot place order", code: 0 };
                    }

                    salesOrderRecord.setValue({ fieldId: "custbody_wl_ymo_synced", value: true });

                    var isCouponOrder = false;
                    if (message.order.couponCode) {
                        try {
                            var couponId = getCouponId(message.order.couponCode.toString().toUpperCase());
                            if (couponId != 0) {
                                //line below is for non-SuitePromotions environment
                                //salesOrderRecord.setValue({ fieldId: "promocode", value: couponId });

                                //These lines are for SuitePromotions environment
                                salesOrderRecord.selectNewLine({ sublistId: "promotions" });
                                salesOrderRecord.setCurrentSublistValue({ sublistId: "promotions", fieldId: "promocode", value: couponId });
                                salesOrderRecord.commitLine({ sublistId: "promotions" });
                                //isCouponOrder = true;
                            }
                        } catch (err) {
                            logError('place-order', err);
                            if (!message.comment) {
                                message.comment = "Coupon " + message.order.couponCode + " used but could not be applied";
                            } else {
                                message.comment += "|Coupon " + message.order.couponCode + " used but could not be applied";
                            }
                        }
                    }

                    if (message.comment) {
                        salesOrderRecord.setValue({ fieldId: "memo", value: message.comment.substring(0, 997) });
                    }

                    if (message.order.PONum) {
                        salesOrderRecord.setValue({ fieldId: "otherrefnum", value: message.order.PONum });
                    }

                    if (message.order.optEmail) {
                        salesOrderRecord.setValue({ fieldId: "tobeemailed", value: true });
                        salesOrderRecord.setValue({ fieldId: "email", value: message.order.optEmail });
                    }

                    if (message.user.shipmethod != -3) {
                        salesOrderRecord.setValue({ fieldId: "shipmethod", value: message.user.shipmethod });
                    }

                    var orderShipDate;
                    var nsShipDate;
                    if (!message.order.items[i].isVaultPreorderItem) {
                        if (message.order.items[i].shipDate) {
                            log.audit({ title: 'Customer #' + message.user.id + ', Item ' + message.order.items[i].Name, details: 'Ship date from YMO2: ' + message.order.items[i].shipDate });
                            orderShipDate = new Date(message.order.items[i].shipDate);
                        } else {
                            log.audit({ title: 'Customer #' + message.user.id + ', Item ' + message.order.items[i].Name, details: 'Ship date from YMO2 missing, using ' + new Date() });
                            orderShipDate = new Date();
                        }
                        nsShipDate = new Date(unGetLocalTime(orderShipDate, userLocation));
                        log.audit({ title: 'Item ' + message.order.items[i].Name, details: 'Ship date: ' + nsShipDate });
                        salesOrderRecord.setValue({ fieldId: 'shipdate', value: nsShipDate });
                    } else {
                        orderShipDate = null;
                        nsShipDate = null;
                        salesOrderRecord.setValue({ fieldId: 'shipdate', value: null });
                    }
                    log.audit({ title: 'Customer #' + message.user.id + ', Original Ship Date', details: message.order.items[i].shipDate });
                    log.audit({ title: 'Customer #' + message.user.id + ', Adjusted Ship Date', details: nsShipDate });

                    if (WillCall) {
                        setWillCallAddress(WillCall, salesOrderRecord, salesOrderRecord.getText({ fieldId: "entity" }));
                    } else if (shipaddressindex >= 0) {
                        salesOrderRecord.setValue({ fieldId: "shipaddresslist", value: shipaddressindex });
                    }

                    // Preliminarily put the order in the location of the first item.
                    // If this is Asheville and other items come from San Diego, this
                    // will be changed to San Diego in the items loop.
                    var orderLocation = message.order.items[i].Warehouse;
                    salesOrderRecord.setValue({ fieldId: 'location', value: orderLocation });

                    var orderLines = [];

                    salesOrderRecord.selectNewLine({ sublistId: "item" });
                    salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "item", value: message.order.items[i].MerchandiseID });
                    salesOrderRecord.setCurrentSublistValue({ sublistId: 'item', fieldId: 'price', value: -1 });
                    salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "quantity", value: message.order.items[i].OrderDetailQty });
                    salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "location", value: message.order.items[i].Warehouse });
                    if (!message.order.items[i].couponDiscount) {
                        salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "rate", value: message.order.items[i].pricePerUnit });
                        salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "amount", value: (message.order.items[i].pricePerUnit * message.order.items[i].OrderDetailQty) });
                    } else {
                        salesOrderRecord.setCurrentSublistValue({
                            sublistId: "item", fieldId: "amount", value: (message.order.items[i].pricePerUnit * message.order.items[i].OrderDetailQty)
                                - Math.abs(message.order.items[i].couponDiscount)
                        });
                        salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "description", value: message.order.items[i].Name + ' (' + message.order.couponCode + ')' });
                    }
                    salesOrderRecord.commitLine({ sublistId: "item" });

                    orderLines.push(message.order.items[i]);

                    for (var j = i + 1; j < message.order.items.length; j++) {
                        if (message.order.items[j].OrderDetailQty > 0) {
                            var itemShipDate;
                            var addItemToOrder = false;

                            if (!message.order.items[i].isVaultPreorderItem && !message.order.items[j].isVaultPreorderItem) {
                                switch (message.order.items[j].MerchandiseID) {
                                    case 4662:
                                    case 9471:
                                    case 9472:
                                    case 16366:
                                    case 16365:
                                    case 16846:
                                        itemShipDate = new Date(message.order.items[j].shipDate);
                                        break;
                                    default:
                                        itemShipDate = new Date(message.order.items[j].shipDate);
                                        break;
                                }

                                if (isCouponOrder) {
                                    // Per discussion with Mike, do not split coupon orders by ship date
                                    // At the moment, this is never true.
                                    addItemToOrder = true;
                                } else {
                                    itemShipDate = new Date(unGetLocalTime(itemShipDate, userLocation));
                                    log.audit({ title: 'Looking to add item ' + message.order.items[j].Name + ' to order ship date ' + new Date(nsShipDate), details: 'Item ship date: ' + itemShipDate });
                                    addItemToOrder = (compareDates(new Date(nsShipDate), new Date(itemShipDate)) == 0);
                                    if (!addItemToOrder) {
                                        var adjustedShipDate = unGetLocalTime(itemShipDate, userLocation);
                                        log.audit({
                                            title: 'Try again: Looking to add item ' + message.order.items[j].Name + ' to order ship date ' + new Date(nsShipDate),
                                            details: 'Adjusted ship date: ' + itemShipDate
                                        });
                                        addItemToOrder = (compareDates(new Date(nsShipDate), new Date(adjustedShipDate)) == 0);
                                    }

                                    //Temporary hack for gel packs
                                    //if (!addItemToOrder && message.order.items[j].MerchandiseID == 4328) addItemToOrder = true;
                                }
                            } else {
                                log.audit({ title: 'Looking to add item to order with vault preorder item ' + message.order.items[i].Name, details: 'This item: ' + message.order.items[j].Name });
                                if (message.order.items[i].isVaultPreorderItem && message.order.items[j].isVaultPreorderItem
                                    && (message.order.items[i].Name == message.order.items[j].Name)) {
                                    addItemToOrder = true;
                                }
                            }

                            log.audit({ title: 'Can I add ' + message.order.items[j].Name + ' to order?', details: addItemToOrder });

                            if (addItemToOrder) {
                                salesOrderRecord.selectNewLine({ sublistId: "item" });
                                salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "item", value: message.order.items[j].MerchandiseID });
                                salesOrderRecord.setCurrentSublistValue({ sublistId: 'item', fieldId: 'price', value: -1 });
                                salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "quantity", value: message.order.items[j].OrderDetailQty });
                                salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "location", value: message.order.items[j].Warehouse });
                                if (!message.order.items[j].couponDiscount) {
                                    salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "rate", value: message.order.items[j].pricePerUnit });
                                    salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "amount", value: (message.order.items[j].pricePerUnit * message.order.items[j].OrderDetailQty) });
                                } else {
                                    salesOrderRecord.setCurrentSublistValue({
                                        sublistId: "item", fieldId: "amount", value: (message.order.items[j].pricePerUnit * message.order.items[j].OrderDetailQty)
                                            - Math.abs(message.order.items[j].couponDiscount)
                                    });
                                    salesOrderRecord.setCurrentSublistValue({ sublistId: "item", fieldId: "description", value: message.order.items[j].Name + ' (' + message.order.couponCode + ')' });
                                }
                                salesOrderRecord.commitLine({ sublistId: "item" });
                                message.order.items[j].done = true;

                                orderLines.push(message.order.items[j]);

                                if (message.order.items[j].Warehouse != orderLocation && orderLocation == 11) {
                                    // Move order to San Diego
                                    orderLocation = message.order.items[j].Warehouse;
                                    salesOrderRecord.setValue({ fieldId: 'location', value: orderLocation });
                                }
                            }
                        }
                    }

                    if (message.salesrep) {
                        salesOrderRecord.setValue({ fieldId: "salesrep", value: message.salesrep });
                    }

                    var id = salesOrderRecord.save({ enableSourcing: true });

                    if (id != null && id != 0) {
                        //Need to reload order to get the actual transaction ID
                        addPlacedOrderDetails(response, id, orderLines);
                    } else {
                        throw { message: "failed to submit order", code: -1 };
                    }
                }
            }
            return SendMessage(response);
        } catch (error) {
            logError("post order for customer: " + message.user.id, error);
            return { error: error };
        }
    }

    function addPlacedOrderDetails(response, id, orderLines) {
        var nsSalesOrder = record.load({ type: record.Type.SALES_ORDER, id: id });
        var orderId = nsSalesOrder.getValue({ fieldId: "tranid" });

        response.orderNum.push(parseInt(orderId));

        //Don't send every field in the response to YMO2
        var ymo2SalesOrder = {};
        ymo2SalesOrder.id = id;
        ymo2SalesOrder.type = "salesorder";
        ymo2SalesOrder.fields = {};
        ymo2SalesOrder.fields.tranid = orderId;

        ymo2SalesOrder.fields.terms = nsSalesOrder.getValue({ fieldId: "terms" });
        ymo2SalesOrder.fields.billaddress = nsSalesOrder.getValue({ fieldId: "billaddress" });
        ymo2SalesOrder.fields.createddate = nsSalesOrder.getValue({ fieldId: "createddate" });
        ymo2SalesOrder.fields.companyid = nsSalesOrder.getValue({ fieldId: "companyid" });
        ymo2SalesOrder.fields.currencysymbol = nsSalesOrder.getValue({ fieldId: "currencysymbol" });
        ymo2SalesOrder.fields.custbody_expected_delivery_date = nsSalesOrder.getValue({ fieldId: "custbody_expected_delivery_date" });
        ymo2SalesOrder.fields.email = nsSalesOrder.getValue({ fieldId: "email" });
        ymo2SalesOrder.fields.entityname = nsSalesOrder.getValue({ fieldId: "entityname" });
        ymo2SalesOrder.fields.item_total = nsSalesOrder.getValue({ fieldId: "item_total" });
        ymo2SalesOrder.fields.memo = nsSalesOrder.getValue({ fieldId: "memo" });
        ymo2SalesOrder.fields.shipaddress = nsSalesOrder.getValue({ fieldId: "shipaddress" });
        ymo2SalesOrder.fields.shipmethod = nsSalesOrder.getValue({ fieldId: "shipmethod" });
        ymo2SalesOrder.fields.shippingcost = nsSalesOrder.getValue({ fieldId: "shippingcost" });
        ymo2SalesOrder.fields.status = nsSalesOrder.getValue({ fieldId: "status" });
        ymo2SalesOrder.fields.subtotal = nsSalesOrder.getValue({ fieldId: "subtotal" });
        ymo2SalesOrder.fields.total = nsSalesOrder.getValue({ fieldId: "total" });
        ymo2SalesOrder.fields.trandate = nsSalesOrder.getValue({ fieldId: "trandate" });
        ymo2SalesOrder.fields.type = nsSalesOrder.getValue({ fieldId: "type" });

        ymo2SalesOrder.sublists = {};
        var item = {};
        item.lines = orderLines;
        ymo2SalesOrder.sublists.item = item;

        response.NSOrder.push(ymo2SalesOrder);
    }

    function getCsrOrderHistory(message) {
        message.orderHistory = [];

        var filters = [];
        filters.push(search.createFilter({ name: "salesrep", operator: search.Operator.ANYOF, values: message.id }));
        filters.push(search.createFilter({ name: "type", operator: search.Operator.ANYOF, values: "SalesOrd" }));
        filters.push(search.createFilter({ name: "mainline", operator: search.Operator.IS, values: true }));
        filters.push(search.createFilter({ name: "datecreated", operator: search.Operator.NOTBEFORE, values: "startOfLastMonth" }));

        var columns = [];
        columns.push(search.createColumn({ name: "total" }));
        columns.push(search.createColumn({ name: "tranid" }));
        columns.push(search.createColumn({ name: "shipdate" }));
        columns.push(search.createColumn({ name: "custbody_expected_delivery_date" }));
        columns.push(search.createColumn({ name: "datecreated" }));
        columns.push(search.createColumn({ name: "statusref" }));
        columns.push(search.createColumn({ name: "entity" }));
        columns.push(search.createColumn({ name: "currency" }));

        var resultSet = search.create({ type: "transaction", filters: filters, columns: columns }).run();
        var index = 0;
        var results = null;

        do {
            results = resultSet.getRange({ start: index, end: index + 1000 });
            index += 1000;

            for (var i = 0; i < results.length; i++) {
                var result = results[i];
                var order = {};
                order.id = parseInt(result.id);
                order.orderNum = result.getValue({ name: "tranid" });
                order.totalPrice = result.getValue({ name: "total" });
                order.orderDate = result.getValue({ name: "datecreated" });
                order.status = result.getText({ name: "statusref" });
                order.companyName = result.getText({ name: "entity" });
                order.deliverydate = result.getValue({ name: "custbody_expected_delivery_date" });
                order.shipdate = result.getValue({ name: "shipdate" });
                order.currency = result.getValue({ name: "currency" });

                message.orderHistory.unshift(order);
            }
        } while (results.length == 1000)
    }

    function getCustomerOrderHistory(message) {
        message.orderHistory = [];

        var filters = [];

        filters.push(search.createFilter({ name: "entity", operator: search.Operator.ANYOF, values: message.id }));
        filters.push(search.createFilter({ name: "type", operator: search.Operator.ANYOF, values: "SalesOrd" }));
        //If we filter on this, we don't get the CRV
        //filters.push(search.createFilter({ name: "custitem_include_in_ymo_website", join: "item", operator: search.Operator.IS, values: true }));

        var columns = [];
        columns.push(search.createColumn({ name: "entity" }));
        columns.push(search.createColumn({ name: "total" }));
        columns.push(search.createColumn({ name: "item" }));
        columns.push(search.createColumn({ name: "quantity" }));
        columns.push(search.createColumn({ name: "rate" }));
        columns.push(search.createColumn({ name: "tranid" }));
        columns.push(search.createColumn({ name: "billaddress" }));
        columns.push(search.createColumn({ name: "shipaddress" }));
        columns.push(search.createColumn({ name: "shipdate" }));
        columns.push(search.createColumn({ name: "custbody_expected_delivery_date" }));
        columns.push(search.createColumn({ name: "datecreated" }));
        columns.push(search.createColumn({ name: "statusref" }));
        columns.push(search.createColumn({ name: "subsidiary" }));
        columns.push(search.createColumn({ name: "shipmethod" }));
        columns.push(search.createColumn({ name: "shippingamount" }));
        columns.push(search.createColumn({ name: "trackingnumbers" }));
        columns.push(search.createColumn({ name: "currency" }));
        columns.push(search.createColumn({ name: "otherrefnum" }));
        columns.push(search.createColumn({ name: "taxTotal" }));

        var orders = {};
        var resultSet = search.create({ type: "transaction", filters: filters, columns: columns }).run();
        var index = 0;
        var results = null;

        do {
            results = resultSet.getRange({ start: index, end: index + 1000 });
            index += 1000;

            for (var i = 0; i < results.length; i++) {
                var result = results[i];
                var orderID = parseInt(result.id);

                if (!orders[orderID]) {
                    orders[orderID] = {};
                    orders[orderID].id = parseInt(orderID);
                    orders[orderID].companyName = result.getText({ name: "entity" });
                    orders[orderID].items = [];
                    orders[orderID].shipdate = result.getValue({ name: "shipdate" });
                    orders[orderID].totalPrice = result.getValue({ name: "total" });
                    orders[orderID].billaddress = result.getValue({ name: "billaddress" });
                    orders[orderID].shipaddress = result.getValue({ name: "shipaddress" });
                    orders[orderID].orderNum = result.getValue({ name: "tranid" });
                    orders[orderID].deliverydate = result.getValue({ name: "custbody_expected_delivery_date" });
                    orders[orderID].orderDate = result.getValue({ name: "datecreated" });
                    orders[orderID].status = result.getText({ name: "statusref" });
                    orders[orderID].trackingNumber = result.getValue({ name: "trackingnumbers" }) ? result.getValue({ name: "trackingnumbers" }) : "N/A";
                    orders[orderID].currency = result.getValue({ name: "currency" });
                    orders[orderID].ponumber = result.getValue({ name: "otherrefnum" });

                    orders[orderID].subsidiary = result.getValue({ name: "subsidiary" });

                    orders[orderID].shipmethod = result.getText({ name: "shipmethod" });
                    orders[orderID].shipTotal = result.getValue({ name: "shippingamount" });
                    orders[orderID].taxTotal = result.getValue({ name: "taxTotal" });
                }

                var item = {};

                item.id = parseInt(result.getValue({ name: "item" }));
                if (item.id && item.id > 0) {
                    // See if this is the shipping item
                    if (item.id == parseInt(result.getValue({ name: "shipmethod" }))) continue;

                    item.name = result.getText({ name: "item" });
                    item.quantity = parseFloat(result.getValue({ name: "quantity" }));
                    item.price = parseFloat(result.getValue({ name: "rate" }));
                    item.shipDate = result.getValue({ name: "expectedshipdate" });

                    orders[orderID].items.push(item);
                }
            }
        } while (results.length == 1000)

        var keys = Object.keys(orders);

        for (var i = keys.length - 1; i >= 0; i--) {
            message.orderHistory.push(orders[keys[i]]);
        }
    }

    function getOrderHistoryDetail(message) {
        var salesOrderRecord = record.load({ type: record.Type.SALES_ORDER, id: message.id });

        message.items = [];
        message.companyName = salesOrderRecord.getText({ fieldId: "entity" });
        message.shipdate = salesOrderRecord.getValue({ fieldId: "shipdate" });
        message.totalPrice = salesOrderRecord.getValue({ fieldId: "total" });
        message.billaddress = salesOrderRecord.getValue({ fieldId: "billaddress" });
        message.shipaddress = salesOrderRecord.getValue({ fieldId: "shipaddress" });
        message.orderNum = salesOrderRecord.getValue({ fieldId: "tranid" });
        message.deliverydate = salesOrderRecord.getValue({ fieldId: "custbody_expected_delivery_date" });
        message.orderDate = salesOrderRecord.getValue({ fieldId: "createddate" });
        message.status = salesOrderRecord.getValue({ fieldId: "status" });

        message.subsidiary = parseInt(salesOrderRecord.getValue({ fieldId: "subsidiary" }));

        message.shipmethod = salesOrderRecord.getText({ fieldId: "shipmethod" });
        message.shipTotal = salesOrderRecord.getValue({ fieldId: "shippingcost" });

        message.currency = salesOrderRecord.getValue({ fieldId: "currency" });
        message.ponumber = salesOrderRecord.getValue({ fieldId: "otherrefnum" });

        var trackingNumber = salesOrderRecord.getValue({ fieldId: "linkedtrackingnumbers" });
        if (trackingNumber) {
            message.trackingNumber = trackingNumber;
        } else {
            message.trackingNumber = "N/A";
        }

        message.taxRate = salesOrderRecord.getValue({ fieldId: "taxRate" });
        message.taxTotal = salesOrderRecord.getValue({ fieldId: "taxTotal" });

        for (var i = 0; i < salesOrderRecord.getLineCount({ sublistId: "item" }); i++) {
            var item = {};
            item.id = parseInt(salesOrderRecord.getSublistValue({ sublistId: "item", fieldId: "item", line: i }));
            item.name = salesOrderRecord.getSublistText({ sublistId: "item", fieldId: "item", line: i });
            item.quantity = salesOrderRecord.getSublistValue({ sublistId: "item", fieldId: "quantity", line: i });
            item.price = salesOrderRecord.getSublistValue({ sublistId: "item", fieldId: "rate", line: i });
            item.shipDate = salesOrderRecord.getSublistValue({ sublistId: "item", fieldId: "expectedshipdate", line: i });
            item.taxRate = salesOrderRecord.getSublistValue({ sublistId: "item", fieldId: "taxRate1", line: i });
            item.taxAmt = salesOrderRecord.getSublistValue({ sublistId: "item", fieldId: "tax1Amt", line: i });
            message.items.push(item);
        }
    }

    function orderContainsPlates(items) {
        var dryMedia = [
            // 1538, //TK2200
            // 1540, //TK2205
            // 1541, //TK2300
            // 1542, //TK3010
            // 1543,  //TK3100
            // 16224, //TK3101
            3068, //TK3300
            // 1059, //TK3305
            // 5209, //TK3310
            16225, //TK3410
            // 16226, //TK3415
            16227, //TK3420
            // 5211, //TK3450
            // 5212, //TK3455
            // 5214, //TK3495
            16228, //TK3500
            16229, //TK3501
            // 5215, //TK3505
            // 5216, //TK3507
            16230 //TK3600
            // 1077, //TK3602
            // 3910, //TK3701
            // 1053, //TK3710
            // 5218 //TK3800
        ];
        for (var i = items.length - 1; i >= 0; i--) {
            if (dryMedia.indexOf(items[i].MerchandiseID) >= 0) {
                return true;
            }
        }
        return false;
    }

    function putShipDatesIntoOrder(availability, inventoryLocation, packagingWOs) {
        for (var j = 0; j < availability.length; j++) {
            if (/*availability[j].inventoryLocation == inventoryLocation &&*/ availability[j].type == "Packaging WO") {
                log.audit({ title: inventoryLocation + ' Packaging WO', details: availability[j] });
                var k;
                for (k = 0; k < packagingWOs.length; k++) {
                    var date = new Date(packagingWOs[k].dateValue);
                    var insertDate = new Date(availability[j].dateValue);
                    if (insertDate < date) break;
                }

                packagingWOs.splice(k, 0, availability[j]);
            }
        }
        log.audit({ title: 'All Packaging WO Dates', details: packagingWOs });
    }

    function HomebrewShipDate(subsidiary) {
        var hbDate = getLocalTime(2, false);

        hbDate.setDate(hbDate.getDate() + 1);

        // 2pm cutoff for next-day shipping
        // 6/27/19: There is now no longer a next-day cutoff for subsidiary 2
        if (hbDate.getHours() >= 14 && subsidiary !== 2) {
            hbDate.setDate(hbDate.getDate() + 1);
        }

        var valiDatedDate = valiDate(hbDate, subsidiary);
        log.audit({ title: 'HomebrewShipDate: Initial, Validated', details: new Date(hbDate).toString('MM/dd/yyyy') + ', ' + valiDatedDate.toString('MM/dd/yyyy') });
        return valiDatedDate;
    }

    function addTransitTimes() {
        var transittimes = {};
        var resultSet = search.load({ id: 2041 }).run();

        resultSet.each(function (result) {
            transittimes[result.getValue("custrecord_shipping_method")] = result.getValue("custrecord_days_in_transit");
            return true;
        });

        return transittimes;
    }

    function addTransitTimesAndRanges() {
        var transitTimes = {};

        var delay = search.load({ id: 2041 }).run();

        delay.each(function (result) {
            var transitTime = {};
            transitTime.daysInTransit = parseInt(result.getValue("custrecord_days_in_transit"));
            transitTimes[result.getValue("custrecord_shipping_method")] = transitTime;
            return true;
        });

        var ranges = search.load({ id: 1939 }).run();

        ranges.each(function (result) {
            transitTimes[result.getValue("custrecord_shipping_method")].daysInTransitRange = parseInt(result.getValue("custrecord_days_in_transit_range"));
            return true;
        });

        return transitTimes;
    }

    function setWillCallAddress(shipmethod, salesOrderRecord, customerName) {
        switch (shipmethod) {
            case 3470: //Boulder
                salesOrderRecord.setValue({ fieldId: "shipaddresslist", value: null });
                var shipaddr = salesOrderRecord.getSubrecord({ fieldId: 'shippingaddress' });
                shipaddr.setValue({ fieldId: 'country', value: 'US' });
                shipaddr.setValue({ fieldId: 'attention', value: 'Attn To: ' + customerName });
                shipaddr.setValue({ fieldId: 'addressee', value: 'Boulder Will-Call' });
                shipaddr.setValue({ fieldId: 'addr1', value: '1898 S. Flatiron Ct.' });
                shipaddr.setValue({ fieldId: 'addr2', value: 'Suite 213' });
                shipaddr.setValue({ fieldId: 'city', value: 'Boulder' });
                shipaddr.setValue({ fieldId: 'state', value: 'CO' });
                shipaddr.setValue({ fieldId: 'zip', value: '80301' });
                break;
            case 3472: //Asheville
            case 13332: //Go Green
                salesOrderRecord.setValue({ fieldId: "shipaddresslist", value: null });
                var shipaddr = salesOrderRecord.getSubrecord({ fieldId: 'shippingaddress' });
                shipaddr.setValue({ fieldId: 'country', value: 'US' });
                shipaddr.setValue({ fieldId: 'attention', value: 'Attn To: ' + customerName });
                shipaddr.setValue({ fieldId: 'addressee', value: 'Asheville ' + (shipmethod == 3471 ? 'Will-Call' : 'Go Green') });
                shipaddr.setValue({ fieldId: 'addr1', value: '172 South Charlotte Street' });
                shipaddr.setValue({ fieldId: 'city', value: 'Asheville' });
                shipaddr.setValue({ fieldId: 'state', value: 'NC' });
                shipaddr.setValue({ fieldId: 'zip', value: '28801' });
                break;
            case 3469: //SD
            case 3511: //Go Green
                salesOrderRecord.setValue({ fieldId: "shipaddresslist", value: null });
                var shipaddr = salesOrderRecord.getSubrecord({ fieldId: 'shippingaddress' });
                shipaddr.setValue({ fieldId: 'country', value: 'US' });
                shipaddr.setValue({ fieldId: 'attention', value: 'Attn To: ' + customerName });
                shipaddr.setValue({ fieldId: 'addressee', value: 'San Diego ' + (shipmethod == 3469 ? 'Will-Call' : 'Go Green') });
                shipaddr.setValue({ fieldId: 'addr1', value: '9495 Candida Street' });
                shipaddr.setValue({ fieldId: 'city', value: 'San Diego' });
                shipaddr.setValue({ fieldId: 'state', value: 'CA' });
                shipaddr.setValue({ fieldId: 'zip', value: '92126' });
                break;
        }
    }

    function loadItem(type, NSID) {
        try {
            try {
                switch (type) {
                    case 1:
                    case 2:
                    case 5:
                        try {
                            return record.load({ type: record.Type.ASSEMBLY_ITEM, id: NSID });
                        } catch (error) {
                            return record.load({ type: record.Type.INVENTORY_ITEM, id: NSID });
                        }
                    case 3:
                        try {
                            return record.load({ type: record.Type.INVENTORY_ITEM, id: NSID });
                        } catch (error) {
                            return record.load({ type: record.Type.ASSEMBLY_ITEM, id: NSID });
                        }
                    case 4:
                        try {
                            return record.load({ type: record.Type.SERVICE_ITEM, id: NSID });
                        } catch (error) {
                            try {
                                return record.load({ type: record.Type.ASSEMBLY_ITEM, id: NSID });
                            } catch (error) {
                                return record.load({ type: record.Type.INVENTORY_ITEM, id: NSID });
                            }
                        }
                    default:
                        try {
                            return record.load({ type: record.Type.INVENTORY_ITEM, id: NSID });
                        } catch (error) {
                            try {
                                return record.load({ type: record.Type.ASSEMBLY_ITEM, id: NSID });
                            } catch (error) {
                                return record.load({ type: record.Type.SERVICE_ITEM, id: NSID });
                            }
                        }
                }
            } catch (error) {
                try {
                    return record.load({ type: record.Type.KIT_ITEM, id: NSID });
                } catch (error1) {
                    throw error1;
                }
            }
        } catch (error) {
            logError('loadItem', 'Unable to load record of type ' + type + ' for NSID ' + NSID);
            throw error;
        }
    }

    function getCouponId(couponCode) {
        var filters = [];
        filters.push(search.createFilter({ name: 'code', operator: search.Operator.IS, values: couponCode }));

        var columns = [];
        columns.push(search.createColumn({ name: 'internalid' }));

        var couponIds = [];

        try {
            search.create({ type: 'promotionCode', filters: filters, columns: columns }).run().each(function (result) {
                var couponId = parseInt(result.getValue({ name: "internalid" }));
                couponIds.unshift(couponId);
            });
        } catch (err) {
            logError('getCouponId', err);
        }

        if (couponIds.length > 0) {
            return couponIds[0];
        } else {
            return 0;
        }
    }

    // Temporary hack method to extend lead times until final
    // approach is decided upon.
    function getItemLeadTime(itemId, currentLeadTime) {
        switch (itemId) {
            // Blends (some of these IDs might be in another block too)
            case 16474: //WLP-Blend Custom Pour
            case 2740:  //WLP010
            case 2964:  //WLP010 : WLP010-1.5L
            case 2976:  //WLP010 : WLP010-2L
            case 10789: //WLP010 : WLP010-Custom Pour
            case 2852:  //WLP010 : WLP010-HB
            case 2952:  //WLP010 : WLP010-Nano
            case 2741:  //WLP060
            case 2965:  //WLP060 : WLP060-1.5L
            case 2977:  //WLP060 : WLP060-2L
            case 10790: //WLP060 : WLP060-Custom Pour
            case 2853:  //WLP060 : WLP060-HB
            case 2953:  //WLP060 : WLP060-Nano
            case 13452: //WLP064
            case 13453: //WLP064 : WLP064-Custom Pour
            case 14456: //WLP067
            case 16368: //WLP067 : WLP067-1.5L
            case 16369: //WLP067 : WLP067-2L
            case 14457: //WLP067 : WLP067-Custom Pour
            case 14508: //WLP067 : WLP067-HB
            case 16370: //WLP067 : WLP067-Nano
            case 17196: //WLP067-O
            case 17197: //WLP067-O : WLP067-O-2L
            case 17199: //WLP067-O : WLP067-O-Custom Pour
            case 17198: //WLP067-O : WLP067-O-Nano
            case 5739:  //WLP075
            case 13449: //WLP075
            case 13450: //WLP075 : WLP075-Custom Pour
            case 2742:  //WLP080
            case 2966:  //WLP080 : WLP080-1.5L
            case 2978:  //WLP080 : WLP080-2L
            case 10791: //WLP080 : WLP080-Custom Pour
            case 2854:  //WLP080 : WLP080-HB
            case 2954:  //WLP080 : WLP080-Nano
            case 2743:  //WLP085
            case 10792: //WLP085 : WLP085-Custom Pour
            case 3489:  //WLP099
            case 3499:  //WLP099 : WLP099-1.5L
            case 3501:  //WLP099 : WLP099-2L
            case 10793: //WLP099 : WLP099-Custom Pour
            case 3502:  //WLP099 : WLP099-HB
            case 3503:  //WLP099 : WLP099-Nano
            case 15945: //WLP195
            case 16172: //WLP195
            case 16241: //WLP195 : WLP195-Custom Pour
            case 542:   //WLP200
            case 2242:  //WLP200 : WLP200-1.5L
            case 2331:  //WLP200 : WLP200-2L
            case 2851:  //WLP200 : WLP200-HB
            case 2443:  //WLP200 : WLP200-Nano
            case 549:   //WLP4005
            case 16817: //WLP4005 : WLP4005-2L
            case 16819: //WLP4005 : WLP4005-Nano
            case 47:    //WLP4005-Freeze
            case 5908:  //WLP4005-Seed
            case 9891:  //WLP4005-Slant
            case 349:   //WLP4005-WP
            case 3772:  //WLP4007
            case 3777:  //WLP4007 : WLP4007-2L
            case 3780:  //WLP4007 : WLP4007-HB
            case 3779:  //WLP4007 : WLP4007-Nano
            case 2752:  //WLP4010
            case 2750:  //WLP4010-Freeze
            case 5963:  //WLP4010-Seed
            case 9893:  //WLP4010-Slant
            case 2751:  //WLP4010-WP
            case 3795:  //WLP4021
            case 3797:  //WLP4021 : WLP4021-2L
            case 3801:  //WLP4021 : WLP4021-HB
            case 3800:  //WLP4021 : WLP4021-Nano
            case 14386: //WLP4042
            case 14389: //WLP4042 : WLP4042-2L
            case 14396: //WLP4042 : WLP4042-HB
            case 14391: //WLP4042 : WLP4042-Nano
            case 16400: //WLP4044
            case 16402: //WLP4044 : WLP4044-2L
            case 16404: //WLP4044 : WLP4044-HB
            case 16409: //WLP4044 : WLP4044-Nano
            case 2758:  //WLP4600
            case 2756:  //WLP4600-Freeze
            case 9902:  //WLP4600-Slant
            case 2757:  //WLP4600-WP
            case 2780:  //WLP4603
            case 3847:  //WLP4603 : WLP4603-2L
            case 3852:  //WLP4603 : WLP4603-HB
            case 3849:  //WLP4603 : WLP4603-Nano
            case 2761:  //WLP4605
            case 2759:  //WLP4605-Freeze
            case 9904:  //WLP4605-Slant
            case 2760:  //WLP4605-WP
            case 2781:  //WLP4613
            case 3892:  //WLP4613 : WLP4613-2L
            case 3888:  //WLP4613 : WLP4613-HB
            case 3889:  //WLP4613 : WLP4613-Nano
            case 2767:  //WLP4615
            case 2765:  //WLP4615-Freeze
            case 9907:  //WLP4615-Slant
            case 2766:  //WLP4615-WP
            case 2770:  //WLP4620
            case 2768:  //WLP4620-Freeze
            case 9908:  //WLP4620-Slant
            case 2769:  //WLP4620-WP
            case 556:   //WLP4623
            case 2376:  //WLP4623 : WLP4623-2L
            case 2587:  //WLP4623 : WLP4623-HB
            case 2488:  //WLP4623 : WLP4623-Nano
            case 2773:  //WLP4625
            case 2771:  //WLP4625-Freeze
            case 9910:  //WLP4625-Slant
            case 2772:  //WLP4625-WP
            case 2782:  //WLP4626
            case 3860:  //WLP4626 : WLP4626-2L
            case 3863:  //WLP4626 : WLP4626-HB
            case 3862:  //WLP4626 : WLP4626-Nano
            case 2776:  //WLP4630
            case 2774:  //WLP4630-Freeze
            case 9913:  //WLP4630-Slant
            case 2775:  //WLP4630-WP
            case 2779:  //WLP4635
            case 2777:  //WLP4635-Freeze
            case 9915:  //WLP4635-Slant
            case 2778:  //WLP4635-WP
            case 13413: //WLP4636
            case 13436: //WLP4636 : WLP4636-2L
            case 13547: //WLP4636 : WLP4636-HB
            case 13434: //WLP4636 : WLP4636-Nano
            case 6094:  //WLP4682
            case 6097:  //WLP4682 : WLP4682-2L
            case 13599: //WLP4682 : WLP4682-HB
            case 6099:  //WLP4682 : WLP4682-Nano
            case 13403: //WLP564
            case 13504: //WLP564 : WLP564-Custom Pour
            case 2745:  //WLP568
            case 2970:  //WLP568 : WLP568-1.5L
            case 2982:  //WLP568 : WLP568-2L
            case 10799: //WLP568 : WLP568-Custom Pour
            case 2858:  //WLP568 : WLP568-HB
            case 2958:  //WLP568 : WLP568-Nano
            case 17200: //WLP568-O
            case 17201: //WLP568-O : WLP568-O-2L
            case 17202: //WLP568-O : WLP568-O-Custom Pour
            case 17203: //WLP568-O : WLP568-O-Nano
            case 2746:  //WLP575
            case 2971:  //WLP575 : WLP575-1.5L
            case 2983:  //WLP575 : WLP575-2L
            case 10800: //WLP575 : WLP575-Custom Pour
            case 2859:  //WLP575 : WLP575-HB
            case 2959:  //WLP575 : WLP575-Nano
            case 5479:  //WLP611
            case 13506: //WLP611 : WLP611-Custom Pour
            case 13446: //WLP616
            case 13447: //WLP616 : WLP616-Custom Pour
            case 2747:  //WLP630
            case 2972:  //WLP630 : WLP630-1L
            case 2860:  //WLP630 : WLP630-HB
            case 10899: //WLP6560
            case 10896: //WLP6560-Freeze
            case 3513:  //WLP665
            case 13935: //WLP665 : WLP665-Custom Pour
            case 13421: //WLP665-Freeze
            case 2749:  //WLP670
            case 13939: //WLP670 : WLP670-1L
            case 2862:  //WLP670 : WLP670-HB
            case 13430: //WLP773
            case 13432: //WLP773 : WLP773-Custom Pour
            case 13586: //WLP773 : WLP773-HB
                return 2;
            case 5219:
            case 13658:
            case 13859:
                return 2;
            case 1542:
            case 1543:
            case 3068:
            case 1077:
            case 1053:
            case 1554:
            case 1104:
            case 3910:
            case 5209:
            case 5211:
            case 5212:
            case 5214:
            case 5215:
            case 5216:
            case 5217:
            case 5218:
            case 5487:
            case 16224:
            case 16225:
            case 16226:
            case 16227:
            case 16229:
            case 16228:
            case 16230:
            case 1539:
            case 1540:
            case 1541:
            case 1542:
            case 2741:
            case 2742:
            case 2745:
            case 2746:
            case 2747:
            case 2749:
            case 2853:
            case 2854:
            case 2858:
            case 2859:
            case 2860:
            case 2862:
            case 2953:
            case 2954:
            case 2958:
            case 2959:
            case 2965:
            case 2966:
            case 2970:
            case 2971:
            case 2977:
            case 2978:
            case 2982:
            case 2983:
            case 3477:
            case 3478:
            case 3479:
            case 3489:
            case 3499:
            case 3501:
            case 3502:
            case 3503:
            case 3690:
            case 3691:
            case 3692:
            case 5393:
            case 5696:
            case 6167:
            case 6243:
            case 6367:
            case 6443:
            case 6679:
            case 6755:
            case 10479:
            case 10532:
            case 10606:
            case 10790:
            case 10791:
            case 10793:
            case 10799:
            case 10800:
            case 13415:
                //case 13939:
                return 3;
            case 5135:
            case 5136:
            case 5138:
            case 5143:
            case 5220:
            case 5221:
            case 5222:
            case 5223:
            case 5692:
            case 5704:
            case 5707:
            case 5714:
            case 5759:
            case 6086:
            case 6087:
            case 6088:
            case 6134:
            case 6135:
            case 6149:
            case 6557:
            case 9480:
            case 9789:
            case 9890:
            case 9891:
            case 9893:
            case 9894:
            case 9895:
            case 9897:
            case 9898:
            case 9899:
            case 9900:
            case 9901:
            case 9902:
            case 9904:
            case 9907:
            case 9908:
            case 9910:
            case 9913:
            case 9915:
            case 9919:
            case 9920:
            case 9921:
            case 9922:
            case 9923:
            case 9924:
            case 9925:
            case 9926:
            case 9927:
            case 9928:
            case 9929:
            case 9930:
            case 9931:
            case 9932:
            case 9933:
            case 9934:
            case 9935:
            case 9936:
            case 9937:
            case 9938:
            case 9939:
            case 9940:
            case 9941:
            case 9942:
            case 9943:
            case 9944:
            case 9945:
            case 9946:
            case 9947:
            case 9948:
            case 9949:
            case 9950:
            case 9951:
            case 9952:
            case 9953:
            case 9955:
            case 9956:
            case 9957:
            case 9959:
            case 9960:
            case 9961:
            case 9962:
            case 9963:
            case 9964:
            case 9965:
            case 9966:
            case 9967:
            case 9968:
            case 9969:
            case 9970:
            case 9971:
            case 9972:
            case 9973:
            case 9974:
            case 9975:
            case 9976:
            case 9977:
            case 9979:
            case 9980:
            case 9982:
            case 9983:
            case 9984:
            case 9985:
            case 9986:
            case 9987:
            case 9988:
            case 9989:
            case 9990:
            case 9991:
            case 9992:
            case 9993:
            case 9994:
            case 9995:
            case 9996:
            case 9997:
            case 9998:
            case 9999:
            case 10000:
            case 10001:
            case 10002:
            case 10003:
            case 10004:
            case 10005:
            case 10006:
            case 10008:
            case 10009:
            case 10010:
            case 10011:
            case 10012:
            case 10013:
            case 10014:
            case 10015:
            case 10016:
            case 10017:
            case 10018:
            case 10019:
            case 10020:
            case 10021:
            case 10022:
            case 10023:
            case 10025:
            case 10026:
            case 10027:
            case 10028:
            case 10029:
            case 10030:
            case 10031:
            case 10032:
            case 10033:
            case 10034:
            case 10035:
            case 10036:
            case 10037:
            case 10038:
            case 10039:
            case 10040:
            case 10041:
            case 10042:
            case 10043:
            case 10044:
            case 10045:
            case 10046:
            case 10047:
            case 10048:
            case 10049:
            case 10050:
            case 10051:
            case 10052:
            case 10053:
            case 10054:
            case 10055:
            case 10056:
            case 10057:
            case 10058:
            case 10059:
            case 10060:
            case 10061:
            case 10062:
            case 10063:
            case 10064:
            case 10065:
            case 10066:
            case 10069:
            case 10071:
            case 10072:
            case 10073:
            case 10074:
            case 10077:
            case 10078:
            case 10079:
            case 10080:
            case 10082:
            case 10083:
            case 10084:
            case 10085:
            case 10086:
            case 10087:
            case 10088:
            case 10089:
            case 10090:
            case 10091:
            case 10092:
            case 10093:
            case 10094:
            case 10095:
            case 10096:
            case 10097:
            case 10098:
            case 10099:
            case 10100:
            case 10101:
            case 10102:
            case 10103:
            case 10104:
            case 10105:
            case 10106:
            case 10107:
            case 10109:
            case 10110:
            case 10111:
            case 10112:
            case 10113:
            case 10114:
            case 10115:
            case 10117:
            case 10118:
            case 10119:
            case 10120:
            case 10121:
            case 10122:
            case 10124:
            case 10125:
            case 10126:
            case 10127:
            case 10128:
            case 10129:
            case 10130:
            case 10131:
            case 10132:
            case 10133:
            case 10134:
            case 10135:
            case 10136:
            case 10137:
            case 10138:
            case 10139:
            case 10140:
            case 10141:
            case 10142:
            case 10143:
            case 10144:
            case 10145:
            case 10146:
            case 10147:
            case 10148:
            case 10149:
            case 10150:
            case 10151:
            case 10153:
            case 10154:
            case 10155:
            case 10156:
            case 10157:
            case 10158:
            case 10159:
            case 10160:
            case 10161:
            case 10162:
            case 10163:
            case 10164:
            case 10165:
            case 10166:
            case 10167:
            case 10168:
            case 10169:
            case 10170:
            case 10171:
            case 10172:
            case 10173:
            case 10174:
            case 10175:
            case 11021:
            case 11022:
            case 11364:
            case 11365:
            case 12989:
            case 12990:
            case 12998:
            case 13056:
            case 13140:
            case 13145:
            case 13146:
            case 13147:
            case 13148:
            case 13156:
            case 13232:
            case 13235:
            case 13327:
            case 13353:
            case 13358:
            case 13374:
            case 13379:
            case 13380:
            case 13405:
            case 13475:
            case 13476:
            case 13572:
            case 13574:
            case 13575:
            case 13576:
            case 13660:
            case 13707:
            case 13708:
            case 13786:
            case 13789:
            case 13843:
            case 13844:
            case 13845:
            case 13857:
            case 13867:
            case 13895:
            case 13936:
            case 13937:
            case 13938:
            case 13941:
            case 13942:
            case 14351:
            case 14352:
            case 14353:
            case 14354:
            case 14355:
            case 14443:
            case 14459:
            case 14460:
            case 14467:
            case 14472:
            case 14531:
            case 14532:
            case 14533:
            case 16176:
            case 16181:
            case 16183:
            case 16232:
            case 16255:
            case 16356:
            case 16371:
            case 16379:
            case 16380:
            case 16398:
            case 16486:
                return 5;
            default:
                //Temporary hack: Ignore the item's lead time custom field and just return 0
                return 0;
            //return (!currentLeadTime ? 0 : currentLeadTime);
        }
    }

    function logError(func, error) {
        var errorText = error.code ? JSON.stringify(error.code) + ': ' + error.message : error.toString();
        log.error({
            title: "ORDER - " + func,
            details: errorText
        });
    }

    return {
        get: get,
        put: put,
        post: post
    };
});