/**
*@NApiVersion 2.x
*@NScriptType ScheduledScript
* Author: dvasilev@whitelabs.com
* Last Update: 11/7/2018 dvasilev@whitelabs.com
* Purpose: Get and cache inventory for app and website. This scheduled script runs every 10 minutes and caches
*       inventory to be retrieved by app/YMO 2.0
* File: cacheInventory.js
*/

define(['N/record', 'N/log', 'N/search', 'N/email', 'N/runtime', 'N/task', 'N/cache', 'N/crypto', 'N/encode', 'N/https', 'N/url', 'N/format', '../wl_ba_cs_item_availability.js', '../WLBALib20.js',
'../WL_BA/WLVaultItemsAvail.js', '../WL_BA/WLYMOCacheItemAvail.js', './YMO-LIB'],
    function (record, log, search, email, runtime, task, cache, crypto, encode, https, urlMod, format, itemAvailability, WLBALib20, WLVaultItemsAvail, WLYMOCacheItemAvail) {
        /**
        * The following function creates a saved search to find inventory/assembly/service items to be included in the app/website
        * and groups associated items together
        *
        * @return {items[]}, an object with one key - an array of items
        */
        function execute(scriptContext) {
            try {
                var response = { items: [] };
                var currentContext = runtime.getCurrentScript();

                var inventory = searchForItems();

                fixNetSuiteJavaScript();

                var hbInventory = null;
                try {
                    hbInventory = getHBAvailability();
                    //log.audit({ title: 'HB Availbility', details: hbInventory });
                } catch (error) {
                    logError('HB Avail', error);
                }

                var vaultAvail = WLVaultItemsAvail.GetVaultItemAvail();

                var startIndex = parseInt(currentContext.getParameter({ name: 'count' })) || 0;
                var startPage = parseInt(currentContext.getParameter({ name: 'inventoryPage' })) || 0;

                //initializations
                var results, parentIDs = [], slantRetries = [];
                var yeastMap = {}; //is a map such indexOfItemInResponse = yeastMap[parentName] => response[indexOfItemInResponse]

                for (var inventoryPage = startPage; inventoryPage < inventory.length; inventoryPage++) {
                    results = inventory[inventoryPage];

                    for (var i = startIndex; i < results.length; i++) {
                        // rescheduled script task if governance has fallen to less than 50 units
                        if (currentContext.getRemainingUsage() <= 50) {
                            var scriptTask = {};
                            scriptTask.scriptId = 944;
                            scriptTask.deploymentId = 'customdeploy1';
                            scriptTask.params = { count: startIndex, inventoryPage: startPage };
                            scriptTask.submit();
                            return;
                        }

                        startIndex++;

                        var slantExceptionStr = results[i].getValue({ name: 'itemid' }).toLowerCase();
                        var itemName = String(results[i].getValue({ name: 'itemid' }));
                        var displayName = String(results[i].getValue({ name: 'displayname' }));

                        setItemVaultAvail(results, i, vaultAvail);

                        if (hbInventory) {
                            results[i].hbAvail = hbInventory.find(function (element) {
                                return (element.item == results[i].getValue({ name: 'itemid' }));
                            });
                        }

                        if (results[i].getText({ name: 'parent' })) {
                            // Yeast item with parent
                            processYeastItemWithParent(i, results, response, slantExceptionStr, yeastMap, parentIDs);
                        } else if (slantExceptionStr.indexOf('slant') >= 0) {
                            // Slant item without parent
                            processSlantItemWithoutParent(i, results, response, slantExceptionStr, yeastMap, slantRetries);
                        } else {
                            //Non Yeast Item
                            var itemClass = parseInt(results[i].getValue({ name: 'class' }));

                            if (itemClass == 28) {
                                // WLEDU -- Group options (webinar and in-person) together
                                processEducationItem(i, results, response, yeastMap, itemName);
                            } else if ([27, 10].indexOf(itemClass) >= 0) {
                                //Gift Shop & Merchandise
                                processGiftShopItem(i, results, response, yeastMap, displayName, itemName, itemClass);
                            } else if ([29, 30].indexOf(itemClass) >= 0) {
                                //Nutrients & Enzymes
                                processNutrientsAndEnzymes(i, results, response, yeastMap, displayName, itemName, itemClass);
                            } else if (itemClass == 11) {
                                // Beer
                                processAlcoholItem(i, results, response, yeastMap, displayName, itemName, itemClass);
                            } else {
                                //Lab services & Other
                                processLabServices(i, results, response, yeastMap, itemName);
                            }
                        }
                    }

                    startIndex = 0;
                }

                retrySlants(response, slantRetries, yeastMap);
                setBigQCDayDates(response);

                if (parentIDs.length > 0) {
                    fixNames(response.items, yeastMap, parentIDs);
                }

                cacheItems(response.items, hbInventory, vaultAvail);

                log.debug('cacheInventory completed successfully');
            }
            catch (error) {
                log.error({
                    title: 'error',
                    details: error
                });
            }
        }

        function cacheItems(items, hbAvail, vaultAvail) {
            var Inventory = cache.getCache({ name: 'YMO2Inventory0', scope: cache.Scope.PUBLIC });
            Inventory.remove({ key: 'lastCache' });

            var HBAvailability = cache.getCache({ name: 'YMO2HBAvail', scope: cache.Scope.PUBLIC });
            var VaultAvailability = cache.getCache({ name: 'YMO2VaultAvail', scope: cache.Scope.PUBLIC });

            var cleanedItems = [];
            var cacheNum = 0;
            var cacheLength = 0;

            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                item.vaultAvail = null;
                item.hbAvail = null;
                cleanedItems.push(item);

                cacheLength = lengthInUtf8Bytes(JSON.stringify(cleanedItems));

                // Write to cache when we hit 400KB
                if (cacheLength / 1000 >= 400) {
                    log.audit({ title: 'Writing to inventory cache ' + cacheNum, details: cacheLength / 1000 + 'KB' });
                    Inventory.put({ key: 'items', value: cleanedItems });
                    cacheNum++;
                    cleanedItems = [];
                    cacheLength = 0;
                    Inventory = cache.getCache({ name: 'YMO2Inventory' + cacheNum, scope: cache.Scope.PUBLIC });
                }
            }

            if (cleanedItems.length > 0) {
                log.audit({ title: 'Writing to inventory cache ' + cacheNum, details: cacheLength / 1000 + 'KB' });
                Inventory.put({ key: 'items', value: cleanedItems });
            }
            Inventory.put({ key: 'lastCache', value: true });

            if (hbAvail) HBAvailability.put({ key: 'avail', value: hbAvail });
            if (vaultAvail) VaultAvailability.put({ key: 'avail', value: vaultAvail });
        }

        function lengthInUtf8Bytes(str) {
            var m = encodeURIComponent(str).match(/%[89ABab]/g);
            return str.length + (m ? m.length : 0);
        }

        function retrySlants(response, slantRetries, yeastMap) {
            for (var i = 0; i < slantRetries.length; i++) {
                var slantExceptionStr = slantRetries[i].getValue({ name: 'itemid' }).toLowerCase();

                if (slantExceptionStr.indexOf('slant') >= 0) {
                    //obtain the parent items name from item name
                    var splitIndex = slantExceptionStr.indexOf('-');
                    var parentName = slantRetries[i].getValue({ name: 'itemid' });
                    parentName = parentName.substring(0, splitIndex);

                    //vol should be 'slant' see addVolId() for details
                    var vol = slantExceptionStr.substring(splitIndex + 1, slantExceptionStr.length);
                    if (yeastMap[parentName]) {
                        additionalYInfo(response.items[yeastMap[parentName]], slantRetries[i], vol);
                    }
                } else {
                    //Item is thrown out if it doesn't pass, may lead to possible missing slants
                    //nlapiLogExecution('ERROR', 'Failed to add slant, first item', 'Item: '+ slantRetries[i].getId());
                }
            }
        }

        function setItemVaultAvail(results, i, vaultAvail) {
            if (vaultAvail) {
                var availResults = vaultAvail.filter(function (element) {
                    return (element.ItemId == results[i].id);
                });

                if (availResults) {
                    results[i].vaultAvail = { itemId: results[i].id };
                    if (availResults.length) {
                        for (var j = 0; j < availResults.length; j++) {
                            var vAvailResult = availResults[j];
                            if (vAvailResult.Type && vAvailResult.Type == 'Packaging WO') {
                                if (!results[i].vaultAvail.WorkOrders) results[i].vaultAvail.WorkOrders = [];
                                results[i].vaultAvail.WorkOrders.push(vAvailResult);
                            } else if (vAvailResult.AvailQty) {
                                results[i].vaultAvail.AvailQty = vAvailResult.AvailQty;
                                results[i].vaultAvail.Location = vAvailResult.Location
                            }
                        }
                    }
                }
            }
        }

        function setBigQCDayDates(response) {
            if (response.qcDayItems && response.qcDayItems.length > 1) {
                for (var i = 0; i < response.qcDayItems.length; i++) {
                    var thisQcdi = response.qcDayItems[i];
                    if (!thisQcdi.isPastDeadline) continue;

                    var thisQcdiBase = thisQcdi.partNum.substring(0, thisQcdi.partNum.length - 2);
                    for (var j = 0; j < response.qcDayItems.length; j++) {
                        if (j == i) {
                            continue;
                        } else {
                            var thatQcdi = response.qcDayItems[j];
                            if (thatQcdi.isPastDeadline) continue;

                            var thatQcdiBase = thatQcdi.partNum.substring(0, thatQcdi.partNum.length - 2);

                            if (thisQcdiBase == thatQcdiBase) {
                                //log.audit({ title: 'Got match: ' + thatQcdi.partNum, details: thatQcdi });
                                if (!thisQcdi.nextItem) {
                                    thisQcdi.nextItem = thatQcdi;
                                    //log.audit({ title: 'Applied ' + thatQcdi.partNum + ' as next item', details: 'QC Day Quarter: ' + thatQcdi.qcDayQuarter });
                                } else if (thisQcdi.nextItem.qcDayQuarter > thatQcdi.qcDayQuarter || (thisQcdi.nextItem.qcDayQuarter < thisQcdi.qcDayQuarter && thatQcdi.qcDayQuarter > thisQcdi.qcDayQuarter)) {
                                    //log.audit({ title: 'Replacing ' + thisQcdi.nextItem.partNum + ' with ' + thatQcdi.partNum + ' as next item', details: 'QC Day Quarter: ' + thatQcdi.qcDayQuarter });
                                    thisQcdi.nextItem = thatQcdi;
                                }
                            }
                        }
                    }
                }
            }
        }

        function processYeastItemWithParent(i, results, response, slantExceptionStr, yeastMap, parentIDs) {
            //retrieve yeast size/vol from name
            var splitIndex = slantExceptionStr.indexOf('-');
            var vol = slantExceptionStr.substring(splitIndex + 1, slantExceptionStr.length);

            var parent = results[i].getText({ name: 'parent' });
            if ((
                parseInt(results[i].getValue({ name: 'custrecord_wl_yeastgrowth_straincategory', join: 'custitem_qc_spec_record' })) == 31
                || parseInt(results[i].getValue({ name: 'custrecord_wl_yeastgrowth_straincategory', join: 'custitem_qc_spec_record' })) == 32
                || parseInt(results[i].getValue({ name: 'custrecord_wl_yeastgrowth_straincategory', join: 'custitem_qc_spec_record' })) == 33
            ) && parseInt(results[i].getValue({ name: 'custitem_manufacturing_environment' })) == 5) {
                parent += '_vault_preorder';
            }

            //check if sibling item has been encountered before (and that the parent is also a vault pre-order item, if this is one)
            if (yeastMap[parent] !== undefined) {
                additionalYInfo(response.items[yeastMap[parent]], results[i], vol);
            } else {
                yeastMap[parent] = response.items.length;
                //Deliberately using field value below because parent IDs is an ID number search, not a text search
                parentIDs.push(results[i].getValue({ name: 'parent' }));
                //Deliberately not using modified parent name below
                addItem(response.items, results[i], results[i].getText({ name: 'parent' }), vol, true);
            }
        }

        function processSlantItemWithoutParent(i, results, response, slantExceptionStr, yeastMap, slantRetries) {
            var parentName = '';
            var splitIndex = 0;

            //obtain the parent items name from item name
            splitIndex = slantExceptionStr.indexOf('-');
            parentName = results[i].getValue({ name: 'itemid' }).substring(0, splitIndex);

            //vol should be 'slant' see addVolId() for details
            var vol = slantExceptionStr.substring(splitIndex + 1, slantExceptionStr.length);

            if (yeastMap[parentName]) {
                //update item with slant item id
                additionalYInfo(response.items[yeastMap[parentName]], results[i], vol);
            }
            else {
                slantRetries.push(results[i]); //Slant should never be the first item as they generally don't have the correct yeast attributes
            }
        }

        function processEducationItem(i, results, response, yeastMap, itemName) {
            var eduItem = null;

            if (String(itemName.slice(-3)).toLowerCase() == "web") {
                var partSlice = itemName.slice(0, -3);
                if (!yeastMap[partSlice]) {
                    yeastMap[partSlice] = response.items.length;
                    addItem(response.items, results[i], partSlice, null, false);
                }
                addWebinarOrInPersonVolId(response.items[yeastMap[partSlice]], results[i], "webinar");
                eduItem = response.items[yeastMap[partSlice]];
            } else if (String(itemName.slice(-5)).toLowerCase().substring(0, 3) == "web") {
                // Looks like we have some webinar items that end with a year too, e.g., "xxxxxxxWEB19"
                var partSlice = itemName.slice(0, -5);
                if (!yeastMap[partSlice]) {
                    yeastMap[partSlice] = response.items.length;
                    addItem(response.items, results[i], partSlice, null, false);
                }
                addWebinarOrInPersonVolId(response.items[yeastMap[partSlice]], results[i], "webinar");
                eduItem = response.items[yeastMap[partSlice]];
            } else if (String(itemName).toLowerCase().indexOf("web") >= 0) {
                // A webinar, but we can't parse a base class out of it
                yeastMap[itemName] = response.items.length;
                addItem(response.items, results[i], itemName, null, false);
                addWebinarOrInPersonVolId(response.items[yeastMap[itemName]], results[i], "webinar");
                eduItem = response.items[yeastMap[itemName]];
            } else {
                // In Person
                if (!yeastMap[itemName]) {
                    yeastMap[itemName] = response.items.length;
                    addItem(response.items, results[i], itemName, null, false);
                }
                addWebinarOrInPersonVolId(response.items[yeastMap[itemName]], results[i], "in-person");
                eduItem = response.items[yeastMap[itemName]];
            }

            if (eduItem) {
                if (!eduItem.displayPrice) eduItem.displayPrice = [];

                var price = getMSRP(results[i].id);
                if (price) {
                    eduItem.displayPrice.push(price);
                } else {
                    eduItem.displayPrice.push(0);
                }
            }
        }

        function processGiftShopItem(i, results, response, yeastMap, displayName, itemName, itemClass) {
            if (displayName.indexOf('DO NOT USE') < 0) {
                var gsItem = null;

                var splitString = itemName.split('-');
                var volIDIndex = itemSizeToIndex(splitString[splitString.length - 1]);

                if (volIDIndex == -1) {
                    yeastMap[itemName] = response.items.length;
                    addItem(response.items, results[i], null, 0, false);
                    if (itemClass == 27) additionalNYInfo(response.items[yeastMap[partSlice]], results[i], volIDIndex);
                    gsItem = response.items[yeastMap[itemName]];
                } else {
                    var partSlice = itemName.slice(0, itemName.lastIndexOf('-'));

                    if (yeastMap[partSlice]) {
                        additionalNYInfo(response.items[yeastMap[partSlice]], results[i], volIDIndex);
                    } else {
                        yeastMap[partSlice] = response.items.length;
                        addItem(response.items, results[i], null, volIDIndex, false);
                    }

                    gsItem = response.items[yeastMap[partSlice]];
                }

                if (gsItem) {
                    if (!gsItem.displayPrice) gsItem.displayPrice = [];

                    var price = getMSRP(results[i].id);
                    if (price) {
                        gsItem.displayPrice.push(price);
                    } else {
                        gsItem.displayPrice.push(0);
                    }
                }
            }
        }

        function processAlcoholItem(i, results, response, yeastMap, displayName, itemName, itemClass) {
            if (displayName.indexOf('DO NOT USE') < 0) {
                var gsItem = null;

                var splitString = itemName.split('-');
                var volIDIndex = itemSizeToIndex(splitString[splitString.length - 1]);

                if (volIDIndex == -1) {
                    yeastMap[itemName] = response.items.length;
                    addItem(response.items, results[i], null, 0, false);
                    additionalNYInfo(response.items[yeastMap[partSlice]], results[i], volIDIndex);
                    gsItem = response.items[yeastMap[itemName]];
                } else {
                    var partSlice = itemName.slice(0, itemName.lastIndexOf('-'));

                    if (yeastMap[partSlice]) {
                        additionalNYInfo(response.items[yeastMap[partSlice]], results[i], volIDIndex);
                    } else {
                        yeastMap[partSlice] = response.items.length;
                        addItem(response.items, results[i], null, volIDIndex, false);
                    }

                    gsItem = response.items[yeastMap[partSlice]];
                }

                if (gsItem) {
                    gsItem.isAlcoholItem = true;
                    if (!gsItem.displayPrice) gsItem.displayPrice = [];

                    var price = getMSRP(results[i].id);
                    if (price) {
                        gsItem.displayPrice.push(price);
                    } else {
                        gsItem.displayPrice.push(0);
                    }
                }
            }
        }

        function processNutrientsAndEnzymes(i, results, response, yeastMap, itemName) {
            //var thisItem = null;

            //obtain the parent item's name from item name
            var splitIndex = results[i].getValue({ name: 'itemid' }).indexOf('-');

            if (splitIndex >= 0) {
                var parentName = results[i].getValue({ name: 'itemid' });
                var vol = results[i].getValue({ name: 'itemid' }).substring(splitIndex + 1);
                parentName = parentName.substring(0, splitIndex);

                if (yeastMap[parentName]) {
                    //update item with nutrient/enzyme item id
                    addEnzymeOrNutrientVolId(response.items[yeastMap[parentName]], results[i], vol);
                } else {
                    //add to the map
                    yeastMap[parentName] = response.items.length;
                    addItem(response.items, results[i], parentName, null, false);
                    //add the pack size
                    addEnzymeOrNutrientVolId(response.items[yeastMap[parentName]], results[i], vol);
                }
                //thisItem = response.items[yeastMap[parentName]];
            } else {
                // Un-sized nutrient/enzyme, like WLN3200 or WLN3500
                if (!yeastMap[itemName]) {
                    yeastMap[itemName] = response.items.length;
                    addItem(response.items, results[i], null, null, false);
                }
                //thisItem = response.items[yeastMap[itemName]];
            }

            //if (thisItem) setItemAvailableDate(thisItem);
        }

        function processLabServices(i, results, response, yeastMap, itemName) {
            var thisItem = null;

            if (!yeastMap[itemName]) {
                yeastMap[itemName] = response.items.length;
                addItem(response.items, results[i], null, null, false);
            }

            thisItem = response.items[yeastMap[itemName]];
            if (thisItem) setItemAvailableDate(response, thisItem, thisItem.volID[0]);
        }

        function setItemAvailableDate(item, itemId) {
            // Production IDs
            const qcDayItems = [4662, 17272, 17273, 17274, 17283, 17279, 17280, 17281, 17282, 17276, 17277, 17278, 17284, 17269, 17270, 17271, 17285];
            const qcDayItemsQ1 = [17272, 17279, 17276, 17269];
            const qcDayItemsQ2 = [17273, 17280, 17277, 17270];
            const qcDayItemsQ3 = [17274, 17281, 17278, 17271];
            const qcDayItemsQ4 = [17283, 17282, 17284, 17285];

            // Sandbox IDs (currently same as production)
            /*
            const qcDayItems = [16845, 16846, 16847, 16848, 17246, 17247, 17248, 17249, 17250, 17251, 17252, 17253, 17254, 17255, 17256, 17257];
            const qcDayItemsQ1 = [17246, 17249, 17252, 17255];
            const qcDayItemsQ2 = [17247, 17250, 17253, 17256];
            const qcDayItemsQ3 = [17248, 17251, 17254, 17257];
            const qcDayItemsQ4 = [16845, 16846, 16847, 16848];
            */

            const qcDates = [new Date('2/03/2020'), new Date('5/18/2020'), new Date('8/17/2020'), new Date('11/09/2020')];

            if (item.TagDate) {
                item.dateAvailable = new Date(item.TagDate);
            }

            if (qcDayItems.find(function (element) { return element == itemId; })) {
                //QC Day item
                var qcQuarter = 0;
                if (qcDayItemsQ1.find(function (element) { return element == itemId; })) {
                    qcQuarter = 0;
                } else if (qcDayItemsQ2.find(function (element) { return element == itemId; })) {
                    qcQuarter = 1;
                } else if (qcDayItemsQ3.find(function (element) { return element == itemId; })) {
                    qcQuarter = 2;
                } else if (qcDayItemsQ4.find(function (element) { return element == itemId; })) {
                    qcQuarter = 3;
                }

                item.isBigQCDayItem = true;
                item.qcDayQuarter = qcQuarter;
                if (!item.dateAvailable) item.dateAvailable = qcDates[qcQuarter];
            }

            if (item.dateAvailable) {
                item.isFixedShipDateItem = true;

                var sdDate = getLocalTime(2, false);
                if (item.dateAvailable < sdDate) item.isPastDeadline = true;
            }

            if (item.isBigQCDayItem) {
                if (!response.qcDayItems) response.qcDayItems = [];
                response.qcDayItems.push(item);
            }
        }

        function searchForItems() {
            // Search for items ready to sync.
            var filters = [];
            filters.push(search.createFilter({ name: 'type', operator: search.Operator.ANYOF, values: ['Assembly', 'InvtPart', 'Service', 'Kit'] }));
            filters.push(search.createFilter({ name: 'custitem_include_in_ymo_website', operator: search.Operator.IS, values: true }));
            filters.push(search.createFilter({ name: 'isinactive', operator: search.Operator.IS, values: false }));

            var columns = [];
            columns.push(search.createColumn({ name: 'parent' }));
            columns.push(search.createColumn({ name: 'itemid' }));
            columns.push(search.createColumn({ name: 'class' }));
            columns.push(search.createColumn({ name: 'displayname' }));
            columns.push(search.createColumn({ name: 'price' }));
            columns.push(search.createColumn({ name: 'outofstockbehavior' }));
            columns.push(search.createColumn({ name: 'outofstockmessage' }));
            columns.push(search.createColumn({ name: 'storedisplayimage' }));
            columns.push(search.createColumn({ name: 'storedisplaythumbnail' }));
            columns.push(search.createColumn({ name: 'storedisplayname' }));
            // Custom item fields
            columns.push(search.createColumn({ name: 'custitem_ymo_new_image_url' }));
            columns.push(search.createColumn({ name: 'custitem_wl_packaging_methods' }));
            columns.push(search.createColumn({ name: 'custitemwarehouse' }));
            columns.push(search.createColumn({ name: 'custitem_wl_classlocation' }));
            columns.push(search.createColumn({ name: 'custitem_wl_classdates' }));
            columns.push(search.createColumn({ name: 'custitem_wl_yeast_designation' }));
            columns.push(search.createColumn({ name: 'custitem_manufacturing_environment' }));
            //Column doesn't exist
            //columns.push(search.createColumn({ name: 'custitem_wl_analytic_serv_cat' }));
            // Strain fields (mostly custom) that are changing
            /*
            columns.push(search.createColumn({ name: 'custitemauth_purchaser' }));
            columns.push(search.createColumn({ name: 'custitemsearchtags' }));
            columns.push(search.createColumn({ name: 'custitembeerstyles' }));
            columns.push(search.createColumn({ name: 'custitem_wl_attenuation_low' }));
            columns.push(search.createColumn({ name: 'custitem_wl_attenuation_high' }));
            columns.push(search.createColumn({ name: 'custitem_wl_flocculation' }));
            columns.push(search.createColumn({ name: 'custitem_wl_alcohol_tolerance' }));
            columns.push(search.createColumn({ name: 'custitem_wl_opt_ferm_faren_low' }));
            columns.push(search.createColumn({ name: 'custitem_opt_ferm_temp_high_faren' }));
            columns.push(search.createColumn({ name: 'custitem_wl_opt_ferm_celsius_low' }));
            columns.push(search.createColumn({ name: 'custitem_wl_opt_ferm_celsius_high' }));
            columns.push(search.createColumn({ name: 'custitem_wl_style_recommend' }));
            columns.push(search.createColumn({ name: 'custitem_wl_yeast_strain_category' }));
            columns.push(search.createColumn({ name: 'custitem_wl_sta1' }));
            columns.push(search.createColumn({ name: 'custitem_ymo_is_private' }));
            */
            columns.push(search.createColumn({ name: 'storedescription' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_w_description', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_auth_purchaser', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_search_tags', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_beer_styles', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_attenuation_lo', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_attenuation_hi', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_flocculation', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_alc_tolerance', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_low', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_hig', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_low', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_hig', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_style_recommen', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_straincategory', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_sta1', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_ymo_is_private', join: 'custitem_qc_spec_record' }));
            columns.push(search.createColumn({ name: 'custrecord_wl_yeastgrowth_ferm_speed', join: 'custitem_qc_spec_record' }));

            var inventorySearch = search.create({ type: 'item', filters: filters, columns: columns }).runPaged({ "pageSize": 1000 });
            //log.audit({ title: 'Got inventory pages', details: inventorySearch.pageRanges });
            var inventory = [];
            inventorySearch.pageRanges.forEach(function (pageRange) {
                var results = [];
                var inventoryPage = inventorySearch.fetch({ index: pageRange.index });
                //log.audit({ title: 'Loading page ' + pageRange.index, details: inventoryPage.data });
                inventoryPage.data.forEach(function (result) {
                    results.push(result);
                });
                inventory.push(results);
            });

            return inventory;
        }

        function itemSizeToIndex(itemSize) {
            var giftItemSize = String(itemSize).toLowerCase();

            if (giftItemSize == "xs") {
                return 1;
            }
            else if (giftItemSize == "s") {
                return 2;
            }
            else if (giftItemSize == "m") {
                return 0;
            }
            else if (giftItemSize == "l") {
                return 3;
            }
            else if (giftItemSize == "xl") {
                return 4;
            }
            else if (giftItemSize == "2xl" || giftItemSize == "xxl") {
                return 5;
            }
            else if (giftItemSize == "3xl") {
                return 6;
            }
            else {
                return -1;
            }
        }

        function additionalNYInfo(item, result, vol) {
            if (item) {
                item.volID[vol] = parseInt(result.id);

                item.Description = result.getValue({ name: 'custrecord_wl_yeastgrowth_w_description', join: 'custitem_qc_spec_record' });
                if (!item.Description) {
                    item.Description = result.getValue({ name: 'storedescription' });
                }

                if (!item.searchTags) {
                    item.searchTags = result.getValue({ name: 'custrecord_wl_yeastgrowth_search_tags', join: 'custitem_qc_spec_record' });
                }

                var imgUrlId = result.getValue({ name: 'storedisplayimage' });
                var thumbnailUrlId = result.getValue({ name: 'storedisplaythumbnail' });

                if (imgUrlId && !item.imageUrl) {
                    item.imageUrl = loadImageUrl(imgUrlId);
                    item.ImageURL = item.imageUrl; // The old upper-case field name is still used in some places for now
                }

                if (thumbnailUrlId && !item.thumbnailUrl) {
                    item.thumbnailUrl = loadImageUrl(thumbnailUrlId);
                }
            }
        }

        function additionalYInfo(item, result, vol) {
            var vp = addVolId(item, result, vol);

            item.Description = result.getValue({ name: 'custrecord_wl_yeastgrowth_w_description', join: 'custitem_qc_spec_record' });
            if (!item.Description) {
                item.Description = result.getValue({ name: 'storedescription' });
            }

            var designation = result.getText({ name: 'custitem_wl_yeast_designation' });
            // season vault items
            if (item.strainCategory == 31 || item.strainCategory == 32 || item.strainCategory == 33) {
                item.isVaultItem = true;
                if (item.mfgEnvironment == 5 || result.getValue({ name: 'custitem_manufacturing_environment' }) == 5 /*Preorder*/) {
                    setUpVaultPreorderItem(item, result, vp);
                }
                //These are not actually mutually exclusive
                //else {
                setUpVaultAvailability(item, result, vp);
                //}


                if (designation == 'HB' || vp == 4) {
                    item.seasonalHBVault = true;
                }
            }

            // General HB item
            if (designation == 'HB' || vp == 4) {
                setUpHBAvailability(item, result);
            }

            if (!item.IsPrivate) {
                if (result.getValue({ name: 'custrecord_wl_yeastgrowth_ymo_is_private', join: 'custitem_qc_spec_record' })) {
                    item.IsPrivate = [true];
                    if (result.getValue({ name: 'custrecord_wl_yeastgrowth_auth_purchaser', join: 'custitem_qc_spec_record' })) {
                        item.IsPrivate = result.getValue({ name: 'custrecord_wl_yeastgrowth_auth_purchaser', join: 'custitem_qc_spec_record' });
                    } else {
                        item.IsPrivate = [false];
                    }
                }
                else {
                    item.IsPrivate = [false];
                }
            }

            if (!item.attenuation) {
                item.attenuation = result.getValue({ name: 'custrecord_wl_yeastgrowth_attenuation_lo', join: 'custitem_qc_spec_record' });
                if (item.attenuation && result.getValue({ name: 'custrecord_wl_yeastgrowth_attenuation_hi', join: 'custitem_qc_spec_record' })) item.attenuation += ' - ';
                if (result.getValue({ name: 'custrecord_wl_yeastgrowth_attenuation_hi', join: 'custitem_qc_spec_record' }))
                    item.attenuation += result.getValue({ name: 'custrecord_wl_yeastgrowth_attenuation_hi', join: 'custitem_qc_spec_record' });
            }

            if (!item.flocculation) {
                item.flocculation = result.getText({ name: 'custrecord_wl_yeastgrowth_flocculation', join: 'custitem_qc_spec_record' });
            }

            if (!item.sta1) {
                item.sta1 = result.getText({ name: 'custrecord_wl_yeastgrowth_sta1', join: 'custitem_qc_spec_record' });
            }

            if (!item.alcoholTol) {
                item.alcoholTol = result.getText({ name: 'custrecord_wl_yeastgrowth_alc_tolerance', join: 'custitem_qc_spec_record' });
            }

            if (!item.optFermentTempF) {
                item.optFermentTempF = result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_low', join: 'custitem_qc_spec_record' });
                if (item.optFermentTempF && result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_hig', join: 'custitem_qc_spec_record' })) item.optFermentTempF += '-';
                if (result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_hig', join: 'custitem_qc_spec_record' }))
                    item.optFermentTempF += result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_hig', join: 'custitem_qc_spec_record' });
                if (item.optFermentTempF) item.optFermentTempF += '�F';
            }

            if (!item.optFermentTempC) {
                item.optFermentTempC = result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_low', join: 'custitem_qc_spec_record' });
                if (item.optFermentTempC && result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_hig', join: 'custitem_qc_spec_record' })) item.optFermentTempC += '-';
                if (result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_hig', join: 'custitem_qc_spec_record' }))
                    item.optFermentTempC += result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_hig', join: 'custitem_qc_spec_record' });
                if (item.optFermentTempC) item.optFermentTempC += '�C';
            }

            if (!item.styleRec) {
                item.styleRec = result.getValue({ name: 'custrecord_wl_yeastgrowth_style_recommen', join: 'custitem_qc_spec_record' });
            }

            if (!item.searchTags) {
                item.searchTags = result.getValue({ name: 'custrecord_wl_yeastgrowth_search_tags', join: 'custitem_qc_spec_record' });
            }

            var tags = result.getValue({ name: 'custrecord_wl_yeastgrowth_style_recommen', join: 'custitem_qc_spec_record' });
            if (!item.custitemsearchtags && tags) {
                if (tags.length > item.custitemsearchtags) {
                    item.custitemsearchtags = tags;
                }
            }

            var imgUrlId = result.getValue({ name: 'storedisplayimage' });
            var thumbnailUrlId = result.getValue({ name: 'storedisplaythumbnail' });

            if (imgUrlId && !item.imageUrl) {
                item.imageUrl = loadImageUrl(imgUrlId);
                item.ImageURL = item.imageUrl; // The old upper-case field name is still used in some places for now
            }

            if (thumbnailUrlId && !item.thumbnailUrl) {
                item.thumbnailUrl = loadImageUrl(thumbnailUrlId);
            }
        }

        function addItem(response, result, parentName, vol, isYeastItem) {
            var item = {};
            item.volID = [];
            item.mfgEnvironment = result.getValue({ name: 'custitem_manufacturing_environment' });
            item.outofstockbehavior = result.getValue({ name: 'outofstockbehavior' });
            item.outofstockmessage = result.getValue({ name: 'outofstockmessage' });

            var imgUrlId = result.getValue({ name: 'storedisplayimage' });
            var thumbnailUrlId = result.getValue({ name: 'storedisplaythumbnail' });

            if (imgUrlId) {
                item.imageUrl = loadImageUrl(imgUrlId);
                item.ImageURL = item.imageUrl; // The old upper-case field name is still used in some places for now
            }

            if (thumbnailUrlId) {
                item.thumbnailUrl = loadImageUrl(thumbnailUrlId);
            }

            if (isYeastItem) {
                addYeastItem(item, result, vol, parentName);
                item.searchTags = result.getValue({ name: 'custrecord_wl_yeastgrowth_search_tags', join: 'custitem_qc_spec_record' });
            } else {
                addNonYeastItem(item, result, vol, parentName);
            }

            item.Description = result.getValue({ name: 'custrecord_wl_yeastgrowth_w_description', join: 'custitem_qc_spec_record' });
            if (!item.Description) {
                item.Description = result.getValue({ name: 'storedescription' });
            }

            item.warehouse = result.getValue({ name: 'custitemwarehouse' });
            item.salesCategory = parseInt(result.getValue({ name: 'class' }));

            response.push(item);
        }

        function addYeastItem(item, result, vol, parentName) {
            //Yeast Item
            item.partNum = String(parentName);
            item.Name = parentName + " ";

            var vp = addVolId(item, result, vol);

            item.isYeast = true;
            item.strainCategory = parseInt(result.getValue({ name: 'custrecord_wl_yeastgrowth_straincategory', join: 'custitem_qc_spec_record' }));

            var designation = result.getText({ name: 'custitem_wl_yeast_designation' });
            // season vault items
            if (item.strainCategory == 31 || item.strainCategory == 32 || item.strainCategory == 33) {
                item.isVaultItem = true;
                if (item.mfgEnvironment == 5 || result.getValue({ name: 'custitem_manufacturing_environment' }) == 5 /*Preorder*/) {
                    setUpVaultPreorderItem(item, result, vp);
                }
                //These are not actually mutually exclusive
                //else {
                setUpVaultAvailability(item, result, vp);
                //}

                if (designation == 'HB' || vp == 4) {
                    item.seasonalHBVault = true;
                }
            }

            // General HB item
            if (designation == 'HB' || vp == 4) {
                setUpHBAvailability(item, result);
            }

            if (result.getValue({ name: 'custrecord_wl_yeastgrowth_ymo_is_private', join: 'custitem_qc_spec_record' })) {
                item.IsPrivate = [true];
                if (result.getValue({ name: 'custrecord_wl_yeastgrowth_auth_purchaser', join: 'custitem_qc_spec_record' })) {
                    item.IsPrivate = result.getValue({ name: 'custrecord_wl_yeastgrowth_auth_purchaser', join: 'custitem_qc_spec_record' });
                }
            }
            else {
                item.IsPrivate = [false];
            }

            item.attenuation = result.getValue({ name: 'custrecord_wl_yeastgrowth_attenuation_lo', join: 'custitem_qc_spec_record' });
            if (item.attenuation && result.getValue({ name: 'custrecord_wl_yeastgrowth_attenuation_hi', join: 'custitem_qc_spec_record' })) item.attenuation += ' - ';
            if (result.getValue({ name: 'custrecord_wl_yeastgrowth_attenuation_hi', join: 'custitem_qc_spec_record' }))
                item.attenuation += result.getValue({ name: 'custrecord_wl_yeastgrowth_attenuation_hi', join: 'custitem_qc_spec_record' });

            item.flocculation = result.getText({ name: 'custrecord_wl_yeastgrowth_flocculation', join: 'custitem_qc_spec_record' });
            item.sta1 = result.getText({ name: 'custrecord_wl_yeastgrowth_sta1', join: 'custitem_qc_spec_record' });
            item.alcoholTol = result.getText({ name: 'custrecord_wl_yeastgrowth_alc_tolerance', join: 'custitem_qc_spec_record' });

            item.optFermentTempF = result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_low', join: 'custitem_qc_spec_record' });
            if (item.optFermentTempF && result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_hig', join: 'custitem_qc_spec_record' })) item.optFermentTempF += '-';
            if (result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_hig', join: 'custitem_qc_spec_record' }))
                item.optFermentTempF += result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_f_hig', join: 'custitem_qc_spec_record' });
            if (item.optFermentTempF) item.optFermentTempF += '�F';

            item.optFermentTempC = result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_low', join: 'custitem_qc_spec_record' });
            if (item.optFermentTempC && result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_hig', join: 'custitem_qc_spec_record' })) item.optFermentTempC += '-';
            if (result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_hig', join: 'custitem_qc_spec_record' }))
                item.optFermentTempC += result.getValue({ name: 'custrecord_wl_yeastgrowth_opt_ferm_c_hig', join: 'custitem_qc_spec_record' });
            if (item.optFermentTempC) item.optFermentTempC += '�C';

            item.styleRec = result.getValue({ name: 'custrecord_wl_yeastgrowth_style_recommen', join: 'custitem_qc_spec_record' });

            var beerStyles = result.getText({ name: 'custrecord_wl_yeastgrowth_beer_styles', join: 'custitem_qc_spec_record' });
            var beerStylesID = result.getValue({ name: 'custrecord_wl_yeastgrowth_beer_styles', join: 'custitem_qc_spec_record' });
            if (beerStyles) {
                item.beerStylesSearch = String(beerStyles);
                beerStyles = beerStyles.split(','), beerStylesID = beerStylesID.split(',');
                item.beerStyles = [];
                for (var i = 0, totalLength = beerStyles.length - 1; i < totalLength; i++) {
                    item.beerStyles.push({ name: beerStyles[i], id: beerStylesID[i] });
                }
            }
        }

        function addNonYeastItem(item, result, vol, parentName) {
            if (!vol) vol = 0;

            //Non Yeast Item
            if (parentName) {
                item.partNum = String(parentName);
                if (parseInt(result.getValue({ name: 'class' })) != 28) {
                    //education items get named below
                    item.Name = parentName;
                }
            } else {
                item.partNum = result.getValue({ name: 'itemid' });
                item.volID[vol] = parseInt(result.id);
            }

            item.designation = result.getValue({ name: 'custitem_wl_yeast_designation' });

            if (result.getValue({ name: 'custitem_wl_classdates' })) item.TagDate = result.getValue({ name: 'custitem_wl_classdates' });

            if (parseInt(result.getValue({ name: 'class' })) == 28) {
                //education
                if (!item.Name) item.Name = result.getValue({ name: 'displayname' }).split(";")[0];
                item.TagLocation = result.getValue({ name: 'custitem_wl_classlocation' });
            } else if ([27, 10, 11].indexOf(parseInt(result.getValue({ name: 'class' }))) >= 0) {
                if (!item.Name) item.Name = result.getValue({ name: 'displayname' });
                item.Price = result.getValue({ name: 'price' });
            } else {
                if (!item.Name) {
                    item.Name = result.getValue({ name: 'itemid' }) + ': ' + result.getValue({ name: 'displayname' });
                } else {
                    // Find the base part of the item name, without the size
                    var displayName = result.getValue({ name: 'displayname' });
                    if (displayName) {
                        displayName = displayName.replace(' Homebrew', '');
                        var firstNumber = displayName.search(/\d/);
                        if (firstNumber < 0) {
                            item.Name += ' ' + displayName;
                        } else {
                            item.Name += ' ' + displayName.substring(0, firstNumber);
                        }
                    }
                }
            }

            item.isYeast = false;
            item.IsPrivate = [false];

            setUpAnalyticalTestingCategories(item, result);
        }

        function loadImageUrl(imgId) {
            if (imgId) {
                try {
                    var itemFilters = [];
                    itemFilters.push(search.createFilter({ name: 'internalid', operator: search.Operator.IS, values: imgId }));

                    var itemColumns = [];
                    itemColumns.push(search.createColumn({ name: 'url' }));

                    var files = search.create({ type: 'file', filters: itemFilters, columns: itemColumns }).run();
                    var file = files.getRange({ start: 0, end: 1 });

                    if (file.length > 0) {
                        if (file[0].getValue({ name: 'url' }).indexOf('http') < 0) {
                            //Production
                            //return 'https://4099054.app.netsuite.com' + file[0].getValue({ name: 'url' });
                            //Staging
                            return 'https://4099054-sb1.app.netsuite.com' + file[0].getValue({ name: 'url' });
                        } else {
                            return file[0].getValue({ name: 'url' });
                        }
                    } else {
                        return '';
                    }
                } catch (error) {
                    log.error({ title: 'Load Image URL error', details: error });
                }
            }
        }

        function addVolId(item, result, volume) {
            var volPosition = -1;

            if (volume.indexOf('nano') >= 0) {
                volPosition = 0;
                item.purePitch = (result.getText({ name: 'custitem_wl_packaging_methods' }) == 'PurePitch') ? true : false;
            }
            else if (volume.indexOf('1.5l') >= 0) {
                volPosition = 1;
                item.purePitch = (result.getText({ name: 'custitem_wl_packaging_methods' }) == 'PurePitch') ? true : false;
            }
            else if (volume.indexOf('2l') >= 0) {
                volPosition = 2;
                item.purePitch = (result.getText({ name: 'custitem_wl_packaging_methods' }) == 'PurePitch') ? true : false;
            }
            else if (volume.indexOf('custom pour') >= 0) {
                volPosition = 3;
            }
            else if (volume.indexOf('hb') >= 0) {
                volPosition = 4;
            }
            else if (volume.indexOf('slant') >= 0) {
                volPosition = 5;
            }
            else if (parseInt(volume) == '1') {
                volPosition = 6;
            }

            if (volPosition >= 0) item.volID[volPosition] = parseInt(result.id);

            if (item.strainCategory == 31 || item.strainCategory == 32 || item.strainCategory == 33) {
                if (item.mfgEnvironment == 5 || result.getValue({ name: 'custitem_manufacturing_environment' }) == 5) {
                    setUpVaultPreorderItem(item, result, volPosition);
                }
                //These are not actually mutually exclusive
                //else {
                setUpVaultAvailability(item, result, volPosition);
                //}
            }

            if (volPosition == 4 || (item.volID[4] != 0 && !item.hbAvailQty)) {
                setUpHBAvailability(item, result);
            }

            return volPosition;
        }

        function addEnzymeOrNutrientVolId(item, result, packSize) {
            if (!item.pack) {
                item.pack = [];
            }

            if (!item.volID) {
                item.volID = [];
            }

            item.pack.push(packSize);
            item.volID.push(parseInt(result.id));
        }

        function addWebinarOrInPersonVolId(item, result, classType) {
            if (!item.option) {
                item.option = [];
            }

            if (!item.volID) {
                item.volID = [];
            }

            item.option.push(classType);
            item.volID.push(parseInt(result.id));
        }

        function setUpHBAvailability(item, result) {
            if (!item.hbAvailQty && result.hbAvail) {
                item.hbAvailQty = result.hbAvail.availQtyToShip;
            }
        }

        function setUpVaultPreorderItem(item, result, volPosition) {
            item.isVaultPreorderItem = true;

            if (!item.preorderedQty) {
                item.preorderedQty = [0, 0, 0, 0, 0, 0, 0];
                item.preorderedTotal = 0;
                item.preorderTriggerQty = 150;
            }

            // Vault item - get preordered quantity
            var preorderedQty = WLBALib20.GetItemPreorderSumTotal(parseInt(result.id));
            if (preorderedQty) {
                item.preorderedTotal = item.preorderedTotal - item.preorderedQty[volPosition] + parseFloat(preorderedQty);
                item.preorderedQty[volPosition] = parseFloat(preorderedQty);
            } else {
                item.preorderedTotal = item.preorderedTotal - item.preorderedQty[volPosition];
                item.preorderedQty[volPosition] = 0;
            }
        }

        function setUpVaultAvailability(item, result, volPosition) {
            if (!item.availQty) item.availQty = [0, 0, 0, 0, 0, 0, 0];

            if (volPosition >= 0 && volPosition < item.availQty.length && result.vaultAvail) {
                item.availQty[volPosition] = parseInt(result.vaultAvail.AvailQty);
            }

            if (item.volID[4] != 0 && item.availQty[4] <= 0 && !item.isVaultPreorderItem) {
                // Suppress vault HB items with no available quantity from YMO2
                // We have to keep checking this because apparently sometimes items don't get identified as Vault before the HB item gets processed
                // Does not apply to vault preorder items which are HB but not in stock (because they're preorders)
                item.volID[4] = 0;
            }

            if (result.vaultAvail && result.vaultAvail.WorkOrders) {
                if (!item.vaultWorkOrders) {
                    item.vaultWorkOrders = result.vaultAvail.WorkOrders;
                } else {
                    for (var j = 0; j < result.vaultAvail.length; j++) {
                        item.vaultWorkOrders.push(result.vaultAvail.WorkOrders[j]);
                    }
                }
            }
        }

        function setUpAnalyticalTestingCategories(item, result) {
            if (result.getText({ name: 'custitem_wl_analytic_serv_cat' })) {
                var catText = result.getText({ name: 'custitem_wl_analytic_serv_cat' });
                item.itemGroup = catText.split(',');
            } else {
                var partNum = result.getValue({ name: 'itemid' });
                var cats = [
                    'Most Popular',     //0
                    'Beer',             //1
                    'Wine/Cider',       //2
                    'Distilled Spirit', //3
                    'Kombucha',         //4
                    'Water',            //5
                    'Malt',             //6
                    'Hops',             //7
                    'Yeast/Bacteria',   //8
                    'Microbial',        //9
                    'Other'             //10
                ];

                switch (partNum) {
                    case 'LS3010':
                    case 'LS3410':
                    case 'SIT0033':
                        item.itemGroup = [cats[1], cats[2], cats[3]];
                        break;
                    case 'LS3450':
                        item.itemGroup = [cats[3]];
                        break;
                    case 'LS3600':
                        item.itemGroup = [cats[0], cats[1], cats[2], cats[3], cats[4]];
                        break;
                    case 'LS4000':
                    case 'LS4050':
                    case 'LS4060':
                    case 'LS4070':
                    case 'LS4071':
                        item.itemGroup = [cats[2]];
                        break;
                    case 'LS4030':
                    case 'LS4040':
                    case 'SIT0040':
                        item.itemGroup = [cats[2], cats[4]];
                        break;
                    case 'LS6670':
                        item.itemGroup = [cats[10]];
                        break;
                    case 'LS6600':
                    case 'LS6620':
                    case 'LS6644':
                    case 'SIT0001':
                    case 'SIT0011':
                    case 'SIT0021':
                    case 'SIT0041':
                    case 'SIT0090':
                    case 'SIT0091':
                    case 'SIT0110':
                    case 'SIT0200':
                    case 'SIT0291':
                    case 'SIT0350':
                        item.itemGroup = [cats[1]];
                        break;
                    case 'LS6605':
                        item.itemGroup = [cats[1], cats[2], cats[9]];
                        break;
                    case 'LS6610':
                        item.itemGroup = [cats[0], cats[1], cats[2], cats[9]];
                        break;
                    case 'LS6643':
                    case 'LS6672':
                        item.itemGroup = [cats[0], cats[1], cats[4]];
                        break;
                    case 'LS6645':
                    case 'LS6660':
                    case 'LS6725':
                    case 'SIT0050':
                    case 'SIT0130':
                        item.itemGroup = [cats[1], cats[2], cats[4]];
                        break;
                    case 'LS6646':
                        item.itemGroup = [cats[0], cats[1], cats[2], cats[3], cats[4]];
                        break;
                    case 'LS6670':
                    case 'LS9014':
                        item.itemGroup = [cats[1], cats[2], cats[3], cats[4]];
                        break;
                    case 'LS6671':
                    case 'LS6705':
                        item.itemGroup = [cats[8]];
                        break;
                    case 'LS6730':
                        item.itemGroup = [cats[1], cats[8]];
                        break;
                    case 'LS6910':
                    case 'LS6920':
                    case 'LS6930':
                        item.itemGroup = [cats[4]];
                        break;
                    case 'SIT0031':
                    case 'SIT0320':
                        item.itemGroup = [cats[1], cats[2]];
                        break;
                    case 'SIT0070':
                    case 'SIT0140':
                    case 'SIT0141':
                        item.itemGroup = [cats[0], cats[1]];
                        break;
                    case 'SIT0080':
                    case 'SIT9000':
                    case 'SIT9001':
                    case 'SIT9025':
                        item.itemGroup = [cats[1], cats[4], cats[5]];
                        break;
                    case 'SIT0120':
                        item.itemGroup = [cats[1], cats[4]];
                        break;
                    case 'SIT1000':
                    case 'SIT4000':
                    case 'SIT4005':
                    case 'SIT4010':
                    case 'SIT4015':
                        item.itemGroup = [cats[6]];
                        break;
                    case 'SIT3000':
                    case 'SIT3010':
                    case 'SIT3060':
                    case 'SIT3110':
                        item.itemGroup = [cats[7]];
                        break;
                    case 'SIT6000':
                        item.itemGroup = [cats[1], cats[5]];
                        break;
                    default:
                        break;
                }
            }
        }

        function getMSRP(itemId) {
            var itemSearchObj = search.create({
                type: "item",
                filters:
                    [
                        ["pricing.pricelevel", "anyof", "1"],
                        "AND",
                        ["internalidnumber", "equalto", itemId]
                    ],
                columns:
                    [
                        search.createColumn({ name: "internalid", label: "Internal ID" }),
                        search.createColumn({ name: "itemid", label: "Name" }),
                        search.createColumn({ name: "baseprice", label: "Base Price" }),
                        search.createColumn({ name: "unitprice", join: "pricing", label: "Unit Price" })
                    ]
            });

            var searchResults = itemSearchObj.run();
            var results = searchResults.getRange({ start: 0, end: 1 });
            //log.audit({ title: 'Pricing for item ' + itemId, details: results });

            if (results.length > 0) {
                var result = results[0];
                //log.audit({ title: 'First pricing record for item ' + itemId, details: result });
                //log.audit({ title: 'Unit price ' + itemId, details: result.getValue({ name: "unitprice", join: "pricing" }) });
                return result.getValue({ name: "unitprice", join: "pricing" });
            } else {
                return 0;
            }
        }

        function fixNames(response, yeastMap, parentIDs) {
            var filters = [];
            filters.push(search.createFilter({ name: 'internalid', operator: search.Operator.ANYOF, values: parentIDs }));

            var columns = [];
            columns.push(search.createColumn({ name: 'itemid' }));
            columns.push(search.createColumn({ name: 'displayname' }));

            var results;
            try {
                results = search.create({ type: 'item', filters: filters, columns: columns }).run().getRange({ start: 0, end: 1000 });
            }
            catch (error) {
                log.error({ title: 'ERROR', details: error });
                throw { message: 'Failed to load search, Names could not be fixed', code: -1 };
            }
            if (results == null || results.length == 0) {
                throw { message: 'No results returned in name fixing, Names could not be fixed', code: -1 };
            }

            for (var i = 0; i < results.length; i++) {
                if (response[yeastMap[results[i].getValue({ name: 'itemid' })]]) {
                    response[yeastMap[results[i].getValue({ name: 'itemid' })]].Name += results[i].getValue({ name: 'displayname' });
                }

                if (response[yeastMap[results[i].getValue({ name: 'itemid' }) + '_vault_preorder']]) {
                    response[yeastMap[results[i].getValue({ name: 'itemid' }) + '_vault_preorder']].Name += results[i].getValue({ name: 'displayname' });
                }
            }
        }

        function getHBAvailability() {
            var hbAvail = WLYMOCacheItemAvail.GetYMOCacheItemAvail(1, 9, getLocalTime(2, false));
            return hbAvail;
        }

        function logError(func, error) {
            var errorText = error.code ? JSON.stringify(error) : error.toString();
            log.error({
                title: 'cacheInventory - ' + func,
                details: errorText
            });
        }
        return {
            execute: execute
        };
    });
