import SalesLib from './SalesLib';
import {
    beerTypeSelector,
    enzymeSelector,
    homebrewTypeSelector,
    maltValueSelector,
    wortGravitySelector,
    wortVolumeSelector
} from '../redux/selectors/calculator';
import calculator from '../pages/calculator';

function convertVol(calculator) {
    const { volVal, volUnit, volChoices, isHomebrewer } = calculator;
    var index = volChoices[volUnit].indexOf(volVal);
    return volChoices[isHomebrewer ? 'L' : 'BBL'][index];
}

// Convert temperature into F
function convertTemp(calculator) {
    const { tempVal, tempUnit, tempChoices } = calculator;
    var index = tempChoices[tempUnit].indexOf(tempVal);
    return tempChoices['F'][index];
}

// Convert gravity into PLA
function convertGrav(calculator) {
    const { gravVal, gravUnit, gravChoices } = calculator;
    var index = gravChoices[gravUnit].indexOf(gravVal);
    return gravChoices['PLA'][index];
}

const convertPPNGGravityToPlato = (calculator) => {
    var retVal = 0;

    if (calculator.gravUnit === "SPE") {
        //This formula doesn't work, it returns negative values
        //retVal = (-1 * 616.868) + (1111.14 * 1.002) - (630.272 * Math.pow(calculator.ppnggravVal, 2)) + (135.997 * Math.pow(calculator.ppnggravVal, 3)).toFixed(1);

        retVal =
              (135.997 * Math.pow(calculator.ppnggravVal, 3)
            - (630.272 * Math.pow(calculator.ppnggravVal, 2))
            + (1111.14 * Math.pow(calculator.ppnggravVal, 1)) // Just to be consistent
            - 616.868).toFixed(2);

        //Simplified formula
        //retVal = 259 - (259 / calculator.ppnggravVal);
    } else {
        retVal = calculator.ppnggravVal;
    }
    if (retVal < 0) retVal = 0;

    return retVal;
}

export function calculate(calculator) {
    try {
        if (calculator.custom == 'Re-Pitching') {
            return getPacks(custom(calculator), true);
        } else if (calculator.isHomebrewer) {
            return tableLookup(calculator);
        } else if (calculator.custom == 'PurePitch') {
            return getPacks(tableLookup(calculator));
        } else if (calculator.custom == 'FANMax Bio®') {
            return tableLookUpForFanMax(calculator);
        //} else if (calculator.custom == 'Clarity-Ferm') {
         //   return getClarityFermResults(calculator);
        } else if (calculator.custom === "PPNG") {
            return calculatePPNG(calculator);
        } else {
            return tableLookupForUltraFerm(calculator)
        }
    } catch (error) {
        throw error;
    }

}

const calculatePPNG = (calculate) => {
    var grav = convertPPNGGravityToPlato(calculate);

    const packageMultiplier =
        ((calculate.tempUnit == 'F' && parseFloat(calculate.ppngTempVal) <= 62) || (calculate.tempUnit == 'C' && parseFloat(calculate.ppngTempVal) <= 16.6667))
        ? 2
        : 1;

    const proPacks = (Number((calculate.ppngVolVal * calculate.volume) / 500) * packageMultiplier).toFixed(2);
    const hbPacks = (Number((calculate.ppngVolVal * calculate.volume) / 19) * packageMultiplier).toFixed(2);

    var proPitch = (((2150000000 * 1750) * proPacks) / ((calculate.ppngVolVal * calculate.volume) * 1000) / 1000000).toFixed(2);
    var hbPitch = (((2150000000 * 70) * (hbPacks) / ((calculate.ppngVolVal * calculate.volume) * 1000)) / 1000000).toFixed(2);

    var resultproPitchPlato = (proPitch / grav).toFixed(2);
    var resulthbPitchPlato = (hbPitch / grav).toFixed(2);

    var ppngBarMin = 0.1;
    var ppngBarMax = 3;

    var ppngBarUnderpitchingBelow = 0.42;
    var ppngBarIdealLow = 0.42;
    var ppngBarIdealHigh = 0.83;
    var ppngBarOverpitchingAbove = 1.6;

    switch (packageMultiplier) {
        case 2:
        case 3:
            ppngBarUnderpitchingBelow = 0.84;
            ppngBarIdealLow = 0.84;
            ppngBarIdealHigh = 1.68;
            ppngBarOverpitchingAbove = 2.25;
            break;
        case 1:
        default:
            // Leave assigned values unchanged
            break;
    }

    var proPackResultToUse = proPacks;

    // Make adjustments due to over or under pitching
    if (resultproPitchPlato < ppngBarIdealLow || proPackResultToUse < 1) {
        while (resultproPitchPlato < ppngBarIdealLow || proPackResultToUse < 1) {
            // Pitch rate too low or less than one pack suggested; bump up to the next integer and recalculate pitch rate
            if (proPackResultToUse == Math.ceil(proPackResultToUse)) {
                proPackResultToUse++;
            } else {
                proPackResultToUse = Math.ceil(proPackResultToUse);
            }
            proPitch = (((2150000000 * 1750) * proPackResultToUse) / ((calculate.ppngVolVal * calculate.volume) * 1000) / 1000000).toFixed(2);
            resultproPitchPlato = (proPitch / grav).toFixed(2);
        }
    } else if (proPackResultToUse != Math.floor(proPackResultToUse) && proPackResultToUse > 1) {
        // See if we can bump down to the next lower whole number without going below the minimum
        var nextLower = Math.floor(proPackResultToUse);
        // See if that actually changed anything or if we need to remove a pack (if there's no decimal, Math.floor won't do anything)
        if (nextLower == proPackResultToUse) nextLower--;
        var nextLowerPitchRate = (((2150000000 * 1750) * nextLower) / ((calculate.ppngVolVal * calculate.volume) * 1000) / 1000000).toFixed(2);
        var nextLowerPitchRatePlato = (nextLowerPitchRate / grav).toFixed(2);

        if (nextLowerPitchRatePlato >= ppngBarIdealLow) {
            proPackResultToUse = nextLower;
            proPitch = parseFloat(nextLowerPitchRate);
            resultproPitchPlato = parseFloat(nextLowerPitchRatePlato);
        } else {
            // Per discussion with Mike, bump up to the next whole number if we can't bump down to the previous one
            proPackResultToUse = Math.ceil(proPacks);
            // We don't need to check here to see if Math.ceil did anything -- we know it did, because we're only here because Math.floor did something.
            proPitch = (((2150000000 * 1750) * proPackResultToUse) / ((calculate.ppngVolVal * calculate.volume) * 1000) / 1000000).toFixed(2);
            resultproPitchPlato = (proPitch / grav).toFixed(2);

        }
    }

    ppngBarMax = Math.max(ppngBarMax, Math.ceil(resultproPitchPlato));
    ppngBarIdealHigh = Math.max(ppngBarIdealHigh, ppngBarOverpitchingAbove);

    return {
        isPPNG: true,
        proPacks: proPacks,
        hbPacks: hbPacks,
        proPitch: proPitch,
        hbPitch: hbPitch,
        resultproPitchPlato: resultproPitchPlato,
        resulthbPitchPlato: resulthbPitchPlato,
        ppngWortVolumeL: (calculate.ppngVolVal * calculate.volume).toFixed(2),
        ppngWortGravity: grav,
        ppngPackageMultiplier: packageMultiplier,
        proPackResult: proPackResultToUse,
        resultProPitchRate: proPitch,
        ppngBarMin: ppngBarMin,
        ppngBarMax: ppngBarMax,
        ppngBarUnderpitchingBelow: ppngBarUnderpitchingBelow,
        ppngBarIdealLow: ppngBarIdealLow,
        ppngBarIdealHigh: ppngBarIdealHigh,
        ppngBarOverpitchingAbove: ppngBarOverpitchingAbove
    }
}

/*
export function getClarityFermResults(calculator) {
    const { responseSuccess, responseFailure } = action;
    try {
        const state = yield select();
        const enzymeType = enzymeSelector(state);
        const wortVolume = wortVolumeSelector(state);
        const beerType = beerTypeSelector(state);
        const homebrewType = homebrewTypeSelector(state);
        const wortGravity = wortGravitySelector(state);
        const maltValue = maltValueSelector(state);

        if (enzymeType === '5' && wortVolume && wortGravity && maltValue && beerType && enzymeType) {
            const maltValue = (+maltValue !== 100) ? 1 : maltValue / 100;
            const quantCalc = wortVolume * 1.173477658 * ((wortGravity * wortGravity * maltValue * beerType / 12) / 1000) * enzymeType;
            const calcResult = Math.round(quantCalc / 1.1 * 1000) / 100;
            yield put(responseSuccess({ calcResult }));
        } else if (enzymeType === '1') {
            const beerValue = +beerType === 6 ? 4 : 2;
            const calcResult = Math.round(homebrewType * beerValue * 100) / 100;
            yield put(responseSuccess({ calcResult }));
        } else {
            yield put(responseSuccess({ calcResult: null }));
        }
    } catch (error) {
        yield put(responseFailure(error));
    }
}
*/

function tableLookupForUltraFerm(calculator) {
    const low = Math.floor(calculator.poundOfGrains / 100 * 36);
    const mid = low * 2;
    const high = Math.floor(low * 4.028);
    return {
        ultraFerm: true,
        Low: low,
        Mid: mid,
        High: high,
        poundsOfGrain: calculator.poundOfGrains
    }
}

function tableLookUpForFanMax(calculator) {
    let selectedProduct = SalesLib.listProducts.results.filter(val => val.product == calculator.product)[0];
    let selectedunits = selectedProduct.range.filter(val => val.unit == calculator.fanMaxUnits)[0]
    return {
        isFanmax: true,
        calculatedMax: selectedunits.high * selectedProduct.high * calculator.volume,
        calculatedMin: selectedunits.low * selectedProduct.low * calculator.volume,
        total: calculator.volume,
        unit: calculator.fanMaxUnits,
        product: calculator.product
    }
}

function tableLookup(calculator) {

    try {
        var temp = convertTemp(calculator);
        var vol = convertVol(calculator);
        var grav = convertGrav(calculator);
        const { isHomebrewer } = calculator;
        if (isHomebrewer && SalesLib.homebrewPackSizeChart[temp] && SalesLib.homebrewPackSizeChart[temp][vol] && SalesLib.homebrewPackSizeChart[temp][vol][grav]) {
            var total = SalesLib.homebrewPackSizeChart[temp][vol][grav];

            return { isHomebrewer, total };
        } else if (SalesLib.packSizeChart[temp] && SalesLib.packSizeChart[temp][vol] && SalesLib.packSizeChart[temp][vol][grav]) {
            var total = SalesLib.packSizeChart[temp][vol][grav];

            //Pro Warning
            if (!isHomebrewer && total < 0.450) {
                return;
            }
            return total;

        } else {
            throw { message: 'value does not exist in table lookup', code: 0 };
        }
    } catch (error) {
        throw error;
    }
}

function custom(params) {
    try {
        var { startingGravity, targetPitchRate, volume, viability, cellCount, quaUnit } = params;

        startingGravity = parseFloat(startingGravity);
        targetPitchRate = parseFloat(targetPitchRate);
        volume = parseFloat(volume);
        viability = parseFloat(viability);
        cellCount = parseFloat(cellCount);
        quaUnit = parseFloat(quaUnit)

        return (((startingGravity * targetPitchRate * volume * quaUnit) / ((viability / 100) * cellCount)) / 1000).toFixed(1);
    } catch (error) {
        throw error;
    }
}

function getPacks(total, isCustom) {

    try {
        var packs = findMinPacks(total);
        return { total, packs, isCustom };
    } catch (error) {
        throw error;
    }

}

function findMinPacks(totalVol) {

    var packs = {
        '0.5': 0,
        //'1.5': 0,
        '2.0': 0
    }

    while (totalVol != 0) {
        if (totalVol % 2 == 0) {
            packs['2.0']++;
            totalVol -= 2
        } else if (totalVol % 3 == 0) {
            //packs['1.5'] += 2;
            packs['2.0']++;
            packs['0.5'] += 2;
            totalVol -= 3;
        } else if (totalVol - 2 >= 0) {
            packs['2.0']++;
            totalVol -= 2;
        } else if (totalVol - 1.5 >= 0) {
            //packs['1.5']++;
            packs['0.5'] += 3;
            totalVol -= 1.5;
        } else if (totalVol - 0.5 >= 0) {
            packs['0.5']++;
            totalVol -= 0.5;
        } else {
            break;
        }
    }

    return packs;
}

export function getTotalAmount(purchasePacks) {
    return Object.keys(purchasePacks).reduce(function (previous, key) {

        var float = parseFloat(key);

        if (!isNaN(key)) {
            return previous + (float * purchasePacks[float.toFixed(1)]);
        }
        return previous;

    }, 0);
}

function getClosestAmount(purchasePackAmounts, totalVol, knownResults) {

    // default pack
    var minPack = {
        '0.5': 0,
        //'1.5': 0,
        '2.0': 0,
        num: 0
    }

    // base case # 1: totalVol is 0
    if (totalVol === 0) {
        return Object.assign({}, minPack);
    }
    // base case # 2: optimal amount for totalVol has already been calculated. retrieve and return.
    else if (knownResults[totalVol]) {
        return Object.assign({}, knownResults[totalVol]);
    }

    // base case # 3: totalVol is one of purchasePackAmounts
    else if (purchasePackAmounts.includes(totalVol)) {
        minPack[totalVol.toFixed(1)] = 1;
        minPack.num += 1;
        knownResults[totalVol] = minPack;
        return Object.assign({}, minPack);
    } else {

        // flag to determine if no optimal solution was found for a particular totalVol
        var flag = false;

        // min will hold the current min of
        var min = Number.MAX_VALUE;

        // array keeps optimal packs for each type less than totalVol
        var subsetOptimalAmounts = [];

        // index of current min inside subsetOptimalAmounts
        var minIndexOfOptimalAmounts = 0;

        // holds the type of the pack indexed at minIndexOfOptimalAmounts needed to get to totalVol
        var minType = '';


        for (var i = 0; i < purchasePackAmounts.length; i++) {
            var type = purchasePackAmounts[i];
            if (type <= totalVol) {
                var pack = getClosestAmount(purchasePackAmounts, totalVol - type, knownResults);
                subsetOptimalAmounts.push(pack);

                // found at least one pack where num > 0
                if (pack.num > 0) {
                    flag = true;
                }

                // found new pack with less quantity of types than the previous min
                // make this the new min and preserve index
                if (pack.num < min) {
                    min = pack.num;
                    minType = type;
                    minIndexOfOptimalAmounts = subsetOptimalAmounts.length - 1;
                }
            }
        }

        // if flag === true: we know there is at least one optimal solution LESS than
        // totalVol from which we can reach totalVol.
        // calculate the optimal solution of totalVol based on this type.
        if (flag) {
            minPack = subsetOptimalAmounts[minIndexOfOptimalAmounts];
            minPack[(minType).toFixed(1)] += 1;
            minPack.num += 1;
            knownResults[totalVol] = minPack;

        }

    }

    return Object.assign({}, minPack);

}


function getOptimalAmount(purchasePackAmounts, totalVol, knownResults) {
    var initialVol = totalVol;

    var amount = getClosestAmount(purchasePackAmounts, totalVol, knownResults);

    // found exact amount
    if (amount.num > 0) {
        return amount;
    }
    // no exact amount, find nearest neighbor with exact amount
    else {
        // should only get here once if no match was found for initialVol at end of recursion
        // code block finds next optimal solution within 0.5 range of initialVol
        if (initialVol === totalVol) {
            var newVol = initialVol + 0.5;
            for (var i = initialVol + 0.1; i <= newVol; i += 0.1) {

                // eliminate Javascript rounding errors
                var float = parseFloat((i).toFixed(1));

                var pack = getClosestAmount(purchasePackAmounts, float, knownResults);

                // if we find a pack in which we can calculate the exact amount (above initialVol)
                // then this is the optimal amount for initialVol
                if (pack.num != 0) {
                    return pack;
                }
            }
        }
    }
}