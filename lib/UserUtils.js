import _isEqual from 'lodash/isEqual';
import _isEmpty from 'lodash/isEmpty';
import _size from 'lodash/size';
import _get from 'lodash/get';
import _filter from 'lodash/filter';

import WLHelper from './WLHelper';
import SalesLib from './SalesLib';
const noDefaultShipMethod=[
{ NSID: "2787", Name: "Priority Overnight (Recommended Pro)", Subsidiary: "White Labs Inc", CustomerVisible: true, USinternational: false, SubsidiaryID: 2 },
{ NSID: "2844", Name: "FedEx International Priority", Subsidiary: "White Labs Inc", CustomerVisible: false, USinternational: true, SubsidiaryID: 2 },
]
export const prepareUserInfo = userInfo => {
    const { 
        subsidiary, 
        connectedaccounts, 
        shipmethod, 
        shipping: { 
            countryid 
    }} = userInfo;

    var loginSubsidiary = subsidiary;
    userInfo.subsidiaryOptions = loadSubsidiaryOptions(connectedaccounts);

    if (userInfo.isLogin && userInfo.subsidiaryOptions && userInfo.subsidiaryOptions.length > 1) {
        if (userInfo.subsidiaryOptions.includes(7)) {
            loginSubsidiary = 7;
        } else if (userInfo.subsidiaryOptions.includes(5)) {
            loginSubsidiary = 5;
        }
        userInfo.subsidiary = loginSubsidiary;
    }

    userInfo.isRetailer = [2, 7].includes(parseInt(userInfo.category));
    userInfo.isPro = [1, 4, 5, 6, 10, 11].includes(parseInt(userInfo.category));
    userInfo.isEndUser = [3, 9].includes(parseInt(userInfo.category));
    userInfo.isWhiteLabsEntity = [8].includes(parseInt(userInfo.category));

    if (userInfo.isRetailer) {
        userInfo.customerCategory = SalesLib.CUSTOMER_TYPE_RETAILER;
    } else if (userInfo.isPro) {
        userInfo.customerCategory = SalesLib.CUSTOMER_TYPE_PRO;
    } else if (userInfo.isEndUser) {
        userInfo.customerCategory = SalesLib.CUSTOMER_TYPE_END_USER;
    } else if (userInfo.isWhiteLabsEntity) {
        userInfo.customerCategory = SalesLib.CUSTOMER_TYPE_WHITE_LABS;
    } else {
        userInfo.customerCategory = SalesLib.CUSTOMER_TYPE_NOT_DEFINED;
    }

    userInfo.shipMethods = WLHelper.shipMethodGroup(loginSubsidiary, shipmethod, countryid, userInfo.customerCategory);
    //if no default ship method available set default ship method according to user country
    if(!userInfo.shipmethod){
        if(userInfo.shipping.countryid==="US"){
            userInfo.shipMethods.push(noDefaultShipMethod[0])
            userInfo.shipmethod="2787"
        }
        else{
            userInfo.shipMethods.push(noDefaultShipMethod[1])
            userInfo.shipmethod="2844" 
        }
    }
    
    userInfo.currencyOptions = loadCurrencyOptions(loginSubsidiary);
    userInfo.defaultShipMethodId = shipmethod;
    userInfo.isOnCreditHold = (userInfo.creditHold && userInfo.creditHold === 'ON');
};

const loadSubsidiaryOptions = connectedaccounts => {

    let subsidiaryOptions = [];

    if (connectedaccounts) {
        for (let i = 0; i < connectedaccounts.length; i++) {
            let value = parseInt(connectedaccounts[i].subsidiaryid);
            subsidiaryOptions.push(value);
        }
    } else {
        // USA by default
        subsidiaryOptions.push(2);
    }

    // USA
    if(!subsidiaryOptions.includes(2)) {
        subsidiaryOptions.push(-2);
    }

    // HK
    if(!subsidiaryOptions.includes(5)) {
        subsidiaryOptions.push(-5);
    }

    // CPH
    if(!subsidiaryOptions.includes(7)) {
        subsidiaryOptions.push(-7);
    }

    return subsidiaryOptions;
}

const loadCurrencyOptions = subsidiary => {

    let currencyOptions = [];

    if(subsidiary == 7){
        currencyOptions = _filter(SalesLib.CURRENCY_MAP, {subsidiary});
    }
   
    return currencyOptions;

}
