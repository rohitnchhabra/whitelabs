import _filter from 'lodash/filter';

import SalesLib from './SalesLib';
import WLHelper from './WLHelper';
import { IN_STOCK, OUT_OF_STOCK } from './Constants';

export function parseAvailabilityResults(availability, subsidiary, quantityRequested, availabilityMessage) {
    var key = (availability == null ? OUT_OF_STOCK : (availabilityMessage ? availabilityMessage : (availability ? IN_STOCK : OUT_OF_STOCK)));

    if (key == IN_STOCK) {
        if (quantityRequested > 0) {
            key = 'At least ' + quantityRequested + ' ' + key;
        } else {
            key = 'Item ' + key;
        }
    } else if (key == OUT_OF_STOCK) {
        if (quantityRequested > 0) {
            key = 'At least ' + quantityRequested + ' ' + key;
        } else {
            key = 'Item ' + key;
        }
    }

    return key + " in " + WLHelper.getSubsidiaryLabel(subsidiary);
}

export function filterItems(
    items,
    type=null,
    search=null,
    userInfo=null,
    homebrew=false,
    sortByDate = false,
    vaultPreorderOnly = false
) {
    var subsidiary,
        userID,
        userIsRetailer = false,
        category = parseInt(type);

    if (userInfo) {
        subsidiary = (userInfo.subsidiary === '' ? 2 : parseInt(userInfo.subsidiary));
        userIsRetailer = userInfo.isRetailer;
        userID = userInfo.id;
    } else {
        subsidiary = 2;
        userIsRetailer = false;
    }

    if (search) {
        search = search.toLowerCase();
    }

    var itemsToShow = items.filter(function (item) {
        var name = item.Name ? item.Name : '';
        var styleRec = item.styleRec ? item.styleRec : '';
        var searchTags = item.searchTags ? item.searchTags : '';
        var description = item.Description ? item.Description : '';
        var partNum = item.partNum ? item.partNum : '';
        var beerStyles = item.beerStylesSearch ? item.beerStylesSearch : '';
        const salesCategory = parseInt(item.salesCategory);

        if (vaultPreorderOnly) {
            return item.isVaultPreorderItem;
        } else if (subsidiary == 7 && item.hasOrganicVersion) {
            // CPH: Don't show regular version of strain if there's an organic version
            return false;
        }

        if (homebrew) {
            var includeItem = false;
            if (!item.IsPrivate[0] || item.IsPrivate.indexOf(userID) >= 0) {
                if (item.volID[4] && SalesLib.SALESCATEGORY[16].includes(item.salesCategory)) {
                    if (isNaN(category) || SalesLib.SALESCATEGORY[category].indexOf(salesCategory) >= 0) {
                        includeItem = true;
                    }
                }

                if (!includeItem && SalesLib.SALESCATEGORY[8].includes(item.salesCategory)) {
                    // Inclusion overrides for nutrients & enzymes
                    includeItem = (item.designation == "1"); //(SalesLib.RetailEnzNutCodes.includes(item.partNum));
                } else if (item.salesCategory == 4 && item.strainCategory == 34) {
                    // Inclusion overrides for Kombucha and American Farmhouse
                    includeItem = (
                           item.partNum.indexOf('WLP600') >= 0
                        || item.partNum.indexOf('WLP630') >= 0
                        || item.partNum.indexOf('WLP645') >= 0
                        || item.partNum.indexOf('WLP648') >= 0
                        || item.partNum.indexOf('WLP650') >= 0
                        || item.partNum.indexOf('WLP653') >= 0
                        || item.partNum.indexOf('WLP655') >= 0
                        || item.partNum.indexOf('WLP661') >= 0
                        || item.partNum.indexOf('WLP670') >= 0
                        || item.partNum.indexOf('WLP672') >= 0
                        || item.partNum.indexOf('WLP675') >= 0
                        || item.partNum.indexOf('WLP677') >= 0
                    );
                }
            }

            return includeItem;
        }
        else if ((WLHelper.warehouseMatch(item.warehouse, subsidiary) && (!SalesLib.POSItems.includes(item.volID[0])
        || (SalesLib.POSItems.includes(item.volID[0]) && userIsRetailer)))
        || (item.isOrganic && subsidiary == 2) /*Show organic items to the US subsidiary*/)
        {
            if(item.IsPrivate.indexOf(userID) >= 0 || !item.IsPrivate[0])
            {
                if(search)
                {
                    if(name.toLowerCase().includes(search))
                    {
                        return true;
                    }
                    else if(styleRec.toLowerCase().includes(search))
                    {
                        return true;
                    }
                    else if(searchTags.toLowerCase().includes(search))
                    {
                        return true;
                    }
                    else if(description.toLowerCase().includes(search))
                    {
                        return true;
                    }
                    else if(partNum.toLowerCase().includes(search))
                    {
                        return true;
                    }
                    else if(beerStyles.toLowerCase().includes(search))
                    {
                        return true;
                    }
                } else if (category == 998) {
                    return item.isOrganic;
                }
                else if (!isNaN(category) && SalesLib.SALESCATEGORY[category].indexOf(salesCategory) >= 0 && !(homebrew && !item.volID[4]))
                {
                    if (!item.isOrganic || subsidiary == 7) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

        return false;
    });

    var itemGroups = null;
    var noGroupItems = [];
    for (var i = 0; i < itemsToShow.length; i++) {
        var item = itemsToShow[i];
        if (item.itemGroup && item.itemGroup.length > 0) {
            if (!itemGroups) itemGroups = [];
            for (var j = 0; j < item.itemGroup.length; j++) {
                var lsCategory = item.itemGroup[j];
                if (!itemGroups[lsCategory]) itemGroups[lsCategory] = [];
                itemGroups[lsCategory].push(item);
            }
        } else {
            noGroupItems.push(item);
        }
    }

    var keys = null;
    if (itemGroups) keys = Object.keys(itemGroups);
    if (!search && keys && keys.length > 0) {
        itemsToShow = setItemGroups(keys, itemGroups, noGroupItems);
        itemsToShow.sort(compareFunctionGroup);
    } else {
        if (sortByDate) {
            itemsToShow.sort(compareFunctionDate);
        }
        else if (homebrew) {
            itemsToShow.sort(compareFunctionHB);
        }
        else {
            itemsToShow.sort(compareFunction);
        }
    }

    return itemsToShow;
}

function setItemGroups(keys, itemGroups, noGroupItems) {
    var itemsToShow = [];
    for (var i = 0; i < keys.length; i++) {
        var items = itemGroups[keys[i]];
        for (var j = 0; j < items.length; j++) {
            var item = Object.assign({}, items[j]);
            item.group = keys[i];
            itemsToShow.push(item);
        }
    }

    if (noGroupItems.length > 0) {
        for (var j = 0; j < noGroupItems.length; j++) {
            var item = noGroupItems[j];
            item.group = 'Other';
            itemsToShow.push(item);
        }
    }

    return itemsToShow;
}

function compareFunction(item1, item2) {
    if(item1.Name.includes('WLP') && item2.Name.includes('WLP'))
    {
        return item1.Name.slice(3).localeCompare(item2.Name.slice(3));
    }
    else if(item1.Name.includes('WLP'))
    {
        return -1;
    }
    else if(item2.Name.includes('WLP'))
    {
        return 1;
    }
    else
    {
        return item1.Name.localeCompare(item2.Name);
    }
}

function compareFunctionHB(item1, item2) {
    if (item1.strainCategory == item2.strainCategory) {
        if (item1.salesCategory == item2.salesCategory) {
            return compareFunction(item1, item2);
        } else if (item1.salesCategory < item2.salesCategory) {
            return -1;
        } else {
            return 1;
        }
    } else {
        if (item1.salesCategory == item2.salesCategory) {
            if (item1.strainCategory < item2.strainCategory) {
                return -1;
            } else {
                return 1;
            }
        } else {
            if (item1.salesCategory < item2.salesCategory) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}

function compareFunctionGroup(item1, item2) {
    var sortMap = [
        'Most Popular',     
        'Beer',             
        'Kombucha',         
        'Wine/Cider',       
        'Distilled Spirit', 
        'Distilled Spirits', 
        'Microbial',        
        'Yeast/Bacteria',   
        'Water',
        'Water or Wort',    
        'Malt',             
        'Hops',             
        'Other'             
    ];

    if (item1.group == item2.group) {
        return compareFunction(item1, item2);
    } else {
        var index1 = sortMap.indexOf(item1.group);
        var index2 = sortMap.indexOf(item2.group);

        if (index1 == index2) {
            return compareFunction(item1, item2);
        } else if (index1 == -1) {
            // Not found
            return 1;
        } else if (index2 == -1) {
            // Not found
            return -1;
        } else if (index1 < index2) {
            return -1;
        } else {
            return 1;
        }
    }
}

function convertToDate(date) {
    try {
        let [month, day, year] = date.split(" ");
        day = day.match(/\d+/g).map(Number)[0];
        const dateObj = new Date(`${month}/${day}/${year}`);
        dateObj.setHours(0, 0, 0, 0);
        return dateObj;
    } catch (error) {
        console.log("Error parsing WLEDU item date '" + date + "'");
        return new Date();
    }
}

function compareFunctionDate(item1, item2) {
    if (!item1.TagDate && !item2.TagDate) {
        return compareFunction(item1, item2);
    } else if (!item1.TagDate) {
        // This has no date but the other item does so sort this one on top
        return -1;
    } else if (!item2.TagDate) {
        // Other item has no date but this one does, so sort other item on top
        return 1;
    } else {
        return convertToDate(item1.TagDate) - convertToDate(item2.TagDate);
    }
}
