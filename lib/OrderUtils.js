import Utils from './Utils';
import SalesLib from './SalesLib';
import WLHelper from './WLHelper';

const dev = process.env.NODE_ENV != "production";
const gelPackItemId = 4328;
const unsplittableItems = [gelPackItemId, 3068, 16225, 16227, 16228, 16229, 16230];

const qcDayItems = (dev ?
    // Development IDs
    [16845, 16846, 16847, 16848, 17246, 17247, 17248, 17249, 17250, 17251, 17252, 17253, 17254, 17255, 17256, 17257]
    // Production IDs
    : [4662, 17272, 17273, 17274, 17283, 17279, 17280, 17281, 17282, 17276, 17277, 17278, 17284, 17269, 17270, 17271, 17285]
);

export const initOrder = (order, user, cart) => ({
    ...order,
    items: changeShippingOption(initDates(order, user), user, 'Earliest For Each'),
    removedItems: checkForRemovedItems(order, cart)    
});

/*
 * Initialize items with valid ship and delivery dates
 */
const initDates = (order, user) => ({
    ...order,
    items: (!order.items ? [] : order.items.map(item => {
        var shipDate = new Date(item.shipDate);
        var deliveryDate = getDeliveryDate(order, user, shipDate);
        const AllWarehouses = item.AllWarehouses;

        if (!qcDayItems.includes(item.MerchandiseID)) {
            if (item.isVaultPreorderItem) {
                // Vault preorder items -- no ship date
                shipDate = new Date(8640000000000000);
                deliveryDate = new Date(8640000000000000);
            } else {
                while (!checkDeliveryDate(user, deliveryDate) || !checkShipDate(user, shipDate)) {
                    deliveryDate.setDate(deliveryDate.getDate() + 1);
                    shipDate.setDate(shipDate.getDate() + 1);
                }
                item.shipDate = shipDate;
                item.deliveryDate = deliveryDate;
            }
        }

        if (unsplittableItems.includes(item.MerchandiseID)) {
            checkForUnsplittableItem(item, order.items, order, user);
            if (item.MerchandiseID == gelPackItemId) {
                // Gel pack
                shipDate = new Date(item.shipDate);
                deliveryDate = getDeliveryDate(order, user, shipDate);
            }
        }

        const OriginalWarehouse = item.Warehouse;

        return {
            ...item,
            deliveryDate,
            shipDate,
            earliestShipDate: new Date(shipDate),
            OriginalWarehouse,
            AllWarehouses,
        }
    })
    )
});

export const setDistributedItems = (localItems, orderItems) => {
    for (var i = 0; i < localItems.length; i++) {
        if (localItems[i].isDistributed) {
            if (!orderItems.includes(localItems[i])) {
                orderItems.push(localItems[i]);
            }
        }
    }
};

export const checkForUnsplittableItem = (item, allItems, order, user) => {
    var whFound = false;
    var hasGelPacks = false;

    // Item WH are arrays but currently each is only expected to have 1 entry
    // If/when we can split orders among 3 or more warehouses, some assumptions
    // below will need to be updated, and ship dates will need to be explicitly
    // assigned to warehouses.

    // "Keys" arrays store just the date, "Dates" arrays store one representative ship date
    // for each date in the corresponding "Keys" array
    // This is a hacky thing to help get around NS TZ adjustments (which seem to be affecting gel pack items)
    // without rewriting the gel pack logic here.
    var otherItemWh = [];
    var otherItemWhShipDates = [];
    var otherItemWhShipDateKeys = [];

    var thisItemWh = [];
    var thisItemWhShipDates = [];
    var thisItemWhShipDateKeys = [];

    for (var i = 0; i < allItems.length; i++) {
        var thisItem = allItems[i];
        if (thisItem.MerchandiseID == gelPackItemId) {
            // Don't consider gel packs when calculating ship date lists, 
            // since they ALWAYS have to ship with other items. Do capture
            // the warehouse though.
            if (!thisItemWh.includes(thisItem.Warehouse)) thisItemWh.push(thisItem.Warehouse);
            hasGelPacks = true;
            continue;
        } else if (thisItem.MerchandiseID == item.MerchandiseID) {
            if (!thisItemWh.includes(thisItem.Warehouse)) thisItemWh.push(thisItem.Warehouse);
            if (!otherItemWhShipDateKeys.includes(justDateString(thisItem.shipDate))) {
                otherItemWhShipDateKeys.push(justDateString(thisItem.shipDate));
                otherItemWhShipDates.push(thisItem.shipDate);
            }
            continue;
        } else if (unsplittableItems.includes(thisItem.MerchandiseID)) {
            // These items can ship on their own, but if other items are
            // ordered, they will preferentially ship with them, so don't
            // allow them to drag other items along
            continue;
        } else if (
            // If item that came in as a parameter is not a gel pack, any other item will qualify
            item.MerchandiseID != gelPackItemId
            // Item is a gel pack, so make sure it is coming from the same warehouse as a yeast item
        || thisItem.type == 2
        || thisItem.salesCategory == 2
        || thisItem.salesCategory == 3
        || thisItem.salesCategory == 4
        || thisItem.salesCategory == 5
        || thisItem.salesCategory == 6
        || thisItem.salesCategory == 7
        || thisItem.salesCategory == 32) {
            if (thisItem.Warehouse == item.Warehouse) {
                whFound = true;
                if (!thisItemWhShipDateKeys.includes(justDateString(thisItem.shipDate))) {
                    thisItemWhShipDateKeys.push(justDateString(thisItem.shipDate));
                    thisItemWhShipDates.push(thisItem.shipDate);
                }
            } else {
                if (!otherItemWh.includes(thisItem.Warehouse)) {
                    otherItemWh.push(thisItem.Warehouse);
                    otherItemWhShipDateKeys.push(justDateString(thisItem.shipDate));
                    otherItemWhShipDates.push(thisItem.shipDate);
                } else if (!otherItemWhShipDateKeys.includes(justDateString(thisItem.shipDate))) {
                    otherItemWhShipDateKeys.push(justDateString(thisItem.shipDate));
                    otherItemWhShipDates.push(thisItem.shipDate);
                }
            }
        }
    }

    if (!whFound) {
        if (otherItemWh.length > 0) {
            item.Warehouse = otherItemWh[0];
            if (item.MerchandiseID == gelPackItemId) item.shipDate = otherItemWhShipDates[0];
        }
    } else if (item.MerchandiseID == gelPackItemId)  {
        if (thisItemWhShipDates.length > 0) {
            // Make sure the gel pack ships on the same date as another item from this warehouse
            item.shipDate = thisItemWhShipDates[0];
        } else {
            // Okay fine ship with something from the other warehouse then
            item.shipDate = otherItemWhShipDates[0];
        }
    }

    if (hasGelPacks) {
        if (otherItemWh.length > 0 && item.MerchandiseID == gelPackItemId) {
            // We need to include the ice packs in with the other warehouse order too
            for (var i = 0; i < otherItemWh.length; i++) {
                if (!thisItemWh.includes(otherItemWh[i]) && item.Warehouse != otherItemWh[i]) {
                    var newItem = {};
                    newItem.Name = item.Name;
                    newItem.salesCategory = item.salesCategory;
                    newItem.dispQuantity = item.dispQuantity;
                    newItem.OrderDetailQty = item.OrderDetailQty;
                    newItem.MerchandiseID = item.MerchandiseID;
                    newItem.Warehouse = otherItemWh[i];
                    newItem.OriginalWarehouse = newItem.Warehouse;
                    newItem.shipDate = otherItemWhShipDates[i];
                    newItem.earliestShipDate = new Date(newItem.shipDate);
                    newItem.AllWarehouses = item.AllWarehouses;
                    newItem.details = item.details;
                    newItem.pricePerUnit = item.pricePerUnit;
                    newItem.type = item.type;
                    newItem.isDistributed = true;
                    newItem.originalQty = newItem.OrderDetailQty;
                    newItem.deliveryDate = getDeliveryDate(order, user, new Date(newItem.shipDate));
                    allItems.push(newItem);
                }
            }

            // See if we need to distribute ice packs across more ship dates
            setGelPackShipDates(user, order, item, otherItemWh, otherItemWhShipDates, allItems);
        }

        if (item.MerchandiseID == gelPackItemId) {
            // See if we need to distribute ice packs across more ship dates
            setGelPackShipDates(user, order, item, thisItemWh, thisItemWhShipDates, allItems);
        } else {
            // We may be adjusting the ship date for something that has or needs a gel pack
            for (var i = 0; i < allItems.length; i++) {
                var gpItem = allItems[i];
                if (gpItem.MerchandiseID == gelPackItemId && gpItem.Warehouse == item.Warehouse) {
                    // See if we need to distribute ice packs across more ship dates
                    setGelPackShipDates(user, order, gpItem, thisItemWh, thisItemWhShipDates, allItems);
                    break;
                }
            }
        }

        // Now eliminate duplicate or unnecessary gel packs
        for (var i = allItems.length - 1; i >= 0; i--) {
            var gpItem = allItems[i];
            if (gpItem && gpItem.MerchandiseID == gelPackItemId) {
                for (var j = i - 1; j >= 0; j--) {
                    var gpItem2 = allItems[j];
                    if (gpItem2.MerchandiseID == gpItem.MerchandiseID && gpItem2.Warehouse == gpItem.Warehouse) {
                        if (justDateString(gpItem.shipDate) == justDateString(gpItem2.shipDate)) {
                            // Duplicate gel pack
                            allItems.splice(j, 1);
                        } else if (gpItem.Warehouse == item.Warehouse && !thisItemWhShipDateKeys.includes(justDateString(gpItem.shipDate))) {
                            // Unnecessary gel pack
                            allItems.splice(j, 1);
                        }
                    }
                }
            }
        }
    }
}

export const justDateString = (date) => {
    var d = new Date(date);
    d.setHours(0, 0, 0, 0);
    return d.toDateString();
}

const setGelPackShipDates = (user, order, item, itemWh, itemWhShipDates, allItems) => {
    for (var i = 0; i < itemWhShipDates.length; i++) {
        var shipDateFound = false;

        for (var j = 0; j < allItems.length; j++) {
            var gpItem = allItems[j];
            if (justDateString(gpItem.shipDate) == justDateString(itemWhShipDates[i]) && gpItem.Warehouse == itemWh[0] && gpItem.MerchandiseID == item.MerchandiseID) {
                item.shipDate = itemWhShipDates[i];
                shipDateFound = true;
                break;
            }
        }

        if (shipDateFound) {
            continue;
        } else {
            var newItem = {};
            newItem.Name = item.Name;
            newItem.salesCategory = item.salesCategory;
            newItem.dispQuantity = item.dispQuantity;
            newItem.OrderDetailQty = item.OrderDetailQty;
            newItem.MerchandiseID = item.MerchandiseID;
            newItem.Warehouse = itemWh[0];
            newItem.OriginalWarehouse = newItem.Warehouse;
            newItem.shipDate = itemWhShipDates[i];
            newItem.earliestShipDate = new Date(newItem.shipDate);
            newItem.AllWarehouses = item.AllWarehouses;
            newItem.details = item.details;
            newItem.pricePerUnit = item.pricePerUnit;
            newItem.type = item.type;
            newItem.isDistributed = true;
            newItem.originalQty = newItem.OrderDetailQty;
            newItem.deliveryDate = getDeliveryDate(order, user, new Date(newItem.shipDate));
            allItems.push(newItem);
        }
    }
}

const checkForRemovedItems = (order, cart) => {
    var removedItems = cart.items.filter(element => {
        return (!order.items || !order.items.find(item => item.MerchandiseID == element.MerchandiseID))
    });
    return removedItems;
}

/*
 * Utility function to check for valid delivery date
 */
const checkDeliveryDate = (user, deliveryDate) => {
    const subsidiary = user.subsidiary;
    const saturday = deliveryDate.getDay() == 0;
    const sunday = deliveryDate.getDay() == 6;
    const holiday = Utils.checkHoliday(deliveryDate, subsidiary, false);

    if (user.shipmethod === '2791' || user.shipmethod === '4') {
        // Ground methods, allow Saturday or Sunday delivery
        return !holiday;
    } else {
        return !(saturday || sunday || holiday);
    }
};

/*
 * Utility function to check for valid ship date
 */
const checkShipDate = (user, shipDate) => {
    const subsidiary = user.subsidiary;
    const saturday = shipDate.getDay() == 0;
    const sunday = shipDate.getDay() == 6;
    const holiday = Utils.checkHoliday(shipDate, user.subsidiary, true);

    if (user.shipmethod == 3470 && shipDate.getDay() != 3) {
        return false;
    } else if ((user.shipmethod == 17237 || user.shipmethod == 2792) && shipDate.getDay() >= 3) {
        //FedEx One Rate and Ground (Cans Only), no shipping after Tuesday
        return false;
    } else if ((shipDate.getDay() < 1 || shipDate.getDay() > 3) && subsidiary == 7) { //International only monday tuesday
        return false;
    } else if (saturday || sunday || holiday) {
        return false;
    }
    return true;
};

const getDeliveryDate = (order, user, shipDate) => {
    const deliveryDate = new Date(shipDate);    
    let transitDelay = order.transitTimes[user.shipmethod];
    if (!transitDelay || !transitDelay.daysInTransit) {
        transitDelay = 1;
    } else {
        transitDelay = parseInt(transitDelay.daysInTransit);
    }

    deliveryDate.setDate(deliveryDate.getDate() + transitDelay);
    return deliveryDate;
};

/*
* Compute the delivery date range based on ship method
*/
const getDeliveryDateRange = (order, user, date) => {
    const deliveryDate = new Date(date);
    const transitTime = order.transitTimes[user.shipmethod];
    const range = transitTime ? parseInt(transitTime.daysInTransitRange) : 0;
    deliveryDate.setDate(deliveryDate.getDate() + range);
    return deliveryDate;
};

/*
 * Find earliest ship dates for each item
 */
export const earliestForAll = (order, user) => order.items.map(item => ({
    ...item,
    shipDate: item.earliestShipDate,
    deliveryDate: getDeliveryDate(order, user, new Date(item.earliestShipDate)),
    Warehouse: item.OriginalWarehouse,
    OrderDetailQty: (item.isDistributed ? item.originalQty : item.OrderDetailQty)
}));

/*
 * Find item with longest ship date interval and set all items' ship
 * dates to that item's ship date
 */
export const shipAllTogether = (order, user) => {

    const shipDates = order.items.map((item) => item.shipDate);

    // Find item with farthest ship date
    const index = Utils.findMaxDateIndex(shipDates);

    var wh = order.items[index].Warehouse;
    const isInternationalShipMethod = (user.shipmethod && WLHelper.isShipMethodInternational(user.shipmethod));
    if (isInternationalShipMethod && wh == 11) {
        // Per discussion with Mike, for now, if any of the items can't come from AVL, ship everything from SD
        for (var i = 0; i < order.items.length; i++) {
            if (!order.items[i].AllWarehouses.includes(wh)) {
                wh = 9;
                break;
            }
        }
    }

    var qcDayItemIDs = [4662, 9471, 9472, 16365, 16366, 16846];

    return order.items.map((item) => ({
        ...item,
        shipDate: (qcDayItemIDs.includes(item.MerchandiseID) ? item.shipDate : new Date(shipDates[index])),
        deliveryDate: (qcDayItemIDs.includes(item.MerchandiseID) ? item.deliveryDate : getDeliveryDate(order, user, new Date(shipDates[index]))),
        Warehouse: ((item.AllWarehouses.includes(wh) || isInternationalShipMethod) && !qcDayItemIDs.includes(item.MerchandiseID) ? wh : item.OriginalWarehouse),
        OrderDetailQty: (item.isDistributed ? 0 : item.OrderDetailQty)
    }))
};

export const changeShippingOption = (order, user, option) => {
    switch (option) {
        case 'Ship All Together':
            return shipAllTogether(order, user);
        case 'Custom':
        case 'Earliest For Each':
        default:
            return earliestForAll(order, user);
    }

};


/*
 * Find next closest ship and delivery dates for item
 */
export const incrementItemDate = (order, user, item) => {
    const shipDate = new Date(item.shipDate);
    const deliveryDate = getDeliveryDate(order, user, shipDate);

    if (!qcDayItems.includes(item.MerchandiseID)) {
        let foundDate;
        while (!foundDate) {
            deliveryDate.setDate(deliveryDate.getDate() + 1);
            shipDate.setDate(shipDate.getDate() + 1);

            if (checkDeliveryDate(user, deliveryDate) && checkShipDate(user, shipDate)) {
                foundDate = true;
            }
        }
    }

    return {
        ...item,
        deliveryDate,
        shipDate
    }
}

/*
 * Find previous closest ship and delivery dates for item
 */
export const decrementItemDate = (order, user, item) => {
    const shipDate = new Date(item.shipDate);
    const deliveryDate = getDeliveryDate(order, user, shipDate);

    if (!qcDayItems.includes(item.MerchandiseID)) {
        let foundDate;
        while (!foundDate && Utils.compareDates(shipDate, item.earliestShipDate) < 0) {
            deliveryDate.setDate(deliveryDate.getDate() - 1);
            shipDate.setDate(shipDate.getDate() - 1);

            if (checkDeliveryDate(user, deliveryDate) && checkShipDate(user, shipDate)) {
                foundDate = true;
            }
        }
    }

    return {
        ...item,
        deliveryDate,
        shipDate
    }
}

const createExtraParams = (order) => {
    let extraParams = '--From YMO v2--';

    for (var item of order.items) {
        if (item.extraParams) {
            for (var param in item.extraParams) {
                extraParams += `|${param}=${item.extraParams[param]}`;
            }
        }
    }

    return extraParams;
}

/*
 * Validate final order before placing it
 */
export const validateOrder = (order, user) => {
    if (user.subsidiary == 2 && SalesLib.USEmbargo.indexOf(user.shipping.countryid) >= 0) {
        throw { message: 'Your shipping address is from a country we cannot ship to directly. Please contact White Labs customer support to discuss alternatives or try switching your order region.', code: 0 };
    }

    if (!user.card.id && WLHelper.getPaymentTerm(user.terms) == 'Credit Card') {
        throw { message: 'No credit card on file. Please go to My Account > Manage Payment to add one.', code: 0 };
    }

    if (!user.shipmethod) {
        throw { message: 'Please select a shipping method.', code: 0 };
    }

    if (!user.shipping.id) {
        throw { message: 'Please select a shipping address.', code: 0 };
    }

    if (!user.billing.id) {
        throw { message: 'Please select a billing address.', code: 0 };
    }

    var finalOrder = new Object();
    finalOrder.salesrep = user.admin ? user.id : 43522; // Default sales rep to Yeastman

    finalOrder.comment = createExtraParams(order);

    const { id, shipmethod, shipping, billing, terms } = user;
    finalOrder.user = { id, shipmethod, shipping, billing };
    finalOrder.order = order;
    finalOrder.order.PONum = order.ponumber;
    finalOrder.order.terms = terms;
    finalOrder.order.cardId = user.card.id;
    return finalOrder;
}
