import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { orderActions } from 'appRedux/actions/orderActions';
import LoadingScreen from 'components/UI/LoadingScreen';


const PrepareOrder = Component => {
    const Wrapper = props => (
        class extends React.Component {
            UNSAFE_componentWillMount() {
                this.props.prepareOrder();
                localStorage.setItem('session', new Date());
            }

            render () {
                return <Component {...this.props}/>
            }
        }
    )

    return connect(
        state => ({ 
            user: state.user,
            cart: state.cart,
            message: state.messages,
            order: state.order,
            loading: state.loading
         }),
        dispatch => bindActionCreators({ ...orderActions}, dispatch)
    )(Wrapper());
};


export default PrepareOrder;
