const express = require("express");
const nextJS = require("next");
const bodyParser = require("body-parser");
const fs = require("fs");
const morgan = require("morgan");
const fetch = require("isomorphic-unfetch");
const winston = require("./server.config/winston");

const dev = process.env.NODE_ENV !== "production";
const app = nextJS({ dev });
const handle = app.getRequestHandler();

const CryptoJS = require("./lib/CryptoJS");
const XMLParser = require("xml2js").parseString;
const Utils = require("./lib/Utils");

const system = dev ?
    JSON.parse(fs.readFileSync("config.json", "utf8")) :
    JSON.parse(fs.readFileSync("config.prod.json", "utf8"));

// Uncomment to use a proxy to test TBA or other troubleshooting
// You will need to npm install https-proxy-agent if you haven't done so already.
/*
var HttpsProxyAgent = require('https-proxy-agent');
var HttpProxyAgent = require('http-proxy-agent');
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
*/
// Place the code below in a fetch block to make that block use a proxy.
// You will probably need to change the port in the proxy agent.
// Format may be different for GET or POST requests.
/*,
agent:new HttpsProxyAgent('http://127.0.0.1:23772'),
rejectUnauthorized: false,
strictSSL: false
*/


app.prepare().then(() => {
    const server = express();
    server.use(bodyParser.json()); // to support JSON-encoded bodies
    server.use(
        bodyParser.urlencoded({
            // to support URL-encoded bodies
            extended: true
        })
    );

    // logging part
    server.use(morgan('combined', { stream: winston.stream }));
    server.use(function(err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = dev ? err : {};

        // winston logging
        winston.error(`${err.status || 500} - ${err.message} - ${req.originalUrl} - ${req.method} - ${req.ip}`);

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });
    server.use(bodyParser.json({limit: '50mb'}));
    server.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

    /*
    //User-based authentication
    function NSAuth(scriptID, type = "post") {
        return "NLAuth nlauth_account=4099054_SB1, nlauth_email=yeastman@whitelabs.com, nlauth_signature=<redacted>, nlauth_role=1067";
    }
    */

    //Token-based authentication
    function NSAuth(scriptID, type = 'post', deployId = "1") {
        //RESTlet deployments cannot be in TESTING mode with token-based authentication
        var time = Math.round(new Date().getTime() / 1000);
        var nonce = Utils.uuid();

        var base = "deploy=" + deployId
            + "&oauth_consumer_key=" + system.NSAuthentication.consumerKey
            + "&oauth_nonce=" + nonce
            + "&oauth_signature_method=HMAC-SHA1"
            + "&oauth_timestamp=" + time
            + "&oauth_token=" + system.NSAuthentication.consumerToken
            + "&oauth_version=1.0"
            + "&script=" + scriptID.toString();

        var encodedBase = type.toUpperCase() + "&"
            + encodeURIComponent(system.NSAuthentication.NSURIBase) + "&" + encodeURIComponent(base);
        var baseSignature = system.NSAuthentication.consumerSecret + "&" + system.NSAuthentication.consumerTokenSecret;
        var signature = encodeURIComponent(CryptoJS.HmacSHA1(encodedBase, baseSignature).toString(CryptoJS.enc.Base64));

        var header = 'OAuth realm="' + system.NSAuthentication.NSAccountNo + '",'
            + 'oauth_consumer_key="' + system.NSAuthentication.consumerKey + '",'
            + 'oauth_token="' + system.NSAuthentication.consumerToken + '",'
            + 'oauth_nonce="' + nonce + '",'
            + 'oauth_timestamp="' + time + '",'
            + 'oauth_signature_method="HMAC-SHA1",'
            + 'oauth_version="1.0", '
            + 'oauth_signature="' + signature + '"';

        return header;
    }

    function NSReceiveMessage(message) {
        return JSON.parse(
            CryptoJS.AES.decrypt(message.data, system.NSAuthentication.ReceiveAuth, { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: system.NSAuthentication.Receiveiv }).toString(
                CryptoJS.enc.Utf8
            )
        );
    }

    function NSSendMessage(data) {
        //data.version = SalesLib.clientVersion;
        var message = {
            data: CryptoJS.AES.encrypt(JSON.stringify(data), system.NSAuthentication.SendAuth, {
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7,
                iv: system.NSAuthentication.Sendiv
            }).toString()
        };
        return JSON.stringify(message);
    }

    function XMLtoJSON(xml, cb) {
        XMLParser(xml, function (error, result) {
            if (error) {
                throw error;
            } else {
                cb(result);
            }
        });
    }

    function logErrorToConsoleIfRequired(response, functionName) {
        var errorCode = getErrorCode(response);
        if (!(errorCode == "WS_CONCUR_SESSION_DISALLWD" || errorCode == "WS_REQUEST_BLOCKED" || errorCode == -1)) {
            console.log("server", functionName, response, true);
        }
    }

    function getErrorCode(response) {
        // As far as I can see, response.error.code is undefined. The value they're looking for in field code appears to be in field name.
        if (response.error) {
            if (response.error.code) {
                return response.error.code;
            } else if (response.error.name) {
                return response.error.name;
            } else if (response.error.cause && response.error.cause.code) {
                return response.error.cause.code;
            } else {
                return "-1";
            }
        } else {
            return null;
        }
    }

    function loopSplitOrderSubmission(res, response, numLoops) {
        if (response.type == "error.SuiteScriptError") {
            return res.send({ error: { message: response.message, code: -1 } });
        } else if (response.error) {
            logErrorToConsoleIfRequired(response, "prepare-order-loop");
            return res.send({ error: { message: response.error.message, code: -1 } });
        } else if (numLoops > 10) {
            try {
                var message = NSReceiveMessage(response);
                console.log(message);

                if (!message.items || message.items.length == 0) {
                    console.log("Items have been removed due to unavailability");
                    return res.send({ error: { message: "Items have been removed due to unavailability", code: 0 } });
                } else {
                    console.log('Order submission loop stuck, aborting at ' + numLoops + ' loops');
                    return res.send({ error: { message: 'Order submission failed, please try again or contact White Labs for assistance.', code: -1 } });
                }
            } catch (error) {
                console.log('Order submission loop stuck, aborting at ' + numLoops + ' loops');
                return res.send({ error: { message: 'Order submission failed, please try again or contact White Labs for assistance.', code: -1 } });
            }
        } else {
            var message = NSReceiveMessage(response);
            //console.log(message);
            if (message.pricingAdded) {
                if (message.items && message.items.length > 0 && message.transitTimes) {
                    return res.send(message);
                } else {
                    console.log("Items have been removed due to unavailability");
                    //Returning the error to OE just seems to confuse it and prevents it from showing the "items removed" message
                    //return res.send({ error: { message: "Items have been removed due to unavailability", code: 0 } });
                    return res.send(message);
                }
            } else {
                // Process to get more items or to get pricing 
                message = NSSendMessage(message);
                fetch(system.ORDER.url, {
                    method: "PUT",
                    headers: {
                        Authorization: NSAuth(system.ORDER.id, "put"),
                        Accept: "application/json",
                        "Content-Type": "application/json"
                    },
                    body: message
                })
                    .then(response => response.json())
                    .then(response => {
                        if (response.type == "error.SuiteScriptError") {
                            return res.send({ error: { message: response.message, code: -1 } });
                        } else if (response.error) {
                            logErrorToConsoleIfRequired(response, "prepare-order-loop");
                            return res.send({ error: { message: response.error.message, code: -1 } });
                        } else {
                            //console.log("server", "prepare-order", "continuing to loop: " + (numLoops + 1));
                            return loopSplitOrderSubmission(res, response, numLoops + 1);
                        }
                    })
            }
        }
    }

    /******************
     * YMO Connections *
     ******************/

    server.post("/get-user-id", function (req, res, next) {
        var username = req.body.username;
        var password = req.body.password;

        var time = new Date();
        var data =
            '<CustomerInformationRequest Operation="Login">' +
            '<Token>' + system.YeastmanAuthentication.Token + "</Token>" +
            "<UserName>" + username.toString() + "</UserName>" +
            "<Password>" + password.toString() + "</Password>" +
            "<TimeStamp>" + time.getTime() + "</TimeStamp>" +
            "<Nonce>" + Utils.uuid() + "</Nonce>" +
            "</CustomerInformationRequest>";
        data = "Validate=" + CryptoJS.AES.encrypt(data, system.YeastmanAuthentication.Auth, { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: system.YeastmanAuthentication.iv }).toString();
        fetch(system.YeastmanAuthentication.url, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: data
        })
            .then(response => response.text())
            .then(response => {
                var data = CryptoJS.AES.decrypt(response, system.YeastmanAuthentication.Auth, {
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7,
                    iv: system.YeastmanAuthentication.iv
                }).toString(CryptoJS.enc.Utf8);
                XMLtoJSON(data, result => {
                    if (result.CustomerInformation.Result[0].$.Status == "OK") {
                        var isPasswordChangeRequired = result.CustomerInformation.NetSuiteID[0].$.PasswordChangeRequired;
                        if (!isPasswordChangeRequired) isPasswordChangeRequired = "No";

                        if (result.CustomerInformation.NetSuiteID[0].$.UserType == "Staff" || result.CustomerInformation.NetSuiteID[0]._ == "43148") {
                            return res.send({ userID: result.CustomerInformation.NetSuiteID[0]._, changePasswordRequired: isPasswordChangeRequired, isStaff: true });
                        } else {
                            return res.send({ userID: result.CustomerInformation.NetSuiteID[0]._, changePasswordRequired: isPasswordChangeRequired });
                        }
                    } else {
                        res.send({ error: { message: "Your username or password is invalid", code: 0 } });
                    }
                });
            })
            .catch(function (error) {
                console.log("error", error);
                res.send({ error: { message: error, code: -1 } });
            });
    });

    server.post("/create-yeastman-account", function (req, res, next) {
        //console.log(req.body.request.id);

        if (!req.body.request.id || req.body.request.id.length == 0) {
            res.send({ error: { message: "failed to create customer in NetSuite", code: 0 } });
            return;
        }

        var time = new Date();
        var data =
            '<CustomerInformationRequest Operation="Register">' +
            '<Token>' + system.YeastmanAuthentication.Token + "</Token>" +
            "<Password>" + req.body.request.password + "</Password>" +
            "<TimeStamp>" + time.getTime() + "</TimeStamp>" +
            "<Nonce>" + req.body.request.nonce + "</Nonce>" +
            "<ExternalID>" + req.body.request.id[0] + "</ExternalID>" +
            "<Email>" + req.body.request.email + "</Email>" +
            "</CustomerInformationRequest>";

        //console.log(data);

        data = "Validate=" + CryptoJS.AES.encrypt(data, system.YeastmanAuthentication.Auth, { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: system.YeastmanAuthentication.iv }).toString();
        fetch(system.YeastmanAuthentication.url, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: data
        })
            .then(response => response.text())
            .then(response => {
                var data = CryptoJS.AES.decrypt(response, system.YeastmanAuthentication.Auth, {
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7,
                    iv: system.YeastmanAuthentication.iv
                }).toString(CryptoJS.enc.Utf8);
                //console.log(data);
                XMLtoJSON(data, async result => {
                    //console.log(result.CustomerInformation);
                    //console.log(result.CustomerInformation.Result[0].$.Status);
                    if (result.CustomerInformation.Result[0].$.Status == "OK") {
                        res.sendStatus(200);
                    } else if (result.CustomerInformation.Result[0]._.includes("already exist") || result.CustomerInformation.Result[0]._.includes("already in use")) {
                        res.send({ error: { message: "This account already exists in Yeastman", code: 0 } });
                    } else {
                        res.send({ error: { message: "failed to create an account in Yeastman", code: 0 } });
                    }
                });
            });
    });

    server.post("/forgot-password", function (req, res, next) {
        var request = req.body.request;
        var time = new Date();

        var data =
            '<CustomerInformationRequest Operation="Reset Password">' +
            '<Token>' + system.YeastmanAuthentication.Token + "</Token>" +
            "<TimeStamp>" + time.getTime() + "</TimeStamp>" +
            "<Nonce>" + Utils.uuid() + "</Nonce>" +
            "<Email>" + request.email + "</Email>" +
            "<UserName>" + request.username + "</UserName>" +
            "</CustomerInformationRequest>";
        data =
            "Validate=" + CryptoJS.AES.encrypt(data, system.YeastmanAuthentication.Auth, { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: system.YeastmanAuthentication.iv }).toString();
        fetch(system.YeastmanAuthentication.url, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: data
        })
            .then(response => response.text())
            .then(function (response) {
                var data = CryptoJS.AES.decrypt(response, system.YeastmanAuthentication.Auth, {
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7,
                    iv: system.YeastmanAuthentication.iv
                }).toString(CryptoJS.enc.Utf8);
                XMLtoJSON(data, result => {
                    if (result.CustomerInformation.Result[0].$.Status == "OK") {
                        res.sendStatus(200);
                    } else {
                        res.send({ error: { message: "No account was found", code: 0 } });
                    }
                });
            });
    });

    server.post("/change-password", function (req, res, next) {
        const request = req.body.request;
        const user = request.user;
        const newPassword = request.newPassword;
        const currentPassword = request.currentPassword;
        var time = new Date();

        var data = '<CustomerInformationRequest Operation="Change Password"><Token>' + system.YeastmanAuthentication.Token + '</Token>'
            + '<NetSuiteID>' + user.id + '</NetSuiteID>'
            + '<UserName>' + user.username + '</UserName>'
            + '<Email>' + user.email + '</Email>'
            + '<NewPassword>' + newPassword + '</NewPassword>'
            + '<CurrentPassword>' + currentPassword + '</CurrentPassword>'
            + '<TimeStamp>' + time.getTime() + '</TimeStamp>'
            + '<Nonce>' + Utils.uuid() + '</Nonce>'
            + '</CustomerInformationRequest>';

        data = 'Validate=' + CryptoJS.AES.encrypt(data, system.YeastmanAuthentication.Auth, { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: system.YeastmanAuthentication.iv }).toString();
        fetch(system.YeastmanAuthentication.url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: data
        })
            .then(response => response.text())
            .then(function (response) {
                var data = CryptoJS.AES.decrypt(response, system.YeastmanAuthentication.Auth, {
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7,
                    iv: system.YeastmanAuthentication.iv
                }).toString(CryptoJS.enc.Utf8);
                XMLtoJSON(data, result => {
                    if (result.CustomerInformation.Result[0].$.Status == "OK") {
                        res.sendStatus(200);
                    } else {
                        res.send({ error: { message: "Could not process your request. Please contact White Labs for support.", code: 0 } });
                    }
                });
            });
    });

    server.post("/activate-account", function (req, res, next) {
        var request = req.body.request;

        var time = new Date();

        var subjective;
        if (request.userOrEmail) {
            subjective = "<AccountNumber>" + request.userInfo + "</AccountNumber>";
        } else {
            subjective = "<Email>" + request.userInfo + "</Email>";
        }

        var data =
            '<CustomerInformationRequest Operation="Lookup">' +
            '<Token>' + system.YeastmanAuthentication.Token + "</Token>" +
            "<TimeStamp>" + time.getTime() + "</TimeStamp>" +
            "<Nonce>" + Utils.uuid() + "</Nonce>" +
            subjective +
            "</CustomerInformationRequest>";
        data =
            "Validate=" + CryptoJS.AES.encrypt(data, system.YeastmanAuthentication.Auth, { mode: CryptoJS.mode.CBC, padding: CryptoJS.pad.Pkcs7, iv: system.YeastmanAuthentication.iv }).toString();
        fetch(system.YeastmanAuthentication.url, {
            method: "POST",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: data
        })
            .then(response => response.text())
            .then(response => {
                var data = CryptoJS.AES.decrypt(response, system.YeastmanAuthentication.Auth, {
                    mode: CryptoJS.mode.CBC,
                    padding: CryptoJS.pad.Pkcs7,
                    iv: system.YeastmanAuthentication.iv
                }).toString(CryptoJS.enc.Utf8);
                XMLtoJSON(data, result => {
                    if (result.CustomerInformation.Result[0].$.Status == "OK") {
                        res.sendStatus(200);
                    } else {
                        res.send({ error: { message: "Could not recover your account please try again later", code: 0 } });
                    }
                });
            });
    });

    /***********************
     * NetSuite Connections *
     ***********************/

    /**
     * Get all inventory
     *
     * @return [Items] - Returns array of items
     */
    server.get("/inventory", (req, res, next) => {
        fetch(system.ITEM.url, {
            method: "GET",
            headers: {
                Authorization: NSAuth(system.ITEM.id, "get"),
                Accept: "application/json",
                "Content-Type": "application/json"
            }
        })
            .then(response => response.json())
            .then(response => {
                if (response.type == "error.SuiteScriptError") {
                    res.send({ error: { message: response.message, code: -1 } });
                } else if (response.error) {
                    logErrorToConsoleIfRequired(response, "inventory");
                    res.send({ error: { message: response.error.message, code: getErrorCode(response) } });
                } else {
                    var message = NSReceiveMessage(response);

                    if (message.items && message.items.length > 0) {
                        return res.send(message);
                    } else {
                        res.send({ error: { message: "No items in inventory", code: -1 } });
                    }
                }
            })
            .catch(error => {
                res.send({ error: { message: error, code: -1 } });
            });
    });

    /**
     * Get item availability
     *
     * @param {Integer} itemID - Request item availability for item with id
     *
     * @return {Map} - Returns map of ship locations and quantity available
     */
    server.post("/item-availability", function (req, res, next) {
        var itemID = req.body.itemID;
        var subsidiary = req.body.subsidiary;
        var quantity = req.body.quantity;

        if (itemID) {
            var body = NSSendMessage({ itemID, subsidiary, quantity });

            fetch(system.ITEM.url, {
                method: "POST",
                headers: {
                    Authorization: NSAuth(system.ITEM.id, "post"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "item-availability");
                        res.send({ error: { message: response.error.message, code: getErrorCode(response) } });
                    } else {
                        var message = NSReceiveMessage(response);
                        return res.send(message);
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "No item provided. Cannot get item availability", code: -1 } });
        }
    });

    /**
     * Get user information
     *
     * @param {Integer} userID - Request user information for user with id
     *
     * @return {Object} - Returns object with items and ship dates, transit times, ship locations, and pricing
     */
    server.post("/get-user-info", function (req, res, next) {
        var userID = req.body.userID;

        if (userID) {
            var body = NSSendMessage({ id: userID, get: true });

            fetch(system.CUST.url, {
                method: "POST",
                headers: {
                    Authorization: NSAuth(system.CUST.id, "post"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "get-user-info");
                        res.send({ error: { message: response.error.message, code: getErrorCode(response) } });
                    } else {
                        var message = NSReceiveMessage(response);
                        //console.log(message);
                        if (message) {
                            return res.send(message);
                        } else {
                            res.send({ error: { message: "user info not working", code: -1 } });
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "User is not logged in, cannot retrieve User Info", code: -1 } });
        }
    });

    /**
     * Prepare order
     *
     * @param {Object} order - Object with item information
     *
     * @return {Object} - Returns object with items and ship dates, transit times, ship locations, and pricing
     */
    server.post("/prepare-order", function (req, res, next) {
        const request = req.body;
        //console.log(request);
        if (request.user.id) {
            request.splitProcessing = true;
            var message = NSSendMessage(request);

            fetch(system.ORDER.url, {
                method: "PUT",
                headers: {
                    Authorization: NSAuth(system.ORDER.id, "put"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: message
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        return res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "prepare-order");
                        return res.send({ error: { message: response.error.message, code: -1 } });
                    } else {
                        var message = NSReceiveMessage(response);
                        //console.log(message);
                        if (request.splitProcessing) {
                            // Process again to get items
                            message = NSSendMessage(message);

                            fetch(system.ORDER.url, {
                                method: "PUT",
                                headers: {
                                    Authorization: NSAuth(system.ORDER.id, "put"),
                                    Accept: "application/json",
                                    "Content-Type": "application/json"
                                },
                                body: message
                            })
                                .then(response => response.json())
                                .then(response => {
                                    //console.log(response);
                                    if (response.type == "error.SuiteScriptError") {
                                        return res.send({ error: { message: response.message, code: -1 } });
                                    } else if (response.error) {
                                        logErrorToConsoleIfRequired(response, "prepare-order");
                                        return res.send({ error: { message: response.error.message, code: -1 } });
                                    } else {
                                        // Process to get more items or to get pricing
                                        return loopSplitOrderSubmission(res, response, 0);
                                    }
                                })
                        } else {
                            if (message.items && message.items.length > 0 && message.transitTimes) {
                                return res.send(message);
                            } else {
                                console.log("Items have been removed due to unavailability");
                                //return res.send({ error: { message: "Items have been removed due to unavailability", code: 0 } });
                                return res.send(message);
                            }
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "User is not logged in, cannot retrieve ship dates", code: -1 } });
        }
    });

    /**
     * Place order in NetSuite
     *
     * @param {Object} order - Object with items and user information
     *
     * @return [Integer] - Array of integers representing order numbers
     */
    server.post("/place-order", function (req, res, next) {
        const request = req.body.request;

        if (!request) {
            console.log('Missing or incomplete place-order request');
            if (req.body) {
                console.log(req.body);
            } else {
                console.log(req);
            }

            res.send({ error: { message: "Could not submit order. Please return to the cart and try again.", code: 0 } });
        } else if (request.user && request.user.id) {
            var message = NSSendMessage(request);

            fetch(system.ORDER.url, {
                method: "POST",
                headers: {
                    Authorization: NSAuth(system.ORDER.id, "post"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: message
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "place-order");
                        res.send({ error: { message: response.error.message, code: getErrorCode(response) } });
                    } else {
                        var message = NSReceiveMessage(response);

                        if (message.orderNum.length > 0) {
                            res.status(200).send(message);
                        } else {
                            res.send({ error: { message: "Items have been removed due to unavailability", code: 0 } });
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "User is not logged in, cannot place order", code: 0 } });
        }
    });

    /**
     * Update user account information in NetSuite
     *
     * @param {Object} custInfo - Object containing customer info to be changed
     *
     * @return null if success
     */
    server.post("/update-user-info", function (req, res, next) {
        const request = req.body.request;
        //console.log(request);

        if (request.id) {
            var message = NSSendMessage(request);

            fetch(system.CUST.url, {
                method: "PUT",
                headers: {
                    Authorization: NSAuth(system.CUST.id, "put"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: message
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "update-user-info");
                        res.send({ error: { message: response.error.message, code: getErrorCode(response) } });
                    } else {
                        var message = NSReceiveMessage(response);
                        res.sendStatus(200);
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "User is not logged in, cannot change User Info", code: -1 } });
        }
    });

    server.post("/add-subsidiary", function (req, res, next) {
        var request = req.body.request;

        if (request.id) {
            const body = NSSendMessage(request);

            fetch(system.CUST.url, {
                method: 'PUT',
                headers: {
                    Authorization: NSAuth(system.CUST.id, "put"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "add-subsidiary");
                        res.send({ error: { message: response.error.message, code: -1 } });
                    } else {
                        var message = NSReceiveMessage(response);
                        if (message.id) {
                            res.send(message);
                        } else {
                            res.send({ error: { message: "Failed to create subsidiary", code: 0 } });
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                })
        } else {
            res.send({ error: { message: "User is not logged in, cannot create subsidiary", code: -1 } });
        }
    })

    server.post("/record-survey-responses", function (req, res, next) {
        var request = req.body.request;

        const body = NSSendMessage(request);

        fetch(system.CUST.url, {
            method: 'PUT',
            headers: {
                Authorization: NSAuth(system.CUST.id, "put"),
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body
        })
            .then(response => response.json())
            .then(response => {
                if (response.type == "error.SuiteScriptError") {
                    res.send({ error: { message: response.message, code: -1 } });
                } else if (response.error) {
                    logErrorToConsoleIfRequired(response, "record-survey-responses");
                    res.send({ error: { message: response.error.message, code: -1 } });
                } else {
                    var message = NSReceiveMessage(response);
                    if (message.id) {
                        res.send(message);
                    } else {
                        res.send({ error: { message: "Failed to record survey responses", code: 0 } });
                    }
                }
            })
            .catch(error => {
                res.send({ error: { message: error, code: -1 } });
            })
    })

    /**
     * Get order history for a customer
     *
     * @param String userID - User Id of customer requesting order history
     *
     * @return [Object] - Array of order objects
     */
    server.post("/get-order-history", function (req, res, next) {
        var request = req.body.request;

        if (request.id) {
            const body = NSSendMessage({ id: request.id, admin: false, get: true });

            fetch(system.ORDER.url, {
                method: "POST",
                headers: {
                    Authorization: NSAuth(system.ORDER.id, "post"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "get-order-history");
                        res.send({ error: { message: response.error.message, code: -1 } });
                    } else {
                        var message = NSReceiveMessage(response);
                        if (message.orderHistory && message.orderHistory.length > 0) {
                            res.send(message);
                        } else {
                            res.send({ error: { message: "No past orders were found", code: 0 } });
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "User is not logged in, cannot retrieve Order History", code: -1 } });
        }
    });

    /**
     * Get pricing for order
     *
     * @param {Object} request - Object containing items, shipmethod, and coupon code (optional)
     *
     * @return {Object} - Object containing item subtotal, shipping subtotal, and order subtotal
     */
    server.post("/get-order-price", function (req, res, next) {
        var request = req.body.request;
        if (request.userID) {
            var message = NSSendMessage(request);

            fetch(system.ORDER.url, {
                method: "PUT",
                headers: {
                    Authorization: NSAuth(system.ORDER.id, "put"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: message
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "get-order-price");
                        res.send({ error: { message: response.error.message, code: -1 } });
                    } else if (response.message && response.message.toLowerCase().includes("invalid couponcode")) {
                        res.send({ error: { message: "Invalid coupon code", code: 0 } });
                    } else {
                        var message = NSReceiveMessage(response);
                        if (message.orderSubtotal) {
                            res.send(message);
                        } else {
                            res.send({ error: { message: "No subtotal", code: -1 } });
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "User is not logged in, cannot retrieve pricing", code: -1 } });
        }
    });

    /**
     * Create customer account in NetSuite
     *
     * @param {Object} custInfo - Object containing all customer's info required upon registration
     *
     * @return {Object} - Object containing array of ids for each related customer account, username, and token id
     */
    server.post("/create-netsuite-account", function (req, res, next) {
        //var custInfo = req.body.custInfo;
        var custInfo = req.body.request;
        //console.log(custInfo);
        var message = NSSendMessage(custInfo);
        creditInfo = null;

        fetch(system.CUST.url, {
            method: "POST",
            headers: {
                Authorization: NSAuth(system.CUST.id),
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: message
        })
            .then(response => response.json())
            .then(response => {
                //console.log(response);
                if (response.type == "error.SuiteScriptError") {
                    res.send({ error: { message: response.message, code: -1 } });
                } else if (response.error) {
                    logErrorToConsoleIfRequired(response, "create-netsuite-account");
                    res.send({ error: { message: response.error.message, code: getErrorCode(response) } });
                } else {
                    var respMessage = NSReceiveMessage(response);
                    //respMessage.nonce = message.nonce;
                    //console.log(respMessage);
                    res.send(respMessage);
                }
            })
            .catch(error => {
                //console.log(error);
                res.send({ error: { message: error, code: -1 } });
            });
    });

    /**
     * Get similar strains for a particular item
     *
     * @param {int} userID - ID of user logged in
     * @param {Object} item - the yeast item being exchanged
     * @param {Object} itemRef - the inventory item reference to the yeast item
     * @param [string] selectedStyles - List of beer styles that will be used
     * 		to look up alternative strains
     * @return [Object] - array of strains
     */
    server.post("/similar-strains", function (req, res, next) {
        var request = req.body.request;

        if (request.id) {
            var message = NSSendMessage(request);

            fetch(system.ITEM.url, {
                method: "PUT",
                headers: {
                    Authorization: NSAuth(system.ITEM.id, "put"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: message
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "similar-strains");
                        res.send({ error: { message: response.error.message, code: -1 } });
                    } else {
                        var message = NSReceiveMessage(response);
                        res.send(message);
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "User is not logged in", code: -1 } });
        }
    });

    /**
     * Get alternate sizes for a particular item
     *
     * @param {int} userID - ID of user logged in
     * @param {Object} item - the yeast item being exchanged
     * @param {Object} itemRef - the inventory item reference to the yeast item
     *
     * @return [Object] - array of strains
     */
    server.post("/alternate-sizes", function (req, res, next) {
        var request = req.body.request;

        if (request.id) {
            var message = NSSendMessage(request);

            fetch(system.ITEM.url, {
                method: "PUT",
                headers: {
                    Authorization: NSAuth(system.ITEM.id, "put"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: message
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "alternate-sizes");
                        res.send({ error: { message: response.error.message, code: -1 } });
                    } else {
                        var message = NSReceiveMessage(response);
                        res.send(message);
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "User is not logged in", code: -1 } });
        }
    });

    /**
     * Get customer list
     */
    server.post("/request-customer-lookup", function (req, res) {
        var custName = req.body.custName;
        var version = 'YMO2';

        if (custName) {
            var message = NSSendMessage({ name: custName, version: version });

            fetch(system.FIND_CUSTOMER.url, {
                method: 'POST',
                headers: {
                    'Authorization': NSAuth(system.FIND_CUSTOMER.id, "post"),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: message
            })
                .then((response) => response.json())
                .then(function (response) {
                    if (response.type == 'error.SuiteScriptError') {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        if (!(response.error.code == "WS_CONCUR_SESSION_DISALLWD" || response.error.code == "WS_REQUEST_BLOCKED" || response.error.code == -1)) {
                            console.log("server", "request-customer-lookup", response, true);
                        }

                        res.send({ error: { message: response.error.message, code: -1 } });
                    }
                    else {
                        var message = NSReceiveMessage(response);
                        //console.log(message);
                        if (message.length > 0) {
                            res.send({ customers: message });
                        } else {
                            res.send({ error: { message: 'No customers found matching "' + custName + '".', code: -1 } });
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        }
        else {
            res.send({ error: { message: "Please enter a valid customer name", code: -1 } });
        }
    });

    /**
     * Get customer list
     */
    server.post("/request-customer-lookup", function (req, res) {
        var custName = req.body.custName;
        var version = req.body.version;

        if (custName) {
            var message = NSSendMessage({ name: custName, version: version });

            fetch(system.FIND_CUSTOMER.url, {
                method: 'POST',
                headers: {
                    'Authorization': NSAuth(system.FIND_CUSTOMER.id, "post"),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: message
            })
                .then((response) => response.json())
                .then(function (response) {
                    if (response.type == 'error.SuiteScriptError') {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        if (!(response.error.code == "WS_CONCUR_SESSION_DISALLWD" || response.error.code == "WS_REQUEST_BLOCKED" || response.error.code == -1)) {
                            console.log("server", "request-customer-lookup", response, true);
                        }

                        res.send({ error: { message: response.error.message, code: -1 } });
                    }
                    else {
                        var message = NSReceiveMessage(response);
                        if (message.length > 0) {
                            res.send(message);
                        } else {
                            res.send({ error: { message: 'No customers found for: ' + custName, code: -1 } });
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        }
        else {
            res.send({ error: { message: "Please enter a valid customer name", code: -1 } });
        }
    });

    /**
     * Get related items for a particular item
     *
     * @param {string} itemId - the item ID being checked for related items
     * @return [Object] - array of related items (may be empty)
     */
    server.post("/related-items", function (req, res, next) {
        var itemId = req.body.itemId;

        if (itemId) {
            var message = NSSendMessage({ itemId: itemId, relatedItems: true });

            fetch(system.ITEM.url, {
                method: "PUT",
                headers: {
                    Authorization: NSAuth(system.ITEM.id, "put"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: message
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "related-items");
                        res.send({ error: { message: response.error.message, code: -1 } });
                    } else {
                        var message = NSReceiveMessage(response);
                        message.itemId = itemId;
                        res.send(message);
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            res.send({ error: { message: "Item ID missing", code: -1 } });
        }
    });

    /**
     * Log something order-related to NetSuite
     */
    server.post("/log-checkout", function (req, res, next) {
        var request = req.body.request;
        request.get = true;
        request.logCheckoutView = true;
        const body = NSSendMessage(request);

        fetch(system.ORDER.url, {
            method: "POST",
            headers: {
                Authorization: NSAuth(system.ORDER.id, "post"),
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: body
        })
            .then(response => response.json())
            .then(response => {
                if (response.type == "error.SuiteScriptError") {
                    console.log("server", "log-checkout", response.message, true);
                } else if (response.error) {
                    logErrorToConsoleIfRequired(response, "log-checkout");
                } else {
                    // Do nothing, no return value needed
                }
            })
            .catch(error => {
                console.log("server", "log-checkout", error, true);
            });
    });

    /**
     * Get QC results
     *
     * @param {string} itemId - the item ID being checked for qc results
     * @param {string} lotNumber - the lot being checked for qc results
     * @return [Object] - QC results in JSON format
     */
    server.post("/qc-results", function (req, res, next) {
        var itemId = req.body.itemId;
        var lotNumber = req.body.lotNumber;
        var packagingId = req.body.packagingId;

        //console.log(req.body);

        if ((itemId || packagingId) && lotNumber) {
            var message = NSSendMessage({ itemId: itemId, lotNumber: lotNumber, packagingId: packagingId, qcReport: true });

            fetch(system.ITEM.url, {
                method: "PUT",
                headers: {
                    Authorization: NSAuth(system.ITEM.id, "put"),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: message
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "qc-results");
                        res.send({ error: { message: response.error.message, code: -1 } });
                    } else {
                        var message = NSReceiveMessage(response);
                        res.send(message);
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            if (!itemId && !lotNumber) {
                res.send({ error: { message: "Item ID and lot number missing", code: -1 } });
            } else if (!itemId) {
                res.send({ error: { message: "Item ID missing", code: -1 } });
            } else {
                res.send({ error: { message: "Lot number missing", code: -1 } });
            }
        }
    });

    /**
     * Ask NetSuite to resend an invoice for a previous order
     *
     * @param {string} orderId - the order number to be re-sent
     * @param {string} email - the recipient of the invoice email
     */
    server.post("/resend-invoice", function (req, res, next) {
        var orderId = req.body.orderId;
        var email = req.body.email;

        //console.log(req.body);

        if (orderId && email) {
            var message = NSSendMessage({ orderID: orderId, email: email, version: 'YMO2' });

            fetch(system.RESEND_INVOICE.url, {
                method: "POST",
                headers: {
                    Authorization: NSAuth(system.RESEND_INVOICE.id, "post", system.RESEND_INVOICE.deploy),
                    Accept: "application/json",
                    "Content-Type": "application/json"
                },
                body: message
            })
                .then(response => response.json())
                .then(response => {
                    if (response.type == "error.SuiteScriptError") {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        logErrorToConsoleIfRequired(response, "resend-invoice");
                        res.send({ error: { message: response.error.message, code: -1 } });
                    } else {
                        var message = NSReceiveMessage(response);
                        res.send(message);
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        } else {
            if (!orderId && !email) {
                res.send({ error: { message: "Order number and recipient address missing", code: -1 } });
            } else if (!orderId) {
                res.send({ error: { message: "Order number missing", code: -1 } });
            } else {
                res.send({ error: { message: "Recipient address missing", code: -1 } });
            }
        }
    });

    /**
     * Get customer list
     */
    server.post("/request-customer-lookup", function (req, res) {
        var custName = req.body.custName;
        var version = req.body.version;

        if (custName) {
            var message = NSSendMessage({ name: custName, version: version });

            fetch(system.FIND_CUSTOMER.url, {
                method: 'POST',
                headers: {
                    'Authorization': NSAuth(system.FIND_CUSTOMER.id, "post"),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: message
            })
                .then((response) => response.json())
                .then(function (response) {
                    if (response.type == 'error.SuiteScriptError') {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        if (!(response.error.code == "WS_CONCUR_SESSION_DISALLWD" || response.error.code == "WS_REQUEST_BLOCKED" || response.error.code == -1)) {
                            console.log("server", "request-customer-lookup", response, true);
                        }

                        res.send({ error: { message: response.error.message, code: -1 } });
                    }
                    else {
                        var message = NSReceiveMessage(response);
                        if (message.length > 0) {
                            res.send(message);
                        } else {
                            res.send({ error: { message: 'No customers found for: ' + custName, code: -1 } });
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        }
        else {
            res.send({ error: { message: "Please enter a valid customer name", code: -1 } });
        }
    });

    /**
     * Get customer list
     */
    server.post("/request-customer-lookup", function (req, res) {
        var custName = req.body.custName;
        var version = req.body.version;

        if (custName) {
            var message = NSSendMessage({ name: custName, version: version });

            fetch(system.FIND_CUSTOMER.url, {
                method: 'POST',
                headers: {
                    'Authorization': NSAuth(system.FIND_CUSTOMER.id, "post"),
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: message
            })
                .then((response) => response.json())
                .then(function (response) {
                    if (response.type == 'error.SuiteScriptError') {
                        res.send({ error: { message: response.message, code: -1 } });
                    } else if (response.error) {
                        if (!(response.error.code == "WS_CONCUR_SESSION_DISALLWD" || response.error.code == "WS_REQUEST_BLOCKED" || response.error.code == -1)) {
                            console.log("server", "request-customer-lookup", response, true);
                        }

                        res.send({ error: { message: response.error.message, code: -1 } });
                    }
                    else {
                        var message = NSReceiveMessage(response);
                        if (message.length > 0) {
                            res.send(message);
                        } else {
                            res.send({ error: { message: 'No customers found for: ' + custName, code: -1 } });
                        }
                    }
                })
                .catch(error => {
                    res.send({ error: { message: error, code: -1 } });
                });
        }
        else {
            res.send({ error: { message: "Please enter a valid customer name", code: -1 } });
        }
    });

    server.get("*", (req, res, next) => {
        return handle(req, res, next);
    });

    server.listen(5000, error => {
        if (error) throw error;
        console.log("> Ready on http://localhost:5000");
    });
})
    .catch(ex => {
        console.error(ex.stack);
        process.exit(1);
    });
